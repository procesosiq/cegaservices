var Services = angular.module('App.Services', [])
    .config(function ($httpProvider){
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
        $httpProvider.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
        $httpProvider.defaults.transformRequest = function(obj) {
            var str = [];
            for(var p in obj){
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            }
            return str.join("&");
        };
    });


app.controller('reportes', ['$scope','$http','$interval','client','$controller', function($scope,$http,$interval,client, $controller){

	$scope.init = function(){
		
		client.post("./controllers/index.php?accion=Reportes.getClientesEquipos" , function(r, b){
			b();
			$scope.clientes = r
            console.log(r)
		} , {});
	}

    $scope.verHistorico = function(equipo){
        var data = {
            cliente : $scope.selectCliente.id,
            sucursal : $scope.selectCliente.id,
            equipo : equipo.codigo,
        }
        client.post("./controllers/index.php?accion=Reportes.historicoEquipos", function(r, b){
            b()
            console.log(r);
        }, data)
    }

    $scope.verSucursal = function(cliente, sucursal){
        $scope.selectCliente = cliente
        $scope.selectSucursal = sucursal
        client.post("./controllers/index.php?accion=Reportes.getEquiposTipos", function(r, b){
            b()
            $scope.tiposEquipos = r;
        }, { id_sucursal : sucursal.id, id_cliente : cliente.id })
    }

	$scope.init();

}]);