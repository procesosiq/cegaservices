<?php
	/**
	
		ALIAS :
		- $arrayConfig [Arreglo que contiene todos los alias de las vistas con respecto a su clase] 
	
	 */
	$arrayConfig = array();
	
	$arrayConfig["clientes"] = "clientes";
	$arrayConfig["clienteList"] = "clientes";
    $arrayConfig["sucursales"] = "sucursales";
    $arrayConfig["sucursalesList"] = "sucursales";
    $arrayConfig["equipos"] = "equipos";
    $arrayConfig["equiposList"] = "equipos";

	/*----------  PATHS y OBJETO GLOBAL ----------*/
	$path = "./controllers";
	$loader = (object)array();
	/*----------  PATHS y OBJETO GLOBAL ----------*/

	/*----------  CARGAMOS LA CLASE DE LA VISTA  ----------*/
	$controlName = $path."/".$arrayConfig[$page].".php";
	if (file_exists($controlName)){
		include $path.'/conexion.php';
		include ($controlName);
		/*----------  CARGAMOS LA CLASE DE LA VISTA  ----------*/

		/*----------  CREAMOS LA INSTANCIA DE LA VISTA  ----------*/
		$nameClass = $arrayConfig[$page];
		// print_r($clientes);
		// die($nameClass);
		// $loader = new clientes();
		// die($nameClass);
		$loader = new $nameClass();
		/*----------  CREAMOS LA INSTANCIA DE LA VISTA  ----------*/
	}

?>