<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/
    include './controllers/class.sesion.php';
    $session = Session::getInstance();
    $session = (object)$session;
    // echo $session->nombre;
    if(!isset($session->logged)){
        header('Location: http://cegaservices2.procesos-iq.com/login.php');
    }

    if(isset($_GET["page"])){
        if($_GET["page"] != ""){
            $page = $_GET["page"];
            if ($page != "logout" || $page != "index" || $page != ""){
                include 'loader.php';
            }
        }else{
            $page = "404";
        }


    }else{
       $page = "index"; 
    }

    if(isset($_GET["page"])){
        if($_GET["page"] == "logout"){
            Session::getInstance()->kill();
            header('Location: http://cegaservices2.procesos-iq.com/login.php');
        }
    }


    //$html = basename($_SERVER['PHP_SELF']);
    /*=====  End of Insertar aqui validacion  de usuarios  ======*/
?>
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.6
Version: 4.5.4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Cegaservices</title>
        <link rel="icon" href="iqicon.png"/>
        <link rel="apple-touch-icon" href="iqicon.png"/>
        <link rel="apple-touch-icon-precomposed" href="iqicon.png"/>
        <link rel="icon" href="iqicon.png"/>
        <link rel="icon" href="iqicon.png"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="http://cdn.procesos-iq.com/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="http://cdn.procesos-iq.com/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="http://cdn.procesos-iq.com/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="http://cdn.procesos-iq.com/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="http://cdn.procesos-iq.com/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="http://cdn.procesos-iq.com/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="http://cdn.procesos-iq.com/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="http://cdn.procesos-iq.com/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="http://cdn.procesos-iq.com/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />


        <link href="../metronic_v4.5.4/theme/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
        <link href="http://cdn.procesos-iq.com/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
        <link href="http://cdn.procesos-iq.com/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
        <link href="http://cdn.procesos-iq.com/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
        <link href="http://cdn.procesos-iq.com/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
        <link href="http://cdn.procesos-iq.com/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css" />
        <link href="http://cdn.procesos-iq.com/global/plugins/clockface/css/clockface.css" rel="stylesheet" type="text/css" />
        <link href="http://cdn.procesos-iq.com/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="http://cdn.procesos-iq.com/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="http://cdn.procesos-iq.com/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="../assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/layouts/layout4/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color" />
        <!-- <link href="../assets/layouts/layout4/css/themes/sigat.css" rel="stylesheet" type="text/css" id="style_color" /> -->
        <link href="../assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> 

        <script type="text/javascript" src="//code.jquery.com/jquery-2.1.1.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    </head>
    <!-- END HEAD -->

    <style>
        ul.page-sidebar-menu > li > .active {
            background: #5b9bd1 !important;
        }
    
        .nobmp-reporte-accordion{
            border: 0 !important;
            margin: 0 !important;
            padding: 0 !important;
        }
    </style>
    <body class="page-container-bg-solid page-header-fixed page-footer-fixed page-sidebar-closed-hide-logo">
        <?php include ("./header.php"); ?>
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("./menu.php"); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
            <?php 
                if(
                    !($session->tipo_usuario == 2 && $page != "readCalendar") &&
                    !($session->tipo_usuario == 3 && ($page != "cotizacionList" && $page != "inventario"))
                ){
                    include ("./views/{$page}.php"); 
                    echo $page; 
                }
            ?>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <?php include("./footer.php");?>
        <!--[if lt IE 9]>
<script src="http://cdn.procesos-iq.com/global/plugins/respond.min.js"></script>
<script src="http://cdn.procesos-iq.com/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <!--script src="http://cdn.procesos-iq.com/global/plugins/jquery.min.js" type="text/javascript"></script> victor-->
    
        <script src="http://cdn.procesos-iq.com/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="http://cdn.procesos-iq.com/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="http://cdn.procesos-iq.com/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="http://cdn.procesos-iq.com/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="http://cdn.procesos-iq.com/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="http://cdn.procesos-iq.com/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="http://cdn.procesos-iq.com/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="http://cdn.procesos-iq.com/global/plugins/moment.min.js" type="text/javascript"></script>
        <script src="http://cdn.procesos-iq.com/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
        <script src="http://cdn.procesos-iq.com/global/plugins/morris/morris.min.js" type="text/javascript"></script>
        <script src="http://cdn.procesos-iq.com/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
        <script src="http://cdn.procesos-iq.com/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
        <script src="http://cdn.procesos-iq.com/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
        <script src="http://cdn.procesos-iq.com/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
        <!-- <script src="http://cdn.procesos-iq.com/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
        <script src="http://cdn.procesos-iq.com/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
        <script src="http://cdn.procesos-iq.com/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
        <script src="http://cdn.procesos-iq.com/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
        <script src="http://cdn.procesos-iq.com/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
        <script src="http://cdn.procesos-iq.com/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
        <script src="http://cdn.procesos-iq.com/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
        <script src="http://cdn.procesos-iq.com/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
        <script src="http://cdn.procesos-iq.com/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
        <script src="http://cdn.procesos-iq.com/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
        <script src="http://cdn.procesos-iq.com/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
        <script src="http://cdn.procesos-iq.com/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
        <script src="http://cdn.procesos-iq.com/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
        <script src="http://cdn.procesos-iq.com/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
        <script src="http://cdn.procesos-iq.com/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
        <script src="http://cdn.procesos-iq.com/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
        <script src="http://cdn.procesos-iq.com/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
        <script src="http://cdn.procesos-iq.com/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
        <script src="http://cdn.procesos-iq.com/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
        <script src="http://cdn.procesos-iq.com/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
        <script src="http://cdn.procesos-iq.com/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
        <script src="http://cdn.procesos-iq.com/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
        <script src="http://cdn.procesos-iq.com/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script> -->
        <!-- END PAGE LEVEL PLUGINS -->
        <?php
            if(
                !($session->tipo_usuario == 2 && $page != "readCalendar") &&
                !($session->tipo_usuario == 3 && ($page != "cotizacionList" && $page != "inventario"))
            ){
                $file_js_php = "./views/{$page}.js.php";
                if (file_exists($file_js_php)){
                    include ($file_js_php);
                }
            }

        ?>
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="../assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <!--<script src="../assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>-->
        <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.10.0/ui-bootstrap-tpls.min.js"></script> -->
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="../assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
        <script src="../assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
        <script src="../assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="http://cdn.procesos-iq.com/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js"></script>
         <script src="../views/js/main.js" type="text/javascript"></script> 
         <script src="./js/directives.js" type="text/javascript"></script> 

        <script>
            <?php
                if(
                    !($session->tipo_usuario == 2 && $page != "readCalendar") &&
                    !($session->tipo_usuario == 3 && ($page != "cotizacionList" && $page != "inventario"))
                ){
                    $file_js = "./views/js/{$page}.js";
                    if (file_exists($file_js)){
                        include ($file_js);
                    }
                }
            ?>
        </script>
    </body>

</html>