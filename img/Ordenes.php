<?php

/**
* 
*/
class Ordenes
{
	
    private $conexion;
    private $session;
    
    public function __construct(){
        $this->conexion = new M_Conexion;
        $this->session = Session::getInstance();
    }
    

    public function index(){
    	 $datos = (object)$_POST;
        
        $sWhere = "";
        $sOrder = " ORDER BY nombre";
        $DesAsc = "ASC";
        $sOrder .= " {$DesAsc}";
        $sLimit = "";
        // print_r($_POST);
        if(isset($_POST)){

            /*----------  ORDER BY ----------*/
            
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY id {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY fecha {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY cliente {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY  tipo_cliente {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 5){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY tipo_trabajo {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 6){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY num_equipos {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 7){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY status {$DesAsc}";
            }
            /*----------  ORDER BY ----------*/

            if(isset($_POST['order_id']) && trim($_POST['order_id']) != ""){
                $sWhere .= " AND id = ".$_POST["order_id"];
            }               
            if((isset($_POST['order_date_from']) && trim($_POST['order_date_from']) != "") && (isset($_POST['order_date_to']) && trim($_POST['order_date_to']) != "")){
                $sWhere .= " AND fecha BETWEEN '".$_POST["order_date_from"]."' AND '".$_POST["order_date_to"]."'";
            }
            if(isset($_POST['search_cliente']) && trim($_POST['search_cliente']) != ""){
                $sWhere .= " AND cliente LIKE '%".$_POST['search_cliente']."%'";
            }
            if(isset($_POST['search_tipo_cliente']) && trim($_POST['search_tipo_cliente']) != ""){
                $sWhere .= " AND (SELECT nombre FROM cat_clientesTipo WHERE id = tipo_cliente) LIKE '%".$_POST['search_tipo_cliente']."%'";
            }
            if(isset($_POST['order_tipo_trabajo']) && trim($_POST['order_tipo_trabajo']) != ""){
                $sWhere .= " AND tipo_trabajo LIKE '%".$_POST['order_tipo_trabajo']."%'";
            }
            if(isset($_POST['order_numero_equipo']) && trim($_POST['order_numero_equipo']) != ""){
                $sWhere .= " AND num_equipos LIKE '%".$_POST['order_numero_equipo']."%'";
            }
            if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
                $sWhere .= " AND status = ".$_POST['order_status'];
            }
            // if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
            //     $sWhere .= " AND cat_equipos.`status` = ".$_POST["order_status"];
            // }

            /*----------  LIMIT  ----------*/
            if(isset($_POST['length']) && $_POST['length'] > 0){
                $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
            }
        }

        $sql = "SELECT id, fecha_status as fecha, (SELECT nombre FROM cat_clientes WHERE id = id_cliente) AS cliente, (SELECT cat_clientesTipo.nombre FROM cat_clientes INNER JOIN cat_clientesTipo ON id_tipcli = cat_clientesTipo.id WHERE cat_clientes.id = id_cliente) AS tipo_cliente, '' AS tipo_trabajo, 0 as num_equipos, status FROM cotizaciones WHERE id_usuario = '{$this->session->logged}' AND status >= 2 
        $sWhere $sOrder $sLimit";
        $res = $this->conexion->link->query($sql);
        #print_r($sql);
        $response = (object)[];
        #$response->sql = $sql;
        while($fila = $res->fetch_assoc()){
            $fila = (object)$fila;
        
            $response->data[] = array (
                '<input type="checkbox" name="id[]" value="'.$fila->id.'">',
                $fila->id,
                $fila->fecha,
                $fila->cliente,
                $fila->tipo_cliente,
                $this->getDetailsCotizacion($fila->id),
                $this->getNumEquipos($fila->id),
                '<button class="btn btn-sm '.(($fila->status==3)?'bg-green-jungle bg-font-green-jungle':'bg-yellow-gold bg-font-yellow-gold').'" id="status">'.(($fila->status==3)?'AGENDADA':'SIN AGENDAR').'</button>',
                '<button id="edit" class="btn btn-sm green btn-outline filter-submit margin-bottom"><i class="fa fa-plus"></i> Editar</button>'
            );
        }

        $response->recordsTotal = count($response->data);
        #response->customActionMessage = "Informacion completada con exito";
        $response->customActionStatus = "OK";

        return json_encode($response);
    }

    private function getDetailsCotizacion($id = 0){
        $span = "<div style='width: 180px;'>";
        if($id > 0){
            $sql = "SELECT (SELECT descripcion FROM cat_tipos_trabajo WHERE id = tipo_trabajo) AS label , 
                (SELECT clase FROM cat_tipos_trabajo WHERE id = tipo_trabajo) AS class
            FROM cotizaciones_detalle
            WHERE id_cotizacion = {$id} 
            GROUP BY cotizaciones_detalle.id";
            $res = $this->conexion->link->query($sql);
            $count = 0;
            $span .= "<div class='row' style='padding: 2px;'> <div class='col-md-3'>";
            while($fila = $res->fetch_assoc()){
                $fila = (object)$fila;
                $fila->label = substr($fila->label , 0 , 3);
                // if($count == 1) 
                $span .= '<span class="label '.$fila->class.'" style="margin:2px">'.wordwrap($fila->label).'</span>';
                // if($count%2 != 0) 
                $count++;
            }
        }
        $span .= "</div>";
        $span .= "</div>";
        $span .= '</div>';
        return $span;
    }

    public function save(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if((int)$data->idOrder>0){
            $mysqli->real_query("UPDATE orden_trabajo SET cliente='$data->cliente',
            direccion='$data->direccion',tiempo_estimado='$data->tiempo',tipo_cliente='$data->tipo_cliente',
            tipo_trabajo='$data->tipo_trabajo',fecha='$data->fecha' WHERE id='$data->idOrder';");
            $ids=$data->idOrder;
            
            $mysqli->real_query("DELETE FROM orden_trabajo_detalle WHERE id_orden='$data->idOrder';");
            foreach($data->areas as $areaa){ 
                $areaa = (array)$areaa;
                $mysqli->real_query("INSERT INTO orden_trabajo_detalle SET id_orden='$ids',orden_trabajo_detalle.`area`='".$areaa['area']."',
                tipo_equipo='".$areaa['tequipo']."',
                des_equipo='".$areaa['desequipo']."',tipo_trabajo='$data->tipo_trabajo'");
            }
            echo $ids;
        }
        else{
            $mysqli->real_query("INSERT INTO orden_trabajo SET cliente='$data->cliente',
            direccion='$data->direccion',tiempo_estimado='$data->tiempo',tipo_cliente='$data->tipo_cliente',
            tipo_trabajo='$data->tipo_trabajo',fecha='$data->fecha';");
            $ids=$mysqli->insert_id;
            // print_r($data);
            foreach($data->areas as $areaa){ 
                $areaa = (array)$areaa;
                $mysqli->real_query("INSERT INTO orden_trabajo_detalle SET id_orden='$ids',orden_trabajo_detalle.`area`='".$areaa['area']."',
                tipo_equipo='".$areaa['tequipo']."',des_equipo='".$areaa['desequipo']."',tipo_trabajo='$data->tipo_trabajo'");
            }
            echo $ids;
        }
    }

    public function saveEvent(){
        $postdata = (object)$_POST;
        if($postdata->idorden >  0){
            $sql="UPDATE orden_trabajo SET fecha = '{$postdata->start}' , tiempo_estimado = '{$postdata->end}' , status = 1 WHERE id='$postdata->idorden'";
            $this->conexion->Consultas(1,$sql);
        }
        echo true;
    }

    public function edit(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        if($postdata->id_orden > 0){
            $sql = "SELECT * FROM orden_trabajo  
                    INNER JOIN cotizaciones 
                    ON orden_trabajo.id_cotizacion = cotizaciones.id 
                    INNER JOIN cat_clientes 
                    ON  cotizaciones.id_cliente = cat_clientes.id
                    WHERE cotizaciones.id ='{$postdata->id_orden}'";
            $res = $this->conexion->link->query($sql);
            $res = $res->fetch_object();
            $response->data = $res;
            $materiales = "SELECT orden_trabajo_materiales.id, orden_trabajo_materiales.seleccion AS item, orden_trabajo_materiales.descripcion
AS descripcion_item, orden_trabajo_materiales.unidad, orden_trabajo_materiales.cantidad_requerida AS cantidad FROM orden_trabajo_materiales  
                    JOIN orden_trabajo 
                    ON orden_trabajo.id = orden_trabajo_materiales.id_orden               
                    JOIN cotizaciones
                    ON  cotizaciones.id = orden_trabajo.id_cotizacion
                    WHERE cotizaciones.id ='{$postdata->id_orden}' and orden_trabajo_materiales.status=1";
            $materiales = $this->conexion->link->query($materiales);
            $response->materiales = [];
			while($fila = $materiales->fetch_assoc()){
            $fila = (object)$fila;
            $response->materiales[] = array ('id'=>$fila->id,
                'item'=>$fila->item,
                'descripcion_item'=>$fila->descripcion_item,
                'unidad'=>$fila->unidad,
                'cantidad'=>$fila->cantidad,
				'edit'=>0
            );		
			}
            $herramientas = "SELECT orden_trabajo_herramientas.* FROM orden_trabajo_herramientas  
                    JOIN orden_trabajo 
                    ON orden_trabajo.id = orden_trabajo_herramientas.id_orden               
                    JOIN cotizaciones
                    ON  cotizaciones.id = orden_trabajo.id_cotizacion
                    WHERE cotizaciones.id ='{$postdata->id_orden}' and orden_trabajo_herramientas.status=1";
			$herramientas = $this->conexion->link->query($herramientas);
			$response->herramientas = [];
			while($fila = $herramientas->fetch_assoc()){
            $fila = (object)$fila;
            $response->herramientas[] = array ('id'=>$fila->id,
                'id_orden'=>$fila->id_orden,
                'herramienta'=>$fila->herramientas,
                'requerimientos'=>$fila->requerimientos,
				'edit'=>0
            );		
			}
			$insclimA = "SELECT ins_caracteristicas_climatizacion_a.*, cat_descripciones_equipos.nombre AS des, cat_areas.nombre AS are FROM ins_caracteristicas_climatizacion_a
JOIN orden_trabajo ON orden_trabajo.id = ins_caracteristicas_climatizacion_a.id_orden 
JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = ins_caracteristicas_climatizacion_a.descripcion
JOIN cat_areas ON cat_areas.id = ins_caracteristicas_climatizacion_a.area
WHERE orden_trabajo.id_cotizacion  = {$postdata->id_orden} AND ins_caracteristicas_climatizacion_a.status=1";
			$insclimA = $this->conexion->link->query($insclimA);
			$response->insclimA = [];
			while($fila = $insclimA->fetch_assoc()){
            $fila = (object)$fila;
            $response->insclimA[] = array ('id'=>$fila->id,
                'area'=>$fila->are,
                'desequipo'=>$fila->des,
                'codigo'=>$fila->codigo,
                'marca'=>$fila->marca,
                'capacidadBTU'=>$fila->capacidadBTU,
                'modelo'=>$fila->modelo,
                'serie'=>$fila->serie,
				'edit'=>0
            );		
			}
			$insclimB = "SELECT ins_caracteristicas_climatizacion_b.*, cat_descripciones_equipos.nombre AS des, cat_areas.nombre AS are FROM ins_caracteristicas_climatizacion_b
JOIN orden_trabajo ON orden_trabajo.id = ins_caracteristicas_climatizacion_b.id_orden 
JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = ins_caracteristicas_climatizacion_b.descripcion
JOIN cat_areas ON cat_areas.id = ins_caracteristicas_climatizacion_b.area
WHERE orden_trabajo.id_cotizacion  = {$postdata->id_orden} AND ins_caracteristicas_climatizacion_b.status=1";

			$insclimB = $this->conexion->link->query($insclimB);
			$response->insclimB = [];
			while($fila = $insclimB->fetch_assoc()){
            $fila = (object)$fila;
            $response->insclimB[] = array ('id'=>$fila->id,
                'area'=>$fila->are,
                'desequipo'=>$fila->des,
                'codigo'=>$fila->codigo,
                'marcaE'=>$fila->marca,
                'capacidadBTUE'=>$fila->capacidadBTU,
                'modeloE'=>$fila->modelo,
                'serieE'=>$fila->serie,
				'marcaC'=>$fila->condensadorMarca,
                'capacidadBTUC'=>$fila->condensadorBTU,
                'modeloC'=>$fila->condensadorModelo,
                'serieC'=>$fila->condensadorSerie,
				'edit'=>0
            );		
			}
			$equiposclimA = "SELECT cat_equipos.id, cat_equipos.nombre_area, cat_descripciones_equipos.nombre FROM cat_equipos JOIN cat_descripciones_equipos ON 
cat_descripciones_equipos.id = cat_equipos.id_descripcion_equipo WHERE cat_equipos.id_tipo_equipo = 1 AND id_cliente = {$res->id_cliente}";
			$equiposclimA = $this->conexion->link->query($equiposclimA);
			$response->equiposclimA = [];
			while($fila = $equiposclimA->fetch_assoc()){
            $fila = (object)$fila;
            $response->equiposclimA[] = array ('id'=>$fila->id,
                'nombre'=>$fila->nombre.' - '.$fila->nombre_area,
            );		
			}
			
			$equiposclimB = "SELECT cat_equipos.id, cat_equipos.nombre_area, cat_descripciones_equipos.nombre FROM cat_equipos JOIN cat_descripciones_equipos ON 
cat_descripciones_equipos.id = cat_equipos.id_descripcion_equipo WHERE cat_equipos.id_tipo_equipo = 2 AND id_cliente = {$res->id_cliente}";
			$equiposclimB = $this->conexion->link->query($equiposclimB);
			$response->equiposclimB = [];
			while($fila = $equiposclimB->fetch_assoc()){
            $fila = (object)$fila;
            $response->equiposclimB[] = array ('id'=>$fila->id,
                'nombre'=>$fila->nombre.' - '.$fila->nombre_area,
            );		
			}
			
			$equiposVentilacion = "SELECT cat_equipos.id, cat_equipos.nombre_area, cat_descripciones_equipos.nombre FROM cat_equipos JOIN cat_descripciones_equipos ON 
cat_descripciones_equipos.id = cat_equipos.id_descripcion_equipo WHERE cat_equipos.id_tipo_equipo = 3 AND id_cliente = {$res->id_cliente}";
			$equiposVentilacion = $this->conexion->link->query($equiposVentilacion);
			$response->equiposVentilacion = [];
			while($fila = $equiposVentilacion->fetch_assoc()){
            $fila = (object)$fila;
            $response->equiposVentilacion[] = array ('id'=>$fila->id,
                'nombre'=>$fila->nombre.' - '.$fila->nombre_area,
            );		
			}
            
            $response->cliente = $this->getClients($res->id_cotizacion);
        }
        return json_encode($response);            
    }
	
	public function addMaterial(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        if($postdata->id_cotizacion > 0){
            $sql = "SELECT orden_trabajo.* FROM orden_trabajo  
                    INNER JOIN cotizaciones 
                    ON orden_trabajo.id_cotizacion = cotizaciones.id 
                    INNER JOIN cat_clientes 
                    ON  cotizaciones.id_cliente = cat_clientes.id
                    WHERE cotizaciones.id ='{$postdata->id_cotizacion}'";
            $res = $this->conexion->link->query($sql);
            $res = $res->fetch_object();
			$sql ="INSERT INTO orden_trabajo_materiales SET id_orden ='{$res->id}', seleccion='{$postdata->item}', descripcion='{$postdata->descripcion_item}',
			unidad='{$postdata->unidad}', cantidad_requerida='{$postdata->cantidad}';";
			//$response->sql = $sql;
			$this->conexion->link->query($sql);
            $materiales = "SELECT orden_trabajo_materiales.id, orden_trabajo_materiales.seleccion AS item, orden_trabajo_materiales.descripcion
AS descripcion_item, orden_trabajo_materiales.unidad, orden_trabajo_materiales.cantidad_requerida AS cantidad FROM orden_trabajo_materiales  
                    JOIN orden_trabajo 
                    ON orden_trabajo.id = orden_trabajo_materiales.id_orden               
                    JOIN cotizaciones
                    ON  cotizaciones.id = orden_trabajo.id_cotizacion
                    WHERE cotizaciones.id ='{$postdata->id_cotizacion}' and orden_trabajo_materiales.status=1";
            $materiales = $this->conexion->link->query($materiales);
            $response->materiales = [];
			while($fila = $materiales->fetch_assoc()){
            $fila = (object)$fila;
            $response->materiales[] = array ('id'=>$fila->id,
                'item'=>$fila->item,
                'descripcion_item'=>$fila->descripcion_item,
                'unidad'=>$fila->unidad,
                'cantidad'=>$fila->cantidad,
				'edit'=>0
            );		
        }
            
        }
        return json_encode($response);            
    }
	
	public function addinsclimA(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        if($postdata->id_cotizacion > 0){
			$sql = "SELECT orden_trabajo.* FROM orden_trabajo  
                    INNER JOIN cotizaciones 
                    ON orden_trabajo.id_cotizacion = cotizaciones.id 
                    INNER JOIN cat_clientes 
                    ON  cotizaciones.id_cliente = cat_clientes.id
                    WHERE cotizaciones.id ='{$postdata->id_cotizacion}'";
            $res2 = $this->conexion->link->query($sql);
            $res2 = $res2->fetch_object();
            $sql = "SELECT * FROM cat_equipos WHERE id ='{$postdata->equipo}'";
            $res = $this->conexion->link->query($sql);
            $res = $res->fetch_object();
			$datos = json_decode($res->data_piezas);
			if($datos[0]->parte=='EVAPORADOR')
				$datos=$datos[0];
			else
				$datos=$datos[1];
			$sql = "INSERT INTO ins_caracteristicas_climatizacion_a SET id_orden = {$res2->id}, descripcion = '{$res->id_descripcion_equipo}'
			, codigo = '{$res->codigo}', marca = '{$datos->marca}', capacidadBTU = '{$datos->capacidad}', modelo = '{$datos->modelo}'
			, serie = '{$datos->serie}', area='{$res->id_area_climatiza}'";
            $this->conexion->link->query($sql);
			$insclimA = "SELECT ins_caracteristicas_climatizacion_a.*, cat_descripciones_equipos.nombre AS des, cat_areas.nombre AS are FROM ins_caracteristicas_climatizacion_a
JOIN orden_trabajo ON orden_trabajo.id = ins_caracteristicas_climatizacion_a.id_orden 
JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = ins_caracteristicas_climatizacion_a.descripcion
JOIN cat_areas ON cat_areas.id = ins_caracteristicas_climatizacion_a.area
WHERE orden_trabajo.id_cotizacion  = {$postdata->id_cotizacion} AND ins_caracteristicas_climatizacion_a.status=1";
			$insclimA = $this->conexion->link->query($insclimA);
			$response->insclimA = [];
			while($fila = $insclimA->fetch_assoc()){
            $fila = (object)$fila;
            $response->insclimA[] = array ('id'=>$fila->id,
                'area'=>$fila->are,
                'desequipo'=>$fila->des,
                'codigo'=>$fila->codigo,
                'marca'=>$fila->marca,
                'capacidadBTU'=>$fila->capacidadBTU,
                'modelo'=>$fila->modelo,
                'serie'=>$fila->serie,
				'edit'=>0
            );		
			}
            
        }
        return json_encode($response);            
    }
	
	public function addHerramienta(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        if($postdata->id_cotizacion > 0){
            $sql = "SELECT orden_trabajo.* FROM orden_trabajo  
                    INNER JOIN cotizaciones 
                    ON orden_trabajo.id_cotizacion = cotizaciones.id 
                    INNER JOIN cat_clientes 
                    ON  cotizaciones.id_cliente = cat_clientes.id
                    WHERE cotizaciones.id ='{$postdata->id_cotizacion}'";
            $res = $this->conexion->link->query($sql);
            $res = $res->fetch_object();
			$sql ="INSERT INTO orden_trabajo_herramientas SET id_orden ='{$res->id}', herramientas='{$postdata->herramienta}', requerimientos='{$postdata->requerimientos}';";
			//$response->sql = $sql;
			$this->conexion->link->query($sql);
            $herramientas = "SELECT orden_trabajo_herramientas.* FROM orden_trabajo_herramientas  
                    JOIN orden_trabajo 
                    ON orden_trabajo.id = orden_trabajo_herramientas.id_orden               
                    JOIN cotizaciones
                    ON  cotizaciones.id = orden_trabajo.id_cotizacion
                    WHERE cotizaciones.id ='{$postdata->id_cotizacion}' and orden_trabajo_herramientas.status=1";
			$herramientas = $this->conexion->link->query($herramientas);
			$response->herramientas = [];
			while($fila = $herramientas->fetch_assoc()){
            $fila = (object)$fila;
            $response->herramientas[] = array ('id'=>$fila->id,
                'id_orden'=>$fila->id_orden,
                'herramienta'=>$fila->herramientas,
                'requerimientos'=>$fila->requerimientos,
				'edit'=>0
            );		
			}
            
        }
        return json_encode($response);            
    }
	
	public function addinsclimB(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        if($postdata->id_cotizacion > 0){
			$sql = "SELECT orden_trabajo.* FROM orden_trabajo  
                    INNER JOIN cotizaciones 
                    ON orden_trabajo.id_cotizacion = cotizaciones.id 
                    INNER JOIN cat_clientes 
                    ON  cotizaciones.id_cliente = cat_clientes.id
                    WHERE cotizaciones.id ='{$postdata->id_cotizacion}'";
            $res2 = $this->conexion->link->query($sql);
            $res2 = $res2->fetch_object();
            $sql = "SELECT * FROM cat_equipos WHERE id ='{$postdata->equipo}'";
            $res = $this->conexion->link->query($sql);
            $res = $res->fetch_object();
			$datos = json_decode($res->data_piezas);
			if($datos[0]->parte=='EVAPORADOR'){
				$datosE=$datos[0];
				$datosC=$datos[1];
			}
			else{
				$datosC=$datos[0];
				$datosE=$datos[1];
			}
			$sql = "INSERT INTO ins_caracteristicas_climatizacion_b SET id_orden = {$res2->id}, descripcion = '{$res->id_descripcion_equipo}'
			, codigo = '{$res->codigo}', marca = '{$datosE->marca}', capacidadBTU = '{$datosE->capacidad}', modelo = '{$datosE->modelo}'
			, serie = '{$datosE->serie}', condensadorMarca = '{$datosC->marca}', condensadorBTU = '{$datosC->capacidad}', condensadorModelo = '{$datosC->modelo}'
			, condensadorSerie = '{$datosC->serie}', area='{$res->id_area_climatiza}'";
            $this->conexion->link->query($sql);
			$insclimB = "SELECT ins_caracteristicas_climatizacion_b.*, cat_descripciones_equipos.nombre AS des, cat_areas.nombre AS are FROM ins_caracteristicas_climatizacion_b
JOIN orden_trabajo ON orden_trabajo.id = ins_caracteristicas_climatizacion_b.id_orden 
JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = ins_caracteristicas_climatizacion_b.descripcion
JOIN cat_areas ON cat_areas.id = ins_caracteristicas_climatizacion_b.area
WHERE orden_trabajo.id_cotizacion  = {$postdata->id_cotizacion} AND ins_caracteristicas_climatizacion_b.status=1";
			$insclimB = $this->conexion->link->query($insclimB);
			$response->insclimB = [];
			while($fila = $insclimB->fetch_assoc()){
            $fila = (object)$fila;
            $response->insclimB[] = array ('id'=>$fila->id,
                'area'=>$fila->are,
                'desequipo'=>$fila->des,
                'codigo'=>$fila->codigo,
                'marcaE'=>$fila->marca,
                'capacidadBTUE'=>$fila->capacidadBTU,
                'modeloE'=>$fila->modelo,
                'serieE'=>$fila->serie,
				'marcaC'=>$fila->condensadorMarca,
                'capacidadBTUC'=>$fila->condensadorBTU,
                'modeloC'=>$fila->condensadorModelo,
                'serieC'=>$fila->condensadorSerie,
				'edit'=>0
            );		
			}
            
        }
        return json_encode($response);            
    }
	
	public function editMaterial(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        if($postdata->id_cotizacion > 0){
            $sql ="UPDATE orden_trabajo_materiales SET seleccion='{$postdata->item}', descripcion='{$postdata->descripcion_item}',
			unidad='{$postdata->unidad}', cantidad_requerida='{$postdata->cantidad}' where id = {$postdata->id_material};";
			//$response->sql = $sql;
			$this->conexion->link->query($sql);
            $materiales = "SELECT orden_trabajo_materiales.id, orden_trabajo_materiales.seleccion AS item, orden_trabajo_materiales.descripcion
AS descripcion_item, orden_trabajo_materiales.unidad, orden_trabajo_materiales.cantidad_requerida AS cantidad FROM orden_trabajo_materiales  
                    JOIN orden_trabajo 
                    ON orden_trabajo.id = orden_trabajo_materiales.id_orden               
                    JOIN cotizaciones
                    ON  cotizaciones.id = orden_trabajo.id_cotizacion
                    WHERE cotizaciones.id ='{$postdata->id_cotizacion}' and orden_trabajo_materiales.status=1";
            $materiales = $this->conexion->link->query($materiales);
            $response->materiales = [];
			while($fila = $materiales->fetch_assoc()){
            $fila = (object)$fila;
            $response->materiales[] = array ('id'=>$fila->id,
                'item'=>$fila->item,
                'descripcion_item'=>$fila->descripcion_item,
                'unidad'=>$fila->unidad,
                'cantidad'=>$fila->cantidad,
				'edit'=>0
            );		
        }
            
        }
        return json_encode($response);            
    }
	
	public function editHerramienta(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        if($postdata->id_cotizacion > 0){
            $sql ="UPDATE orden_trabajo_herramientas SET herramientas='{$postdata->herramienta}', requerimientos='{$postdata->requerimientos}' where id = {$postdata->id_herramienta};";
			//$response->sql = $sql;
			$this->conexion->link->query($sql);
            $herramientas = "SELECT orden_trabajo_herramientas.* FROM orden_trabajo_herramientas  
                    JOIN orden_trabajo 
                    ON orden_trabajo.id = orden_trabajo_herramientas.id_orden               
                    JOIN cotizaciones
                    ON  cotizaciones.id = orden_trabajo.id_cotizacion
                    WHERE cotizaciones.id ='{$postdata->id_cotizacion}' and orden_trabajo_herramientas.status=1";
			$herramientas = $this->conexion->link->query($herramientas);
			$response->herramientas = [];
			while($fila = $herramientas->fetch_assoc()){
            $fila = (object)$fila;
            $response->herramientas[] = array ('id'=>$fila->id,
                'id_orden'=>$fila->id_orden,
                'herramienta'=>$fila->herramientas,
                'requerimientos'=>$fila->requerimientos,
				'edit'=>0
            );		
			}
            
            
        }
        return json_encode($response);            
    }
	
	public function deleteMaterial(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        if($postdata->id_material > 0){
			$sql ="UPDATE orden_trabajo_materiales SET status ='{0}' where id='{$postdata->id_material}'";
			//$response->sql = $sql;
			$this->conexion->link->query($sql);
            $materiales = "SELECT orden_trabajo_materiales.id, orden_trabajo_materiales.seleccion AS item, orden_trabajo_materiales.descripcion
AS descripcion_item, orden_trabajo_materiales.unidad, orden_trabajo_materiales.cantidad_requerida AS cantidad FROM orden_trabajo_materiales  
                    JOIN orden_trabajo 
                    ON orden_trabajo.id = orden_trabajo_materiales.id_orden               
                    JOIN cotizaciones
                    ON  cotizaciones.id = orden_trabajo.id_cotizacion
                    WHERE cotizaciones.id ='{$postdata->id_cotizacion}' and orden_trabajo_materiales.status=1";
            $materiales = $this->conexion->link->query($materiales);
            $response->materiales = [];
			while($fila = $materiales->fetch_assoc()){
            $fila = (object)$fila;
            $response->materiales[] = array ('id'=>$fila->id,
                'item'=>$fila->item,
                'descripcion_item'=>$fila->descripcion_item,
                'unidad'=>$fila->unidad,
                'cantidad'=>$fila->cantidad,
                'edit'=>0
            );		
        }
            
        }
        return json_encode($response);            
    }
	
	public function deleteinsclimA(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        if($postdata->id > 0){
			$sql ="UPDATE ins_caracteristicas_climatizacion_a SET status ='0' where id='{$postdata->id}'";
			//$response->sql = $sql;
			$this->conexion->link->query($sql);
            $insclimA = "SELECT ins_caracteristicas_climatizacion_a.*, cat_descripciones_equipos.nombre AS des, cat_areas.nombre AS are FROM ins_caracteristicas_climatizacion_a
JOIN orden_trabajo ON orden_trabajo.id = ins_caracteristicas_climatizacion_a.id_orden 
JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = ins_caracteristicas_climatizacion_a.descripcion
JOIN cat_areas ON cat_areas.id = ins_caracteristicas_climatizacion_a.area
WHERE orden_trabajo.id_cotizacion  = {$postdata->id_cotizacion} AND ins_caracteristicas_climatizacion_a.status=1";
			$insclimA = $this->conexion->link->query($insclimA);
			$response->insclimA = [];
			while($fila = $insclimA->fetch_assoc()){
            $fila = (object)$fila;
            $response->insclimA[] = array ('id'=>$fila->id,
                'area'=>$fila->are,
                'desequipo'=>$fila->des,
                'codigo'=>$fila->codigo,
                'marca'=>$fila->marca,
                'capacidadBTU'=>$fila->capacidadBTU,
                'modelo'=>$fila->modelo,
                'serie'=>$fila->serie,
				'edit'=>0
            );		
			}
            
        }
        return json_encode($response);            
    }
	
	public function deleteinsclimB(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        if($postdata->id > 0){
			$sql ="UPDATE ins_caracteristicas_climatizacion_b SET status ='0' where id='{$postdata->id}'";
			//$response->sql = $sql;
			$this->conexion->link->query($sql);
            $insclimB = "SELECT ins_caracteristicas_climatizacion_b.*, cat_descripciones_equipos.nombre AS des, cat_areas.nombre AS are FROM ins_caracteristicas_climatizacion_b
JOIN orden_trabajo ON orden_trabajo.id = ins_caracteristicas_climatizacion_b.id_orden 
JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = ins_caracteristicas_climatizacion_b.descripcion
JOIN cat_areas ON cat_areas.id = ins_caracteristicas_climatizacion_b.area
WHERE orden_trabajo.id_cotizacion  = {$postdata->id_cotizacion} AND ins_caracteristicas_climatizacion_b.status=1";
			$insclimB = $this->conexion->link->query($insclimB);
			$response->insclimB = [];
			while($fila = $insclimB->fetch_assoc()){
            $fila = (object)$fila;
            $response->insclimB[] = array ('id'=>$fila->id,
                'area'=>$fila->are,
                'desequipo'=>$fila->des,
                'codigo'=>$fila->codigo,
                'marcaE'=>$fila->marca,
                'capacidadBTUE'=>$fila->capacidadBTU,
                'modeloE'=>$fila->modelo,
                'serieE'=>$fila->serie,
				'marcaC'=>$fila->condensadorMarca,
                'capacidadBTUC'=>$fila->condensadorBTU,
                'modeloC'=>$fila->condensadorModelo,
                'serieC'=>$fila->condensadorSerie,
				'edit'=>0
            );		
			}
            
        }
        return json_encode($response);            
    }
	
	public function deleteHerramienta(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        if($postdata->id_herramienta > 0){
			$sql ="UPDATE orden_trabajo_herramientas SET status ='0' where id='{$postdata->id_herramienta}'";
			//$response->sql = $sql;
			$this->conexion->link->query($sql);
            $herramientas = "SELECT orden_trabajo_herramientas.* FROM orden_trabajo_herramientas  
                    JOIN orden_trabajo 
                    ON orden_trabajo.id = orden_trabajo_herramientas.id_orden               
                    JOIN cotizaciones
                    ON  cotizaciones.id = orden_trabajo.id_cotizacion
                    WHERE cotizaciones.id ='{$postdata->id_cotizacion}' and orden_trabajo_herramientas.status=1";
			$herramientas = $this->conexion->link->query($herramientas);
			$response->herramientas = [];
			while($fila = $herramientas->fetch_assoc()){
            $fila = (object)$fila;
            $response->herramientas[] = array ('id'=>$fila->id,
                'id_orden'=>$fila->id_orden,
                'herramienta'=>$fila->herramientas,
                'requerimientos'=>$fila->requerimientos,
				'edit'=>0
            );		
			}
        }
        return json_encode($response);            
    }

    private function getClients($id_client){
        $id_client = (int)$id_client;
        $response = [];
        if($id_client > 0){
            $sql = "SELECT cat_clientes.* FROM cat_clientes 
            INNER JOIN cotizaciones ON cat_clientes.id = cotizaciones.id_cliente
            WHERE cotizaciones.id = {$id_client}";
            $res = $this->conexion->link->query($sql);
            $res = $res->fetch_object();
            $response = $res;
        }

        return $response;
    }


    public function ChangeStatus(){
        $datos = (object)$_POST;
        
        $sql="UPDATE orden_trabajo SET status=IF(status , 0 , 1) WHERE id='$datos->idorden'";
        $this->conexion->Consultas(1,$sql);
        echo true;
    }

    public function GetEvents(){
        $sql = "SELECT orden_trabajo.id, (SELECT nombre FROM cat_clientes WHERE id = cliente) AS title, 
                (SELECT GROUP_CONCAT(DISTINCT CONCAT_WS( ' : ',cat_tipos_trabajo.descripcion,clase)  ORDER BY cat_tipos_trabajo.id DESC SEPARATOR ' , ') 
                FROM cotizaciones_detalle , cat_tipos_trabajo
                WHERE tipo_trabajo = cat_tipos_trabajo.id AND id_cotizacion = orden_trabajo.id_cotizacion) as clase , fecha AS start, tiempo_estimado AS end, 
                observaciones AS description, (SELECT direccion FROM cat_clientes WHERE id = cliente) AS direccion, cliente, tipo_cliente, status FROM orden_trabajo 
                LEFT JOIN cat_tipos_trabajo ON tipo_trabajo = cat_tipos_trabajo.id 
                WHERE status = 1
                GROUP BY orden_trabajo.id";
        $res = $this->conexion->link->query($sql);
        while($fila = $res->fetch_assoc()){
            $data[] = (object)$fila;
        }
        #$data = $this->conexion->Consultas(2, $sql);
        echo json_encode($data);
    }

    public function GetEventsSinFecha(){
        $sql = "SELECT orden_trabajo.id, (SELECT nombre FROM cat_clientes WHERE id = cliente) AS title, 
                (SELECT GROUP_CONCAT(DISTINCT CONCAT_WS( ' : ',cat_tipos_trabajo.descripcion,clase)  ORDER BY cat_tipos_trabajo.id DESC SEPARATOR ' , ') 
                FROM cotizaciones_detalle , cat_tipos_trabajo
                WHERE tipo_trabajo = cat_tipos_trabajo.id AND id_cotizacion = orden_trabajo.id_cotizacion) as clase , fecha AS start, tiempo_estimado AS end, 
                observaciones AS description, (SELECT direccion FROM cat_clientes WHERE id = cliente) AS direccion, cliente, tipo_cliente, status FROM orden_trabajo 
                LEFT JOIN cat_tipos_trabajo ON tipo_trabajo = cat_tipos_trabajo.id 
                WHERE status = 0
                GROUP BY orden_trabajo.id";
        $res = $this->conexion->link->query($sql);
        $details = "";
        $detalle = "";
        $detalle2 = "";
        while($fila = $res->fetch_assoc()){
            $fila = (object)$fila;
            if($fila->clase != ""){
                $detalle2 = [];
                $detalle = explode(",", $fila->clase);
                if(count($detalle) > 0){
                    foreach ($detalle as $value) {
                        $detalle2[] = explode(":", $value);
                    }
                }else{
                    $detalle2 = explode(":", $detalle);
                }
                $details = $detalle2;
            }
            $data[] = [
                "id" => $fila->id,
                "title" => $fila->title,
                "details" => $details,
                "start" => $fila->start,
                "end" => $fila->end,
                "description" => $fila->description,
                "direccion" => $fila->direccion,
            ];
        }
        #$data = $this->conexion->Consultas(2, $sql);
        echo json_encode($data);
    }

    public function getNumEquipos($id_orden){
        $sql = "SELECT COUNT(id) AS num_equipos FROM cotizaciones_detalle WHERE id_cotizacion = $id_orden";
        $res = $this->conexion->Consultas(2, $sql);
        return $res[0]["num_equipos"];
    }
}

?>