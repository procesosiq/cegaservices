<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/
    
    
    
    /*=====  End of Insertar aqui validacion  de usuarios  ======*/
?>
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <ul class="page-sidebar-menu page-sidebar-menu-hover-submenu page-sidebar-menu-closed" data-keep-expanded="true" data-auto-scroll="true" data-slide-speed="200">
                        <?php if($session->tipo_usuario == 1): ?>
                            <li class="nav-item <?php if($page == 'actualizarOrigenes'){ ?> active <? } ?>">
                                <a href="/actualizarOrigenes" class="nav-link nav-toggle">
                                    <i class="icon-layers"></i>
                                    <span class="title">Actualizar Origenes</span>
                                </a>
                            </li>
                            <li class="nav-item <?php if($page == 'trabajosEjecutados'){ ?> active <? } ?>">
                                <a href="/trabajosEjecutados" class="nav-link nav-toggle">
                                    <i class="icon-layers"></i>
                                    <span class="title">Trabajos Ejecutados (Prontoforms)</span>
                                </a>
                            </li>
                            <li class="nav-item <?php if($page == 'clienteList'){ ?> active <? } ?>">
                                <a href="/clienteList" class="nav-link nav-toggle">
                                    <i class="icon-layers"></i>
                                    <span class="title">Clientes</span>
                                    <!--span class="arrow"></span-->
                                </a>
                            </li>
                            <li class="nav-item <?php if($page == 'controlRequerimientos'){ ?> active <? } ?>">
                                <a href="/controlRequerimientos" class="nav-link nav-toggle">
                                    <i class="icon-layers"></i>
                                    <span class="title">Control de requerimientos</span>
                                    <!--span class="arrow"></span-->
                                </a>
                            </li>
                            <?php if(1==2): ?>
                            <li class="nav-item <?php if($page == 'herramientas'){ ?> active <? } ?>">
                                <a href="/herramientasList" class="nav-link nav-toggle">
                                    <i class="icon-briefcase"></i>
                                    <span class="title">Herramientas de trabajo</span>
                                    <!--span class="arrow"></span-->
                                </a>
                            </li>
                            <?php endif; ?>
                            <?php if($session->id == 11): ?>
                            <li class="nav-item <?php if($page == 'cotizacionPruebas'){ ?> active <? } ?>">
                                <a href="/cotizacionPruebas" class="nav-link nav-toggle">
                                    <i class="icon-briefcase"></i>
                                    <span class="title">Cotizacion pruebas</span>
                                    <!--span class="arrow"></span-->
                                </a>
                            </li>
                            <?php endif; ?>
                            <li class="nav-item <?php if($page == 'cotizacionList'){ ?> active <? } ?>">
                                <a href="/cotizacionList" class="nav-link nav-toggle">
                                    <i class="icon-wallet"></i>
                                    <span class="title">Cotizaciones</span>
                                    <!--span class="arrow"></span-->
                                </a>
                            </li>
                            <li class="nav-item <?php if($page == 'ordenesTrabajoNew'){ ?> active <? } ?>">
                                <a href="/ordenesTrabajoNew" class="nav-link nav-toggle">
                                    <i class="icon-briefcase"></i>
                                    <span class="title">Orden de Trabajo</span>
                                    <!--span class="arrow"></span-->
                                </a>
                            </li>
                            <li class="nav-item <?php if($page == 'programacion'){ ?> active <? } ?>">
                                <a href="/programacion" class="nav-link nav-toggle">
                                    <i class="icon-briefcase"></i>
                                    <span class="title">Programación</span>
                                </a>
                            </li>
                            <li class="nav-item <?php if($page == 'inventario'){ ?> active <? } ?>">
                                <a href="/inventario" class="nav-link nav-toggle">
                                    <i class="icon-wallet"></i>
                                    <span class="title">Inventario</span>
                                    <!--span class="arrow"></span-->
                                </a>
                            </li>
                            <li class="nav-item <?php if($page == 'reportes1' || $page == 'historialReportes'){ ?> active start <? } ?>">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-puzzle"></i>
                                    <span class="title">Reportes</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item ">
                                        <a href="/historialReportes" class="nav-link nav-toggle"><span>Historial de Reportes</span></a>
                                    </li>
                                    <li class="nav-item ">
                                        <a href="/reportes1" class="nav-link nav-toggle"><span>Reporte General</span></a>
                                    </li>
                                </ul>
                            </li>
                            <?php if($session->tipo_usuario_nombre == 'ADMINISTRADOR'): ?>
                                <li class="nav-item <?php 
                                    switch($page) { 
                                        case 'configDescripciones':
                                        case 'configMarcas':
                                        case 'configRefrigerantes':
                                        case 'configMotores':
                                        case 'configSupervisores':
                                        case 'configAreasClimatizacion':
                                            echo 'active';
                                            break;
                                    } 

                                ?>">
                                    <a href="javascript:;" class="nav-link nav-toggle">
                                        <i class="icon-settings"></i>
                                        <span class="title">Configuración</span>
                                        <span class="arrow"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="nav-item ">
                                            <a href="javascript:;" class="nav-link nav-toggle"><span>Equipos</span></a>
                                            <ul class="sub-menu">
                                            <li class="nav-item">
                                                    <a href="configAreasClimatizacion" class="nav-link nav-toggle"><span>Áreas de Climatización</span></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="configDescripciones" class="nav-link nav-toggle"><span>Descripciones</span></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="configMarcas" class="nav-link nav-toggle"><span>Marcas</span></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="javascript:;" class="nav-link nav-toggle"><span>Partes de Equipo</span></a>
                                                    <ul class="sub-menu">
                                                        <li class="nav-item">
                                                            <a href="configPiezas" class="nav-link nav-toggle"><span>Piezas</span></a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a href="configRefrigerantes" class="nav-link nav-toggle"><span>Refrigerantes</span></a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a href="configRepuestos" class="nav-link nav-toggle"><span>Repuestos</span></a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a href="configMotores" class="nav-link nav-toggle"><span>Motores</span></a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="nav-item">
                                            <a href="javascript:;" class="nav-link nav-toggle">Personal</a>
                                            <ul class="sub-menu">
                                                <li class="nav-item">
                                                    <a href="configAuxiliares" class="nav-link nav-toggle"><span>Auxiliares</span></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="configSupervisores" class="nav-link nav-toggle"><span>Tecnicos</span></a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="nav-item">
                                            <a href="configVehiculos" class="nav-link nav-toggle">Vehiculos</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="configUsuarios" class="nav-link nav-toggle">Usuarios</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="configZonas" class="nav-link nav-toggle">Zonas</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="configEstadoReq" class="nav-link nav-toggle">Estados control req.</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="configTiposTrabajos" class="nav-link nav-toggle">Tipos trabajos</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="javascript:;" class="nav-link nav-toggle">Otros</a>
                                            <ul class="sub-menu">
                                                <li class="nav-item">
                                                    <a href="configHerramientas" class="nav-link nav-toggle"><span>Herramientas</span></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="configMateriales" class="nav-link nav-toggle"><span>Materiales</span></a>
                                                </li>
                                            </ul>
                                        </li>
                                    <?php endif; ?>
                                </ul>
                            </li>
                        <?php endif; ?>

                        <?php if($session->tipo_usuario == 5): ?>
                            <li class="nav-item <?php if($page == 'readCalendar'){ ?> active <? } ?>">
                                <a href="/readCalendar" class="nav-link nav-toggle">
                                    <i class="icon-briefcase"></i>
                                    <span class="title">Calendario</span>
                                    <!--span class="arrow"></span-->
                                </a>
                            </li>
                            <li class="nav-item <?php if($page == 'reportes1' || $page == 'historialReportes'){ ?> active start <? } ?>">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-puzzle"></i>
                                    <span class="title">Reportes</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item ">
                                        <a href="/historialReportes" class="nav-link nav-toggle"><span>Historial de Reportes</span></a>
                                    </li>
                                    <li class="nav-item ">
                                        <a href="/reportes1" class="nav-link nav-toggle"><span>Reporte General</span></a>
                                    </li>
                                </ul>
                            </li>
                        <?php endif; ?>

                        <?php if($session->tipo_usuario == 3): ?>
                            <li class="nav-item <?php if($page == 'cotizacionList'){ ?> active <? } ?>">
                                <a href="/cotizacionList" class="nav-link nav-toggle">
                                    <i class="icon-wallet"></i>
                                    <span class="title">Cotizaciones</span>
                                    <!--span class="arrow"></span-->
                                </a>
                            </li>
                        <?php endif; ?>

                    </ul>
                </div>
            </div>