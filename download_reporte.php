<?php
$nameFile = 'Reporte_OT-'.$_GET['id'].'.pdf';
require_once("dompdf/dompdf_config.inc.php");
header("Cache-Control: no-cache, must-revalidate"); //no guardar en CACHE 
header("Pragma: no-cache");  
header("Content-type:application/pdf");
header("Content-Disposition:attachment; filename='{$nameFile}'");
set_time_limit(-1);

//$reporte = file_get_contents(utf8_decode(""));

$reporte = file_get_contents("http://cegaservices2.procesos-iq.com/print_reporte.php?id=".$_GET['id']."&download=true");

$dompdf = new DOMPDF();
$dompdf->load_html(utf8_decode($reporte));

$dompdf->render();
$dompdf->stream($reporte);
$output = $dompdf->output();

file_put_contents("./reporte_pdf_generados/".$nameFile, $output);

readfile("reporte_pdf_generados/{$nameFile}");