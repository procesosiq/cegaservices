$(function(){
    
            $('.date-picker').datepicker({
            rtl: App.isRTL(),
            autoclose: true
        });
    $("#btnadd").click(function(){
        
        if($("#txtnom").val()==''){
            alert("Favor de ingresar un nombre de la sucursal");
            return false;
        }
        else if($("#txtemail").val()==''){
            alert("Favor de ingresar un email de la sucursal");
            return false;
        }
        else if($("#txtfec").val()==''){
            alert("Favor de ingresar fecha de registro");
            return false;
        }
        else if($("#txtrazon").val()==''){
            alert("Favor de ingresar la razón social");
            return false;
        }
        else if($("#txtruc").val()==''){
            alert("Favor de ingresar el RUC");
            return false;
        }
        else if($("#txtdircli").val()==''){
            alert("Favor de ingresar una dirección");
            return false;
        }
        else if($("#txttel").val()==''){
            alert("Favor de ingresar un teléfono");
            return false;
        }
        else if($("#txtciudad").val()==''){
            alert("Favor de ingresar una ciudad");
            return false;
        }
        else{ 
            $.ajax({
				type: "POST",
				url: "controllers/index.php",
				data: "accion=sucursales.AddSucursal&txtnom="+$("#txtnom").val()+"&txtemail="+$("#txtemail").val()+"&s_tipocli="+$("#s_tipocli").val()+
                "&txtfec="+$("#txtfec").val()+"&txtrazon="+$("#txtrazon").val()+
                "&txtruc="+$("#txtruc").val()+"&txtdircli="+$("#txtdircli").val()+"&txttel="+$("#txttel").val()+"&txtciudad="+$("#txtciudad").val()+"&idcli="+$("#idcli").val(),
				success: function(msg){
                    alert("Sucursal registrado con el ID "+ msg);
                    alert("Sucursal registrado con el ID "+ msg , "CLIENTES" , 'success' , function(){
                        if(msg > 0){
                            document.location.href='/sucursales?id='+msg+'&idc=2';
                        }
                    });
                    $('input[type=text]').each(function() {
                        $(this).val('');
                    });
				}
			});
        }
        return false;
	});
    
    $("#btnupd").click(function(){
        
        if($("#txtnom").val()==''){
            alert("Favor de ingresar un nombre de cliente");
            return false;
        }
        else if($("#txtemail").val()==''){
            alert("Favor de ingresar un email del cliente");
            return false;
        }
        else if($("#txtfec").val()==''){
            alert("Favor de ingresar fecha de registro");
            return false;
        }
        else if($("#txtrazon").val()==''){
            alert("Favor de ingresar la razón social");
            return false;
        }
        else if($("#txtruc").val()==''){
            alert("Favor de ingresar el RUC");
            return false;
        }
        else if($("#txtdircli").val()==''){
            alert("Favor de ingresar una dirección");
            return false;
        }
        else if($("#txttel").val()==''){
            alert("Favor de ingresar un teléfono");
            return false;
        }
        else if($("#txtciudad").val()==''){
            alert("Favor de ingresar una ciudad");
            return false;
        }
        else{ 
            $.ajax({
				type: "POST",
				url: "controllers/index.php",
				data: "accion=sucursales.UpdateSucursales&txtnom="+$("#txtnom").val()+"&txtemail="+$("#txtemail").val()+"&s_tipocli="+$("#s_tipocli").val()+
                "&txtfec="+$("#txtfec").val()+"&txtrazon="+$("#txtrazon").val()+
                "&txtruc="+$("#txtruc").val()+"&txtdircli="+$("#txtdircli").val()+"&txttel="+$("#txttel").val()+"&txtciudad="+$("#txtciudad").val()+"&idsuc="+$("#idsuc").val()+"&idcli="+$("#idcli").val(),
				success: function(msg){ 
                    if(msg){
                        alert("Sucursal modificada con éxito");
                        document.location.href='/sucursalesList?idc='+$("#idcli").val();
                    }
                    else{
                        alert("Error encontrado, favor de intentar más tarde");
                    }
				}
			});
        }
        return false;
	});

    $("#btnCancelar").click(function(){
        window.location.assign("sucursalesList?idc=<? echo $_GET['idc']?>");
    });

     $("#btnAddcontact").on("click" , function(){
        addRow()
    });

     $("#datos_princial").on("click" , function(){
        var self = this;
        if($(this).is(':checked')){
            ahttp.post("./controllers/index.php?accion=sucursales.GetClienteDatos",function(r,b){
                b();
                if(r){
                    if(confirm("Esta seguro de cargar la informacion del Cliente?")){
                        $("#txtnom").val(r.nombre);
                        $("#txtrazon").val(r.razon_social);
                        $("#txtruc").val(r.ruc);
                        $("#txtdircli").val(r.direccion);
                        $("#txttel").val(r.telefono);
                        $("#s_tipocli").val(r.id_tipcli);
                        // $("#datos_princial").css('display', 'none');
                    }else{
                        // $("#datos_princial").prop("checked" , false);
                    }
                }

            } , { id: "<? echo $_GET['idc']?>"});
        }
     })

    function addRow(){
        var data = {
            id : "<? echo $_GET['id']?>",
            nombre : $("#txtnom_contacto").val(),
            cargo : $("#txtCargo").val(),
            telefono : $("#txttel_contacto").val(),
            correo : $("#txtemail").val(),
            area : $("#s_areas").val()
        }

        if(data.lote != ""){
            console.log(data);
            ahttp.post("./controllers/index.php?accion=sucursales.addContact",printData , data);
        }
    }

    $("#rowsFilas").on("click" ,".removeRow", function(){
        var id = this.id;
        removeRow(id)
    });

    function removeRow(id){
        console.log(id);
        if(confirm("Esta seguro de eliminar el contacto?")){
            if(id && id > 0){
                var data = {
                    id : "<? echo $_GET['id']?>",
                    id_contacto : id
                }
              ahttp.post("./controllers/index.php?accion=sucursales.removeContact",printData , data);  
            }
        }
    }

    function getData(){
        var data = {
            id : "<? echo $_GET['id']?>"
        }

        ahttp.post("./controllers/index.php?accion=sucursales.Contactos",printData , data);
    }

    function printData(r , b){
        b();
        if(r){
            var inHTML = [];

            $.each(r, function(index, value){
                inHTML.push("<tr>");
                    inHTML.push("<td>");
                    inHTML.push(value.id);
                    inHTML.push("</td>");
                    inHTML.push("<td>");
                    inHTML.push(value.nombre);
                    inHTML.push("</td>");
                    inHTML.push("<td>");
                    inHTML.push(value.cargo);
                    inHTML.push("</td>");
                    inHTML.push("<td>");
                    inHTML.push(value.telefono);
                    inHTML.push("</td>");
                    inHTML.push("<td>");
                    inHTML.push(value.correo);
                    inHTML.push("</td>");
                    inHTML.push("<td>");
                    inHTML.push(value.area);
                    inHTML.push("</td>");
                    inHTML.push("<td>");
                    inHTML.push('<button type="button" class="btn btn-sm red-thunderbird removeRow" id="'+value.id+'">Eliminar</button>');
                    inHTML.push("</td>");
                inHTML.push("</tr>"); 
            });

            $("#rowsFilas").html(inHTML.join("")); //add generated tr html to corresponding table
            $("#txtnom_contacto").val('');
            $("#txtCargo").val('');
            $("#txttel_contacto").val('');
            $("#txtemail").val('');
            $("#s_areas").val('');
        }
    }
    getData();
    
});