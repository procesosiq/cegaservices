<?php
header('Content-Type: text/html; charset=utf-8');
ini_set('display_errors',1);
error_reporting(E_ALL);

/** DATABASE */
$connect = new stdClass;
$connect->db = @new mysqli("localhost", "auditoriasbonita", "u[V(fTIUbcVb", "cegaservices");
$connect->db2 = @new mysqli("localhost", "auditoriasbonita", "u[V(fTIUbcVb", "cegaservices2");

#$mysqli = @new mysqli("localhost", "root", "", "auditoriasbonita");

if (mysqli_connect_errno()) {
    printf("Falló la conexión: %s\n", mysqli_connect_error());
    exit();
}
$connect->db->set_charset("utf8");
$connect->db2->set_charset("utf8");

/** Directorio de los JSON */
$path = realpath('./json');

echo "noe=".$path;
$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
foreach($objects as $name => $object){ echo "<br>ruta ".$object->getPath();
    if('.' != $object->getFileName() && '..' != $object->getFileName() && "/home/procesosiq/public_html/cegaservices2/recibe/json/correctivo_"== $object->getPath()){ 
        $pos1 = strpos($object->getPath(), "revision_correctivo_");
		$pos1 = true;
        if($pos1 !== false){ 
            $ext = pathinfo($object->getPathName(), PATHINFO_EXTENSION);
			echo "<br>Extencion=".$ext; 
            if('json' == $ext || 'jpeg' == $ext || 'jpg' == $ext || 'png' == $ext || 'mp3' == $ext || '3gpp' == $ext){
                switch ($ext) { 
                    case 'json':
                        /* READ AND MOVE JSON */
                        $json = json_decode(trim(file_get_contents($object->getPathName())), true);
                        echo '<pre>';
                        //print_r($json);
                        echo '</pre>';
                        process_json($json, $object->getFileName(), $connect);
                        move_json($object->getPathName(), $object->getFileName());
                        break;

                    case 'jpeg':
                    case 'jpg':
                    case 'png':
                        /* MOVE JSON */
                        move_image($object->getPathName(), $object->getFileName());
                        break;

                    case '3gpp':
                    case 'mp3':
                        /* MOVE AUDIO */
                        move_audio($object->getPathName(), $object->getFileName());
                        break;
                    case 'mp4':
                        /* MOVE AUDIO */
                        move_audio($object->getPathName(), $object->getFileName());
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
die("<br>fin del ciclo");
//$mysqli->close();

function move_json($file, $nameFile){
    echo $file;
    echo $nameFile;
    // die();
    if(!rename($file, __DIR__."/../reportes_vistos/json/$nameFile")){
        echo 'error';
    }
}

function move_image($file, $nameFile){
    // die();
    if(!rename($file, __DIR__."/../reportes_vistos/image/$nameFile")){
        echo 'error';
    }
}

function move_audio($file, $nameFile){
    // die();
    if(!rename($file, __DIR__."/../reportes_vistos/audio/$nameFile")){
        echo 'error';
    }
}

function process_json($json, $filename, $connect){
    $identifier = $json['identifier'];
    $version = $json['version']. '';
    $zone = $json['zone']. '';
    $referenceNumber = $json['referenceNumber']. '';
    $state = $json['state']. '';
    $deviceSubmitDate = $json['deviceSubmitDate'];
    $deviceSubmitDateDate = $deviceSubmitDate['time']. '';
    $deviceSubmitDateZone = $deviceSubmitDate['zone']. '';
    $fecha_file = str_replace("-", "", explode("T", $deviceSubmitDateDate)[0]);
    $shiftedDeviceSubmitDate = $json['shiftedDeviceSubmitDate'].'';
    $serverReceiveDate = $json['serverReceiveDate'].'';
	
	
	
    $form = $json['form'];
    $form_identifier = $form['identifier'].'';
    $form_versionIdentifier = $form['versionIdentifier'].'';
    $form_name = $form['name'].'';
    $form_version = $form['version'].'';
    $form_formSpaceIdentifier = $form['formSpaceIdentifier'].'';
    $form_formSpaceName = $form['formSpaceName'].'';

    $user = $json['user'];
    $user_identifier = $user['identifier'].'';
    $user_username = $user['username'].'';
    $user_displayName = $user['displayName'].'';

    $geoStamp = $json['geoStamp'];
    $geoStamp_success = $geoStamp['success'].'';
    $geoStamp_captureTimestamp = $geoStamp['captureTimestamp'];
    $geoStamp_captureTimestamp_provided = $geoStamp_captureTimestamp['provided'];
    $geoStamp_captureTimestamp_provided_time = $geoStamp_captureTimestamp_provided['time'].'';;
    $geoStamp_captureTimestamp_provided_zone = $geoStamp_captureTimestamp_provided['zone'].'';;
    $geoStamp_captureTimestamp_shifted = $geoStamp_captureTimestamp['shifted'].'';;
    $geoStamp_errorMessage = $geoStamp['errorMessage'].'';;
    $geoStamp_source = $geoStamp['source'].'';;
    $geoStamp_coordinates = $geoStamp['coordinates'];
    $geoStamp_coordinates_latitude = $geoStamp_coordinates['latitude'].'';;
    $geoStamp_coordinates_longitude = $geoStamp_coordinates['longitude'].'';;
    $geoStamp_coordinates_altitude = $geoStamp_coordinates['altitude'].'';;
    $geoStamp_address = $geoStamp['address'].'';;

    $pages = $json['pages'];

    // revision correctivo
   
 /*Ingo Gral*/
 $infgen_cliente="";
 $infgen_id_cliente="";
 $infgen_id_sucursal="";
 $infgen_tipoCliente="";
 $infgen_direccion="";
 $infgen_tipoTrabajo="";
 $infgen_referenciaLugar="";
 $infgen_ruta="";

 /*Info equipo*/
 $infequ_tipo="";
 $oinfequ_id_tipoEquipo= "";
 $infequ_area="";
 $infequ_id_areaClimatiza= "";
 $infequ_nombreArea="";

 /*Carac equipo cli a*/
 $climaA_descripcion="";
 $climaA_codigo="";
 $climaA_marca="";
 $climaA_btu="";
 $climaA_modelo="";
 $climaA_serie="";
 $climaAD_reparacion="";
 $climaAD_cambio="";
 $climaAD_observaciones="";
 $climaAD_comentarios="";
 $climaAD_capturaFotos="";
 $climaAD_tiempoTrabajo="";

 /*Diagnostico cli A*/
 $climaAD_reparacion="";
 $climaAD_cambio="";
 $climaAD_observaciones="";
 $climaAD_comentarios="";
 $climaAD_capturaFotos="";
 $climaAD_tiempoTrabajo="";

 /*Carac equipo cli b*/
$climaB_descripcion="";
$climaB_codigo="";
$climaB_evaporador="";
$climaB_marca="";
$climaB_btu="";
$climaB_modelo="";
$climaB_serie="";
$climaB_condensador="";
$climaB_condensadorMarca="";
$climaB_condensadorBtu="";
$climaB_condensadorModelo="";
$climaB_condensadorSerie="";

/*Diagnostico cli B*/
$climaBD_reparacion="";
$climaBD_cambio="";
$climaBD_condensaRep="";
$climaBD_condensaCambio="";
$climaBD_observaciones="";
$climaBD_comentarios="";
$climaBD_fotos="";
$climaBD_tiempoTrabajo="";

/*caracteristicas ventilacion*/
$ventila_descripcion="";
$ventila_codigo="";
$ventila_marca="";
$ventila_hp="";
$ventila_cfn="";
$ventila_modelo="";
$ventila_serie="";

/*Diagnostico Ventilacion*/
$ventilaD_reparacion="";
$ventilaD_cambio="";
$ventilaD_observaciones="";
$ventilaD_comentarios="";
$ventilaD_fotos="";
$ventilaD_tiempoTrabajo="";

/*caracteristicas refrigeracion*/
$ref_descripcion="";
$ref_codigo="";
$ref_marca="";
$ref_modelo="";
$ref_serie="";
$ref_compresorBtu="";
$ref_compresorCantidad="";
$ref_ventiladorHp="";
$ref_ventiladorCantidad="";

/*Evaporadores*/
$evapora1_marca="";
$evapora1_modelo="";
$evapora1_serie="";
$evapora1_hp="";
$evapora1_cantidad="";

$evapora2_marca="";
$evapora2_modelo="";
$evapora2_serie="";
$evapora2_hp="";
$evapora2_cantidad="";

$evapora3_marca="";
$evapora3_modelo="";
$evapora3_serie="";
$evapora3_hp="";
$evapora3_cantidad="";

$evapora4_marca="";
$evapora4_modelo="";
$evapora4_serie="";
$evapora4_hp="";
$evapora4_cantidad="";

$evapora5_marca="";
$evapora5_modelo="";
$evapora5_serie="";
$evapora5_hp="";
$evapora5_cantidad="";

$evapora6_marca="";
$evapora6_modelo="";
$evapora6_serie="";
$evapora6_hp="";
$evapora6_cantidad="";

/*Diagnostico refrigeracion*/
$refD_evaporaRep="";
$refD_evaporaCambio="";
$refD_condesaRep="";
$refD_condensaCambio="";
$refD_observaciones="";
$refD_comentarios="";
$refD_fotos="";
$refD_tiempoTrabajo="";

/*Materiales con items*/
$item1="";
$item1_seleccion="";
$item1_descripcion="";
$item1_unidad="";
$item1_cantidad="";
$item1_observaciones="";

$item2="";
$item2_seleccion="";
$item2_descripcion="";
$item2_unidad="";
$item2_cantidad="";
$item2_observaciones="";

$item3="";
$item3_seleccion="";
$item3_descripcion="";
$item3_unidad="";
$item3_cantidad="";
$item3_observaciones="";

$item4="";
$item4_seleccion="";
$item4_descripcion="";
$item4_unidad="";
$item4_cantidad="";
$item4_observaciones="";

$item5="";
$item5_seleccion="";
$item5_descripcion="";
$item5_unidad="";
$item5_cantidad="";
$item5_observaciones="";

$item6="";
$item6_seleccion="";
$item6_descripcion="";
$item6_unidad="";
$item6_cantidad="";
$item6_observaciones="";

$item7="";
$item7_seleccion="";
$item7_descripcion="";
$item7_unidad="";
$item7_cantidad="";
$item7_observaciones="";

$item8="";
$item8_seleccion="";
$item8_descripcion="";
$item8_unidad="";
$item8_cantidad="";
$item8_observaciones="";

$item9="";
$item9_seleccion="";
$item9_descripcion="";
$item9_unidad="";
$item9_cantidad="";
$item9_observaciones="";

$item10="";
$item10_seleccion="";
$item10_descripcion="";
$item10_unidad="";
$item10_cantidad="";
$item10_observaciones="";

$hertra_requerimientos="";
$obs_nombreResponsable="";
$obs_cargo="";
$obs_comentarios="";
$obs_observaciones="";
$obs_firma="";
$obs_copia="";
	
    // ciclo
    foreach($pages as $key => $page_data){
        $pagina = $key+1;
        $pagina_nombre = $page_data['name'];
        $sql_muestra_causas = array();
        $answers = $page_data["answers"];

        //echo '<br><br><br>'.ltl($pagina_nombre).'<hr>';
        foreach($answers as $answer){
            $label    = $answer['label'];
            $dataType = $answer['dataType'];
            $question = $answer['question'];
            $values   = $answer['values'];
            $labelita = limpiar(trim(strtolower($label)));
		
			
            if("informacion general" == limpiar(trim(strtolower($pagina_nombre))))
            {
                // D(ltl($question));
                // D(($values));
				print_r($values);echo "<br>";
				
                $infgen_cliente = "cliente" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $infgen_cliente;
				echo "<br>cliente=".$infgen_cliente."<br>";
                $infgen_id_cliente = "cliente: - id" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $infgen_id_cliente;
                $infgen_tipoCliente = "tipo de cliente:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $infgen_tipoCliente;
                $infgen_id_sucursal = "sucursal: - id" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $infgen_id_sucursal;
                $infgen_direccion = "direccion:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $infgen_direccion;
                $infgen_tipoTrabajo = "tipo de trabajo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $infgen_tipoTrabajo;
                $infgen_referenciaLugar = "referencia del lugar:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $infgen_referenciaLugar;
                $infgen_ruta = "ruta" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $infgen_ruta;
            }

            if("informacion equipo" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $infequ_tipo = "tipo de equipo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $infequ_tipo;
                $infequ_area = "area que climatiza:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') :  $infequ_area;
                $infequ_nombreArea = "nombre del area:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $infequ_nombreArea;
                $infequ_id_tipoEquipo = ltl($question) == 'tipo de equipo: - id' ? (isset($values[0]) ? $values[0] : '') : $infequ_id_tipoEquipo;
                $infequ_id_areaClimatiza = ltl($question) == 'area que climatiza - id' ? (isset($values[0]) ? $values[0] : '') : $infequ_id_areaClimatiza;
            }

            if("caracteristicas equipo (climatizacion a)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $climaA_descripcion = "descripcion del equipo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaA_descripcion;
                $climaA_codigo = "codigo del equipo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaA_codigo;
                $climaA_marca = "marca" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaA_marca;
                $climaA_btu = "capacidad (btu):" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaA_btu;
                $climaA_modelo = "modelo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaA_modelo;
                $climaA_serie = "serie:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaA_serie;
            }

            if("diagnostico (climatizacion a)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $climaAD_reparacion = "piezas no operativas (reparacion):" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaAD_reparacion;
                $climaAD_cambio = "piezas no operativas (cambio):" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaAD_cambio;
                $climaAD_observaciones = "observaciones:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaAD_observaciones;
                $climaAD_comentarios = "comentarios:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaAD_comentarios;
                $climaAD_capturaFotos = "captura de fotos" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaAD_capturaFotos;
                $climaAD_tiempoTrabajo = "tiempo de trabajo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaAD_tiempoTrabajo;
            }

            if("caracteristicas equipo (climatizacion b)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $climaB_descripcion = "descripcion del equipo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaB_descripcion;
                $climaB_codigo = "codigo del equipo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaB_codigo;
                $climaB_evaporador = "evaporador" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaB_evaporador;
                $climaB_marca = "marca:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaB_marca;
                $climaB_btu = "capacidad (btu):" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaB_btu;
                $climaB_modelo = "modelo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaB_modelo;
                $climaB_serie = "serie:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaB_serie;
                $climaB_condensador = "condensador" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaB_condensador;
                $climaB_condensadorMarca = "marca:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaB_condensadorMarca;
                $climaB_condensadorBtu = "capacidad (btu):" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaB_condensadorBtu;
                $climaB_condensadorModelo = "modelo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaB_condensadorModelo;
                $climaB_condensadorSerie = "serie:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaB_condensadorSerie;
            }

            if("diagnostico (climatizacion b)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $climaBD_reparacion = "pno(ron): 2" == ltl($label) ? (isset($values[0]) ? $values[0] : $climaBD_reparacion) : ''; // Piezas no Operativas (Reparación):
                $climaBD_cambio = "pno(c): 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaBD_cambio; // Piezas no Operativas (Cambio):
                $climaBD_condensaRep = "pno(ron): 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaBD_condensaRep; // Piezas no Operativas (Reparación):
                $climaBD_condensaCambio = "pno(c): 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaBD_condensaCambio; // Piezas no Operativas (Cambio):
                $climaBD_observaciones = "observaciones:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaBD_observaciones;
                $climaBD_comentarios = "comentarios:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaBD_comentarios;
                $climaBD_fotos = "captura de fotos" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaBD_fotos;
                $climaBD_tiempoTrabajo = "tiempo de trabajo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaBD_tiempoTrabajo;
            }

            if("caracteristicas equipo (ventilacion)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $ventila_descripcion = "descripcion del equipo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ventila_descripcion;
                $ventila_codigo = "codigo del equipo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ventila_codigo;
                $ventila_marca = "marca:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ventila_marca;
                $ventila_hp = "capacidad (hp):" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ventila_hp;
                $ventila_cfn = "capacidad (cfn):" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ventila_cfn;
                $ventila_modelo = "modelo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ventila_modelo;
                $ventila_serie = "serie:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ventila_serie;
            }

            if("diagnostico (ventilacion)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $ventilaD_reparacion = "partes no operativas (reparacion):" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ventilaD_reparacion;
                $ventilaD_cambio = "partes no operativas (cambio):" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ventilaD_cambio;
                $ventilaD_observaciones = "observaciones:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ventilaD_observaciones;
                $ventilaD_comentarios = "comentarios:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ventilaD_comentarios;
                $ventilaD_fotos = "captura de fotos" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ventilaD_fotos;
                $ventilaD_tiempoTrabajo = "tiempo de trabajo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ventilaD_tiempoTrabajo;
            }

            if("caracteristicas equipo (refrigeracion)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $ref_descripcion = "descripcion del equipo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ref_descripcion;
                $ref_codigo = "codigo del equipo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ref_codigo;
                $ref_marca = "marca:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ref_marca;
                $ref_modelo = "modelo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ref_modelo;
                $ref_serie = "serie:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ref_serie;
                $ref_compresorBtu = "capacidad compresor (hp/btu):" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ref_compresorBtu;
                $ref_compresorCantidad = "cantidad compresores:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ref_compresorCantidad;
                $ref_ventiladorHp = "capacidad motor ventilador (hp):" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ref_ventiladorHp;
                $ref_ventiladorCantidad = "cantidad motor ventilador:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ref_ventiladorCantidad;
            }

            if("evaporador #1" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $evapora1_marca = "marca:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora1_marca;
                $evapora1_modelo = "modelo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora1_modelo;
                $evapora1_serie = "serie:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora1_serie;
                $evapora1_hp = "capacidad motor ventilador (hp):" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora1_hp;
                $evapora1_cantidad = "cantidad motor ventilador:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora1_cantidad;
            }

            if("evaporador #2" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $evapora2_marca = "marca:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora2_marca;
                $evapora2_modelo = "modelo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora2_modelo;
                $evapora2_serie = "serie:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora2_serie;
                $evapora2_hp = "capacidad motor ventilador (hp):" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora2_hp;
                $evapora2_cantidad = "cantidad motor ventilador:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora2_cantidad;
            }

            if("evaporador #3" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $evapora3_marca = "marca:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora3_marca;
                $evapora3_modelo = "modelo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora3_modelo;
                $evapora3_serie = "serie:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora3_serie;
                $evapora3_hp = "capacidad motor ventilador (hp):" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora3_hp;
                $evapora3_cantidad = "cantidad motor ventilador:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora3_cantidad;
            }

            if("evaporador #4" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $evapora4_marca = "marca:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora4_marca;
                $evapora4_modelo = "modelo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora4_modelo;
                $evapora4_serie = "serie:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora4_serie;
                $evapora4_hp = "capacidad motor ventilador (hp):" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora4_hp;
                $evapora4_cantidad = "cantidad motor ventilador:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora4_cantidad;
            }

            if("evaporador #5" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $evapora5_marca = "marca:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora5_marca;
                $evapora5_modelo = "modelo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora5_modelo;
                $evapora5_serie = "serie:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora5_serie;
                $evapora5_hp = "capacidad motor ventilador (hp):" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora5_hp;
                $evapora5_cantidad = "cantidad motor ventilador:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora5_cantidad;
            }

            if("evaporador #6" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $evapora6_marca = "marca:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora6_marca;
                $evapora6_modelo = "modelo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora6_modelo;
                $evapora6_serie = "serie:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora6_serie;
                $evapora6_hp = "capacidad motor ventilador (hp):" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora6_hp;
                $evapora6_cantidad = "cantidad motor ventilador:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora6_cantidad;
            }

            if("diagnostico (refrigeracion)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $refD_evaporaRep = "pno(ron): 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $refD_evaporaRep; // Partes no Operativas (Reparación):
                $refD_evaporaCambio = "pno(c): 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $refD_evaporaCambio; // Partes no Operativas (Cambio):
                $refD_condesaRep = "pno(ron): 6" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $refD_condesaRep; // Partes no Operativas (Reparación):
                $refD_condensaCambio = "pno(c): 6" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $refD_condensaCambio; // Partes no Operativas (Cambio):
                $refD_observaciones = "observaciones: 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $refD_observaciones; // Observaciones:
                $refD_comentarios = "comentarios: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $refD_comentarios; // Comentarios:
                $refD_fotos = "captura de fotos 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $refD_fotos; // Captura de Fotos
                $refD_tiempoTrabajo = "tdt: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $refD_tiempoTrabajo; // Tiempo de Trabajo:
            }

            if("materiales requeridos" == limpiar(trim(strtolower($pagina_nombre))))
            {
                // item #1
                $item1 = "item #1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item1; // Item #1
                $item1_seleccion = "sondi: 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item1_seleccion; // Selección de Item:
                $item1_descripcion = "descripcion item: 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item1_descripcion; // Descripción Item:
                $item1_unidad = "definir unidad: 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item1_unidad; // Definir Unidad:
                $item1_cantidad = "cr: 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item1_cantidad; // Cantidad Requerida:
                $item1_observaciones = "observaciones: 6" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item1_observaciones; // Observaciones:
            }

            if("item #2" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $item2_seleccion = "sondi:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item2_seleccion; // Selección de Item:
                $item2_descripcion = "descripcion item:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item2_descripcion; // Descripción Item:
                $item2_unidad = "definir unidad:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item2_unidad; // Definir Unidad:
                $item2_cantidad = "cr:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item2_cantidad; // Cantidad Requerida:
                $item2_observaciones = "observaciones:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item2_observaciones; // Observaciones:
            }

            if("item #3" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $item3_seleccion = "sondi: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item3_seleccion; // Selección de Item:
                $item3_descripcion = "descripcion item: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item3_descripcion; // Descripción Item:
                $item3_unidad = "definir unidad: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item3_unidad; // Definir Unidad:
                $item3_cantidad = "cr: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item3_cantidad; // Cantidad Requerida:
                $item3_observaciones = "observaciones: 7" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item3_observaciones; // Observaciones:
            }

            if("item #4" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $item4_seleccion = "sondi: 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item4_seleccion; // Selección de Item:
                $item4_descripcion = "descripcion item: 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item4_descripcion; // Descripción Item:
                $item4_unidad = "definir unidad: 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item4_unidad; // Definir Unidad:
                $item4_cantidad = "cr: 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item4_cantidad; // Cantidad Requerida:
                $item4_observaciones = "observaciones: 8" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item4_observaciones; // Observaciones:
            }

            if("item #5" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $item5_seleccion = "sondi: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item5_seleccion; // Selección de Item:
                $item5_descripcion = "descripcion item: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item5_descripcion; // Descripción Item:
                $item5_unidad = "definir unidad: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item5_unidad; // Definir Unidad:
                $item5_cantidad = "cr: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item5_cantidad; // Cantidad Requerida:
                $item5_observaciones = "observaciones: 9" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item5_observaciones; // Observaciones:
            }

            if("item #6" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $item6_seleccion = "sondi: 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item6_seleccion; // Selección de Item:
                $item6_descripcion = "descripcion item: 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item6_descripcion; // Descripción Item:
                $item6_unidad = "definir unidad: 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item6_unidad; // Definir Unidad:
                $item6_cantidad = "cr: 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item6_cantidad; // Cantidad Requerida:
                $item6_observaciones = "observaciones: 10" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item6_observaciones; // Observaciones:
            }

            if("item #7" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $item7_seleccion = "sondi: 6" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item7_seleccion; // Selección de Item:
                $item7_descripcion = "descripcion item: 6" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item7_descripcion; // Descripción Item:
                $item7_unidad = "definir unidad: 6" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item7_unidad; // Definir Unidad:
                $item7_cantidad = "cr: 6" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item7_cantidad; // Cantidad Requerida:
                $item7_observaciones = "observaciones: 11" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item7_observaciones; // Observaciones:
            }

            if("item #8" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $item8_seleccion = "sondi: 7" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item8_seleccion; // Selección de Item:
                $item8_descripcion = "descripcion item: 7" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item8_descripcion; // Descripción Item:
                $item8_unidad = "definir unidad: 7" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item8_unidad; // Definir Unidad:
                $item8_cantidad = "cr: 7" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item8_cantidad; // Cantidad Requerida:
                $item8_observaciones = "observaciones: 12" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item8_observaciones; // Observaciones:
            }

            if("item #9" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $item9_seleccion = "sondi: 8" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item9_seleccion; // Selección de Item:
                $item9_descripcion = "descripcion item: 8" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item9_descripcion; // Descripción Item:
                $item9_unidad = "definir unidad: 8" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item9_unidad; // Definir Unidad:
                $item9_cantidad = "cr: 8" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item9_cantidad; // Cantidad Requerida:
                $item9_observaciones = "observaciones: 13" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item9_observaciones; // Observaciones:
            }

            if("item #10" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $item10_seleccion = "sondi: 9" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item10_seleccion; // Selección de Item:
                $item10_descripcion = "descripcion item: 9" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item10_descripcion; // Descripción Item:
                $item10_unidad = "definir unidad: 9" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item10_unidad; // Definir Unidad:
                $item10_cantidad = "cr: 9" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item10_cantidad; // Cantidad Requerida:
                $item10_observaciones = "observaciones: 14" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item10_observaciones; // Observaciones:
            }

            if("herramientas de trabajo" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $hertra_herramientas = "hnpet:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $hertra_herramientas; // Herramientas necesarias para el trabajo:
                $hertra_requerimientos = "re:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $hertra_requerimientos; // Requerimientos Especiales:
            }

            if("observaciones generales y firmas" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $obs_nombreResponsable = "ndrqset:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $obs_nombreResponsable; // Nombre del responsable que supervisa el trabajo:
                $obs_cargo = "cargo:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $obs_cargo; // Cargo:
                $obs_comentarios = "cg" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $obs_comentarios; // Comentarios Generales
                $obs_observaciones = "og:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $obs_observaciones; // Observaciones Generales:
                $obs_firma = "fdcor" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $obs_firma; // Firma del Cliente o Responsable
                $obs_copia = "eucdefasc:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $obs_copia; // Enviar una copia de este formulario al siguiente correo:
            }
            
        }
		
    }
	die("fin");
    // MySQL
    $mysqli = $connect->db;
    $id_usuario = 2;
    if(isset($user_username) && $user_username != "" && $user_username == "ernesto.polit"){
        $mysqli = $connect->db2;
        $id_usuario = 2;
    }
	
    /*Cotización*/
    $sqlValid = "SELECT * FROM cotizaciones WHERE fecha_create = CURRENT_DATE AND id_cliente = '{$infgen_id_cliente}'";
	$last_cot = 0;
	$res = $mysqli->query($sqlValid);
    if($res->num_rows > 0){
                    $datas = $res->fetch_assoc();
                    $last_cot = $datas['id'];
    }
	else{
    /*Cotización*/
    $sql = "INSERT INTO cotizaciones SET 
        id_cliente = '".$infgen_id_cliente."', 
        id_sucursal = ' ".$infgen_id_sucursal."' , 
        status = 0, 
		fecha_status = CURRENT_DATE,
        id_usuario = '{$id_usuario}',
        fecha_create = CURRENT_DATE";
		if($mysqli->query($sql)== TRUE){
        $last_cot = $mysqli->insert_id;
		$folio = str_pad($last_cot , 10 ,0, STR_PAD_LEFT);
        $mysqli->query("UPDATE cotizaciones SET folio = '{$folio}' WHERE id = '{$last_cot}'");
        
    } else {
    echo "Error: " . $last_cot . "<br>" . $mysqli->error;
    }
    }
	
    // D($sql);    
    
	$sql_cotizacion = "INSERT INTO cotizaciones_detalle SET id_cotizacion = '".$last_cot."',
         tipo_trabajo = 1 , des_trabajo = 'CORRECTIVO'";
         // D($sql_cotizacion); 
        $mysqli->query($sql_cotizacion);
	
    /*Orden Correctivo
    $sql_correctivo = "INSERT INTO orden_trabajo SET 
    id_cotizacion = '".$last_cot."',
    cliente = '".$infgen_cliente."',
    fecha_create = CURRENT_DATE,
    tipo_cliente = '".$infgen_tipoCliente."',
    direccion = '".$infgen_direccion."',
    tipo_trabajo = '".$infgen_tipoTrabajo."'";

    
    if($mysqli->query($sql_correctivo)== TRUE){
        $last_id = $mysqli->insert_id;
        //echo "Ultimo ID insertado " . $last_id;
    } else {
    echo "Error: " . $sql_correctivo . "<br>" . $mysqli->error;
    }



    /*Caracteristicas Climatizacion A
    $sql_caracteristicasClimA = "INSERT INTO cor_caracteristicas_climatizacion_a SET id_orden = '".$last_id."', descripcion = '".$climaA_descripcion."' ,codigo = '".$climaA_codigo."', marca = '".$climaA_marca."', capacidadBTU = '".$climaA_btu."', modelo = '".$climaA_modelo."', serie = '".$climaA_serie."'";

    if ($mysqli->query($sql_caracteristicasClimA) === TRUE) {
    echo "New record created successfully";
    }

    /*Diagnostico Climatizacion A
    $sql_diagnosticoClimA = "INSERT INTO cor_diagnostico_climatizacion_a SET id_orden = '".$last_id."', reparacion = '".$climaAD_reparacion."' , cambio = '".$climaAD_cambio."', observaciones = '".$climaAD_observaciones."', comentarios = '".$climaAD_comentarios."', capturaFotos = '".$climaAD_capturaFotos."', tiempoTrabajo = '".$climaAD_tiempoTrabajo."'";

    if ($mysqli->query($sql_diagnosticoClimA) === TRUE) {
        echo "New record created successfully";
        }    
    /*CLimatizacion Caracteristicas B
    $sql_caracClimB="INSERT INTO cor_caracteristicas_climatizacion_b SET id_orden = '".$last_id."', descripcion= '".$climaB_descripcion."', codigo = '".$climaB_codigo."', evaporador = '".$climaB_evaporador."', marca = '".$climaB_marca."', capacidadBTU ='".$climaB_btu."', modelo = '".$climaB_modelo."', serie = '".$climaB_serie."', condensador='".$climaB_condensador."',  condensadorMarca = '".$climaB_condensadorMarca."',condensadorBTU = '".$climaB_condensadorBtu."', condensadorModelo = '".$climaB_condensadorModelo."', condensadorSerie = '".$climaB_condensadorSerie."'";
    
    if ($mysqli->query($sql_caracClimB) === TRUE) {
    echo "New record created successfully";
    } 
    /*Diagnostico Climatizacion B
    $sql_diagnClimB="INSERT INTO cor_diagnostico_climatizacion_b SET id_orden = '".$last_id."', reparacion= '".$climaBD_reparacion."', cambio = '".$climaBD_cambio."', condensaRep = '".$climaBD_condensaRep."', condensaCambio = '".$climaBD_condensaCambio."', observaciones ='".$climaBD_observaciones."', comentarios = '".$climaBD_comentarios."', capturaFotos = '".$climaBD_fotos."',tiempoTrabajo = '".$climaBD_tiempoTrabajo."'";

    if ($mysqli->query($sql_diagnClimB) === TRUE) {
    echo "New record created successfully";
    }
    /*Evaporadores
    $sql_evaporador1 = "INSERT INTO cor_evaporador SET id_orden ='".$last_id."',marca = '".$evapora1_marca."', modelo= '".$evapora1_modelo."', serire = '".$evapora1_serie."', capacidadHP = '".$evapora1_hp."', cantidad = '".$evapora1_cantidad."'";
    if ($mysqli->query($sql_evaporador1) === TRUE) {
        echo "New record created successfully";
        }  
    $sql_evaporador2 = "INSERT INTO cor_evaporador SET id_orden ='".$last_id."',marca = '".$evapora2_marca."', modelo= '".$evapora2_modelo."', serire = '".$evapora2_serie."', capacidadHP = '".$evapora2_hp."', cantidad = '".$evapora2_cantidad."'";
    if ($mysqli->query($sql_evaporador2) === TRUE) {
        echo "New record created successfully";
        }  
    $sql_evaporador3 = "INSERT INTO cor_evaporador SET id_orden ='".$last_id."',marca = '".$evapora3_marca."', modelo= '".$evapora3_modelo."', serire = '".$evapora3_serie."', capacidadHP = '".$evapora3_hp."', cantidad = '".$evapora3_cantidad."'";
    if ($mysqli->query($sql_evaporador3) === TRUE) {
        echo "New record created successfully";
        } 
    $sql_evaporador4 = "INSERT INTO cor_evaporador SET id_orden ='".$last_id."',marca = '".$evapora4_marca."', modelo= '".$evapora4_modelo."', serire = '".$evapora4_serie."', capacidadHP = '".$evapora4_hp."', cantidad = '".$evapora4_cantidad."'";
    if ($mysqli->query($sql_evaporador4) === TRUE) {
        echo "New record created successfully";
        }  
    $sql_evaporador5 = "INSERT INTO cor_evaporador SET id_orden ='".$last_id."',marca = '".$evapora5_marca."', modelo= '".$evapora5_modelo."', serire = '".$evapora5_serie."', capacidadHP = '".$evapora5_hp."', cantidad = '".$evapora5_cantidad."'";
    if ($mysqli->query($sql_evaporador2) === TRUE) {
        echo "New record created successfully";
        } 
    $sql_evaporador6 = "INSERT INTO cor_evaporador SET id_orden ='".$last_id."',marca = '".$evapora6_marca."', modelo= '".$evapora6_modelo."', serire = '".$evapora6_serie."', capacidadHP = '".$evapora6_hp."', cantidad = '".$evapora6_cantidad."'";
    if ($mysqli->query($sql_evaporador6) === TRUE) {
        echo "New record created successfully";
        } 

    /*Materiales/items

    $sql_item1 ="INSERT INTO orden_trabajo_materiales SET id_orden = '".$last_id."', seleccion = '".$item1_seleccion."',descripcion ='".$item1_descripcion."', unidad ='".$item1_unidad."', cantidad = '".$item1_cantidad."', observaciones = '".$item1_observaciones."'";

if ($mysqli->query($sql_item1) === TRUE) {
    echo "New record created successfully";
    }

$sql_item2 ="INSERT INTO orden_trabajo_materiales SET id_orden = '".$last_id."', seleccion = '".$item2_seleccion."',descripcion ='".$item2_descripcion."', unidad ='".$item2_unidad."', cantidad = '".$item2_cantidad."', observaciones = '".$item2_observaciones."'";

if ($mysqli->query($sql_item2) === TRUE) {
    echo "New record created successfully";
    }

$sql_item3 ="INSERT INTO orden_trabajo_materiales SET id_orden = '".$last_id."', seleccion = '".$item3_seleccion."',descripcion ='".$item3_descripcion."', unidad ='".$item3_unidad."', cantidad = '".$item3_cantidad."', observaciones = '".$item3_observaciones."'";

if ($mysqli->query($sql_item3) === TRUE) {
    echo "New record created successfully";
    }

$sql_item4 ="INSERT INTO orden_trabajo_materiales SET id_orden = '".$last_id."', seleccion = '".$item4_seleccion."',descripcion ='".$item4_descripcion."', unidad ='".$item4_unidad."', cantidad = '".$item4_cantidad."', observaciones = '".$item4_observaciones."'";

if ($mysqli->query($sql_item4) === TRUE) {
    echo "New record created successfully";
    }

$sql_item5 ="INSERT INTO orden_trabajo_materiales SET id_orden = '".$last_id."', seleccion = '".$item5_seleccion."',descripcion ='".$item5_descripcion."', unidad ='".$item5_unidad."', cantidad = '".$item5_cantidad."', observaciones = '".$item5_observaciones."'";

if ($mysqli->query($sql_item5) === TRUE) {
    echo "New record created successfully";
    }

$sql_item6 ="INSERT INTO orden_trabajo_materiales SET id_orden = '".$last_id."', seleccion = '".$item6_seleccion."',descripcion ='".$item6_descripcion."', unidad ='".$item6_unidad."', cantidad = '".$item6_cantidad."', observaciones = '".$item6_observaciones."'";

if ($mysqli->query($sql_item6) === TRUE) {
    echo "New record created successfully";
    }

$sql_item7 ="INSERT INTO orden_trabajo_materiales SET id_orden = '".$last_id."', seleccion = '".$item7_seleccion."',descripcion ='".$item7_descripcion."', unidad ='".$item7_unidad."', cantidad = '".$item7_cantidad."', observaciones = '".$item7_observaciones."'";

if ($mysqli->query($sql_item7) === TRUE) {
    echo "New record created successfully";
    }

$sql_item8 ="INSERT INTO orden_trabajo_materiales SET id_orden = '".$last_id."', seleccion = '".$item8_seleccion."',descripcion ='".$item8_descripcion."', unidad ='".$item8_unidad."', cantidad = '".$item8_cantidad."', observaciones = '".$item8_observaciones."'";

if ($mysqli->query($sql_item8) === TRUE) {
    echo "New record created successfully";
    }

$sql_item9 ="INSERT INTO orden_trabajo_materiales SET id_orden = '".$last_id."', seleccion = '".$item9_seleccion."',descripcion ='".$item9_descripcion."', unidad ='".$item9_unidad."', cantidad = '".$item9_cantidad."', observaciones = '".$item9_observaciones."'";

if ($mysqli->query($sql_item9) === TRUE) {
    echo "New record created successfully";
    }

$sql_item10 ="INSERT INTO orden_trabajo_materiales SET id_orden = '".$last_id."', seleccion = '".$item10_seleccion."',descripcion ='".$item10_descripcion."', unidad ='".$item10_unidad."', cantidad = '".$item10_cantidad."', observaciones = '".$item10_observaciones."'";

if ($mysqli->query($sql_item10) === TRUE) {
    echo "New record created successfully";
    } 
/*Diagnostico Refrigeracion
    $sql_diagnosticoRefD = "INSERT INTO cor_diagnostico_refrigeracion SET id_orden = '".$last_id."', evaporaRep = '".$refD_evaporaRep."' , evaporacambio = '".$refD_evaporaCambio."', condesaRep = '".$refD_condesaRep."', condesaCambio = '".$refD_condensaCambio."', observaciones = '".$refD_observaciones."', comentarios = '".$refD_comentarios."', fotos = '".$refD_fotos."', tiempoTrabajo = '".$refD_tiempoTrabajo."'";

    if ($mysqli->query($sql_diagnosticoRefD) === TRUE) {
        echo "New record created successfully";
        }
        
        /*Caracteristicas Refrigeracion
    $sql_caracteristicasRef = "INSERT INTO cor_caracteristicas_refrigeracion SET id_orden = '".$last_id."', descripcion = '".$ref_descripcion."' , codigo = '".$ref_codigo."', marca = '".$ref_marca."', modelo = '".$ref_modelo."', serie = '".$ref_serie."', compresorBTU = '".$ref_compresorBtu."', compresorCantidad = '".$ref_compresorCantidad."', ventiladorHP = '".$ref_ventiladorHp."', ventiladorCantidad = '".$ref_ventiladorCantidad."'";

    if ($mysqli->query($sql_caracteristicasRef) === TRUE) {
        echo "New record created successfully";
        }
        
        /*Diagnostico Ventilacion
    $sql_diagnosticoventilaD = "INSERT INTO cor_diagnostico_ventilacion SET id_orden = '".$last_id."', reparacion = '".$ventilaD_reparacion."' , cambio = '".$ventilaD_cambio."', observaciones = '".$ventilaD_observaciones."', comentarios = '".$ventilaD_comentarios."', capturaFotos = '".$ventilaD_fotos."', tiempoTrabajo = '".$ventilaD_tiempoTrabajo."'";

    if ($mysqli->query($sql_diagnosticoventilaD) === TRUE) {
        echo "New record created successfully";
        }
        
        /*Caracteristicas Ventilacion
    $sql_caracteristicasventila = "INSERT INTO cor_caracteristicas_ventilacion SET id_orden = '".$last_id."', descripcion = '".$ventila_descripcion."' , codigo = '".$ventila_codigo."', marca = '".$ventila_marca."', modelo = '".$ventila_modelo."', serie = '".$ventila_serie."', capacidadHP = '".$ventila_hp."', capacidadCFN = '".$ventila_cfn."'";

    if ($mysqli->query($sql_caracteristicasventila) === TRUE) {
        echo "New record created successfully";
        }
        
        /*herramientas de trabajo
    $sql_hertra = "INSERT INTO orden_trabajo_herramientas SET id_orden = '".$last_id."', herramientas = '".$hertra_herramientas."' , requerimientos = '".$hertra_requerimientos."'";

    if ($mysqli->query($sql_hertra) === TRUE) {
        echo "New record created successfully";
        }
        
        /*observaciones generales y firmas
    $sql_obs    = "INSERT INTO cor_observaciones_firmas SET id_orden = '".$last_id."', nombreResponsable = '".$obs_nombreResponsable."' , cargo = '".$obs_cargo."' , comentarios = '".$obs_comentarios."' , observaciones = '".$obs_observaciones."' , firma = '".$obs_firma."' , copia = '".$obs_copia."'";

    if ($mysqli->query($sql_obs) === TRUE) {
        echo "New record created successfully";
        }

/*geolocalizacion = '".$infgen_fechaHora."', fechaCorrectivo = '".$infgen_geolocalizacion."',
horaLlegada = '".$infgen_hrllegada."',*/

/*quert*/
$reporte_r_correctivo ="INSERT INTO reportes_r_correctivo SET 
cliente = '".$infgen_cliente."',
id_cliente = '".$infgen_id_cliente."', tipo_cliente = '".$infgen_tipoCliente."',
sucursal = '".$infgen_sucursal."', id_sucursal = '".$infgen_id_sucursal."',
direccion = '".$infgen_direccion."', refLugar = '".$infgen_refLugar."',
ruta = '".$infgen_ruta."', detalle_ref = '".$infgen_referencia."',
observacion1 = '".$infgen_observaciones."', tipo_trabajo = '".$infgen_tipoTrabajo."',
tipo_equipo = '".$infequ_tipo."', id_tipo_equipo = '".$oinfequ_id_tipoEquipo."',
area_climatizar = '".$infequ_area."', nombre_area = '".$infequ_nombreArea."',
id_area = '".$infequ_id_areaClimatiza."', 
climaA_descripcion = '".$climaA_descripcion."', climaA_codigo = '".$climaA_codigo."', climaA_marca = '".$climaA_marca."', 
climaA_btu = '".$climaA_btu."', climaA_modelo = '".$climaA_modelo."', climaA_serie = '".$climaA_serie."', 
climaAD_reparacion = '".$climaAD_reparacion."', climaAD_cambio = '".$climaAD_cambio."', climaAD_observaciones = '".$climaAD_observaciones."', 
climaAD_comentarios = '".$climaAD_comentarios."', climaAD_capturaFotos = '".$climaAD_capturaFotos."', climaAD_tiempoTrabajo = '".$climaAD_tiempoTrabajo."', 
climaAD_reparacion = '".$climaAD_reparacion."', climaAD_cambio = '".$climaAD_cambio."', climaAD_observaciones = '".$climaAD_observaciones."', 
climaAD_comentarios = '".$climaAD_comentarios."', climaAD_capturaFotos = '".$climaAD_capturaFotos."', climaAD_tiempoTrabajo = '".$climaAD_tiempoTrabajo."', 
climaB_descripcion = '".$climaB_descripcion."', climaB_codigo = '".$climaB_codigo."', climaB_evaporador = '".$climaB_evaporador."', 
climaB_marca = '".$climaB_marca."', climaB_btu = '".$climaB_btu."', climaB_modelo = '".$climaB_modelo."', 
climaB_serie = '".$climaB_serie."', climaB_condensador = '".$climaB_condensador."', climaB_condensadorMarca = '".$climaB_condensadorMarca."', 
climaB_condensadorBtu = '".$climaB_condensadorBtu."', climaB_condensadorModelo = '".$climaB_condensadorModelo."', climaB_condensadorSerie = '".$climaB_condensadorSerie."', 
climaBD_reparacion = '".$climaBD_reparacion."', climaBD_cambio = '".$climaBD_cambio."', climaBD_condensaRep = '".$climaBD_condensaRep."', 
climaBD_condensaCambio = '".$climaBD_condensaCambio."', climaBD_observaciones = '".$climaBD_observaciones."', climaBD_comentarios = '".$climaBD_comentarios."', 
climaBD_fotos = '".$climaBD_fotos."', climaBD_tiempoTrabajo = '".$climaBD_tiempoTrabajo."', ventila_descripcion = '".$ventila_descripcion."',
 ventila_codigo = '".$ventila_codigo."', ventila_marca = '".$ventila_marca."', ventila_hp = '".$ventila_hp."', 
ventila_cfn = '".$ventila_cfn."', ventila_modelo = '".$ventila_modelo."', ventila_serie = '".$ventila_serie."', 
ventilaD_reparacion = '".$ventilaD_reparacion."', ventilaD_cambio = '".$ventilaD_cambio."', ventilaD_observaciones = '".$ventilaD_observaciones."', 
ventilaD_comentarios = '".$ventilaD_comentarios."', ventilaD_fotos = '".$ventilaD_fotos."', ventilaD_tiempoTrabajo = '".$ventilaD_tiempoTrabajo."', 
ref_descripcion = '".$ref_descripcion."', ref_codigo = '".$ref_codigo."', ref_marca = '".$ref_marca."', 
ref_modelo = '".$ref_modelo."', ref_modelo = '".$ref_modelo."', ref_compresorBtu = '".$ref_compresorBtu."', 
ref_compresorCantidad = '".$ref_compresorCantidad."', ref_ventiladorHp = '".$ref_ventiladorHp."', ref_ventiladorCantidad = '".$ref_ventiladorCantidad."', 
evapora1_marca = '".$evapora1_marca."', evapora1_modelo = '".$evapora1_modelo."', evapora1_serie = '".$evapora1_serie."', 
evapora1_hp = '".$evapora1_hp."', evapora1_cantidad = '".$evapora1_cantidad."', evapora2_modelo = '".$evapora2_modelo."', 
evapora2_marca = '".$evapora2_marca."', evapora2_serie = '".$evapora2_serie."', evapora2_hp = '".$evapora2_hp."', 
evapora2_cantidad = '".$evapora2_cantidad."', evapora3_marca = '".$evapora3_marca."', evapora3_modelo = '".$evapora3_modelo."', 
evapora3_serie = '".$evapora3_serie."', evapora3_hp = '".$evapora3_hp."', evapora3_cantidad = '".$evapora3_cantidad."', 
evapora4_marca = '".$evapora4_marca."', evapora4_modelo = '".$evapora4_modelo."', evapora4_serie = '".$evapora4_serie."', 
evapora4_hp = '".$evapora4_hp."', evapora4_cantidad = '".$evapora4_cantidad."', evapora5_marca = '".$evapora5_marca."', 
evapora5_modelo = '".$evapora5_modelo."', evapora5_serie = '".$evapora5_serie."', evapora5_hp = '".$evapora5_hp."', 
evapora5_cantidad = '".$evapora5_cantidad."', evapora6_mmodelo = '".$evapora6_mmodelo."', evapora6_marca = '".$evapora6_marca."', 
evapora6_serie = '".$evapora6_serie."', evapora6_hp = '".$evapora6_hp."', evapora6_cantidad = '".$evapora6_cantidad."', 
refD_evaporaRep = '".$refD_evaporaRep."', refD_evaporaCambio = '".$refD_evaporaCambio."', refD_condesaRep = '".$refD_condesaRep."', 
refD_condensaCambio = '".$refD_condensaCambio."', refD_observaciones = '".$refD_observaciones."', refD_comentarios = '".$refD_comentarios."', 
refD_fotos = '".$refD_fotos."', refD_tiempoTrabajo = '".$refD_tiempoTrabajo."', 
selectedItem1 = '".$item1_seleccion."', descriptItem1 = '".$item1_descripcion."', 
unidadItem1 = '".$item1_unidad."', cantidadItem1 = '".$item1_cantidad."', observacItem1 = '".$item1_observaciones."',
selectedItem2 = '".$item2_seleccion."', descriptItem2 = '".$item2_descripcion."', 
unidadItem2 = '".$item2_unidad."', cantidadItem2 = '".$item2_cantidad."', observacItem2 = '".$item2_observaciones."' , 
selectedItem3 = '".$item3_seleccion."', descriptItem3 = '".$item3_descripcion."', 
unidadItem3 = '".$item3_unidad."', cantidadItem3 = '".$item3_cantidad."', observacItem3 = '".$item3_observaciones."',
selectedItem4 = '".$item4_seleccion."', descriptItem4 = '".$item4_descripcion."', 
unidadItem4 = '".$item4_unidad."', cantidadItem4 = '".$item4_cantidad."', observacItem4 = '".$item4_observaciones."', 
selectedItem5 = '".$item5_seleccion."', descriptItem5 = '".$item5_descripcion."', 
unidadItem5 = '".$item5_unidad."', cantidadItem5 = '".$item5_cantidad."', observacItem5 = '".$item5_observaciones."',
selectedItem6 = '".$item6_seleccion."', descriptItem6 = '".$item6_descripcion."', 
unidadItem6 = '".$item6_unidad."', cantidadItem6 = '".$item6_cantidad."', observacItem6 = '".$item6_observaciones."', 
selectedItem7 = '".$item7_seleccion."', descriptItem7 = '".$item7_descripcion."', 
unidadItem7  = '".$item7_unidad."', cantidadItem7    = '".$item7_cantidad."', observacItem7    = '".$item7_observaciones."',
selectedItem8 = '".$item8_seleccion."', descriptItem8 = '".$item8_descripcion."', 
unidadItem8 = '".$item8_unidad."', cantidadItem8 = '".$item8_cantidad."', observacItem8 = '".$item8_observaciones."', 
selectedItem9 = '".$item9_seleccion."', descriptItem9 = '".$item9_descripcion."', 
unidadItem9 = '".$item9_unidad."',cantidadItem9 = '".$item9_cantidad."', observacItem9 = '".$item9_observaciones."', 
selectedItem10 = '".$item10_seleccion."', descriptItem10 = '".$item10_descripcion."', 
unidadItem10 = '".$item10_unidad."', cantidadItem10 = '".$item10_cantidad."', observacItem10 = '".$item10_observaciones."', 
nombreResponsable = '".$obs_nombreResponsable."', cargo = '".$obs_cargo."', comentarios = '".$obs_comentarios."', 
observaciones = '".$obs_observaciones."', firma  = '".$obs_firma."', correoCopia = '".$obs_copia."'"; die($reporte_r_correctivo);

die($reporte_r_correctivo); 
    if ($mysqli->query($reporte_r_correctivo) === TRUE) {
        echo "New record created successfully";
        }  

}

function D($arreglo){
    echo "<pre>";
        print_r($arreglo);
    echo "</pre>";
}


function limpiar($String)
{
    $String = str_replace(array('á','à','â','ã','ª','ä'),"a",$String);
    $String = str_replace(array('Á','À','Â','Ã','Ä'),"A",$String);
    $String = str_replace(array('Í','Ì','Î','Ï'),"I",$String);
    $String = str_replace(array('í','ì','î','ï'),"i",$String);
    $String = str_replace(array('é','è','ê','ë'),"e",$String);
    $String = str_replace(array('É','È','Ê','Ë'),"E",$String);
    $String = str_replace(array('ó','ò','ô','õ','ö','º'),"o",$String);
    $String = str_replace(array('Ó','Ò','Ô','Õ','Ö'),"O",$String);
    $String = str_replace(array('ú','ù','û','ü'),"u",$String);
    $String = str_replace(array('Ú','Ù','Û','Ü'),"U",$String);
    $String = str_replace(array('[','^','´','`','¨','~',']'),"",$String);
    $String = str_replace("ç","c",$String);
    $String = str_replace("Ç","C",$String);
    $String = str_replace("ñ","n",$String);
    $String = str_replace("Ñ","N",$String);
    $String = str_replace("Ý","Y",$String);
    $String = str_replace("ý","y",$String);
    
    $String = str_replace("&aacute;","a",$String);
    $String = str_replace("&Aacute;","A",$String);
    $String = str_replace("&eacute;","e",$String);
    $String = str_replace("&Eacute;","E",$String);
    $String = str_replace("&iacute;","i",$String);
    $String = str_replace("&Iacute;","I",$String);
    $String = str_replace("&oacute;","o",$String);
    $String = str_replace("&Oacute;","O",$String);
    $String = str_replace("&uacute;","u",$String);
    $String = str_replace("&Uacute;","U",$String);

    $String = str_replace("\u00C1;","Á",$String);
    $String = str_replace("\u00E1;","á",$String);
    $String = str_replace("\u00C9;","É",$String);
    $String = str_replace("\u00E9;","é",$String);
    $String = str_replace("\u00CD;","Í",$String);
    $String = str_replace("\u00ED;","í",$String);
    $String = str_replace("\u00D3;","Ó",$String);
    $String = str_replace("\u00F3;","ó",$String);
    $String = str_replace("\u00DA;","Ú",$String);
    $String = str_replace("\u00FA;","ú",$String);
    $String = str_replace("\u00DC;","Ü",$String);
    $String = str_replace("\u00FC;","ü",$String);
    $String = str_replace("\u00D1;","Ṅ",$String);
    $String = str_replace("\u00F1;","ñ",$String);

    $String = str_replace("A", "a", $String);
    return $String;
}

function ltl($s)
{
    return limpiar(trim(strtolower($s)));
}