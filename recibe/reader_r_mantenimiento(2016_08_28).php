<?php
header('Content-Type: text/html; charset=utf-8');
ini_set('display_errors',1);
error_reporting(E_ALL);

/** DATABASE */
$connect = new stdClass;
$connect->db = @new mysqli("localhost", "auditoriasbonita", "u[V(fTIUbcVb", "cegaservices");
$connect->db2 = @new mysqli("localhost", "auditoriasbonita", "u[V(fTIUbcVb", "cegaservices2");

#$mysqli = @new mysqli("localhost", "root", "", "auditoriasbonita");
if (mysqli_connect_errno()) {
    printf("Falló la conexión: %s\n", mysqli_connect_error());
    exit();
}
$connect->db->set_charset("utf8");
$connect->db2->set_charset("utf8");

/** Directorio de los JSON */
$path = realpath('./json');

$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
foreach($objects as $name => $object){
    if('.' != $object->getFileName() && '..' != $object->getFileName() && $path != $object->getPath()){
        D($object->getPath());
        $pos1 = strpos($object->getPath(), "revision_mantenimiento_");
        // echo "Hola";
        // echo "Hola ".(int)$pos1;
        if($pos1 !== false){
            $ext = pathinfo($object->getPathName(), PATHINFO_EXTENSION);
            if('json' == $ext || 'jpeg' == $ext || 'jpg' == $ext || 'png' == $ext || 'mp3' == $ext || '3gpp' == $ext){
                switch ($ext) {
                    case 'json':
                        /* READ AND MOVE JSON */
                        $json = json_decode(trim(file_get_contents($object->getPathName())), true);
                        // echo '<pre>';
                        // print_r($json);
                        // echo '</pre>';
                        process_json($json, $object->getFileName(), $connect);
                        move_json($object->getPathName(), $object->getFileName());
                        break;

                    case 'jpeg':
                    case 'jpg':
                    case 'png':
                        /* MOVE JSON */
                        move_image($object->getPathName(), $object->getFileName());
                        break;

                    case '3gpp':
                    case 'mp3':
                        /* MOVE AUDIO */
                        move_audio($object->getPathName(), $object->getFileName());
                        break;
                    case 'mp4':
                        /* MOVE AUDIO */
                        move_audio($object->getPathName(), $object->getFileName());
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
//$mysqli->close();

function move_json($file, $nameFile){
    echo $file;
    echo $nameFile;
    // die();
    if(!rename($file, __DIR__."/../reportes_vistos/json/$nameFile")){
        echo 'error';
    }
}

function move_image($file, $nameFile){
    // die();
    if(!rename($file, __DIR__."/../reportes_vistos/image/$nameFile")){
        echo 'error';
    }
}

function move_audio($file, $nameFile){

    // die();

    if(!rename($file, __DIR__."/../reportes_vistos/audio/$nameFile")){

        echo 'error';

    }

}

function process_json($json, $filename, $connect){
    $identifier = $json['identifier'];
    // D($identifier);
    $version = $json['version']. '';
    $zone = $json['zone']. '';
    $referenceNumber = $json['referenceNumber']. '';
    $state = $json['state']. '';
    $deviceSubmitDate = $json['deviceSubmitDate'];
    $deviceSubmitDateDate = $deviceSubmitDate['time']. '';
    $deviceSubmitDateZone = $deviceSubmitDate['zone']. '';
    $fecha_file = str_replace("-", "", explode("T", $deviceSubmitDateDate)[0]);
    $shiftedDeviceSubmitDate = $json['shiftedDeviceSubmitDate'].'';
    $serverReceiveDate = $json['serverReceiveDate'].'';

    $form = $json['form'];
    $form_identifier = $form['identifier'].'';
    $form_versionIdentifier = $form['versionIdentifier'].'';
    $form_name = $form['name'].'';
    $form_version = $form['version'].'';
    $form_formSpaceIdentifier = $form['formSpaceIdentifier'].'';
    $form_formSpaceName = $form['formSpaceName'].'';

    $user = $json['user'];
    $user_identifier = $user['identifier'].'';
    $user_username = $user['username'].'';
    $user_displayName = $user['displayName'].'';

    $geoStamp = $json['geoStamp'];
    $geoStamp_success = $geoStamp['success'].'';
    $geoStamp_captureTimestamp = $geoStamp['captureTimestamp'];
    $geoStamp_captureTimestamp_provided = $geoStamp_captureTimestamp['provided'];
    $geoStamp_captureTimestamp_provided_time = $geoStamp_captureTimestamp_provided['time'].'';;
    $geoStamp_captureTimestamp_provided_zone = $geoStamp_captureTimestamp_provided['zone'].'';;
    $geoStamp_captureTimestamp_shifted = $geoStamp_captureTimestamp['shifted'].'';;
    $geoStamp_errorMessage = $geoStamp['errorMessage'].'';;
    $geoStamp_source = $geoStamp['source'].'';;
    $geoStamp_coordinates = $geoStamp['coordinates'];
    $geoStamp_coordinates_latitude = $geoStamp_coordinates['latitude'].'';;
    $geoStamp_coordinates_longitude = $geoStamp_coordinates['longitude'].'';;
    $geoStamp_coordinates_altitude = $geoStamp_coordinates['altitude'].'';;
    $geoStamp_address = $geoStamp['address'].'';;

    $pages = $json['pages'];

    // revision mantenimiento
    // -> cotizacion
    $cot_cliente     = "";
    $cot_id_cliente  = "";
    $cot_sucursal     = "";
    $cot_id_sucursal  = "";
    $cot_tipoCliente = "";
    $cot_direccion   = "";
    $cot_tipoTrabajo = "";

    // -> order
    $ord_tipoEquipo               = "";
    $ord_id_tipoEquipo            = "";
    $ord_areaClimatiza            = "";
    $ord_id_areaClimatiza         = "";
    $ord_nombreArea               = "";
    $ord_climaDescripcionEquipo = "";
    $ord_id_climaDescripcionEquipo = "";
    $ord_climaBtu                 = "";
    $ord_climaCantidad            = "";
    $ord_ventilaDescripcionEquipo = "";
    $ord_id_ventilaDescripcionEquipo = "";
    $ord_ventilaHp                = "";
    $ord_ventilaCfn               = "";
    $ord_ventilaCantidad          = "";
    $ord_refDescripcion           = "";
    $ord_id_refDescripcion           = "";
    $ord_refBtu                   = "";
    $ord_refCantidad              = "";
    $ord_refCantidadEva           = "";
    $ord_refCantidadMotorEva      = "";
    $ord_refCantidadUnidadCon     = "";
    $ord_refCantidadMotorCon      = "";
    $ord_herNecesaria             = [];
    $ord_herReq                   = "";

    // -> cliente
    $cli_refLugar = "";
    $cli_refRuta  = "";

    // -> sucursal
    $suc_refLugar = "";
    $suc_refRuta  = "";

    // -> equipo
    $equ_tipoEquipo         = "";
    $equ_areaClimatiza      = "";
    $equ_climaDescripcion   = "";
    $equ_ventilaDescripcion = "";

    /*Observaciones Generales y firmas*/
    $obsgen_nombreResponsable="";
    $obsgen_cargo="";
    $obsgen_comentarios="";
    $obsgen_observaciones="";
    $obsgen_firma="";
    $obsgen_envioCopia ="";
    // ciclo
    foreach($pages as $key => $page_data){
        $pagina = $key+1;
        $pagina_nombre = $page_data['name'];
        $sql_muestra_causas = array();
        $answers = $page_data["answers"];

        //echo ltl($pagina_nombre).'<br>';
        foreach($answers as $answer){
            // D($answer);
            $label    = $answer['label'];
            $dataType = $answer['dataType'];
            $question = $answer['question'];
            $values   = $answer['values'];
            $labelita = limpiar(trim(strtolower($label)));

            if("informacion general" == limpiar(trim(strtolower($pagina_nombre))))
            {
                // D(ltl($question));
                // cotizacion
                $cot_cliente = ltl($question) == "cliente:" ? (isset($values[0]) ? $values[0] : '') : $cot_cliente;
                $cot_id_cliente = ltl($question) == "cliente: - id" ? (isset($values[0]) ? $values[0] : '') : $cot_id_cliente;
                $cot_sucursal = ltl($question) == "sucursal:" ? (isset($values[0]) ? $values[0] : '') : $cot_sucursal;
                $cot_id_sucursal = ltl($question) == "sucursal: - id" ? (isset($values[0]) ? $values[0] : '') : $cot_id_sucursal;
                $cot_tipoCliente = ltl($question) == "tipo de cliente:" ? (isset($values[0]) ? $values[0] : '') : $cot_tipoCliente;
                $cot_direccion = ltl($question) == "direccion:" ? (isset($values[0]) ? $values[0] : '') : $cot_direccion;
                $cot_tipoTrabajo = ltl($question) == "tipo de trabajo:" ? (isset($values[0]) ? $values[0] : '') : $cot_tipoTrabajo;

                // cliente
                $cli_refLugar = ltl($question) == "referencia del lugar:" ? (isset($values[0]) ? $values[0] : '') : $cli_refLugar;
                $cli_refRuta = ltl($question) == "ruta" ? (isset($values[0]) ? $values[0] : '') : $cli_refRuta;

                // sucursal
                $suc_refLugar = ltl($question) == "referencia del lugar:" ? (isset($values[0]) ? $values[0] : '') : $suc_refLugar;
                $suc_refRuta = ltl($question) == "ruta" ? (isset($values[0]) ? $values[0] : '') : $suc_refRuta;                
            }

            if("informacion equipo" == limpiar(trim(strtolower($pagina_nombre))))
            {
                // D(ltl($question));
                // D($values);
                // orden
                $ord_tipoEquipo = ltl($question) == 'tipo de equipo:' ? (isset($values[0]) ? $values[0] : '') : $ord_tipoEquipo;
                $ord_id_tipoEquipo = ltl($question) == 'tipo de equipo: - id' ? (isset($values[0]) ? $values[0] : '') : $ord_id_tipoEquipo;
                $ord_areaClimatiza = ltl($question) == 'area que climatiza:' ? (isset($values[0]) ? $values[0] : '') : $ord_areaClimatiza;
                $ord_id_areaClimatiza = ltl($question) == 'area que climatiza - id' ? (isset($values[0]) ? $values[0] : '') : $ord_id_areaClimatiza;
                $ord_nombreArea = ltl($question) == 'area que climatiza - nombre' ? (isset($values[0]) ? $values[0] : '') : $ord_nombreArea;

                // equipo
                $equ_tipoEquipo = ltl($question) == 'tipo de equipo:' ? (isset($values[0]) ? $values[0] : '') : $equ_tipoEquipo;
                $equ_areaClimatiza = ltl($question) == 'area que climatiza:' ? (isset($values[0]) ? $values[0] : '') : $equ_areaClimatiza;
            }

            if("caracteristicas equipo (climatizacion)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                // orden
                $ord_climaDescripcionEquipo = ltl($question) == 'descripcion del equipo:' ? (isset($values[0]) ? $values[0] : '') : $ord_climaDescripcionEquipo;
                $ord_id_climaDescripcionEquipo = ($ord_climaDescripcionEquipo == "CLIMATIZACIÓN A") ? 1 : 2;
                // $ord_id_climaDescripcionEquipo = ltl($question) == 'descripción del equipo: - id' ? (isset($values[0]) ? $values[0] : '') : $ord_id_climaDescripcionEquipo;
                $ord_climaBtu = ltl($question) == 'capacidad (btu):' ? (isset($values[0]) ? $values[0] : '') : $ord_climaBtu;
                $ord_climaCantidad = ltl($question) == 'cantidad:' ? (isset($values[0]) ? $values[0] : '') : $ord_climaCantidad;
                
                // equipo
                $equ_climaDescripcion = ltl($question) == 'descripcion del equipo:' ? (isset($values[0]) ? $values[0] : '') : $equ_climaDescripcion;
            }

            if("caracteristicas equipo (ventilacion)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                // orden
                $ord_id_ventilaDescripcionEquipo = 3;
                $ord_ventilaDescripcionEquipo = ltl($question) == 'descripcion del equipo:' ? (isset($values[0]) ? $values[0] : '') : $ord_ventilaDescripcionEquipo;
                 // $ord_id_ventilaDescripcionEquipo = ltl($question) == 'descripción del equipo: - id' ? (isset($values[0]) ? $values[0] : '') : $ord_id_ventilaDescripcionEquipo;
                $ord_ventilaHp = ltl($question) == 'capacidad (hp):' ? (isset($values[0]) ? $values[0] : '') : $ord_ventilaHp;
                $ord_ventilaCfn = ltl($question) == 'capacidad (cfn):' ? (isset($values[0]) ? $values[0] : '') : $ord_ventilaCfn;
                $ord_ventilaCantidad = ltl($question) == 'cantidad:' ? (isset($values[0]) ? $values[0] : '') : $ord_ventilaCantidad;

                // equipo
                $equ_ventilaDescripcion = ltl($question) == 'descripcion del equipo:' ? (isset($values[0]) ? $values[0] : '') : $equ_ventilaDescripcion;
            }

            if("caracteristicas equipo (refrigeracion)" == ltl($pagina_nombre))
            {
                $ord_id_refDescripcion = 4;
                $ord_refDescripcion = ltl($question) == "descripcion del equipo:" ? (isset($values[0]) ? $values[0] : '') : $ord_refDescripcion;
                $ord_id_refDescripcion = ltl($question) == "descripción del equipo: - id" ? (isset($values[0]) ? $values[0] : '') : $ord_id_refDescripcion;
                $ord_refBtu = ltl($question) == "capacidad (hp):" ? (isset($values[0]) ? $values[0] : '') : $ord_refBtu;
                $ord_refCantidad = ltl($question) == "cantidad:" ? (isset($values[0]) ? $values[0] : '') : $ord_refCantidad;
                $ord_refCantidadEva = ltl($question) == "cantidad evaporadores:" ? (isset($values[0]) ? $values[0] : '') : $ord_refCantidadEva;
                $ord_refCantidadMotorEva = ltl($question) == "cantidad motor ventiladores (evaporadores):" ? (isset($values[0]) ? $values[0] : '') : $ord_refCantidadMotorEva;
                $ord_refCantidadUnidadCon = ltl($question) == "cantidad unidad condensadoras:" ? (isset($values[0]) ? $values[0] : '') : $ord_refCantidadUnidadCon;
                $ord_refCantidadMotorCon = ltl($question) == "cantidad motor ventiladores (condensadoras):" ? (isset($values[0]) ? $values[0] : '') : $ord_refCantidadMotorCon;
            }

            if("herramientas de trabajo" == ltl($pagina_nombre))
            {
                $ord_herNecesaria = ltl($question) == "herramientas necesarias para el trabajo:" ? (isset($values) ? $values : []) : $ord_herNecesaria;
                $ord_herReq = ltl($question) == "requerimientos especiales:" ? (isset($values[0]) ? $values[0] : '') :$ord_herReq;
            }

            if("observaciones generales y firmas" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $obsgen_nombreResponsable = "ndrqset:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $obsgen_nombreResponsable; // Nombre del responsable 
                $obsgen_cargo = "cargo:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $obsgen_cargo; // Cargo:
                $obsgen_comentarios = "cg:"? (isset($values[0]) ? $values[0] : '') : $obsgen_comentarios; // Comentarios Generales
                $obsgen_observaciones = "og:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $obsgen_observaciones; // Observaciones Generales:
                $obsgen_firma = "fdcor" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $obsgen_firma; // Firma del Cliente o Responsable
                $obsgen_envioCopia = "eucdefasc:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $obsgen_envioCopia; // Enviar una copia al siguiente correo:
            }
        }
    }

    // MySQL
    $mysqli = $connect->db;
    $id_usuario = 2;
    if(isset($user_username) && $user_username != "" && $user_username == "ernesto.polit"){
        $mysqli = $connect->db2;
        $id_usuario = 2;
    }

    $sqlValid = "SELECT * FROM cotizaciones WHERE fecha_create = CURRENT_DATE AND id_cliente = ".$cot_id_cliente;
    D($sqlValid);
	$last_cot = 0;
	$res = $mysqli->query($sqlValid);
    if($res->num_rows > 0){
                    $datas = $res->fetch_assoc();
                    $last_cot = $datas['id'];
    }
	else{
		$sql = "INSERT INTO cotizaciones SET 
        id_cliente = '".$cot_id_cliente."', 
        id_sucursal = ' ".$cot_id_sucursal."' , 
        status = 0, 
        fecha_status = CURRENT_DATE,
        id_usuario = {$id_usuario},
        fecha_create = CURRENT_DATE";
 
    // D($sql);    
    if($mysqli->query($sql)== TRUE){
        $last_cot = $mysqli->insert_id;
        $folio = str_pad($last_cot , 10 ,0, STR_PAD_LEFT);
        $mysqli->query("UPDATE cotizaciones SET folio = '{$folio}' WHERE id = '{$last_cot}'");
       
    } else {
        echo "Error: " . $last_cot . "<br>" . $mysqli->error;
    }
		
	}
     $sql_cotizacion = "INSERT INTO cotizaciones_detalle SET id_cotizacion = '".$last_cot."',
         tipo_trabajo = 4 , des_trabajo = 'MANTENIMIENTO'";
        $mysqli->query($sql_cotizacion);
		/*
    
    $sql_detalle = "INSERT INTO orden_trabajo SET 
    id_cotizacion = '".$last_cot."',
    cliente = '".$cot_cliente."',
    fecha_create = CURRENT_DATE,
    tipo_cliente = '".$cot_tipoCliente."',
    direccion = '".$cot_direccion."',
    tipo_trabajo = '".$cot_tipoTrabajo."'";
    // D($sql_detalle);

    
    $last_id = 0;
    if($mysqli->query($sql_detalle)== TRUE){
        $last_id = $mysqli->insert_id;
        //echo "Ultimo ID insertado " . $last_id;
    } else {
    echo "Error: " . $sql_detalle . "<br>" . $mysqli->error;
    }


    // /*----------  Equipos  ----------/
    $sql_equipos = "INSERT INTO orden_trabajo_detalle SET id_orden = '".$last_id."',area = '".$ord_id_areaClimatiza."' ,tipo_equipo = '".$ord_id_tipoEquipo ."' ,des_equipo = '".$equ_climaDescripcion."',tipo_trabajo = '".$equ_ventilaDescripcion."'";
    // D($sql_equipos);
    if ($mysqli->query($sql_equipos) === TRUE) {
    echo "New record created successfully";
    } 
    // D($ord_herNecesaria);
    // /*----------  Herramientas  ----------/
    if(count($ord_herNecesaria) > 0){
        D($ord_herNecesaria);
        foreach ($ord_herNecesaria as $key => $value) {
            $sql_herramientas = "INSERT INTO orden_trabajo_herramientas SET id_orden = '".$last_id."',id_herramienta = '".$key."' ,
                 herramientas = '".$value."' , requerimientos = '".$ord_herReq."'";
            // D($sql_herramientas);

            if ($mysqli->query($sql_herramientas) === TRUE) {
            echo "New record created successfully";
            } 
        }
    }
    // /*----------  Climatización  ---------/
    $sql_climatizacion = "INSERT INTO orden_climatizacion SET id_orden = '".$last_id."',id_tipo_equipo = '".$ord_id_climaDescripcionEquipo."' ,
        des_equipo = '".$ord_climaDescripcionEquipo ."', capacidad = '".$ord_climaBtu."', cantidad = '".$ord_climaCantidad."' ";
    // D($sql_climatizacion);
    if ($mysqli->query($sql_climatizacion) === TRUE) {
    echo "New record created successfully";
    } 

    // /*----------  Refrigeración  ---------/
    $sql_refrigeracion = "INSERT INTO orden_refrigeracion SET id_orden = '".$last_id."', id_tipo_equipo = '".$ord_id_refDescripcion."' ,
    des_equipo = '".$ord_refDescripcion."', capa_btu = '".$ord_refBtu."', cantidad = '".$ord_refCantidad."', cant_evap = '".$ord_refCantidadEva."', motor_evap = '".$ord_refCantidadMotorEva."', cant_con = '".$ord_refCantidadUnidadCon."', motor_con = '".$ord_refCantidadMotorCon ."' ";
    // D($sql_refrigeracion);
    if ($mysqli->query($sql_refrigeracion) === TRUE) {
    echo "New record created successfully";
    } 

    // /*----------  Ventilación  ---------/
    $sql_ventilacion = "INSERT INTO orden_ventilacion SET id_orden = '".$last_id."', id_tipo_equipo = '".$ord_id_ventilaDescripcionEquipo."' ,
    des_equipo = '".$ord_ventilaDescripcionEquipo."', capacidad_hp = '".$ord_ventilaHp."', capacidad_cfn = '".$ord_ventilaCfn."', cantidad = '".$ord_ventilaCantidad."'";
    // D($sql_ventilacion);
    if ($mysqli->query($sql_ventilacion) === TRUE) {
    echo "New record created successfully";
    } 
    
    /*Reporte Mantenimiento revision*/
$reporte_r_mantenimiento ="INSERT INTO reportes_r_mantenimiento SET 
geolocalizacion = '".$infgen_geolocalizacion."', fechaCorrectivo = '".$infgen_fechaHora."', 
horaLlegada = '".$infgen_hrllegada."', cliente = '". $cot_cliente ."',
id_cliente = '".$cot_id_cliente."', tipo_cliente = '".$cot_tipoCliente."',
sucursal = '".$cot_sucursal."', id_sucursal = '".$cot_id_sucursal."',
direccion = '".$cot_direccion."', refLugar = '".$cli_refLugar."',
ruta = '".$suc_refRuta."', detalle_ref = '".$suc_refLugar."',
observacion1 = '".$infgen_observaciones."', tipo_trabajo = '".$cot_tipoTrabajo."',
tipo_equipo = '".$ord_tipoEquipo."', id_tipo_equipo = '".$ord_id_tipoEquipo."',
area_climatizar = '".$ord_areaClimatiza."', nombre_area = '".$ord_nombreArea."',
clima_equipo = '".$ord_climaDescripcionEquipo."',
climaBTU = '".$ord_climaBtu."',
clima_cantidad = '".$ord_climaCantidad."',
clima_descripcion = '".$equ_climaDescripcion."',
venti_id_equipo = '".$ord_id_ventilaDescripcionEquipo."',
venti_descrip_equipo = '".$ord_ventilaDescripcionEquipo."',
venti_hp = '".$ord_ventilaHp."',
venti_cfn = '".$ord_ventilaCfn."',
venti_cantidad = '".$ord_ventilaCantidad."',
venti_decripcion = '".$equ_ventilaDescripcion."',
ref_id_equipo = '".$ord_id_refDescripcion."',
ref_descrip = '".$ord_refDescripcion."',
ref_BTU = '".$ord_refBtu."',
ref_cantidad = '".$ord_refCantidad."',
ref_cantidadEvap = '".$ord_refCantidadEva."',
ref_motor = '".$ord_refCantidadMotorEva."',
ref_unidad = '".$ord_refCantidadUnidadCon."',
ref_motor_condensador = '".$ord_refCantidadMotorCon."',
nombreResponsable = '".$obsgen_nombreResponsable."', 
cargo = '".$obsgen_cargo."', comentarios = '".$obsgen_comentarios."', 
observaciones = '".$obsgen_observaciones."', firma  = '".$obsgen_firma."', correoCopia = '".$obsgen_envioCopia."'"; 


    if ($mysqli->query($reporte_r_mantenimiento) === TRUE) {
        echo "New record created successfully";
        }  
      

}

function D($arreglo){
    echo "<pre>";
        print_r($arreglo);
    echo "</pre>";
}

function limpiar($String)
{
    $String = str_replace(array('á','à','â','ã','ª','ä'),"a",$String);
    $String = str_replace(array('Á','À','Â','Ã','Ä'),"A",$String);
    $String = str_replace(array('Í','Ì','Î','Ï'),"I",$String);
    $String = str_replace(array('í','ì','î','ï'),"i",$String);
    $String = str_replace(array('é','è','ê','ë'),"e",$String);
    $String = str_replace(array('É','È','Ê','Ë'),"E",$String);
    $String = str_replace(array('ó','ò','ô','õ','ö','º'),"o",$String);
    $String = str_replace(array('Ó','Ò','Ô','Õ','Ö'),"O",$String);
    $String = str_replace(array('ú','ù','û','ü'),"u",$String);
    $String = str_replace(array('Ú','Ù','Û','Ü'),"U",$String);
    $String = str_replace(array('[','^','´','`','¨','~',']'),"",$String);
    $String = str_replace("ç","c",$String);
    $String = str_replace("Ç","C",$String);
    $String = str_replace("ñ","n",$String);
    $String = str_replace("Ñ","N",$String);
    $String = str_replace("Ý","Y",$String);
    $String = str_replace("ý","y",$String);
    
    $String = str_replace("&aacute;","a",$String);
    $String = str_replace("&Aacute;","A",$String);
    $String = str_replace("&eacute;","e",$String);
    $String = str_replace("&Eacute;","E",$String);
    $String = str_replace("&iacute;","i",$String);
    $String = str_replace("&Iacute;","I",$String);
    $String = str_replace("&oacute;","o",$String);
    $String = str_replace("&Oacute;","O",$String);
    $String = str_replace("&uacute;","u",$String);
    $String = str_replace("&Uacute;","U",$String);

    $String = str_replace("\u00C1;","Á",$String);
    $String = str_replace("\u00E1;","á",$String);
    $String = str_replace("\u00C9;","É",$String);
    $String = str_replace("\u00E9;","é",$String);
    $String = str_replace("\u00CD;","Í",$String);
    $String = str_replace("\u00ED;","í",$String);
    $String = str_replace("\u00D3;","Ó",$String);
    $String = str_replace("\u00F3;","ó",$String);
    $String = str_replace("\u00DA;","Ú",$String);
    $String = str_replace("\u00FA;","ú",$String);
    $String = str_replace("\u00DC;","Ü",$String);
    $String = str_replace("\u00FC;","ü",$String);
    $String = str_replace("\u00D1;","Ṅ",$String);
    $String = str_replace("\u00F1;","ñ",$String);

    $String = str_replace("A", "a", $String);
    return $String;
}

function ltl($s)
{
    return limpiar(trim(strtolower($s)));
}