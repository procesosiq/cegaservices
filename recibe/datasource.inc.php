<?php

class DataSource
{
    private $mysqli;

    function __construct(){
        $this->mysqli = @new mysqli("localhost", "auditoriasbonita", "u[V(fTIUbcVb", "cegaservices");
        if (mysqli_connect_errno()) {
            printf("Falló la conexión: %s\n", mysqli_connect_error());
            exit();
        }
        $this->mysqli->set_charset("utf8");
    }

    public function getClientes(){
        $d = array();
        $sql = "SELECT id, nombre , id_tipcli AS id_tipo_cliente , direccion , referencia FROM cat_clientes WHERE id_usuario = 2 AND status = 1";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }
	
	public function getTipoEquipo(){
        $d = array();
        $sql = "SELECT id, nombre FROM cat_equiposTipo where status=1";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }

    public function getDescriptionEquipo(){
        $d = array();
        $sql = "SELECT id , 
            (SELECT nombre FROM cat_equiposTipo WHERE id = id_tipoequipo) AS id_tipoequipo ,  
                nombre
            FROM cat_descripciones_equipos where status=1";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }

    public function getHerramientas(){
        $d = array();
        $sql = "SELECT id , IF(especificacion = NULL , herramienta ,CONCAT_WS('-',herramienta ,especificacion)) AS nombre FROM cat_herramientas_trabajo where status=1";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }

    public function getSucursalByCliente($clienteId){
        $d = array();
        $sql = "SELECT id, id_cliente, nombre_contacto FROM cat_sucursales WHERE id_cliente = $clienteId AND id_usuario = 2 AND status = 1";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }
	
	public function getAreaClimatizada(){
        $d = array();
        $sql = "SELECT * FROM cat_areas where status=1";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }
	public function getCapacidadBUTU(){
        $d = array();
        $sql = "SELECT * FROM cat_capacidadBUTU where status=1";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }
	public function getCapacidadHP(){
        $d = array();
        $sql = "SELECT * FROM cat_capacidadHP where status=1";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }
	
	public function getOrdenesTrabajo(){
        $d = array();
		//$sql = "SELECT * FROM cat_capacidadHP where status=1";
        $sql = "SELECT *, TIME(fecha_agendada) AS hora FROM orden_trabajo WHERE DATE(fecha_agendada) = CURRENT_DATE() and where status = 2 GROUP BY fecha_agendada";
		return $sql;
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }
}