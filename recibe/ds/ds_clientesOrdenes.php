<?php

include_once __DIR__ . '/datasource.inc.php';

$c = 0;
$d = array();
$o = new DataSource();
$s = new DataSource();
foreach ($o->getOrdenesTrabajo() as $cliente){
    $f = $s->getClienteByOrden($cliente['id_cliente']);
    if(sizeof($f) > 0){
        foreach ($f as $ele) {
            $d[$c]['id'] = $ele['id'];
            $d[$c]['nombre'] = $ele['nombre'];
            $d[$c]['direccion'] = $ele['direccion'];
            $d[$c]['tipo_cliente'] = $ele['tipo_cliente'];
            $c++;
        }
    }
    else{
        $d[$c]['id'] = 0;
        $d[$c]['nombre'] = "No tiene nombre";
        $d[$c]['direccion'] = "No tiene direccion";
        $d[$c]['tipo_cliente'] = "No tiene tipo_cliente";
        $c++;
    }
}
echo json_encode($d, true);
/*
echo '<pre>';
print_r($d);
echo '</pre>';
*/