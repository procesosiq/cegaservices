<?php

include_once __DIR__ . '/datasource.inc.php';

$c = 0;
$d = array();
$o = new DataSource();
$s = new DataSource();
foreach ($o->getOrdenesTrabajo() as $orden){
	//echo $orden;
    $f = $s->getMaterialesByOrden($orden['id']);
	//echo $f;
    if(sizeof($f) > 0){
        foreach ($f as $ele) {
            $d[$c]['id'] = $ele['id'];
            $d[$c]['id_orden'] = $ele['id_orden'];
            $d[$c]['codigo'] = $ele['codigo'];
            $d[$c]['seleccion'] = $ele['seleccion'];
            $d[$c]['descripcion'] = $ele['descripcion'];
            $d[$c]['unidad'] = $ele['unidad'];
            $d[$c]['cantidad'] = $ele['cantidad_requerida'];
            $c++;
        }
    }
    else{
        $d[$c]['id'] = 0;
        $d[$c]['materiales'] = "No tiene tipos de materiales";
        $c++;
    }
}
echo json_encode($d, true);
/*
echo '<pre>';
print_r($d);
echo '</pre>';
*/