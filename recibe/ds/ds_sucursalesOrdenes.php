<?php

include_once __DIR__ . '/datasource.inc.php';

$c = 0;
$d = array();
$o = new DataSource();
$s = new DataSource();
foreach ($o->getOrdenesTrabajo() as $cliente){
    $f = $s->getSucursalByCliente($cliente['id_sucursal']);
    if(sizeof($f) > 0){
        foreach ($f as $ele) {
            $d[$c]['id'] = $ele['id'];
            $d[$c]['id_cliente'] = $ele['id_cliente'];
            $d[$c]['nombre_contacto'] = $ele['nombre_contacto'];
            $c++;
        }
    }
    else{
        $d[$c]['id'] = 0;
        $d[$c]['id_cliente'] = $cliente['id_sucursal'];
        $d[$c]['nombre_contacto'] = "No tiene sucusales";
        $c++;
    }
}
echo json_encode($d, true);
/*
echo '<pre>';
print_r($d);
echo '</pre>';
*/