<?php

include_once __DIR__ . '/datasource.inc.php';

$c = 0;
$d = array();
$o = new DataSource();
$s = new DataSource();
foreach ($o->getOrdenesTrabajo() as $orden){
	//echo $orden;
    $f = $s->getrepuestosByOrden($orden['id']);
	//echo $f;
    if(sizeof($f) > 0){
        foreach ($f as $ele) {
            $d[$c]['id'] = $ele['id'];
            $d[$c]['id_orden'] = $ele['id_orden'];
            $d[$c]['codigo'] = $ele['codigo'];
            $d[$c]['parte'] = $ele['parte'];
            $d[$c]['descripcion'] = $ele['descripcion'];
            $d[$c]['item'] = $ele['item'];
            $d[$c]['cantidad'] = $ele['cantidad'];
            $c++;
        }
    }
    else{
        $d[$c]['id'] = 0;
        $d[$c]['repuesto'] = "No tiene repuestos";
        $c++;
    }
}
echo json_encode($d, true);
/*
echo '<pre>';
print_r($d);
echo '</pre>';
*/