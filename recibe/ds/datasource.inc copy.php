<?php

class DataSource
{
    private $mysqli;

    function __construct(){
        $this->mysqli = @new mysqli("localhost", "auditoriasbonita", "u[V(fTIUbcVb", "cegaservices2");
        if (mysqli_connect_errno()) {
            printf("Falló la conexión: %s\n", mysqli_connect_error());
            exit();
        }
        $this->mysqli->set_charset("utf8");
    }

    public function tiposTrabajosOrden(){
    	$sql = "
    	SELECT id AS id_orden, TRIM(SUBSTRING_INDEX(SUBSTRING_INDEX(t.tipo_trabajo, ',', n.n), ',', -1)) VALUE, CONCAT(TRIM(SUBSTRING_INDEX(SUBSTRING_INDEX(t.tipo_trabajo, ',', n.n), ',', -1)), ' - ' , id) AS tipo_id_orden
		FROM orden_trabajo t CROSS JOIN 
		(
		   SELECT a.N + b.N * 10 + 1 n
		     FROM 
		    (SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) a
		   ,(SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) b
		    ORDER BY n
		) n
		WHERE n.n <= 1 + (LENGTH(t.tipo_trabajo) - LENGTH(REPLACE(t.tipo_trabajo, ',', ''))) AND DATE(fecha_agendada) = CURRENT_DATE
		ORDER BY id";

		$d = array();
		$res = $this->mysqli->query($sql);
		while($row = $res->fetch_assoc()){
			$d[] = $row;
		}
		return $d;
    }

    public function getSucursales(){
    	$sql = "
    	SELECT 
			IF(cat_sucursales.id IS NULL, 0, cat_sucursales.id) AS id, 
			cat_clientes.id AS id_cliente, 
			IF(cat_sucursales.nombre_contacto IS NULL, 'SIN INFORMACIÓN', cat_sucursales.`nombre_contacto`) AS nombre_contacto, 
			#IF(cat_sucursales.id_tipcli IS NULL, 'SIN INFORMACIÓN', cat_sucursales.`id_tipcli`) AS id_tipcli,
			IF((SELECT COUNT(*) FROM cat_sucursales WHERE id_cliente = cat_clientes.id) > 0, cat_sucursales.razon_social, 'NO TIENE SUCURSALES') AS razon_social,
			#IF(cat_sucursales.ruc IS NULL, 'N/A', cat_sucursales.ruc) AS ruc,
			IF(cat_sucursales.direccion IS NULL, cat_clientes.direccion, cat_sucursales.direccion) AS direccion
			#IF(cat_sucursales.telefono IS NULL, 'N/A', cat_sucursales.telefono) AS telefono,
			#IF(cat_sucursales.`ciudad` IS NULL, 'N/A', cat_sucursales.`ciudad`) AS ciudad,
			#IF(cat_sucursales.`fecha` IS NULL, 'N/A', cat_sucursales.`fecha`) AS fecha,
			#IF(cat_sucursales.id_usuario IS NULL, 'N/A',cat_sucursales.id_usuario) AS id_usuario,
			#IF(cat_sucursales.status IS NULL, 'N/A', cat_sucursales.status) AS status
		FROM cat_clientes 
		LEFT JOIN cat_sucursales ON id_cliente = cat_clientes.id;";
    	$resultado = $this->mysqli->query($sql);
    	while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }

    public function getClientes(){
        $d = array();
        $sql = "SELECT 
            cat_clientes.id, 
            cat_clientes.nombre , 
            cat_clientesTipo.nombre AS tipo_cliente, 
            cat_clientes.id_tipcli AS id_tipo_cliente, 
            cat_clientes.direccion, 
            CONCAT(orden_trabajo.id, '-', cat_clientes.id) AS id_orden_cliente,
            IF(orden_trabajo.direccion IS NULL, 'SIN INFORMACIÓN', orden_trabajo.direccion) AS direccion_orden, 
            IF(cat_sucursales.`direccion` IS NULL, cat_clientes.direccion, cat_sucursales.`direccion`) AS direccion_sucursal,
            IF(referencia IS NULL, 'SIN INFORMACIÓN', referencia) AS referencia,  
            IF(id_sucursal IS NULL, 'SIN INFORMACIÓN', id_sucursal) AS id_sucursal, 
            IF(cat_sucursales.razon_social IS NULL, 'NO TIENE SUCURSALES', cat_sucursales.razon_social) AS razon_social, 
            IF(cat_clientes.nombre IS NULL, 'SIN INFORMACIÓN', cat_clientes.nombre) AS nombre_contacto, 
            IF(cat_clientes.email IS NULL, 'SIN INFORMACIÓN', cat_clientes.email) AS email_cliente, 
            IF(cat_sucursales.telefono IS NULL, '',cat_sucursales.telefono) AS telefono_sucursal,
            IF(cat_clientes.telefono IS NULL, 'SIN INFORMACIÓN', cat_clientes.telefono) AS telefono_cliente, 
            IF(cat_sucursales.direccion IS NULL OR cat_sucursales.direccion = ' ', 'SIN INFORMACIÓN', cat_sucursales.direccion) AS direccion, 
            orden_trabajo.id as id_orden_trabajo, orden_trabajo.observaciones AS observaciones, tipo_trabajo, 
            CONCAT(TIME(fecha_agendada), DATE_FORMAT(fecha_agendada, ' %p')) AS fecha_agendada, grupo_trabajo 
                FROM cat_clientes 
                LEFT JOIN cotizaciones ON cotizaciones.id_cliente = cat_clientes.id 
                LEFT JOIN orden_trabajo ON id_cotizacion = cotizaciones.id 
                LEFT JOIN cat_sucursales ON id_sucursal = cat_sucursales.id 
                LEFT JOIN cat_clientesTipo ON tipo_cliente = cat_clientesTipo.id 
                WHERE cat_clientes.id_usuario = 2 AND cat_clientes.status = 1 AND DATE(fecha_agendada) = DATE(CURRENT_TIMESTAMP);";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
        	$row["observaciones"] = strip_tags($row["observaciones"]);
            $d[] = $row;
        }
        return $d;
    }

    public function getClientesCorrectivos(){
        $d = array();
        $sql = "SELECT 
        	cat_clientes.id, 
        	cat_clientes.nombre , 
        	cat_clientesTipo.nombre AS tipo_cliente, 
        	cat_clientes.id_tipcli AS id_tipo_cliente, 
        	cat_clientes.direccion, 
        	CONCAT(orden_trabajo.id, '-', cat_clientes.id) AS id_orden_cliente,
        	IF(orden_trabajo.direccion IS NULL, 'SIN INFORMACIÓN', orden_trabajo.direccion) AS direccion_orden, 
        	IF(cat_sucursales.`direccion` IS NULL, cat_clientes.direccion, cat_sucursales.`direccion`) AS direccion_sucursal,
	    	IF(referencia IS NULL, 'SIN INFORMACIÓN', referencia) AS referencia,  
	    	IF(id_sucursal IS NULL, 'SIN INFORMACIÓN', id_sucursal) AS id_sucursal, 
	    	IF(cat_sucursales.razon_social IS NULL, 'NO TIENE SUCURSALES', cat_sucursales.razon_social) AS razon_social, 
	    	IF(cat_clientes.nombre IS NULL, 'SIN INFORMACIÓN', cat_clientes.nombre) AS nombre_contacto, 
	    	IF(cat_clientes.email IS NULL, 'SIN INFORMACIÓN', cat_clientes.email) AS email_cliente, 
	    	IF(cat_sucursales.telefono IS NULL, '',cat_sucursales.telefono) AS telefono_sucursal,
	    	IF(cat_clientes.telefono IS NULL, 'SIN INFORMACIÓN', cat_clientes.telefono) AS telefono_cliente, 
	    	IF(cat_sucursales.direccion IS NULL OR cat_sucursales.direccion = ' ', 'SIN INFORMACIÓN', cat_sucursales.direccion) AS direccion, 
	    	orden_trabajo.id as id_orden_trabajo, orden_trabajo.observaciones AS observaciones, tipo_trabajo, 
	    	CONCAT(TIME(fecha_agendada), DATE_FORMAT(fecha_agendada, ' %p')) AS fecha_agendada, grupo_trabajo 
                FROM cat_clientes 
                LEFT JOIN cotizaciones ON cotizaciones.id_cliente = cat_clientes.id 
                LEFT JOIN orden_trabajo ON id_cotizacion = cotizaciones.id 	
                LEFT JOIN cat_sucursales ON cat_sucursales.id_cliente = cat_clientes.id  
                LEFT JOIN cat_clientesTipo ON tipo_cliente = cat_clientesTipo.id 
                WHERE cat_clientes.id_usuario = 2 AND cat_clientes.status = 1 AND DATE(fecha_agendada) = DATE(CURRENT_TIMESTAMP) AND tipo_trabajo LIKE '%CORRECTIVO%'";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
        	$row["observaciones"] = strip_tags($row["observaciones"]);
            $d[] = $row;
        }
        return $d;
    }

    public function getClientesInstalacion(){
        $d = array();
        $sql = "SELECT 
        	cat_clientes.id, 
        	cat_clientes.nombre , 
        	cat_clientesTipo.nombre AS tipo_cliente, 
        	cat_clientes.id_tipcli AS id_tipo_cliente, 
        	cat_clientes.direccion, 
        	CONCAT(orden_trabajo.id, '-', cat_clientes.id) AS id_orden_cliente,
        	IF(orden_trabajo.direccion IS NULL, 'SIN INFORMACIÓN', orden_trabajo.direccion) AS direccion_orden, 
        	IF(cat_sucursales.`direccion` IS NULL, cat_clientes.direccion, cat_sucursales.`direccion`) AS direccion_sucursal,
	    	IF(referencia IS NULL, 'SIN INFORMACIÓN', referencia) AS referencia,  
	    	IF(id_sucursal IS NULL, 'SIN INFORMACIÓN', id_sucursal) AS id_sucursal, 
	    	IF(cat_sucursales.razon_social IS NULL, 'NO TIENE SUCURSALES', cat_sucursales.razon_social) AS razon_social, 
	    	IF(cat_clientes.nombre IS NULL, 'SIN INFORMACIÓN', cat_clientes.nombre) AS nombre_contacto, 
	    	IF(cat_clientes.email IS NULL, 'SIN INFORMACIÓN', cat_clientes.email) AS email_cliente, 
	    	IF(cat_sucursales.telefono IS NULL, '',cat_sucursales.telefono) AS telefono_sucursal,
	    	IF(cat_clientes.telefono IS NULL, 'SIN INFORMACIÓN', cat_clientes.telefono) AS telefono_cliente, 
	    	IF(cat_sucursales.direccion IS NULL OR cat_sucursales.direccion = ' ', 'SIN INFORMACIÓN', cat_sucursales.direccion) AS direccion, 
	    	orden_trabajo.id as id_orden_trabajo, orden_trabajo.observaciones AS observaciones, tipo_trabajo, 
	    	CONCAT(TIME(fecha_agendada), DATE_FORMAT(fecha_agendada, ' %p')) AS fecha_agendada, grupo_trabajo 
                FROM cat_clientes 
                LEFT JOIN cotizaciones ON cotizaciones.id_cliente = cat_clientes.id 
                LEFT JOIN orden_trabajo ON id_cotizacion = cotizaciones.id 	
                LEFT JOIN cat_sucursales ON cat_sucursales.id_cliente = cat_clientes.id  
                LEFT JOIN cat_clientesTipo ON tipo_cliente = cat_clientesTipo.id 
                WHERE cat_clientes.id_usuario = 2 AND cat_clientes.status = 1 AND DATE(fecha_agendada) = DATE(CURRENT_TIMESTAMP) AND tipo_trabajo LIKE '%INSTALACION%'";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
        	$row["observaciones"] = strip_tags($row["observaciones"]);
            $d[] = $row;
        }
        return $d;
    }

    public function getClientesMantenimiento(){
        $d = array();
        $sql = "SELECT 
        	cat_clientes.id, 
        	cat_clientes.nombre , 
        	cat_clientesTipo.nombre AS tipo_cliente, 
        	cat_clientes.id_tipcli AS id_tipo_cliente, 
        	cat_clientes.direccion, 
        	CONCAT(orden_trabajo.id, '-', cat_clientes.id) AS id_orden_cliente,
        	IF(orden_trabajo.direccion IS NULL, 'SIN INFORMACIÓN', orden_trabajo.direccion) AS direccion_orden, 
        	IF(cat_sucursales.`direccion` IS NULL, cat_clientes.direccion, cat_sucursales.`direccion`) AS direccion_sucursal,
	    	IF(referencia IS NULL, 'SIN INFORMACIÓN', referencia) AS referencia,  
	    	IF(id_sucursal IS NULL, 'SIN INFORMACIÓN', id_sucursal) AS id_sucursal, 
	    	IF(cat_sucursales.razon_social IS NULL, 'NO TIENE SUCURSALES', cat_sucursales.razon_social) AS razon_social, 
	    	IF(cat_clientes.nombre IS NULL, 'SIN INFORMACIÓN', cat_clientes.nombre) AS nombre_contacto, 
	    	IF(cat_clientes.email IS NULL, 'SIN INFORMACIÓN', cat_clientes.email) AS email_cliente, 
	    	IF(cat_sucursales.telefono IS NULL, '',cat_sucursales.telefono) AS telefono_sucursal,
	    	IF(cat_clientes.telefono IS NULL, 'SIN INFORMACIÓN', cat_clientes.telefono) AS telefono_cliente, 
	    	IF(cat_sucursales.direccion IS NULL OR cat_sucursales.direccion = ' ', 'SIN INFORMACIÓN', cat_sucursales.direccion) AS direccion, 
	    	orden_trabajo.id as id_orden_trabajo, orden_trabajo.observaciones AS observaciones, 
	    	tipo_trabajo, 
	    	CONCAT(TIME(fecha_agendada), DATE_FORMAT(fecha_agendada, ' %p')) AS fecha_agendada, 
	    	grupo_trabajo 
                FROM cat_clientes 
                LEFT JOIN cotizaciones ON cotizaciones.id_cliente = cat_clientes.id 
                LEFT JOIN orden_trabajo ON id_cotizacion = cotizaciones.id 	
                LEFT JOIN cat_sucursales ON cat_sucursales.id_cliente = cat_clientes.id  
                LEFT JOIN cat_clientesTipo ON tipo_cliente = cat_clientesTipo.id 
                WHERE cat_clientes.id_usuario = 2 AND cat_clientes.status = 1 AND DATE(fecha_agendada) = DATE(CURRENT_TIMESTAMP) AND tipo_trabajo LIKE '%MANTENIMIENTO%'";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
        	$row["observaciones"] = strip_tags($row["observaciones"]);
            $d[] = $row;
        }
        return $d;
    }

    public function getListadoEquipos(){
        $d = array();
        $sql = "
#mantenimiento
(SELECT 'MANTENIMIENTO' AS tipo_trabajo, CONCAT('MANTENIMIENTO - ', id_orden) AS tipo_id_orden, CONCAT(man_caracteristicas_climatizacion_a.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.`nombre`,' - ',man_caracteristicas_climatizacion_a.codigo,' - ',IF(marca IS NULL, '', marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM man_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE man_caracteristicas_climatizacion_a.status = 1 HAVING reg IS NOT NULL) 
UNION 
(SELECT 'MANTENIMIENTO' AS tipo_trabajo, CONCAT('MANTENIMIENTO - ', id_orden) AS tipo_id_orden, CONCAT(man_caracteristicas_climatizacion_b.`codigo`, '-', id_cliente) AS codigo_cliente,  id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.`nombre`,' - ',man_caracteristicas_climatizacion_b.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM man_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE man_caracteristicas_climatizacion_b.status = 1 HAVING reg IS NOT NULL)
UNION
(SELECT 'MANTENIMIENTO' AS tipo_trabajo, CONCAT('MANTENIMIENTO - ', id_orden) AS tipo_id_orden, CONCAT(man_caracteristicas_refrigeracion.`codigo`, '-', id_cliente) AS codigo_cliente,  id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.`nombre`,' - ',man_caracteristicas_refrigeracion.codigo,' - ',IF(marcaEva IS NULL,'',marcaEva),' - ',IF(capaciadadEva IS NULL,'',capaciadadEva),' - ',IF(modeloEva IS NULL,'',modeloEva),' - ',IF(serieEva IS NULL,'',serieEva)) AS reg FROM man_caracteristicas_refrigeracion INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE man_caracteristicas_refrigeracion.status = 1 HAVING reg IS NOT NULL)
UNION
(SELECT 'MANTENIMIENTO' AS tipo_trabajo, CONCAT('MANTENIMIENTO - ', id_orden) AS tipo_id_orden, CONCAT(man_caracteristicas_ventilacion.`codigo`, '-', id_cliente) AS codigo_cliente,  id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.`nombre`,' - ',man_caracteristicas_ventilacion.codigo,' - ',IF(marca,'',marca),' - ',IF(capacidadHP IS NULL,'',capacidadHP),' - ',IF(capacidadCFN IS NULL,'',capacidadCFN),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM man_caracteristicas_ventilacion INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE man_caracteristicas_ventilacion.status = 1 HAVING reg IS NOT NULL)

#instalacion
UNION
(SELECT 'INSTALACION' AS tipo_trabajo, CONCAT('INSTALACION - ', id_orden) AS tipo_id_orden, CONCAT(ins_caracteristicas_climatizacion_a.`codigo`, '-', id_cliente) AS codigo_cliente,  id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.`nombre`,' - ',ins_caracteristicas_climatizacion_a.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie,'',serie)) AS reg FROM ins_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE ins_caracteristicas_climatizacion_a.status = 1 HAVING reg IS NOT NULL) 
UNION 
(SELECT 'INSTALACION' AS tipo_trabajo, CONCAT('INSTALACION - ', id_orden) AS tipo_id_orden,  CONCAT(ins_caracteristicas_climatizacion_b.`codigo`, '-', id_cliente) AS codigo_cliente,  id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.`nombre`,' - ',ins_caracteristicas_climatizacion_b.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM ins_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE ins_caracteristicas_climatizacion_b.status = 1 HAVING reg IS NOT NULL)
UNION
(SELECT 'INSTALACION' AS tipo_trabajo, CONCAT('INSTALACION - ', id_orden) AS tipo_id_orden,  CONCAT(ins_caracteristicas_refrigeracion.`codigo`, '-', id_cliente) AS codigo_cliente,  id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.`nombre`,' - ',ins_caracteristicas_refrigeracion.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie),' - ',IF(compresorBTU IS NULL,'',compresorBTU),' - ',IF(compresorCantidad IS NULL,'',compresorCantidad),' - ',IF(ventiladorCantidad IS NULL,'',ventiladorCantidad)) AS reg FROM ins_caracteristicas_refrigeracion INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE ins_caracteristicas_refrigeracion.status = 1 HAVING reg IS NOT NULL)
UNION
(SELECT 'INSTALACION' AS tipo_trabajo, CONCAT('INSTALACION - ', id_orden) AS tipo_id_orden,  CONCAT(ins_caracteristicas_ventilacion.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.`nombre`,' - ',ins_caracteristicas_ventilacion.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadHP IS NULL,'',capacidadHP),' - ',IF(capacidadCFN IS NULL,'',capacidadCFN),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM ins_caracteristicas_ventilacion INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE ins_caracteristicas_ventilacion.status = 1 HAVING reg IS NOT NULL)

#correctivo
UNION
(SELECT 'CORRECTIVO' AS tipo_trabajo, CONCAT('CORRECTIVO - ', id_orden) AS tipo_id_orden, CONCAT(cor_caracteristicas_climatizacion_a.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.nombre,' - ',cor_caracteristicas_climatizacion_a.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM cor_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE cor_caracteristicas_climatizacion_a.status = 1 HAVING reg IS NOT NULL) 
UNION 
(SELECT 'CORRECTIVO' AS tipo_trabajo, CONCAT('CORRECTIVO - ', id_orden) AS tipo_id_orden, CONCAT(cor_caracteristicas_climatizacion_b.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.nombre,' - ',cor_caracteristicas_climatizacion_b.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM cor_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE cor_caracteristicas_climatizacion_b.status = 1 HAVING reg IS NOT NULL)
UNION
(SELECT 'CORRECTIVO' AS tipo_trabajo, CONCAT('CORRECTIVO - ', id_orden) AS tipo_id_orden, CONCAT(cor_caracteristicas_refrigeracion.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.nombre,' - ',cor_caracteristicas_refrigeracion.`codigo`,' - ',IF(marca IS NULL,'',marca),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie),' - ',IF(compresorBTU IS NULL,'',compresorBTU),' - ',IF(compresorCantidad IS NULL,'',compresorCantidad),' - ',IF(ventiladorCantidad IS NULL,'',ventiladorCantidad)) AS reg FROM cor_caracteristicas_refrigeracion INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE cor_caracteristicas_refrigeracion.status = 1 HAVING reg IS NOT NULL)
UNION
(SELECT 'CORRECTIVO' AS tipo_trabajo, CONCAT('CORRECTIVO - ', id_orden) AS tipo_id_orden, CONCAT(cor_caracteristicas_ventilacion.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.nombre,' - ',cor_caracteristicas_ventilacion.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadHP IS NULL,'',capacidadHP),' - ',IF(capacidadCFN IS NULL,'',capacidadHP),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM cor_caracteristicas_ventilacion INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE cor_caracteristicas_ventilacion.status = 1 HAVING reg IS NOT NULL)
        ";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }

    public function getListadoEquiposVentilacion(){
    	$d = array();
        $sql = "
#mantenimiento
(SELECT  man_caracteristicas_ventilacion.id, nombre AS descripcion, codigo, marca, capacidadHP, modelo, serie, id_orden FROM man_caracteristicas_ventilacion INNER JOIN cat_descripciones_equipos)

#instalacion
UNION
(SELECT  ins_caracteristicas_ventilacion.id, nombre AS descripcion, codigo, marca, capacidadHP, modelo, serie, id_orden FROM ins_caracteristicas_ventilacion INNER JOIN cat_descripciones_equipos)

#correctivo
UNION
(SELECT  cor_caracteristicas_ventilacion.id, nombre AS descripcion, codigo, marca, capacidadHP, modelo, serie, id_orden FROM cor_caracteristicas_ventilacion INNER JOIN cat_descripciones_equipos)
        ";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }

    public function getListadoEquiposRefrigeracion(){
    	$d = array();
        $sql = "
#mantenimiento
(SELECT  id_orden, CONCAT(cat_areas.nombre,' - ',descripcion,' - ',codigo,' - ',IF(marcaEva IS NULL,'',marcaEva),' - ',IF(capaciadadEva IS NULL,'',capaciadadEva),' - ',IF(modeloEva IS NULL,'',modeloEva),' - ',IF(serieEva IS NULL,'',serieEva)) AS reg FROM man_caracteristicas_refrigeracion INNER JOIN cat_areas ON AREA = cat_areas.id HAVING reg IS NOT NULL)

#instalacion
UNION
(SELECT  id_orden, CONCAT(cat_areas.nombre,' - ',descripcion,' - ',codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie),' - ',IF(compresorBTU IS NULL,'',compresorBTU),' - ',IF(compresorCantidad IS NULL,'',compresorCantidad),' - ',IF(ventiladorCantidad IS NULL,'',ventiladorCantidad)) AS reg FROM ins_caracteristicas_refrigeracion INNER JOIN cat_areas ON AREA = cat_areas.id HAVING reg IS NOT NULL)

#correctivo
UNION
(SELECT  id_orden, CONCAT(cat_areas.nombre,' - ',descripcion,' - ',codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie),' - ',IF(compresorBTU IS NULL,'',compresorBTU),' - ',IF(compresorCantidad IS NULL,'',compresorCantidad),' - ',IF(ventiladorCantidad IS NULL,'',ventiladorCantidad)) AS reg FROM cor_caracteristicas_refrigeracion INNER JOIN cat_areas ON AREA = cat_areas.id HAVING reg IS NOT NULL)
        ";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }

    public function getListadoEquiposRefrigeracionDetalleInstalacion(){
    	$d = array();
        $sql = "
		SELECT  codigo, descripcion, id, marca, modelo, serie, id_orden FROM ins_caracteristicas_refrigeracion INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.id
        ";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }

    public function getListadoEquiposClimatizacionB(){
    	$d = array();
        $sql = "
#mantenimiento
(SELECT  id_orden, CONCAT(cat_areas.`nombre`,' - ',descripcion,' - ',codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM man_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA HAVING reg IS NOT NULL) 

#instalacion
UNION
(SELECT  id_orden, CONCAT(cat_areas.`nombre`,' - ',descripcion,' - ',codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM ins_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA HAVING reg IS NOT NULL)

#correctivo
UNION
(SELECT  id_orden, CONCAT(cat_areas.`nombre`,' - ',descripcion,' - ',codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM cor_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA HAVING reg IS NOT NULL)
        ";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }

    public function getListadoEquiposClimatizacionBDetalleCorrectivo(){
    	$d = array();
        $sql = "
        #correctivo
		(SELECT cor_caracteristicas_climatizacion_b.id, id_orden, cat_equipos.id_cliente, nombre_area, cat_areas.id AS id_area, cat_areas.`nombre` AS AREA, cat_descripciones_equipos.nombre AS descripcion, cor_caracteristicas_climatizacion_b.codigo, evaporador, marca, CAST(capacidadBTU AS DECIMAL) AS capacidadBTU, modelo, serie, condensador, condensadorMarca, CAST(condensadorBTU AS DECIMAL) AS condensadorBTU, condensadorModelo, condensadorSerie FROM cor_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id INNER JOIN cat_equipos ON cor_caracteristicas_climatizacion_b.codigo = cat_equipos.codigo WHERE DATE(fecha_agendada) = CURRENT_DATE AND cotizaciones.id_cliente = cat_equipos.`id_cliente`)
        ";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }

    public function getListadoEquiposClimatizacionBDetalleInstalacion(){
    	$d = array();
        $sql = "
		#instalacion
		(SELECT ins_caracteristicas_climatizacion_b.id, id_orden, cat_equipos.id_cliente, nombre_area, cat_areas.id AS id_area, cat_areas.`nombre` AS AREA, cat_descripciones_equipos.nombre AS descripcion, ins_caracteristicas_climatizacion_b.codigo, evaporador, marca, CAST(capacidadBTU AS DECIMAL) AS capacidadBTU, modelo, serie, condensador, condensadorMarca, CAST(condensadorBTU AS DECIMAL) AS condensadorBTU, condensadorModelo, condensadorSerie FROM ins_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id INNER JOIN cat_equipos ON ins_caracteristicas_climatizacion_b.codigo = cat_equipos.codigo WHERE DATE(fecha_agendada) = CURRENT_DATE AND cotizaciones.id_cliente = cat_equipos.`id_cliente`)
        ";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }

    public function getListadoEquiposClimatizacionBDetalleMantenimiento(){
    	$d = array();
        $sql = "
		#mantenimiento
		(SELECT man_caracteristicas_climatizacion_b.id, id_orden, cat_equipos.id_cliente, nombre_area, cat_areas.id AS id_area, cat_areas.`nombre` AS AREA, cat_descripciones_equipos.nombre AS descripcion, man_caracteristicas_climatizacion_b.codigo, evaporador, marca, CAST(capacidadBTU AS DECIMAL) AS capacidadBTU, modelo, serie, condensador, condensadorMarca, CAST(condensadorBTU AS DECIMAL) AS condensadorBTU, condensadorModelo, condensadorSerie FROM man_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id INNER JOIN cat_equipos ON man_caracteristicas_climatizacion_b.codigo = cat_equipos.codigo WHERE DATE(fecha_agendada) = CURRENT_DATE AND cotizaciones.id_cliente = cat_equipos.`id_cliente`)
        ";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }

    public function getListadoEquiposClimatizacionBDetalle(){
    	$d = array();
        $sql = "
#mantenimiento
(SELECT man_caracteristicas_climatizacion_b.id, id_orden, cat_equipos.id_cliente, nombre_area, cat_areas.id AS id_area, cat_areas.`nombre` AS AREA, cat_descripciones_equipos.nombre AS descripcion, man_caracteristicas_climatizacion_b.codigo, evaporador, marca, CAST(capacidadBTU AS DECIMAL) AS capacidadBTU, modelo, serie, condensador, condensadorMarca, CAST(condensadorBTU AS DECIMAL) AS condensadorBTU, condensadorModelo, condensadorSerie FROM man_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id INNER JOIN cat_equipos ON man_caracteristicas_climatizacion_b.codigo = cat_equipos.codigo WHERE DATE(fecha_agendada) = CURRENT_DATE) 

#instalacion
UNION
(SELECT ins_caracteristicas_climatizacion_b.id, id_orden, cat_equipos.id_cliente, nombre_area, cat_areas.id AS id_area, cat_areas.`nombre` AS AREA, cat_descripciones_equipos.nombre AS descripcion, ins_caracteristicas_climatizacion_b.codigo, evaporador, marca, CAST(capacidadBTU AS DECIMAL) AS capacidadBTU, modelo, serie, condensador, condensadorMarca, CAST(condensadorBTU AS DECIMAL) AS condensadorBTU, condensadorModelo, condensadorSerie FROM ins_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id INNER JOIN cat_equipos ON ins_caracteristicas_climatizacion_b.codigo = cat_equipos.codigo WHERE DATE(fecha_agendada) = CURRENT_DATE)

#correctivo
UNION
(SELECT cor_caracteristicas_climatizacion_b.id, id_orden, cat_equipos.id_cliente, nombre_area, cat_areas.id AS id_area, cat_areas.`nombre` AS AREA, cat_descripciones_equipos.nombre AS descripcion, cor_caracteristicas_climatizacion_b.codigo, evaporador, marca, CAST(capacidadBTU AS DECIMAL) AS capacidadBTU, modelo, serie, condensador, condensadorMarca, CAST(condensadorBTU AS DECIMAL) AS condensadorBTU, condensadorModelo, condensadorSerie FROM cor_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id INNER JOIN cat_equipos ON cor_caracteristicas_climatizacion_b.codigo = cat_equipos.codigo WHERE DATE(fecha_agendada) = CURRENT_DATE)
        ";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }

    public function getListadoEquiposClimatizacionA(){
    	$d = array();
        $sql = "
#mantenimiento
(SELECT id_orden, CONCAT(cat_areas.`nombre`,' - ',descripcion,' - ',codigo,' - ',marca,' - ',capacidadBTU,' - ',modelo,' - ',serie) AS reg FROM man_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id HAVING reg IS NOT NULL) 

#instalacion
UNION
(SELECT  id_orden, CONCAT(cat_areas.`nombre`,' - ',descripcion,' - ',codigo,' - ',marca,' - ',capacidadBTU,' - ',modelo,' - ',serie) AS reg FROM ins_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id HAVING reg IS NOT NULL) 

#correctivo
UNION
(SELECT  id_orden, CONCAT(cat_areas.`nombre`,' - ',descripcion,' - ',codigo,' - ',marca,' - ',capacidadBTU,' - ',modelo,' - ',serie) AS reg FROM cor_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id HAVING reg IS NOT NULL) 
        ";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }

    public function getListadoEquiposClimatizacionADetalle(){
    	$d = array();
        $sql = "
#mantenimiento
(SELECT man_caracteristicas_climatizacion_a.id, cat_areas.id AS id_area, cat_equipos.id_cliente, nombre_area, cat_areas.`nombre` AS AREA, id_orden, cat_descripciones_equipos.nombre AS descripcion, cat_equipos.codigo, marca, CAST(capacidadBTU AS DECIMAL) AS capacidadBTU, modelo, serie FROM man_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id INNER JOIN cat_equipos ON man_caracteristicas_climatizacion_a.codigo = cat_equipos.codigo WHERE DATE(fecha_agendada) = CURRENT_DATE)  

#instalacion
UNION
(SELECT ins_caracteristicas_climatizacion_a.id, cat_areas.id AS id_area, cat_equipos.id_cliente, nombre_area, cat_areas.`nombre` AS AREA, id_orden, cat_descripciones_equipos.nombre AS descripcion, cat_equipos.codigo, marca, CAST(capacidadBTU AS DECIMAL) AS capacidadBTU, modelo, serie FROM ins_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id INNER JOIN cat_equipos ON ins_caracteristicas_climatizacion_a.codigo = cat_equipos.codigo WHERE DATE(fecha_agendada) = CURRENT_DATE) 

#correctivo
UNION
(SELECT cor_caracteristicas_climatizacion_a.id, cat_areas.id AS id_area, cat_equipos.id_cliente, nombre_area, cat_areas.`nombre` AS AREA, id_orden, cat_descripciones_equipos.nombre AS descripcion, cat_equipos.codigo, marca, CAST(capacidadBTU AS DECIMAL) AS capacidadBTU, modelo, serie FROM cor_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id INNER JOIN cat_equipos ON cor_caracteristicas_climatizacion_a.codigo = cat_equipos.codigo WHERE DATE(fecha_agendada) = CURRENT_DATE)
";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }

    public function getListadoEquiposClimatizacionADetalleMantenimiento(){
    	$d = array();
        $sql = "
#mantenimiento
(SELECT man_caracteristicas_climatizacion_a.id, cat_areas.id AS id_area, cat_equipos.id_cliente, nombre_area, cat_areas.`nombre` AS AREA, id_orden, cat_descripciones_equipos.nombre AS descripcion, cat_equipos.codigo, marca, CAST(capacidadBTU AS DECIMAL) AS capacidadBTU, modelo, serie FROM man_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id INNER JOIN cat_equipos ON man_caracteristicas_climatizacion_a.codigo = cat_equipos.codigo WHERE DATE(fecha_agendada) = CURRENT_DATE AND cotizaciones.id_cliente = cat_equipos.`id_cliente`)
";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }

    public function getListadoEquiposClimatizacionADetalleInstalacion(){
    	$d = array();
        $sql = "
#instalacion
(SELECT ins_caracteristicas_climatizacion_a.id, cat_areas.id AS id_area, cat_equipos.id_cliente, nombre_area, cat_areas.`nombre` AS AREA, id_orden, cat_descripciones_equipos.nombre AS descripcion, cat_equipos.codigo, marca, CAST(capacidadBTU AS DECIMAL) AS capacidadBTU, modelo, serie FROM ins_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id INNER JOIN cat_equipos ON ins_caracteristicas_climatizacion_a.codigo = cat_equipos.codigo WHERE DATE(fecha_agendada) = CURRENT_DATE AND cotizaciones.id_cliente = cat_equipos.`id_cliente`)
";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }

    public function getListadoEquiposClimatizacionADetalleCorrectivo(){
    	$d = array();
        $sql = "
#correctivo
(SELECT cor_caracteristicas_climatizacion_a.id, cat_areas.id AS id_area, cat_equipos.id_cliente, nombre_area, cat_areas.`nombre` AS AREA, id_orden, cat_descripciones_equipos.nombre AS descripcion, cat_equipos.codigo, marca, CAST(capacidadBTU AS DECIMAL) AS capacidadBTU, modelo, serie FROM cor_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id INNER JOIN cat_equipos ON cor_caracteristicas_climatizacion_a.codigo = cat_equipos.codigo WHERE DATE(fecha_agendada) = CURRENT_DATE AND cotizaciones.id_cliente = cat_equipos.`id_cliente`)
";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }

    public function getListadoMateriales(){
        $d = array();
        $sql = "
            SELECT cat_equipos.`codigo`, cotizaciones.`id_cliente`, CONCAT(cat_equipos.`codigo`, '-', cotizaciones.`id_cliente`) AS codigo_cliente, CONCAT(cat_equipos.codigo,' - ',seleccion,' - ',descripcion,' - ',unidad,' - ',cantidad_requerida) AS descripcion, id_orden  
            FROM orden_trabajo_materiales 
            INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id 
            INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id
            INNER JOIN cat_equipos ON orden_trabajo_materiales.`codigo` = cat_equipos.`codigo`
            WHERE cat_equipos.`id_cliente` = cotizaciones.`id_cliente` AND orden_trabajo_materiales.status = 1
            HAVING descripcion IS NOT NULL";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }

    public function getListadoHerramientas(){
        $d = array();
        $sql = "SELECT CONCAT(herramientas,' - ',requerimientos) AS herramientas, id_orden FROM orden_trabajo_herramientas WHERE orden_trabajo_herramientas.status = 1 HAVING herramientas IS NOT NULL";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }

    public function getListadoRepuestos(){
        $d = array();
        $sql = "
        SELECT cat_equipos.`codigo`, cotizaciones.`id_cliente`, CONCAT(cat_equipos.`codigo`, '-', cotizaciones.`id_cliente`) AS codigo_cliente,CONCAT(cat_equipos.codigo,' - ',parte,' - ',descripcion,' - ',item,' - ',cantidad) AS descripcion, id_orden 
        FROM orden_trabajo_repuestos 
        INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id 
        INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id
        INNER JOIN cat_equipos ON orden_trabajo_repuestos.`codigo` = cat_equipos.`codigo`
        WHERE cat_equipos.`id_cliente` = cotizaciones.`id_cliente` AND orden_trabajo_repuestos.status = 1
        HAVING descripcion IS NOT NULL";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }
	
	public function getTipoEquipo(){
        $d = array();
        $sql = "SELECT id, nombre FROM cat_equiposTipo where status=1";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }

    public function getDescriptionEquipo(){
        $d = array();
        $sql = "SELECT id , 
            (SELECT nombre FROM cat_equiposTipo WHERE id = id_tipoequipo) AS id_tipoequipo ,  
                nombre
            FROM cat_descripciones_equipos where status=1";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }

    public function getHerramientas(){
        $d = array();
        $sql = "SELECT id , IF(especificacion = NULL , herramienta ,CONCAT_WS('-',herramienta ,especificacion)) AS nombre FROM cat_herramientas_trabajo where status=1";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }

    public function getSucursalByCliente($clienteId){
        $d = array();
        $sql = "SELECT id, id_cliente, nombre_contacto FROM cat_sucursales WHERE id_cliente = $clienteId AND id_usuario = 2 AND status = 1";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }
	
	public function getClienteByOrden($clienteId){
        $d = array();
        $sql = "SELECT cat_clientes.id, cat_clientes.nombre, cat_clientes.direccion, 
			cat_clientesTipo.nombre as tipo_cliente FROM cat_clientes join cat_clientesTipo 
			on cat_clientesTipo.id = cat_clientes.id_tipcli WHERE cat_clientes.id = $clienteId AND cat_clientes.id_usuario = 2 AND status = 1";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }
	
	public function getContactoByOrden($clienteId){
        $d = array();
        $sql = "SELECT * from cat_clientes_contactos WHERE id_cliente = $clienteId";
		
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }
	
	public function getAreaClimatizada(){
        $d = array();
        $sql = "SELECT * FROM cat_areas where status=1";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }
	public function getCapacidadBUTU(){
        $d = array();
        $sql = "SELECT * FROM cat_capacidadBUTU where status=1";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }
	public function getCapacidadHP(){
        $d = array();
        $sql = "SELECT * FROM cat_capacidadHP where status=1";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }

    public function getHorasOcupadas(){
    	$sql = "SELECT CONCAT(TIME(fecha_agendada), DATE_FORMAT(fecha_agendada, ' %p')) AS fecha_agendada, CONCAT(TIME(fecha_fin), DATE_FORMAT(fecha_fin, ' %p')) AS fecha_fin, grupo_trabajo FROM orden_trabajo WHERE DATE(fecha_agendada) = DATE(CURRENT_TIMESTAMP);";
    	$resultado = $this->mysqli->query($sql);
    	$rows = array();
    	while($row = $resultado->fetch_assoc()){
    		$rows[] = $row;
    	}
    	return $rows;
    }

    public function getHorasInstalacion(){
    	$sql = "SELECT CONCAT(TIME(fecha_agendada), DATE_FORMAT(fecha_agendada, ' %p')) AS fecha_agendada, CONCAT(TIME(fecha_fin), DATE_FORMAT(fecha_fin, ' %p')) AS fecha_fin, grupo_trabajo FROM orden_trabajo WHERE tipo_trabajo LIKE '%INSTALACION%' AND DATE(fecha_agendada) = DATE(CURRENT_TIMESTAMP);";
    	$resultado = $this->mysqli->query($sql);
    	$rows = array();
    	while($row = $resultado->fetch_assoc()){
    		$rows[] = $row;
    	}
    	return $rows;
    }

    public function getHorasMantenimiento(){
    	$sql = "SELECT CONCAT(TIME(fecha_agendada), DATE_FORMAT(fecha_agendada, ' %p')) AS fecha_agendada, CONCAT(TIME(fecha_fin), DATE_FORMAT(fecha_fin, ' %p')) AS fecha_fin, grupo_trabajo FROM orden_trabajo WHERE tipo_trabajo LIKE '%MANTENIMIENTO%' AND DATE(fecha_agendada) = DATE(CURRENT_TIMESTAMP);";
    	$resultado = $this->mysqli->query($sql);
    	$rows = array();
    	while($row = $resultado->fetch_assoc()){
    		$rows[] = $row;
    	}
    	return $rows;
    }

    public function getHorasCorrectivo(){
    	$sql = "SELECT CONCAT(TIME(fecha_agendada), DATE_FORMAT(fecha_agendada, ' %p')) AS fecha_agendada, CONCAT(TIME(fecha_fin), DATE_FORMAT(fecha_fin, ' %p')) AS fecha_fin, grupo_trabajo FROM orden_trabajo WHERE tipo_trabajo LIKE '%CORRECTIVO%' AND DATE(fecha_agendada) = DATE(CURRENT_TIMESTAMP);";
    	$resultado = $this->mysqli->query($sql);
    	$rows = array();
    	while($row = $resultado->fetch_assoc()){
    		$rows[] = $row;
    	}
    	return $rows;
    }
	
	public function getOrdenesTrabajo(){
        $d = array();
		//$sql = "SELECT * FROM cat_capacidadHP where status=1";
        $sql = "SELECT orden_trabajo.id, orden_trabajo.observaciones, cotizaciones.id_sucursal, cotizaciones.id_cliente, orden_trabajo.id_cotizacion, TIME(fecha_agendada) AS hora FROM orden_trabajo
JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
WHERE DATE(fecha_agendada) = CURRENT_DATE() AND orden_trabajo.status = 2 GROUP BY hora";
		
        $resultado = $this->mysqli->query($sql);
		//return $resultado;
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }

    public function getOrdenesTrabajoProntoforms(){
        $d = array();
        $sql = "SELECT orden_trabajo.id, orden_trabajo.observaciones, cotizaciones.id_sucursal, cotizaciones.id_cliente, orden_trabajo.id_cotizacion, TIME(fecha_agendada) AS hora FROM orden_trabajo
JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
WHERE DATE(fecha_agendada) = CURRENT_DATE() AND orden_trabajo.status = 2 GROUP BY hora";
        
        $resultado = $this->mysqli->query($sql);
        //return $resultado;
        while($row = $resultado->fetch_assoc()){
            #$row["sucursal"] = $this->getSucusalByCotizacion($row["id_cotizacion"]);
            $row["nombre_cliente"] = $this->getClienteByOrden($row["id_cliente"])[0]["nombre"];
            $row["direccion_cliente"] = $this->getClienteByOrden($row["id_cliente"])[0]["direccion"];
            $row["tipo_cliente"] = $this->getClienteByOrden($row["id_cliente"])[0]["tipo_cliente"];
            #$row["detalles"] = $this->getOrdenesTrabajoDetalles($row["id"])[0];
            #$row["materiales"] = $this->getOrdenesTrabajoMateriales($row["id"]);
            $d[] = $row;
        }
        return $d;
    }

    private function getSucusalByCotizacion($id){
        $sql = "SELECT cat_sucursales.id, nombre_contacto, razon_social, ruc, direccion, telefono, ciudad FROM cotizaciones INNER JOIN cat_sucursales ON id_sucursal = cat_sucursales.id WHERE cotizaciones.id = {$id}";

        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d = $row;
        }
        return $d;
    }

    private function getOrdenesTrabajoDetalles($id){
        $sql = "SELECT *, orden_trabajo.observaciones as obser FROM orden_trabajo  
                    INNER JOIN cotizaciones 
                    ON orden_trabajo.id_cotizacion = cotizaciones.id 
                    INNER JOIN cat_clientes 
                    ON  cotizaciones.id_cliente = cat_clientes.id
                    WHERE cotizaciones.id = {$id}";

        $resultado = $this->mysqli->query($sql);
        $d = array();
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }

    private function getOrdenesTrabajoMateriales($id){
        $sql = "SELECT orden_trabajo_materiales.id, orden_trabajo_materiales.seleccion AS item, orden_trabajo_materiales.descripcion
AS descripcion_item, orden_trabajo_materiales.unidad, orden_trabajo_materiales.cantidad_requerida AS cantidad, orden_trabajo_materiales.codigo as codigo FROM orden_trabajo_materiales  
                    JOIN orden_trabajo 
                    ON orden_trabajo.id = orden_trabajo_materiales.id_orden               
                    JOIN cotizaciones
                    ON  cotizaciones.id = orden_trabajo.id_cotizacion
                    WHERE cotizaciones.id ='{$id}' and orden_trabajo_materiales.status=1";

        $resultado = $this->mysqli->query($sql);
        $d = array();
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }
	
	public function getEquipoByOrden($ordenId){
        $d = array();
        $sql2 = "SELECT CONCAT(cat_areas.nombre,' - ', cat_descripciones_equipos.nombre, ' - ',ins_caracteristicas_climatizacion_a.codigo, ' - ', ins_caracteristicas_climatizacion_a.marca, ' - ', ins_caracteristicas_climatizacion_a.capacidadBTU, ' - ' , ins_caracteristicas_climatizacion_a.modelo, ' - ', ins_caracteristicas_climatizacion_a.serie) as equipo, ins_caracteristicas_climatizacion_a.id_orden,  ins_caracteristicas_climatizacion_a.id FROM ins_caracteristicas_climatizacion_a JOIN cat_areas ON cat_areas.id = ins_caracteristicas_climatizacion_a.area JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = ins_caracteristicas_climatizacion_a.descripcion WHERE id_orden = ".$ordenId;
				$resultado2 = $this->mysqli->query($sql2);
				$equipos = array(); 
				while($row2 = $resultado2->fetch_assoc()){
					
					$d[] = $row2;
				}
        return $d;
    }
	
	public function getTipoTrabajoByOrden($ordenId){
        $d = array();
        $sql2 = "SELECT cat_tipos_trabajo.id, cat_tipos_trabajo.descripcion from cat_tipos_trabajo join cotizaciones_detalle on cotizaciones_detalle.tipo_trabajo
				= cat_tipos_trabajo.id WHERE cotizaciones_detalle.id_cotizacion = ".$ordenId;

				$resultado2 = $this->mysqli->query($sql2);
				$equipos = array(); 
				while($row2 = $resultado2->fetch_assoc()){
					
					$d[] = $row2;
				}
        return $d;
    }
	
	public function getMaterialesByOrden($ordenId){
        $d = array();
        $sql2 = "SELECT * from orden_trabajo_materiales where status=1 and id_orden = ".$ordenId;
				$resultado2 = $this->mysqli->query($sql2);
				$equipos = array(); 
				while($row2 = $resultado2->fetch_assoc()){
					
					$d[] = $row2;
				}
        return $d;
    }
	
	public function getHerramientasByOrden($ordenId){
        $d = array();
        $sql2 = "SELECT * from orden_trabajo_herramientas where status=1 and id_orden = ".$ordenId;
				$resultado2 = $this->mysqli->query($sql2);
				$equipos = array(); 
				while($row2 = $resultado2->fetch_assoc()){
					
					$d[] = $row2;
				}
        return $d;
    }
	
	public function getrepuestosByOrden($ordenId){
        $d = array();
        $sql2 = "SELECT * from orden_trabajo_repuestos where status=1 and id_orden = ".$ordenId;
				$resultado2 = $this->mysqli->query($sql2);
				$equipos = array(); 
				while($row2 = $resultado2->fetch_assoc()){
					
					$d[] = $row2;
				}
        return $d;
    }
}