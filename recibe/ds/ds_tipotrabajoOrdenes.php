<?php

include_once __DIR__ . '/datasource.inc.php';

$c = 0;
$d = array();
$o = new DataSource();
$s = new DataSource();
foreach ($o->getOrdenesTrabajo() as $orden){
    $f = $s->getTipoTrabajoByOrden($orden['id_cotizacion']);
    if(sizeof($f) > 0){
        foreach ($f as $ele) {
            $d[$c]['id'] = $ele['id'];
            $d[$c]['descripcion'] = $ele['descripcion'];
            $c++;
        }
    }
    else{
        $d[$c]['id'] = 0;
        $d[$c]['descripcion'] = "No tiene tipos de trabajo";
        $c++;
    }
}
echo json_encode($d, true);
/*
echo '<pre>';
print_r($d);
echo '</pre>';
*/