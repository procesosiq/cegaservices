<?php

class DataSource
{
    private $mysqli;

    function __construct(){
        $this->mysqli = @new mysqli("localhost", "auditoriasbonita", "u[V(fTIUbcVb", "cegaservices2");
        if (mysqli_connect_errno()) {
            printf("Falló la conexión: %s\n", mysqli_connect_error());
            exit();
        }
        $this->mysqli->set_charset("utf8");
        require_once("../../controllers/conexion.php");
        $this->conexion = new M_Conexion;
    }

    public function tiposTrabajosOrden(){
    	$sql = "SELECT id AS id_orden, TRIM(SUBSTRING_INDEX(SUBSTRING_INDEX(t.tipo_trabajo, ',', n.n), ',', -1)) VALUE, CONCAT(TRIM(SUBSTRING_INDEX(SUBSTRING_INDEX(t.tipo_trabajo, ',', n.n), ',', -1)), ' - ' , id) AS tipo_id_orden
                FROM orden_trabajo t CROSS JOIN 
                (
                SELECT a.N + b.N * 10 + 1 n
                    FROM 
                    (SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) a
                ,(SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) b
                    ORDER BY n
                ) n
                WHERE n.n <= 1 + (LENGTH(t.tipo_trabajo) - LENGTH(REPLACE(t.tipo_trabajo, ',', ''))) AND DATE(fecha_agendada) = CURRENT_DATE
                ORDER BY id";

		$d = array();
		$res = $this->mysqli->query($sql);
		while($row = $res->fetch_assoc()){
			$d[] = $row;
		}
		return $d;
    }

    public function getSucursales(){
    	$sql = "SELECT 
			IF(cat_sucursales.id IS NULL, 0, cat_sucursales.id) AS id, 
			cat_clientes.id AS id_cliente, 
			IF(cat_sucursales.nombre_contacto IS NULL, 'SIN INFORMACIÓN', cat_sucursales.`nombre_contacto`) AS nombre_contacto, 
			#IF(cat_sucursales.id_tipcli IS NULL, 'SIN INFORMACIÓN', cat_sucursales.`id_tipcli`) AS id_tipcli,
			IF((SELECT COUNT(*) FROM cat_sucursales WHERE id_cliente = cat_clientes.id) > 0, cat_sucursales.nombre_contacto, 'NO TIENE SUCURSALES') AS razon_social,
			#IF(cat_sucursales.ruc IS NULL, 'N/A', cat_sucursales.ruc) AS ruc,
			IF(cat_sucursales.direccion IS NULL, cat_clientes.direccion, cat_sucursales.direccion) AS direccion
			#IF(cat_sucursales.telefono IS NULL, 'N/A', cat_sucursales.telefono) AS telefono,
			#IF(cat_sucursales.`ciudad` IS NULL, 'N/A', cat_sucursales.`ciudad`) AS ciudad,
			#IF(cat_sucursales.`fecha` IS NULL, 'N/A', cat_sucursales.`fecha`) AS fecha,
			#IF(cat_sucursales.id_usuario IS NULL, 'N/A',cat_sucursales.id_usuario) AS id_usuario,
			#IF(cat_sucursales.status IS NULL, 'N/A', cat_sucursales.status) AS status
		FROM cat_clientes 
		LEFT JOIN cat_sucursales ON id_cliente = cat_clientes.id;";
    	$resultado = $this->mysqli->query($sql);
    	while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }

    public function getClientes(){
        $d = array();
        $sql = "SELECT 
            cat_clientes.id, 
            cat_clientes.nombre , 
            cat_clientesTipo.nombre AS tipo_cliente, 
            cat_clientes.id_tipcli AS id_tipo_cliente, 
            cat_clientes.direccion, 
            CONCAT(orden_trabajo.id, '-', cat_clientes.id) AS id_orden_cliente,
            IF(orden_trabajo.direccion IS NULL, 'SIN INFORMACIÓN', orden_trabajo.direccion) AS direccion_orden, 
            IF(cat_sucursales.`direccion` IS NULL, cat_clientes.direccion, cat_sucursales.`direccion`) AS direccion_sucursal,
            IF(cat_clientes.referencia IS NULL, 'SIN INFORMACIÓN', cat_clientes.referencia) AS referencia,  
            IF(cotizaciones.id_sucursal IS NULL, 'SIN INFORMACIÓN', cotizaciones.id_sucursal) AS id_sucursal, 
            IF(cat_sucursales.nombre_contacto IS NULL, 'NO TIENE SUCURSALES', cat_sucursales.nombre_contacto) AS razon_social, 
            IF(cat_clientes.nombre IS NULL, 'SIN INFORMACIÓN', cat_clientes.nombre) AS nombre_contacto, 
            IF(cat_clientes.email IS NULL, 'SIN INFORMACIÓN', cat_clientes.email) AS email_cliente, 
            IF(cat_sucursales.telefono IS NULL, '',cat_sucursales.telefono) AS telefono_sucursal,
            IF(cat_clientes.telefono IS NULL, 'SIN INFORMACIÓN', cat_clientes.telefono) AS telefono_cliente, 
            IF(cat_sucursales.direccion IS NULL OR cat_sucursales.direccion = ' ', 'SIN INFORMACIÓN', cat_sucursales.direccion) AS direccion, 
            orden_trabajo.id as id_orden_trabajo, 
            orden_trabajo.observaciones AS observaciones, 
            tipo_trabajo, 
            CONCAT(tipo_trabajo,' - ',orden_trabajo.id) as tipo_id_orden,
            CONCAT(TIME(fecha_agendada), DATE_FORMAT(fecha_agendada, ' %p')) AS fecha_agendada, 
            id_responsable as grupo_trabajo, 
            id_vehiculo, 
            (SELECT nombre FROM cat_vehiculos WHERE id = id_vehiculo) as vehiculo,
            IFNULL(cat_clientes_contactos.nombre, 'NO ASIGNADO') AS contacto,
            IFNULL(cat_clientes_contactos.telefono, '') AS tel_contacto,
            IFNULL(cat_clientes_contactos.correo, 'NO ASIGNADO') AS correo_contacto
                FROM cat_clientes 
                LEFT JOIN cotizaciones ON cotizaciones.id_cliente = cat_clientes.id 
                LEFT JOIN orden_trabajo ON id_cotizacion = cotizaciones.id 
                LEFT JOIN orden_trabajo_responsables ON orden_trabajo_responsables.id_orden = orden_trabajo.id
                LEFT JOIN cat_sucursales ON id_sucursal = cat_sucursales.id 
                LEFT JOIN cat_clientesTipo ON tipo_cliente = cat_clientesTipo.id 
                LEFT JOIN cat_clientes_contactos ON cotizaciones.id_contacto = cat_clientes_contactos.id
                WHERE DATE(fecha_agendada) = DATE(CURRENT_TIMESTAMP);";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
        	$row["observaciones"] = strip_tags($row["observaciones"]);
            $d[] = $row;
        }
        if(count($d)==0){
        	$d[] = array("id"=>0, "nombre"=>"SIN CLIENTES", "tipo_cliente"=>"", "id_tipo_cliente"=>0, "direccion"=>"", "id_orden_cliente"=>0, "direccion_orden"=>"", "direccion_sucursal"=>"", "referencia"=>"", "id_sucursal"=>0, "razon_social"=>"", "nombre_contacto"=>"", "email_cliente"=>"",
        		"telefono_sucursal"=>"", "telefono_cliente"=>"", "direccion"=>"", "tipo_id_orden"=>"", "fecha_agendada"=>"", "grupo_trabajo"=>0);
        }
        return $d;
    }

    public function getClientesCorrectivos(){
        $d = array();
        $sql = "SELECT 
        	cat_clientes.id, 
            orden_trabajo.codigo AS codigo_orden,
        	cat_clientes.nombre , 
        	cat_clientesTipo.nombre AS tipo_cliente, 
        	cat_clientes.id_tipcli AS id_tipo_cliente, 
        	cat_clientes.direccion, 
        	CONCAT(orden_trabajo.id, '-', cat_clientes.id) AS id_orden_cliente,
        	IF(orden_trabajo.direccion IS NULL, 'SIN INFORMACIÓN', orden_trabajo.direccion) AS direccion_orden, 
        	IF(cat_sucursales.`direccion` IS NULL, cat_clientes.direccion, cat_sucursales.`direccion`) AS direccion_sucursal,
	    	IF(cat_clientes.referencia IS NULL, 'SIN INFORMACIÓN', cat_clientes.referencia) AS referencia,  
	    	IF(cotizaciones.id_sucursal IS NULL, 'SIN INFORMACIÓN', cotizaciones.id_sucursal) AS id_sucursal, 
	    	IF(cat_sucursales.nombre_contacto IS NULL, 'NO TIENE SUCURSALES', cat_sucursales.nombre_contacto) AS razon_social, 
	    	IF(cat_clientes.email IS NULL, 'SIN INFORMACIÓN', cat_clientes.email) AS email_cliente, 
	    	IF(cat_sucursales.telefono IS NULL, '',cat_sucursales.telefono) AS telefono_sucursal,
	    	IF(cat_clientes.telefono IS NULL, 'SIN INFORMACIÓN', cat_clientes.telefono) AS telefono_cliente, 
	    	IF(cat_sucursales.direccion IS NULL OR cat_sucursales.direccion = ' ', 'SIN INFORMACIÓN', cat_sucursales.direccion) AS direccion, 
	    	orden_trabajo.id as id_orden_trabajo, orden_trabajo.observaciones AS observaciones, tipo_trabajo, 
	    	CONCAT(TIME(fecha_agendada), DATE_FORMAT(fecha_agendada, ' %p')) AS fecha_agendada, id_responsable as grupo_trabajo,
            IFNULL(IF(cotizaciones.id_sucursal > 0, cat_sucursales_contactos.nombre, cat_clientes_contactos.nombre), 'NO ASIGNADO') AS nombre_contacto,
            IFNULL(IF(cotizaciones.id_sucursal > 0, cat_sucursales_contactos.telefono, cat_clientes_contactos.telefono), '') AS telefono_contacto,
            orden_trabajo.ids_auxiliares
                FROM cat_clientes 
                LEFT JOIN cotizaciones ON cotizaciones.id_cliente = cat_clientes.id 
                LEFT JOIN orden_trabajo ON id_cotizacion = cotizaciones.id 	
                LEFT JOIN orden_trabajo_responsables ON orden_trabajo_responsables.id_orden = orden_trabajo.id
                LEFT JOIN cat_sucursales ON cat_sucursales.id = cotizaciones.id_sucursal  
                LEFT JOIN cat_clientesTipo ON tipo_cliente = cat_clientesTipo.id 
                LEFT JOIN cat_clientes_contactos ON cotizaciones.id_contacto = cat_clientes_contactos.id
                LEFT JOIN cat_sucursales_contactos ON cotizaciones.id_contacto = cat_sucursales_contactos.id
                WHERE DATE(fecha_agendada) = DATE(CURRENT_TIMESTAMP) AND (tipo_trabajo LIKE '%CORRECTIVO%' OR tipo_trabajo LIKE '%REBOTE / RECLAMO%') AND tipo_trabajo NOT LIKE '%REVISION%'";
        $resultado = $this->conexion->queryAll($sql);
        foreach($resultado as $row){
            if($row->ids_auxiliares != ''){
                $aux = explode(',', $row->ids_auxiliares);
                $row->ids_auxiliares = "";
                foreach($aux as &$a){
                    $sql = "SELECT nombre FROM cat_auxiliares WHERE id = '{$a}'";
                    $row->ids_auxiliares .= $this->conexion->queryRow($sql)->nombre." / ";
                }
            }
            $row->observaciones = strip_tags($row->observaciones);
            $d[] = $row;
        }
        if(count($d)==0){
        	$d[] = array("id"=>0, "codigo_orden"=>"OT-0000", "nombre"=>"SIN CLIENTES", "direccion_sucursal" => "", "referencia" => "", "razon_social" => "", "nombre_contacto" => "", "observaciones" =>  "", "id_orden_trabajo" => 0, "tipo_cliente"=>"", "id_tipo_cliente"=>0, "direccion"=>"", "fecha_agendada" => "");
        }
        return $d;
    }

    public function getClientesInstalacion(){
        $d = array();
        $sql = "SELECT 
        	cat_clientes.id, 
            orden_trabajo.codigo AS codigo_orden,
        	cat_clientes.nombre , 
        	cat_clientesTipo.nombre AS tipo_cliente, 
        	cat_clientes.id_tipcli AS id_tipo_cliente, 
        	cat_clientes.direccion, 
        	CONCAT(orden_trabajo.id, '-', cat_clientes.id) AS id_orden_cliente,
        	IF(orden_trabajo.direccion IS NULL, 'SIN INFORMACIÓN', orden_trabajo.direccion) AS direccion_orden, 
        	IF(cat_sucursales.`direccion` IS NULL, cat_clientes.direccion, cat_sucursales.`direccion`) AS direccion_sucursal,
	    	IF(cat_clientes.referencia IS NULL, 'SIN INFORMACIÓN', cat_clientes.referencia) AS referencia,  
	    	IF(cotizaciones.id_sucursal IS NULL, 'SIN INFORMACIÓN', cotizaciones.id_sucursal) AS id_sucursal, 
	    	IF(cat_sucursales.nombre_contacto IS NULL, 'NO TIENE SUCURSALES', cat_sucursales.nombre_contacto) AS razon_social, 
	    	IF(cat_clientes.nombre IS NULL, 'SIN INFORMACIÓN', cat_clientes.nombre) AS nombre_contacto_sucursal, 
	    	IF(cat_clientes.email IS NULL, 'SIN INFORMACIÓN', cat_clientes.email) AS email_cliente, 
	    	IF(cat_sucursales.telefono IS NULL, '',cat_sucursales.telefono) AS telefono_sucursal,
	    	IF(cat_clientes.telefono IS NULL, 'SIN INFORMACIÓN', cat_clientes.telefono) AS telefono_cliente, 
	    	IF(cat_sucursales.direccion IS NULL OR cat_sucursales.direccion = ' ', 'SIN INFORMACIÓN', cat_sucursales.direccion) AS direccion, 
	    	orden_trabajo.id as id_orden_trabajo, orden_trabajo.observaciones AS observaciones, tipo_trabajo, 
	    	CONCAT(TIME(fecha_agendada), DATE_FORMAT(fecha_agendada, ' %p')) AS fecha_agendada, id_responsable as grupo_trabajo,
            IFNULL(IF(cotizaciones.id_sucursal > 0, cat_sucursales_contactos.nombre, cat_clientes_contactos.nombre), 'NO ASIGNADO') AS nombre_contacto,
            IFNULL(IF(cotizaciones.id_sucursal > 0, cat_sucursales_contactos.telefono, cat_clientes_contactos.telefono), '') AS telefono_contacto,
            orden_trabajo.ids_auxiliares
                FROM cat_clientes 
                LEFT JOIN cotizaciones ON cotizaciones.id_cliente = cat_clientes.id 
                LEFT JOIN orden_trabajo ON id_cotizacion = cotizaciones.id 	
                LEFT JOIN orden_trabajo_responsables ON orden_trabajo_responsables.id_orden = orden_trabajo.id
                LEFT JOIN cat_sucursales ON cat_sucursales.id = cotizaciones.id_sucursal
                LEFT JOIN cat_clientesTipo ON tipo_cliente = cat_clientesTipo.id 
                LEFT JOIN cat_clientes_contactos ON cotizaciones.id_contacto = cat_clientes_contactos.id
                LEFT JOIN cat_sucursales_contactos ON cotizaciones.id_contacto = cat_sucursales_contactos.id
                WHERE DATE(fecha_agendada) = DATE(CURRENT_TIMESTAMP) AND tipo_trabajo LIKE '%INSTALACION%' AND tipo_trabajo NOT LIKE '%REVISION%'";
        $resultado = $this->conexion->queryAll($sql);
        foreach($resultado as $row){
            if($row->ids_auxiliares != ''){
                $aux = explode(',', $row->ids_auxiliares);
                $row->ids_auxiliares = "";
                foreach($aux as &$a){
                    $sql = "SELECT nombre FROM cat_auxiliares WHERE id = '{$a}'";
                    $row->ids_auxiliares .= $this->conexion->queryRow($sql)->nombre." / ";
                }
            }
            $row->observaciones = strip_tags($row->observaciones);
            $d[] = $row;
        }

        if(count($d)==0){
        	$d[] = array("grupo_trabajo" => 0,"id"=>0, "codigo_orden"=>"OT-0000", "nombre"=>"SIN CLIENTES", "direccion_sucursal" => "", "referencia" => "", "razon_social" => "", "nombre_contacto" => "", "observaciones" =>  "", "id_orden_trabajo" => 0, "tipo_cliente"=>"", "id_tipo_cliente"=>0, "direccion"=>"", "fecha_agendada" => "", "nombre_contacto" => "", "telefono_contacto" => "");
        }
        return $d;
    }

    public function getClientesMantenimiento(){
        $d = array();
        $sql = "SELECT 
        	cat_clientes.id, 
            orden_trabajo.codigo AS codigo_orden,
        	cat_clientes.nombre , 
        	cat_clientesTipo.nombre AS tipo_cliente, 
        	cat_clientes.id_tipcli AS id_tipo_cliente, 
        	cat_clientes.direccion, 
        	CONCAT(orden_trabajo.id, '-', cat_clientes.id) AS id_orden_cliente,
        	IF(orden_trabajo.direccion IS NULL, 'SIN INFORMACIÓN', orden_trabajo.direccion) AS direccion_orden, 
        	IF(cat_sucursales.`direccion` IS NULL, cat_clientes.direccion, cat_sucursales.`direccion`) AS direccion_sucursal,
	    	IF(cat_clientes.referencia IS NULL, 'SIN INFORMACIÓN', cat_clientes.referencia) AS referencia,  
	    	IF(cotizaciones.id_sucursal IS NULL, 'SIN INFORMACIÓN', cotizaciones.id_sucursal) AS id_sucursal, 
	    	IF(cat_sucursales.nombre_contacto IS NULL, 'NO TIENE SUCURSALES', cat_sucursales.nombre_contacto) AS razon_social, 
	    	IF(cat_clientes.nombre IS NULL, 'SIN INFORMACIÓN', cat_clientes.nombre) AS nombre_contacto, 
	    	IF(cat_clientes.email IS NULL, 'SIN INFORMACIÓN', cat_clientes.email) AS email_cliente, 
	    	IF(cat_sucursales.telefono IS NULL, '',cat_sucursales.telefono) AS telefono_sucursal,
	    	IF(cat_clientes.telefono IS NULL, 'SIN INFORMACIÓN', cat_clientes.telefono) AS telefono_cliente, 
	    	IF(cat_sucursales.direccion IS NULL OR cat_sucursales.direccion = ' ', 'SIN INFORMACIÓN', cat_sucursales.direccion) AS direccion, 
	    	orden_trabajo.id as id_orden_trabajo, orden_trabajo.observaciones AS observaciones, 
	    	tipo_trabajo, 
            CONCAT(tipo_trabajo,' - ',orden_trabajo.id) as tipo_id_orden,
	    	CONCAT(TIME(fecha_agendada), DATE_FORMAT(fecha_agendada, ' %p')) AS fecha_agendada, 
	    	id_responsable as grupo_trabajo,
            IFNULL(IF(cotizaciones.id_sucursal > 0, cat_sucursales_contactos.nombre, cat_clientes_contactos.nombre), 'NO ASIGNADO') AS nombre_contacto,
            IFNULL(IF(cotizaciones.id_sucursal > 0, cat_sucursales_contactos.telefono, cat_clientes_contactos.telefono), '') AS telefono_contacto,
            orden_trabajo.ids_auxiliares
                FROM cat_clientes 
                LEFT JOIN cotizaciones ON cotizaciones.id_cliente = cat_clientes.id 
                LEFT JOIN orden_trabajo ON id_cotizacion = cotizaciones.id 	
                LEFT JOIN orden_trabajo_responsables ON orden_trabajo_responsables.id_orden = orden_trabajo.id
                LEFT JOIN cat_sucursales ON cat_sucursales.id = cotizaciones.id_sucursal  
                LEFT JOIN cat_clientesTipo ON tipo_cliente = cat_clientesTipo.id 
                LEFT JOIN cat_clientes_contactos ON cotizaciones.id_contacto = cat_clientes_contactos.id
                LEFT JOIN cat_sucursales_contactos ON cotizaciones.id_contacto = cat_sucursales_contactos.id
                WHERE DATE(fecha_agendada) = DATE(CURRENT_TIMESTAMP) AND tipo_trabajo LIKE '%MANTENIMIENTO%' AND tipo_trabajo NOT LIKE '%REVISION%'";
        $resultado = $this->conexion->queryAll($sql);
        foreach($resultado as $row){
            if($row->ids_auxiliares != ''){
                $aux = explode(',', $row->ids_auxiliares);
                $row->ids_auxiliares = "";
                foreach($aux as &$a){
                    $sql = "SELECT nombre FROM cat_auxiliares WHERE id = '{$a}'";
                    $row->ids_auxiliares .= $this->conexion->queryRow($sql)->nombre." / ";
                }
            }
            $row->observaciones = strip_tags($row->observaciones);
            $d[] = $row;
        }

        if(count($d)==0){
        	$d[] = array("id"=>0, "codigo_orden"=>"OT-0000", "nombre"=>"SIN CLIENTES", "direccion_sucursal" => "", "referencia" => "", "razon_social" => "", "nombre_contacto" => "", "observaciones" =>  "", "id_orden_trabajo" => 0, "tipo_cliente"=>"", "id_tipo_cliente"=>0, "direccion"=>"", "fecha_agendada" => "", "grupo_trabajo" => 0);
        }
        return $d;
    }

    public function getListadoEquipos(){
        $d = array();
        $sql = "
#mantenimiento
(SELECT 'MANTENIMIENTO' AS tipo_trabajo, CONCAT('MANTENIMIENTO - ', id_orden) AS tipo_id_orden, CONCAT(man_caracteristicas_climatizacion_a.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.`nombre`,' - ',man_caracteristicas_climatizacion_a.codigo,' - ',IF(marca IS NULL, '', marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM man_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE man_caracteristicas_climatizacion_a.status = 1 HAVING reg IS NOT NULL) 
UNION 
(SELECT 'MANTENIMIENTO' AS tipo_trabajo, CONCAT('MANTENIMIENTO - ', id_orden) AS tipo_id_orden, CONCAT(man_caracteristicas_climatizacion_b.`codigo`, '-', id_cliente) AS codigo_cliente,  id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.`nombre`,' - ',man_caracteristicas_climatizacion_b.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM man_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE man_caracteristicas_climatizacion_b.status = 1 HAVING reg IS NOT NULL)
UNION
(SELECT 'MANTENIMIENTO' AS tipo_trabajo, CONCAT('MANTENIMIENTO - ', id_orden) AS tipo_id_orden, CONCAT(man_caracteristicas_refrigeracion.`codigo`, '-', id_cliente) AS codigo_cliente,  id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.`nombre`,' - ',man_caracteristicas_refrigeracion.codigo,' - ',IF(marca_eva IS NULL,'',marca_eva),' - ',IF(capacidad_hp_eva IS NULL,'',capacidad_hp_eva),' - ',IF(modelo_eva IS NULL,'',modelo_eva),' - ',IF(serie_eva IS NULL,'',serie_eva)) AS reg FROM man_caracteristicas_refrigeracion INNER JOIN cat_areas ON id_area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON id_equipo_des = cat_descripciones_equipos.`id` WHERE man_caracteristicas_refrigeracion.status = 1 HAVING reg IS NOT NULL)
UNION
(SELECT 'MANTENIMIENTO' AS tipo_trabajo, CONCAT('MANTENIMIENTO - ', id_orden) AS tipo_id_orden, CONCAT(man_caracteristicas_ventilacion.`codigo`, '-', id_cliente) AS codigo_cliente,  id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.`nombre`,' - ',man_caracteristicas_ventilacion.codigo,' - ',IF(marca,'',marca),' - ',IF(capacidadHP IS NULL,'',capacidadHP),' - ',IF(capacidadCFN IS NULL,'',capacidadCFN),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM man_caracteristicas_ventilacion INNER JOIN cat_areas ON area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE man_caracteristicas_ventilacion.status = 1 HAVING reg IS NOT NULL)
#instalacion
UNION
(SELECT 'INSTALACION' AS tipo_trabajo, CONCAT('INSTALACION - ', id_orden) AS tipo_id_orden, CONCAT(ins_caracteristicas_climatizacion_a.`codigo`, '-', id_cliente) AS codigo_cliente,  id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.`nombre`,' - ',ins_caracteristicas_climatizacion_a.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie,'',serie)) AS reg FROM ins_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE ins_caracteristicas_climatizacion_a.status = 1 HAVING reg IS NOT NULL) 
UNION 
(SELECT 'INSTALACION' AS tipo_trabajo, CONCAT('INSTALACION - ', id_orden) AS tipo_id_orden,  CONCAT(ins_caracteristicas_climatizacion_b.`codigo`, '-', id_cliente) AS codigo_cliente,  id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.`nombre`,' - ',ins_caracteristicas_climatizacion_b.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM ins_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE ins_caracteristicas_climatizacion_b.status = 1 HAVING reg IS NOT NULL)
UNION
(SELECT 'INSTALACION' AS tipo_trabajo, CONCAT('INSTALACION - ', id_orden) AS tipo_id_orden,  CONCAT(ins_caracteristicas_refrigeracion.`codigo`, '-', id_cliente) AS codigo_cliente,  id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.`nombre`,' - ',ins_caracteristicas_refrigeracion.codigo,' - ',IF(marca_eva IS NULL,'',marca_eva),' - ',IF(modelo_eva IS NULL,'',modelo_eva),' - ',IF(serie_eva IS NULL,'',serie_eva),' - '/*,IF(capacidad_hp_eva IS NULL,'',capacidad_hp_eva),' - ',IF(compresorCantidad IS NULL,'',compresorCantidad),' - ',IF(ventiladorCantidad IS NULL,'',ventiladorCantidad)*/) AS reg FROM ins_caracteristicas_refrigeracion INNER JOIN cat_areas ON id_area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON id_equipo_des = cat_descripciones_equipos.`id` WHERE ins_caracteristicas_refrigeracion.status = 1 HAVING reg IS NOT NULL)
UNION
(SELECT 'INSTALACION' AS tipo_trabajo, CONCAT('INSTALACION - ', id_orden) AS tipo_id_orden,  CONCAT(ins_caracteristicas_ventilacion.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.`nombre`,' - ',ins_caracteristicas_ventilacion.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadHP IS NULL,'',capacidadHP),' - ',IF(capacidadCFN IS NULL,'',capacidadCFN),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM ins_caracteristicas_ventilacion INNER JOIN cat_areas ON area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE ins_caracteristicas_ventilacion.status = 1 HAVING reg IS NOT NULL)

#correctivo
UNION
(SELECT 'CORRECTIVO' AS tipo_trabajo, CONCAT('CORRECTIVO - ', id_orden) AS tipo_id_orden, CONCAT(cor_caracteristicas_climatizacion_a.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.nombre,' - ',cor_caracteristicas_climatizacion_a.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM cor_caracteristicas_climatizacion_a INNER JOIN cat_areas ON area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE cor_caracteristicas_climatizacion_a.status = 1 HAVING reg IS NOT NULL) 
UNION 
(SELECT 'CORRECTIVO' AS tipo_trabajo, CONCAT('CORRECTIVO - ', id_orden) AS tipo_id_orden, CONCAT(cor_caracteristicas_climatizacion_b.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.nombre,' - ',cor_caracteristicas_climatizacion_b.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM cor_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = area INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE cor_caracteristicas_climatizacion_b.status = 1 HAVING reg IS NOT NULL)
UNION
(SELECT 'CORRECTIVO' AS tipo_trabajo, CONCAT('CORRECTIVO - ', id_orden) AS tipo_id_orden, CONCAT(cor_caracteristicas_refrigeracion.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.nombre,' - ',cor_caracteristicas_refrigeracion.`codigo`,' - ',IF(marca_eva IS NULL,'',marca_eva),' - ',IF(modelo_eva IS NULL,'',modelo_eva),' - ',IF(serie_eva IS NULL,'',serie_eva),' - '/*,IF(compresorBTU IS NULL,'',compresorBTU),' - ',IF(compresorCantidad IS NULL,'',compresorCantidad),' - ',IF(ventiladorCantidad IS NULL,'',ventiladorCantidad)*/) AS reg FROM cor_caracteristicas_refrigeracion INNER JOIN cat_areas ON id_area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON id_equipo_des = cat_descripciones_equipos.`id` WHERE cor_caracteristicas_refrigeracion.status = 1 HAVING reg IS NOT NULL)
UNION
(SELECT 'CORRECTIVO' AS tipo_trabajo, CONCAT('CORRECTIVO - ', id_orden) AS tipo_id_orden, CONCAT(cor_caracteristicas_ventilacion.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.nombre,' - ',cor_caracteristicas_ventilacion.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadHP IS NULL,'',capacidadHP),' - ',IF(capacidadCFN IS NULL,'',capacidadHP),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM cor_caracteristicas_ventilacion INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE cor_caracteristicas_ventilacion.status = 1 HAVING reg IS NOT NULL)
        ";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }

    public function getListadoEquiposVentilacion(){
    	$d = array();
        $sql = "
			#mantenimiento
			(SELECT  man_caracteristicas_ventilacion.id, nombre AS descripcion, codigo, marca, capacidadHP, modelo, serie, id_orden FROM man_caracteristicas_ventilacion INNER JOIN cat_descripciones_equipos)

			#instalacion
			UNION
			(SELECT  ins_caracteristicas_ventilacion.id, nombre AS descripcion, codigo, marca, capacidadHP, modelo, serie, id_orden FROM ins_caracteristicas_ventilacion INNER JOIN cat_descripciones_equipos)

			#correctivo
			UNION
			(SELECT  cor_caracteristicas_ventilacion.id, nombre AS descripcion, codigo, marca, capacidadHP, modelo, serie, id_orden FROM cor_caracteristicas_ventilacion INNER JOIN cat_descripciones_equipos)
        ";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }

    public function getListadoEquiposRefrigeracion(){
    	$d = array();
        $sql = "
#mantenimiento
(SELECT  id_orden, CONCAT(cat_areas.nombre,' - ',descripcion,' - ',codigo,' - ',IF(marcaEva IS NULL,'',marcaEva),' - ',IF(capaciadadEva IS NULL,'',capaciadadEva),' - ',IF(modeloEva IS NULL,'',modeloEva),' - ',IF(serieEva IS NULL,'',serieEva)) AS reg FROM man_caracteristicas_refrigeracion INNER JOIN cat_areas ON AREA = cat_areas.id HAVING reg IS NOT NULL)

#instalacion
UNION
(SELECT  id_orden, CONCAT(cat_areas.nombre,' - ',descripcion,' - ',codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie),' - ',IF(compresorBTU IS NULL,'',compresorBTU),' - ',IF(compresorCantidad IS NULL,'',compresorCantidad),' - ',IF(ventiladorCantidad IS NULL,'',ventiladorCantidad)) AS reg FROM ins_caracteristicas_refrigeracion INNER JOIN cat_areas ON AREA = cat_areas.id HAVING reg IS NOT NULL)

#correctivo
UNION
(SELECT  id_orden, CONCAT(cat_areas.nombre,' - ',descripcion,' - ',codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie),' - ',IF(compresorBTU IS NULL,'',compresorBTU),' - ',IF(compresorCantidad IS NULL,'',compresorCantidad),' - ',IF(ventiladorCantidad IS NULL,'',ventiladorCantidad)) AS reg FROM cor_caracteristicas_refrigeracion INNER JOIN cat_areas ON AREA = cat_areas.id HAVING reg IS NOT NULL)
        ";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }

    /*public function getListadoEquiposRefrigeracionDetalleInstalacion(){
    	$d = array();
        $sql = "
		SELECT  codigo, descripcion, id, marca, modelo, serie, id_orden FROM ins_caracteristicas_refrigeracion INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.id
        ";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }*/

    public function getListadoEquiposClimatizacionB(){
    	$d = array();
        $sql = "
#mantenimiento
(SELECT  id_orden, CONCAT(cat_areas.`nombre`,' - ',descripcion,' - ',codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM man_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA HAVING reg IS NOT NULL) 

#instalacion
UNION
(SELECT  id_orden, CONCAT(cat_areas.`nombre`,' - ',descripcion,' - ',codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM ins_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA HAVING reg IS NOT NULL)

#correctivo
UNION
(SELECT  id_orden, CONCAT(cat_areas.`nombre`,' - ',descripcion,' - ',codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM cor_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA HAVING reg IS NOT NULL)
        ";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }

    public function getListadoEquiposClimatizacionBDetalleCorrectivo(){
    	$d = array();
        $sql = "SELECT cor_caracteristicas_climatizacion_b.id, 
                id_orden, 
                cat_equipos.id_cliente, 
                nombre_area, 
                cat_areas.id AS id_area, 
                cat_areas.`nombre` AS AREA, 
                cat_descripciones_equipos.nombre AS descripcion, 
                cor_caracteristicas_climatizacion_b.codigo, 
                evaporador, 
                marca, 
                CAST(capacidadBTU AS DECIMAL) AS capacidadBTU, 
                modelo, 
                serie, 
                condensador, 
                condensadorMarca, 
                CAST(condensadorBTU AS DECIMAL) AS condensadorBTU, 
                condensadorModelo, condensadorSerie 
            FROM cor_caracteristicas_climatizacion_b 
            INNER JOIN cat_areas ON cat_areas.id = AREA 
            INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.id 
            INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id 
            INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id 
            INNER JOIN cat_equipos ON cor_caracteristicas_climatizacion_b.codigo = cat_equipos.codigo 
            WHERE 
                cat_equipos.status = 1 
                AND DATE(fecha_agendada) = CURRENT_DATE 
                AND cotizaciones.id_cliente = cat_equipos.`id_cliente` 
                AND cor_caracteristicas_climatizacion_b.status = 1
        ";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        if(count($d)==0){
        	$d[] = array("id" => 0, "id_orden" => 0, "id_cliente" => 0, "nombre_area" => "", "id_area" => 0, "AREA" => "", "descripcion" => "",
        		"codigo" => "", "evaporador" => "", "marca" => "", "capacidadBTU" => 0, "modelo" => "", "serie" => "", "condensador" => "",
        		"condensadorMarca" => "", "condensadorModelo" => "", "condensadorSerie" => "", "condensadorBTU" => "");
        }
        return $d;
    }

    public function getListadoEquiposClimatizacionBDetalleInstalacion(){
    	$d = array();
        $sql = "SELECT ins_caracteristicas_climatizacion_b.id, 
                    id_orden, cat_equipos.id_cliente, 
                    nombre_area, 
                    cat_areas.id AS id_area, 
                    cat_areas.`nombre` AS AREA, 
                    cat_descripciones_equipos.nombre AS descripcion, 
                    ins_caracteristicas_climatizacion_b.codigo, 
                    evaporador, 
                    marca, 
                    CAST(capacidadBTU AS DECIMAL) AS capacidadBTU, 
                    modelo, 
                    serie, 
                    condensador, 
                    condensadorMarca, 
                    CAST(condensadorBTU AS DECIMAL) AS condensadorBTU, 
                    condensadorModelo, condensadorSerie 
                FROM ins_caracteristicas_climatizacion_b 
                INNER JOIN cat_areas ON cat_areas.id = AREA 
                INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.id 
                INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id 
                INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id 
                INNER JOIN cat_equipos ON ins_caracteristicas_climatizacion_b.codigo = cat_equipos.codigo 
                WHERE 
                    cat_equipos.status = 1 
                    AND DATE(fecha_agendada) = CURRENT_DATE 
                    AND cotizaciones.id_cliente = cat_equipos.`id_cliente` 
                    AND ins_caracteristicas_climatizacion_b.status = 1
        ";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        if(count($d)==0){
        	$d[] = array("id" => 0, "id_orden" => 0, "id_cliente" => 0, "nombre_area" => "", "id_area" => 0, "AREA" => "", "descripcion" => "",
        		"codigo" => "", "evaporador" => "", "marca" => "", "capacidadBTU" => 0, "modelo" => "", "serie" => "", "condensador" => "",
        		"condensadorMarca" => "", "condensadorModelo" => "", "condensadorSerie" => "", "condensadorBTU" => "");
        }
        return $d;
    }

    public function getListadoEquiposClimatizacionBDetalleMantenimiento(){
    	$d = array();
        $sql = "
		#mantenimiento
		(SELECT man_caracteristicas_climatizacion_b.id, id_orden, cat_equipos.id_cliente, nombre_area, cat_areas.id AS id_area, cat_areas.`nombre` AS AREA, cat_descripciones_equipos.nombre AS descripcion, man_caracteristicas_climatizacion_b.codigo, evaporador, marca, CAST(capacidadBTU AS DECIMAL) AS capacidadBTU, modelo, serie, condensador, condensadorMarca, CAST(condensadorBTU AS DECIMAL) AS condensadorBTU, condensadorModelo, condensadorSerie FROM man_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id INNER JOIN cat_equipos ON man_caracteristicas_climatizacion_b.codigo = cat_equipos.codigo WHERE DATE(fecha_agendada) = CURRENT_DATE AND cotizaciones.id_cliente = cat_equipos.`id_cliente` AND man_caracteristicas_climatizacion_b.status = 1)
        ";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        if(count($d)==0){
        	$d[] = array("id" => 0, "id_orden" => 0, "id_cliente" => 0, "nombre_area" => "", "id_area" => 0, "AREA" => "", "descripcion" => "",
        		"codigo" => "", "evaporador" => "", "marca" => "", "capacidadBTU" => 0, "modelo" => "", "serie" => "", "condensador" => "",
        		"condensadorMarca" => "", "condensadorModelo" => "", "condensadorSerie" => "", "condensadorBTU" => "");
        }
        return $d;
    }

    public function getListadoEquiposRefrigeracionDetalleMantenimiento(){
        $d = array();
        $sql = "
        #mantenimiento
        (SELECT 
            man_caracteristicas_refrigeracion.id, 
            id_orden, 
            cat_equipos.id_cliente, 
            nombre_area, 
            cat_areas.id AS id_area, 
            cat_areas.`nombre` AS AREA, 
            cat_descripciones_equipos.nombre AS descripcion, 
            man_caracteristicas_refrigeracion.codigo, 
            '' AS evaporador, 
            marca_eva AS marca, 
            CAST(capacidad_hp_eva AS DECIMAL) AS capacidadBTU, 
            modelo_eva AS modelo, 
            serie_eva AS serie, 
            '' condensador, 
            marca_conde AS condensadorMarca, 
            CAST(capacidad_hp_conde AS DECIMAL) AS condensadorBTU, 
            modelo_conde AS condensadorModelo, 
            serie_conde AS condensadorSerie 
        FROM man_caracteristicas_refrigeracion 
        INNER JOIN cat_areas ON cat_areas.id = id_area 
        INNER JOIN cat_descripciones_equipos ON id_equipo_des = cat_descripciones_equipos.id 
        INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id 
        INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id 
        INNER JOIN cat_equipos ON man_caracteristicas_refrigeracion.codigo = cat_equipos.codigo 
        WHERE 
            cat_equipos.status = 1 
            AND DATE(fecha_agendada) = CURRENT_DATE AND cotizaciones.id_cliente = cat_equipos.`id_cliente`
            AND man_caracteristicas_refrigeracion.status = 1
        );
        ";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        if(count($d)==0){
        	$d[] = array("id"=>0, "id_orden"=>0, "id_cliente"=>0, "nombre_area"=>"", "id_area"=>0, "AREA"=>"", "descripcion"=>"", "codigo"=>"", "capacidadBTU"=>"", "modelo"=>"", "serie"=>"", "condensadorModelo"=>"", "condensadorBTU"=>"", "condensadorMarca"=>"", "condensadorSerie"=>"", "marca" => "");
        }
        return $d;
    }

    public function getEvaporadoresRevisionCorrectivo(){
        $d = array();
        $sql = "
        (SELECT 
            cat_equipos.`data_piezas` AS data_equipo, orden_trabajo.id as id_orden
        FROM rev_caracteristicas_refrigeracion 
        INNER JOIN cat_areas ON cat_areas.id = id_area 
        INNER JOIN cat_descripciones_equipos ON id_equipo_des = cat_descripciones_equipos.id 
        INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id 
        INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id 
        INNER JOIN cat_equipos ON rev_caracteristicas_refrigeracion.codigo = cat_equipos.codigo 
        WHERE cat_equipos.status = 1 AND DATE(fecha_agendada) = CURRENT_DATE AND cotizaciones.id_cliente = cat_equipos.`id_cliente` AND rev_caracteristicas_refrigeracion.status = 1)
        ";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $row = (object)$row;
            $row->data_equipo = json_decode($row->data_equipo, true);
            $i = 1;
            foreach($row->data_equipo as $equipo){
                if($equipo["parte"] == "EVAPORADOR"){
                    $equipo["parte"] = "EVAPORADOR #".$i;
                    $equipo["id_orden"] = $row->id_orden;
                    #$equipo["id_motor"] = (((int)$equipo["id_motor"]) > 0) ? "MOTOR ".trim($equipo["id_motor"]) : "S/N";

                    $i++;
                    $d[] = $equipo;
                }
            }
            #$d[] = $row;
        }
        if(count($d)==0){
        	$d[] = array("capacidad" => 0, "id_orden"=>0, "parte"=>"", "id_motor"=>0, "modelo"=>"", "serie"=>"", "marca"=>"");
        }
        return $d;
    }

    public function getEvaporadoresCorrectivo(){
        $d = array();
        $sql = "
        (SELECT 
            cat_equipos.`data_piezas` AS data_equipo, orden_trabajo.id as id_orden
        FROM cor_caracteristicas_refrigeracion 
        INNER JOIN cat_areas ON cat_areas.id = id_area 
        INNER JOIN cat_descripciones_equipos ON id_equipo_des = cat_descripciones_equipos.id 
        INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id 
        INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id 
        INNER JOIN cat_equipos ON cor_caracteristicas_refrigeracion.codigo = cat_equipos.codigo 
        WHERE cat_equipos.status = 1 AND DATE(fecha_agendada) = CURRENT_DATE AND cotizaciones.id_cliente = cat_equipos.`id_cliente` AND cor_caracteristicas_refrigeracion.status = 1)
        ";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $row = (object)$row;
            $row->data_equipo = json_decode($row->data_equipo, true);
            $i = 1;
            foreach($row->data_equipo as $equipo){
                if($equipo["parte"] == "EVAPORADOR"){
                    $equipo["parte"] = "EVAPORADOR #".$i;
                    $equipo["id_orden"] = $row->id_orden;
                    #$equipo["id_motor"] = (((int)$equipo["id_motor"]) > 0) ? "MOTOR ".trim($equipo["id_motor"]) : "S/N";

                    $i++;
                    $d[] = $equipo;
                }
            }
            #$d[] = $row;
        }
        if(count($d)==0){
        	$d[] = array("capacidad" => 0, "id_orden"=>0, "parte"=>"", "id_motor"=>0, "modelo"=>"", "serie"=>"", "marca"=>"");
        }
        return $d;
    }

    public function getEvaporadoresInstalacion(){
        $d = array();
        $sql = "
        (SELECT 
            cat_equipos.`data_piezas` AS data_equipo, orden_trabajo.id as id_orden
        FROM ins_caracteristicas_refrigeracion 
        INNER JOIN cat_areas ON cat_areas.id = id_area 
        INNER JOIN cat_descripciones_equipos ON id_equipo_des = cat_descripciones_equipos.id 
        INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id 
        INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id 
        INNER JOIN cat_equipos ON ins_caracteristicas_refrigeracion.codigo = cat_equipos.codigo 
        WHERE cat_equipos.status = 1 AND DATE(fecha_agendada) = CURRENT_DATE AND cotizaciones.id_cliente = cat_equipos.`id_cliente` AND ins_caracteristicas_refrigeracion.status = 1)
        ";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $row = (object)$row;
            $row->data_equipo = json_decode($row->data_equipo, true);
            $i = 1;
            foreach($row->data_equipo as $equipo){
                if($equipo["parte"] == "EVAPORADOR"){
                    $equipo["parte"] = "EVAPORADOR #".$i;
                    $equipo["id_orden"] = $row->id_orden;
                    #$equipo["id_motor"] = (((int)$equipo["id_motor"]) > 0) ? "MOTOR ".trim($equipo["id_motor"]) : "S/N";

                    $i++;
                    $d[] = $equipo;
                }
            }
            #$d[] = $row;
        }
        if(count($d)==0){
        	$d[] = array("id_orden"=>0, "parte"=>"", "id_motor"=>0, "modelo"=>"", "serie"=>"", "marca"=>"");
        }
        return $d;
    }

    public function getListadoEquiposRefrigeracionDetalleCorrectivo(){
        $d = array();
        $sql = "
        (SELECT cor_caracteristicas_refrigeracion.id, 
            id_orden, 
            cat_equipos.id_cliente, 
            nombre_area, 
            cat_areas.id AS id_area, 
            cat_areas.`nombre` AS AREA, 
            cat_descripciones_equipos.nombre AS descripcion, 
            cor_caracteristicas_refrigeracion.codigo, 
            '' AS evaporador, 
            marca_eva AS marca, 
            marca_conde,
            CAST(capacidad_hp_eva AS DECIMAL) AS capacidadBTU, 
            modelo_eva AS modelo, 
            modelo_conde,
            serie_eva AS serie,
            serie_conde,
            capacidad_hp_conde, 
            cat_equipos.`data_piezas` AS data_equipo
        FROM cor_caracteristicas_refrigeracion 
        INNER JOIN cat_areas ON cat_areas.id = id_area 
        INNER JOIN cat_descripciones_equipos ON id_equipo_des = cat_descripciones_equipos.id 
        INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id 
        INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id 
        INNER JOIN cat_equipos ON cor_caracteristicas_refrigeracion.codigo = cat_equipos.codigo 
        WHERE cat_equipos.status = 1  AND DATE(fecha_agendada) = CURRENT_DATE AND cotizaciones.id_cliente = cat_equipos.`id_cliente` AND cor_caracteristicas_refrigeracion.status = 1)
        ";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $row = (object) $row;
            $row->data_equipo = json_decode($row->data_equipo, true);

            foreach($row->data_equipo as $equipo){
                if($equipo["parte"] == "CONDENSADOR"){
                    $row->cantidad_motor_conde = (int)$equipo["id_motor"];
                }
            }
            unset($row->data_equipo);
            $d[] = $row;
        }
        if(count($d)==0){
            $d[] = array("id"=>0, 
                        "id_orden"=>0, 
                        "id_cliente"=>0, 
                        "nombre_area"=>"", 
                        "id_area"=>0, "AREA"=>"", 
                        "descripcion"=>"", 
                        "codigo"=>"",
                        "evaporador"=>"", 
                        "marca"=>"", 
                        "marca_conde"=>"",
                        "capacidadBTU"=>"", 
                        "modelo"=>"", 
                        "modelo_conde"=>"",
                        "serie"=>"",
                        "serie_conde"=>"",
                        "capacidad_hp_conde"=>"", 
                        "data_equipo"=>"");
        }
        return $d;
    }

    public function getListadoEquiposRefrigeracionDetalleInstalacion(){
        $d = array();
        $sql = "
        (SELECT ins_caracteristicas_refrigeracion.id, 
            id_orden, 
            cat_equipos.id_cliente, 
            nombre_area, 
            cat_areas.id AS id_area, 
            cat_areas.`nombre` AS AREA, 
            cat_descripciones_equipos.nombre AS descripcion, 
            ins_caracteristicas_refrigeracion.codigo, 
            '' AS evaporador, 
            marca_eva AS marca, 
            marca_conde,
            CAST(capacidad_hp_eva AS DECIMAL) AS capacidadBTU, 
            modelo_eva AS modelo, 
            modelo_conde,
            serie_eva AS serie,
            serie_conde,
            capacidad_hp_conde, 
            cat_equipos.`data_piezas` AS data_equipo,
            '' as cantidadCompresores,
            '' as capacidadVentilador,
            '' as cantidadMotorVentilador
        FROM ins_caracteristicas_refrigeracion 
        INNER JOIN cat_areas ON cat_areas.id = id_area 
        INNER JOIN cat_descripciones_equipos ON id_equipo_des = cat_descripciones_equipos.id 
        INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id 
        INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id 
        INNER JOIN cat_equipos ON ins_caracteristicas_refrigeracion.codigo = cat_equipos.codigo 
        WHERE cat_equipos.status = 1  AND DATE(fecha_agendada) = CURRENT_DATE AND cotizaciones.id_cliente = cat_equipos.`id_cliente` AND ins_caracteristicas_refrigeracion.status = 1)
        ";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $row = (object) $row;
            $row->data_equipo = json_decode($row->data_equipo, true);

            foreach($row->data_equipo as $equipo){
                if($equipo["parte"] == "CONDENSADOR"){
                    $row->cantidad_motor_conde = (int)$equipo["id_motor"];
                }
            }
            unset($row->data_equipo);
            $d[] = $row;
        }
        if(count($d)==0){
        	$d[] = array("id"=>0, "id_orden"=>0, "id_cliente"=>0, "nombre_area"=>"", "id_area"=>0, "AREA"=>"", "descripcion"=>"", "codigo"=>"",
        		"evaporador"=>"", "marca"=>"", "marca_conde"=>"", "capacidadBTU"=>0, "modelo"=>"", "modelo_conde"=>"", "serie"=>"", "serie_conde"=>"",
        		"capacidad_hp_conde"=>"", "data_equipo"=>"", "cantidadCompresores"=>"", "capacidadVentilador"=>"", "cantidadMotorVentilador"=>"");
        }
        return $d;
    }

    public function getListadoRefrigeracionDetalleRevisionCorrectivo(){
        $d = array();
        $sql = "
        (SELECT rev_caracteristicas_refrigeracion.id, 
            id_orden, 
            cat_equipos.id_cliente, 
            nombre_area, 
            cat_areas.id AS id_area, 
            cat_areas.`nombre` AS AREA, 
            cat_descripciones_equipos.nombre AS descripcion, 
            rev_caracteristicas_refrigeracion.codigo, 
            '' AS evaporador, 
            marca_eva AS marca, 
            marca_conde,
            CAST(capacidad_hp_eva AS DECIMAL) AS capacidadBTU, 
            capacidad_hp_conde,
            modelo_eva AS modelo, 
            modelo_conde,
            serie_eva AS serie,
            serie_conde,
            marca_conde AS cantidadCompresores,
            CAST(capacidad_hp_conde AS DECIMAL) AS capacidadVentilador, 
            serie_conde AS cantidadMotorVentilador,
            cat_equipos.`data_piezas` AS data_equipo
        FROM rev_caracteristicas_refrigeracion 
        INNER JOIN cat_areas ON cat_areas.id = id_area 
        INNER JOIN cat_descripciones_equipos ON id_equipo_des = cat_descripciones_equipos.id 
        INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id 
        INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id 
        INNER JOIN cat_equipos ON rev_caracteristicas_refrigeracion.codigo = cat_equipos.codigo 
        WHERE cat_equipos.status = 1 AND DATE(fecha_agendada) = CURRENT_DATE AND cotizaciones.id_cliente = cat_equipos.`id_cliente` AND rev_caracteristicas_refrigeracion.status = 1)
        ";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $row = (object) $row;
            $row->data_equipo = json_decode($row->data_equipo, true);

            foreach($row->data_equipo as $equipo){
                if($equipo["parte"] == "CONDENSADOR"){
                    $row->cantidad_motor_conde = (int)$equipo["id_motor"];
                }
            }
            unset($row->data_equipo);
            $d[] = $row;
        }
        if(count($d)==0){
        	$d[] = array("cantidad_motor_conde" => 0, "id"=>0, "id_orden"=>0, "id_cliente"=>0, "marca_conde"=>"", "marca"=>"", "modelo"=>"", "modelo_conde"=>"", "serie"=>"", "serie_conde"=>"", "capacidadBTU"=>"", "codigo"=>"", "descripcion"=>"", "capacidad_hp_conde"=>"", "capacidadVentilador"=>"", "cantidadMotorVentilador"=>"");
        }
        return $d;
    }

    public function getListadoEquiposVentilacionDetalleCorrectivo(){
        $d = array();
        $sql = "
        (SELECT 
            cor_caracteristicas_ventilacion.id, 
            id_orden, 
            cat_equipos.id_cliente, 
            nombre_area, cat_areas.id AS id_area, 
            cat_areas.`nombre` AS AREA, 
            cat_descripciones_equipos.nombre AS descripcion, 
            cor_caracteristicas_ventilacion.codigo, 
            '' AS evaporador,
            IF(marca IS NULL,'',marca) AS marca, 
            IF(CAST(capacidadHP AS DECIMAL) IS NULL,'', CAST(capacidadHP AS DECIMAL)) AS capacidadHP,
            IF(CAST(capacidadCFN AS DECIMAL) IS NULL,'',CAST(capacidadCFN AS DECIMAL)) AS capacidadCFM,
            IF(modelo IS NULL,'',modelo) AS modelo, 
            IF(serie IS NULL,'',serie) AS serie 
        FROM cor_caracteristicas_ventilacion 
        INNER JOIN cat_areas ON cat_areas.id = AREA 
        INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.id 
        INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id 
        INNER JOIN cat_equipos ON cor_caracteristicas_ventilacion.codigo = cat_equipos.codigo 
        WHERE
            cat_equipos.status = 1 
            AND DATE(fecha_agendada) = CURRENT_DATE 
            AND cotizaciones.id_cliente = cat_equipos.`id_cliente`
            AND cor_caracteristicas_ventilacion.status = 1
        );
        ";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        if(count($d)==0){
        	$d[] = array("id"=>0, "id_orden"=>0, "id_cliente"=>0, "id_area"=>0, "descripcion"=>"", "codigo"=>"", "marca"=>"", "capacidadHP"=>"", "capacidadCFM"=>"", "modelo"=>"", "serie"=>"");
        }
        return $d;
    }

    public function getListadoEquiposVentilacionRevisionCorrectivo(){
        $d = array();
        $sql = "
        (SELECT 
            rev_caracteristicas_ventilacion.id, 
            id_orden, 
            cat_equipos.id_cliente, 
            nombre_area, cat_areas.id AS id_area, 
            cat_areas.`nombre` AS AREA, 
            cat_descripciones_equipos.nombre AS descripcion, 
            rev_caracteristicas_ventilacion.codigo, 
            '' AS evaporador,
            IF(marca IS NULL,'',marca) AS marca, 
            IF(CAST(capacidadHP AS DECIMAL) IS NULL,'', CAST(capacidadHP AS DECIMAL)) AS capacidadHP,
            IF(CAST(capacidadCFN AS DECIMAL) IS NULL,'',CAST(capacidadCFN AS DECIMAL)) AS capacidadCFM,
            IF(modelo IS NULL,'',modelo) AS modelo, 
            IF(serie IS NULL,'',serie) AS serie 
        FROM rev_caracteristicas_ventilacion 
        INNER JOIN cat_areas ON cat_areas.id = AREA 
        INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.id 
        INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id 
        INNER JOIN cat_equipos ON rev_caracteristicas_ventilacion.codigo = cat_equipos.codigo 
        WHERE 
            cat_equipos.status = 1 
            AND DATE(fecha_agendada) = CURRENT_DATE 
            AND cotizaciones.id_cliente = cat_equipos.`id_cliente`
            AND rev_caracteristicas_ventilacion.status = 1
        )";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        if(count($d)==0){
        	$d[] = array("id"=>0, "id_orden"=>0, "id_cliente"=>0, "id_area"=>0, "descripcion"=>"", "codigo"=>"NO HAY EQUIPOS", "marca"=>"", "capacidadHP"=>"", "capacidadCFM"=>"", "modelo"=>"", "serie"=>"");
        }
        return $d;
    }

    public function getListadoEquiposVentilacionDetalleInstalacion(){
        $d = array();
        $sql = "
        #mantenimiento
        (SELECT 
            ins_caracteristicas_ventilacion.id, 
            id_orden, 
            cat_equipos.id_cliente, 
            nombre_area, cat_areas.id AS id_area, 
            cat_areas.`nombre` AS AREA, 
            cat_descripciones_equipos.nombre AS descripcion, 
            ins_caracteristicas_ventilacion.codigo, 
            '' AS evaporador,
            marca, 
            CAST(capacidadHP AS DECIMAL) AS capacidadHP,
            IFNULL(CAST(capacidadCFN AS DECIMAL), 0) AS capacidadCFM,
            modelo AS modelo, 
            serie AS serie 
        FROM ins_caracteristicas_ventilacion 
        INNER JOIN cat_areas ON cat_areas.id = AREA 
        INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.id 
        INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id 
        INNER JOIN cat_equipos ON ins_caracteristicas_ventilacion.codigo = cat_equipos.codigo 
        WHERE
            cat_equipos.status = 1  
            AND DATE(fecha_agendada) = CURRENT_DATE 
            AND cotizaciones.id_cliente = cat_equipos.`id_cliente`
            AND ins_caracteristicas_ventilacion.status = 1
        );
        ";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        if(count($d)==0){
        	$d[] = array("id"=>0, "id_orden"=>0, "id_cliente"=>0, "nombre_area"=>"", "id_area"=>0, "AREA"=>"", "descripcion"=>"", "codigo"=>"NO HAY EQUIPOS", "evaporador"=>"",
        		"marca"=>"", "capacidadHP"=>"", "capacidadCFM"=>"", "modelo"=>"", "serie"=>"");
        }
        return $d;
    }

    public function getListadoEquiposVentilacionDetalleMantenimiento(){
        $d = array();
        $sql = "
        #mantenimiento
        (SELECT man_caracteristicas_ventilacion.id, 
        	id_orden, 
			cat_equipos.id_cliente, 
			nombre_area, 
			cat_areas.id AS id_area, 
			cat_areas.`nombre` AS AREA, 
			cat_descripciones_equipos.nombre AS descripcion, 
			man_caracteristicas_ventilacion.codigo, 
			'' AS evaporador,
			marca, 
			CAST(capacidadHP AS DECIMAL) AS capacidadHP, 
			modelo AS modelo, 
			serie AS serie 
		FROM man_caracteristicas_ventilacion INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id INNER JOIN cat_equipos ON man_caracteristicas_ventilacion.codigo = cat_equipos.codigo WHERE cat_equipos.status = 1 AND DATE(fecha_agendada) = CURRENT_DATE AND cotizaciones.id_cliente = cat_equipos.`id_cliente` AND man_caracteristicas_ventilacion.status = 1)
        ";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        if(count($d)==0){
        	$d[] = array("id"=>0, "id_cliente"=>0, "nombre_area"=>"", "id_area"=>0, "AREA"=>"", "descripcion"=>"", "AREA" => "", "codigo"=>"NO HAY EQUIPOS", "evaporador"=>"", "marca"=>"", "capacidadHP"=>"", "modelo"=>"", "serie"=>"");
        }
        return $d;
    }

    public function getListadoEquiposClimatizacionBDetalle(){
    	$d = array();
        $sql = "
#mantenimiento
(SELECT man_caracteristicas_climatizacion_b.id, id_orden, cat_equipos.id_cliente, nombre_area, cat_areas.id AS id_area, cat_areas.`nombre` AS AREA, cat_descripciones_equipos.nombre AS descripcion, man_caracteristicas_climatizacion_b.codigo, evaporador, marca, CAST(capacidadBTU AS DECIMAL) AS capacidadBTU, modelo, serie, condensador, condensadorMarca, CAST(condensadorBTU AS DECIMAL) AS condensadorBTU, condensadorModelo, condensadorSerie FROM man_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id INNER JOIN cat_equipos ON man_caracteristicas_climatizacion_b.codigo = cat_equipos.codigo WHERE cat_equipos.status = 1 AND DATE(fecha_agendada) = CURRENT_DATE AND man_caracteristicas_climatizacion_b.status = 1) 

#instalacion
UNION
(SELECT ins_caracteristicas_climatizacion_b.id, id_orden, cat_equipos.id_cliente, nombre_area, cat_areas.id AS id_area, cat_areas.`nombre` AS AREA, cat_descripciones_equipos.nombre AS descripcion, ins_caracteristicas_climatizacion_b.codigo, evaporador, marca, CAST(capacidadBTU AS DECIMAL) AS capacidadBTU, modelo, serie, condensador, condensadorMarca, CAST(condensadorBTU AS DECIMAL) AS condensadorBTU, condensadorModelo, condensadorSerie FROM ins_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id INNER JOIN cat_equipos ON ins_caracteristicas_climatizacion_b.codigo = cat_equipos.codigo WHERE cat_equipos.status = 1 AND DATE(fecha_agendada) = CURRENT_DATE AND ins_caracteristicas_climatizacion_b.status = 1)

#correctivo
UNION
(SELECT cor_caracteristicas_climatizacion_b.id, id_orden, cat_equipos.id_cliente, nombre_area, cat_areas.id AS id_area, cat_areas.`nombre` AS AREA, cat_descripciones_equipos.nombre AS descripcion, cor_caracteristicas_climatizacion_b.codigo, evaporador, marca, CAST(capacidadBTU AS DECIMAL) AS capacidadBTU, modelo, serie, condensador, condensadorMarca, CAST(condensadorBTU AS DECIMAL) AS condensadorBTU, condensadorModelo, condensadorSerie FROM cor_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id INNER JOIN cat_equipos ON cor_caracteristicas_climatizacion_b.codigo = cat_equipos.codigo WHERE cat_equipos.status = 1 AND DATE(fecha_agendada) = CURRENT_DATE AND cor_caracteristicas_climatizacion_b.status = 1)
        ";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        if(count($d)==0){
        	$d[] = array("id"=>0, "id_orden"=>0, "id_cliente"=>0, "nombre_area"=>"", "id_area"=>0, "AREA"=>"", "descripcion"=>"", "codigo"=>"NO HAY EQUIPOS", "evaporador"=>"",
        		"marca"=>"", "capacidadBTU"=>"", "modelo"=>"", "serie"=>"", "condensador"=>"", "condensadorMarca"=>"", "condensadorBTU"=>"", "condensadorModelo"=>"", "condensadorSerie"=>"");
        }
        return $d;
    }

    public function getListadoEquiposClimatizacionA(){
    	$d = array();
        $sql = "
#mantenimiento
(SELECT id_orden, CONCAT(cat_areas.`nombre`,' - ',descripcion,' - ',codigo,' - ',marca,' - ',capacidadBTU,' - ',modelo,' - ',serie) AS reg FROM man_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id HAVING reg IS NOT NULL) 

#instalacion
UNION
(SELECT  id_orden, CONCAT(cat_areas.`nombre`,' - ',descripcion,' - ',codigo,' - ',marca,' - ',capacidadBTU,' - ',modelo,' - ',serie) AS reg FROM ins_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id HAVING reg IS NOT NULL) 

#correctivo
UNION
(SELECT  id_orden, CONCAT(cat_areas.`nombre`,' - ',descripcion,' - ',codigo,' - ',marca,' - ',capacidadBTU,' - ',modelo,' - ',serie) AS reg FROM cor_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id HAVING reg IS NOT NULL) 
        ";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }

    public function getListadoEquiposClimatizacionADetalle(){
    	$d = array();
        $sql = "
#mantenimiento
(SELECT man_caracteristicas_climatizacion_a.id, cat_areas.id AS id_area, cat_equipos.id_cliente, nombre_area, cat_areas.`nombre` AS AREA, id_orden, cat_descripciones_equipos.nombre AS descripcion, cat_equipos.codigo, marca, CAST(capacidadBTU AS DECIMAL) AS capacidadBTU, modelo, serie FROM man_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id INNER JOIN cat_equipos ON man_caracteristicas_climatizacion_a.codigo = cat_equipos.codigo WHERE cat_equipos.status = 1 AND DATE(fecha_agendada) = CURRENT_DATE AND man_caracteristicas_climatizacion_a.status = 1)  

#instalacion
UNION
(SELECT ins_caracteristicas_climatizacion_a.id, cat_areas.id AS id_area, cat_equipos.id_cliente, nombre_area, cat_areas.`nombre` AS AREA, id_orden, cat_descripciones_equipos.nombre AS descripcion, cat_equipos.codigo, marca, CAST(capacidadBTU AS DECIMAL) AS capacidadBTU, modelo, serie FROM ins_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id INNER JOIN cat_equipos ON ins_caracteristicas_climatizacion_a.codigo = cat_equipos.codigo WHERE cat_equipos.status = 1 AND DATE(fecha_agendada) = CURRENT_DATE  AND ins_caracteristicas_climatizacion_a.status = 1) 

#correctivo
UNION
(SELECT cor_caracteristicas_climatizacion_a.id, cat_areas.id AS id_area, cat_equipos.id_cliente, nombre_area, cat_areas.`nombre` AS AREA, id_orden, cat_descripciones_equipos.nombre AS descripcion, cat_equipos.codigo, marca, CAST(capacidadBTU AS DECIMAL) AS capacidadBTU, modelo, serie FROM cor_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id INNER JOIN cat_equipos ON cor_caracteristicas_climatizacion_a.codigo = cat_equipos.codigo WHERE cat_equipos.status = 1 AND DATE(fecha_agendada) = CURRENT_DATE AND cor_caracteristicas_climatizacion_a.status = 1)
";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        if(count($d)==0){
        	$d[] = array("id"=>0, "id_orden"=>0, "id_cliente"=>0, "nombre_area"=>"", "id_area"=>0, "AREA"=>"", "descripcion"=>"", "codigo"=>"NO HAY EQUIPOS",
        		"marca"=>"", "capacidadBTU"=>"", "modelo"=>"", "serie"=>"");
        }
        return $d;
    }

    public function getListadoEquiposClimatizacionADetalleMantenimiento(){
    	$d = array();
        $sql = "
#mantenimiento
(SELECT man_caracteristicas_climatizacion_a.id, cat_areas.id AS id_area, cat_equipos.id_cliente, nombre_area, cat_areas.`nombre` AS AREA, id_orden, cat_descripciones_equipos.nombre AS descripcion, cat_equipos.codigo, marca, CAST(capacidadBTU AS DECIMAL) AS capacidadBTU, modelo, serie FROM man_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id INNER JOIN cat_equipos ON man_caracteristicas_climatizacion_a.codigo = cat_equipos.codigo WHERE cat_equipos.status = 1 AND DATE(fecha_agendada) = CURRENT_DATE AND cotizaciones.id_cliente = cat_equipos.`id_cliente` AND man_caracteristicas_climatizacion_a.status = 1)
";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        if(count($d)==0){
        	$d[] = array("id" => 0, "id_area" => 0, "id_cliente" => 0, "nombre_area" => "", "AREA" => 0, "id_orden" => 0, "descripcion" => "",
        		"codigo" => "NO HAY EQUIPOS", "marca" => "", "capacidadBTU" => "", "modelo" => "", "serie" => "");
        }
        return $d;
    }

    public function getListadoEquiposClimatizacionADetalleInstalacion(){
    	$d = array();
        $sql = "
#instalacion
(SELECT ins_caracteristicas_climatizacion_a.id, cat_areas.id AS id_area, cat_equipos.id_cliente, nombre_area, cat_areas.`nombre` AS AREA, id_orden, cat_descripciones_equipos.nombre AS descripcion, cat_equipos.codigo, marca, CAST(capacidadBTU AS DECIMAL) AS capacidadBTU, modelo, serie FROM ins_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id INNER JOIN cat_equipos ON ins_caracteristicas_climatizacion_a.codigo = cat_equipos.codigo WHERE cat_equipos.status = 1 AND DATE(fecha_agendada) = CURRENT_DATE AND cotizaciones.id_cliente = cat_equipos.`id_cliente` AND ins_caracteristicas_climatizacion_a.status = 1)
";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        if(count($d)==0){
        	$d[] = array("id" => 0, "id_area" => 0, "id_cliente" => 0, "nombre_area" => "", "AREA" => 0, "id_orden" => 0, "descripcion" => "",
        		"codigo" => "NO HAY EQUIPOS", "maraca" => "", "capacidadBTU" => "", "modelo" => "", "serie" => "");
        }
        return $d;
    }

    public function getListadoEquiposClimatizacionADetalleCorrectivo(){
    	$d = array();
        $sql = "SELECT 
                cor_caracteristicas_climatizacion_a.id, 
                cat_areas.id AS id_area, 
                cat_equipos.id_cliente, 
                cat_equipos.id as id_equipo,
                nombre_area, 
                cat_areas.`nombre` AS AREA, 
                id_orden, 
                cat_descripciones_equipos.nombre AS descripcion, 
                cat_equipos.codigo, 
                marca, 
                CAST(capacidadBTU AS DECIMAL) AS capacidadBTU, 
                modelo, 
                serie 
            FROM cor_caracteristicas_climatizacion_a 
            INNER JOIN cat_areas ON AREA = cat_areas.id 
            INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.id 
            INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id 
            INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id 
            INNER JOIN cat_equipos ON cor_caracteristicas_climatizacion_a.codigo = cat_equipos.codigo
            WHERE 
                cat_equipos.status = 1 
                AND DATE(fecha_agendada) = CURRENT_DATE 
                AND cotizaciones.id_cliente = cat_equipos.`id_cliente` 
                AND cor_caracteristicas_climatizacion_a.status = 1";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        if(count($d)==0){
        	$d[] = array("id" => 0, "id_equipo" => 0, "id_area" => 0, "id_cliente" => 0, "nombre_area" => "", "AREA" => 0, "id_orden" => 0, "descripcion" => "",
        		"codigo" => "NO HAY EQUIPOS", "marca" => "", "capacidadBTU" => "", "modelo" => "", "serie" => "");
        }
        return $d;
    }

    public function getListadoEquiposClimatizacionADetalleRevisionCorrectivo(){
        $d = array();
        $sql = "SELECT 
                rev_caracteristicas_climatizacion_a.id, 
                cat_areas.id AS id_area, 
                cat_equipos.id as id_equipo,
                cat_equipos.id_cliente, 
                nombre_area, cat_areas.`nombre` AS AREA, 
                id_orden, 
                cat_descripciones_equipos.nombre AS descripcion, 
                cat_equipos.codigo, 
                marca, 
                CAST(capacidadBTU AS DECIMAL) AS capacidadBTU, 
                modelo, 
                serie 
            FROM rev_caracteristicas_climatizacion_a 
            INNER JOIN cat_areas ON AREA = cat_areas.id 
            INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.id 
            INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id 
            INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id 
            INNER JOIN cat_equipos ON rev_caracteristicas_climatizacion_a.codigo = cat_equipos.codigo 
            WHERE 
                cat_equipos.status = 1 
                AND DATE(fecha_agendada) = CURRENT_DATE 
                AND cotizaciones.id_cliente = cat_equipos.`id_cliente` 
                AND rev_caracteristicas_climatizacion_a.status = 1
        ";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        if(count($d)==0){
        	$d[] = array("id" => 0, "id_equipo" => 0, "id_area" => 0, "id_cliente" => 0, "nombre_area" => "", "AREA" => 0, "id_orden" => 0, "descripcion" => "",
        		"codigo" => "NO HAY EQUIPOS", "marca" => "", "capacidadBTU" => "", "modelo" => "", "serie" => "");
        }
        return $d;
    }

    public function getListadoEquiposClimatizacionBDetalleRevisionCorrectivo(){
        $d = array();
        $sql = "
        #correctivo 
                (SELECT rev_caracteristicas_climatizacion_b.id, cat_areas.id AS id_area, cat_equipos.id_cliente, nombre_area, cat_areas.`nombre` AS AREA, id_orden, cat_descripciones_equipos.nombre AS descripcion, cat_equipos.codigo, marca, CAST(capacidadBTU AS DECIMAL) AS capacidadBTU, modelo, serie FROM rev_caracteristicas_climatizacion_b INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id INNER JOIN cat_equipos ON rev_caracteristicas_climatizacion_b.codigo = cat_equipos.codigo WHERE cat_equipos.status = 1 AND DATE(fecha_agendada) = CURRENT_DATE AND cotizaciones.id_cliente = cat_equipos.`id_cliente` AND rev_caracteristicas_climatizacion_b.status = 1)
        ";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        if(count($d)==0){
        	$d[] = array("id" => 0, "id_area" => 0, "id_cliente" => 0, "nombre_area" => "", "AREA" => 0, "id_orden" => 0, "descripcion" => "",
        		"codigo" => "NO HAY EQUIPOS", "marca" => "", "capacidadBTU" => "", "modelo" => "", "serie" => "");
        }
        return $d;
    }

    public function getListadoMateriales(){
        $d = array();
        $sql = "
            SELECT cat_equipos.`codigo`, cotizaciones.`id_cliente`, CONCAT(cat_equipos.`codigo`, '-', cotizaciones.`id_cliente`) AS codigo_cliente, CONCAT(cat_equipos.codigo,' - ',seleccion,' - ',descripcion,' - ',unidad,' - ',cantidad_requerida) AS descripcion, id_orden  
            FROM orden_trabajo_materiales 
            INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id 
            INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id
            INNER JOIN cat_equipos ON orden_trabajo_materiales.`codigo` = cat_equipos.`codigo`
            WHERE cat_equipos.status = 1 AND cat_equipos.`id_cliente` = cotizaciones.`id_cliente` AND orden_trabajo_materiales.status = 1
            HAVING descripcion IS NOT NULL";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }

    public function getListadoHerramientas(){
        $d = array();
        $sql = "SELECT CONCAT(herramientas,' - ',requerimientos) AS herramientas, id_orden FROM orden_trabajo_herramientas WHERE orden_trabajo_herramientas.status = 1 HAVING herramientas IS NOT NULL";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        if(count($d)==0){
            $d[] = array("id"=>0, "herramientas"=>"");
        }
        return $d;
    }

    public function getListadoRepuestos(){
        $d = array();
        $sql = "
        SELECT cat_equipos.`codigo`, cotizaciones.`id_cliente`, CONCAT(cat_equipos.`codigo`, '-', cotizaciones.`id_cliente`) AS codigo_cliente,CONCAT(cat_equipos.codigo,' - ',parte,' - ',descripcion,' - ',item,' - ',cantidad) AS descripcion, id_orden 
        FROM orden_trabajo_repuestos 
        INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id 
        INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id
        INNER JOIN cat_equipos ON orden_trabajo_repuestos.`codigo` = cat_equipos.`codigo`
        WHERE cat_equipos.status = 1 AND cat_equipos.`id_cliente` = cotizaciones.`id_cliente` AND orden_trabajo_repuestos.status = 1
        HAVING descripcion IS NOT NULL";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }
	
	public function getTipoEquipo(){
        $d = array();
        $sql = "SELECT id, nombre FROM cat_equiposTipo where status=1";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }

    public function getDescriptionEquipo(){
        $d = array();
        $sql = "SELECT id , 
            (SELECT nombre FROM cat_equiposTipo WHERE id = id_tipoequipo) AS id_tipoequipo ,  
                nombre
            FROM cat_descripciones_equipos where status=1";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }

    public function getHerramientas(){
        $d = array();
        $sql = "SELECT id , IF(especificacion = NULL , herramienta ,CONCAT_WS('-',herramienta ,especificacion)) AS nombre FROM cat_herramientas_trabajo where status=1";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }

    public function getSucursalByCliente($clienteId){
        $d = array();
        $sql = "SELECT id, id_cliente, nombre_contacto FROM cat_sucursales WHERE id_cliente = $clienteId AND id_usuario = 2 AND status = 1";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }
	
	public function getClienteByOrden($clienteId){
        $d = array();
        $sql = "SELECT cat_clientes.id, cat_clientes.nombre, cat_clientes.direccion, 
			cat_clientesTipo.nombre as tipo_cliente FROM cat_clientes join cat_clientesTipo 
			on cat_clientesTipo.id = cat_clientes.id_tipcli WHERE cat_clientes.id = $clienteId AND cat_clientes.id_usuario = 2 AND status = 1";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }
	
	public function getContactoByOrden($clienteId){
        $d = array();
        $sql = "SELECT * from cat_clientes_contactos WHERE id_cliente = $clienteId";
		
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }
	
	public function getAreaClimatizada(){
        $d = array();
        $sql = "SELECT * FROM cat_areas where status=1";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }
	public function getCapacidadBUTU(){
        $d = array();
        $sql = "SELECT * FROM cat_capacidadBUTU where status=1";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }
	public function getCapacidadHP(){
        $d = array();
        $sql = "SELECT * FROM cat_capacidadHP where status=1";
        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }

    public function getHorasOcupadas(){
    	$sql = "SELECT orden_trabajo.responsable, orden_trabajo.id AS id_orden, ids_auxiliares, id_vehiculo, (SELECT nombre FROM cat_vehiculos WHERE id = id_vehiculo) as vehiculo, CONCAT(TIME(fecha_agendada), DATE_FORMAT(fecha_agendada, ' %p')) AS fecha_agendada, CONCAT(TIME(fecha_fin), DATE_FORMAT(fecha_fin, ' %p')) AS fecha_fin, id_responsable as grupo_trabajo 
    	FROM orden_trabajo 
        LEFT JOIN orden_trabajo_responsables ON orden_trabajo_responsables.id_orden = orden_trabajo.id
    	WHERE DATE(fecha_agendada) = DATE(CURRENT_TIMESTAMP)";
    	$resultado = $this->mysqli->query($sql);
    	$rows = array();
    	while($row = $resultado->fetch_assoc()){
    		$aux = explode(",", $row["ids_auxiliares"]);
    		$row["auxs"] = "";

    		foreach($aux as $a){
	    		$sql2 = "SELECT * FROM cat_auxiliares WHERE id = '{$a}'";
	    		$res = $this->mysqli->query($sql2);
	    		while($r = $res->fetch_assoc()){
	    			$row["auxs"] .= $r["nombre"].",";
	    		}
	    	}
    		$rows[] = $row;
    	}
    	if(count($rows)==0){
    		$rows[] = array("responsable" => "", "id_orden" => 0,"id_vehiculo" => "", "vehiculo" => "", "auxs" => "","orden_trabajo"=>0, "fecha_agendada"=>"SIN ORDENES");
    	}
    	return $rows;
    }

    public function getHorasInstalacion(){
    	$sql = "SELECT CONCAT(TIME(fecha_agendada), DATE_FORMAT(fecha_agendada, ' %p')) AS fecha_agendada, CONCAT(TIME(fecha_fin), DATE_FORMAT(fecha_fin, ' %p')) AS fecha_fin, id_responsable as grupo_trabajo 
        FROM orden_trabajo 
        LEFT JOIN orden_trabajo_responsables ON orden_trabajo_responsables.id_orden = orden_trabajo.id
        WHERE tipo_trabajo LIKE '%INSTALACION%' AND DATE(fecha_agendada) = DATE(CURRENT_TIMESTAMP)";
    	$resultado = $this->mysqli->query($sql);
    	$rows = array();
    	while($row = $resultado->fetch_assoc()){
    		$rows[] = $row;
    	}
    	if(count($rows)==0){
    		$rows[] = array("fecha_agendada"=>"SIN ORDENES", "fecha_fin"=>"", "grupo_trabajo"=>0);
    	}
    	return $rows;
    }

    public function getHorasMantenimiento(){
    	$sql = "SELECT CONCAT(TIME(fecha_agendada), DATE_FORMAT(fecha_agendada, ' %p')) AS fecha_agendada, CONCAT(TIME(fecha_fin), DATE_FORMAT(fecha_fin, ' %p')) AS fecha_fin, id_responsable as grupo_trabajo 
        FROM orden_trabajo 
        LEFT JOIN orden_trabajo_responsables ON orden_trabajo_responsables.id_orden = orden_trabajo.id
        WHERE tipo_trabajo LIKE '%MANTENIMIENTO%' AND DATE(fecha_agendada) = DATE(CURRENT_TIMESTAMP);";
    	$resultado = $this->mysqli->query($sql);
    	$rows = array();
    	while($row = $resultado->fetch_assoc()){
    		$rows[] = $row;
    	}
    	if(count($rows)==0){
    		$rows[] = array("fecha_agendada"=>"SIN ORDENES", "fecha_fin"=>"", "grupo_trabajo"=>0);
    	}
    	return $rows;
    }

    public function getHorasCorrectivo(){
    	$sql = "SELECT CONCAT(TIME(fecha_agendada), DATE_FORMAT(fecha_agendada, ' %p')) AS fecha_agendada, CONCAT(TIME(fecha_fin), DATE_FORMAT(fecha_fin, ' %p')) AS fecha_fin, id_responsable as grupo_trabajo 
        FROM orden_trabajo 
        LEFT JOIN orden_trabajo_responsables ON orden_trabajo_responsables.id_orden = orden_trabajo.id
        WHERE tipo_trabajo LIKE '%CORRECTIVO%' AND DATE(fecha_agendada) = DATE(CURRENT_TIMESTAMP);";
    	$resultado = $this->mysqli->query($sql);
    	$rows = array();
    	while($row = $resultado->fetch_assoc()){
    		$rows[] = $row;
    	}
    	if(count($rows)==0){
    		$rows[] = array("fecha_agendada"=>"SIN ORDENES", "fecha_fin"=>"", "grupo_trabajo"=>0);
    	}
    	return $rows;
    }
	
	public function getOrdenesTrabajo(){
        $d = array();
		//$sql = "SELECT * FROM cat_capacidadHP where status=1";
        $sql = "SELECT orden_trabajo.id, orden_trabajo.observaciones, cotizaciones.id_sucursal, cotizaciones.id_cliente, orden_trabajo.id_cotizacion, TIME(fecha_agendada) AS hora FROM orden_trabajo
JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
WHERE DATE(fecha_agendada) = CURRENT_DATE() AND orden_trabajo.status = 2 GROUP BY hora";
		
        $resultado = $this->mysqli->query($sql);
		//return $resultado;
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }

        return $d;
    }

    public function getOrdenesTrabajoProntoforms(){
        $d = array();
        $sql = "SELECT orden_trabajo.id, orden_trabajo.observaciones, cotizaciones.id_sucursal, cotizaciones.id_cliente, orden_trabajo.id_cotizacion, TIME(fecha_agendada) AS hora FROM orden_trabajo
JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
WHERE DATE(fecha_agendada) = CURRENT_DATE() AND orden_trabajo.status = 2 GROUP BY hora";
        
        $resultado = $this->mysqli->query($sql);
        //return $resultado;
        while($row = $resultado->fetch_assoc()){
            #$row["sucursal"] = $this->getSucusalByCotizacion($row["id_cotizacion"]);
            $row["nombre_cliente"] = $this->getClienteByOrden($row["id_cliente"])[0]["nombre"];
            $row["direccion_cliente"] = $this->getClienteByOrden($row["id_cliente"])[0]["direccion"];
            $row["tipo_cliente"] = $this->getClienteByOrden($row["id_cliente"])[0]["tipo_cliente"];
            #$row["detalles"] = $this->getOrdenesTrabajoDetalles($row["id"])[0];
            #$row["materiales"] = $this->getOrdenesTrabajoMateriales($row["id"]);
            $d[] = $row;
        }
        return $d;
    }

    private function getSucusalByCotizacion($id){
        $sql = "SELECT cat_sucursales.id, nombre_contacto, razon_social, ruc, direccion, telefono, ciudad FROM cotizaciones INNER JOIN cat_sucursales ON id_sucursal = cat_sucursales.id WHERE cotizaciones.id = {$id}";

        $resultado = $this->mysqli->query($sql);
        while($row = $resultado->fetch_assoc()){
            $d = $row;
        }
        return $d;
    }

    private function getOrdenesTrabajoDetalles($id){
        $sql = "SELECT *, orden_trabajo.observaciones as obser FROM orden_trabajo  
                    INNER JOIN cotizaciones 
                    ON orden_trabajo.id_cotizacion = cotizaciones.id 
                    INNER JOIN cat_clientes 
                    ON  cotizaciones.id_cliente = cat_clientes.id
                    WHERE cotizaciones.id = {$id}";

        $resultado = $this->mysqli->query($sql);
        $d = array();
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }

    private function getOrdenesTrabajoMateriales($id){
        $sql = "SELECT orden_trabajo_materiales.id, orden_trabajo_materiales.seleccion AS item, orden_trabajo_materiales.descripcion
AS descripcion_item, orden_trabajo_materiales.unidad, orden_trabajo_materiales.cantidad_requerida AS cantidad, orden_trabajo_materiales.codigo as codigo FROM orden_trabajo_materiales  
                    JOIN orden_trabajo 
                    ON orden_trabajo.id = orden_trabajo_materiales.id_orden               
                    JOIN cotizaciones
                    ON  cotizaciones.id = orden_trabajo.id_cotizacion
                    WHERE cotizaciones.id ='{$id}' and orden_trabajo_materiales.status=1";

        $resultado = $this->mysqli->query($sql);
        $d = array();
        while($row = $resultado->fetch_assoc()){
            $d[] = $row;
        }
        return $d;
    }
	
	public function getEquipoByOrden($ordenId){
        $d = array();
        $sql2 = "SELECT CONCAT(cat_areas.nombre,' - ', cat_descripciones_equipos.nombre, ' - ',ins_caracteristicas_climatizacion_a.codigo, ' - ', ins_caracteristicas_climatizacion_a.marca, ' - ', ins_caracteristicas_climatizacion_a.capacidadBTU, ' - ' , ins_caracteristicas_climatizacion_a.modelo, ' - ', ins_caracteristicas_climatizacion_a.serie) as equipo, ins_caracteristicas_climatizacion_a.id_orden,  ins_caracteristicas_climatizacion_a.id FROM ins_caracteristicas_climatizacion_a JOIN cat_areas ON cat_areas.id = ins_caracteristicas_climatizacion_a.area JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = ins_caracteristicas_climatizacion_a.descripcion WHERE id_orden = ".$ordenId;
				$resultado2 = $this->mysqli->query($sql2);
				$equipos = array(); 
				while($row2 = $resultado2->fetch_assoc()){
					
					$d[] = $row2;
				}
        return $d;
    }
	
	public function getTipoTrabajoByOrden($ordenId){
        $d = array();
        $sql2 = "SELECT cat_tipos_trabajo.id, cat_tipos_trabajo.descripcion from cat_tipos_trabajo join cotizaciones_detalle on cotizaciones_detalle.tipo_trabajo
				= cat_tipos_trabajo.id WHERE cotizaciones_detalle.id_cotizacion = ".$ordenId;

				$resultado2 = $this->mysqli->query($sql2);
				$equipos = array(); 
				while($row2 = $resultado2->fetch_assoc()){
					
					$d[] = $row2;
				}
        return $d;
    }
	
	public function getMaterialesByOrden($ordenId){
        $d = array();
        $sql2 = "SELECT * from orden_trabajo_materiales where status=1 and id_orden = ".$ordenId;
				$resultado2 = $this->mysqli->query($sql2);
				$equipos = array(); 
				while($row2 = $resultado2->fetch_assoc()){
					
					$d[] = $row2;
				}
        return $d;
    }
	
	public function getHerramientasByOrden($ordenId){
        $d = array();
        $sql2 = "SELECT * from orden_trabajo_herramientas where status=1 and id_orden = ".$ordenId;
				$resultado2 = $this->mysqli->query($sql2);
				$equipos = array(); 
				while($row2 = $resultado2->fetch_assoc()){
					
					$d[] = $row2;
				}
        return $d;
    }
	
	public function getrepuestosByOrden($ordenId){
        $d = array();
        $sql2 = "SELECT * from orden_trabajo_repuestos where status=1 and id_orden = ".$ordenId;
                $resultado2 = $this->mysqli->query($sql2);
                $equipos = array(); 
                while($row2 = $resultado2->fetch_assoc()){
                    
                    $d[] = $row2;
                }
        return $d;
    }

    public function getEquiposRevision(){
        $d = array();
        $sql2 = "
        SELECT 
            id,
            id_cotizacion,
            tipo_trabajo,
            '' unidad,
            descuento,
            '' des_trabajo,
            cantidad,
            precio,
            total
        FROM cotizaciones_detalle";
        $resultado2 = $this->mysqli->query($sql2);
        $equipos = array(); 
        while($row2 = $resultado2->fetch_assoc()){
            $d[] = $row2;
        }
        return $d;
    }

    public function getRevisionMatenimiento(){
        $sql = "SELECT 
                orden_trabajo.id AS id_orden,
                orden_trabajo.codigo AS codigo_orden,
                orden_trabajo.fecha_agendada,
                orden_trabajo.tipo_trabajo,
                id_responsable as grupo_trabajo,
                orden_trabajo.id_cotizacion,
                cotizaciones.id_cliente,
                cat_clientes.referencia,
                cat_clientes.nombre AS nombre_cliente,
                cat_clientes.id_tipcli  AS id_tipo_cliente,
                cotizaciones.id_sucursal,
                IFNULL(IF(cotizaciones.id_sucursal > 0, cat_sucursales.nombre_contacto, 'NO TIENE SUCURSALES'), 'NO TIENE SUCURSALES') AS sucursal,
                IFNULL(IF(cotizaciones.id_sucursal > 0, cat_sucursales.direccion, orden_trabajo.direccion), '') AS direccion,
                IFNULL(orden_trabajo.observaciones, '') AS observaciones,
                IFNULL(IF(cotizaciones.id_sucursal > 0, cat_sucursales_contactos.nombre, cat_clientes_contactos.nombre), 'NO ASIGNADO') AS nombre_contacto,
                IFNULL(IF(cotizaciones.id_sucursal > 0, cat_sucursales_contactos.telefono, cat_clientes_contactos.telefono), '') AS telefono_contacto,
                orden_trabajo.ids_auxiliares
            FROM orden_trabajo
            INNER JOIN cotizaciones                        ON orden_trabajo.id_cotizacion = cotizaciones.id
            LEFT JOIN orden_trabajo_responsables           ON orden_trabajo_responsables.id_orden = orden_trabajo.id
            LEFT JOIN cat_clientes                         ON cotizaciones.id_cliente = cat_clientes.id
            LEFT JOIN cat_sucursales                       ON cotizaciones.id_sucursal = cat_sucursales.id
            LEFT JOIN rev_caracteristicas_climatizacion_a  ON orden_trabajo.id = rev_caracteristicas_climatizacion_a.id_orden
            LEFT JOIN rev_caracteristicas_climatizacion_b  ON orden_trabajo.id = rev_caracteristicas_climatizacion_b.id_orden
            LEFT JOIN rev_caracteristicas_refrigeracion    ON orden_trabajo.id = rev_caracteristicas_refrigeracion.id_orden
            LEFT JOIN rev_caracteristicas_ventilacion      ON orden_trabajo.id = rev_caracteristicas_ventilacion.id_orden
            LEFT JOIN cat_clientes_contactos ON cotizaciones.id_contacto = cat_clientes_contactos.id
            LEFT JOIN cat_sucursales_contactos ON cotizaciones.id_contacto = cat_sucursales_contactos.id
            WHERE orden_trabajo.tipo_trabajo LIKE '%REVISION MANTENIMIENTO%' AND DATE(fecha_agendada) = CURRENT_DATE()
        ";
        $resultado = $this->conexion->queryAll($sql);
        foreach($resultado as $row){
            if($row->ids_auxiliares != ''){
                $aux = explode(',', $row->ids_auxiliares);
                $row->ids_auxiliares = "";
                foreach($aux as &$a){
                    $sql = "SELECT nombre FROM cat_auxiliares WHERE id = '{$a}'";
                    $row->ids_auxiliares .= $this->conexion->queryRow($sql)->nombre." / ";
                }
            }
            $row->observaciones = strip_tags($row->observaciones);
            $d[] = $row;
        }

        if(count($d)==0){
            $d[] = array("grupo_trabajo" => 0,"id_orden"=>0, "codigo_orden"=>"", "fecha_agendada"=>"", "direccion"=>"", "tipo_trabajo"=>"", "id_cotizacion"=>0, "id_cliente"=>0, "nombre_cliente"=>"NO HAY CLIENTES", "referencia"=>"", "id_tipo_cliente"=>0, "id_sucursal"=>0, "sucursal"=>"", "direccion_sucursal"=>"", "observaciones"=>"", "ids_auxiliares"=>"");
        }
        return $d;
    }

    public function getRevisionInstalacion(){
        $sql = "SELECT 
                orden_trabajo.id AS id_orden,
                orden_trabajo.codigo AS codigo_orden,
                orden_trabajo.fecha_agendada,
                orden_trabajo.tipo_trabajo,
                orden_trabajo.id_cotizacion,
                id_responsable as grupo_trabajo,
                cotizaciones.id_cliente,
                cat_clientes.nombre AS nombre_cliente,
                cat_clientes.referencia,
                cat_clientes.id_tipcli  AS id_tipo_cliente,
                cotizaciones.id_sucursal,
                IFNULL(IF(cotizaciones.id_sucursal > 0, cat_sucursales.nombre_contacto, 'NO TIENE SUCURSALES'), 'NO TIENE SUCURSALES') AS sucursal,
                IFNULL(IF(cotizaciones.id_sucursal > 0, cat_sucursales.direccion, orden_trabajo.direccion), '') AS direccion,
                IFNULL(orden_trabajo.observaciones, '') AS observaciones,
                IFNULL(IF(cotizaciones.id_sucursal > 0, cat_sucursales_contactos.nombre, cat_clientes_contactos.nombre), 'NO ASIGNADO') AS nombre_contacto,
                IFNULL(IF(cotizaciones.id_sucursal > 0, cat_sucursales_contactos.telefono, cat_clientes_contactos.telefono), '') AS telefono_contacto,
                orden_trabajo.ids_auxiliares
            FROM orden_trabajo
            INNER JOIN cotizaciones                        ON orden_trabajo.id_cotizacion = cotizaciones.id
            LEFT JOIN orden_trabajo_responsables           ON orden_trabajo_responsables.id_orden = orden_trabajo.id
            LEFT JOIN cat_clientes                         ON cotizaciones.id_cliente = cat_clientes.id
            LEFT JOIN cat_sucursales                       ON cotizaciones.id_sucursal = cat_sucursales.id
            LEFT JOIN rev_caracteristicas_climatizacion_a  ON orden_trabajo.id = rev_caracteristicas_climatizacion_a.id_orden
            LEFT JOIN rev_caracteristicas_climatizacion_b  ON orden_trabajo.id = rev_caracteristicas_climatizacion_b.id_orden
            LEFT JOIN rev_caracteristicas_refrigeracion    ON orden_trabajo.id = rev_caracteristicas_refrigeracion.id_orden
            LEFT JOIN rev_caracteristicas_ventilacion      ON orden_trabajo.id = rev_caracteristicas_ventilacion.id_orden
            LEFT JOIN cat_clientes_contactos ON cotizaciones.id_contacto = cat_clientes_contactos.id
            LEFT JOIN cat_sucursales_contactos ON cotizaciones.id_contacto = cat_sucursales_contactos.id
            WHERE orden_trabajo.tipo_trabajo LIKE '%REVISION INSTALACION%' AND DATE(fecha_agendada) = CURRENT_DATE()
        ";
       $resultado = $this->conexion->queryAll($sql);
       foreach($resultado as $row){
           if($row->ids_auxiliares != ''){
               $aux = explode(',', $row->ids_auxiliares);
               $row->ids_auxiliares = "";
               foreach($aux as &$a){
                   $sql = "SELECT nombre FROM cat_auxiliares WHERE id = '{$a}'";
                   $row->ids_auxiliares .= $this->conexion->queryRow($sql)->nombre." / ";
               }
           }
           $row->observaciones = strip_tags($row->observaciones);
           $d[] = $row;
       }
        if(count($d)==0){
            $d[] = array("grupo_trabajo"=>0,"id_orden"=>0, "codigo_orden"=>"", "fecha_agendada"=>"", "direccion"=>"", "tipo_trabajo"=>"", "id_cotizacion"=>0, "id_cliente"=>0, "nombre_cliente"=>"NO HAY CLIENTES", "referencia"=>"", "id_tipo_cliente"=>0, "id_sucursal"=>0, "sucursal"=>"", "direccion_sucursal"=>"", "observaciones"=>"","ids_auxiliares"=>"");
        }
        return $d;
    }

    public function getRevisionCorrectivo(){
        $sql = "SELECT 
                orden_trabajo.id AS id_orden,
                orden_trabajo.codigo AS codigo_orden,
                orden_trabajo.fecha_agendada,
                orden_trabajo.tipo_trabajo,
                id_responsable as grupo_trabajo,
                orden_trabajo.id_cotizacion,
                cotizaciones.id_cliente,
                cat_clientes.nombre AS nombre_cliente,
                cat_clientes.referencia,
                cat_clientes.id_tipcli  AS id_tipo_cliente,
                cotizaciones.id_sucursal,
                IFNULL(IF(cotizaciones.id_sucursal > 0, cat_sucursales.nombre_contacto, 'NO TIENE SUCURSALES'), 'NO TIENE SUCURSALES') AS sucursal,
                IFNULL(IF(cotizaciones.id_sucursal > 0, cat_sucursales.direccion, orden_trabajo.direccion), '') AS direccion,
                IFNULL(orden_trabajo.observaciones, '') AS observaciones,
                IFNULL(IF(cotizaciones.id_sucursal > 0, cat_sucursales_contactos.nombre, cat_clientes_contactos.nombre), 'NO ASIGNADO') AS nombre_contacto,
                IFNULL(IF(cotizaciones.id_sucursal > 0, cat_sucursales_contactos.telefono, cat_clientes_contactos.telefono), '') AS telefono_contacto,
                orden_trabajo.ids_auxiliares
            FROM orden_trabajo
            INNER JOIN cotizaciones                        ON orden_trabajo.id_cotizacion = cotizaciones.id
            LEFT JOIN orden_trabajo_responsables           ON orden_trabajo_responsables.id_orden = orden_trabajo.id
            LEFT JOIN cat_clientes                         ON cotizaciones.id_cliente = cat_clientes.id
            LEFT JOIN cat_sucursales                       ON cotizaciones.id_sucursal = cat_sucursales.id
            LEFT JOIN rev_caracteristicas_climatizacion_a  ON orden_trabajo.id = rev_caracteristicas_climatizacion_a.id_orden
            LEFT JOIN rev_caracteristicas_climatizacion_b  ON orden_trabajo.id = rev_caracteristicas_climatizacion_b.id_orden
            LEFT JOIN rev_caracteristicas_refrigeracion    ON orden_trabajo.id = rev_caracteristicas_refrigeracion.id_orden
            LEFT JOIN rev_caracteristicas_ventilacion      ON orden_trabajo.id = rev_caracteristicas_ventilacion.id_orden
            LEFT JOIN cat_clientes_contactos ON cotizaciones.id_contacto = cat_clientes_contactos.id
            LEFT JOIN cat_sucursales_contactos ON cotizaciones.id_contacto = cat_sucursales_contactos.id
            WHERE orden_trabajo.tipo_trabajo LIKE '%REVISION CORRECTIVO%' AND DATE(fecha_agendada) = CURRENT_DATE()
        ";
       $resultado = $this->conexion->queryAll($sql);
       foreach($resultado as $row){
           if($row->ids_auxiliares != ''){
               $aux = explode(',', $row->ids_auxiliares);
               $row->ids_auxiliares = "";
               foreach($aux as &$a){
                   $sql = "SELECT nombre FROM cat_auxiliares WHERE id = '{$a}'";
                   $row->ids_auxiliares .= $this->conexion->queryRow($sql)->nombre." / ";
               }
           }
           $row->observaciones = strip_tags($row->observaciones);
           $d[] = $row;
       }
        if(count($d)==0){
            $d[] = array("grupo_trabajo" => 0, "id_orden"=>0, "codigo_orden"=>"", "fecha_agendada"=>"", "direccion"=>"", "tipo_trabajo"=>"", "id_cotizacion"=>0, "id_cliente"=>0, "nombre_cliente"=>"NO HAY CLIENTES", "referencia"=>"", "id_tipo_cliente"=>0, "id_sucursal"=>0, "sucursal"=>"", "direccion_sucursal"=>"", "observaciones"=>"", "ids_auxiliares"=>"");
        }
        return $d;
    }

    public function getSupervisores(){
        $sql = "(SELECT id, nombre, 'Supervisor' AS tipo
                FROM cat_supervisores 
                WHERE STATUS = '1')
                UNION ALL
                (SELECT id, nombre, 'Asistente' AS tipo
                FROM cat_auxiliares
                WHERE STATUS = '1')";
        $res = $this->mysqli->query($sql);
        $d = array();
        
        while($row = $res->fetch_assoc()){
            $row["filter_in"] = "";
            $in_mantenimiento = "SELECT *
                                    FROM orden_trabajo
                                    INNER JOIN orden_trabajo_responsables ON id_orden = orden_trabajo.id
                                    WHERE 
                                        orden_trabajo.tipo_trabajo LIKE '%MANTENIMIENTO%' AND 
                                        orden_trabajo.tipo_trabajo NOT LIKE '%REVISION MANTENIMIENTO%' AND 
                                        DATE(fecha_agendada) = CURRENT_DATE() 
                                        AND id_responsable = {$row["id"]} AND tipo = '{$row["tipo"]}'";
            $res2 = $this->mysqli->query($in_mantenimiento);
            if($yes = $res2->fetch_assoc()){
                $row["filter_in"] .= "|MAN| ";
            }

            $in_instalacion = "SELECT *
                                    FROM orden_trabajo
                                    INNER JOIN orden_trabajo_responsables ON id_orden = orden_trabajo.id
                                    WHERE 
                                        orden_trabajo.tipo_trabajo LIKE '%INSTALACION%' AND 
                                        orden_trabajo.tipo_trabajo NOT LIKE '%REVISION INSTALACION%' AND 
                                        DATE(fecha_agendada) = CURRENT_DATE() AND 
                                        id_responsable = {$row["id"]} AND tipo = '{$row["tipo"]}'";
            $res2 = $this->mysqli->query($in_instalacion);
            if($yes = $res2->fetch_assoc()){
                $row["filter_in"] .= "|INS| ";
            }

            $in_correctivo = "SELECT *
                                    FROM orden_trabajo
                                    INNER JOIN orden_trabajo_responsables ON id_orden = orden_trabajo.id
                                    WHERE 
                                        DATE(fecha_agendada) = CURRENT_DATE AND 
                                        (orden_trabajo.tipo_trabajo LIKE '%CORRECTIVO%' OR orden_trabajo.tipo_trabajo LIKE '%REBOTE / RECLAMO%') AND
                                        orden_trabajo.tipo_trabajo NOT LIKE '%REVISION CORRECTIVO%' AND 
                                        id_responsable = {$row["id"]} AND tipo = '{$row["tipo"]}'";
            $res2 = $this->mysqli->query($in_correctivo);
            if($yes = $res2->fetch_assoc()){
                $row["filter_in"] .= "|COR| ";
            }

            $in_rev_mantenimiento = "SELECT *
                                    FROM orden_trabajo
                                    INNER JOIN orden_trabajo_responsables ON id_orden = orden_trabajo.id
                                    WHERE 
                                        orden_trabajo.tipo_trabajo LIKE '%REVISION MANTENIMIENTO%' AND 
                                        DATE(fecha_agendada) = CURRENT_DATE() AND 
                                        id_responsable = {$row["id"]} AND tipo = '{$row["tipo"]}'";
            $res2 = $this->mysqli->query($in_rev_mantenimiento);
            if($yes = $res2->fetch_assoc()){
                $row["filter_in"] .= "|REV. MAN| ";
            }

            $in_rev_instalacion = "SELECT *
                                    FROM orden_trabajo
                                    INNER JOIN orden_trabajo_responsables ON id_orden = orden_trabajo.id
                                    WHERE 
                                        orden_trabajo.tipo_trabajo LIKE '%REVISION INSTALACION%' AND 
                                        DATE(fecha_agendada) = CURRENT_DATE() AND 
                                        id_responsable = {$row["id"]} AND tipo = '{$row["tipo"]}'";
            $res2 = $this->mysqli->query($in_rev_instalacion);
            if($yes = $res2->fetch_assoc()){
                $row["filter_in"] .= "|REV. INS| ";
            }

            $in_rev_correctivo = "SELECT *
                                    FROM orden_trabajo
                                    INNER JOIN orden_trabajo_responsables ON id_orden = orden_trabajo.id
                                    WHERE 
                                        orden_trabajo.tipo_trabajo LIKE '%REVISION CORRECTIVO%' AND 
                                        DATE(fecha_agendada) = CURRENT_DATE() AND 
                                        id_responsable = {$row["id"]} AND tipo = '{$row["tipo"]}'";
            $res2 = $this->mysqli->query($in_rev_correctivo);
            if($yes = $res2->fetch_assoc()){
                $row["filter_in"] .= "|REV. COR| ";
            }

            if($row["filter_in"] != "")
                $d[] = $row;
        }
        if(count($d)==0){
            $d[] = ["filter_in" => "MAN", "id" => 0, "nombre" => "NO HAY ASIGNADOS"];
        }
        return $d;
    }
}