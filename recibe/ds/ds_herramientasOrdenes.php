<?php

include_once __DIR__ . '/datasource.inc.php';

$c = 0;
$d = array();
$o = new DataSource();
$s = new DataSource();
foreach ($o->getOrdenesTrabajo() as $orden){
	//echo $orden;
    $f = $s->getHerramientasByOrden($orden['id']);
	//echo $f;
    if(sizeof($f) > 0){
        foreach ($f as $ele) {
            $d[$c]['id'] = $ele['id'];
            $d[$c]['id_orden'] = $ele['id_orden'];
            $d[$c]['herramienta'] = $ele['herramientas'];
            $c++;
        }
    }
    else{
        $d[$c]['id'] = 0;
        $d[$c]['herramienta'] = "No tiene herramienta";
        $c++;
    }
}
echo json_encode($d, true);
/*
echo '<pre>';
print_r($d);
echo '</pre>';
*/