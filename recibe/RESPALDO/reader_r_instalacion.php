<?php
header('Content-Type: text/html; charset=utf-8');
ini_set('display_errors',1);
error_reporting(E_ALL);

/** DATABASE */
$mysqli = @new mysqli("localhost", "auditoriasbonita", "u[V(fTIUbcVb", "cegaservices2");

#$mysqli = @new mysqli("localhost", "root", "", "auditoriasbonita");

if (mysqli_connect_errno()) {
    printf("Falló la conexión: %s\n", mysqli_connect_error());
    exit();
}
$mysqli->set_charset("utf8");

/** Directorio de los JSON */
$path = realpath('./json');

$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
foreach($objects as $name => $object){
    if('.' != $object->getFileName() && '..' != $object->getFileName() && $path != $object->getPath()){
        $pos1 = strpos($object->getPath(), "/recibe/json/revision_instalacion_");
        if($pos1 !== false){
            $ext = pathinfo($object->getPathName(), PATHINFO_EXTENSION);
            if('json' == $ext || 'jpeg' == $ext || 'jpg' == $ext || 'png' == $ext || 'mp3' == $ext || '3gpp' == $ext){
                switch ($ext) {
                    case 'json':
                        /* READ AND MOVE JSON */
                        $json = json_decode(trim(file_get_contents($object->getPathName())), true);
                        echo '<pre>';
                        //print_r($json);
                        echo '</pre>';
                        process_json($json, $object->getFileName(), $mysqli);
                        exit;
                        #move_json($object->getPathName(), $object->getFileName());
                        break;

                    case 'jpeg':
                    case 'jpg':
                    case 'png':
                        /* MOVE JSON */
                        #move_image($object->getPathName(), $object->getFileName());
                        break;

                    case '3gpp':
                    case 'mp3':
                        /* MOVE AUDIO */
                        #move_audio($object->getPathName(), $object->getFileName());
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
$mysqli->close();

function move_json($file, $nameFile){
    echo $file;
    echo $nameFile;
    // die();
    if(!rename($file, __DIR__."/../reportes_vistos/json/$nameFile")){
        echo 'error';
    }
}

function move_image($file, $nameFile){
    // die();
    if(!rename($file, __DIR__."/../reportes_vistos/image/$nameFile")){
        echo 'error';
    }
}

function move_audio($file, $nameFile){
    // die();
    if(!rename($file, __DIR__."/../reportes_vistos/audio/$nameFile")){
        echo 'error';
    }
}

function process_json($json, $filename, $mysqli){
    $identifier = $json['identifier'];
    $version = $json['version']. '';
    $zone = $json['zone']. '';
    $referenceNumber = $json['referenceNumber']. '';
    $state = $json['state']. '';
    $deviceSubmitDate = $json['deviceSubmitDate'];
    $deviceSubmitDateDate = $deviceSubmitDate['time']. '';
    $deviceSubmitDateZone = $deviceSubmitDate['zone']. '';
    $fecha_file = str_replace("-", "", explode("T", $deviceSubmitDateDate)[0]);
    $shiftedDeviceSubmitDate = $json['shiftedDeviceSubmitDate'].'';
    $serverReceiveDate = $json['serverReceiveDate'].'';

    $form = $json['form'];
    $form_identifier = $form['identifier'].'';
    $form_versionIdentifier = $form['versionIdentifier'].'';
    $form_name = $form['name'].'';
    $form_version = $form['version'].'';
    $form_formSpaceIdentifier = $form['formSpaceIdentifier'].'';
    $form_formSpaceName = $form['formSpaceName'].'';

    $user = $json['user'];
    $user_identifier = $user['identifier'].'';
    $user_username = $user['username'].'';
    $user_displayName = $user['displayName'].'';

    $geoStamp = $json['geoStamp'];
    $geoStamp_success = $geoStamp['success'].'';
    $geoStamp_captureTimestamp = $geoStamp['captureTimestamp'];
    $geoStamp_captureTimestamp_provided = $geoStamp_captureTimestamp['provided'];
    $geoStamp_captureTimestamp_provided_time = $geoStamp_captureTimestamp_provided['time'].'';;
    $geoStamp_captureTimestamp_provided_zone = $geoStamp_captureTimestamp_provided['zone'].'';;
    $geoStamp_captureTimestamp_shifted = $geoStamp_captureTimestamp['shifted'].'';;
    $geoStamp_errorMessage = $geoStamp['errorMessage'].'';;
    $geoStamp_source = $geoStamp['source'].'';;
    $geoStamp_coordinates = $geoStamp['coordinates'];
    $geoStamp_coordinates_latitude = $geoStamp_coordinates['latitude'].'';;
    $geoStamp_coordinates_longitude = $geoStamp_coordinates['longitude'].'';;
    $geoStamp_coordinates_altitude = $geoStamp_coordinates['altitude'].'';;
    $geoStamp_address = $geoStamp['address'].'';;

    $pages = $json['pages'];

    // revision correctivo
    // -> cotizacion
    $cot_inst_cliente     = "";
    $cot_inst_sucursal     = "";
    $cot_inst_tipoCliente = "";
    $cot_inst_direccion   = "";
    $cot_inst_tipoTrabajo = "";

    // -> order
    // Info del area
    $ord_inst_tipoEquipo               = "";
    $ord_inst_areaClimatiza            = "";
    $ord_inst_nombreArea               = "";
    //climatización
    $ord_inst_climaDescripcionEquipo   = "";
    $ord_inst_climaBtu                 = "";
    $ord_inst_climaTiempoAprox         = "";
    //ventilación
    $ord_inst_ventilaDescripcionEquipo = "";
    $ord_inst_ventilaHp                = "";
    $ord_inst_ventilaCfn               = "";
    $ord_inst_ventilaTiempoAprox       = "";
    //refrigeración
    $ord_inst_refDescripcion           = "";
    $ord_inst_refCapacidadHP           = "";
    $ord_inst_refTiempoAprox           = "";
    //material requerido
    $ord_inst_item                     ="";
    $ord_inst_itemDescripcion          ="";
    $ord_inst_Unidad                   ="";
    $ord_inst_CantidadRequerida        ="";
    //herramientas de trabajo
    $ord_inst_Herramientas             ="";
    $ord_inst_RequeriEspecial          ="";

    // -> cliente
    $cli_inst_refLugar = "";
    $cli_inst_refRuta  = "";

    // -> sucursal
    $suc_inst_refLugar = "";
    $suc_inst_refRuta  = "";

    /*Items*/
    $item2_seleccion    ="";
    $item2_descripcion  ="";
    $item2_unidad       ="";
    $item2_cantidad     ="";

    $item3_seleccion    ="";
    $item3_descripcion  ="";
    $item3_unidad       ="";
    $item3_cantidad     ="";

    $item4_seleccion    ="";
    $item4_descripcion  ="";
    $item4_unidad       ="";
    $item4_cantidad     ="";

    $item5_seleccion    ="";
    $item5_descripcion  ="";
    $item5_unidad       ="";
    $item5_cantidad     ="";

    $item6_seleccion    ="";
    $item6_descripcion  ="";
    $item6_unidad       ="";
    $item6_cantidad     ="";

    $item7_seleccion    ="";
    $item7_descripcion  ="";
    $item7_unidad       ="";
    $item7_cantidad     ="";

    $item8_seleccion    ="";
    $item8_descripcion  ="";
    $item8_unidad       ="";
    $item8_cantidad     ="";

    $item9_seleccion    ="";
    $item9_descripcion  ="";
    $item9_unidad       ="";
    $item9_cantidad     ="";
  
    // ciclo
    foreach($pages as $key => $page_data){
        $pagina = $key+1;
        $pagina_nombre = $page_data['name'];
        $sql_muestra_causas = array();
        $answers = $page_data["answers"];

        //echo '<br><br><br>'.ltl($pagina_nombre).'<hr>';
        foreach($answers as $answer){
            $label    = $answer['label'];
            $dataType = $answer['dataType'];
            $question = $answer['question'];
            $values   = $answer['values'];
            $labelita = limpiar(trim(strtolower($label)));

            if("informacion general" == limpiar(trim(strtolower($pagina_nombre))))
            {
                // cotizacion
                $cot_inst_cliente = ltl($question) == "cliente:" ? (isset($values[0]) ? $values[0] : '') : $cot_inst_cliente;
                $cot_inst_sucursal = ltl($question) == "sucursal:" ? (isset($values[0]) ? $values[0] : '') : $cot_inst_sucursal;
                $cot_inst_tipoCliente = ltl($question) == "tipo de cliente:" ? (isset($values[0]) ? $values[0] : '') : $cot_inst_tipoCliente;
                $cot_inst_direccion = ltl($question) == "direccion:" ? (isset($values[0]) ? $values[0] : '') : $cot_inst_direccion;
                $cot_inst_tipoTrabajo = ltl($question) == "tipo de trabajo:" ? (isset($values[0]) ? $values[0] : '') : $cot_inst_tipoTrabajo;

                // cliente
                $cli_inst_refLugar = ltl($question) == "referencia del lugar:" ? (isset($values[0]) ? $values[0] : '') : $cli_inst_refLugar;
                $cli_inst_refRuta = ltl($question) == "ruta" ? (isset($values[0]) ? $values[0] : '') : $cli_inst_refRuta;

                // sucursal
                $suc_inst_refLugar = ltl($question) == "referencia del lugar:" ? (isset($values[0]) ? $values[0] : '') : $suc_inst_refLugar;
                $suc_inst_refRuta = ltl($question) == "ruta" ? (isset($values[0]) ? $values[0] : '') : $suc_inst_refRuta;
            }
            
            // Info del area
            if("informacion del area" == limpiar(trim(strtolower($pagina_nombre))))
            {
              $ord_inst_tipoEquipo =  ltl($question) == "tipo de equipo:" ? (isset($values[0]) ? $values[0] : '') : $ord_inst_tipoEquipo;
              $ord_inst_areaClimatiza = ltl($question) == "area que climatiza:" ? (isset($values[0]) ? $values[0] : '') : $ord_inst_areaClimatiza;
              $ord_inst_nombreArea = ltl($question) == "nombre del area:" ? (isset($values[0]) ? $values[0] : '') : $ord_inst_nombreArea;
            }

            if("caracteristicas area (climatizacion)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                /*echo ltl($pagina_nombre).'<br>';
                echo "\$ = \"".ltl($label)."\" == ltl(\$label) ? (isset(\$values[0]) ? \$values[0] : '') : ''; // $question<br>";*/
            }

            //climatización
            if("especificaciones del trabajo (climatizacion)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $ord_inst_climaDescripcionEquipo =  ltl($question) == "donde: 1" ? (isset($values[0]) ? $values[0] : '') : $ord_inst_climaDescripcionEquipo;
                $ord_inst_climaBtu =  ltl($question) == "capacidad (btu):" ? (isset($values[0]) ? $values[0] : '') : $ord_inst_climaBtu;
                $ord_inst_climaTiempoAprox =  ltl($question) == "tapion: 1" ? (isset($values[0]) ? $values[0] : '') : $ord_inst_climaTiempoAprox;
            }

            if("caracteristicas area (ventilacion)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                /*echo ltl($pagina_nombre).'<br>';
                echo "\$ = \"".ltl($label)."\" == ltl(\$label) ? (isset(\$values[0]) ? \$values[0] : '') : ''; // $question<br>";*/
            }

            //ventilación
            if("especificaciones del trabajo (ventilacion)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $ord_inst_ventilaDescripcionEquipo =  ltl($question) == "donde: 2" ? (isset($values[0]) ? $values[0] : '') : $ord_inst_ventilaDescripcionEquipo;
                $ord_inst_ventilaHp =  ltl($question) == "capacidad (hp): 1" ? (isset($values[0]) ? $values[0] : '') : $ord_inst_ventilaHp;
                $ord_inst_ventilaCfn =  ltl($question) == "capacidad (cfn):" ? (isset($values[0]) ? $values[0] : '') : $ord_inst_ventilaCfn;
                $ord_inst_ventilaTiempoAprox =  ltl($question) == "tapion: 2" ? (isset($values[0]) ? $values[0] : '') : $ord_inst_ventilaTiempoAprox;
            }

            if("caracteristicas area (refrigeracion)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                /*echo ltl($pagina_nombre).'<br>';
                echo "\$ = \"".ltl($label)."\" == ltl(\$label) ? (isset(\$values[0]) ? \$values[0] : '') : ''; // $question<br>";*/
            }
            //refrigeracion
            if("especificaciones del trabajo (refrigeracion)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $ord_inst_refDescripcion =  ltl($question) == "donde: 3" ? (isset($values[0]) ? $values[0] : '') : $ord_inst_refDescripcion;
                $ord_inst_refCapacidadHP =  ltl($question) == "capacidad (hp): 2" ? (isset($values[0]) ? $values[0] : '') : $ord_inst_refCapacidadHP;
                $ord_inst_refTiempoAprox =  ltl($question) == "tapion: 3" ? (isset($values[0]) ? $values[0] : '') : $ord_inst_refTiempoAprox;
            }
            //material requerido
            if("materiales requeridos" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $ord_inst_item =  ltl($question) == "sondi: 1" ? (isset($values[0]) ? $values[0] : '') : $ord_inst_item ;
                $ord_inst_itemDescripcion =  ltl($question) == "descripcion item: 1" ? (isset($values[0]) ? $values[0] : '') : $ord_inst_itemDescripcion ;
                $ord_inst_Unidad =  ltl($question) == "definir unidad: 1" ? (isset($values[0]) ? $values[0] : '') : $ord_inst_Unidad;
                $ord_inst_CantidadRequerida =  ltl($question) == "cr: 1" ? (isset($values[0]) ? $values[0] : '') : $ord_inst_CantidadRequerida;
            }

            if("item #2" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $item2_seleccion = "sondi:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item2_seleccion; // Selección de Item:
                $item2_descripcion = "descripcion item:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item2_descripcion; // Descripción Item:
                $item2_unidad = "definir unidad:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item2_unidad; // Definir Unidad:
                $item2_cantidad = "cr:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item2_cantidad; // Cantidad Requerida:
            }

            if("item #3" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $item3_seleccion = "sondi: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item3_seleccion; // Selección de Item:
                $item3_descripcion = "descripcion item: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item3_descripcion; // Descripción Item:
                $item3_unidad = "definir unidad: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item3_unidad; // Definir Unidad:
                $item3_cantidad = "cr: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item3_cantidad; // Cantidad Requerida:
            }

            if("item #4" == ltl($pagina_nombre))
            {
                $item4_seleccion = "sondi: 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item4_seleccion; // Selección de Item:
                $item4_descripcion = "descripcion item: 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item4_descripcion; // Descripción Item:
                $item4_unidad = "definir unidad: 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item4_unidad; // Definir Unidad:
                $item4_cantidad = "cr: 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item4_cantidad; // Cantidad Requerida:
            }

            if("item #5" == ltl($pagina_nombre))
            {
                $item5_seleccion = "sondi: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item5_seleccion; // Selección de Item:
                $item5_descripcion = "descripcion item: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item5_descripcion; // Descripción Item:
                $item5_unidad = "definir unidad: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item5_unidad; // Definir Unidad:
                $item5_cantidad = "cr: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item5_cantidad; // Cantidad Requerida:
            }

            if("item #6" == ltl($pagina_nombre))
            {
                $item6_seleccion = "sondi: 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item6_seleccion; // Selección de Item:
                $item6_descripcion = "descripcion item: 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item6_descripcion; // Descripción Item:
                $item6_unidad = "definir unidad: 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item6_unidad; // Definir Unidad:
                $item6_cantidad = "cr: 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item6_cantidad; // Cantidad Requerida:
            }

            if("item #7" == ltl($pagina_nombre))
            {
                $item7_seleccion = "sondi: 6" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item7_seleccion; // Selección de Item:
                $item7_descripcion = "descripcion item: 6" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item7_descripcion; // Descripción Item:
                $item7_unidad = "definir unidad: 6" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item7_unidad; // Definir Unidad:
                $item7_cantidad = "cr: 6" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item7_cantidad; // Cantidad Requerida:
            }

            if("item #8" == ltl($pagina_nombre))
            {
                $item8_seleccion = "sondi: 7" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : ''; // Selección de Item:
                $item8_descripcion = "descripcion item: 7" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : ''; // Descripción Item:
                $item8_unidad = "definir unidad: 7" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : ''; // Definir Unidad:
                $item8_cantidad = "cr: 7" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : ''; // Cantidad Requerida:
            }

            if("item #9" == ltl($pagina_nombre))
            {
                $item9_seleccion = "sondi: 8" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : ''; // Selección de Item:
                $item9_descripcion = "descripcion item: 8" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : ''; // Descripción Item:
                $item9_unidad = "definir unidad: 8" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : ''; // Definir Unidad:
                $item9_cantidad = "cr: 8" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : ''; // Cantidad Requerida:
            }

            if("item #10" == ltl($pagina_nombre))
            {
                $item10_seleccion = "sondi: 9" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : ''; // Selección de Item:
                $item10_descripcion = "descripcion item: 9" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : ''; // Descripción Item:
                $item10_unidad = "definir unidad: 9" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : ''; // Definir Unidad:
                $item10_cantidad = "cr: 9" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : ''; // Cantidad Requerida:
            }

            
            //herramientas de trabajo
            if("herramientas de trabajo" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $ord_inst_Herramientas =  ltl($question) == "hnpet:" ? (isset($values[0]) ? $values[0] : '') : $ord_inst_Herramientas ;
                $ord_inst_RequeriEspecial =  ltl($question) == "re:" ? (isset($values[0]) ? $values[0] : '') : $ord_inst_RequeriEspecial ;
            }

            if("observaciones generales y firmas" == ltl($pagina_nombre))
            {
                echo ltl($pagina_nombre).'<br>';
                echo "\$ = \"".ltl($label)."\" == ltl(\$label) ? (isset(\$values[0]) ? \$values[0] : '') : ''; // $question<br>";
            }
        }
        /*
        echo "if(\"". ltl($pagina_nombre) ."\" == ltl(\$pagina_nombre))<br>";
        echo "{<br> <br>";
        echo "}<br> <br>";
        */
    }

    // MySQL
    
    /*Climatización*/
    $sql = "INSERT INTO cotizaciones_instalacion SET id_cliente = '".$cot_inst_cliente."', id_sucursal = ' ".$cot_inst_sucursal."'";
    if($mysqli->query($sql)== TRUE){
        $last_cot = $mysqli->insert_id;
        //echo "Ultimo ID insertado " . $last_cot;
    } else {
    echo "Error: " . $last_cot . "<br>" . $mysqli->error;
    }
    /*Orden de la instalcion*/
    $sql_instalacion = "INSERT INTO orden_instalacion SET id_cotizacion = '".$last_cot."'";

    if($mysqli->query($sql_instalacion)== TRUE){
        $last_id = $mysqli->insert_id;
        //echo "Ultimo ID insertado " . $last_id;
    } else {
    echo "Error: " . $sql_instalacion . "<br>" . $mysqli->error;
    }
   /*Herramientas*/
    $sql_herramientas ="INSERT INTO orden_instalacion_herramientas SET id_orden ='".$last_id."',herramientas = '".$ord_inst_Herramientas."' , requerimientos = '".$ord_inst_RequeriEspecial."'";

    if ($mysqli->query($sql_herramientas) === TRUE) {
    echo "New record created successfully";
    }
    /*Climatización*/
    $sql_climatizacion ="INSERT INTO orden_instalacion_climatizacion SET id_orden ='".$last_id."',des_equipo = '".$ord_inst_climaDescripcionEquipo."' , capacidad = '".$ord_inst_climaBtu."', tiempo_aprox ='".$ord_inst_climaTiempoAprox."'";

    if ($mysqli->query($sql_climatizacion) === TRUE) {
    echo "New record created successfully";
    }
    /*Ventilacion*/
    $sql_ventilacion ="INSERT INTO orden_instalacion_ventilacion SET id_orden ='".$last_id."',des_equipo = '".$ord_inst_ventilaDescripcionEquipo."' , capacidad_hp = '".$ord_inst_ventilaHp."',capacidad_cfn ='".$ord_inst_ventilaCfn."', tiempo_aprox ='".$ord_inst_ventilaTiempoAprox."'";

    if ($mysqli->query($sql_ventilacion) === TRUE) {
    echo "New record created successfully";
    }
    /*Refrigeracion*/
    $sql_refrigeracion ="INSERT INTO orden_instalacion_refrigeracion SET id_orden ='".$last_id."',des_equipo = '".$ord_inst_refDescripcion."' , capacidad_hp = '".$ord_inst_refCapacidadHP."', tiempo_aprox ='".$ord_inst_refTiempoAprox."'";
	
    if ($mysqli->query($sql_refrigeracion) === TRUE) {
    echo "New record created successfully";
    }
    /*Material requerido todos los items*/
    $sql_item1 ="INSERT INTO orden_instalacion_material SET id_orden='".$last_id."',seleccion ='".$ord_inst_item."', descripcion='".$ord_inst_itemDescripcion."', unidad ='".$ord_inst_Unidad."',cantidad_requerida ='".$ord_inst_CantidadRequerida."'";
    
    if ($mysqli->query($sql_item1) === TRUE) {
    echo "New record created successfully";
    }

    $sql_item2 ="INSERT INTO orden_instalacion_material SET id_orden='".$last_id."',seleccion ='".$item2_seleccion."', descripcion='".$item2_descripcion."', unidad ='".$item2_unidad."',cantidad_requerida ='".$item2_cantidad."'";
    
    if ($mysqli->query($sql_item2) === TRUE) {
    echo "New record created successfully";
    }

    $sql_item3 ="INSERT INTO orden_instalacion_material SET id_orden='".$last_id."',seleccion ='".$item3_seleccion."', descripcion='".$item3_descripcion."', unidad ='".$item3_unidad."',cantidad_requerida ='".$item3_cantidad."'";
    
    if ($mysqli->query($sql_item3) === TRUE) {
    echo "New record created successfully";
    }

    $sql_item4 ="INSERT INTO orden_instalacion_material SET id_orden='".$last_id."',seleccion ='".$item4_seleccion."', descripcion='".$item4_descripcion."', unidad ='".$item4_unidad."',cantidad_requerida ='".$item4_cantidad."'";
    
    if ($mysqli->query($sql_item4) === TRUE) {
    echo "New record created successfully";
    }

    $sql_item5 ="INSERT INTO orden_instalacion_material SET id_orden='".$last_id."',seleccion ='".$item5_seleccion."', descripcion='".$item5_descripcion."', unidad ='".$item5_unidad."',cantidad_requerida ='".$item5_cantidad."'";
    
    if ($mysqli->query($sql_item5) === TRUE) {
    echo "New record created successfully";
    }

    $sql_item6 ="INSERT INTO orden_instalacion_material SET id_orden='".$last_id."',seleccion ='".$item6_seleccion."', descripcion='".$item6_descripcion."', unidad ='".$item6_unidad."',cantidad_requerida ='".$item6_cantidad."'";
    
    if ($mysqli->query($sql_item6) === TRUE) {
    echo "New record created successfully";
    }

    $sql_item7 ="INSERT INTO orden_instalacion_material SET id_orden='".$last_id."',seleccion ='".$item7_seleccion."', descripcion='".$item7_descripcion."', unidad ='".$item7_unidad."',cantidad_requerida ='".$item7_cantidad."'";
    
    if ($mysqli->query($sql_item7) === TRUE) {
    echo "New record created successfully";
    }

    $sql_item8 ="INSERT INTO orden_instalacion_material SET id_orden='".$last_id."',seleccion ='".$item8_seleccion."', descripcion='".$item8_descripcion."', unidad ='".$item8_unidad."',cantidad_requerida ='".$item8_cantidad."'";
    
    if ($mysqli->query($sql_item8) === TRUE) {
    echo "New record created successfully";
    }

    $sql_item9 ="INSERT INTO orden_instalacion_material SET id_orden='".$last_id."',seleccion ='".$item9_seleccion."', descripcion='".$item9_descripcion."', unidad ='".$item9_unidad."',cantidad_requerida ='".$item9_cantidad."'";
    
    if ($mysqli->query($sql_item9) === TRUE) {
    echo "New record created successfully";
    }


    

}

function limpiar($String)
{
    $String = str_replace(array('á','à','â','ã','ª','ä'),"a",$String);
    $String = str_replace(array('Á','À','Â','Ã','Ä'),"A",$String);
    $String = str_replace(array('Í','Ì','Î','Ï'),"I",$String);
    $String = str_replace(array('í','ì','î','ï'),"i",$String);
    $String = str_replace(array('é','è','ê','ë'),"e",$String);
    $String = str_replace(array('É','È','Ê','Ë'),"E",$String);
    $String = str_replace(array('ó','ò','ô','õ','ö','º'),"o",$String);
    $String = str_replace(array('Ó','Ò','Ô','Õ','Ö'),"O",$String);
    $String = str_replace(array('ú','ù','û','ü'),"u",$String);
    $String = str_replace(array('Ú','Ù','Û','Ü'),"U",$String);
    $String = str_replace(array('[','^','´','`','¨','~',']'),"",$String);
    $String = str_replace("ç","c",$String);
    $String = str_replace("Ç","C",$String);
    $String = str_replace("ñ","n",$String);
    $String = str_replace("Ñ","N",$String);
    $String = str_replace("Ý","Y",$String);
    $String = str_replace("ý","y",$String);
    
    $String = str_replace("&aacute;","a",$String);
    $String = str_replace("&Aacute;","A",$String);
    $String = str_replace("&eacute;","e",$String);
    $String = str_replace("&Eacute;","E",$String);
    $String = str_replace("&iacute;","i",$String);
    $String = str_replace("&Iacute;","I",$String);
    $String = str_replace("&oacute;","o",$String);
    $String = str_replace("&Oacute;","O",$String);
    $String = str_replace("&uacute;","u",$String);
    $String = str_replace("&Uacute;","U",$String);

    $String = str_replace("\u00C1;","Á",$String);
    $String = str_replace("\u00E1;","á",$String);
    $String = str_replace("\u00C9;","É",$String);
    $String = str_replace("\u00E9;","é",$String);
    $String = str_replace("\u00CD;","Í",$String);
    $String = str_replace("\u00ED;","í",$String);
    $String = str_replace("\u00D3;","Ó",$String);
    $String = str_replace("\u00F3;","ó",$String);
    $String = str_replace("\u00DA;","Ú",$String);
    $String = str_replace("\u00FA;","ú",$String);
    $String = str_replace("\u00DC;","Ü",$String);
    $String = str_replace("\u00FC;","ü",$String);
    $String = str_replace("\u00D1;","Ṅ",$String);
    $String = str_replace("\u00F1;","ñ",$String);

    $String = str_replace("A", "a", $String);
    return $String;
}

function ltl($s)
{
    return limpiar(trim(strtolower($s)));
}