<?php
header('Content-Type: text/html; charset=utf-8');
ini_set('display_errors',1);
error_reporting(E_ALL);

/** DATABASE */
$mysqli = @new mysqli("localhost", "auditoriasbonita", "u[V(fTIUbcVb", "cegaservices2");

#$mysqli = @new mysqli("localhost", "root", "", "auditoriasbonita");

if (mysqli_connect_errno()) {
    printf("Falló la conexión: %s\n", mysqli_connect_error());
    exit();
}
$mysqli->set_charset("utf8");

/** Directorio de los JSON */
$path = realpath('./json');

$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
foreach($objects as $name => $object){
    if('.' != $object->getFileName() && '..' != $object->getFileName() && $path != $object->getPath()){
        $pos1 = strpos($object->getPath(), "/recibe/json/revision_correctivo_");
        if($pos1 !== false){
            $ext = pathinfo($object->getPathName(), PATHINFO_EXTENSION);
            if('json' == $ext || 'jpeg' == $ext || 'jpg' == $ext || 'png' == $ext || 'mp3' == $ext || '3gpp' == $ext){
                switch ($ext) {
                    case 'json':
                        /* READ AND MOVE JSON */
                        $json = json_decode(trim(file_get_contents($object->getPathName())), true);
                        echo '<pre>';
                        //print_r($json);
                        echo '</pre>';
                        process_json($json, $object->getFileName(), $mysqli);
                        exit;
                        #move_json($object->getPathName(), $object->getFileName());
                        break;

                    case 'jpeg':
                    case 'jpg':
                    case 'png':
                        /* MOVE JSON */
                        #move_image($object->getPathName(), $object->getFileName());
                        break;

                    case '3gpp':
                    case 'mp3':
                        /* MOVE AUDIO */
                        #move_audio($object->getPathName(), $object->getFileName());
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
$mysqli->close();

function move_json($file, $nameFile){
    echo $file;
    echo $nameFile;
    // die();
    if(!rename($file, __DIR__."/../reportes_vistos/json/$nameFile")){
        echo 'error';
    }
}

function move_image($file, $nameFile){
    // die();
    if(!rename($file, __DIR__."/../reportes_vistos/image/$nameFile")){
        echo 'error';
    }
}

function move_audio($file, $nameFile){
    // die();
    if(!rename($file, __DIR__."/../reportes_vistos/audio/$nameFile")){
        echo 'error';
    }
}

function process_json($json, $filename, $mysqli){
    $identifier = $json['identifier'];
    $version = $json['version']. '';
    $zone = $json['zone']. '';
    $referenceNumber = $json['referenceNumber']. '';
    $state = $json['state']. '';
    $deviceSubmitDate = $json['deviceSubmitDate'];
    $deviceSubmitDateDate = $deviceSubmitDate['time']. '';
    $deviceSubmitDateZone = $deviceSubmitDate['zone']. '';
    $fecha_file = str_replace("-", "", explode("T", $deviceSubmitDateDate)[0]);
    $shiftedDeviceSubmitDate = $json['shiftedDeviceSubmitDate'].'';
    $serverReceiveDate = $json['serverReceiveDate'].'';

    $form = $json['form'];
    $form_identifier = $form['identifier'].'';
    $form_versionIdentifier = $form['versionIdentifier'].'';
    $form_name = $form['name'].'';
    $form_version = $form['version'].'';
    $form_formSpaceIdentifier = $form['formSpaceIdentifier'].'';
    $form_formSpaceName = $form['formSpaceName'].'';

    $user = $json['user'];
    $user_identifier = $user['identifier'].'';
    $user_username = $user['username'].'';
    $user_displayName = $user['displayName'].'';

    $geoStamp = $json['geoStamp'];
    $geoStamp_success = $geoStamp['success'].'';
    $geoStamp_captureTimestamp = $geoStamp['captureTimestamp'];
    $geoStamp_captureTimestamp_provided = $geoStamp_captureTimestamp['provided'];
    $geoStamp_captureTimestamp_provided_time = $geoStamp_captureTimestamp_provided['time'].'';;
    $geoStamp_captureTimestamp_provided_zone = $geoStamp_captureTimestamp_provided['zone'].'';;
    $geoStamp_captureTimestamp_shifted = $geoStamp_captureTimestamp['shifted'].'';;
    $geoStamp_errorMessage = $geoStamp['errorMessage'].'';;
    $geoStamp_source = $geoStamp['source'].'';;
    $geoStamp_coordinates = $geoStamp['coordinates'];
    $geoStamp_coordinates_latitude = $geoStamp_coordinates['latitude'].'';;
    $geoStamp_coordinates_longitude = $geoStamp_coordinates['longitude'].'';;
    $geoStamp_coordinates_altitude = $geoStamp_coordinates['altitude'].'';;
    $geoStamp_address = $geoStamp['address'].'';;

    $pages = $json['pages'];

    // revision correctivo
   
 /*Ingo Gral*/
 $infgen_cliente="";
 $infgen_id_cliente="";
 $infgen_tipoCliente="";
 $infgen_direccion="";
 $infgen_tipoTrabajo="";
 $infgen_referenciaLugar="";
 $infgen_ruta="";

/*Info equipo*/
 $infequ_tipo="";
 $infequ_area="";
 $infequ_nombreArea="";

 /*Carac equipo cli a*/
$climaA_descripcion="";
 $climaA_codigo="";
 $climaA_marca="";
 $climaA_btu="";
 $climaA_modelo="";
 $climaA_serie="";
 $climaAD_reparacion="";
 $climaAD_cambio="";
 $climaAD_observaciones="";
 $climaAD_comentarios="";
 $climaAD_capturaFotos="";
 $climaAD_tiempoTrabajo="";

 /*Diagnostico cli A*/
 $climaAD_reparacion="";
 $climaAD_cambio="";
 $climaAD_observaciones="";
 $climaAD_comentarios="";
 $climaAD_capturaFotos="";
 $climaAD_tiempoTrabajo="";

 /*Carac equipo cli b*/
$climaB_descripcion="";
$climaB_codigo="";
$climaB_evaporador="";
$climaB_marca="";
$climaB_btu="";
$climaB_modelo="";
$climaB_serie="";
$climaB_condensador="";
$climaB_condensadorMarca="";
$climaB_condensadorBtu="";
$climaB_condensadorModelo="";
$climaB_condensadorSerie="";

/*Diagnostico cli B*/
$climaBD_reparacion="";
$climaBD_cambio="";
$climaBD_condensaRep="";
$climaBD_condensaCambio="";
$climaBD_observaciones="";
$climaBD_comentarios="";
$climaBD_fotos="";
$climaBD_tiempoTrabajo="";

/*caracteristicas ventilacion*/
$ventila_descripcion="";
$ventila_codigo="";
$ventila_marca="";
$ventila_hp="";
$ventila_cfn="";
$ventila_modelo="";
$ventila_serie="";

/*Diagnostico Ventilacion*/
$ventilaD_reparacion="";
$ventilaD_cambio="";
$ventilaD_observaciones="";
$ventilaD_comentarios="";
$ventilaD_fotos="";
$ventilaD_tiempoTrabajo="";

/*caracteristicas refrigeracion*/
$ref_descripcion="";
$ref_codigo="";
$ref_marca="";
$ref_modelo="";
$ref_serie="";
$ref_compresorBtu="";
$ref_compresorCantidad="";
$ref_ventiladorHp="";
$ref_ventiladorCantidad="";

/*Evaporadores*/
$evapora1_marca="";
$evapora1_modelo="";
$evapora1_serie="";
$evapora1_hp="";
$evapora1_cantidad="";

$evapora2_marca="";
$evapora2_modelo="";
$evapora2_serie="";
$evapora2_hp="";
$evapora2_cantidad="";

$evapora3_marca="";
$evapora3_modelo="";
$evapora3_serie="";
$evapora3_hp="";
$evapora3_cantidad="";

$evapora4_marca="";
$evapora4_modelo="";
$evapora4_serie="";
$evapora4_hp="";
$evapora4_cantidad="";

$evapora5_marca="";
$evapora5_modelo="";
$evapora5_serie="";
$evapora5_hp="";
$evapora5_cantidad="";

$evapora6_marca="";
$evapora6_modelo="";
$evapora6_serie="";
$evapora6_hp="";
$evapora6_cantidad="";

/*Diagnostico refrigeracion*/
$refD_evaporaRep="";
$refD_evaporaCambio="";
$refD_condesaRep="";
$refD_condensaCambio="";
$refD_observaciones="";
$refD_comentarios="";
$refD_fotos="";
$refD_tiempoTrabajo="";

/*Materiales con items*/
$item1="";
$item1_seleccion="";
$item1_descripcion="";
$item1_unidad="";
$item1_cantidad="";
$item1_observaciones="";

$item2="";
$item2_seleccion="";
$item2_descripcion="";
$item2_unidad="";
$item2_cantidad="";
$item2_observaciones="";

$item3="";
$item3_seleccion="";
$item3_descripcion="";
$item3_unidad="";
$item3_cantidad="";
$item3_observaciones="";

$item4="";
$item4_seleccion="";
$item4_descripcion="";
$item4_unidad="";
$item4_cantidad="";
$item4_observaciones="";

$item5="";
$item5_seleccion="";
$item5_descripcion="";
$item5_unidad="";
$item5_cantidad="";
$item5_observaciones="";

$item6="";
$item6_seleccion="";
$item6_descripcion="";
$item6_unidad="";
$item6_cantidad="";
$item6_observaciones="";

$item7="";
$item7_seleccion="";
$item7_descripcion="";
$item7_unidad="";
$item7_cantidad="";
$item7_observaciones="";

$item8="";
$item8_seleccion="";
$item8_descripcion="";
$item8_unidad="";
$item8_cantidad="";
$item8_observaciones="";

$item9="";
$item9_seleccion="";
$item9_descripcion="";
$item9_unidad="";
$item9_cantidad="";
$item9_observaciones="";

$item10="";
$item10_seleccion="";
$item10_descripcion="";
$item10_unidad="";
$item10_cantidad="";
$item10_observaciones="";

$hertra_requerimientos="";
            $obs_nombreResponsable="";
            $obs_cargo="";
            $obs_comentarios="";
            $obs_observaciones="";
            $obs_firma="";
            $obs_copia="";

    // ciclo
    foreach($pages as $key => $page_data){
        $pagina = $key+1;
        $pagina_nombre = $page_data['name'];
        $sql_muestra_causas = array();
        $answers = $page_data["answers"];

        //echo '<br><br><br>'.ltl($pagina_nombre).'<hr>';
        foreach($answers as $answer){
            $label    = $answer['label'];
            $dataType = $answer['dataType'];
            $question = $answer['question'];
            $values   = $answer['values'];
            $labelita = limpiar(trim(strtolower($label)));

            if("informacion general" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $infgen_cliente = "cliente:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $infgen_cliente;
                $infgen_id_cliente = "cliente: -id" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $infgen_id_cliente;
                $infgen_tipoCliente = "tipo de cliente:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $infgen_tipoCliente;
                $infgen_direccion = "direccion:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $infgen_direccion;
                $infgen_tipoTrabajo = "tipo de trabajo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $infgen_tipoTrabajo;
                $infgen_referenciaLugar = "referencia del lugar:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $infgen_referenciaLugar;
                $infgen_ruta = "ruta" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $infgen_ruta;
            }

            if("informacion equipo" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $infequ_tipo = "tipo de equipo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $infequ_tipo;
                $infequ_area = "area que climatiza:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') :  $infequ_area;
                $infequ_nombreArea = "nombre del area:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $infequ_nombreArea;
            }

            if("caracteristicas equipo (climatizacion a)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $climaA_descripcion = "descripcion del equipo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaA_descripcion;
                $climaA_codigo = "codigo del equipo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaA_codigo;
                $climaA_marca = "marca" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaA_marca;
                $climaA_btu = "capacidad (btu):" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaA_btu;
                $climaA_modelo = "modelo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaA_modelo;
                $climaA_serie = "serie:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaA_serie;
            }

            if("diagnostico (climatizacion a)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $climaAD_reparacion = "piezas no operativas (reparacion):" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaAD_reparacion;
                $climaAD_cambio = "piezas no operativas (cambio):" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaAD_cambio;
                $climaAD_observaciones = "observaciones:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaAD_observaciones;
                $climaAD_comentarios = "comentarios:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaAD_comentarios;
                $climaAD_capturaFotos = "captura de fotos" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaAD_capturaFotos;
                $climaAD_tiempoTrabajo = "tiempo de trabajo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaAD_tiempoTrabajo;
            }

            if("caracteristicas equipo (climatizacion b)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $climaB_descripcion = "descripcion del equipo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaB_descripcion;
                $climaB_codigo = "codigo del equipo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaB_codigo;
                $climaB_evaporador = "evaporador" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaB_evaporador;
                $climaB_marca = "marca:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaB_marca;
                $climaB_btu = "capacidad (btu):" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaB_btu;
                $climaB_modelo = "modelo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaB_modelo;
                $climaB_serie = "serie:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaB_serie;
                $climaB_condensador = "condensador" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaB_condensador;
                $climaB_condensadorMarca = "marca:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaB_condensadorMarca;
                $climaB_condensadorBtu = "capacidad (btu):" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaB_condensadorBtu;
                $climaB_condensadorModelo = "modelo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaB_condensadorModelo;
                $climaB_condensadorSerie = "serie:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaB_condensadorSerie;
            }

            if("diagnostico (climatizacion b)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $climaBD_reparacion = "pno(ron): 2" == ltl($label) ? (isset($values[0]) ? $values[0] : $climaBD_reparacion) : ''; // Piezas no Operativas (Reparación):
                $climaBD_cambio = "pno(c): 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaBD_cambio; // Piezas no Operativas (Cambio):
                $climaBD_condensaRep = "pno(ron): 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaBD_condensaRep; // Piezas no Operativas (Reparación):
                $climaBD_condensaCambio = "pno(c): 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaBD_condensaCambio; // Piezas no Operativas (Cambio):
                $climaBD_observaciones = "observaciones:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaBD_observaciones;
                $climaBD_comentarios = "comentarios:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaBD_comentarios;
                $climaBD_fotos = "captura de fotos" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaBD_fotos;
                $climaBD_tiempoTrabajo = "tiempo de trabajo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $climaBD_tiempoTrabajo;
            }

            if("caracteristicas equipo (ventilacion)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $ventila_descripcion = "descripcion del equipo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ventila_descripcion;
                $ventila_codigo = "codigo del equipo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ventila_codigo;
                $ventila_marca = "marca:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ventila_marca;
                $ventila_hp = "capacidad (hp):" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ventila_hp;
                $ventila_cfn = "capacidad (cfn):" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ventila_cfn;
                $ventila_modelo = "modelo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ventila_modelo;
                $ventila_serie = "serie:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ventila_serie;
            }

            if("diagnostico (ventilacion)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $ventilaD_reparacion = "partes no operativas (reparacion):" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ventilaD_reparacion;
                $ventilaD_cambio = "partes no operativas (cambio):" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ventilaD_cambio;
                $ventilaD_observaciones = "observaciones:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ventilaD_observaciones;
                $ventilaD_comentarios = "comentarios:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ventilaD_comentarios;
                $ventilaD_fotos = "captura de fotos" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ventilaD_fotos;
                $ventilaD_tiempoTrabajo = "tiempo de trabajo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ventilaD_tiempoTrabajo;
            }

            if("caracteristicas equipo (refrigeracion)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $ref_descripcion = "descripcion del equipo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ref_descripcion;
                $ref_codigo = "codigo del equipo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ref_codigo;
                $ref_marca = "marca:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ref_marca;
                $ref_modelo = "modelo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ref_modelo;
                $ref_serie = "serie:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ref_serie;
                $ref_compresorBtu = "capacidad compresor (hp/btu):" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ref_compresorBtu;
                $ref_compresorCantidad = "cantidad compresores:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ref_compresorCantidad;
                $ref_ventiladorHp = "capacidad motor ventilador (hp):" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ref_ventiladorHp;
                $ref_ventiladorCantidad = "cantidad motor ventilador:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $ref_ventiladorCantidad;
            }

            if("evaporador #1" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $evapora1_marca = "marca:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora1_marca;
                $evapora1_modelo = "modelo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora1_modelo;
                $evapora1_serie = "serie:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora1_serie;
                $evapora1_hp = "capacidad motor ventilador (hp):" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora1_hp;
                $evapora1_cantidad = "cantidad motor ventilador:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora1_cantidad;
            }

            if("evaporador #2" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $evapora2_marca = "marca:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora2_marca;
                $evapora2_modelo = "modelo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora2_modelo;
                $evapora2_serie = "serie:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora2_serie;
                $evapora2_hp = "capacidad motor ventilador (hp):" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora2_hp;
                $evapora2_cantidad = "cantidad motor ventilador:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora2_cantidad;
            }

            if("evaporador #3" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $evapora3_marca = "marca:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora3_marca;
                $evapora3_modelo = "modelo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora3_modelo;
                $evapora3_serie = "serie:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora3_serie;
                $evapora3_hp = "capacidad motor ventilador (hp):" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora3_hp;
                $evapora3_cantidad = "cantidad motor ventilador:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora3_cantidad;
            }

            if("evaporador #4" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $evapora4_marca = "marca:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora4_marca;
                $evapora4_modelo = "modelo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora4_modelo;
                $evapora4_serie = "serie:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora4_serie;
                $evapora4_hp = "capacidad motor ventilador (hp):" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora4_hp;
                $evapora4_cantidad = "cantidad motor ventilador:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora4_cantidad;
            }

            if("evaporador #5" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $evapora5_marca = "marca:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora5_marca;
                $evapora5_modelo = "modelo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora5_modelo;
                $evapora5_serie = "serie:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora5_serie;
                $evapora5_hp = "capacidad motor ventilador (hp):" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora5_hp;
                $evapora5_cantidad = "cantidad motor ventilador:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora5_cantidad;
            }

            if("evaporador #6" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $evapora6_marca = "marca:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora6_marca;
                $evapora6_modelo = "modelo:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora6_modelo;
                $evapora6_serie = "serie:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora6_serie;
                $evapora6_hp = "capacidad motor ventilador (hp):" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora6_hp;
                $evapora6_cantidad = "cantidad motor ventilador:" == ltl($question) ? (isset($values[0]) ? $values[0] : '') : $evapora6_cantidad;
            }

            if("diagnostico (refrigeracion)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $refD_evaporaRep = "pno(ron): 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $refD_evaporaRep; // Partes no Operativas (Reparación):
                $refD_evaporaCambio = "pno(c): 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $refD_evaporaCambio; // Partes no Operativas (Cambio):
                $refD_condesaRep = "pno(ron): 6" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $refD_condesaRep; // Partes no Operativas (Reparación):
                $refD_condensaCambio = "pno(c): 6" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $refD_condensaCambio; // Partes no Operativas (Cambio):
                $refD_observaciones = "observaciones: 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $refD_observaciones; // Observaciones:
                $refD_comentarios = "comentarios: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $refD_comentarios; // Comentarios:
                $refD_fotos = "captura de fotos 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $refD_fotos; // Captura de Fotos
                $refD_tiempoTrabajo = "tdt: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $refD_tiempoTrabajo; // Tiempo de Trabajo:
            }

            if("materiales requeridos" == limpiar(trim(strtolower($pagina_nombre))))
            {
                // item #1
                $item1 = "item #1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item1; // Item #1
                $item1_seleccion = "sondi: 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item1_seleccion; // Selección de Item:
                $item1_descripcion = "descripcion item: 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item1_descripcion; // Descripción Item:
                $item1_unidad = "definir unidad: 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item1_unidad; // Definir Unidad:
                $item1_cantidad = "cr: 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item1_cantidad; // Cantidad Requerida:
                $item1_observaciones = "observaciones: 6" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item1_observaciones; // Observaciones:
            }

            if("item #2" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $item2_seleccion = "sondi:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item2_seleccion; // Selección de Item:
                $item2_descripcion = "descripcion item:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item2_descripcion; // Descripción Item:
                $item2_unidad = "definir unidad:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item2_unidad; // Definir Unidad:
                $item2_cantidad = "cr:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item2_cantidad; // Cantidad Requerida:
                $item2_observaciones = "observaciones:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item2_observaciones; // Observaciones:
            }

            if("item #3" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $item3_seleccion = "sondi: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item3_seleccion; // Selección de Item:
                $item3_descripcion = "descripcion item: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item3_descripcion; // Descripción Item:
                $item3_unidad = "definir unidad: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item3_unidad; // Definir Unidad:
                $item3_cantidad = "cr: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item3_cantidad; // Cantidad Requerida:
                $item3_observaciones = "observaciones: 7" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item3_observaciones; // Observaciones:
            }

            if("item #4" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $item4_seleccion = "sondi: 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item4_seleccion; // Selección de Item:
                $item4_descripcion = "descripcion item: 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item4_descripcion; // Descripción Item:
                $item4_unidad = "definir unidad: 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item4_unidad; // Definir Unidad:
                $item4_cantidad = "cr: 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item4_cantidad; // Cantidad Requerida:
                $item4_observaciones = "observaciones: 8" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item4_observaciones; // Observaciones:
            }

            if("item #5" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $item5_seleccion = "sondi: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item5_seleccion; // Selección de Item:
                $item5_descripcion = "descripcion item: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item5_descripcion; // Descripción Item:
                $item5_unidad = "definir unidad: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item5_unidad; // Definir Unidad:
                $item5_cantidad = "cr: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item5_cantidad; // Cantidad Requerida:
                $item5_observaciones = "observaciones: 9" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item5_observaciones; // Observaciones:
            }

            if("item #6" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $item6_seleccion = "sondi: 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item6_seleccion; // Selección de Item:
                $item6_descripcion = "descripcion item: 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item6_descripcion; // Descripción Item:
                $item6_unidad = "definir unidad: 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item6_unidad; // Definir Unidad:
                $item6_cantidad = "cr: 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item6_cantidad; // Cantidad Requerida:
                $item6_observaciones = "observaciones: 10" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item6_observaciones; // Observaciones:
            }

            if("item #7" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $item7_seleccion = "sondi: 6" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item7_seleccion; // Selección de Item:
                $item7_descripcion = "descripcion item: 6" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item7_descripcion; // Descripción Item:
                $item7_unidad = "definir unidad: 6" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item7_unidad; // Definir Unidad:
                $item7_cantidad = "cr: 6" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item7_cantidad; // Cantidad Requerida:
                $item7_observaciones = "observaciones: 11" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item7_observaciones; // Observaciones:
            }

            if("item #8" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $item8_seleccion = "sondi: 7" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item8_seleccion; // Selección de Item:
                $item8_descripcion = "descripcion item: 7" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item8_descripcion; // Descripción Item:
                $item8_unidad = "definir unidad: 7" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item8_unidad; // Definir Unidad:
                $item8_cantidad = "cr: 7" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item8_cantidad; // Cantidad Requerida:
                $item8_observaciones = "observaciones: 12" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item8_observaciones; // Observaciones:
            }

            if("item #9" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $item9_seleccion = "sondi: 8" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item9_seleccion; // Selección de Item:
                $item9_descripcion = "descripcion item: 8" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item9_descripcion; // Descripción Item:
                $item9_unidad = "definir unidad: 8" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item9_unidad; // Definir Unidad:
                $item9_cantidad = "cr: 8" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item9_cantidad; // Cantidad Requerida:
                $item9_observaciones = "observaciones: 13" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item9_observaciones; // Observaciones:
            }

            if("item #10" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $item10_seleccion = "sondi: 9" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item10_seleccion; // Selección de Item:
                $item10_descripcion = "descripcion item: 9" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item10_descripcion; // Descripción Item:
                $item10_unidad = "definir unidad: 9" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item10_unidad; // Definir Unidad:
                $item10_cantidad = "cr: 9" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item10_cantidad; // Cantidad Requerida:
                $item10_observaciones = "observaciones: 14" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item10_observaciones; // Observaciones:
            }

            if("herramientas de trabajo" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $hertra_herramientas = "hnpet:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $hertra_herramientas; // Herramientas necesarias para el trabajo:
                $hertra_requerimientos = "re:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $hertra_requerimientos; // Requerimientos Especiales:
            }

            if("observaciones generales y firmas" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $obs_nombreResponsable = "ndrqset:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $obs_nombreResponsable; // Nombre del responsable que supervisa el trabajo:
                $obs_cargo = "cargo:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $obs_cargo; // Cargo:
                $obs_comentarios = "cg" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $obs_comentarios; // Comentarios Generales
                $obs_observaciones = "og:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $obs_observaciones; // Observaciones Generales:
                $obs_firma = "fdcor" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $obs_firma; // Firma del Cliente o Responsable
                $obs_copia = "eucdefasc:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $obs_copia; // Enviar una copia de este formulario al siguiente correo:
            }
            
        }

    }

    // MySQL
    
    $sql = "";

}

function limpiar($String)
{
    $String = str_replace(array('á','à','â','ã','ª','ä'),"a",$String);
    $String = str_replace(array('Á','À','Â','Ã','Ä'),"A",$String);
    $String = str_replace(array('Í','Ì','Î','Ï'),"I",$String);
    $String = str_replace(array('í','ì','î','ï'),"i",$String);
    $String = str_replace(array('é','è','ê','ë'),"e",$String);
    $String = str_replace(array('É','È','Ê','Ë'),"E",$String);
    $String = str_replace(array('ó','ò','ô','õ','ö','º'),"o",$String);
    $String = str_replace(array('Ó','Ò','Ô','Õ','Ö'),"O",$String);
    $String = str_replace(array('ú','ù','û','ü'),"u",$String);
    $String = str_replace(array('Ú','Ù','Û','Ü'),"U",$String);
    $String = str_replace(array('[','^','´','`','¨','~',']'),"",$String);
    $String = str_replace("ç","c",$String);
    $String = str_replace("Ç","C",$String);
    $String = str_replace("ñ","n",$String);
    $String = str_replace("Ñ","N",$String);
    $String = str_replace("Ý","Y",$String);
    $String = str_replace("ý","y",$String);
    
    $String = str_replace("&aacute;","a",$String);
    $String = str_replace("&Aacute;","A",$String);
    $String = str_replace("&eacute;","e",$String);
    $String = str_replace("&Eacute;","E",$String);
    $String = str_replace("&iacute;","i",$String);
    $String = str_replace("&Iacute;","I",$String);
    $String = str_replace("&oacute;","o",$String);
    $String = str_replace("&Oacute;","O",$String);
    $String = str_replace("&uacute;","u",$String);
    $String = str_replace("&Uacute;","U",$String);

    $String = str_replace("\u00C1;","Á",$String);
    $String = str_replace("\u00E1;","á",$String);
    $String = str_replace("\u00C9;","É",$String);
    $String = str_replace("\u00E9;","é",$String);
    $String = str_replace("\u00CD;","Í",$String);
    $String = str_replace("\u00ED;","í",$String);
    $String = str_replace("\u00D3;","Ó",$String);
    $String = str_replace("\u00F3;","ó",$String);
    $String = str_replace("\u00DA;","Ú",$String);
    $String = str_replace("\u00FA;","ú",$String);
    $String = str_replace("\u00DC;","Ü",$String);
    $String = str_replace("\u00FC;","ü",$String);
    $String = str_replace("\u00D1;","Ṅ",$String);
    $String = str_replace("\u00F1;","ñ",$String);

    $String = str_replace("A", "a", $String);
    return $String;
}

function ltl($s)
{
    return limpiar(trim(strtolower($s)));
}