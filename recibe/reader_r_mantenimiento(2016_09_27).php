<?php
header('Content-Type: text/html; charset=utf-8');
ini_set('display_errors',1);
error_reporting(E_ALL);

/** DATABASE */
$connect = new stdClass;
$connect->db = @new mysqli("localhost", "auditoriasbonita", "u[V(fTIUbcVb", "cegaservices");
$connect->db2 = @new mysqli("localhost", "auditoriasbonita", "u[V(fTIUbcVb", "cegaservices2");

#$mysqli = @new mysqli("localhost", "root", "", "auditoriasbonita");
if (mysqli_connect_errno()) {
    printf("Falló la conexión: %s\n", mysqli_connect_error());
    exit();
}
$connect->db->set_charset("utf8");
$connect->db2->set_charset("utf8");

/** Directorio de los JSON */
$path = realpath('./json');

$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
foreach($objects as $name => $object){
    if('.' != $object->getFileName() && '..' != $object->getFileName() && 
	   "/home/procesosiq/public_html/cegaservices2/recibe/json/mantenimiento_" == $object->getPath()){
        //D($object->getPath());
        //$pos1 = strpos($object->getPath(), "revision_mantenimiento_");
		$pos1 = true;
        if($pos1 !== false){
            $ext = pathinfo($object->getPathName(), PATHINFO_EXTENSION);
            if('json' == $ext || 'jpeg' == $ext || 'jpg' == $ext || 'png' == $ext || 'mp3' == $ext || '3gpp' == $ext){
                switch ($ext) {
                    case 'json':
                        $json = json_decode(trim(file_get_contents($object->getPathName())), true);
                        process_json($json, $object->getFileName(), $connect);
                        move_json($object->getPathName(), $object->getFileName());
                        break;
                    case 'jpeg':
						move_image($object->getPathName(), $object->getFileName());
                        break;
                    case 'jpg':
						move_image($object->getPathName(), $object->getFileName());
                        break;
                    case 'png':
                        move_image($object->getPathName(), $object->getFileName());
                        break;
                    case '3gpp':
						move_audio($object->getPathName(), $object->getFileName());
						break;
                    default:
                        break;
                }
            }
        }
    }
}

function move_json($file, $nameFile){
    if(!rename($file, __DIR__."/../reportes_vistos/mantenimiento/$nameFile")){
        echo 'error';
    }
}

function move_image($file, $nameFile){
    if(!rename($file, __DIR__."/../reportes_vistos/mantenimiento/".limpiar($nameFile))){
        echo 'error';
    }
}

function move_audio($file, $nameFile){
    if(!rename($file, __DIR__."/../reportes_vistos/mantenimiento/$nameFile")){
        echo 'error';
    }
}

function process_json($json, $filename, $connect){ 
	//print_r($json);die('');
	
    $identifier = $json['identifier'];
    // D($identifier);
    $version = $json['version']. '';
    $zone = $json['zone']. '';
    $referenceNumber = $json['referenceNumber']. '';
    $state = $json['state']. '';
    $deviceSubmitDate = $json['deviceSubmitDate'];
    $deviceSubmitDateDate = $deviceSubmitDate['time']. '';
    $deviceSubmitDateZone = $deviceSubmitDate['zone']. '';
    $fecha_file = str_replace("-", "", explode("T", $deviceSubmitDateDate)[0]);
    $shiftedDeviceSubmitDate = $json['shiftedDeviceSubmitDate'].'';
    $serverReceiveDate = $json['serverReceiveDate'].'';

    $form = $json['form'];
    $form_identifier = $form['identifier'].'';
    $form_versionIdentifier = $form['versionIdentifier'].'';
    $form_name = $form['name'].'';
    $form_version = $form['version'].'';
    $form_formSpaceIdentifier = $form['formSpaceIdentifier'].'';
    $form_formSpaceName = $form['formSpaceName'].'';

    $user = $json['user'];
    $user_identifier = $user['identifier'].'';
    $user_username = $user['username'].'';
    $user_displayName = $user['displayName'].'';

    $geoStamp = $json['geoStamp'];
    $geoStamp_success = $geoStamp['success'].'';
    $geoStamp_captureTimestamp = $geoStamp['captureTimestamp'];
    $geoStamp_captureTimestamp_provided = $geoStamp_captureTimestamp['provided'];
    $geoStamp_captureTimestamp_provided_time = $geoStamp_captureTimestamp_provided['time'].'';;
    $geoStamp_captureTimestamp_provided_zone = $geoStamp_captureTimestamp_provided['zone'].'';;
    $geoStamp_captureTimestamp_shifted = $geoStamp_captureTimestamp['shifted'].'';;
    $geoStamp_errorMessage = $geoStamp['errorMessage'].'';;
    $geoStamp_source = $geoStamp['source'].'';;
    $geoStamp_coordinates = $geoStamp['coordinates'];
    $geoStamp_coordinates_latitude = $geoStamp_coordinates['latitude'].'';;
    $geoStamp_coordinates_longitude = $geoStamp_coordinates['longitude'].'';;
    $geoStamp_coordinates_altitude = $geoStamp_coordinates['altitude'].'';;
    $geoStamp_address = $geoStamp['address'].'';;

    $pages = $json['pages'];

    
	
	
	/* VARIABLES HECHAS POR NOE */
	
	/* INFORMACION GENERAL */
	$nombre_json = $filename;
	$infGen_idOrden = "";
	$infGen_TipoRepote = "";
	$infGen_Geoloca = "";
	$infGen_HoraLlegada = "";
	$infGen_horaPro = "";
	$infGen_fecMant = "";
	
	$cot_cliente     = "";
    $cot_id_cliente  = "";
	$cot_tipo_cli = "";
	
	$cot_sucursal     = "";
    $cot_id_sucursal  = "";
	$cot_direccion   = "";
	$cli_refLugar = "";
	
	
	$infTrabajo = "";
	$cot_tipoTrabajo = "";
	$infObserva = "";
	
	
	/* Información Equipo  */
	$ord_tipoEquipo = "";
	
	/* Características Equipo (Climatización A) */
	$ord_codEquipo = "";
	$ord_desEquipo = "";
	$ord_areaClimatiza = "";
    $ord_id_areaClimatiza = "";
	$ord_nombreArea = "";
	$ord_evacon = "";
	$equi_marca = "";
	$ord_climaBtu = "";
	$ord_VentilaHP = "";
	$equi_modelo = "";
	$equi_serie = "";
	$ord_evacon2 = "";
	$equi_marca2 = "";
	$ord_climaBtu2 = "";
	$ord_VentilaHP2 = "";
	$equi_modelo2 = "";
	$equi_serie2 = "";
	$orde_observas = "";
	$orde_pruebaIni = "";
	$equi_opera = "";
	
	$retvav_cara = '';
	$retval_carac = '';
	
	/* Diagnostico (Climatización A) */
	$diag_conde = "";
	$diag_par_no_ope = "";
	$diag_par_no_ope2 = "";
	$diag_conde2 = "";
	$diag_par_no_ope22 = "";
	$diag_par_no_ope2_2 = "";
	$diag_tiem_tra = "";
	$diag_observa = "";
	$retval_diag = "";
	$diag_audio = "";
	$diag_fotos = "";
	
	/* mantenimiento equipo (climatizacion a) */
	$mant_conde = "";
	$mant_serpe = "";
	$mant_filtro = "";
	$mant_filtro_com = "";
	$mant_carcasa = "";
	$mant_carcasa_com = "";
	$mant_lubiMoto = "";
	$mant_lubiMoto_com = "";
	$mant_parElec = "";
	$mant_parElec_com = "";
	$mant_drena = "";
	
	$mant_banda_polea = "";
	$mant_banda_polea_com = "";
	$mant_lubrica_roda = "";
	$mant_lubrica_roda_com = "";
	
	$mant_observa = "";
	$retval_mant = '';
	
	$mant_conde2 = "";
	$mant_serpe2 = "";
	$mant_filtro2 = "";
	$mant_filtro_com2 = "";
	$mant_carcasa2 = "";
	$mant_carcasa_com2 = "";
	$mant_lubiMoto2 = "";
	$mant_lubiMoto_com2 = "";
	$mant_parElec2 = "";
	$mant_parElec_com2 = "";
	$mant_drena2 = "";
	
	$retvav_mante = "";
	$mante_fotos = "";
	
	/* Materiales Requeridos  */
	$mat_selitem1 = "";
	$mat_desitem1 = "";
	$mat_uniditem1 = "";
	$mat_cantitem1 = "";
	$mat_observa = "";
	
	$mat_selitem2 = "";
	$mat_desitem2 = "";
	$mat_uniditem2 = "";
	$mat_cantitem2 = "";
	
	$mat_selitem3 = "";
	$mat_desitem3 = "";
	$mat_uniditem3 = "";
	$mat_cantitem3 = "";
	
	$mat_selitem4 = "";
	$mat_desitem4 = "";
	$mat_uniditem4 = "";
	$mat_cantitem4 = "";
	
	$mat_selitem5 = "";
	$mat_desitem5 = "";
	$mat_uniditem5 = "";
	$mat_cantitem5 = "";
	
	$mat_selitem6 = "";
	$mat_desitem6 = "";
	$mat_uniditem6 = "";
	$mat_cantitem6 = "";
	
	$mat_selitem7 = "";
	$mat_desitem7 = "";
	$mat_uniditem7 = "";
	$mat_cantitem7 = "";
	
	$mat_selitem8 = "";
	$mat_desitem8 = "";
	$mat_uniditem8 = "";
	$mat_cantitem8 = "";
	
	$mat_selitem9 = "";
	$mat_desitem9 = "";
	$mat_uniditem9 = "";
	$mat_cantitem9 = "";
	
	$mat_selitem10 = "";
	$mat_desitem10 = "";
	$mat_uniditem10 = "";
	$mat_cantitem10 = "";	
	
	
	/* Herramientas de Trabajo  */
	$herr_detalle = "";
	$herr_otros = "";
	
	/* Pruebas Finales Equipo (Climatización) */
	$prueFin_1 = "";
	$prueFin_2 = "";
	$prueFin_3 = "";
	$prueFin_4 = "";
	$prueFin_5 = "";
	$prueFin_obser = "";
	$prueFin_retval = "";
	
	
	/* Observaciones Generales y Firmas */
	$resposable = "";
	$cargo_respon = "";
	$observa_gen_fin = "";
	$img_firma = "";
	$audio_comen = "";
	/* FIN DE LAS VARIABLES */
	
    
    foreach($pages as $key => $page_data){
        $pagina = $key+1;
        $pagina_nombre = $page_data['name'];
        $sql_muestra_causas = array();
        $answers = $page_data["answers"];

        //echo ltl($pagina_nombre).'<br>';
        foreach($answers as $answer){
            // D($answer);
            $label    = $answer['label'];
            $dataType = $answer['dataType'];
            $question = $answer['question'];
            $values   = $answer['values'];
            $labelita = limpiar(trim(strtolower($label)));

			if("informacion general" == limpiar(trim(strtolower($pagina_nombre))))
            {
                //INFO GENERAL (FALTA LA HORA DE LLEGADA Y GEOLOCALIZACION) ****************************
				if(ltl($question) == "geolocalizacion"){
					$infGen_Geoloca = $values[0]['coordinates']['latitude'].','.$values[0]['coordinates']['longitude'];
				}
				
				$infGen_idOrden = ltl($question) == "orden de trabajo - id" ? (isset($values[0]) ? $values[0] : '') : $infGen_idOrden;
				
				if(ltl($question) == "hora de llegada:"){
					$infGen_HoraLlegada = $values[0]['provided']['time'];
				}
				
				$infGen_horaPro = ltl($question) == "hora programada:" ? (isset($values[0]) ? $values[0] : '') : $infGen_horaPro;
				$infGen_fecMant = ltl($question) == "fecha del mantenimiento:" ? (isset($values[0]) ? $values[0] : '') : $infGen_fecMant;
				
				//CLIENTE
				$cot_cliente = ltl($question) == "cliente" ? (isset($values[0]) ? $values[0] : '') : $cot_cliente;
    			$cot_id_cliente = ltl($question) == "cliente - id" ? (isset($values[0]) ? $values[0] : '') : $cot_id_cliente;
				$cot_tipo_cli = ltl($question) == "cliente - id_tipo_cliente" ? (isset($values[0]) ? $values[0] : '') : $cot_tipo_cli;
				
                //SUCURSAL
				$cot_sucursal = ltl($question) == "sucursal" ? (isset($values[0]) ? $values[0] : '') : $cot_sucursal;
                $cot_id_sucursal = ltl($question) == "sucursal - id" ? (isset($values[0]) ? $values[0] : '') : $cot_id_sucursal;
				$cot_direccion = ltl($question) == "direccion sucursal:" ? (isset($values[0]) ? $values[0] : '') : $cot_direccion;
				$cli_refLugar = ltl($question) == "referencia:" ? (isset($values[0]) ? $values[0] : '') : $cli_refLugar;
				
				$infTrabajo = ltl($question) == "detalle del trabajo:" ? (isset($values[0]) ? $values[0] : '') : $infTrabajo;
				$infObserva = ltl($question) == "observaciones:" ? (isset($values[0]) ? $values[0] : '') : $infObserva;
				$cot_tipoTrabajo = ltl($question) == "tipo de trabajo:" ? (isset($values[0]) ? $values[0] : '') : $cot_tipoTrabajo;
            }

            if("informacion equipo" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $ord_tipoEquipo = ltl($question) == 'tipo de equipo:' ? (isset($values[0]) ? $values[0] : '') : $ord_tipoEquipo;
            }			
			
			
			/* CARACTERISTICAS DE LOS EQUIPOS */
			
			if("caracteristicas equipo (climatizacion a)" == limpiar(trim(strtolower($pagina_nombre))) && 
			($retval_carac != 'CLIMA-B' && $retval_carac != 'VENTILACION'))
			{ 
				if($ord_codEquipo != ''){
					$retval_carac = "CLIMA-A";
				}
				
				$ord_codEquipo = ltl($question) == 'codigo del equipo:' ? (isset($values[0]) ? $values[0] : '') : $ord_codEquipo;
				$ord_desEquipo = ltl($question) == 'descripcion del equipo:' ? (isset($values[0]) ? $values[0] : '') : $ord_desEquipo;
				
				$ord_id_areaClimatiza = ltl($question) == 'area - id:' ? (isset($values[0]) ? $values[0] : '') : $ord_id_areaClimatiza;
				$ord_areaClimatiza = ltl($question) == 'area que climatiza:' ? (isset($values[0]) ? $values[0] : '') : $ord_areaClimatiza;
    			$ord_nombreArea = ltl($question) == 'nombre del area:' ? (isset($values[0]) ? $values[0] : '') : $ord_nombreArea;
				
				$ord_evacon = ltl($question) == 'evaporador / condensador' ? (isset($values[0]) ? $values[0] : '') : $ord_evacon;
				$equi_marca = ltl($question) == 'marca:' ? (isset($values[0]) ? $values[0] : '') : $equi_marca;
				$ord_climaBtu = ltl($question) == 'capacidad (btu):' ? (isset($values[0]) ? $values[0] : '') : $ord_climaBtu;
				$equi_modelo = ltl($question) == 'modelo:' ? (isset($values[0]) ? $values[0] : '') : $equi_modelo;
				$equi_serie = ltl($question) == 'serie:' ? (isset($values[0]) ? $values[0] : '') : $equi_serie;
				$orde_observas = ltl($question) == 'observaciones:' ? (isset($values[0]) ? $values[0] : '') : $orde_observas;
				
				$orde_pruebaIni = ltl($question) == 'pruebas iniciales' ? (isset($values[0]) ? $values[0] : '') : $orde_pruebaIni;
				
				if(ltl($question) == '¿equipo operativo?'){
					if(isset($values[0])){
						$equi_opera = $values[0];
						$retval_carac = "CLIMA-A";
					}
				}
				
            }
			
			if("caracteristicas equipo (climatizacion b)" == limpiar(trim(strtolower($pagina_nombre))) && 
			($retval_carac != 'CLIMA-A' && $retval_carac != 'VENTILACION'))
			{
				if($ord_codEquipo != ''){
					$retval_carac = "CLIMA-B";
				}
				
				$ord_codEquipo = ltl($question) == 'codigo del equipo:' ? (isset($values[0]) ? $values[0] : '') : $ord_codEquipo;
				$ord_desEquipo = ltl($question) == 'descripcion del equipo:' ? (isset($values[0]) ? $values[0] : '') : $ord_desEquipo;
				
				$ord_id_areaClimatiza = ltl($question) == 'area - id:' ? (isset($values[0]) ? $values[0] : '') : $ord_id_areaClimatiza;
				$ord_areaClimatiza = ltl($question) == 'area que climatiza:' ? (isset($values[0]) ? $values[0] : '') : $ord_areaClimatiza;
    			$ord_nombreArea = ltl($question) == 'nombre del area:' ? (isset($values[0]) ? $values[0] : '') : $ord_nombreArea;
				
				
				if(ltl($question) == 'evaporador')
				{
					$retvav_cara = "evapora";
				}
				if(ltl($question) == 'condensador')
				{
					$retvav_cara = "condensa";
				}			
				
				$equi_marca = (ltl($question) == 'marca:' && $retvav_cara == "evapora") ? (isset($values[0]) ? $values[0] : '') : $equi_marca;
				$ord_climaBtu = (ltl($question) == 'capacidad (btu):' && $retvav_cara == "evapora") ? (isset($values[0]) ? $values[0] : '') : $ord_climaBtu;
				$equi_modelo = (ltl($question) == 'modelo:' && $retvav_cara == "evapora") ? (isset($values[0]) ? $values[0] : '') : $equi_modelo;
				$equi_serie = (ltl($question) == 'serie:' && $retvav_cara == "evapora") ? (isset($values[0]) ? $values[0] : '') : $equi_serie;
				
				
				$equi_marca2 = (ltl($question) == 'marca:' && $retvav_cara == "condensa") ? (isset($values[0]) ? $values[0] : '') : $equi_marca2;
				$ord_climaBtu2 = (ltl($question) == 'capacidad (btu):' && $retvav_cara == "condensa") ? (isset($values[0]) ? $values[0] : '') : $ord_climaBtu2;
				$equi_modelo2 = (ltl($question) == 'modelo:' && $retvav_cara == "condensa") ? (isset($values[0]) ? $values[0] : '') : $equi_modelo2;
				$equi_serie2 = (ltl($question) == 'serie:' && $retvav_cara == "condensa") ? (isset($values[0]) ? $values[0] : '') : $equi_serie2;
				
				$orde_observas = (ltl($question) == 'observaciones:') ? (isset($values[0]) ? $values[0] : '') : $orde_observas;
				$orde_pruebaIni = ltl($question) == 'pruebas iniciales' ? (isset($values[0]) ? $values[0] : '') : $orde_pruebaIni;
				$equi_opera = ltl($question) == '¿equipo operativo?' ? (isset($values[0]) ? $values[0] : '') : $equi_opera;
				
            }
		
			if("caracteristicas equipo (ventilacion)" == limpiar(trim(strtolower($pagina_nombre))) && 
			($retval_carac != 'CLIMA-A' && $retval_carac != 'CLIMA-B'))
			{				
				
				if($ord_codEquipo != ''){
					$retval_carac = "VENTILACION";
				}
				
				$ord_codEquipo = ltl($question) == 'codigo del equipo:' ? (isset($values[0]) ? $values[0] : '') : $ord_codEquipo;
				$ord_desEquipo = ltl($question) == 'descripcion del equipo:' ? (isset($values[0]) ? $values[0] : '') : $ord_desEquipo;
				
				
				$equi_marca = ltl($question) == 'marca:' ? (isset($values[0]) ? $values[0] : '') : $equi_marca;
				$ord_VentilaHP = ltl($question) == 'capacidad (hp):' ? (isset($values[0]) ? $values[0] : '') : $ord_VentilaHP;
				$equi_modelo = ltl($question) == 'modelo:' ? (isset($values[0]) ? $values[0] : '') : $equi_modelo;
				$equi_serie = ltl($question) == 'serie:' ? (isset($values[0]) ? $values[0] : '') : $equi_serie;
				$orde_observas = ltl($question) == 'observaciones:' ? (isset($values[0]) ? $values[0] : '') : $orde_observas;
				
				if(ltl($question) == '¿equipo operativo?'){
					if(isset($values[0])){
						$equi_opera = $values[0];
					}
				}
			}
			
			/* FIN DE LAS CARACTERISTICAS DE LOS EQUIPOS */
			
			
			
			/* MANTENIMIENTO DE LOS DISTINTOS TIPOS DE EQUIPOS */
			
			if("mantenimiento equipo (climatizacion a)" == limpiar(trim(strtolower($pagina_nombre)))  && 
			($retval_mant != 'CLIMA-MAT-B' && $retval_mant != 'MAT-VENTILACION')){
								
				if($mant_serpe != '' || $mant_filtro != '' || $mant_carcasa != '' || $mant_lubiMoto != '' || $mant_parElec != '' || $mant_drena != '' || $mant_observa != ''){
					$retval_mant = 'CLIMA-MAT-A';
				}
				
				$mant_conde = ltl($question) == 'evaporador / condensador' ? (isset($values[0]) ? $values[0] : '') : $mant_conde;
				$mant_serpe = ltl($question) == 'limpieza de serpertin:' ? (isset($values[0]) ? $values[0] : '') : $mant_serpe;
				$mant_filtro = ltl($question) == 'limpieza de filtros:' ? (isset($values[0]) ? $values[0] : '') : $mant_filtro;
			$mant_filtro_com = ltl($question) == 'limpieza de filtros: comentarios' ? (isset($values[0]) ? $values[0] : '') : $mant_filtro_com;
				$mant_carcasa = ltl($question) == 'limpieza de carcasa:' ? (isset($values[0]) ? $values[0] : '') : $mant_carcasa;
			$mant_carcasa_com = ltl($question) == 'limpieza de carcasa: comentarios' ? (isset($values[0]) ? $values[0] : '') : $mant_carcasa_com;
				$mant_lubiMoto = ltl($question) == 'limpieza y lubricacion de motores:' ? (isset($values[0]) ? $values[0] : '') : $mant_lubiMoto;
		$mant_lubiMoto_com = ltl($question) == 'limpieza y lubricacion de motores: comentarios' ? (isset($values[0]) ? $values[0] : '') : $mant_lubiMoto_com;
				$mant_parElec = ltl($question) == 'limpieza de parte electrica:' ? (isset($values[0]) ? $values[0] : '') : $mant_parElec;
	$mant_parElec_com = ltl($question) == 'limpieza de parte electrica: comentarios' ? (isset($values[0]) ? $values[0] : '') : $mant_parElec_com;
				$mant_drena = ltl($question) == 'limpieza de drenaje:' ? (isset($values[0]) ? $values[0] : '') : $mant_drena;
				$mant_observa = ltl($question) == 'observaciones:' ? (isset($values[0]) ? $values[0] : '') : $mant_observa;
				
				if(ltl($question) == 'captura de fotos'){
					
					if(isset($values[0]['filename']) != ''){
						$mante_fotos .= $referenceNumber.'_'.limpiar($values[0]['filename'])."|";
						$retval_mant = 'CLIMA-A';
					}
					if(isset($values[1]['filename']) != ''){
						$mante_fotos .= $referenceNumber.'_'.limpiar($values[1]['filename'])."|";
						$retval_mant = 'CLIMA-A';
					}
					if(isset($values[2]['filename']) != ''){
						$mante_fotos .= $referenceNumber.'_'.limpiar($values[2]['filename'])."|";
						$retval_mant = 'CLIMA-A';
					}
					if(isset($values[3]['filename']) != ''){
						$mante_fotos .= $referenceNumber.'_'.limpiar($values[3]['filename'])."|";
						$retval_mant = 'CLIMA-A';
					}
					if(isset($values[4]['filename']) != ''){
						$mante_fotos .= $referenceNumber.'_'.limpiar($values[4]['filename'])."|";
						$retval_mant = 'CLIMA-A';
					}
					if(isset($values[5]['filename']) != ''){
						$mante_fotos .= $referenceNumber.'_'.limpiar($values[5]['filename'])."|";
						$retval_mant = 'CLIMA-A';
					}
					if(isset($values[6]['filename']) != ''){
						$mante_fotos .= $referenceNumber.'_'.limpiar($values[6]['filename'])."|";
						$retval_mant = 'CLIMA-A';
					}
					if(isset($values[7]['filename']) != ''){
						$mante_fotos .= $referenceNumber.'_'.limpiar($values[7]['filename'])."|";
						$retval_mant = 'CLIMA-A';
					}
					if(isset($values[8]['filename']) != ''){
						$mante_fotos .= $referenceNumber.'_'.limpiar($values[8]['filename'])."|";
						$retval_mant = 'CLIMA-A';
					}
					if(isset($values[9]['filename']) != ''){
						$mante_fotos .= $referenceNumber.'_'.limpiar($values[9]['filename'])."|";
						$retval_mant = 'CLIMA-A';
					}
				}
				
			}
			
			if("mantenimiento equipo (climatizacion b)" == limpiar(trim(strtolower($pagina_nombre))) && 
			($retval_mant != 'CLIMA-MAT-A' && $retval_mant != 'MAT-VENTILACION')){
				
				if($mant_serpe != '' || $mant_filtro != '' || $mant_carcasa != '' || $mant_lubiMoto != '' || $mant_parElec != '' || $mant_drena != '' || $mant_observa != ''){
					$retval_mant = 'CLIMA-MAT-B';
				}
				
				if(ltl($question) == 'evaporador')
				{
					$retvav_mante = "evapora";
				}
				if(ltl($question) == 'condensador')
				{
					$retvav_mante = "condensa";
				}
				
				$mant_serpe = (ltl($question) == 'limpieza de serpertin:' && $retvav_mante == "evapora") ? (isset($values[0]) ? $values[0] : '') : $mant_serpe;
				$mant_filtro = (ltl($question) == 'limpieza de filtros:' && $retvav_mante == "evapora") ? (isset($values[0]) ? $values[0] : '') : $mant_filtro;
				$mant_filtro_com = (ltl($question) == 'limpieza de filtros: comentarios' && $retvav_mante == "evapora") ? (isset($values[0]) ? $values[0] : '') : $mant_filtro_com;
				$mant_carcasa = (ltl($question) == 'limpieza de carcasa:' && $retvav_mante == "evapora") ? (isset($values[0]) ? $values[0] : '') : $mant_carcasa;
				$mant_carcasa_com = (ltl($question) == 'limpieza de carcasa: comentarios' && $retvav_mante == "evapora") ? (isset($values[0]) ? $values[0] : '') : $mant_carcasa_com;
				$mant_lubiMoto = (ltl($question) == 'limpieza y lubricacion de motores:' && $retvav_mante == "evapora") ? (isset($values[0]) ? $values[0] : '') : $mant_lubiMoto;
				$mant_lubiMoto_com = (ltl($question) == 'limpieza y lubricacion de motores: comentarios' && $retvav_mante == "evapora") ? (isset($values[0]) ? $values[0] : '') : $mant_lubiMoto_com;
				$mant_parElec = (ltl($question) == 'limpieza de parte electrica:' && $retvav_mante == "evapora") ? (isset($values[0]) ? $values[0] : '') : $mant_parElec;
				$mant_parElec_com = (ltl($question) == 'limpieza de parte electrica: comentarios' && $retvav_mante == "evapora") ? (isset($values[0]) ? $values[0] : '') : $mant_parElec_com;
				$mant_drena = (ltl($question) == 'limpieza de drenaje:' && $retvav_mante == "evapora") ? (isset($values[0]) ? $values[0] : '') : $mant_drena;
			
				$mant_serpe2 = (ltl($question) == 'limpieza de serpertin:' && $retvav_mante == "condensa") ? (isset($values[0]) ? $values[0] : '') : $mant_serpe2;
				$mant_carcasa2 = (ltl($question) == 'limpieza de carcasa:' && $retvav_mante == "condensa") ? (isset($values[0]) ? $values[0] : '') : $mant_carcasa2;
				$mant_carcasa_com2 = (ltl($question) == 'limpieza de carcasa: comentarios' && $retvav_mante == "condensa") ? (isset($values[0]) ? $values[0] : '') : $mant_carcasa_com2;
				$mant_lubiMoto2 = (ltl($question) == 'limpieza y lubricacion de motores:' && $retvav_mante == "condensa") ? (isset($values[0]) ? $values[0] : '') : $mant_lubiMoto2;
				$mant_lubiMoto_com2 = (ltl($question) == 'limpieza y lubricacion de motores: comentarios' && $retvav_mante == "condensa") ? (isset($values[0]) ? $values[0] : '') : $mant_lubiMoto_com2;
				$mant_parElec2 = (ltl($question) == 'limpieza de parte electrica:' && $retvav_mante == "condensa") ? (isset($values[0]) ? $values[0] : '') : $mant_parElec2;
				$mant_parElec_com2 = (ltl($question) == 'limpieza de parte electrica: comentarios' && $retvav_mante == "condensa") ? (isset($values[0]) ? $values[0] : '') : $mant_parElec_com2;
				
				$mant_observa = ltl($question) == 'observaciones:' ? (isset($values[0]) ? $values[0] : '') : $mant_observa;
				
				if(ltl($question) == 'captura de fotos'){
					
					if(isset($values[0]['filename']) != ''){
						$mante_fotos .= $referenceNumber.'_'.limpiar($values[0]['filename'])."|";
						$retval_mant = 'CLIMA-B';
					}
					if(isset($values[1]['filename']) != ''){
						$mante_fotos .= $referenceNumber.'_'.limpiar($values[1]['filename'])."|";
					}
					if(isset($values[2]['filename']) != ''){
						$mante_fotos .= $referenceNumber.'_'.limpiar($values[2]['filename'])."|";
					}
					if(isset($values[3]['filename']) != ''){
						$mante_fotos .= $referenceNumber.'_'.limpiar($values[3]['filename'])."|";
					}
					if(isset($values[4]['filename']) != ''){
						$mante_fotos .= $referenceNumber.'_'.limpiar($values[4]['filename'])."|";
					}
					if(isset($values[5]['filename']) != ''){
						$mante_fotos .= $referenceNumber.'_'.limpiar($values[5]['filename'])."|";
					}
					if(isset($values[6]['filename']) != ''){
						$mante_fotos .= $referenceNumber.'_'.limpiar($values[6]['filename'])."|";
					}
					if(isset($values[7]['filename']) != ''){
						$mante_fotos .= $referenceNumber.'_'.limpiar($values[7]['filename'])."|";
					}
					if(isset($values[8]['filename']) != ''){
						$mante_fotos .= $referenceNumber.'_'.limpiar($values[8]['filename'])."|";
					}
					if(isset($values[9]['filename']) != ''){
						$mante_fotos .= $referenceNumber.'_'.limpiar($values[9]['filename'])."|";
					}
				}
				
			}
			
			if("mantenimiento equipo (ventilacion)" == limpiar(trim(strtolower($pagina_nombre)))  && 
			($retval_mant != 'CLIMA-MAT-B' && $retval_mant != 'CLIMA-MAT-A')){
				
				if($mant_filtro != '' || $mant_carcasa != '' || $mant_lubiMoto != '' || $mant_parElec != '' || $mant_banda_polea != '' || $mant_lubrica_roda != '' || $mant_observa != ''){
					$retval_mant = 'MAT-VENTILACION';
				}
				
				$mant_filtro = ltl($question) == 'limpieza de filtros:' ? (isset($values[0]) ? $values[0] : '') : $mant_filtro;
				$mant_filtro_com = ltl($question) == 'limpieza de filtros: comentarios' ? (isset($values[0]) ? $values[0] : '') : $mant_filtro_com;
				$mant_carcasa = ltl($question) == 'limpieza de carcasa:' ? (isset($values[0]) ? $values[0] : '') : $mant_carcasa;
				$mant_carcasa_com = ltl($question) == 'limpieza de carcasa: comentarios' ? (isset($values[0]) ? $values[0] : '') : $mant_carcasa_com;	
				$mant_lubiMoto = ltl($question) == 'limpieza y lubricacion de motores:' ? (isset($values[0]) ? $values[0] : '') : $mant_lubiMoto;
				$mant_lubiMoto_com = ltl($question) == 'limpieza y lubricacion de motores: comentarios' ? (isset($values[0]) ? $values[0] : '') : $mant_lubiMoto_com;
				$mant_parElec = ltl($question) == 'limpieza de parte electrica:' ? (isset($values[0]) ? $values[0] : '') : $mant_parElec;
				$mant_parElec_com = ltl($question) == 'limpieza de parte electrica: comentarios' ? (isset($values[0]) ? $values[0] : '') : $mant_parElec_com;
				$mant_banda_polea = ltl($question) == 'ajuste de bandas y poleas:' ? (isset($values[0]) ? $values[0] : '') : $mant_banda_polea;
				$mant_banda_polea_com = ltl($question) == 'ajuste de bandas y poleas: comentarios' ? (isset($values[0]) ? $values[0] : '') : $mant_banda_polea_com;
				$mant_lubrica_roda = ltl($question) == 'limpieza y lubricacion de rodamientos:' ? (isset($values[0]) ? $values[0] : '') : $mant_lubrica_roda;
				$mant_lubrica_roda_com = ltl($question) == 'limpieza y lubricacion de rodamientos: comentarios' ? (isset($values[0]) ? $values[0] : '') : $mant_lubrica_roda_com;
				$mant_observa = ltl($question) == 'observaciones:' ? (isset($values[0]) ? $values[0] : '') : $mant_observa;
				
				if(ltl($question) == 'captura de fotos'){
					
					if(isset($values[0]['filename']) != ''){
						$mante_fotos .= $referenceNumber.'_'.limpiar($values[0]['filename'])."|";
						$retval_mant = 'VENTILACION';
					}
					if(isset($values[1]['filename']) != ''){
						$mante_fotos .= $referenceNumber.'_'.limpiar($values[1]['filename'])."|";
					}
					if(isset($values[2]['filename']) != ''){
						$mante_fotos .= $referenceNumber.'_'.limpiar($values[2]['filename'])."|";
					}
					if(isset($values[3]['filename']) != ''){
						$mante_fotos .= $referenceNumber.'_'.limpiar($values[3]['filename'])."|";
					}
					if(isset($values[4]['filename']) != ''){
						$mante_fotos .= $referenceNumber.'_'.limpiar($values[4]['filename'])."|";
					}
					if(isset($values[5]['filename']) != ''){
						$mante_fotos .= $referenceNumber.'_'.limpiar($values[5]['filename'])."|";
					}
					if(isset($values[6]['filename']) != ''){
						$mante_fotos .= $referenceNumber.'_'.limpiar($values[6]['filename'])."|";
					}
					if(isset($values[7]['filename']) != ''){
						$mante_fotos .= $referenceNumber.'_'.limpiar($values[7]['filename'])."|";
					}
					if(isset($values[8]['filename']) != ''){
						$mante_fotos .= $referenceNumber.'_'.limpiar($values[8]['filename'])."|";
					}
					if(isset($values[9]['filename']) != ''){
						$mante_fotos .= $referenceNumber.'_'.limpiar($values[9]['filename'])."|";
					}
				}
			}
			
			/* FIN MANTENIMIENTO DE LOS DISTINTOS TIPOS DE EQUIPOS */
			
			
			/* DIAGNOSTICO DE LOS DISTINTOS TIPOS DE EQUIPOS */
			
			if("diagnostico (climatizacion a)" == limpiar(trim(strtolower($pagina_nombre))) && 
			($retval_diag != "CLIMA-DIAG-B" && $retval_diag != "VENTILA")){
				
				if($diag_par_no_ope != "" || $diag_par_no_ope2 != "" || $diag_observa != "" || $diag_fotos != ""){
					$retval_diag = "CLIMA-DIAG-A";
				}
				
				$diag_conde = ltl($question) == 'evaporador / condensador' ? (isset($values[0]) ? $values[0] : '') : $diag_conde;
				
				if(ltl($question) == 'partes no operativas (reparacion):'){ 
					foreach($values as $dato){
						$diag_par_no_ope .= $dato."<br>";
					}					
				}
				
				if(ltl($question) == 'partes no operativas (cambio):'){ 
					foreach($values as $dato){
						$diag_par_no_ope2 .= $dato."<br>";
					}					
				}
				
	$diag_tiem_tra = ltl($question) == 'tiempo de trabajo:' ? (isset($values[0]['display']) ? $values[0]['display'] : '') : $diag_tiem_tra;
				$diag_observa = ltl($question) == 'observaciones:' ? (isset($values[0]) ? $values[0] : '') : $diag_observa;
				
				if(ltl($question) == 'comentarios:'){
					
					if(isset($values[0]['filename'])){
						$ext = pathinfo($values[0]['filename'], PATHINFO_EXTENSION);
						$diag_audio = $referenceNumber.'_'.$values[0]['identifier'].'.'.$ext;
					}		
				}
				
				if(ltl($question) == 'captura de fotos'){
					
					if(isset($values[0]['filename']) != ''){
						$diag_fotos .= $referenceNumber.'_'.limpiar($values[0]['filename'])."|";
					}
					if(isset($values[1]['filename']) != ''){
						$diag_fotos .= $referenceNumber.'_'.limpiar($values[1]['filename'])."|";
					}
					if(isset($values[2]['filename']) != ''){
						$diag_fotos .= $referenceNumber.'_'.limpiar($values[2]['filename'])."|";
					}
					if(isset($values[3]['filename']) != ''){
						$diag_fotos .= $referenceNumber.'_'.limpiar($values[3]['filename'])."|";
					}
					if(isset($values[4]['filename']) != ''){
						$diag_fotos .= $referenceNumber.'_'.limpiar($values[4]['filename'])."|";
					}
					if(isset($values[5]['filename']) != ''){
						$diag_fotos .= $referenceNumber.'_'.limpiar($values[5]['filename'])."|";
					}
					if(isset($values[6]['filename']) != ''){
						$diag_fotos .= $referenceNumber.'_'.limpiar($values[6]['filename'])."|";
					}
					if(isset($values[7]['filename']) != ''){
						$diag_fotos .= $referenceNumber.'_'.limpiar($values[7]['filename'])."|";
					}
					if(isset($values[8]['filename']) != ''){
						$diag_fotos .= $referenceNumber.'_'.limpiar($values[8]['filename'])."|";
					}
					if(isset($values[9]['filename']) != ''){
						$diag_fotos .= $referenceNumber.'_'.limpiar($values[9]['filename'])."|";
					}
				}
				
			}
			
			if("diagnostico (climatizacion b)" == limpiar(trim(strtolower($pagina_nombre))) && 
			($retval_diag != "CLIMA-DIAG-A" && $retval_diag != "VENTILA")){ 
				
				if($diag_par_no_ope != "" || $diag_par_no_ope2 != "" || 
				$diag_par_no_ope22 != "" || $diag_par_no_ope2_2 != "" || $diag_observa != "" || $diag_fotos != ""){
					$retval_diag = "CLIMA-DIAG-B";
				}
				
				if(ltl($question) == 'evaporador')
				{
					$diag_conde = "evapora";
				}
				if(ltl($question) == 'condensador')
				{
					$diag_conde = "condensa";
				}
				
				
				if(ltl($question) == 'partes no operativas (reparacion):' && $diag_conde == "evapora"){ 
					foreach($values as $dato){
						$diag_par_no_ope .= $dato."<br>";
					}
				}
				
				if(ltl($question) == 'partes no operativas (cambio):' && $diag_conde == "evapora"){ 
					foreach($values as $dato){
						$diag_par_no_ope2 .= $dato."<br>";
					}					
				}
				
				if(ltl($question) == 'partes no operativas (reparacion):' && $diag_conde == "condensa"){ 
					foreach($values as $dato){
						$diag_par_no_ope22 .= $dato."<br>";
					}					
				}
				
				if(ltl($question) == 'partes no operativas (cambio):' && $diag_conde == "condensa"){ 
					foreach($values as $dato){
						$diag_par_no_ope2_2 .= $dato."<br>";
					}					
				}
				
				$diag_tiem_tra = ltl($question) == 'tiempo de trabajo:' ? (isset($values[0]['display']) ? $values[0]['display'] : '') : $diag_tiem_tra;
				$diag_observa = ltl($question) == 'observaciones:' ? (isset($values[0]) ? $values[0] : '') : $diag_observa;
				
				if(ltl($question) == 'comentarios:'){
					
					if(isset($values[0]['filename'])){
						$ext = pathinfo($values[0]['filename'], PATHINFO_EXTENSION);
						$diag_audio = $referenceNumber.'_'.$values[0]['identifier'].'.'.$ext;
					}		
				}
				
				if(ltl($question) == 'captura de fotos'){
					
					if(isset($values[0]['filename']) != ''){
						$diag_fotos .= $referenceNumber.'_'.limpiar($values[0]['filename'])."|";
					}
					if(isset($values[1]['filename']) != ''){
						$diag_fotos .= $referenceNumber.'_'.limpiar($values[1]['filename'])."|";
					}
					if(isset($values[2]['filename']) != ''){
						$diag_fotos .= $referenceNumber.'_'.limpiar($values[2]['filename'])."|";
					}
					if(isset($values[3]['filename']) != ''){
						$diag_fotos .= $referenceNumber.'_'.limpiar($values[3]['filename'])."|";
					}
					if(isset($values[4]['filename']) != ''){
						$diag_fotos .= $referenceNumber.'_'.limpiar($values[4]['filename'])."|";
					}
					if(isset($values[5]['filename']) != ''){
						$diag_fotos .= $referenceNumber.'_'.limpiar($values[5]['filename'])."|";
					}
					if(isset($values[6]['filename']) != ''){
						$diag_fotos .= $referenceNumber.'_'.limpiar($values[6]['filename'])."|";
					}
					if(isset($values[7]['filename']) != ''){
						$diag_fotos .= $referenceNumber.'_'.limpiar($values[7]['filename'])."|";
					}
					if(isset($values[8]['filename']) != ''){
						$diag_fotos .= $referenceNumber.'_'.limpiar($values[8]['filename'])."|";
					}
					if(isset($values[9]['filename']) != ''){
						$diag_fotos .= $referenceNumber.'_'.limpiar($values[9]['filename'])."|";
					}
				}
			}
			
			if("diagnostico (ventilacion)" == limpiar(trim(strtolower($pagina_nombre))) && 
			($retval_diag != "CLIMA-DIAG-A" && $retval_diag != "CLIMA-DIAG-B")){
				
				if($diag_par_no_ope != "" || $diag_par_no_ope2 != "" || $diag_observa != "" || $diag_fotos != ""){
					$retval_diag = "VENTILA";
				}
								
				if(ltl($question) == 'partes no operativas (reparacion):'){ 
					foreach($values as $dato){
						$diag_par_no_ope .= $dato."<br>";
					}					
				}
				
				if(ltl($question) == 'partes no operativas (cambio):'){ 
					foreach($values as $dato){
						$diag_par_no_ope2 .= $dato."<br>";
					}					
				}
				
	$diag_tiem_tra = ltl($question) == 'tiempo de trabajo:' ? (isset($values[0]['display']) ? $values[0]['display'] : '') : $diag_tiem_tra;
				$diag_observa = ltl($question) == 'observaciones:' ? (isset($values[0]) ? $values[0] : '') : $diag_observa;
				
				if(ltl($question) == 'comentarios:'){
					
					if(isset($values[0]['filename'])){
						$ext = pathinfo($values[0]['filename'], PATHINFO_EXTENSION);
						$diag_audio = $referenceNumber.'_'.$values[0]['identifier'].'.'.$ext;
					}		
				}
				
				if(ltl($question) == 'captura de fotos'){
					
					if(isset($values[0]['filename']) != ''){
						$diag_fotos .= $referenceNumber.'_'.limpiar($values[0]['filename'])."|";
					}
					if(isset($values[1]['filename']) != ''){
						$diag_fotos .= $referenceNumber.'_'.limpiar($values[1]['filename'])."|";
					}
					if(isset($values[2]['filename']) != ''){
						$diag_fotos .= $referenceNumber.'_'.limpiar($values[2]['filename'])."|";
					}
					if(isset($values[3]['filename']) != ''){
						$diag_fotos .= $referenceNumber.'_'.limpiar($values[3]['filename'])."|";
					}
					if(isset($values[4]['filename']) != ''){
						$diag_fotos .= $referenceNumber.'_'.limpiar($values[4]['filename'])."|";
					}
					if(isset($values[5]['filename']) != ''){
						$diag_fotos .= $referenceNumber.'_'.limpiar($values[5]['filename'])."|";
					}
					if(isset($values[6]['filename']) != ''){
						$diag_fotos .= $referenceNumber.'_'.limpiar($values[6]['filename'])."|";
					}
					if(isset($values[7]['filename']) != ''){
						$diag_fotos .= $referenceNumber.'_'.limpiar($values[7]['filename'])."|";
					}
					if(isset($values[8]['filename']) != ''){
						$diag_fotos .= $referenceNumber.'_'.limpiar($values[8]['filename'])."|";
					}
					if(isset($values[9]['filename']) != ''){
						$diag_fotos .= $referenceNumber.'_'.limpiar($values[9]['filename'])."|";
					}
				}
				
			}
			
			/* FIN DIAGNOSTICO DE LOS DISTINTOS TIPOS DE EQUIPOS */
			
			
			if("materiales requeridos" == limpiar(trim(strtolower($pagina_nombre)))){
				$mat_selitem1 = ltl($question) == 'seleccion de item:' ? (isset($values[0]) ? $values[0] : '') : $mat_selitem1;
				$mat_desitem1 = ltl($question) == 'descripcion item:' ? (isset($values[0]) ? $values[0] : '') : $mat_desitem1;
				$mat_uniditem1 = ltl($question) == 'definir unidad:' ? (isset($values[0]) ? $values[0] : '') : $mat_uniditem1;
				$mat_cantitem1 = ltl($question) == 'cantidad requerida:' ? (isset($values[0]) ? $values[0] : '') : $mat_cantitem1;
				$mat_observa = ltl($question) == 'observaciones:' ? (isset($values[0]) ? $values[0] : '') : $mat_observa;
			}
			
			if("item #2" == limpiar(trim(strtolower($pagina_nombre)))){
				$mat_selitem2 = ltl($question) == 'seleccion de item:' ? (isset($values[0]) ? $values[0] : '') : $mat_selitem2;
				$mat_desitem2 = ltl($question) == 'descripcion item:' ? (isset($values[0]) ? $values[0] : '') : $mat_desitem2;
				$mat_uniditem2 = ltl($question) == 'definir unidad:' ? (isset($values[0]) ? $values[0] : '') : $mat_uniditem2;
				$mat_cantitem2 = ltl($question) == 'cantidad requerida:' ? (isset($values[0]) ? $values[0] : '') : $mat_cantitem2;
			}
			
			if("item #3" == limpiar(trim(strtolower($pagina_nombre)))){
				$mat_selitem3 = ltl($question) == 'seleccion de item:' ? (isset($values[0]) ? $values[0] : '') : $mat_selitem3;
				$mat_desitem3 = ltl($question) == 'descripcion item:' ? (isset($values[0]) ? $values[0] : '') : $mat_desitem3;
				$mat_uniditem3 = ltl($question) == 'definir unidad:' ? (isset($values[0]) ? $values[0] : '') : $mat_uniditem3;
				$mat_cantitem3 = ltl($question) == 'cantidad requerida:' ? (isset($values[0]) ? $values[0] : '') : $mat_cantitem3;
			}
			
			if("item #4" == limpiar(trim(strtolower($pagina_nombre)))){
				$mat_selitem4 = ltl($question) == 'seleccion de item:' ? (isset($values[0]) ? $values[0] : '') : $mat_selitem4;
				$mat_desitem4 = ltl($question) == 'descripcion item:' ? (isset($values[0]) ? $values[0] : '') : $mat_desitem4;
				$mat_uniditem4 = ltl($question) == 'definir unidad:' ? (isset($values[0]) ? $values[0] : '') : $mat_uniditem4;
				$mat_cantitem4 = ltl($question) == 'cantidad requerida:' ? (isset($values[0]) ? $values[0] : '') : $mat_cantitem4;
			}
			
			if("item #5" == limpiar(trim(strtolower($pagina_nombre)))){
				$mat_selitem5 = ltl($question) == 'seleccion de item:' ? (isset($values[0]) ? $values[0] : '') : $mat_selitem5;
				$mat_desitem5 = ltl($question) == 'descripcion item:' ? (isset($values[0]) ? $values[0] : '') : $mat_desitem5;
				$mat_uniditem5 = ltl($question) == 'definir unidad:' ? (isset($values[0]) ? $values[0] : '') : $mat_uniditem5;
				$mat_cantitem5 = ltl($question) == 'cantidad requerida:' ? (isset($values[0]) ? $values[0] : '') : $mat_cantitem5;
			}
			
			if("item #6" == limpiar(trim(strtolower($pagina_nombre)))){
				$mat_selitem6 = ltl($question) == 'seleccion de item:' ? (isset($values[0]) ? $values[0] : '') : $mat_selitem6;
				$mat_desitem6 = ltl($question) == 'descripcion item:' ? (isset($values[0]) ? $values[0] : '') : $mat_desitem6;
				$mat_uniditem6 = ltl($question) == 'definir unidad:' ? (isset($values[0]) ? $values[0] : '') : $mat_uniditem6;
				$mat_cantitem6 = ltl($question) == 'cantidad requerida:' ? (isset($values[0]) ? $values[0] : '') : $mat_cantitem6;
			}
			
			if("item #7" == limpiar(trim(strtolower($pagina_nombre)))){
				$mat_selitem7 = ltl($question) == 'seleccion de item:' ? (isset($values[0]) ? $values[0] : '') : $mat_selitem7;
				$mat_desitem7 = ltl($question) == 'descripcion item:' ? (isset($values[0]) ? $values[0] : '') : $mat_desitem7;
				$mat_uniditem7 = ltl($question) == 'definir unidad:' ? (isset($values[0]) ? $values[0] : '') : $mat_uniditem7;
				$mat_cantitem7 = ltl($question) == 'cantidad requerida:' ? (isset($values[0]) ? $values[0] : '') : $mat_cantitem7;
			}
			
			if("item #8" == limpiar(trim(strtolower($pagina_nombre)))){
				$mat_selitem8 = ltl($question) == 'seleccion de item:' ? (isset($values[0]) ? $values[0] : '') : $mat_selitem8;
				$mat_desitem8 = ltl($question) == 'descripcion item:' ? (isset($values[0]) ? $values[0] : '') : $mat_desitem8;
				$mat_uniditem8 = ltl($question) == 'definir unidad:' ? (isset($values[0]) ? $values[0] : '') : $mat_uniditem8;
				$mat_cantitem8 = ltl($question) == 'cantidad requerida:' ? (isset($values[0]) ? $values[0] : '') : $mat_cantitem8;
			}
			
			if("item #9" == limpiar(trim(strtolower($pagina_nombre)))){
				$mat_selitem9 = ltl($question) == 'seleccion de item:' ? (isset($values[0]) ? $values[0] : '') : $mat_selitem9;
				$mat_desitem9 = ltl($question) == 'descripcion item:' ? (isset($values[0]) ? $values[0] : '') : $mat_desitem9;
				$mat_uniditem9 = ltl($question) == 'definir unidad:' ? (isset($values[0]) ? $values[0] : '') : $mat_uniditem9;
				$mat_cantitem9 = ltl($question) == 'cantidad requerida:' ? (isset($values[0]) ? $values[0] : '') : $mat_cantitem9;
			}
			
			if("item #10" == limpiar(trim(strtolower($pagina_nombre)))){
				$mat_selitem10 = ltl($question) == 'seleccion de item:' ? (isset($values[0]) ? $values[0] : '') : $mat_selitem10;
				$mat_desitem10 = ltl($question) == 'descripcion item:' ? (isset($values[0]) ? $values[0] : '') : $mat_desitem10;
				$mat_uniditem10 = ltl($question) == 'definir unidad:' ? (isset($values[0]) ? $values[0] : '') : $mat_uniditem10;
				$mat_cantitem10 = ltl($question) == 'cantidad requerida:' ? (isset($values[0]) ? $values[0] : '') : $mat_cantitem10;
			}
			
			if("herramientas de trabajo" == limpiar(trim(strtolower($pagina_nombre))))
			{
				if(ltl($question) == 'herramientas necesarias para el trabajo:'){ 
					foreach($values as $dato){
						$herr_detalle .= $dato."<br>";
					}					
				}
				
	$herr_otros = ltl($question) == 'si eligio la opcion otros por favor especificar:' ? (isset($values[0]) ? $values[0] : '') : $herr_otros;
			}
			
			
			/* PRUEBAS FINALES DE LOS DISTINTOS EQUIPOS */
			
			if("pruebas finales equipo (climatizacion)" == limpiar(trim(strtolower($pagina_nombre))) && 
			$prueFin_retval != 'VENTILA'){
				
				if($prueFin_1 != "" || $prueFin_2 != "" || $prueFin_3 != "" || $prueFin_4 != "" || $prueFin_5 != ""){
					$prueFin_retval = "CLIMA";	
				}
				
				$prueFin_1 = ltl($question) == '¿el equipo enciende y funciona?' ? (isset($values[0]) ? $values[0] : '') : $prueFin_1;
			$prueFin_2 = ltl($question) == '¿el equipo enciende y funciona? comentarios' ? (isset($values[0]) ? $values[0] : '') : $prueFin_2;
				$prueFin_3 = ltl($question) == 'prueba de arranque y funcionamiento:' ? (isset($values[0]['display']) ? $values[0]['display'] : '') : $prueFin_3;
				$prueFin_4 = ltl($question) == '¿se realizo la prueba de drenaje?' ? (isset($values[0]) ? $values[0] : '') : $prueFin_4;
			$prueFin_5 = ltl($question) == '¿se realizo la prueba de drenaje? comentarios' ? (isset($values[0]) ? $values[0] : '') : $prueFin_5;
			$prueFin_obser = ltl($question) == 'observaciones generales del equipo:' ? (isset($values[0]) ? $values[0] : '') : $prueFin_obser;
			}
			
			if("pruebas finales equipo (ventilacion)" == limpiar(trim(strtolower($pagina_nombre))) && 
			$prueFin_retval != 'CLIMA'){
				
				if($prueFin_1 != "" || $prueFin_2 != "" || $prueFin_3 != "" || $prueFin_4 != "" || $prueFin_5 != ""){
					$prueFin_retval = "VENTILA";	
				}
				
				$prueFin_1 = ltl($question) == '¿el equipo enciende y funciona?' ? (isset($values[0]) ? $values[0] : '') : $prueFin_1;
			$prueFin_2 = ltl($question) == '¿el equipo enciende y funciona? comentarios' ? (isset($values[0]) ? $values[0] : '') : $prueFin_2;
				$prueFin_3 = ltl($question) == 'prueba de arranque y funcionamiento:' ? (isset($values[0]['display']) ? $values[0]['display'] : '') : $prueFin_3;
				$prueFin_4 = ltl($question) == '¿se realizo la prueba de drenaje?' ? (isset($values[0]) ? $values[0] : '') : $prueFin_4;
			$prueFin_5 = ltl($question) == '¿se realizo la prueba de drenaje? comentarios' ? (isset($values[0]) ? $values[0] : '') : $prueFin_5;
			$prueFin_obser = ltl($question) == 'observaciones generales del equipo:' ? (isset($values[0]) ? $values[0] : '') : $prueFin_obser;
			}
			
			/* FIN PRUEBAS FINALES DE LOS DISTINTOS EQUIPOS */
			
			if("observaciones generales y firmas" == limpiar(trim(strtolower($pagina_nombre)))){
				$resposable = ltl($question) == 'nombre del responsable que supervisa el trabajo:' ? (isset($values[0]) ? $values[0] : '') : $resposable;
				$cargo_respon = ltl($question) == 'cargo:' ? (isset($values[0]) ? $values[0] : '') : $cargo_respon;
			$observa_gen_fin = ltl($question) == 'observaciones generales:' ? (isset($values[0]) ? $values[0] : '') : $observa_gen_fin;
				
				//IMAGEN DE LA FIRMA DIGITAL
				if(ltl($question) == 'firma del cliente o responsable'){
					if(isset($values[0]['filename']))
						$img_firma = $referenceNumber.'_'.$values[0]['filename'];
				}
				
				//AUDIO DEL COMENTARIO FINAL
				if(ltl($question) == 'comentarios generales:'){
					if(isset($values[0]['filename'])){
						$ext = pathinfo($values[0]['filename'], PATHINFO_EXTENSION);
						$audio_comen = $referenceNumber.'_'.$values[0]['identifier'].'.'.$ext;
					}
				}
			}
        }
    }
	
	
    // MySQL
    $mysqli = $connect->db;
    $id_usuario = 2;
    if(isset($user_username) && $user_username != "" && $user_username == "ernesto.polit"){
        $mysqli = $connect->db2;
        $id_usuario = 2;
    }
	
	
	if($retval_mant == 'CLIMA-A'){
		$infGen_TipoRepote = "MANTENIMIENTO A";
	}
	elseif($retval_mant == 'CLIMA-B'){
		$infGen_TipoRepote = "MANTENIMIENTO B";
	}
	elseif($retval_mant == 'MAT-VENTILACION'){
		$infGen_TipoRepote = "MANTENIMIENTO VENTILACION";
	}
	else if($retval_diag == 'CLIMA-DIAG-A'){
		$infGen_TipoRepote = "DIAGNOSTICO A";
	}
	else if($retval_diag == 'CLIMA-DIAG-B'){
		$infGen_TipoRepote = "DIAGNOSTICO B";
	}
	else if($retval_diag == 'VENTILA'){
		$infGen_TipoRepote = "DIAGNOSTICO VENTILACION";
	}
	
    $sql = "INSERT INTO `reportes_r_mantenimiento` SET id_orden='$infGen_idOrden',
	`geolocalizacion`='$infGen_Geoloca',`fecha_mante`='$infGen_fecMant',`hora_llegada`='$infGen_HoraLlegada',
	`hora_programada`='$infGen_horaPro',`cliente`='$cot_cliente',`id_cliente`='$cot_id_cliente',
	`tipo_cliente`='$cot_tipo_cli',`sucursal`='$cot_sucursal',`id_sucursal`='$cot_id_sucursal',`direccion`='$cot_direccion',
	`referencia`='$cli_refLugar',`detalle_trabajo`='$infTrabajo ',tipo_trabajo='$cot_tipoTrabajo',`observacion1`='$infObserva',
	
	
	`tipo_equipo`='$ord_tipoEquipo',
	
	
	`codigo_equipo`='$ord_codEquipo',`desc_equipo`='$ord_desEquipo',`nombre_area`='$ord_nombreArea ',
	`area_clima`='$ord_areaClimatiza',`id_area`='$ord_id_areaClimatiza',
	`evaporador`='$ord_evacon',`marca_eva`='$equi_marca',`btu_eva`='$ord_climaBtu',
	`modelo_eva`='$equi_modelo',`serie_eva`='$equi_serie',
	`condensador`='$ord_evacon2',
	`marca_conde`='$equi_marca2',`btu_conde`='$ord_climaBtu2',
	`modelo_conde`='$equi_modelo2',`serie_conde`='$equi_serie2',`observa2`='$orde_observas',`pruebas_ini`='$orde_pruebaIni',
	`equipo_operativo`='$equi_opera',
	capacidad_hp_eva='$ord_VentilaHP',capacidad_hp_conde='$ord_VentilaHP2',
	
	
	`evaporador_mante`='$mant_conde',`limpia_serpin_eva`='$mant_serpe',`limpia_filtro_eva`='$mant_filtro',
	`limpia_filtro_com_eva`='$mant_filtro_com',`limpia_carca_eva`='$mant_carcasa',`limpia_carca_com_eva`='$mant_carcasa_com',
	`limpia_motor_eva`='$mant_lubiMoto',`limpia_motor_com_eva`='$mant_lubiMoto_com',`limpia_elec_eva`='$mant_parElec',
	`limpia_elec_com_eva`='$mant_parElec_com',`limpia_drenaje`='$mant_drena',
	`condensador_mant`='$mant_observa',
	`limpia_serpin_conde`='$mant_serpe2',`limpia_carca_conde`='$mant_carcasa2',`limpia_carca_com_conde`='$mant_carcasa_com2',
	`limpia_motor_conde`='$mant_lubiMoto2',`limpia_motor_com_conde`='$mant_lubiMoto_com2',
	`limpia_elec_conde`='$mant_parElec2',`limpia_elec_com_conde`='$mant_parElec_com2',
	
	`ajuste_band_polea`='$mant_banda_polea',
	`ajuste_band_polea_com`='$mant_banda_polea_com',
	`lim_lubri_roda`='$mant_lubrica_roda',
	`lim_lubri_roda_com`='$mant_lubrica_roda_com',
	
	
	`pf_pregunta_1`='$prueFin_1',
	`pf_pregunta_2`='$prueFin_2',
	`pf_pregunta_3`='$prueFin_3',
	`pf_pregunta_4`='$prueFin_4',
	`pf_pregunta_5`='$prueFin_5',
	`pf_observa`='$prueFin_obser',
	
	`responsable`='$resposable',`cargo_responsable`='$cargo_respon',`observa_fin`='$observa_gen_fin',
	img_firma='$img_firma',audio_comentario_gral='$audio_comen',
	
	
	`diag_conden`='$diag_conde',`diag_parno_opera`='$diag_par_no_ope',`diag_parno_opera2`='$diag_par_no_ope2',
	`diag_eva`='$diag_conde2',`diag_parno_eva`='$diag_par_no_ope22',
	`diag_parno_eva2`='$diag_par_no_ope2_2',`diag_tiem_tra`='$diag_tiem_tra',`diag_observa`='$diag_observa',
	
	
	`mat_item1_sel`='$mat_selitem1',`mat_item1_des`='$mat_desitem1',`mat_item1_uni`='$mat_uniditem1',
	`mat_item1_cant`='$mat_cantitem1',`mat_item1_obser`='$mat_observa',`mat_item2_sel`='$mat_selitem2',`mat_item2_des`='$mat_desitem2',
	`mat_item2_uni`='$mat_uniditem2',`mat_item2_cant`='$mat_cantitem2',`mat_item3_sel`='$mat_selitem3',
	`mat_item3_des`='$mat_desitem3',`mat_item3_uni`='$mat_uniditem3',`mat_item3_cant`='$mat_cantitem3',`mat_item4_sel`='$mat_selitem4',
	`mat_item4_des`='$mat_desitem4',`mat_item4_uni`='$mat_uniditem4',
	`mat_item4_cant`='$mat_cantitem4',`mat_item5_sel`='$mat_selitem5',
	`mat_item5_des`='$mat_desitem5',`mat_item5_uni`='$mat_uniditem5',`mat_item5_cant`='$mat_cantitem5',
	`mat_item6_sel`='$mat_selitem6',`mat_item6_des`='$mat_desitem6',
	`mat_item6_uni`='$mat_uniditem6',`mat_item6_cant`='$mat_cantitem6',
	`mat_item7_sel`='$mat_selitem7',`mat_item7_des`='$mat_desitem7',`mat_item7_uni`='$mat_uniditem7',
	`mat_item7_cant`='$mat_cantitem7',`mat_item8_sel`='$mat_selitem8',
	`mat_item8_des`='$mat_desitem8',`mat_item8_uni`='$mat_uniditem8',
	`mat_item8_cant`='$mat_cantitem8',`mat_item9_sel`='$mat_selitem9',`mat_item9_des`='$mat_desitem9',
	`mat_item9_uni`='$mat_uniditem9',`mat_item9_cant`='$mat_cantitem9',
	`mat_item10_sel`='$mat_selitem10',`mat_item10_des`='$mat_desitem10',
	`mat_item10_uni`='$mat_uniditem10',`mat_item10_cant`='$mat_cantitem10',
	
	`herr_detalle`='$herr_detalle',`herr_otros`='$herr_otros',
	
	
	diag_audio='$diag_audio',diag_fotos='$diag_fotos',mante_fotos='$mante_fotos',
	
	tipo_reporte='$infGen_TipoRepote',nombre_json='$nombre_json'
	
	"; 
	if($mysqli->query($sql)== TRUE){
        
    } else {
        echo "Error: " . $last_cot . "<br>" . $mysqli->error;
    }
	
	echo "<br>Inserto: ".$nombre_json;
	#die("NOE TERMINO ".$sql);
}

function D($arreglo){
    echo "<pre>";
        print_r($arreglo);
    echo "</pre>";
}

function limpiar($String)
{
    $String = utf8_encode($String);
	
	$String = str_replace(array('á','à','â','ã','ª','ä'),"a",$String);
    $String = str_replace(array('Á','À','Â','Ã','Ä'),"A",$String);
    $String = str_replace(array('Í','Ì','Î','Ï'),"I",$String);
    $String = str_replace(array('í','ì','î','ï'),"i",$String);
    $String = str_replace(array('é','è','ê','ë'),"e",$String);
    $String = str_replace(array('É','È','Ê','Ë'),"E",$String);
    $String = str_replace(array('ó','ò','ô','õ','ö','º'),"o",$String);
    $String = str_replace(array('Ó','Ò','Ô','Õ','Ö'),"O",$String);
    $String = str_replace(array('ú','ù','û','ü'),"u",$String);
    $String = str_replace(array('Ú','Ù','Û','Ü'),"U",$String);
    $String = str_replace(array('[','^','´','`','¨','~',']'),"",$String);
    $String = str_replace("ç","c",$String);
    $String = str_replace("Ç","C",$String);
    $String = str_replace("ñ","n",$String);
    $String = str_replace("Ñ","N",$String);
    $String = str_replace("Ý","Y",$String);
    $String = str_replace("ý","y",$String);
    
    $String = str_replace("&aacute;","a",$String);
    $String = str_replace("&Aacute;","A",$String);
    $String = str_replace("&eacute;","e",$String);
    $String = str_replace("&Eacute;","E",$String);
    $String = str_replace("&iacute;","i",$String);
    $String = str_replace("&Iacute;","I",$String);
    $String = str_replace("&oacute;","o",$String);
    $String = str_replace("&Oacute;","O",$String);
    $String = str_replace("&uacute;","u",$String);
    $String = str_replace("&Uacute;","U",$String);

    $String = str_replace("\u00C1;","Á",$String);
    $String = str_replace("\u00E1;","á",$String);
    $String = str_replace("\u00C9;","É",$String);
    $String = str_replace("\u00E9;","é",$String);
    $String = str_replace("\u00CD;","Í",$String);
    $String = str_replace("\u00ED;","í",$String);
    $String = str_replace("\u00D3;","Ó",$String);
    $String = str_replace("\u00F3;","ó",$String);
    $String = str_replace("\u00DA;","Ú",$String);
    $String = str_replace("\u00FA;","ú",$String);
    $String = str_replace("\u00DC;","Ü",$String);
    $String = str_replace("\u00FC;","ü",$String);
    $String = str_replace("\u00D1;","Ṅ",$String);
    $String = str_replace("\u00F1;","ñ",$String);

    return $String;
}

function ltl($s)
{
    return limpiar(trim(strtolower($s)));
}