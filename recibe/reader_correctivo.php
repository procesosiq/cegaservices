<?php
header('Content-Type: text/html; charset=utf-8');
ini_set('display_errors',1);
error_reporting(E_ALL);

/** DATABASE */
$mysqli = @new mysqli("localhost", "auditoriasbonita", "u[V(fTIUbcVb", "cegaservices2");

#$mysqli = @new mysqli("localhost", "root", "", "auditoriasbonita");

if (mysqli_connect_errno()) {
    printf("Falló la conexión: %s\n", mysqli_connect_error());
    exit();
}
$mysqli->set_charset("utf8");

/** Directorio de los JSON */
$path = realpath('./json');

$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
foreach($objects as $name => $object){
    if('.' != $object->getFileName() && '..' != $object->getFileName() && $path != $object->getPath()){
        $pos1 = strpos($object->getPath(), "/recibe/json/revision_correctivo_");
        if($pos1 !== false){
            $ext = pathinfo($object->getPathName(), PATHINFO_EXTENSION);
            if('json' == $ext || 'jpeg' == $ext || 'jpg' == $ext || 'png' == $ext || 'mp3' == $ext || '3gpp' == $ext){
                switch ($ext) {
                    case 'json':
                        /* READ AND MOVE JSON */
                        $json = json_decode(trim(file_get_contents($object->getPathName())), true);
                        echo '<pre>';
                        //print_r($json);
                        echo '</pre>';
                        process_json($json, $object->getFileName(), $mysqli);
                        exit;
                        #move_json($object->getPathName(), $object->getFileName());
                        break;

                    case 'jpeg':
                    case 'jpg':
                    case 'png':
                        /* MOVE JSON */
                        #move_image($object->getPathName(), $object->getFileName());
                        break;

                    case '3gpp':
                    case 'mp3':
                        /* MOVE AUDIO */
                        #move_audio($object->getPathName(), $object->getFileName());
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
//$mysqli->close();

function move_json($file, $nameFile){
    echo $file;
    echo $nameFile;
    // die();
    if(!rename($file, __DIR__."/../reportes_vistos/json/$nameFile")){
        echo 'error';
    }
}

function move_image($file, $nameFile){
    // die();
    if(!rename($file, __DIR__."/../reportes_vistos/image/$nameFile")){
        echo 'error';
    }
}

function move_audio($file, $nameFile){
    // die();
    if(!rename($file, __DIR__."/../reportes_vistos/audio/$nameFile")){
        echo 'error';
    }
}

function process_json($json, $filename, $mysqli){
    $identifier = $json['identifier'];
    $version = $json['version']. '';
    $zone = $json['zone']. '';
    $referenceNumber = $json['referenceNumber']. '';
    $state = $json['state']. '';
    $deviceSubmitDate = $json['deviceSubmitDate'];
    $deviceSubmitDateDate = $deviceSubmitDate['time']. '';
    $deviceSubmitDateZone = $deviceSubmitDate['zone']. '';
    $fecha_file = str_replace("-", "", explode("T", $deviceSubmitDateDate)[0]);
    $shiftedDeviceSubmitDate = $json['shiftedDeviceSubmitDate'].'';
    $serverReceiveDate = $json['serverReceiveDate'].'';

    $form = $json['form'];
    $form_identifier = $form['identifier'].'';
    $form_versionIdentifier = $form['versionIdentifier'].'';
    $form_name = $form['name'].'';
    $form_version = $form['version'].'';
    $form_formSpaceIdentifier = $form['formSpaceIdentifier'].'';
    $form_formSpaceName = $form['formSpaceName'].'';

    $user = $json['user'];
    $user_identifier = $user['identifier'].'';
    $user_username = $user['username'].'';
    $user_displayName = $user['displayName'].'';

    $geoStamp = $json['geoStamp'];
    $geoStamp_success = $geoStamp['success'].'';
    $geoStamp_captureTimestamp = $geoStamp['captureTimestamp'];
    $geoStamp_captureTimestamp_provided = $geoStamp_captureTimestamp['provided'];
    $geoStamp_captureTimestamp_provided_time = $geoStamp_captureTimestamp_provided['time'].'';;
    $geoStamp_captureTimestamp_provided_zone = $geoStamp_captureTimestamp_provided['zone'].'';;
    $geoStamp_captureTimestamp_shifted = $geoStamp_captureTimestamp['shifted'].'';;
    $geoStamp_errorMessage = $geoStamp['errorMessage'].'';;
    $geoStamp_source = $geoStamp['source'].'';;
    $geoStamp_coordinates = $geoStamp['coordinates'];
    $geoStamp_coordinates_latitude = $geoStamp_coordinates['latitude'].'';;
    $geoStamp_coordinates_longitude = $geoStamp_coordinates['longitude'].'';;
    $geoStamp_coordinates_altitude = $geoStamp_coordinates['altitude'].'';;
    $geoStamp_address = $geoStamp['address'].'';;

    $pages = $json['pages'];

    // correctivo
/*Info general*/               
$infgen_fechaHora="";
$infgen_geolocalizacion="";
$infgen_fechaCorrectivo="";
$infgen_hrllegada="";
$infgen_cliente="";
$infgen_tipoCliente="";
$infgen_direccion="";
$infgen_refLugar="";
$infgen_ruta="";
$infgen_refLugar="";
$infgen_referencia="";
$infgen_observaciones="";
$infgen_tipoTrabajo="";
$infgen_id_cliente="";
$infgen_id_sucursal="";
$infgen_sucursal="";

/*Info equipo*/
$infequ_tipo="";
$infequ_id_tipoEquipo="";
$infequ_areaClimatiza="";
$infequ_areaNombre="";
$infequ_id_areaClimatiza="";

/*Partes*/
$parte1_descripcion="";
$parte1_descripcion_id="";
$parte1_descripcion_nombre="";
$parte1_parte="";
$parte1_pieza="";
$parte1_tipo="";
$parte1_desTrabajo="";
$parte1_add1="";

$parte2_parte="";
$parte2_pieza="";
$parte2_tipo="";
$parte2_desTrabajo="";
$parte2_add1="";

$parte3_parte="";
$parte3_pieza="";
$parte3_tipo="";
$parte3_desTrabajo="";
$parte3_add1="";

$parte4_parte="";
$parte4_pieza="";
$parte4_tipo="";
$parte4_desTrabajo="";
$parte4_add1="";

$parte5_parte="";
$parte5_pieza="";
$parte5_tipo="";
$parte5_desTrabajo="";
$parte5_add1="";

$parte6_parte="";
$parte6_pieza="";
$parte6_tipo="";
$parte6_desTrabajo="";
$parte6_add1="";

$parte7_parte="";
$parte7_pieza="";
$parte7_tipo="";
$parte7_desTrabajo="";
$parte7_add1="";

$parte8_parte="";
$parte8_pieza="";
$parte8_tipo="";
$parte8_desTrabajo="";
$parte8_add1="";

$parte9_parte="";
$parte9_pieza="";
$parte9_tipo="";
$parte9_desTrabajo="";
$parte9_add1="";

$parte10_parte="";
$parte10_pieza="";
$parte10_tipo="";
$parte10_desTrabajo="";

/*Materiales con items*/
$item1="";
$item1_seleccion="";
$item1_descripcion="";
$item1_unidad="";
$item1_cantidad="";
$item1_observaciones="";

$item2="";
$item2_seleccion="";
$item2_descripcion="";
$item2_unidad="";
$item2_cantidad="";
$item2_observaciones="";

$item3="";
$item3_seleccion="";
$item3_descripcion="";
$item3_unidad="";
$item3_cantidad="";
$item3_observaciones="";

$item4="";
$item4_seleccion="";
$item4_descripcion="";
$item4_unidad="";
$item4_cantidad="";
$item4_observaciones="";

$item5="";
$item5_seleccion="";
$item5_descripcion="";
$item5_unidad="";
$item5_cantidad="";
$item5_observaciones="";

$item6="";
$item6_seleccion="";
$item6_descripcion="";
$item6_unidad="";
$item6_cantidad="";
$item6_observaciones="";

$item7="";
$item7_seleccion="";
$item7_descripcion="";
$item7_unidad="";
$item7_cantidad="";
$item7_observaciones="";

$item8="";
$item8_seleccion="";
$item8_descripcion="";
$item8_unidad="";
$item8_cantidad="";
$item8_observaciones="";

$item9="";
$item9_seleccion="";
$item9_descripcion="";
$item9_unidad="";
$item9_cantidad="";
$item9_observaciones="";

$item10="";
$item10_seleccion="";
$item10_descripcion="";
$item10_unidad="";
$item10_cantidad="";
$item10_observaciones="";

    // ciclo
    foreach($pages as $key => $page_data){
        $pagina = $key+1;
        $pagina_nombre = $page_data['name'];
        $sql_muestra_causas = array();
        $answers = $page_data["answers"];

        echo '<br><br><br>'.ltl($pagina_nombre).'<hr>';
        foreach($answers as $answer){
            $label    = $answer['label'];
            $dataType = $answer['dataType'];
            $question = $answer['question'];
            $values   = $answer['values'];
            $labelita = limpiar(trim(strtolower($label)));

            echo '----'.ltl($question).'<br>--------<br>';
            if(is_array($values[0]))
                print_r($values);
            else
                echo $values[0];
            echo '<br>';
        }


    die($pagina_nombre);
    
    if("informacion general" == ltl($pagina_nombre))
    {
        $infgen_fechaHora = "fyhds:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $infgen_fechaHora; // Fecha y Hora del Sistema:
        $infgen_geolocalizacion = "geolocalizacion" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $infgen_geolocalizacion; // Geolocalización
        $infgen_fechaCorrectivo = "fdc:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $infgen_fechaCorrectivo; // Fecha de la Instalación
        $infgen_hrllegada = "hora de llegada:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $infgen_hrllegada; // Hora de Llegada:
        $infgen_cliente = "cliente" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $infgen_hrllegada; // Cliente:
        $infgen_id_cliente = ltl($question) == "cliente: - id" ? (isset($values[0]) ? $values[0] : '') : $infgen_id_cliente;
        $infgen_tipoCliente = "c-i__" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $infgen_tipoCliente; // Tipo de Cliente:
        $infgen_sucursal = ltl($question) == "sucursal" ? (isset($values[0]) ? $values[0] : '') : $infgen_id_sucursal;
        $infgen_id_sucursal = ltl($question) == "sucursal: - id" ? (isset($values[0]) ? $values[0] : '') : $infgen_id_sucursal;
        $infgen_direccion = "direccion:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $infgen_direccion; // Dirección:
        $infgen_refLugar = "rdl:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $infgen_refLugar; // Referencia del Lugar:
        $infgen_ruta = "ruta" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $infgen_ruta; // Ruta
        $infgen_referencia = "uearpdunr" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $infgen_referencia; // Utilice esta área para detallar una nueva referencia
        $infgen_observaciones = "observaciones: 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $infgen_observaciones; // Observaciones:
        $infgen_tipoTrabajo = "tipo de trabajo:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $infgen_tipoTrabajo; // Tipo de Trabajo:
    }

    if("informacion equipo" == ltl($pagina_nombre))
    {
        $infequ_tipo = "tipo de equipo:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $infequ_tipo; // Tipo de Equipo:
        $infequ_areaClimatiza = "area que climatiza:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $infequ_areaClimatiza; // Área que Climatiza:
        $infequ_id_areaClimatiza = ltl($question) == 'arqc:-i' ? (isset($values[0]) ? $values[0] : '') : $infequ_id_areaClimatiza;
        $infequ_id_tipoEquipo = ltl($question) == 'arqc:-i_' ? (isset($values[0]) ? $values[0] : '') : $infequ_id_tipoEquipo;
        $infequ_areaNombre = "arqc:-n" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $infequ_areaNombre; // Nombre del Área:
    } 

    if("correctivo equipo" == ltl($pagina_nombre))
    {
        $parte1_descripcion = "donde:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte1_descripcion;
        $parte1_descripcion_id = "donde:-i" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte1_descripcion_nombre;
        $parte1_descripcion_nombre = "donde-n" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte1_descripcion_id;
        $parte1_parte = "sp: 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte1_parte;
        $parte1_pieza = "sp: 11" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte1_pieza;
        $parte1_tipo = "tipo de trabajo: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte1_tipo;
        $parte1_desTrabajo = "dondtr: 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte1_desTrabajo;
        $parte1_add1 = "aop: 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte1_add1;
    }

    if("parte #2" == ltl($pagina_nombre))
    {
       $parte2_parte = "sp: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte2_parte;
        $parte2_pieza = "sp: 12" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte2_pieza;
        $parte2_tipo = "tipo de trabajo: 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte2_tipo;
        $parte2_desTrabajo = "dondtr: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte2_desTrabajo;
        $parte2_add1 = "aop: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte2_add1;
    }

    if("parte #3" == ltl($pagina_nombre))
    {
       $parte3_parte = "sp: 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte3_parte;
        $parte3_pieza = "sp: 13" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte3_pieza;
        $parte3_tipo = "tipo de trabajo: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte3_tipo;
        $parte3_desTrabajo = "dondtr: 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte3_desTrabajo;
        $parte3_add1 = "aop: 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte3_add1;
    }  

    if("parte #4" == ltl($pagina_nombre))
    {
       $parte4_parte = "sp: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte4_parte;
        $parte4_pieza = "sp: 14" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte4_pieza;
        $parte4_tipo = "tipo de trabajo: 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte4_tipo;
        $parte4_desTrabajo = "dondtr: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte4_desTrabajo;
        $parte4_add1 = "aop: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte4_add1;
    }  

    if("parte #5" == ltl($pagina_nombre))
    {
       $parte5_parte = "sp: 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte5_parte;
        $parte5_pieza = "sp: 15" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte5_pieza;
        $parte5_tipo = "tipo de trabajo: 6" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte5_tipo;
        $parte5_desTrabajo = "dondtr: 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte5_desTrabajo;
        $parte5_add1 = "aop: 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte5_add1;
    }  

    if("parte #6" == ltl($pagina_nombre))
    {
       $parte6_parte = "sp: 6" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte6_parte;
        $parte6_pieza = "sp: 16" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte6_pieza;
        $parte6_tipo = "tipo de trabajo: 7" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte6_tipo;
        $parte6_desTrabajo = "dondtr: 6" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte6_desTrabajo;
        $parte6_add1 = "aop: 6" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte6_add1;
    } 

    if("parte #7" == ltl($pagina_nombre))
    {
       $parte7_parte = "sp: 7" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte7_parte;
        $parte7_pieza = "sp: 17" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte7_pieza;
        $parte7_tipo = "tipo de trabajo: 8" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte7_tipo;
        $parte7_desTrabajo = "dondtr: 7" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte7_desTrabajo;
        $parte7_add1 = "aop: 7" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte7_add1;
    } 

    if("parte #8" == ltl($pagina_nombre))
    {
       $parte8_parte = "sp: 8" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte8_parte;
        $parte8_pieza = "sp: 18" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte8_pieza;
        $parte8_tipo = "tipo de trabajo: 9" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte8_tipo;
        $parte8_desTrabajo = "dondtr: 8" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte8_desTrabajo;
        $parte8_add1 = "aop: 8" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte8_add1;
    }

    if("parte #9" == ltl($pagina_nombre))
    {
       $parte9_parte = "sp: 9" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte9_parte;
        $parte9_pieza = "sp: 19" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte9_pieza;
        $parte9_tipo = "tipo de trabajo: 10" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte9_tipo;
        $parte9_desTrabajo = "dondtr: 9" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte9_desTrabajo;
        $parte9_add1 = "aop: 9" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte9_add1;
    }  

    if("parte #10" == ltl($pagina_nombre))
    {
       $parte10_parte = "sp: 10" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte10_parte;
        $parte10_pieza = "sp: 20" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte10_pieza;
        $parte10_tipo = "tipo de trabajo: 11" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte10_tipo;
        $parte10_desTrabajo = "dondtr: 10" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $parte10_desTrabajo;
    }    

    if("materiales utilizados" == ltl($pagina_nombre))
    {
        $item1_selecItem = "sondi: 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item1_selecItem;
        $item1_descripItem = "descripcion item: 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item1_descripItem;
        $item1_unidad = "definir unidad: 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item1_unidad;
        $item1_cantUtilizada = "cu: 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item1_cantUtilizada;
        $item1_observaciones = "observaciones: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item1_observaciones;
        $item1_addItem = "aoi: 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item1_addItem;
    }

    if("item #2" == ltl($pagina_nombre))
    {
        $item2_selecItem = "sondi:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item2_selecItem;
        $item2_descripItem = "descripcion item:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item2_descripItem;
        $item2_unidad = "definir unidad:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item2_unidad;
        $item2_cantUtilizada = "cu:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item2_cantUtilizada;
        $item2_observaciones = "observaciones:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item2_observaciones;
        $item2_addItem = "aoi:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item1_addItem;
    }

    if("item #3" == ltl($pagina_nombre))
    {
        $item3_selecItem = "sondi: 9" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item3_selecItem;
        $item3_descripItem = "descripcion item: 9" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item3_descripItem;
        $item3_unidad = "definir unidad: 9" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item3_unidad;
        $item3_cantUtilizada = "cu: 9" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item3_cantUtilizada;
        $item3_observaciones = "observaciones: 10" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item3_observaciones;
    }

    if("item #4" == ltl($pagina_nombre))
    {
        $item4_selecItem = "sondi: 8" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item4_selecItem;
        $item4_descripItem = "descripcion item: 8" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item4_descripItem;
        $item4_unidad = "definir unidad: 8" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item4_unidad;
        $item4_cantUtilizada = "cu: 8" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item4_cantUtilizada;
        $item4_observaciones = "observaciones: 9" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item4_observaciones;
    }

    if("item #5" == ltl($pagina_nombre))
    {
        $item5_selecItem = "sondi: 7" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item5_selecItem;
        $item5_descripItem = "descripcion item: 7" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item5_descripItem;
        $item5_unidad = "definir unidad: 7" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item5_unidad;
        $item5_cantUtilizada = "cu: 7" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item5_cantUtilizada;
        $item5_observaciones = "observaciones: 8" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item5_observaciones;
    }

    if("item #6" == ltl($pagina_nombre))
    {
        $item6_selecItem = "sondi: 6" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item6_selecItem;
        $item6_descripItem = "descripcion item: 6" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item6_descripItem;
        $item6_unidad = "definir unidad: 6" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item6_unidad;
        $item6_cantUtilizada = "cu: 6" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item6_cantUtilizada;
        $item6_observaciones = "observaciones: 7" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item6_observaciones;
    }

    if("item #7" == ltl($pagina_nombre))
    {
        $item7_selecItem = "sondi: 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item7_selecItem;
        $item7_descripItem = "descripcion item: 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item7_descripItem;
        $item7_unidad = "definir unidad: 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item7_unidad;
        $item7_cantUtilizada = "cu: 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item7_cantUtilizada;
        $item7_observaciones = "observaciones: 6" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item7_observaciones;
    }

    if("item #8" == ltl($pagina_nombre))
    {
        $item8_selecItem = "sondi: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item8_selecItem;
        $item8_descripItem = "descripcion item: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item8_descripItem;
        $item8_unidad = "definir unidad: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item8_unidad;
        $item8_cantUtilizada = "cu: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item8_cantUtilizada;
        $item8_observaciones = "observaciones: 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item8_observaciones;
    }

    if("item #9" == ltl($pagina_nombre))
    {
        $item9_selecItem = "sondi: 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item9_selecItem;
        $item9_descripItem = "descripcion item: 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item9_descripItem;
        $item9_unidad = "definir unidad: 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item9_unidad;
        $item9_cantUtilizada = "cu: 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item9_cantUtilizada;
        $item9_observaciones = "observaciones: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item9_observaciones;
    }

    if("item #10" == ltl($pagina_nombre))
    {
        $item10_selecItem = "sondi: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item10_selecItem;
        $item10_descripItem = "descripcion item: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item10_descripItem;
        $item10_unidad = "definir unidad: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item10_unidad;
        $item10_cantUtilizada = "cu: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item10_cantUtilizada;
        $item10_observaciones = "observaciones: 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item10_observaciones;
    }

    if("observaciones generales y firmas" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $obsgen_nombreResponsable = "ndrqset:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $obsgen_nombreResponsable; // Nombre del responsable 
                $obsgen_cargo = "cargo:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $obsgen_cargo; // Cargo:
                $obsgen_comentarios = "cg:"? (isset($values[0]) ? $values[0] : '') : $obsgen_comentarios; // Comentarios Generales
                $obsgen_observaciones = "og:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $obsgen_observaciones; // Observaciones Generales:
                $obsgen_firma = "fdcor" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $obsgen_firma; // Firma del Cliente o Responsable
                $obsgen_envioCopia = "eucdefasc:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $obsgen_envioCopia; // Enviar una copia al siguiente correo:
            }

    }

    die();

    // MySQL
    echo ($sql);
    $sql = "INSERT INTO cotizaciones SET 
        id_cliente = '".$infgen_id_cliente."', 
        id_sucursal = ' ".$infgen_id_sucursal."' , 
        status = 0, 
        fecha_status = CURRENT_DATE,
        id_usuario = {$id_usuario},
        fecha_create = CURRENT_TIMESTAMP";
    $last_cot = 0;
    // D($sql);    
    if($mysqli->query($sql)== TRUE){
        $last_cot = $mysqli->insert_id;
        $sql_cotizacion = "INSERT INTO cotizaciones_detalle SET id_cotizacion = '".$last_cot."',
         tipo_trabajo = 2 , des_trabajo = 'INSTALACIÓN'";
        $mysqli->query($sql_cotizacion);
    } else {
    echo "Error: " . $last_cot . "<br>" . $mysqli->error;
    }
    
    $sql_detalle = "INSERT INTO orden_trabajo SET id_cotizacion = '".$last_cot."'";

     $last_id = 0;
    if($mysqli->query($sql_detalle)== TRUE){
        $last_id = $mysqli->insert_id;
        //echo "Ultimo ID insertado " . $last_id;
    } else {
    echo "Error: " . $sql_detalle . "<br>" . $mysqli->error;
    }

    // /*----------  Equipos  ----------*/
    $sql_equipos = "INSERT INTO orden_trabajo_detalle SET id_orden = '".$last_id."', id_area = '".$infequ_id_areaClimatiza."' ,tipo_equipo = '".$infequ_id_tipoEquipo ."' ,nombre_area= '".$infequ_areaNombre."', area = '".$infequ_areaClimatiza."',tipo_trabajo = '".$infequ_tipo."'";
    // D($sql_equipos);
    if ($mysqli->query($sql_equipos) === TRUE) {
    echo "New record created successfully";
    } 
    /*Partes Equipos*/
    $sql_parte1 ="INSERT INTO correctivo_equipo SET 
    id_orden = '".$last_id."', 
    descripcion = '".$parte1_descripcion."',
    parte ='".$parte1_parte."', 
    pieza ='".$parte1_pieza."', 
    tipo_trabajo = '".$parte1_tipo."', 
    descrip_trabajo = '".$parte1_desTrabajo."'";

    if ($mysqli->query($sql_parte1) === TRUE) {
        echo "New record created successfully";
        }  

    $sql_parte2 ="INSERT INTO correctivo_equipo SET 
    id_orden = '".$last_id."', 
    parte ='".$parte2_parte."', 
    pieza ='".$parte2_pieza."', 
    tipo_trabajo = '".$parte2_tipo."', 
    descrip_trabajo = '".$parte2_desTrabajo."'";

    if ($mysqli->query($sql_parte2) === TRUE) {
        echo "New record created successfully";
        }  

    $sql_parte3 ="INSERT INTO correctivo_equipo SET 
    id_orden = '".$last_id."', 
    parte ='".$parte3_parte."', 
    pieza ='".$parte3_pieza."', 
    tipo_trabajo = '".$parte3_tipo."', 
    descrip_trabajo = '".$parte3_desTrabajo."'";

    if ($mysqli->query($sql_parte3) === TRUE) {
        echo "New record created successfully";
        }  

    $sql_parte3 ="INSERT INTO correctivo_equipo SET 
    id_orden = '".$last_id."', 
    parte ='".$parte3_parte."', 
    pieza ='".$parte3_pieza."', 
    tipo_trabajo = '".$parte3_tipo."', 
    descrip_trabajo = '".$parte3_desTrabajo."'";

    if ($mysqli->query($sql_parte3) === TRUE) {
        echo "New record created successfully";
        }  

    $sql_parte4 ="INSERT INTO correctivo_equipo SET 
    id_orden = '".$last_id."', 
    parte ='".$parte4_parte."', 
    pieza ='".$parte4_pieza."', 
    tipo_trabajo = '".$parte4_tipo."', 
    descrip_trabajo = '".$parte4_desTrabajo."'";

    if ($mysqli->query($sql_parte4) === TRUE) {
        echo "New record created successfully";
        }  

    $sql_parte5 ="INSERT INTO correctivo_equipo SET 
    id_orden = '".$last_id."', 
    parte ='".$parte5_parte."', 
    pieza ='".$parte5_pieza."', 
    tipo_trabajo = '".$parte5_tipo."', 
    descrip_trabajo = '".$parte5_desTrabajo."'";

    if ($mysqli->query($sql_parte5) === TRUE) {
        echo "New record created successfully";
        } 

    $sql_parte6 ="INSERT INTO correctivo_equipo SET 
    id_orden = '".$last_id."', 
    parte ='".$parte6_parte."', 
    pieza ='".$parte6_pieza."', 
    tipo_trabajo = '".$parte6_tipo."', 
    descrip_trabajo = '".$parte6_desTrabajo."'";

    if ($mysqli->query($sql_parte6) === TRUE) {
        echo "New record created successfully";
        }

    $sql_parte7 ="INSERT INTO correctivo_equipo SET 
    id_orden = '".$last_id."', 
    parte ='".$parte7_parte."', 
    pieza ='".$parte7_pieza."', 
    tipo_trabajo = '".$parte7_tipo."', 
    descrip_trabajo = '".$parte7_desTrabajo."'";

    if ($mysqli->query($sql_parte7) === TRUE) {
        echo "New record created successfully";
        }    
    $sql_parte8 ="INSERT INTO correctivo_equipo SET 
    id_orden = '".$last_id."', 
    parte ='".$parte8_parte."', 
    pieza ='".$parte8_pieza."', 
    tipo_trabajo = '".$parte8_tipo."', 
    descrip_trabajo = '".$parte8_desTrabajo."'";

    if ($mysqli->query($sql_parte8) === TRUE) {
        echo "New record created successfully";
        } 

    $sql_parte10 ="INSERT INTO correctivo_equipo SET 
    id_orden = '".$last_id."', 
    parte ='".$parte10_parte."', 
    pieza ='".$parte10_pieza."', 
    tipo_trabajo = '".$parte10_tipo."', 
    descrip_trabajo = '".$parte10_desTrabajo."'";

    if ($mysqli->query($sql_parte10) === TRUE) {
        echo "New record created successfully";
        }  

    /*Items*/
    $sql_item1 ="INSERT INTO orden_trabajo_materiales SET id_orden = '".$last_id."', seleccion = '".$item1_selecItem."',descripcion ='".$item1_descripItem."', unidad ='".$item1_unidad."', cantidad = '".$item1_cantUtilizada."', observaciones = '".$item1_observaciones."'";

    if ($mysqli->query($sql_item1) === TRUE) {
        echo "New record created successfully";
        }  

    $sql_item2 ="INSERT INTO orden_trabajo_materiales SET id_orden = '".$last_id."', seleccion = '".$item2_selecItem."',descripcion ='".$item2_descripItem."', unidad ='".$item2_unidad."', cantidad = '".$item2_cantUtilizada."', observaciones = '".$item2_observaciones."'";

    if ($mysqli->query($sql_item2) === TRUE) {
        echo "New record created successfully";
        }  

    $sql_item3 ="INSERT INTO orden_trabajo_materiales SET id_orden = '".$last_id."', seleccion = '".$item3_selecItem."',descripcion ='".$item3_descripItem."', unidad ='".$item3_unidad."', cantidad = '".$item3_cantUtilizada."', observaciones = '".$item3_observaciones."'";

    if ($mysqli->query($sql_item3) === TRUE) {
        echo "New record created successfully";
        }  

    $sql_item4 ="INSERT INTO orden_trabajo_materiales SET id_orden = '".$last_id."', seleccion = '".$item4_selecItem."',descripcion ='".$item4_descripItem."', unidad ='".$item4_unidad."', cantidad = '".$item4_cantUtilizada."', observaciones = '".$item4_observaciones."'";

    if ($mysqli->query($sql_item4) === TRUE) {
        echo "New record created successfully";
        }  

    $sql_item5 ="INSERT INTO orden_trabajo_materiales SET id_orden = '".$last_id."', seleccion = '".$item5_selecItem."',descripcion ='".$item5_descripItem."', unidad ='".$item5_unidad."', cantidad = '".$item5_cantUtilizada."', observaciones = '".$item5_observaciones."'";

    if ($mysqli->query($sql_item5) === TRUE) {
        echo "New record created successfully";
        }  

    $sql_item6 ="INSERT INTO orden_trabajo_materiales SET id_orden = '".$last_id."', seleccion = '".$item6_selecItem."',descripcion ='".$item6_descripItem."', unidad ='".$item6_unidad."', cantidad = '".$item6_cantUtilizada."', observaciones = '".$item6_observaciones."'";

    if ($mysqli->query($sql_item6) === TRUE) {
        echo "New record created successfully";
        }  

    $sql_item7 ="INSERT INTO orden_trabajo_materiales SET id_orden = '".$last_id."', seleccion = '".$item7_selecItem."',descripcion ='".$item7_descripItem."', unidad ='".$item7_unidad."', cantidad = '".$item7_cantUtilizada."', observaciones = '".$item7_observaciones."'";

    if ($mysqli->query($sql_item7) === TRUE) {
        echo "New record created successfully";
        }  

    $sql_item8 ="INSERT INTO orden_trabajo_materiales SET id_orden = '".$last_id."', seleccion = '".$item8_selecItem."',descripcion ='".$item8_descripItem."', unidad ='".$item8_unidad."', cantidad = '".$item8_cantUtilizada."', observaciones = '".$item8_observaciones."'";

    if ($mysqli->query($sql_item8) === TRUE) {
        echo "New record created successfully";
        }  

    $sql_item9 ="INSERT INTO orden_trabajo_materiales SET id_orden = '".$last_id."', seleccion = '".$item9_selecItem."',descripcion ='".$item9_descripItem."', unidad ='".$item9_unidad."', cantidad = '".$item9_cantUtilizada."', observaciones = '".$item9_observaciones."'";

    if ($mysqli->query($sql_item9) === TRUE) {
        echo "New record created successfully";
        }  

    $sql_item10 ="INSERT INTO orden_trabajo_materiales SET id_orden = '".$last_id."', seleccion = '".$item10_selecItem."',descripcion ='".$item10_descripItem."', unidad ='".$item10_unidad."', cantidad = '".$item10_cantUtilizada."', observaciones = '".$item10_observaciones."'";

    if ($mysqli->query($sql_item10) === TRUE) {
        echo "New record created successfully";
        }  


/*Query del reporte*/
$reporte_correctivo ="INSERT INTO reporte_correctivo SET 
geolocalizacion = '".$infgen_fechaHora."', fechaCorrectivo = '".$infgen_geolocalizacion."',
horaLlegada = '".$infgen_hrllegada."', cliente = '".$infgen_cliente."',
id_cliente = '".$infgen_id_cliente."', tipo_cliente = '".$infgen_tipoCliente."',
sucursal = '".$infgen_sucursal."', id_sucursal = '".$infgen_id_sucursal."',
direccion = '".$infgen_direccion."', refLugar = '".$infgen_refLugar."',
ruta = '".$infgen_ruta."', detalle_ref = '".$infgen_referencia."',
observacion1 = '".$infgen_observaciones."', tipo_trabajo = '".$infgen_tipoTrabajo."',
tipo_equipo = '".$infequ_tipo."', id_tipo_equipo = '".$infequ_id_tipoEquipo."',
area_climatizar = '".$infequ_areaClimatiza."', nombre_area = '".$infequ_areaNombre."',
id_area = '".$infequ_id_areaClimatiza."', des_equipo = '".$parte1_descripcion."', parte1 = '".$parte1_parte."',
pieza1 = '".$parte1_pieza."', tipo_trabajo1 = '".$parte1_tipo."',descripTrabajo1 = '".$parte1_desTrabajo."', 
parte2 = '".$parte2_parte."', pieza2 = '".$parte2_pieza."', tipo_trabajo2 = '".$parte2_tipo."', descripTrabajo2 = '".$parte2_desTrabajo."', 
parte3 = '".$parte3_parte."', pieza3 = '".$parte3_pieza."', tipo_trabajo3 = '".$parte3_tipo."', descripTrabajo3 = '".$parte3_desTrabajo."', 
parte4 = '".$parte4_parte."', pieza4 = '".$parte4_pieza."', tipo_trabajo4 = '".$parte4_tipo."', descripTrabajo4 = '".$parte4_desTrabajo."', 
parte5 = '".$parte5_parte."', pieza5 = '".$parte5_pieza."', tipo_trabajo5 = '".$parte5_tipo."', descripTrabajo5 = '".$parte5_desTrabajo."', 
parte6 = '".$parte6_parte."', pieza6 = '".$parte6_pieza."', tipo_trabajo6 = '".$parte6_tipo."', descripTrabajo6 = '".$parte6_desTrabajo."', 
parte7 = '".$parte7_parte."', pieza7 = '".$parte7_pieza."', tipo_trabajo7 = '".$parte7_tipo."', descripTrabajo7 = '".$parte7_desTrabajo."',  
parte8 = '".$parte8_parte."', pieza8 = '".$parte8_pieza."', tipo_trabajo8 = '".$parte8_tipo."', descripTrabajo8 = '".$parte8_desTrabajo."', 
parte9 = '".$parte9_parte."', pieza9 = '".$parte1_pieza9."', tipo_trabajo9 = '".$parte9_tipo."', descripTrabajo9 = '".$parte9_desTrabajo."',
parte10 = '".$parte10_parte."', pieza10 = '".$parte1_pieza10."', tipo_trabajo10 = '".$parte10_tipo."', descripTrabajo10 = '".$parte10_desTrabajo."', 
selectedItem1 = '".$item1_selecItem."', descriptItem1 = '".$item1_descripItem."', 
unidadItem1 = '".$item1_unidad."', cantidadItem1 = '".$item1_cantUtilizada."', observacItem1 = '".$item1_observaciones."',
selectedItem2 = '".$item2_selecItem."', descriptItem2 = '".$item2_descripItem."', 
unidadItem2 = '".$item2_unidad."', cantidadItem2 = '".$item2_cantUtilizada."', observacItem2 = '".$item2_observaciones."' , 
selectedItem3 = '".$item3_selecItem."', descriptItem3 = '".$item3_descripItem."', 
unidadItem3 = '".$item3_unidad."', cantidadItem3 = '".$item3_cantUtilizada."', observacItem3 = '".$item3_observaciones."',
selectedItem4 = '".$item4_selecItem."', descriptItem4 = '".$item4_descripItem."', 
unidadItem4 = '".$item4_unidad."', cantidadItem4 = '".$item4_cantUtilizada."', observacItem4 = '".$item4_observaciones."', 
selectedItem5 = '".$item5_selecItem."', descriptItem5 = '".$item5_descripItem."', 
unidadItem5 = '".$item5_unidad."', cantidadItem5 = '".$item5_cantUtilizada."', observacItem5 = '".$item5_observaciones."',
selectedItem6 = '".$item6_selecItem."', descriptItem6 = '".$item6_descripItem."', 
unidadItem6 = '".$item6_unidad."', cantidadItem6 = '".$item6_cantUtilizada."', observacItem6 = '".$item6_observaciones."', 
selectedItem7 = '".$item7_selecItem."', descriptItem7 = '".$item7_descripItem."', 
unidadItem7  = '".$item7_unidad."', cantidadItem7    = '".$item7_cantUtilizada."', observacItem7    = '".$item7_observaciones."',
selectedItem8 = '".$item8_selecItem."', descriptItem8 = '".$item8_descripItem."', 
unidadItem8 = '".$item8_unidad."', cantidadItem8 = '".$item8_cantUtilizada."', observacItem8 = '".$item8_observaciones."', 
selectedItem9 = '".$item9_selecItem."', descriptItem9 = '".$item9_descripItem."', 
unidadItem9 = '".$item9_unidad."',cantidadItem9 = '".$item9_cantUtilizada."', observacItem9 = '".$item9_observaciones."', 
selectedItem10 = '".$item10_selecItem."', descriptItem10 = '".$item10_descripItem."', 
unidadItem10 = '".$item10_unidad."', cantidadItem10 = '".$item10_cantUtilizada."', observacItem10 = '".$item10_observaciones."', 
nombreResponsable = '".$obsgen_nombreResponsable."', cargo = '".$obsgen_cargo."', comentarios = '".$obsgen_comentarios."', 
observaciones = '".$obsgen_observaciones."', firma  = '".$obsgen_firma."', correoCopia = '".$obsgen_envioCopia."'"; 



    if ($mysqli->query($reporte_correctivo) === TRUE) {
        echo "New record created successfully";
        }  

}

function limpiar($String)
{
    $String = str_replace(array('á','à','â','ã','ª','ä'),"a",$String);
    $String = str_replace(array('Á','À','Â','Ã','Ä'),"A",$String);
    $String = str_replace(array('Í','Ì','Î','Ï'),"I",$String);
    $String = str_replace(array('í','ì','î','ï'),"i",$String);
    $String = str_replace(array('é','è','ê','ë'),"e",$String);
    $String = str_replace(array('É','È','Ê','Ë'),"E",$String);
    $String = str_replace(array('ó','ò','ô','õ','ö','º'),"o",$String);
    $String = str_replace(array('Ó','Ò','Ô','Õ','Ö'),"O",$String);
    $String = str_replace(array('ú','ù','û','ü'),"u",$String);
    $String = str_replace(array('Ú','Ù','Û','Ü'),"U",$String);
    $String = str_replace(array('[','^','´','`','¨','~',']'),"",$String);
    $String = str_replace("ç","c",$String);
    $String = str_replace("Ç","C",$String);
    $String = str_replace("ñ","n",$String);
    $String = str_replace("Ñ","N",$String);
    $String = str_replace("Ý","Y",$String);
    $String = str_replace("ý","y",$String);
    
    $String = str_replace("&aacute;","a",$String);
    $String = str_replace("&Aacute;","A",$String);
    $String = str_replace("&eacute;","e",$String);
    $String = str_replace("&Eacute;","E",$String);
    $String = str_replace("&iacute;","i",$String);
    $String = str_replace("&Iacute;","I",$String);
    $String = str_replace("&oacute;","o",$String);
    $String = str_replace("&Oacute;","O",$String);
    $String = str_replace("&uacute;","u",$String);
    $String = str_replace("&Uacute;","U",$String);

    $String = str_replace("\u00C1;","Á",$String);
    $String = str_replace("\u00E1;","á",$String);
    $String = str_replace("\u00C9;","É",$String);
    $String = str_replace("\u00E9;","é",$String);
    $String = str_replace("\u00CD;","Í",$String);
    $String = str_replace("\u00ED;","í",$String);
    $String = str_replace("\u00D3;","Ó",$String);
    $String = str_replace("\u00F3;","ó",$String);
    $String = str_replace("\u00DA;","Ú",$String);
    $String = str_replace("\u00FA;","ú",$String);
    $String = str_replace("\u00DC;","Ü",$String);
    $String = str_replace("\u00FC;","ü",$String);
    $String = str_replace("\u00D1;","Ṅ",$String);
    $String = str_replace("\u00F1;","ñ",$String);

    $String = str_replace("A", "a", $String);
    return $String;
}

function ltl($s)
{
    return limpiar(trim(strtolower($s)));
}