 <?php
header('Content-Type: text/html; charset=utf-8');
ini_set('display_errors',1);
error_reporting(E_ALL);

/** DATABASE */
$mysqli = @new mysqli("localhost", "auditoriasbonita", "u[V(fTIUbcVb", "cegaservices2");

#$mysqli = @new mysqli("localhost", "root", "", "auditoriasbonita");

if (mysqli_connect_errno()) {
    printf("Falló la conexión: %s\n", mysqli_connect_error());
    exit();
}
$mysqli->set_charset("utf8");

/** Directorio de los JSON */
$path = realpath('./json');

$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
foreach($objects as $name => $object){
    if('.' != $object->getFileName() && '..' != $object->getFileName() && $path != $object->getPath()){
        $pos1 = strpos($object->getPath(), "/recibe/json/mantenimiento_");
        if($pos1 !== false){
            $ext = pathinfo($object->getPathName(), PATHINFO_EXTENSION);
            if('json' == $ext || 'jpeg' == $ext || 'jpg' == $ext || 'png' == $ext || 'mp3' == $ext || '3gpp' == $ext){
                switch ($ext) {
                    case 'json':
                        /* READ AND MOVE JSON */
                        $json = json_decode(trim(file_get_contents($object->getPathName())), true);
                        echo '<pre>';
                        //print_r($json);
                        echo '</pre>';
                        //exit();
						process_json($json, $object->getFileName(), $mysqli);
                        exit;
                        #move_json($object->getPathName(), $object->getFileName());
                        break;

                    case 'jpeg':
                    case 'jpg':
                    case 'png':
                        /* MOVE JSON */
                        #move_image($object->getPathName(), $object->getFileName());
                        break;

                    case '3gpp':
                    case 'mp3':
                        /* MOVE AUDIO */
                        #move_audio($object->getPathName(), $object->getFileName());
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
//$mysqli->close();

function move_json($file, $nameFile){
    echo $file;
    echo $nameFile;
    // die();
    if(!rename($file, __DIR__."/../reportes_vistos/json/$nameFile")){
        echo 'error';
    }
}

function move_image($file, $nameFile){
    // die();
    if(!rename($file, __DIR__."/../reportes_vistos/image/$nameFile")){
        echo 'error';
    }
}

function move_audio($file, $nameFile){
    // die();
    if(!rename($file, __DIR__."/../reportes_vistos/audio/$nameFile")){
        echo 'error';
    }
}

function process_json($json, $filename, $mysqli){
    $identifier = $json['identifier'];
    $version = $json['version']. '';
    $zone = $json['zone']. '';
    $referenceNumber = $json['referenceNumber']. '';
    $state = $json['state']. '';
    $deviceSubmitDate = $json['deviceSubmitDate'];
    $deviceSubmitDateDate = $deviceSubmitDate['time']. '';
    $deviceSubmitDateZone = $deviceSubmitDate['zone']. '';
    $fecha_file = str_replace("-", "", explode("T", $deviceSubmitDateDate)[0]);
    $shiftedDeviceSubmitDate = $json['shiftedDeviceSubmitDate'].'';
    $serverReceiveDate = $json['serverReceiveDate'].'';

    $form = $json['form'];
    $form_identifier = $form['identifier'].'';
    $form_versionIdentifier = $form['versionIdentifier'].'';
    $form_name = $form['name'].'';
    $form_version = $form['version'].'';
    $form_formSpaceIdentifier = $form['formSpaceIdentifier'].'';
    $form_formSpaceName = $form['formSpaceName'].'';

    $user = $json['user'];
    $user_identifier = $user['identifier'].'';
    $user_username = $user['username'].'';
    $user_displayName = $user['displayName'].'';

    $geoStamp = $json['geoStamp'];
    $geoStamp_success = $geoStamp['success'].'';
    $geoStamp_captureTimestamp = $geoStamp['captureTimestamp'];
    $geoStamp_captureTimestamp_provided = $geoStamp_captureTimestamp['provided'];
    $geoStamp_captureTimestamp_provided_time = $geoStamp_captureTimestamp_provided['time'].'';;
    $geoStamp_captureTimestamp_provided_zone = $geoStamp_captureTimestamp_provided['zone'].'';;
    $geoStamp_captureTimestamp_shifted = $geoStamp_captureTimestamp['shifted'].'';;
    $geoStamp_errorMessage = $geoStamp['errorMessage'].'';;
    $geoStamp_source = $geoStamp['source'].'';;
    $geoStamp_coordinates = $geoStamp['coordinates'];
    $geoStamp_coordinates_latitude = $geoStamp_coordinates['latitude'].'';;
    $geoStamp_coordinates_longitude = $geoStamp_coordinates['longitude'].'';;
    $geoStamp_coordinates_altitude = $geoStamp_coordinates['altitude'].'';;
    $geoStamp_address = $geoStamp['address'].'';;

    $pages = $json['pages'];

    /*Información General*/
$infgen_fechaHora           ="";
$infgen_geolocalizacion     ="";
$infgen_fechaMantenimiento  ="";
$infgen_horaLlegada         ="";
$infgen_cliente             ="";
$infgen_tipoCliente         ="";
$infgen_direccion           ="";
$infgen_referenciaLugar     ="";
$infgen_ruta                ="";
$infgen_nuevaRefDetalle     ="";
$infgen_observaciones       ="";
$infgen_tipoTrabajo         ="";
$infgen_id_cliente          ="";
$infgen_id_sucursal         ="";

/*Informacion Equipo*/
$infequ_tipo                ="";
$infequ_areaClimatiza       ="";
$infequ_areaNombre          ="";
$infequ_id_tipoEquipo            = "";
$infequ_id_areaClimatiza         = "";

/*Carac. Equipo A*/
$climaA_descripcion         ="";
$climaA_codigo              ="";
$climaA_Ec	                ="";
$climaA_marca               ="";
$climaA_btu                 ="";
$climaA_modelo              ="";
$climaA_serie               ="";
$climaA_prueini               ="";
$climaA_equiop               ="";

/*Mant. Equipo A*/
$mantA_ec = "";
$mantA_liser = "";
$mantA_lifil = "";
$mantA_lifilC = "";
$mantA_licar = "";
$mantA_licarC = "";
$mantA_lilumo = "";
$mantA_lilumoC = "";
$mantA_lipael = "";
$mantA_lipaelC = "";
$mantA_lidre = "";
$mantA_observaciones = "";
$mantA_capfo = "";
$mantA_prufiequi = "";

/*Diag. Equipo A*/
$diagA_ec = "";
$diagA_pnor = "";
$diagA_pnoc = "";
$diagA_observaciones = "";
$diagA_comentarios = "";
$diagA_capfo = "";
$diagA_tiempoT = "";
$diagA_pasema = "";

/*Carac. Equipo B*/
$climaB_descripcion="";
$climaB_codigo="";
$climaB_evapo="";
$climaB_marcaEvapo="";
$climaB_btuEvaporador="";
$climaB_modelo="";
$climaB_serieEvaporador="";
$climaB_conden="";
$climaB_marcaConden="";
$climaB_btuConde="";
$climaB_modeloCondensador="";
$climaB_serieCondesador="";
$climaB_prueini="";
$climaB_equiope="";

/*Mant. Equipo B*/
$mantB_evaporador = "";
$mantB_condensador = "";
$mantB_liser = "";
$mantB_lifil = "";
$mantB_lifilC = "";
$mantB_licar = "";
$mantB_licarC = "";
$mantB_lilumo = "";
$mantB_lilumoC = "";
$mantB_lipael = "";
$mantB_lipaelC = "";
$mantB_lidre = "";
$mantB_observaciones = "";
$mantB_capfo = "";
$mantB_prufiequi = "";

/*Diag. Equipo B*/
$diagB_evaporador = "";
$diagB_pnor = "";
$diagB_pnoc = "";
$diagB_observaciones = "";
$diagB_comentarios = "";
$diagB_capfo = "";
$diagB_tiempoT = "";
$diagB_pasema = "";
$diagB_pnor_con = "";
$diagB_pnoc_con = "";

/*Carac. Equipo Ventilacion*/
$climaV_descripcion="";
$climaV_codigo="";
$climaV_marca="";
$climaV_hpEvaporador="";
$climaV_cfnEvaporador="";
$climaV_serieEvaporador="";
$climaV_modelo="";
$climaV_prueini="";
$climaV_equiope="";

/*Mant. Equipo Ventilacion*/
$mantV_ec = "";
$mantV_lifil = "";
$mantV_lifilC = "";
$mantV_licar = "";
$mantV_licarC = "";
$mantV_lilumo = "";
$mantV_lilumoC = "";
$mantV_lipael = "";
$mantV_lipaelC = "";
$mantV_ajusbanpo = "";
$mantV_ajusbanpoC = "";
$mantV_liluro = "";
$mantV_liluroC = "";
$mantV_observaciones = "";
$mantV_capfo = "";
$mantV_prufiequi = "";
$mantV_prufiequi = "";

/*Diag. Equipo Ventilacion */
$diagV_pnor = "";
$diagV_pnoc = "";
$diagV_observaciones = "";
$diagV_comentarios = "";
$diagV_capfo = "";
$diagV_tiempoT = "";
$diagV_pasema = "";

/*Carac. Equipo Refrigeracion*/
$climaR_descripcion="";
$climaR_codigo="";
$climaR_evaporador="";
$climaR_condensador="";
$climaR_marca="";
$climaR_hpEvaporador="";
$climaR_modelo="";
$climaR_prueini="";
$climaR_equiope="";
$climaR_serieEvaporador ="";
$climaR_descripcion="";
$climaR_codigo="";
$climaR_marcaEvaporador="";
$climaR_capacidadEvaporador="";
$climaR_modeloEvaporador="";
$climaR_serieEvaporador="";
$climaR_marcaCondensador="";
$climaR_capacidadCondensador="";
$climaR_modeloCondensador="";
$climaR_serieCondensador="";

/*Mant. Equipo Refrigeracion*/
$mantR_evaporador = "";
$mantR_condensador = "";
$mantR_liser = "";
$mantR_lifil = "";
$mantR_lifilC = "";
$mantR_licar = "";
$mantR_licarC = "";
$mantR_lilumo = "";
$mantR_lilumoC = "";
$mantR_lipael = "";
$mantR_lipaelC = "";
$mantR_lidre = "";
$mantR_observaciones = "";
$mantR_capfo = "";
$mantR_prufiequi = "";

/*Diag. Equipo Refrigeracion */
$diagR_condensador = "";
$diagR_evaporador = "";
$diagR_pnor = "";
$diagR_pnoc = "";
$diagR_observaciones = "";
$diagR_comentarios = "";
$diagR_capfo = "";
$diagR_tiempoT = "";
$diagR_pasema = "";
$diagR_pnor="";
$diagR_pnoc="";
$diagR_pnor_con="";
$diagR_pnoc_con="";
$diagR_observaciones="";
$diagR_comentarios="";
$diagR_capfo="";
$diagR_tiempoT="";

/*Materiales Utilizados*/
$mauti_item1="";
$mauti_selec="";
$mauti_descripcion="";
$mauti_definiUnidad="";
$mauti_cantidadUti="";
$mauti_agregarItem="";
$mauti_observaciones="";

/*Item #2*/
$item2_selec="";
$item2_descripcion="";
$item2_definiUnidad="";
$item2_cantidadUti="";
$item2_observaciones="";
$item2_agregarItem="";

/*Item #3*/
$item3_selec="";
$item3_descripcion="";
$item3_definiUnidad="";
$item3_cantidadUti="";
$item3_observaciones="";
$item3_agregarItem="";

/*Item #4*/
$item4_selec="";
$item4_descripcion="";
$item4_definiUnidad="";
$item4_cantidadUti="";
$item4_observaciones="";
$item4_agregarItem="";

/*Item #5*/
$item5_selec="";
$item5_descripcion="";
$item5_definiUnidad="";
$item5_cantidadUti="";
$item5_observaciones="";
$item5_agregarItem="";

/*Item #6*/
$item6_selec="";
$item6_descripcion="";
$item6_definiUnidad="";
$item6_cantidadUti="";
$item6_observaciones="";
$item6_agregarItem="";

/*Item #7*/
$item7_selec="";
$item7_descripcion="";
$item7_definiUnidad="";
$item7_cantidadUti="";
$item7_observaciones="";
$item7_agregarItem="";

/*Item #8*/
$item8_selec="";
$item8_descripcion="";
$item8_definiUnidad="";
$item8_cantidadUti="";
$item8_observaciones="";
$item8_agregarItem="";

/*Item #9*/
$item9_selec="";
$item9_descripcion="";
$item9_definiUnidad="";
$item9_cantidadUti="";
$item9_observaciones="";
$item9_agregarItem="";

/*Item #10*/
$item10_selec="";
$item10_descripcion="";
$item10_definiUnidad="";
$item10_cantidadUti="";
$item10_observaciones="";
$item10_agregarItem="";

/*Pruebas Finales Equipo (Climatización)*/
$puebasC_EnciendeFunciona="";
$puebasC_EnciendeFuncionaC="";
$puebasC_Arranque="";
$pruebasC_drenaje="";
$pruebasC_drenajeC="";
$pruebasC_observaciones="";
$pruebasC_observacionesPasar="";

/*Pruebas Finales Equipo (Ventilación)*/
$puebasV_EnciendeFunciona="";
$puebasV_EnciendeFuncionaC="";
$puebasV_Arranque="";
$pruebasV_observaciones="";
$pruebasV_observacionesPasar="";

/*Pruebas Finales Equipo (Refrigeración)*/
$puebasR_EnciendeFunciona="";
$puebasR_EnciendeFuncionaC="";
$puebasR_Arranque="";
$pruebasR_observaciones="";
$pruebasR_observacionesPasar="";

/*Observaciones Generales y firmas*/
$obsgen_nombreResponsable="";
$obsgen_cargo="";
$obsgen_comentarios="";
$obsgen_observaciones="";
$obsgen_firma="";
$obsgen_envioCopia ="";
    // ciclo
    foreach($pages as $key => $page_data){
        $pagina = $key+1;
        $pagina_nombre = $page_data['name'];
        $sql_muestra_causas = array();
        $answers = $page_data["answers"];

        //echo '<br><br><br>'.ltl($pagina_nombre).'<hr>';
        foreach($answers as $answer){
            $label    = $answer['label'];
            $dataType = $answer['dataType'];
            $question = $answer['question'];
            $values   = $answer['values'];
            $labelita = limpiar(trim(strtolower($label)));

            if("informacion general" == limpiar(trim(strtolower($pagina_nombre))))
            {
				
                $infgen_fechaHora = "fyhds:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $infgen_fechaHora; // Fecha y Hora del Sistema:
                $infgen_geolocalizacion = "geolocalizacion" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $infgen_geolocalizacion; // Geolocalización
                $infgen_fechaMantenimiento = "fdm:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $infgen_fechaMantenimiento; // Fecha del mantenimient
                $infgen_horaLlegada = "hora de llegada:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $infgen_horaLlegada; // Hora de Llegada:
                $infgen_cliente = "cliente:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $infgen_cliente; // Cliente:
                $infgen_id_cliente = ltl($question) == "cliente: - id" ? (isset($values[0]) ? $values[0] : '') : $infgen_id_cliente;
                $infgen_sucursal = ltl($question) == "sucursal:" ? (isset($values[0]) ? $values[0] : '') : $infgen_sucursal;
                $infgen_id_sucursal = ltl($question) == "sucursal: - id" ? (isset($values[0]) ? $values[0] : '') : $infgen_id_sucursal;
                $infgen_tipoCliente = "tipo de cliente:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $infgen_tipoCliente; // Tipo de Cliente:
                $infgen_direccion = "direccion:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $infgen_direccion; // Dirección:
                $infgen_referenciaLugar = "rdl:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $infgen_referenciaLugar; // Referencia del Lugar:
                $infgen_ruta = "ruta" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $infgen_ruta; // Ruta
                $infgen_nuevaRefDetalle = "uearpdunr" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $infgen_nuevaRefDetalle; // Utilice esta área para detallar una nueva referencia
                $infgen_observaciones = "observaciones: 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $infgen_observaciones; // Observaciones:
                $infgen_tipoTrabajo = "tipo de trabajo:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $infgen_tipoTrabajo; // Tipo de Trabajo:
            }

            if("informacion equipo" == limpiar(trim(strtolower($pagina_nombre))))
            {
				
                $infequ_tipo = "tipo de equipo:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $infequ_tipo; // Tipo de Equipo:
                $infequ_areaClimatiza = "area que climatiza:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $infequ_areaClimatiza; // Área que Climatiza:
                $infequ_id_tipoEquipo = ltl($question) == 'tipo de equipo: - id' ? (isset($values[0]) ? $values[0] : '') : $infequ_id_tipoEquipo;
                $infequ_id_areaClimatiza = ltl($question) == 'area que climatiza - id' ? (isset($values[0]) ? $values[0] : '') : $infequ_id_areaClimatiza;
                $infequ_areaNombre = "nombre del area:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $infequ_areaNombre; // Nombre del Área:
            }

            if("caracteristicas equipo (climatizacion a)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $climaA_descripcion = "donde: 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaA_descripcion; // Descripción del Equipo:
                $climaA_codigo = "codde: 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaA_codigo; // Código del Equipo:
                $climaA_Ec = "e/c 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaA_Ec; // Marca:
                $climaA_marca = "marca: 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaA_marca; // Marca:
                $climaA_btu = "capacidad (btu): 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaA_btu; // Capacidad (BTU):
                $climaA_modelo = "modelo: 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaA_modelo; // Modelo:
                $climaA_serie = "serie: 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaA_serie; // Serie:
                $climaA_prueini = "pruebas iniciales 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaA_serie; // Pruebas Iniciales
                $climaA_equiop = "Â¿eo? 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaA_serie; // ¿Equipo Operativo?
            }
			
			if("mantenimiento equipo (climatizacion a)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $mantA_ec = "e/c 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantA_ec; // Evaporador / Condensador
                $mantA_liser = "ldsin: 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantA_liser; // Limpieza de Serpertín:
                $mantA_lifil = "ldf: 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantA_lifil; // Limpieza de Filtros:
                $mantA_lifilC = "ldf:c 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantA_lifilC; // Limpieza de Filtros: Comentarios
                $mantA_licar = "ldc: 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantA_licar; // Limpieza de Carcasa:
                $mantA_licarC = "ldc:c 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantA_licarC; // Limpieza de Carcasa: Comentarios
                $mantA_lilumo = "lylondm: 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantA_lilumo; // Limpieza y Lubricación de Motores:
                $mantA_lilumoC = "lylondm:c 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantA_lilumoC; // Limpieza y Lubricación de Motores: Comentarios
                $mantA_lipael = "ldpeec: 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantA_lipael; // Limpieza de Parte Eléctrica:
                $mantA_lipaelC = "ldpeec:c 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantA_lipaelC; // Limpieza de Parte Eléctrica: Comentarios
                $mantA_lidre = "ldd: 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantA_lidre; // Limpieza de Drenaje:
                $mantA_observaciones = "observaciones: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantA_observaciones; // Observaciones:
                $mantA_capfo = "captura de fotos 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantA_capfo; // Captura de Fotos
                $mantA_prufiequi = "pfde: 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantA_prufiequi; // Pruebas finales de Equipo:

            }
			
			if("diagnostico (climatizacion a)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $diagA_ec = "e/c 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $diagA_ec; // Evaporador / Condensador
                $diagA_pnor = "pno(ron): 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $diagA_pnor; // Partes no Operativas (Reparación):
                $diagA_pnoc = "pno(c): 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $diagA_pnoc; // Partes no Operativas (Reparación):
                $diagA_observaciones = "observaciones: 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $diagA_observaciones; // Observaciones:
                $diagA_comentarios = "comentarios: 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $diagA_comentarios; // Comentarios:
                $diagA_capfo = "captura de fotos 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $diagA_capfo; // Captura de Fotos
                $diagA_tiempoT = "tdt: 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $diagA_tiempoT; // Tiempo de Trabajo:
                $diagA_pasema = "pasondm: 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $diagA_pasema; // Pasar a Selección de Materiales:
                 
            }
			
            if("caracteristicas equipo (climatizacion b)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $climaB_descripcion = "donde: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaB_descripcion; // Descripción del Equipo:
                $climaB_codigo = "codde: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaB_codigo; // Código del Equipo:
                $climaB_evapo = "evaporador 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaB_evapo; // Evaporador:
                $climaB_marcaEvapo = "marca: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaB_marcaEvapo; // MarcaEvaporador:
                $climaB_btuEvaporador = "capacidad (btu): 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaB_btuEvaporador; // Capacidad (BTU):
                $climaB_modelo = "modelo: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaB_modelo; // Modelo Evaporador:
                $climaB_serieEvaporador = "serie: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaB_serieEvaporador; // Serie Evaporador evaporador:
                $climaB_conden = "condensador 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaB_conden; // Condensedor
                $climaB_marcaConden = "marca: 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaB_marcaConden; // Marca Condensedor
                $climaB_btuConde = "capacidad (btu): 3:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaB_btuConde; // Capacidad BTU
                $climaB_modeloCondensador = "modelo: 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaB_modeloCondensador; // Modelo:
                $climaB_serieCondesador = "serie: 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaB_serieCondesador; // Serie:
                $climaB_prueini = "pruebas iniciales 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaB_prueini; // Pruebas Iniciales
                $climaB_equiope = "Â¿eo? 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaB_equiope; // ¿Equipo Operativo?
            }

			if("mantenimiento equipo (climatizacion b)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $mantB_evaporador = "evaporador 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantB_evaporador; // Evaporador / Condensador
                $mantB_condensador = "condensador 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantB_condensador; // Evaporador / Condensador
                $mantB_liser = "ldsin: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantB_liser; // Limpieza de Serpertín:
                $mantB_lifil = "ldf: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantB_lifil; // Limpieza de Filtros:
                $mantB_lifilC = "ldf:c 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantB_lifilC; // Limpieza de Filtros: Comentarios
                $mantB_licar = "ldc: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantB_licar; // Limpieza de Carcasa:
                $mantB_licarC = "ldc:c 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantB_licarC; // Limpieza de Carcasa: Comentarios
                $mantB_lilumo = "lylondm: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantB_lilumo; // Limpieza y Lubricación de Motores:
                $mantB_lilumoC = "lylondm:c 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantB_lilumoC; // Limpieza y Lubricación de Motores: Comentarios
                $mantB_lipael = "ldpeec: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantB_lipael; // Limpieza de Parte Eléctrica:
                $mantB_lipaelC = "ldpeec:c 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantB_lipaelC; // Limpieza de Parte Eléctrica: Comentarios
                $mantB_lidre = "ldd: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantB_lidre; // Limpieza de Drenaje:
                $mantB_observaciones = "observaciones: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantB_observaciones; // Observaciones:
                $mantB_capfo = "observaciones: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantB_capfo; // Captura de Fotos
                $mantB_prufiequi = "pfde: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantB_prufiequi; // Pruebas finales de Equipo:

            }
			
			if("diagnostico (climatizacion b)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $diagB_evaporador = "evaporador 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $diagB_evaporador; // Evaporador
                $diagB_pnor = "pno(ron): 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $diagB_pnor; // Partes no Operativas (Reparación):
                $diagB_pnoc = "pno(c): 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $diagB_pnoc; // Partes no Operativas (Reparación):
                $diagB_observaciones = "observaciones: 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $diagB_observaciones; // Observaciones:
                $diagB_comentarios = "comentarios: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $diagB_comentarios; // Comentarios:
                $diagB_capfo = "captura de fotos 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $diagB_capfo; // Captura de Fotos
                $diagB_tiempoT = "tdt: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $diagB_tiempoT; // Tiempo de Trabajo:
                $diagB_pasema = "pasondm: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $diagB_pasema; // Pasar a Selección de Materiales:
                 
            }
			
           if("caracteristicas equipo (ventilacion)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $climaV_descripcion = "donde: 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaV_descripcion; // Descripción del Equipo:
                $climaV_codigo = "codde: 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaV_codigo; // Código del Equipo:
                $climaV_marca = "marca: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaV_marca; // Evaporador:
                $climaV_hpEvaporador = "capacidad (hp): 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaV_hpEvaporador; // MarcaEvaporador:
                $climaV_cfnEvaporador = "capacidad (cfn):" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaV_cfnEvaporador; // Capacidad (BTU):
                $climaV_modelo = "modelo: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaV_modelo; // Modelo Evaporador:
                $climaV_serieEvaporador = "serie: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaV_serieEvaporador; // Serie Evaporador evaporador:
                $climaV_prueini = "pruebas iniciales 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaV_prueini; // Pruebas Iniciales
                $climaV_equiope = "Â¿eo? 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaV_equiope; // ¿Equipo Operativo?
            }

			if("mantenimiento equipo (ventilacion)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $mantV_ec = "e/c 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantV_ec; // Evaporador / Condensador
                $mantV_lifil = "ldf: 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantV_lifil; // Limpieza de Filtros:
                $mantV_lifilC = "ldf:c 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantV_lifilC; // Limpieza de Filtros: Comentarios
                $mantV_licar = "ldc: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantV_licar; // Limpieza de Carcasa:
                $mantV_licarC = "ldc:c 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantV_licarC; // Limpieza de Carcasa: Comentarios
                $mantV_lilumo = "lylondm: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantV_lilumo; // Limpieza y Lubricación de Motores:
                $mantV_lilumoC = "lylondm:c 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantV_lilumoC; // Limpieza y Lubricación de Motores: Comentarios
                $mantV_lipael = "ldpeec: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantV_lipael; // Limpieza de Parte Eléctrica:
                $mantV_lipaelC = "ldpeec:c 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantV_lipaelC; // Limpieza de Parte Eléctrica: Comentarios
                $mantV_ajusbanpo = "adbyp:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantV_ajusbanpo; // Limpieza de Drenaje:
                $mantV_ajusbanpoC = "adbyp:c" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantV_ajusbanpoC; // Limpieza de Drenaje:
                $mantV_liluro = "lylondr:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantV_liluro; // Limpieza de Drenaje:
                $mantV_liluroC = "lylondr:c" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantV_liluroC; // Limpieza de Drenaje:
                $mantV_observaciones = "observaciones: 6" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantV_observaciones; // Observaciones:
                $mantV_capfo = "captura de fotos 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantV_capfo; // Captura de Fotos
                $mantV_prufiequi = "pfde: 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantV_prufiequi; // Pruebas finales de Equipo:

            }
			
			if("diagnostico (ventilacion)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                
                $diagV_pnor = "pno(ron): 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $diagV_pnor; // Partes no Operativas (Reparación):
                $diagV_pnoc = "pno(c): 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $diagV_pnoc; // Partes no Operativas (Reparación):
                $diagV_observaciones = "observaciones: 7" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $diagV_observaciones; // Observaciones:
                $diagV_comentarios = "comentarios: 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $diagV_comentarios; // Comentarios:
                $diagV_capfo = "captura de fotos 6" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $diagV_capfo; // Captura de Fotos
                $diagV_tiempoT = "tdt: 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $diagV_tiempoT; // Tiempo de Trabajo:
                $diagV_pasema = "pasondm: 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $diagV_pasema; // Pasar a Selección de Materiales:
                 
            }
			
			if("caracteristicas equipo (refrigeracion)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $climaR_descripcion = "donde: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaR_descripcion; // Descripción del Equipo:
                $climaR_codigo = "codde: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaR_codigo; // Código del Equipo:
                $climaR_evaporador = "evaporador 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaR_evaporador; // Evaporador:
                $climaR_marcaCondensador = "marca: 6" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaR_marcaCondensador; // Evaporador:
                $climaR_marcaEvaporador = "marca: 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaR_marcaEvaporador; // Evaporador:
                $climaR_capacidadEvaporador = "capacidad (hp): 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaR_capacidadEvaporador; // MarcaEvaporador:
                $climaR_capacidadCondensador = "capacidad (hp): 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaR_capacidadCondensador; // MarcaEvaporador:
                $climaR_modeloEvaporador = "modelo: 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaR_modeloEvaporador; // Modelo Evaporador:
                $climaR_modeloCondensador = "modelo: 6" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaR_modeloCondensador; // Modelo Evaporador:
                $climaR_serieEvaporador = "serie: 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaR_serieEvaporador; // Serie Evaporador evaporador:
                $climaR_serieCondensador = "serie: 6" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaR_serieCondensador; // Serie Evaporador evaporador:
                $climaR_condensador = "condensador 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaR_condensador; // Evaporador:
                $climaR_prueini = "pruebas iniciales 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaR_prueini; // Pruebas Iniciales
                $climaR_equiope = "Â¿eo? 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $climaR_equiope; // ¿Equipo Operativo?
            }
			
			if("mantenimiento equipo (refrigeracion)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $mantR_evaporador = "evaporador 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantR_evaporador; // Evaporador / Condensador
                $mantR_lifil = "ldf: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantR_lifil; // Limpieza de Filtros:
                $mantR_lifilC = "ldf:c 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantR_lifilC; // Limpieza de Filtros: Comentarios
                $mantR_licar = "ldc: 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantR_licar; // Limpieza de Carcasa:
                $mantR_licarC = "ldc:c 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantR_licarC; // Limpieza de Carcasa: Comentarios
                $mantR_lilumo = "lylondm: 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantR_lilumo; // Limpieza y Lubricación de Motores:
                $mantR_lilumoC = "lylondm:c 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantR_lilumoC; // Limpieza y Lubricación de Motores: Comentarios
                $mantR_lipael = "ldpeec: 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantR_lipael; // Limpieza de Parte Eléctrica:
                $mantR_lipaelC = "ldpeec:c 5: Comentarios" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantR_lipaelC; // Limpieza de Parte Eléctrica: Comentarios
                $mantR_lidre = "ldd: 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantR_lidre; // Limpieza de Drenaje:
                $mantR_condensador = "condensador 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantR_condensador; // Limpieza de Drenaje:
                $mantR_liser = "ldsin: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantR_liser; // Limpieza de Drenaje:
                $mantR_observaciones = "observaciones: 8" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantR_observaciones; // Observaciones:
                $mantR_capfo = "captura de fotos 7" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantR_capfo; // Captura de Fotos
                $mantR_prufiequi = "pfde: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mantR_prufiequi; // Pruebas finales de Equipo:

            }
			
			if("diagnostico (refrigeracion)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                
                $diagR_condensador = "condensador 6" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $diagR_condensador; // Partes no Operativas (Reparación):
                $diagR_pnor = "pno(ron): 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $diagR_pnor; // Partes no Operativas (Reparación):
                $diagR_evaporador = "evaporador 6" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $diagR_evaporador; // Partes no Operativas (Reparación):
                $diagR_pnoc = "pno(c): 5" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $diagR_pnoc; // Partes no Operativas (Reparación):
                $diagR_observaciones = "observaciones: 9" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $diagR_observaciones; // Observaciones:
                $diagR_comentarios = "comentarios: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $diagR_comentarios; // Comentarios:
                $diagR_capfo = "captura de fotos 8" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $diagR_capfo; // Captura de Fotos
                $diagR_tiempoT = "tdt: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $diagR_tiempoT; // Tiempo de Trabajo:
                $diagR_pasema = "pasondm: 4" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $diagR_pasema; // Pasar a Selección de Materiales:
                 
            }
			
            if("materiales utilizados" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $mauti_item1 = "item #1" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mauti_item1;
                $mauti_selec = "sondi: 1" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mauti_selec;
				$mauti_descripcion = "descripcion item: 1" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mauti_descripcion;
                $mauti_definiUnidad = "definir unidad: 1" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mauti_definiUnidad;
                $mauti_cantidadUti = "cu: 1" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mauti_cantidadUti;
                $mauti_observaciones = "observaciones: 10" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mauti_observaciones;
                $mauti_agregarItem = "aoi: 1" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $mauti_agregarItem;
            }

			if("item #2" == limpiar(trim(strtolower($pagina_nombre))))
            {
				
                $item2_selec = "sondi:" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item2_selec;
                $item2_descripcion = "descripcion item:" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item2_descripcion;
                $item2_definiUnidad = "definir unidad:" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item2_definiUnidad;
                $item2_cantidadUti = "cu:" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item2_cantidadUti;
                $item2_observaciones = "observaciones:" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item2_observaciones;
                $item2_agregarItem = "aoi:" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item2_agregarItem;
            }
			
			if("item #3" == limpiar(trim(strtolower($pagina_nombre))))
            {
				
                $item3_selec = "sondi: 9" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item3_selec;
                $item3_descripcion = "descripcion item: 9" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item3_descripcion;
                $item3_definiUnidad = "definir unidad: 9" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item3_definiUnidad;
                $item3_cantidadUti = "cu: 9" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item3_cantidadUti;
                $item3_observaciones = "observaciones: 18" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item3_observaciones;
                $item3_agregarItem = "aoi: 9" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item3_agregarItem;
            }
			
			if("item #4" == limpiar(trim(strtolower($pagina_nombre))))
            {
				
                $item4_selec = "sondi: 8" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item4_selec;
                $item4_descripcion = "descripcion item: 8" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item4_descripcion;
                $item4_definiUnidad = "definir unidad: 8" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item4_definiUnidad;
                $item4_cantidadUti = "cu: 8" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item4_cantidadUti;
                $item4_observaciones = "observaciones: 17" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item4_observaciones;
                $item4_agregarItem = "aoi: 8" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item4_agregarItem;
            }
			
			if("item #5" == limpiar(trim(strtolower($pagina_nombre))))
            {
				
                $item5_selec = "sondi: 7" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item5_selec;
                $item5_descripcion = "descripcion item: 7" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item5_descripcion;
                $item5_definiUnidad = "definir unidad: 7" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item5_definiUnidad;
                $item5_cantidadUti = "cu: 7" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item5_cantidadUti;
                $item5_observaciones = "observaciones: 16" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item5_observaciones;
                $item5_agregarItem = "aoi: 7" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item5_agregarItem;
            }
			
			if("item #6" == limpiar(trim(strtolower($pagina_nombre))))
            {
				
                $item6_selec = "sondi: 6" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item6_selec;
                $item6_descripcion = "descripcion item: 6" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item6_descripcion;
                $item6_definiUnidad = "definir unidad: 6" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item6_definiUnidad;
                $item6_cantidadUti = "cu: 6" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item6_cantidadUti;
                $item6_observaciones = "observaciones: 15" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item6_observaciones;
                $item6_agregarItem = "aoi: 6" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item6_agregarItem;
            }
			
			if("item #7" == limpiar(trim(strtolower($pagina_nombre))))
            {
				
                $item7_selec = "sondi: 5" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item7_selec;
                $item7_descripcion = "descripcion item: 5" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item7_descripcion;
                $item7_definiUnidad = "definir unidad: 5" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item7_definiUnidad;
                $item7_cantidadUti = "cu: 5" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item7_cantidadUti;
                $item7_observaciones = "observaciones: 14" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item7_observaciones;
                $item7_agregarItem = "aoi: 5" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item7_agregarItem;
            }
			
			if("item #8" == limpiar(trim(strtolower($pagina_nombre))))
            {
				
                $item8_selec = "sondi: 4" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item8_selec;
                $item8_descripcion = "descripcion item: 4" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item8_descripcion;
                $item8_definiUnidad = "definir unidad: 4" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item8_definiUnidad;
                $item8_cantidadUti = "cu: 4" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item8_cantidadUti;
                $item8_observaciones = "observaciones: 13" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item8_observaciones;
                $item8_agregarItem = "aoi: 4" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item8_agregarItem;
            }
			
			if("item #9" == limpiar(trim(strtolower($pagina_nombre))))
            {
				
                $item9_selec = "sondi: 3" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item9_selec;
                $item9_descripcion = "descripcion item: 3" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item9_descripcion;
                $item9_definiUnidad = "definir unidad: 3" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item9_definiUnidad;
                $item9_cantidadUti = "cu: 3" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item9_cantidadUti;
                $item9_observaciones = "observaciones: 12" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item9_observaciones;
                $item9_agregarItem = "aoi: 3" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item9_agregarItem;
            }
			
			if("item #10" == limpiar(trim(strtolower($pagina_nombre))))
            {
				
                $item10_selec = "sondi: 2" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item10_selec;
                $item10_descripcion = "descripcion item: 2" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item10_descripcion;
                $item10_definiUnidad = "definir unidad: 2" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item10_definiUnidad;
                $item10_cantidadUti = "cu: 2" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item10_cantidadUti;
                $item10_observaciones = "observaciones: 11" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item10_observaciones;
                $item10_agregarItem = "aoi: 2" ==ltl($label) ? (isset($values[0]) ? $values[0] : '') : $item10_agregarItem;
            }

            if("pruebas finales equipo (climatizacion)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $puebasC_EnciendeFunciona = "Â¿eeeyf? 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $puebasC_EnciendeFunciona; 
                $puebasC_EnciendeFuncionaC = "Â¿eeeyf?c 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $puebasC_EnciendeFuncionaC; 
                $puebasC_Arranque = "pdayf: 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $puebasC_Arranque; 
                $pruebasC_drenaje = "Â¿srolpdd?" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $pruebasC_drenaje;
                $pruebasC_drenajeC = "Â¿srolpdd?c" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $pruebasC_drenajeC;
                $pruebasC_observaciones = "ogde: 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $pruebasC_observaciones;
                $pruebasC_observacionesPasar = "paog: 1" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $pruebasC_observacionesPasar; 
            }
			
			if("pruebas finales equipo (ventilacion)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $puebasV_EnciendeFunciona = "Â¿eeeyf? 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $puebasV_EnciendeFunciona; 
                $puebasV_EnciendeFuncionaC = "Â¿eeeyf?c 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $puebasV_EnciendeFuncionaC; 
                $puebasV_Arranque = "pdayf: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $puebasV_Arranque; 
                $pruebasV_observaciones = "ogde: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $pruebasV_observaciones;
                $pruebasV_observacionesPasar = "paog: 2" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $pruebasV_observacionesPasar; 
            }

			if("pruebas finales equipo (refrigeracion)" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $puebasR_EnciendeFunciona = "Â¿eeeyf? 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $puebasR_EnciendeFunciona; 
                $puebasR_EnciendeFuncionaC = "Â¿eeeyf?c 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $puebasR_EnciendeFuncionaC; 
                $puebasR_Arranque = "pdayf: 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $puebasR_Arranque; 
                $pruebasR_observaciones = "ogde: 3:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $pruebasR_observaciones;
                $pruebasR_observacionesPasar = "paog: 3" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $pruebasR_observacionesPasar; 
            }

            if("observaciones generales y firmas" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $obsgen_nombreResponsable = "ndrqset:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $obsgen_nombreResponsable; // Nombre del responsable 
                $obsgen_cargo = "cargo:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $obsgen_cargo; // Cargo:
                $obsgen_comentarios = "cg:"? (isset($values[0]) ? $values[0] : '') : $obsgen_comentarios; // Comentarios Generales
                $obsgen_observaciones = "og:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $obsgen_observaciones; // Observaciones Generales:
                $obsgen_firma = "fdcor" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $obsgen_firma; // Firma del Cliente o Responsable
                $obsgen_envioCopia = "eucdefasc:" == ltl($label) ? (isset($values[0]) ? $values[0] : '') : $obsgen_envioCopia; // Enviar una copia al siguiente correo:
            }
        }
        /*
        echo "if(\"". ltl($pagina_nombre) ."\" == ltl(\$pagina_nombre))<br>";
        echo "{<br> <br>";
        echo "}<br> <br>";
        */
    }

    // MySQL
    /*Cotizaciones*/
    $sql = "INSERT INTO cotizaciones SET 
        id_cliente = '".$infgen_id_cliente."', 
        id_sucursal = ' ".$infgen_id_sucursal."' , 
        status = 0, 
        fecha_status = CURRENT_DATE,
        id_usuario = 2,
        fecha_create = CURRENT_TIMESTAMP";
    $last_cot = 0;

    if($mysqli->query($sql)== TRUE){
        $last_cot = $mysqli->insert_id;
        $sql_cotizacion = "INSERT INTO cotizaciones_detalle SET id_cotizacion = '".$last_cot."',
         tipo_trabajo = 3 , des_trabajo = 'INSTALACION'";
         // D($sql_cotizacion); 
        $mysqli->query($sql_cotizacion);
    } else {
    echo "Error: " . $last_cot . "<br>" . $mysqli->error;
    }
    /*Orden de la instalcion*/
    $sql_instalacion = "INSERT INTO orden_trabajo SET id_cotizacion = '".$last_cot."'";

    if($mysqli->query($sql_instalacion)== TRUE){
        $last_id = $mysqli->insert_id;
        //echo "Ultimo ID insertado " . $last_id;
    } else {
    echo "Error: " . $sql_instalacion . "<br>" . $mysqli->error;
    }


    // /*----------  Equipos  ----------*/
    $sql_equipos = "INSERT INTO orden_trabajo_detalle SET id_orden = '".$last_id."',area = '".$infequ_areaClimatiza ."' ,tipo_equipo = '".$infequ_tipo ."' ,nombre_area = '".$infequ_areaNombre."'";
    // D($sql_equipos);
    if ($mysqli->query($sql_equipos) === TRUE) {
    echo "New record created successfully";
    }

    /*Caracteristicas Climatizacion A*/
    $sql_caracteristicasClimA = "INSERT INTO man_caracteristicas_climatizacion_a SET id_orden = '".$last_id."', descripcion = '".$climaA_descripcion."' ,codigo = '".$climaA_codigo."', marca = '".$climaA_marca."', capacidadBTU = '".$climaA_btu."', modelo = '".$climaA_modelo."', serie = '".$climaA_serie."'";

    if ($mysqli->query($sql_caracteristicasClimA) === TRUE) {
    echo "New record created successfully";
    }
    /*Diagnostico Climatizacion A*/
    $sql_diagnosticoClimA = "INSERT INTO man_diagnostico_climatizacion_a SET id_orden = '".$last_id."', reparacion = '".$diagA_pnor."' , cambio = '".$diagA_pnoc."', observaciones = '".$diagA_observaciones."', comentarios = '".$diagA_comentarios."', capturaFotos = '".str_replace(" ","_", $diagA_capfo)."', tiempoTrabajo = '".$diagA_tiempoT."'";

    if ($mysqli->query($sql_diagnosticoClimA) === TRUE) {
        echo "New record created successfully";
        } 

  /*CLimatizacion Caracteristicas B*/
   $sql_caracClimB="INSERT INTO man_caracteristicas_climatizacion_b SET id_orden = '".$last_id."', descripcion= '".$climaB_descripcion."', codigo = '".$climaB_codigo."', evaporador = '".$climaB_evapo."', marca = '".$climaB_marcaEvapo."', capacidadBTU ='".$climaB_btuEvaporador."', modelo = '".$climaB_modelo."', serie = '".$climaB_serieEvaporador."', condensador='".$climaB_conden."',  condensadorMarca = '".$climaB_marcaConden."',condensadorBTU = '".$climaB_btuConde."', condensadorModelo = '".$climaB_modeloCondensador."', condensadorSerie = '".$climaB_serieCondesador."'";
    
    if ($mysqli->query($sql_caracClimB) === TRUE) {
    echo "New record created successfully";
    } 

    /*Diagnostico equipo B*/
    $sql_diagnClimB="INSERT INTO man_diagnostico_climatizacion_b SET id_orden = '".$last_id."', reparacion= '".$diagB_pnor."', cambio = '".$diagB_pnoc."', condensaRep = '".$diagB_pnor_con."', condensaCambio = '".$diagB_pnoc_con."',tiempoTrabajo = '".$diagB_tiempoT."'";
    if ($mysqli->query($sql_diagnClimB) === TRUE) {
        echo "New record created successfully";
        }

     /*Caracteristicas Ventilacion*/
    $sql_caracteristicasventila = "INSERT INTO man_caracteristicas_ventilacion SET id_orden = '".$last_id."', descripcion = '".$climaV_descripcion."' , codigo = '".$climaV_codigo."', marca = '".$climaV_marca."', modelo = '".$climaV_modelo."', serie = '".$climaV_serieEvaporador."', capacidadHP = '".$climaV_hpEvaporador."', capacidadCFN = '".$climaV_cfnEvaporador."'";

    if ($mysqli->query($sql_caracteristicasventila) === TRUE) {
        echo "New record created successfully";
        }
        /*Diagnostico ventilacion*/
    $sql_diagnosticoventilaD = "INSERT INTO man_diagnostico_ventilacion SET id_orden = '".$last_id."', reparacion = '".$diagV_pnor."' , cambio = '".$diagV_pnoc."', observaciones = '".$diagV_observaciones."', comentarios = '".$diagV_comentarios."', capturaFotos = '".str_replace(" ", "_", $diagV_capfo)."', tiempoTrabajo = '".$diagV_tiempoT."'";

    if ($mysqli->query($sql_diagnosticoventilaD) === TRUE) {
        echo "New record created successfully";
        }
        
    /*Caracteristicas Refrigeracion*/
    $sql_caracteristicasRef = "INSERT INTO man_caracteristicas_refrigeracion SET
    id_orden = '".$last_id."',
     descripcion = '".$climaR_descripcion."' ,
    codigo = '".$climaR_codigo."',
    marcaEva = '".$climaR_marcaEvaporador."',
    capacidadEva = '".$climaR_capacidadEvaporador."',
    modeloEva = '".$climaR_modeloEvaporador."',
    serieEva = '".$climaR_serieEvaporador."',
    marcaCond = '".$climaR_marcaCondensador."',
    capacidadCond = '".$climaR_capacidadCondensador."',
    modeloCond = '".$climaR_modeloCondensador."',
    serieCond = '".$climaR_serieCondensador."'";

    if ($mysqli->query($sql_caracteristicasRef) === TRUE) {
        echo "New record created successfully";
        }

    /*Diagnostico Refrigeracion*/
    $sql_diagnosticoRefD = "INSERT INTO man_diagnostico_refrigeracion SET 
    id_orden = '".$last_id."',
    evaporaRep = '".$diagR_pnor."' ,
    evaporacambio = '".$diagR_pnoc."',
    condesaRep = '".$diagR_pnor_con."',
    condesaCambio = '".$diagR_pnoc_con."',
    observaciones = '".$diagR_observaciones."',
    comentarios = '".$diagR_comentarios."',
    fotos = '".str_replace(" ", "_", $diagR_capfo)."',
    tiempoTrabajo = '".$diagR_tiempoT."'";

    if ($mysqli->query($sql_diagnosticoRefD) === TRUE) {
        echo "New record created successfully";
        }    
    
     /*Materiales/items*/
    $sql_item1 ="INSERT INTO orden_trabajo_materiales SET id_orden = '".$last_id."', seleccion = '".$mauti_selec."',descripcion ='".$mauti_descripcion."', unidad ='".$mauti_definiUnidad."', cantidad = '".$mauti_cantidadUti."', observaciones = '".$mauti_observaciones."'";

    if ($mysqli->query($sql_item1) === TRUE) {
        echo "New record created successfully";
        }
		
	//echo $sql_item1;

    $sql_item2 ="INSERT INTO orden_trabajo_materiales SET id_orden = '".$last_id."', seleccion = '".$item2_selec."',descripcion ='".$item2_descripcion."', unidad ='".$item2_definiUnidad."', cantidad = '".$item2_cantidadUti."', observaciones = '".$item2_observaciones."'";

    if ($mysqli->query($sql_item2) === TRUE) {
        echo "New record created successfully";
        }

    $sql_item3 ="INSERT INTO orden_trabajo_materiales SET id_orden = '".$last_id."', seleccion = '".$item3_selec."',descripcion ='".$item3_descripcion."', unidad ='".$item3_definiUnidad."', cantidad = '".$item3_cantidadUti."', observaciones = '".$item3_observaciones."'";

    if ($mysqli->query($sql_item3) === TRUE) {
        echo "New record created successfully";
        }

    $sql_item4 ="INSERT INTO orden_trabajo_materiales SET id_orden = '".$last_id."', seleccion = '".$item4_selec."',descripcion ='".$item4_descripcion."', unidad ='".$item4_definiUnidad."', cantidad = '".$item4_cantidadUti."', observaciones = '".$item4_observaciones."'";

    if ($mysqli->query($sql_item4) === TRUE) {
        echo "New record created successfully";
        }

    $sql_item5 ="INSERT INTO orden_trabajo_materiales SET id_orden = '".$last_id."', seleccion = '".$item5_selec."',descripcion ='".$item5_descripcion."', unidad ='".$item5_definiUnidad."', cantidad = '".$item5_cantidadUti."', observaciones = '".$item5_observaciones."'";

    if ($mysqli->query($sql_item5) === TRUE) {
        echo "New record created successfully";
        }

    $sql_item6 ="INSERT INTO orden_trabajo_materiales SET id_orden = '".$last_id."', seleccion = '".$item6_selec."',descripcion ='".$item6_descripcion."', unidad ='".$item6_definiUnidad."', cantidad = '".$item6_cantidadUti."', observaciones = '".$item6_observaciones."'";

    if ($mysqli->query($sql_item6) === TRUE) {
        echo "New record created successfully";
        }

    $sql_item7 ="INSERT INTO orden_trabajo_materiales SET id_orden = '".$last_id."', seleccion = '".$item7_selec."',descripcion ='".$item7_descripcion."', unidad ='".$item7_definiUnidad."', cantidad = '".$item7_cantidadUti."', observaciones = '".$item7_observaciones."'";

    if ($mysqli->query($sql_item7) === TRUE) {
        echo "New record created successfully";
        }

    $sql_item8 ="INSERT INTO orden_trabajo_materiales SET id_orden = '".$last_id."', seleccion = '".$item8_selec."',descripcion ='".$item8_descripcion."', unidad ='".$item8_definiUnidad."', cantidad = '".$item8_cantidadUti."', observaciones = '".$item8_observaciones."'";

    if ($mysqli->query($sql_item8) === TRUE) {
        echo "New record created successfully";
        }

    $sql_item9 ="INSERT INTO orden_trabajo_materiales SET id_orden = '".$last_id."', seleccion = '".$item9_selec."',descripcion ='".$item9_descripcion."', unidad ='".$item9_definiUnidad."', cantidad = '".$item9_cantidadUti."', observaciones = '".$item9_observaciones."'";

    if ($mysqli->query($sql_item9) === TRUE) {
        echo "New record created successfully";
        }

    $sql_item10 ="INSERT INTO orden_trabajo_materiales SET id_orden = '".$last_id."', seleccion = '".$item10_selec."',descripcion ='".$item10_descripcion."', unidad ='".$item10_definiUnidad."', cantidad = '".$item10_cantidadUti."', observaciones = '".$item10_observaciones."'";

    if ($mysqli->query($sql_item10) === TRUE) {
        echo "New record created successfully";
        }  



$reporte_mantenimiento ="INSERT INTO reportes_mantenimiento SET 
geolocalizacion = '".$infgen_geolocalizacion."', fechaCorrectivo = '".$infgen_fechaMantenimiento."',
horaLlegada = '".$infgen_horaLlegada."', cliente = '".$infgen_cliente."',
id_cliente = '".$infgen_id_cliente."', tipo_cliente = '".$infgen_tipoCliente."',
sucursal = '".$infgen_sucursal."', id_sucursal = '".$infgen_id_sucursal."',
direccion = '".$infgen_direccion."', refLugar = '".$infgen_referenciaLugar."',
ruta = '".$infgen_ruta."', detalle_ref = '".$infgen_nuevaRefDetalle."',
observacion1 = '".$infgen_observaciones."', tipo_trabajo = '".$infgen_tipoTrabajo."',
tipo_equipo = '".$infequ_tipo."', id_tipo_equipo = '".$infequ_id_tipoEquipo."',
area_climatizar = '".$infequ_areaClimatiza."', nombre_area = '".$infequ_areaNombre."',
id_area = '".$infequ_id_areaClimatiza."', climaA_descripcion = '".$climaA_descripcion."', 
climaA_codigo = '".$climaA_codigo."',  climaA_Ec = '".$climaA_Ec."', 
climaA_marca = '".$climaA_marca."', climaA_btu = '".$climaA_btu."', 
climaA_modelo = '".$climaA_modelo."', climaA_serie = '".$climaA_serie."', 
climaA_prueini = '".$climaA_prueini."', climaA_equiop = '".$climaA_equiop."', 
mantA_ec = '".$mantA_ec."',  mantA_liser = '".$mantA_liser."', 
mantA_lifil = '".$mantA_lifil."', mantA_lifilC = '".$mantA_lifilC."', 
mantA_licar = '".$mantA_licar."', mantA_licarC = '".$mantA_licarC."', 
mantA_lilumo = '".$mantA_lilumo."', mantA_lilumoC = '".$mantA_lilumoC."', 
mantA_lipael = '".$mantA_lipael."', mantA_lipaelC = '".$mantA_lipaelC."', 
mantA_lidre = '".$mantA_lidre."', mantA_observaciones = '".$mantA_observaciones."', 
mantA_capfo = '".$mantA_capfo."', mantA_prufiequi = '".$mantA_prufiequi."', 
diagA_ec = '".$diagA_ec."', diagA_pnor = '".$diagA_pnor."', 
diagA_pnoc = '".$diagA_pnoc."', diagA_observaciones = '".$diagA_observaciones."', 
diagA_comentarios = '".$diagA_comentarios."', diagA_capfo = '".$diagA_capfo."', 
diagA_tiempoT = '".$diagA_tiempoT."', 
climaB_descripcion = '".$climaB_descripcion."', climaB_codigo = '".$climaB_codigo."', 
climaB_evapo = '".$climaB_evapo."', climaB_marcaEvapo = '".$climaB_marcaEvapo."', 
climaB_btuEvaporador = '".$climaB_btuEvaporador."', limaB_modelo = '".$climaB_modelo."', 
climaB_serieEvaporador = '".$climaB_serieEvaporador."', climaB_conden = '".$climaB_conden."', 
climaB_marcaConden = '".$climaB_marcaConden."',  climaB_btuConde = '".$climaB_btuConde."', 
climaB_modeloCondensador = '".$climaB_modeloCondensador."', climaB_serieCondesador = '".$climaB_serieCondesador."', 
climaB_prueini = '".$climaB_prueini."', climaB_equiope = '".$climaB_equiope."', mantB_evaporador = '".$mantB_evaporador."', 
mantB_condensador = '".$mantB_condensador."',  mantB_liser = '".$mantB_liser."', 
mantB_lifil = '".$mantB_lifil."',  mantB_lifilC = '".$mantB_lifilC."', 
mantB_licar = '".$mantB_licar."', mantB_licarC = '".$mantB_licarC."', 
mantB_lilumo = '".$mantB_lilumo."', mantB_lilumoC = '".$mantB_lilumoC."', 
mantB_lipael = '".$mantB_lipael."', mantB_lipaelC = '".$mantB_lipaelC."', 
mantB_lidre = '".$mantB_lidre."', mantB_observaciones = '".$mantB_observaciones."', 
mantB_capfo = '".$mantB_capfo."', mantB_prufiequi = '".$mantB_prufiequi."', 
diagB_evaporador = '".$diagB_evaporador."', diagB_pnor = '".$diagB_pnor."', 
diagB_pnoc = '".$diagB_pnoc."', diagB_observaciones = '".$diagB_observaciones."', 
diagB_comentarios = '".$diagB_comentarios."', diagB_capfo = '".$diagB_capfo."', 
diagB_tiempoT = '".$diagB_tiempoT."',  
diagB_pnor_con = '".$diagB_pnor_con."', diagB_pnoc_con = '".$diagB_pnoc_con."', 
climaV_descripcion = '".$climaV_descripcion."', climaV_codigo = '".$climaV_codigo."', 
climaV_marca = '".$climaV_marca."', climaV_hpEvaporador = '".$climaV_hpEvaporador."', 
climaV_cfnEvaporador = '".$climaV_cfnEvaporador."', climaV_serieEvaporador = '".$climaV_serieEvaporador."', 
climaV_modelo = '".$climaV_modelo."', climaV_prueini = '".$climaV_prueini."', 
climaV_equiope = '".$climaV_equiope."', mantV_ec = '".$mantV_ec."', 
mantV_lifil = '".$mantV_lifil."', mantV_lifilC = '".$mantV_lifilC."', 
mantV_licar = '".$mantV_licar."', mantV_licarC = '".$mantV_licarC."', 
mantV_lilumo = '".$mantV_lilumo."', mantV_lilumoC = '".$mantV_lilumoC."', 
mantV_lipael = '".$mantV_lipael."', mantV_lipaelC = '".$mantV_lipaelC."', 
mantV_ajusbanpo = '".$mantV_ajusbanpo."', mantV_ajusbanpoC = '".$mantV_ajusbanpoC."', 
mantV_liluro = '".$mantV_liluro."', mantV_liluroC = '".$mantV_liluroC."', 
mantV_observaciones = '".$mantV_observaciones."', mantV_capfo = '".$mantV_capfo."', 
mantV_prufiequi = '".$mantV_prufiequi."', diagV_pnor = '".$diagV_pnor."', 
diagV_pnoc = '".$diagV_pnoc."', diagV_observaciones = '".$diagV_observaciones."', 
diagV_comentarios = '".$diagV_comentarios."', diagV_capfo = '".$diagV_capfo."', 
diagV_tiempoT = '".$diagV_tiempoT."',  
climaR_descripcion = '".$climaR_descripcion."', climaR_codigo = '".$climaR_codigo."', 
climaR_evaporador = '".$climaR_evaporador."', climaR_condensador = '".$climaR_condensador."', 
climaR_marca = '".$climaR_marca."', climaR_hpEvaporador = '".$climaR_hpEvaporador."', 
climaR_modelo = '".$climaR_modelo."', climaR_prueini = '".$climaR_prueini."', 
climaR_equiope = '".$climaR_equiope."', climaR_serieEvaporador = '".$climaR_serieEvaporador."', 
climaR_marcaEvaporador = '".$climaR_marcaEvaporador."', climaR_capacidadEvaporador = '".$climaR_capacidadEvaporador."', 
climaR_modeloEvaporador = '".$climaR_modeloEvaporador."', climaR_serieEvaporador = '".$climaR_serieEvaporador."', 
climaR_marcaCondensador = '".$climaR_marcaCondensador."', climaR_capacidadCondensador = '".$climaR_capacidadCondensador."', 
climaR_modeloCondensador = '".$climaR_modeloCondensador."', climaR_serieCondensador = '".$climaR_serieCondensador."', 
mantR_evaporador = '".$mantR_evaporador."', mantR_condensador = '".$mantR_condensador."', 
mantR_liser = '".$mantR_liser."', mantR_lifil = '".$mantR_lifil."', 
mantR_lifilC = '".$mantR_lifilC."', mantR_licar = '".$mantR_licar."', 
mantR_licarC = '".$mantR_licarC."', mantR_lilumo = '".$mantR_lilumo."', 
mantR_lilumoC = '".$mantR_lilumoC."', mantR_lipael = '".$mantR_lipael."', 
mantR_lipaelC = '".$mantR_lipaelC."', mantR_lidre = '".$mantR_lidre."', 
mantR_observaciones = '".$mantR_observaciones."', mantR_capfo = '".$mantR_capfo."', 
mantR_prufiequi = '".$mantR_prufiequi."', diagR_condensador = '".$diagR_condensador."', 
diagR_evaporador = '".$diagR_evaporador."', diagR_pnor = '".$diagR_pnor."', 
diagR_pnoc = '".$diagR_pnoc."', diagR_observaciones = '".$diagR_observaciones."', 
diagR_comentarios = '".$diagR_comentarios."', diagR_capfo = '".$diagR_capfo."', 
diagR_tiempoT = '".$diagR_tiempoT."', 
selectedItem1 = '".$mauti_selec."', descriptItem1 = '".$mauti_descripcion."', unidadItem1 = '".$mauti_definiUnidad."', cantidadItem1 = '".$mauti_cantidadUti."', 
selectedItem2 = '".$item2_selec."', descriptItem2 = '".$item2_descripcion."', unidadItem2 = '".$item2_definiUnidad."', cantidadItem2 = '".$item2_cantidadUti."',  
selectedItem3 = '".$item3_selec."', descriptItem3 = '".$item3_descripcion."', unidadItem3 = '".$item3_definiUnidad."', cantidadItem3 = '".$item3_cantidadUti."', 
selectedItem4 = '".$item4_selec."', descriptItem4 = '".$item4_descripcion."', unidadItem4 = '".$item4_definiUnidad."', cantidadItem4 = '".$item4_cantidadUti."',  
selectedItem5 = '".$item5_selec."', descriptItem5 = '".$item5_descripcion."', unidadItem5 = '".$item5_definiUnidad."', cantidadItem5 = '".$item5_cantidadUti."', 
selectedItem6 = '".$item6_selec."', descriptItem6 = '".$item6_descripcion."', unidadItem6 = '".$item6_definiUnidad."', cantidadItem6 = '".$item6_cantidadUti."',  
selectedItem7 = '".$item7_selec."', descriptItem7 = '".$item7_descripcion."', unidadItem7 = '".$item7_definiUnidad."', cantidadItem7 = '".$item7_cantidadUti."', 
selectedItem8 = '".$item8_selec."', descriptItem8 = '".$item8_descripcion."', unidadItem8 = '".$item8_definiUnidad."', cantidadItem8 = '".$item8_cantidadUti."',  
selectedItem9 = '".$item9_selec."', descriptItem9 = '".$item9_descripcion."', unidadItem9 = '".$item9_definiUnidad."', cantidadItem9 = '".$item9_cantidadUti."',
observacItem1 = '".$item1_observaciones."', observacItem2 = '".$item2_observaciones."',
observacItem3 = '".$item3_observaciones."', observacItem4 = '".$item4_observaciones."',
observacItem5 = '".$item5_observaciones."', observacItem6 = '".$item6_observaciones."',
observacItem7 = '".$item7_observaciones."', observacItem8 = '".$item8_observaciones."',
observacItem9 = '".$item9_observaciones."', observacItem10 = '".$item0_observaciones."', 
selectedItem10 = '".$item10_selec."', descriptItem10 = '".$item10_descripcion."', unidadItem10 = '".$item10_definiUnidad."', cantidadItem10 = '".$item10_cantidadUti."', 
nombreResponsable = '".$obsgen_nombreResponsable."', cargo = '".$obsgen_cargo."', comentarios = '".$obsgen_comentarios."', 
observaciones = '".$obsgen_observaciones."', firma  = '".$obsgen_firma."', correoCopia = '".$obsgen_envioCopia."'"; 


if ($mysqli->query($reporte_mantenimiento) === TRUE) {
        echo "New record created successfully";
        } 

function limpiar($String)
{
    $String = str_replace(array('á','à','â','ã','ª','ä'),"a",$String);
    $String = str_replace(array('Á','À','Â','Ã','Ä'),"A",$String);
    $String = str_replace(array('Í','Ì','Î','Ï'),"I",$String);
    $String = str_replace(array('í','ì','î','ï'),"i",$String);
    $String = str_replace(array('é','è','ê','ë'),"e",$String);
    $String = str_replace(array('É','È','Ê','Ë'),"E",$String);
    $String = str_replace(array('ó','ò','ô','õ','ö','º'),"o",$String);
    $String = str_replace(array('Ó','Ò','Ô','Õ','Ö'),"O",$String);
    $String = str_replace(array('ú','ù','û','ü'),"u",$String);
    $String = str_replace(array('Ú','Ù','Û','Ü'),"U",$String);
    $String = str_replace(array('[','^','´','`','¨','~',']'),"",$String);
    $String = str_replace("ç","c",$String);
    $String = str_replace("Ç","C",$String);
    $String = str_replace("ñ","n",$String);
    $String = str_replace("Ñ","N",$String);
    $String = str_replace("Ý","Y",$String);
    $String = str_replace("ý","y",$String);
    
    $String = str_replace("&aacute;","a",$String);
    $String = str_replace("&Aacute;","A",$String);
    $String = str_replace("&eacute;","e",$String);
    $String = str_replace("&Eacute;","E",$String);
    $String = str_replace("&iacute;","i",$String);
    $String = str_replace("&Iacute;","I",$String);
    $String = str_replace("&oacute;","o",$String);
    $String = str_replace("&Oacute;","O",$String);
    $String = str_replace("&uacute;","u",$String);
    $String = str_replace("&Uacute;","U",$String);

    $String = str_replace("\u00C1;","Á",$String);
    $String = str_replace("\u00E1;","á",$String);
    $String = str_replace("\u00C9;","É",$String);
    $String = str_replace("\u00E9;","é",$String);
    $String = str_replace("\u00CD;","Í",$String);
    $String = str_replace("\u00ED;","í",$String);
    $String = str_replace("\u00D3;","Ó",$String);
    $String = str_replace("\u00F3;","ó",$String);
    $String = str_replace("\u00DA;","Ú",$String);
    $String = str_replace("\u00FA;","ú",$String);
    $String = str_replace("\u00DC;","Ü",$String);
    $String = str_replace("\u00FC;","ü",$String);
    $String = str_replace("\u00D1;","Ṅ",$String);
    $String = str_replace("\u00F1;","ñ",$String);

    $String = str_replace("A", "a", $String);
    return $String;
}

function ltl($s)
{
    return limpiar(trim(strtolower($s)));
}