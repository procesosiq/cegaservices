<?php
header('Content-Type: text/html; charset=utf-8');
ini_set('display_errors',1);
error_reporting(E_ALL);

/** DATABASE */
$connect = new stdClass;
$connect->db = @new mysqli("localhost", "auditoriasbonita", "u[V(fTIUbcVb", "cegaservices");
$connect->db2 = @new mysqli("localhost", "auditoriasbonita", "u[V(fTIUbcVb", "cegaservices2");

if (mysqli_connect_errno()) {
    printf("Falló la conexión: %s\n", mysqli_connect_error());
    exit();
}
$connect->db->set_charset("utf8");
$connect->db2->set_charset("utf8");

/** Directorio de los JSON */
$path = realpath('./json');

$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
foreach($objects as $name => $object){
    if('.' != $object->getFileName() && '..' != $object->getFileName() &&
	   "/home/procesosiq/public_html/cegaservices2/recibe/json/revision_instalacion" == $object->getPath()){

		    $pos1 = true;
        if($pos1 !== false){
            $ext = pathinfo($object->getPathName(), PATHINFO_EXTENSION);
            if('json' == $ext || 'jpeg' == $ext || 'jpg' == $ext || 'png' == $ext || 'mp3' == $ext || '3gpp' == $ext || 'pdf' == $ext || 'json1' == $ext || 'mp4' == $ext){
                switch ($ext) {
                    case 'json':
                        $json = json_decode(trim(file_get_contents($object->getPathName())), true);
                        process_json($json, $object->getFileName(), $connect);
                        move_json($object->getPathName(), $object->getFileName());
                        break;
                    case 'jpeg':
                    case 'png':
                    case 'jpg':
						move_image($object->getPathName(), $object->getFileName());
                        break;
                    case 'mp3':
                    case '3gpp':
                    case 'mp4':
						move_audio($object->getPathName(), $object->getFileName());
						break;
					case 'pdf':
					case 'json1':
						$file = $object->getPathName();
						delete_file($file);
						break;
                    default:
                        break;
                }
            }
        }
    }
}



function process_json($json, $filename, $connect){
    $identifier = $json['identifier'];
    // D($identifier);
    $version = $json['version']. '';
    $zone = $json['zone']. '';
    $referenceNumber = $json['referenceNumber']. '';
    $state = $json['state']. '';
    $deviceSubmitDate = $json['deviceSubmitDate'];
    $deviceSubmitDateDate = $deviceSubmitDate['time']. '';
    $deviceSubmitDateZone = $deviceSubmitDate['zone']. '';
    $fecha_file = str_replace("-", "", explode("T", $deviceSubmitDateDate)[0]);
    $shiftedDeviceSubmitDate = $json['shiftedDeviceSubmitDate'].'';
    $serverReceiveDate = $json['serverReceiveDate'].'';
    $serverReceiveDate = substr($serverReceiveDate, 0, 19);
    $serverReceiveDate = str_replace("T", " ", $serverReceiveDate);

    $form = $json['form'];
    $form_identifier = $form['identifier'].'';
    $form_versionIdentifier = $form['versionIdentifier'].'';
    $form_name = $form['name'].'';
    $form_version = $form['version'].'';
    $form_formSpaceIdentifier = $form['formSpaceIdentifier'].'';
    $form_formSpaceName = $form['formSpaceName'].'';

    $user = $json['user'];
    $user_identifier = $user['identifier'].'';
    $user_username = $user['username'].'';
    $user_displayName = $user['displayName'].'';

    $geoStamp = $json['geoStamp'];
    $geoStamp_success = $geoStamp['success'].'';
    $geoStamp_captureTimestamp = $geoStamp['captureTimestamp'];
    $geoStamp_captureTimestamp_provided = $geoStamp_captureTimestamp['provided'];
    $geoStamp_captureTimestamp_provided_time = $geoStamp_captureTimestamp_provided['time'].'';;
    $geoStamp_captureTimestamp_provided_zone = $geoStamp_captureTimestamp_provided['zone'].'';;
    $geoStamp_captureTimestamp_shifted = $geoStamp_captureTimestamp['shifted'].'';;
    $geoStamp_errorMessage = $geoStamp['errorMessage'].'';;
    $geoStamp_source = $geoStamp['source'].'';;
    $geoStamp_coordinates = $geoStamp['coordinates'];
    $geoStamp_coordinates_latitude = $geoStamp_coordinates['latitude'].'';;
    $geoStamp_coordinates_longitude = $geoStamp_coordinates['longitude'].'';;
    $geoStamp_coordinates_altitude = $geoStamp_coordinates['altitude'].'';;
    $geoStamp_address = $geoStamp['address'].'';;

    $pages = $json['pages'];


	/* VARIABLES HECHAS POR NOE */
	/* INFORMACION GENERAL */
	$infGen_idOrden = "";
	$infGen_fecInsta = "";
	$infGen_Geoloca = "";
	$infGen_HoraLlegada = "";
	$infGen_horaPro = "";
	$infGen_TipoRepote = "";
	$nombre_json = $filename;




	$cot_cliente     = "";
    $cot_id_cliente  = "";
	$cot_tipo_cli = "";

	$cot_sucursal     = "";
    $cot_id_sucursal  = "";
	$cot_direccion   = "";
	$cli_refLugar = "";


	$infTrabajo = "";
	$cot_tipoTrabajo = "";
	$infObserva = "";


	/* Información Equipo  */
	$ord_tipoEquipo = "";

	/* Características Equipo (Climatización A) */
	$ord_codEquipo = "";
	$ord_desEquipo = "";
	$ord_areaClimatiza = "";
    $ord_id_areaClimatiza = "";
	$ord_nombreArea = "";
	$ord_evacon = "";
	$equi_marca = "";
	$ord_climaBtu = "";
	$ord_VentilaHP = "";
	$equi_modelo = "";
	$equi_serie = "";
	$ord_evacon2 = "";
	$equi_marca2 = "";
	$ord_climaBtu2 = "";
	$equi_modelo2 = "";
	$equi_serie2 = "";
	$orde_observas = "";
	$orde_pruebaIni = "";
	$equi_opera = "";
	$retvav_cara = "";
	$retval_carac = "";

	/* Instalacion */
	$insta_elec = "";
	$insta_elec_com = "";
	$insta_drena = "";
	$insta_drena_com = "";
	$insta_tuberia = "";
	$insta_tuberia_com = "";
	$insta_eva = "";
	$insta_eva_com = "";
	$insta_conde = "";
	$insta_conde_com = "";
	$insta_termi = "";
	$insta_termi_com = "";
	$insta_drenaEqui = "";
	$insta_drenaEqui_com = "";


	$insta_adecua = "";
	$insta_adecua_com = "";
	$insta_equiposs = "";
	$insta_equiposs_com = "";
	$insta_ductos = "";
	$insta_ductos_com = "";


	$insta_fotos = "";
	$insta_observa = "";
	$retval_insta = "";

	/* Pruebas Finales Equipo (Climatización) */
	$especificaciones = new stdClass;
	$caracteristicas_climatizacion = new stdClass;

	/* Materiales Requeridos  */
	$mat_selitem1 = "";
	$mat_desitem1 = "";
	$mat_uniditem1 = "";
	$mat_cantitem1 = "";
	$mat_observa = "";

	$mat_selitem2 = "";
	$mat_desitem2 = "";
	$mat_uniditem2 = "";
	$mat_cantitem2 = "";
	$mat_observa2 = "";

	$mat_selitem3 = "";
	$mat_desitem3 = "";
	$mat_uniditem3 = "";
	$mat_cantitem3 = "";
	$mat_observa3 = "";

	$mat_selitem4 = "";
	$mat_desitem4 = "";
	$mat_uniditem4 = "";
	$mat_cantitem4 = "";
	$mat_observa4 = "";

	$mat_selitem5 = "";
	$mat_desitem5 = "";
	$mat_uniditem5 = "";
	$mat_cantitem5 = "";
	$mat_observa5 = "";

	$mat_selitem6 = "";
	$mat_desitem6 = "";
	$mat_uniditem6 = "";
	$mat_cantitem6 = "";
	$mat_observa6 = "";

	$mat_selitem7 = "";
	$mat_desitem7 = "";
	$mat_uniditem7 = "";
	$mat_cantitem7 = "";
	$mat_observa7 = "";

	$mat_selitem8 = "";
	$mat_desitem8 = "";
	$mat_uniditem8 = "";
	$mat_cantitem8 = "";
	$mat_observa8 = "";

	$mat_selitem9 = "";
	$mat_desitem9 = "";
	$mat_uniditem9 = "";
	$mat_cantitem9 = "";
	$mat_observa9 = "";

	$mat_selitem10 = "";
	$mat_desitem10 = "";
	$mat_uniditem10 = "";
	$mat_cantitem10 = "";
	$mat_observa10 = "";
	$hora_salida = "";


	/* Observaciones Generales y Firmas */
	$resposable = "";
	$cargo_respon = "";
	$observa_gen_fin = "";
	$img_firma = "";
	$audio_comen = "";
	/* FIN DE LAS VARIABLES */


    foreach($pages as $key => $page_data){
        $pagina = $key+1;
        $pagina_nombre = $page_data['name'];
        $sql_muestra_causas = array();
        $answers = $page_data["answers"];

        #echo ltl($pagina_nombre).'<br>';
        foreach($answers as $answer){
            $label    = $answer['label'];
            $dataType = $answer['dataType'];
            $question = $answer['question'];
            $values   = $answer['values'];
            $labelita = limpiar(trim(strtolower($label)));

			if("informacion general" == ltl($pagina_nombre))
            {
                //INFO GENERAL (FALTA LA HORA DE LLEGADA Y GEOLOCALIZACION) ****************************
				if(ltl($question) == "geolocalizacion"){
					$infGen_Geoloca = $values[0]['coordinates']['latitude'].','.$values[0]['coordinates']['longitude'];

					$salida_fecha = explode("T", $json["shiftedDeviceSubmitDate"])[0];
					$salida_hora = explode("-",explode("T", $json["shiftedDeviceSubmitDate"])[1])[0];
					$hora_salida = $salida_fecha." ".$salida_hora;
				}

				$infGen_idOrden = ltl($question) == "orden de trabajo - id" ? (isset($values[0]) ? $values[0] : '') : $infGen_idOrden;

				if(ltl($question) == "hora de llegada:"){
					$infGen_HoraLlegada = isset($values[0])?$values[0]['provided']['time']:"";
				}

				$infGen_horaPro = ltl($question) == "hora programada:" ? (isset($values[0]) ? $values[0] : '') : $infGen_horaPro;
				$infGen_fecInsta = ltl($question) == "fecha de la revision:" ? (isset($values[0]) ? $values[0] : '') : $infGen_fecInsta;

				//SUCURSAL
				$cot_sucursal = ltl($question) == "sucursal:" ? (isset($values[0]) ? $values[0] : '') : $cot_sucursal;
                $cot_id_sucursal = ltl($question) == "sucursales - id" ? (isset($values[0]) ? $values[0] : '') : $cot_id_sucursal;
				$cot_direccion = ltl($question) == "direccion:" ? (isset($values[0]) ? $values[0] : '') : $cot_direccion;
				$cli_refLugar = ltl($question) == "referencia:" ? (isset($values[0]) ? $values[0] : '') : $cli_refLugar;


				//CLIENTE
				$cot_cliente = ltl($question) == "cliente:" ? (isset($values[0]) ? $values[0] : '') : $cot_cliente;
    			$cot_id_cliente = ltl($question) == "clientes - id" ? (isset($values[0]) ? $values[0] : '') : $cot_id_cliente;
				$cot_tipo_cli = ltl($question) == "clientes - id_tipo_cliente" ? (isset($values[0]) ? $values[0] : '') : $cot_tipo_cli;



				$infTrabajo = ltl($question) == "detalle del trabajo:" ? (isset($values[0]) ? trim($values[0]) : '') : $infTrabajo;
				$infObserva = ltl($question) == "observaciones:" ? (isset($values[0]) ? $values[0] : '') : $infObserva;
				$cot_tipoTrabajo = ltl($question) == "tipo de trabajo:" ? (isset($values[0]) ? $values[0] : '') : $cot_tipoTrabajo;
            }
            #echo limpiar(trim(strtolower($pagina_nombre)))."<br>";
			if("informacion del Area" == limpiar(trim(strtolower($pagina_nombre))))
            {
                $ord_tipoEquipo = ltl($question) == 'tipo de equipo:' ? (isset($values[0]) ? $values[0] : '') : $ord_tipoEquipo;
                if(ltl($question) == 'observaciones:'){
                	$ord_observaciones = (isset($values[0]) ? $values[0] : '');
                }
            }

			if("herramientas de trabajo" == limpiar(trim(strtolower($pagina_nombre))))
            {
                if(ltl($question) == "herramientas necesarias para el trabajo:"){
                	$ord_herramientas = "";
                	foreach($values as $herr){
                		$ord_herramientas .= $herr."|";
                	}
                }
            }            

			/* CARACTERISTICAS DE LOS EQUIPOS */

			if( ("especificaciones del trabajo (climatizacion)" == limpiar(trim(strtolower($pagina_nombre))) && ($ord_tipoEquipo == "CLIMATIZACIÓN A" || $ord_tipoEquipo == "CLIMATIZACIÓN B"))
				||
				("especificaciones del trabajo (ventilacion)" == limpiar(trim(strtolower($pagina_nombre)))  && $ord_tipoEquipo == "VENTILACIÓN")
				||
				("especificaciones del trabajo (refrigeracion)" == limpiar(trim(strtolower($pagina_nombre))) && $ord_tipoEquipo == "REFRIGERACIÓN")
				) {

				if(ltl($question) == 'equipo recomendado'){
					$especificaciones->equipoRecomendado = (isset($values[0]) ? $values[0] : '');
				}
				if(ltl($question) == 'descripcion del equipo:'){
					$especificaciones->descripcionEquipo = (isset($values[0]) ? $values[0] : '');
				}
				if(ltl($question) == 'tiempo aproximado para instalacion:'){
					$especificaciones->tiempoEstimado = (isset($values[0]) ? $values[0]["display"] : '');
				}

				$ord_climaBtu = ltl($question) == 'capacidad (btu):' ? (isset($values[0]) ? $values[0] : '') : $ord_climaBtu;
				$orde_observas = ltl($question) == 'observaciones:' ? (isset($values[0]) ? $values[0] : '') : $orde_observas;
            }

            if( ("caracteristicas Area (climatizacion)" == limpiar(trim(strtolower($pagina_nombre))) && ($ord_tipoEquipo == "CLIMATIZACIÓN A" || $ord_tipoEquipo == "CLIMATIZACIÓN B"))
            	||
            	("caracteristicas Area (ventilacion)" == limpiar(trim(strtolower($pagina_nombre)))  && $ord_tipoEquipo == "VENTILACIÓN")
            	||
            	("caracteristicas Area (refrigeracion)" == limpiar(trim(strtolower($pagina_nombre))) && $ord_tipoEquipo == "REFRIGERACIÓN")
            	){
            	if(ltl($question) == "cantidad de personas por Area:"){
            		$caracteristicas_climatizacion->cantPersonasArea = isset($values[0]) ?  $values[0] : '';
            	}
            	if(ltl($question) == "largo (metros):"){
            		$caracteristicas_climatizacion->largoMetros = isset($values[0]) ? $values[0] : '';
            	}
            	if(ltl($question) == "ancho (metros):"){
            		$caracteristicas_climatizacion->anchoMetros = isset($values[0]) ? $values[0] : '';
            	}
            	if(ltl($question) == "Area total (m2):"){
            		$caracteristicas_climatizacion->areaTotal = isset($values[0]) ? $values[0] : '';
            	}
            	if(ltl($question) == "fotos del Area total"){
            		if(count($values) > 0){
            			$caracteristicas_climatizacion->fotosAreaTotal = "";
            			foreach ($values as $img) {
            				$caracteristicas_climatizacion->fotosAreaTotal .= $referenceNumber.'_'.espacios($img['filename'])."|";
            			}
            		}else{
            			$caracteristicas_climatizacion->fotosAreaTotal = "";
            		}
            	}
            	if(ltl($question) == "fotos de ubicacion de equipos internos"){
            		if(count($values) > 0){
            			$caracteristicas_climatizacion->fotosInterior = "";
            			foreach ($values as $img) {
            				$caracteristicas_climatizacion->fotosInterior .= $referenceNumber.'_'.espacios($img['filename'])."|";
            			}
            		}else{
            			$caracteristicas_climatizacion->fotosInterior = "";
            		}
            	}
            	if(ltl($question) == "fotos de ubicacion de equipos externos"){
            		if(count($values) > 0){
            			$caracteristicas_climatizacion->fotosExterior = "";
            			foreach ($values as $img) {
            				$caracteristicas_climatizacion->fotosExterior .= $referenceNumber.'_'.espacios($img['filename'])."|";
            			}
            		}else{
            			$caracteristicas_climatizacion->fotosExterior = "";
            		}
            	}
            	if(ltl($question) == "observaciones:"){
            		$caracteristicas_climatizacion->observaciones = isset($values[0]) ? $values[0] : '';
            	}
            }
            
            if("informacion del Area" == limpiar(trim(strtolower($pagina_nombre)))){
            	$ord_id_areaClimatiza = ltl($question) == 'Area que climatiza: - id' ? (isset($values[0]) ? $values[0] : '') : $ord_id_areaClimatiza;
				$ord_areaClimatiza = ltl($question) == 'Area que climatiza:' ? (isset($values[0]) ? $values[0] : '') : $ord_areaClimatiza;
    			$ord_nombreArea = ltl($question) == 'nombre del Area:' ? (isset($values[0]) ? $values[0] : '') : $ord_nombreArea;
            }

			if("materiales requeridos" == limpiar(trim(strtolower($pagina_nombre)))){
				$mat_selitem1 = ltl($question) == 'seleccion de item:' ? (isset($values[0]) ? $values[0] : '') : $mat_selitem1;
				$mat_desitem1 = ltl($question) == 'descripcion item:' ? (isset($values[0]) ? $values[0] : '') : $mat_desitem1;
				$mat_uniditem1 = ltl($question) == 'definir unidad:' ? (isset($values[0]) ? $values[0] : '') : $mat_uniditem1;
				$mat_cantitem1 = ltl($question) == 'cantidad requerida:' ? (isset($values[0]) ? $values[0] : '') : $mat_cantitem1;
				$mat_observa = ltl($question) == 'observaciones:' ? (isset($values[0]) ? $values[0] : '') : $mat_observa;
			}

			if("item #2" == limpiar(trim(strtolower($pagina_nombre)))){
				$mat_selitem2 = ltl($question) == 'seleccion de item:' ? (isset($values[0]) ? $values[0] : '') : $mat_selitem2;
				$mat_desitem2 = ltl($question) == 'descripcion item:' ? (isset($values[0]) ? $values[0] : '') : $mat_desitem2;
				$mat_uniditem2 = ltl($question) == 'definir unidad:' ? (isset($values[0]) ? $values[0] : '') : $mat_uniditem2;
				$mat_cantitem2 = ltl($question) == 'cantidad requerida:' ? (isset($values[0]) ? $values[0] : '') : $mat_cantitem2;
				$mat_observa2 = ltl($question) == 'observaciones:' ? (isset($values[0]) ? $values[0] : '') : $mat_observa2;
			}

			if("item #3" == limpiar(trim(strtolower($pagina_nombre)))){
				$mat_selitem3 = ltl($question) == 'seleccion de item:' ? (isset($values[0]) ? $values[0] : '') : $mat_selitem3;
				$mat_desitem3 = ltl($question) == 'descripcion item:' ? (isset($values[0]) ? $values[0] : '') : $mat_desitem3;
				$mat_uniditem3 = ltl($question) == 'definir unidad:' ? (isset($values[0]) ? $values[0] : '') : $mat_uniditem3;
				$mat_cantitem3 = ltl($question) == 'cantidad requerida:' ? (isset($values[0]) ? $values[0] : '') : $mat_cantitem3;
				$mat_observa3 = ltl($question) == 'observaciones:' ? (isset($values[0]) ? $values[0] : '') : $mat_observa3;
			}

			if("item #4" == limpiar(trim(strtolower($pagina_nombre)))){
				$mat_selitem4 = ltl($question) == 'seleccion de item:' ? (isset($values[0]) ? $values[0] : '') : $mat_selitem4;
				$mat_desitem4 = ltl($question) == 'descripcion item:' ? (isset($values[0]) ? $values[0] : '') : $mat_desitem4;
				$mat_uniditem4 = ltl($question) == 'definir unidad:' ? (isset($values[0]) ? $values[0] : '') : $mat_uniditem4;
				$mat_cantitem4 = ltl($question) == 'cantidad requerida:' ? (isset($values[0]) ? $values[0] : '') : $mat_cantitem4;
				$mat_observa4 = ltl($question) == 'observaciones:' ? (isset($values[0]) ? $values[0] : '') : $mat_observa4;
			}

			if("item #5" == limpiar(trim(strtolower($pagina_nombre)))){
				$mat_selitem5 = ltl($question) == 'seleccion de item:' ? (isset($values[0]) ? $values[0] : '') : $mat_selitem5;
				$mat_desitem5 = ltl($question) == 'descripcion item:' ? (isset($values[0]) ? $values[0] : '') : $mat_desitem5;
				$mat_uniditem5 = ltl($question) == 'definir unidad:' ? (isset($values[0]) ? $values[0] : '') : $mat_uniditem5;
				$mat_cantitem5 = ltl($question) == 'cantidad requerida:' ? (isset($values[0]) ? $values[0] : '') : $mat_cantitem5;
				$mat_observa5 = ltl($question) == 'observaciones:' ? (isset($values[0]) ? $values[0] : '') : $mat_observa5;
			}

			if("item #6" == limpiar(trim(strtolower($pagina_nombre)))){
				$mat_selitem6 = ltl($question) == 'seleccion de item:' ? (isset($values[0]) ? $values[0] : '') : $mat_selitem6;
				$mat_desitem6 = ltl($question) == 'descripcion item:' ? (isset($values[0]) ? $values[0] : '') : $mat_desitem6;
				$mat_uniditem6 = ltl($question) == 'definir unidad:' ? (isset($values[0]) ? $values[0] : '') : $mat_uniditem6;
				$mat_cantitem6 = ltl($question) == 'cantidad requerida:' ? (isset($values[0]) ? $values[0] : '') : $mat_cantitem6;
				$mat_observa6 = ltl($question) == 'observaciones:' ? (isset($values[0]) ? $values[0] : '') : $mat_observa6;
			}

			if("item #7" == limpiar(trim(strtolower($pagina_nombre)))){
				$mat_selitem7 = ltl($question) == 'seleccion de item:' ? (isset($values[0]) ? $values[0] : '') : $mat_selitem7;
				$mat_desitem7 = ltl($question) == 'descripcion item:' ? (isset($values[0]) ? $values[0] : '') : $mat_desitem7;
				$mat_uniditem7 = ltl($question) == 'definir unidad:' ? (isset($values[0]) ? $values[0] : '') : $mat_uniditem7;
				$mat_cantitem7 = ltl($question) == 'cantidad requerida:' ? (isset($values[0]) ? $values[0] : '') : $mat_cantitem7;
				$mat_observa7 = ltl($question) == 'observaciones:' ? (isset($values[0]) ? $values[0] : '') : $mat_observa7;
			}

			if("item #8" == limpiar(trim(strtolower($pagina_nombre)))){
				$mat_selitem8 = ltl($question) == 'seleccion de item:' ? (isset($values[0]) ? $values[0] : '') : $mat_selitem8;
				$mat_desitem8 = ltl($question) == 'descripcion item:' ? (isset($values[0]) ? $values[0] : '') : $mat_desitem8;
				$mat_uniditem8 = ltl($question) == 'definir unidad:' ? (isset($values[0]) ? $values[0] : '') : $mat_uniditem8;
				$mat_cantitem8 = ltl($question) == 'cantidad requerida:' ? (isset($values[0]) ? $values[0] : '') : $mat_cantitem8;
				$mat_observa8 = ltl($question) == 'observaciones:' ? (isset($values[0]) ? $values[0] : '') : $mat_observa8;
			}

			if("item #9" == limpiar(trim(strtolower($pagina_nombre)))){
				$mat_selitem9 = ltl($question) == 'seleccion de item:' ? (isset($values[0]) ? $values[0] : '') : $mat_selitem9;
				$mat_desitem9 = ltl($question) == 'descripcion item:' ? (isset($values[0]) ? $values[0] : '') : $mat_desitem9;
				$mat_uniditem9 = ltl($question) == 'definir unidad:' ? (isset($values[0]) ? $values[0] : '') : $mat_uniditem9;
				$mat_cantitem9 = ltl($question) == 'cantidad requerida:' ? (isset($values[0]) ? $values[0] : '') : $mat_cantitem9;
				$mat_observa9 = ltl($question) == 'observaciones:' ? (isset($values[0]) ? $values[0] : '') : $mat_observa9;
			}

			if("item #10" == limpiar(trim(strtolower($pagina_nombre)))){
				$mat_selitem10 = ltl($question) == 'seleccion de item:' ? (isset($values[0]) ? $values[0] : '') : $mat_selitem10;
				$mat_desitem10 = ltl($question) == 'descripcion item:' ? (isset($values[0]) ? $values[0] : '') : $mat_desitem10;
				$mat_uniditem10 = ltl($question) == 'definir unidad:' ? (isset($values[0]) ? $values[0] : '') : $mat_uniditem10;
				$mat_cantitem10 = ltl($question) == 'cantidad requerida:' ? (isset($values[0]) ? $values[0] : '') : $mat_cantitem10;
				$mat_observa10 = ltl($question) == 'observaciones:' ? (isset($values[0]) ? $values[0] : '') : $mat_observa10;
			}

			if("observaciones generales y firmas" == limpiar(trim(strtolower($pagina_nombre)))){
				$resposable = ltl($question) == 'nombre del responsable que supervisa el trabajo:' ? (isset($values[0]) ? $values[0] : '') : $resposable;
				$cargo_respon = ltl($question) == 'cargo:' ? (isset($values[0]) ? $values[0] : '') : $cargo_respon;
				$observa_gen_fin = ltl($question) == 'observaciones generales' ? (isset($values[0]) ? $values[0] : '') : $observa_gen_fin;

				//IMAGEN DE LA FIRMA DIGITAL
				if(ltl($question) == 'firma del cliente o responsable'){
					if(isset($values[0]['filename']))
						$img_firma = $referenceNumber.'_'.$values[0]['filename'];
				}

				//AUDIO DEL COMENTARIO FINAL
				if(ltl($question) == 'comentarios generales'){
					if(isset($values[0]['filename'])){
						$ext = pathinfo($values[0]['filename'], PATHINFO_EXTENSION);
						$audio_comen = $referenceNumber.'_'.$values[0]['filename'];
					}
				}
			}

        }
    }


    // MySQL
    $mysqli = $connect->db2;
    $id_usuario = 2;
    if(isset($user_username) && $user_username != "" && $user_username == "ernesto.polit"){
        $mysqli = $connect->db2;
        $id_usuario = 2;
    }

	if($ord_tipoEquipo == 'CLIMATIZACIÓN A'){
		$infGen_TipoRepote = "REVISION INSTALACION A";
	}
	else if($ord_tipoEquipo == 'CLIMATIZACIÓN B'){
		$infGen_TipoRepote = "REVISION INSTALACION B";
	}
	else if($ord_tipoEquipo == 'VENTILACIÓN'){
		$infGen_TipoRepote = "REVISION VENTILACION";
	}else if($ord_tipoEquipo == "REFRIGERACIÓN"){
		$infGen_TipoRepote = "REVISION REFRIGERACION";
	}

    $sql = "INSERT INTO `reportes_revision_instalacion` SET 

	id_orden			=	'$infGen_idOrden',
	`geolocalizacion`	=	'$infGen_Geoloca',
	`fecha_instalacion`	=	'$infGen_fecInsta',
	`hora_llegada`		=	'$infGen_HoraLlegada',
	hora_salida         =   '{$hora_salida}',
	numero_referencia = '{$referenceNumber}',
	`hora_programada`	=	'$infGen_horaPro',
	`cliente`			=	'$cot_cliente',
	`id_cliente`		=	'$cot_id_cliente',
	`tipo_cliente`		=	'$cot_tipo_cli',
	`sucursal`			=	'$cot_sucursal',
	`id_sucursal`		=	'$cot_id_sucursal',
	`direccion`			=	'$cot_direccion',
	`referencia`		=	'$cli_refLugar',
	`detalle_trabajo`	=	'$infTrabajo',
	tipo_trabajo		=	'$cot_tipoTrabajo',
	`observacion1`		=	'$infObserva',
	`tipo_equipo`		=	'$ord_tipoEquipo',
	`nombre_area`		=	'$ord_nombreArea ',
	`area_clima`		=	'$ord_areaClimatiza',
	`id_area`			=	'$ord_id_areaClimatiza',
	`observa2`			=	'$ord_observaciones',


	equipo_recomendado			= 	'{$especificaciones->equipoRecomendado}',
	`desc_equipo`				= 	'{$especificaciones->descripcionEquipo}',
	`capacidad_btu`				=	'{$ord_climaBtu}',
	tiempo_aprox_instalacion	=	'{$especificaciones->tiempoEstimado}',
	orde_observas = '{$orde_observas}',


	cantidad_personas_area		=	'{$caracteristicas_climatizacion->cantPersonasArea}',
	largo 						=	'{$caracteristicas_climatizacion->largoMetros}',
	ancho 						=	'{$caracteristicas_climatizacion->anchoMetros}',
	area_total					=	'{$caracteristicas_climatizacion->areaTotal}',
	fotos_area_total			=	'{$caracteristicas_climatizacion->fotosAreaTotal}',
	fotos_interior				=	'{$caracteristicas_climatizacion->fotosInterior}',
	fotos_exterior				=	'{$caracteristicas_climatizacion->fotosExterior}',
	observaciones 				=	'{$caracteristicas_climatizacion->observaciones}',

	`responsable`='$resposable',`cargo_responsable`='$cargo_respon',
	img_firma='$img_firma',audio_comentario_gral='$audio_comen',


	`mat_item1_sel`='$mat_selitem1',`mat_item1_des`='$mat_desitem1',`mat_item1_uni`='$mat_uniditem1',`mat_item1_cant`='$mat_cantitem1',`mat_item1_obser`='$mat_observa',
	`mat_item2_sel`='$mat_selitem2',`mat_item2_des`='$mat_desitem2',`mat_item2_uni`='$mat_uniditem2',`mat_item2_cant`='$mat_cantitem2',`mat_item2_obser`='$mat_observa2',
	`mat_item3_sel`='$mat_selitem3',`mat_item3_des`='$mat_desitem3',`mat_item3_uni`='$mat_uniditem3',`mat_item3_cant`='$mat_cantitem3',`mat_item1_obser3`='$mat_observa3',
	`mat_item4_sel`='$mat_selitem4',`mat_item4_des`='$mat_desitem4',`mat_item4_uni`='$mat_uniditem4',`mat_item4_cant`='$mat_cantitem4',`mat_item1_obser4`='$mat_observa4',
	`mat_item5_sel`='$mat_selitem5',`mat_item5_des`='$mat_desitem5',`mat_item5_uni`='$mat_uniditem5',`mat_item5_cant`='$mat_cantitem5',`mat_item1_obser5`='$mat_observa5',
	`mat_item6_sel`='$mat_selitem6',`mat_item6_des`='$mat_desitem6',`mat_item6_uni`='$mat_uniditem6',`mat_item6_cant`='$mat_cantitem6',`mat_item1_obse6`='$mat_observa6',
	`mat_item7_sel`='$mat_selitem7',`mat_item7_des`='$mat_desitem7',`mat_item7_uni`='$mat_uniditem7',`mat_item7_cant`='$mat_cantitem7',`mat_item1_obser7`='$mat_observa7',
	`mat_item8_sel`='$mat_selitem8',`mat_item8_des`='$mat_desitem8',`mat_item8_uni`='$mat_uniditem8',`mat_item8_cant`='$mat_cantitem8',`mat_item1_obser8`='$mat_observa8',
	`mat_item9_sel`='$mat_selitem9',`mat_item9_des`='$mat_desitem9',`mat_item9_uni`='$mat_uniditem9',`mat_item9_cant`='$mat_cantitem9',`mat_item1_obser9`='$mat_observa9',
	`mat_item10_sel`='$mat_selitem10',`mat_item10_des`='$mat_desitem10',`mat_item10_uni`='$mat_uniditem10',`mat_item10_cant`='$mat_cantitem10',`mat_item1_obser10`='$mat_observa10',

	herramientas = '{$ord_herramientas}',
 	tipo_reporte='$infGen_TipoRepote',
 	nombre_json='$nombre_json',
 	order_time ='{$serverReceiveDate}',
 	observa_fin = '{$observa_gen_fin}'";
	if($mysqli->query($sql)== TRUE){
		D($sql);
    } else {
        echo "Error: " . $last_cot . "<br>" . $mysqli->error;
    }
    #D($sql);
	echo "<br>Inserto: ".$nombre_json;
}

/* FUNCIONES QUE SON ECARGADAS DE MOVER LOS ARCHIVOS A LA CARPETA DE LEIDOS*/

function delete_file($file){
	if(!unlink($file)){
		echo "error";
	}
}

function move_json($file, $nameFile){
    if(!rename($file, __DIR__."/../reportes_vistos/revision_instalacion/$nameFile")){
        echo 'error';
    }
}

function move_image($file, $nameFile){
	$nameFile = utf8_encode($nameFile);
    if(!rename($file, __DIR__."/../reportes_vistos/revision_instalacion/".espacios($nameFile))){
        echo 'error';
    }
}

function move_audio($file, $nameFile){
    if(!rename($file, __DIR__."/../reportes_vistos/revision_instalacion/$nameFile")){
        echo 'error';
    }
}

function D($arreglo){
    echo "<pre>";
        print_r($arreglo);
    echo "</pre>";
}

/* FIN DEL BLOQUE DE FUNCIONES QUE SON ENCARGADAS DE MOVER LOS ARCHIVOS A LA CARPETA DE LEIDOS */


function limpiar($String)
{
	#$String = utf8_encode($String);

	$String = str_replace("\u00C1;","Á",$String);
    $String = str_replace("\u00E1;","á",$String);
    $String = str_replace("\u00C9;","É",$String);
    $String = str_replace("\u00E9;","é",$String);
    $String = str_replace("\u00CD;","Í",$String);
    $String = str_replace("\u00ED;","í",$String);
    $String = str_replace("\u00D3;","Ó",$String);
    $String = str_replace("\u00F3;","ó",$String);
    $String = str_replace("\u00DA;","Ú",$String);
    $String = str_replace("\u00FA;","ú",$String);
    $String = str_replace("\u00DC;","Ü",$String);
    $String = str_replace("\u00FC;","ü",$String);
    $String = str_replace("\u00D1;","Ṅ",$String);
    $String = str_replace("\u00F1;","ñ",$String);
	
    $String = str_replace(array('á','à','â','ã','ª','ä'),"a",$String);
    $String = str_replace(array('Á','À','Â','Ã','Ä'),"A",$String);
    $String = str_replace(array('Í','Ì','Î','Ï'),"I",$String);
    $String = str_replace(array('í','ì','î','ï'),"i",$String);
    $String = str_replace(array('é','è','ê','ë'),"e",$String);
    $String = str_replace(array('É','È','Ê','Ë'),"E",$String);
    $String = str_replace(array('ó','ò','ô','õ','ö','º','ó'),"o",$String);
    $String = str_replace(array('Ó','Ò','Ô','Õ','Ö'),"O",$String);
    $String = str_replace(array('ú','ù','û','ü'),"u",$String);
    $String = str_replace(array('Ú','Ù','Û','Ü'),"U",$String);
    $String = str_replace(array('[','^','´','`','¨','~',']'),"",$String);
    $String = str_replace("ç","c",$String);
    $String = str_replace("Ç","C",$String);
    $String = str_replace("ñ","n",$String);
    $String = str_replace("Ñ","N",$String);
    $String = str_replace("Ý","Y",$String);
    $String = str_replace("ý","y",$String);
    
    $String = str_replace("&aacute;","a",$String);
    $String = str_replace("&Aacute;","A",$String);
    $String = str_replace("&eacute;","e",$String);
    $String = str_replace("&Eacute;","E",$String);
    $String = str_replace("&iacute;","i",$String);
    $String = str_replace("&Iacute;","I",$String);
    $String = str_replace("&oacute;","o",$String);
    $String = str_replace("&Oacute;","O",$String);
    $String = str_replace("&uacute;","u",$String);
    $String = str_replace("&Uacute;","U",$String);

    return $String;
}

function ltl($s)
{
    return limpiar(trim(strtolower($s)));
}

function espacios($string){
	return limpiar(str_replace(" ", "_", $string));
}