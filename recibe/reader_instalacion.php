<?php
header('Content-Type: text/html; charset=utf-8');
ini_set('display_errors',1);
error_reporting(E_ALL);

/** DATABASE */
$mysqli = @new mysqli("localhost", "auditoriasbonita", "u[V(fTIUbcVb", "cegaservices2");

#$mysqli = @new mysqli("localhost", "root", "", "auditoriasbonita");

if (mysqli_connect_errno()) {
    printf("Falló la conexión: %s\n", mysqli_connect_error());
    exit();
}
$mysqli->set_charset("utf8");

/** Directorio de los JSON */
$path = realpath('./json');

$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
foreach($objects as $name => $object){
    if('.' != $object->getFileName() && '..' != $object->getFileName() && $path != $object->getPath()){
        $pos1 = strpos($object->getPath(), "/recibe/json/instalacion_");
        if($pos1 !== false){
            $ext = pathinfo($object->getPathName(), PATHINFO_EXTENSION);
            if('json' == $ext || 'jpeg' == $ext || 'jpg' == $ext || 'png' == $ext || 'mp3' == $ext || '3gpp' == $ext){
                switch ($ext) {
                    case 'json':
                        /* READ AND MOVE JSON */
                        $json = json_decode(trim(file_get_contents($object->getPathName())), true);
                        echo '<pre>';
                        //print_r($json);
                        echo '</pre>';
                        process_json($json, $object->getFileName(), $mysqli);
                        move_json($object->getPathName(), $object->getFileName());
                        break;
                    case 'jpeg':
                    case 'jpg':
                    case 'png':
                        /* MOVE JSON */
                        move_json($object->getPathName(), $object->getFileName());
                        break;

                    case '3gpp':
                    case 'mp3':
                        /* MOVE AUDIO */
                        #move_audio($object->getPathName(), $object->getFileName());
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
//$mysqli->close();

function move_json($file, $nameFile){
    if(!rename($file,"/home/procesosiq/public_html/cegaservices2/reportes_vistos/instalacion/".$nameFile)){
        echo 'error';
    }
}

function move_audio($file, $nameFile){
    if(!rename($file,"/home/procesosiq/public_html/cegaservices2/reportes_vistos/instalacion".$nameFile)){
        echo 'error';
    }
}

function process_json($json, $filename, $mysqli){
    $user = $json['user'];
    $user_identifier = $user['identifier'].'';;
    $user_username = $user['username'].'';;
    $referenceNumber = $json["referenceNumber"];
    $pages = $json['pages'];
    $geoStamp = $json["geoStamp"];
    $coordinates = $geoStamp["coordinates"];
    $latitude = $coordinates["latitude"];
    $longitude = $coordinates["longitude"];
    $sql_muestra_causas = array();
    $pagina_nombre = "";
    $labor = "";
    $objectMuestras = new stdClass;
    $objectErros = new stdClass;
    $objectMuestras->json = $filename;
    $objectMuestras->indentifier = $user_identifier;
    $objectMuestras->referenceNumber = $referenceNumber;
    $objectMuestras->auditor = $user["username"];
    $objectMuestras->auditorName = $user["displayName"];
    
    $fecha_salida = explode("T", $json["shiftedDeviceSubmitDate"])[0];
    $hora_salida = explode("-",explode("T", $json["shiftedDeviceSubmitDate"])[1])[0];
    $objectMuestras->hora_envio = $fecha_salida." ".$hora_salida;

    $contador = 0;
    $existZona = 0;
    $existHacienda = 0;
    $existLote = 0;
    
    foreach($pages as $key => $page_data){
        $pagina = $key+1;
        $sql_muestra_causas[] = [];
        if($pagina_nombre != limpiar(trim(strtoupper($page_data['name'])))){
            $pagina_nombre = limpiar(trim(strtoupper($page_data['name'])));
            $contador++;
        }
        $answers = $page_data["answers"];
        foreach($answers as $x => $answer){
            $label    = $answer['label'];
            $dataType = $answer['dataType'];
            $question = $answer['question'];
            $values   = $answer['values'];
            $labelita = limpiar(trim(strtolower($label)));
            
            $objectMuestras->{$pagina_nombre}[$question] = getValueFromDataType($dataType, $values, $referenceNumber);
        }
    }

    DD($objectMuestras);
    
    $id_orden = $objectMuestras->{'INFORMaCIoN GENERaL'}['Orden de Trabajo - id'];
    
    $id_area = 0;
    $id_tipo_equipo = 0;
    if($objectMuestras->{'INFORMaCIoN EQUIPO'}['Área que Climatiza: - id'] == ''){ $id_area = 0;}
    if($objectMuestras->{'INFORMaCIoN EQUIPO'}['Área que Climatiza: - id_tipoequipo'] == ''){ $id_tipo_equipo = 0;}

    $sql = "INSERT INTO orden_trabajo_detalle 
            SET 
            id_orden = '{$id_orden}', 
            id_area = {$id_area},
            id_tipo_equipo = {$id_tipo_equipo},
            nombre_area= '{$objectMuestras->{'INFORMaCIoN EQUIPO'}['Nombre del Área:']}', 
            area = '{$objectMuestras->{'INFORMaCIoN EQUIPO'}['Área que Climatiza:']}',
            tipo_equipo = '{$objectMuestras->{'INFORMaCIoN EQUIPO'}['Tipo de Equipo:']}'";
    DD($sql);
    $mysqli->query($sql);

    $sql = "INSERT INTO reportes_r_instalacion
            SET
            id_orden = {$id_orden},
            geolocalizacion = '{$objectMuestras->{'INFORMaCIoN GENERaL'}['Geolocalización']}',
            fecha_instalacion = '{$objectMuestras->{'INFORMaCIoN GENERaL'}['Fecha de la Instalación']}',
            hora_llegada = '{$objectMuestras->{'INFORMaCIoN GENERaL'}['Hora de Llegada:']['shifted']}',
            hora_programada = '{$objectMuestras->{'INFORMaCIoN GENERaL'}['Hora Programada:']}',
            cliente = '{$objectMuestras->{'INFORMaCIoN GENERaL'}['Cliente - nombre']}',
            id_cliente = '{$objectMuestras->{'INFORMaCIoN GENERaL'}['Cliente - id']}',
            tipo_cliente = '{$objectMuestras->{'INFORMaCIoN GENERaL'}['Cliente - id_tipo_cliente']}',
            sucursal = '{$objectMuestras->{'INFORMaCIoN GENERaL'}['Sucursal']}',
            id_sucursal = '{$objectMuestras->{'INFORMaCIoN GENERaL'}['Sucursal - id']}',
            direccion = '{$objectMuestras->{'INFORMaCIoN GENERaL'}['Dirección Sucursal:']}',
            referencia = '{$objectMuestras->{'INFORMaCIoN GENERaL'}['Referencia:']}',
            tipo_trabajo = '{$objectMuestras->{'INFORMaCIoN GENERaL'}['Tipo de Trabajo:']}',
            detalle_trabajo = '{$objectMuestras->{'INFORMaCIoN GENERaL'}['Detalle del Trabajo:']}',
            observacion1 = '{$objectMuestras->{'INFORMaCIoN GENERaL'}['Observaciones:']}',
            hora_salida = '{$objectMuestras->hora_envio}',
            numero_referencia = '{$objectMuestras->referenceNumber}'";
    DD($sql);
    $mysqli->query($sql);
    $id_reporte = $mysqli->insert_id;

    $sql = "UPDATE reportes_r_instalacion
            SET
            inst_pun_elec = '{$objectMuestras->{'INSTaLaCIoN EQUIPO (CLIMaTIZaCIoN)'}['Puntos Eléctricos:']}',
            inst_pun_elec_com = '{$objectMuestras->{'INSTaLaCIoN EQUIPO (CLIMaTIZaCIoN)'}['Puntos Eléctricos: Comentarios']}',
            inst_drena = '{$objectMuestras->{'INSTaLaCIoN EQUIPO (CLIMaTIZaCIoN)'}['Puntos de Drenaje:']}',
            inst_drena_com = '{$objectMuestras->{'INSTaLaCIoN EQUIPO (CLIMaTIZaCIoN)'}['Puntos de Drenaje: Comentarios']}',
            inst_tube = '{$objectMuestras->{'INSTaLaCIoN EQUIPO (CLIMaTIZaCIoN)'}['Puntos de Tubería de Cobre:']}',
            inst_tube_com = '{$objectMuestras->{'INSTaLaCIoN EQUIPO (CLIMaTIZaCIoN)'}['Puntos de Tubería de Cobre: Comentarios']}',
            inst_eva = '{$objectMuestras->{'INSTaLaCIoN EQUIPO (CLIMaTIZaCIoN)'}['Instalación Evaporador:']}',
            inst_eva_com = '{$objectMuestras->{'INSTaLaCIoN EQUIPO (CLIMaTIZaCIoN)'}['Instalación Evaporador: Comentarios']}',
            inst_conde = '{$objectMuestras->{'INSTaLaCIoN EQUIPO (CLIMaTIZaCIoN)'}['Instalación Condensador:']}',
            inst_conde_com = '{$objectMuestras->{'INSTaLaCIoN EQUIPO (CLIMaTIZaCIoN)'}['Instalación Condensador: Comentarios']}',
            inst_conec = '{$objectMuestras->{'INSTaLaCIoN EQUIPO (CLIMaTIZaCIoN)'}['Conexiones Terminadas:']}',
            inst_conec_com = '{$objectMuestras->{'INSTaLaCIoN EQUIPO (CLIMaTIZaCIoN)'}['Conexiones Terminadas: Comentarios']}',
            inst_prue_dre = '{$objectMuestras->{'INSTaLaCIoN EQUIPO (CLIMaTIZaCIoN)'}['Prueba de Drenaje con el Equipo:']}',
            inst_prue_dre_com = '{$objectMuestras->{'INSTaLaCIoN EQUIPO (CLIMaTIZaCIoN)'}['Prueba de Drenaje con el Equipo: Comentarios']}',
            inst_observa = '{$objectMuestras->{'INSTaLaCIoN EQUIPO (CLIMaTIZaCIoN)'}['Observaciones:']}',
            prueba_temperatura = '{$objectMuestras->{'PRUEBaS FINaLES EQUIPO (CLIMaTIZaCIoN)'}['Temperatura Final (◦C):']}',
            prueba_arranq_funcionamiento = '{$objectMuestras->{'PRUEBaS FINaLES EQUIPO (CLIMaTIZaCIoN)'}['Prueba de Arranque y Funcionamiento:']['display']}',
            inst_fotos = '{$objectMuestras->{'INSTaLaCIoN EQUIPO (CLIMaTIZaCIoN)'}['Captura de Foto']}',
            pf_pregunta_1 = '{$objectMuestras->{'PRUEBaS FINaLES EQUIPO (CLIMaTIZaCIoN)'}['Tiempo de Vacío:']['display']}',
            pf_pregunta_3 = '{$objectMuestras->{'PRUEBaS FINaLES EQUIPO (CLIMaTIZaCIoN)'}['Temperatura Final (◦C):']}',
            pf_pregunta_2 = '{$objectMuestras->{'PRUEBaS FINaLES EQUIPO (CLIMaTIZaCIoN)'}['Prueba de Arranque y Funcionamiento:']['display']}',
            pf_observa = '{$objectMuestras->{'PRUEBaS FINaLES EQUIPO (CLIMaTIZaCIoN)'}['Observaciones:']}',
            pf_fotos = '{$objectMuestras->{'PRUEBaS FINaLES EQUIPO (CLIMaTIZaCIoN)'}['Imágenes de la Instalación']}'
            WHERE id = '{$id_reporte}'"; 
    DD($sql);
    $mysqli->query($sql);

    $sql = "UPDATE reportes_r_instalacion
            SET
            responsable  = '{$objectMuestras->{'OBSERVaCIONES GENERaLES Y FIRMaS'}['Nombre del responsable que supervisa el trabajo:']}',
            cargo_responsable = '{$objectMuestras->{'OBSERVaCIONES GENERaLES Y FIRMaS'}['Cargo:']}',
            observa_fin = '{$objectMuestras->{'OBSERVaCIONES GENERaLES Y FIRMaS'}['Observaciones Generales:']}',
            img_firma = '{$objectMuestras->{'OBSERVaCIONES GENERaLES Y FIRMaS'}['Firma del Cliente o Responsable']}',
            tipo_reporte = '{$objectMuestras->{'INFORMaCIoN EQUIPO'}['Tipo de Equipo:']}',
            nombre_json = '{$objectMuestras->json}',
            order_time = NOW()
            WHERE id = '{$id_reporte}'"; 
    DD($sql);
    $mysqli->query($sql);

    if($objectMuestras->{'INFORMaCIoN EQUIPO'}['Tipo de Equipo:'] == 'CLIMATIZACIÓN B'){
        $sql = "UPDATE reportes_r_instalacion
                SET
                tipo_equipo = '{$objectMuestras->{'INFORMaCIoN EQUIPO'}['Tipo de Equipo:']}',
                codigo_equipo = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN B)'}['Código del Equipo:']}', 
                desc_equipo = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN B)'}['Descripción del Equipo:']}',
                nombre_area = '{$objectMuestras->{'INFORMaCIoN EQUIPO'}['Nombre del Área:']}',
                area_clima = '{$objectMuestras->{'INFORMaCIoN EQUIPO'}['Área que Climatiza:']}',
                id_area = '{$objectMuestras->{'INFORMaCIoN EQUIPO'}['Área que Climatiza: - id']}',
                evaporador = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN B)'}['Evaporador']}', 
                marca_eva = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN B)'}['Marca:']}', 
                btu_eva = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN B)'}['Capacidad (BTU):']}', 
                modelo_eva = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN B)'}['Modelo:']}',
                serie_eva = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN B)'}['Serie:']}',
                condensador = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN B)'}['Condensador']}',
                observa2 = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN B)'}['Observaciones:']}'
                WHERE id = '{$id_reporte}'";
        DD($sql);
        $mysqli->query($sql);

        $sql = "INSERT INTO ins_caracteristicas_climatizacion_b 
            SET 
            id_orden = {$id_orden}, 
            descripcion= '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN B)'}['Descripción del Equipo:']}', 
            codigo = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN B)'}['Código del Equipo:']}', 
            evaporador = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN B)'}['Evaporador']}', 
            marca = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN B)'}['Marca:']}', 
            capacidadBTU = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN B)'}['Capacidad (BTU):']}', 
            modelo = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN B)'}['Modelo:']}', 
            serie = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN B)'}['Serie:']}', 
            condensador = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN B)'}['Condensador']}',  
            condensadorMarca = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN B)'}['Observaciones:']}'";
        $mysqli->query($sql);
    }

    if($objectMuestras->{'INFORMaCIoN EQUIPO'}['Tipo de Equipo:'] == 'CLIMATIZACIÓN A'){
        $sql = "UPDATE reportes_r_instalacion
                SET
                tipo_equipo = '{$objectMuestras->{'INFORMaCIoN EQUIPO'}['Tipo de Equipo:']}',
                codigo_equipo = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN a)'}['Código del Equipo:']}', 
                desc_equipo = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN a)'}['Descripción del Equipo:']}',
                nombre_area = '{$objectMuestras->{'INFORMaCIoN EQUIPO'}['Nombre del Área:']}',
                area_clima = '{$objectMuestras->{'INFORMaCIoN EQUIPO'}['Área que Climatiza:']}',
                id_area = '{$objectMuestras->{'INFORMaCIoN EQUIPO'}['Área que Climatiza: - id']}',
                evaporador = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN a)'}['Evaporador / Condensador']}',
                marca_eva = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN a)'}['Marca:']}',
                btu_eva = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN a)'}['Capacidad (BTU):']}',
                modelo_eva = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN a)'}['Modelo:']}',
                serie_eva = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN a)'}['Serie:']}',
                condensador = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN a)'}['Evaporador / Condensador']}',
                marca_conde = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN a)'}['Marca:']}',
                btu_conde = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN a)'}['Capacidad (BTU):']}',
                modelo_conde = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN a)'}['Modelo:']}',
                serie_conde = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN a)'}['Serie:']}',
                observa2 = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN a)'}['Observaciones:']}'";
        DD($sql);
        $mysqli->query($sql);

        $sql = "INSERT INTO ins_caracteristicas_climatizacion_a 
            SET 
            id_orden = {$id_orden}', 
            descripcion = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN a)'}['Descripción de Equipo']}',
            codigo = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN a)'}['Código del Equipo:']}', 
            marca = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN a)'}['Marca:']}', 
            capacidadBTU = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN a)'}['Capacidad (BTU):']}', 
            modelo = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN a)'}['Modelo:']}', 
            serie = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN a)'}['Serie:']}'";
        $mysqli->query($sql);
    }

    if($objectMuestras->{'INFORMaCIoN EQUIPO'}['Tipo de Equipo:'] == 'VENTILACION'){
        $sql = "UPDATE reportes_r_instalacion
                SET
                tipo_equipo = '{$objectMuestras->{'INFORMaCIoN EQUIPO'}['Tipo de Equipo:']}',
                codigo_equipo = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (VENTILaCIoN)'}['Código del Equipo:']}', 
                desc_equipo = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (VENTILaCIoN)'}['Descripción del Equipo:']}',
                nombre_area = '{$objectMuestras->{'INFORMaCIoN EQUIPO'}['Nombre del Área:']}',
                area_clima = '{$objectMuestras->{'INFORMaCIoN EQUIPO'}['Área que Climatiza:']}',
                id_area = '{$objectMuestras->{'INFORMaCIoN EQUIPO'}['Área que Climatiza: - id']}'";
        DD($sql);
        $mysqli->query($sql);
    }

    if($objectMuestras->{'INFORMaCIoN EQUIPO'}['Tipo de Equipo:'] == 'REFRIGERACION'){
        $sql = "UPDATE reportes_r_instalacion
                SET
                tipo_equipo = '{$objectMuestras->{'INFORMaCIoN EQUIPO'}['Tipo de Equipo:']}',
                codigo_equipo = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (REFRIGERaCIoN)'}['Código del Equipo:']}', 
                desc_equipo = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (REFRIGERaCIoN)'}['Descripción del Equipo:']}',
                nombre_area = '{$objectMuestras->{'INFORMaCIoN EQUIPO'}['Nombre del Área:']}',
                area_clima = '{$objectMuestras->{'INFORMaCIoN EQUIPO'}['Área que Climatiza:']}',
                id_area = '{$objectMuestras->{'INFORMaCIoN EQUIPO'}['Área que Climatiza: - id']}',
                cant_compres = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (REFRIGERaCIoN)'}['Cantidad Compresores:']}',
                condensador = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (REFRIGERaCIoN)'}['Condensador']}',
                marca_conde = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (REFRIGERaCIoN)'}['Marca:']}',
                btu_conde = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (REFRIGERaCIoN)'}['Capacidad Compresor (HP/BTU):']}',
                modelo_conde = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (REFRIGERaCIoN)'}['Modelo:']}',
                serie_conde = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (REFRIGERaCIoN)'}['Serie:']}',
                cant_motor_vent = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (REFRIGERaCIoN)'}['Cantidad Motor Ventilador:']}',,
                observa2 = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (REFRIGERaCIoN)'}['Observaciones:']}'";
        DD($sql);
        $mysqli->query($sql);

        $sql = "INSERT INTO ins_caracteristicas_refrigeracion 
                SET 
                id_orden = {$id_orden}, 
                descripcion = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (REFRIGERaCIoN)'}['Descripción del Equipo:']}', 
                codigo = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (REFRIGERaCIoN)'}['Código del Equipo:']}', 
                marca = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (REFRIGERaCIoN)'}['Marca:']}', 
                modelo = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (REFRIGERaCIoN)'}['Modelo:']}', 
                serie = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (REFRIGERaCIoN)'}['Serie:']}', 
                compresorBTU = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (REFRIGERaCIoN)'}['Capacidad Compresor (HP/BTU):']}', 
                compresorCantidad = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (REFRIGERaCIoN)'}['Cantidad Compresores:']}', 
                ventiladorHP = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (REFRIGERaCIoN)'}['Capacidad Motor Ventilador (HP):']}', 
                ventiladorCantidad = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (REFRIGERaCIoN)'}['Cantidad Motor Ventilador:']}'";
        $mysqli->query($sql);
    }

    for($x = 2; $x < 11; $x ++){
        $sql = "UPDATE reportes_r_instalacion 
                SET 
                mat_item{$x}_sel = '{$objectMuestras->{"ITEM #{$x}"}['Selección de Item:']}',
                mat_item{$x}_des = '{$objectMuestras->{"ITEM #{$x}"}['Descripción Item:']}',
                mat_item{$x}_uni = '{$objectMuestras->{"ITEM #{$x}"}['Definir Unidad:']}',
                mat_item{$x}_cant = '{$objectMuestras->{"ITEM #{$x}"}['Cantidad Utilizada:']}',
                mat_item{$x}_obser = '{$objectMuestras->{"ITEM #{$x}"}['Observaciones:']}'
                WHERE id = '{$id_reporte}'"; 
        DD($sql);
        $mysqli->query($sql);
    }

    if($objectMuestras->{'INFORMaCIoN EQUIPO'}['Tipo de Equipo:'] == 'CLIMATIZACIÓN B'){
        $sql = "INSERT INTO ins_caracteristicas_climatizacion_b 
            SET 
            id_orden = {$id_orden}, 
            descripcion= '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN B)'}['Descripción del Equipo:']}', 
            codigo = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN B)'}['Código del Equipo:']}', 
            evaporador = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN B)'}['Evaporador']}', 
            marca = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN B)'}['Marca:']}', 
            capacidadBTU = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN B)'}['Capacidad (BTU):']}', 
            modelo = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN B)'}['Modelo:']}', 
            serie = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN B)'}['Serie:']}', 
            condensador = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN B)'}['Condensador']}',  
            condensadorMarca = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN B)'}['Observaciones:']}'";
        $mysqli->query($sql);
    }

    if($objectMuestras->{'INFORMaCIoN EQUIPO'}['Tipo de Equipo:'] == 'CLIMATIZACIÓN A'){
        $sql = "INSERT INTO ins_caracteristicas_climatizacion_a 
            SET 
            id_orden = {$id_orden}', 
            descripcion = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN a)'}['Descripción de Equipo']}',
            codigo = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN a)'}['Código del Equipo:']}', 
            marca = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN a)'}['Marca:']}', 
            capacidadBTU = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN a)'}['Capacidad (BTU):']}', 
            modelo = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN a)'}['Modelo:']}', 
            serie = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (CLIMaTIZaCIoN a)'}['Serie:']}'";
        $mysqli->query($sql);
    }

    if($objectMuestras->{'INFORMaCIoN EQUIPO'}['Tipo de Equipo:'] == 'REFRIGERACION'){
        $sql = "INSERT INTO ins_caracteristicas_refrigeracion 
                SET 
                id_orden = {$id_orden}, 
                descripcion = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (REFRIGERaCIoN)'}['Descripción del Equipo:']}', 
                codigo = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (REFRIGERaCIoN)'}['Código del Equipo:']}', 
                marca = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (REFRIGERaCIoN)'}['Marca:']}', 
                modelo = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (REFRIGERaCIoN)'}['Modelo:']}', 
                serie = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (REFRIGERaCIoN)'}['Serie:']}', 
                compresorBTU = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (REFRIGERaCIoN)'}['Capacidad Compresor (HP/BTU):']}', 
                compresorCantidad = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (REFRIGERaCIoN)'}['Cantidad Compresores:']}', 
                ventiladorHP = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (REFRIGERaCIoN)'}['Capacidad Motor Ventilador (HP):']}', 
                ventiladorCantidad = '{$objectMuestras->{'CaRaCTERiSTICaS EQUIPO (REFRIGERaCIoN)'}['Cantidad Motor Ventilador:']}'";
        $mysqli->query($sql);
    }
}
      
function getValueFromDataType($dataType, $values, $referenceNumber){
    switch ($dataType) {
        case 'GeoLocation':
            return getValueFromGeoLocation($values);
        case 'Audio':
        case 'FreeText':
        case 'Information':
        case 'Url':
        case 'EmailAddress':
        case 'Date':
        case 'Duration':
        case 'Timestamp':
            return getValueFromFreeText($values);
        case 'Integer':
            return getValueFromInteger($values);
        case 'Signature':
            return getValueFromSignature($values, $referenceNumber);
        case 'Decimal':
            return getValueFromDecimal($values);
        case 'Time':
            return getValueFromTime($values);
        case 'Image':
            return getValueFromImage($values, $referenceNumber);
        case 'PhoneNumber':
            return getValueFromFreeText($values);
        default:
            DD("No has agregado el tipo: ".$dataType);
            return "";
    }
}

function limpiar($String)
{
    $String = str_replace(array('á','à','â','ã','ª','ä'),"a",$String);
    $String = str_replace(array('Á','À','Â','Ã','Ä'),"A",$String);
    $String = str_replace(array('Í','Ì','Î','Ï'),"I",$String);
    $String = str_replace(array('í','ì','î','ï'),"i",$String);
    $String = str_replace(array('é','è','ê','ë'),"e",$String);
    $String = str_replace(array('É','È','Ê','Ë'),"E",$String);
    $String = str_replace(array('ó','ò','ô','õ','ö','º'),"o",$String);
    $String = str_replace(array('Ó','Ò','Ô','Õ','Ö'),"O",$String);
    $String = str_replace(array('ú','ù','û','ü'),"u",$String);
    $String = str_replace(array('Ú','Ù','Û','Ü'),"U",$String);
    $String = str_replace(array('[','^','´','`','¨','~',']'),"",$String);
    $String = str_replace("ç","c",$String);
    $String = str_replace("Ç","C",$String);
    $String = str_replace("ñ","n",$String);
    $String = str_replace("Ñ","N",$String);
    $String = str_replace("Ý","Y",$String);
    $String = str_replace("ý","y",$String);
    
    $String = str_replace("&aacute;","a",$String);
    $String = str_replace("&Aacute;","A",$String);
    $String = str_replace("&eacute;","e",$String);
    $String = str_replace("&Eacute;","E",$String);
    $String = str_replace("&iacute;","i",$String);
    $String = str_replace("&Iacute;","I",$String);
    $String = str_replace("&oacute;","o",$String);
    $String = str_replace("&Oacute;","O",$String);
    $String = str_replace("&uacute;","u",$String);
    $String = str_replace("&Uacute;","U",$String);

    $String = str_replace("\u00C1;","Á",$String);
    $String = str_replace("\u00E1;","á",$String);
    $String = str_replace("\u00C9;","É",$String);
    $String = str_replace("\u00E9;","é",$String);
    $String = str_replace("\u00CD;","Í",$String);
    $String = str_replace("\u00ED;","í",$String);
    $String = str_replace("\u00D3;","Ó",$String);
    $String = str_replace("\u00F3;","ó",$String);
    $String = str_replace("\u00DA;","Ú",$String);
    $String = str_replace("\u00FA;","ú",$String);
    $String = str_replace("\u00DC;","Ü",$String);
    $String = str_replace("\u00FC;","ü",$String);
    $String = str_replace("\u00D1;","Ṅ",$String);
    $String = str_replace("\u00F1;","ñ",$String);
    $String = str_replace("A", "a", $String);
    
    return $String;
}

function ltl($s)
{
    return limpiar(trim(strtolower($s)));
}

function getValueFromGeoLocation($values){
    return isset($values[0])
        ? $values[0]["coordinates"]["latitude"].",".$values[0]["coordinates"]["longitude"]
        : "";
}

function getValueFromFreeText($values){
    return count($values) > 1
            ? implode(",", $values)
            : isset($values[0]) 
                ? ($values[0])
                : "";
}

function getValueFromInteger($values){
    return count($values) > 0
                ? addslashes(implode(",", $values))
                : "0";
}

function getValueFromDecimal($values){
    return (float) isset($values[0]) ? trim(addslashes($values[0])) : "0";
}

function getValueFromTime($values){
    return isset($values[0])
                        ? explode("-", $values[0]["provided"]["time"])[0]
                        : "";
}

function getValueFromSignature($values, $referenceNumber){
    return isset($values[0]) 
                        ? $referenceNumber ."_". $values[0]["filename"]
                        : "";
}

function getValueFromImage($values, $referenceNumber){
    $newVals = [];
    foreach($values as $val){
        $newVals[] = $referenceNumber ."_". $val["filename"];
    }
    return implode("|", $newVals);
}

function getToken(){
    $prefix = "?procesosiq?";

    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $part1 = '';
    $part2 = '';

    $length1 = rand(1, 15);
    $length2 = rand(1, 15);
    for ($i = 0; $i < $length1; $i++) {
        $part1 .= $characters[rand(0, $charactersLength - 1)];
    }
    for ($i = 0; $i < $length2; $i++) {
        $part2 .= $characters[rand(0, $charactersLength - 1)];
    }
    return base64_encode($part1.$prefix.$part2);
}

function DD($arreglo){
    echo "<pre>";
        print_r($arreglo);
    echo "</pre>";
}
