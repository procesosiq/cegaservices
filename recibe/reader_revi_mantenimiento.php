<?php
header('Content-Type: text/html; charset=utf-8');
ini_set('display_errors',1);
error_reporting(E_ALL);

/** DATABASE */
$connect = new stdClass;
$connect->db = @new mysqli("localhost", "auditoriasbonita", "u[V(fTIUbcVb", "cegaservices");
$connect->db2 = @new mysqli("localhost", "auditoriasbonita", "u[V(fTIUbcVb", "cegaservices2");

if (mysqli_connect_errno()) {
    printf("Falló la conexión: %s\n", mysqli_connect_error());
    exit();
}

$connect->db->set_charset("utf8");
$connect->db2->set_charset("utf8");


/* CODIGO QUE RECORRE LOS DIRECTORIOS EN BUSUEDA DEL CORRECTO*/
$path = realpath('./json');
$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
foreach($objects as $name => $object){
    if('.' != $object->getFileName() && '..' != $object->getFileName() &&
	   "/home/procesosiq/public_html/cegaservices2/recibe/json/revision_mantenimiento" == $object->getPath()){

		$pos1 = true;
        if($pos1 !== false){
            $ext = pathinfo($object->getPathName(), PATHINFO_EXTENSION);
            if('json' == $ext || 'jpeg' == $ext || 'jpg' == $ext || 'png' == $ext || 'mp3' == $ext || '3gpp' == $ext || 'pdf' == $ext || 'json1' == $ext || 'mp4' == $ext){
                switch ($ext) {
                    case 'json':
                        $json = json_decode(trim(file_get_contents($object->getPathName())), true);
                        process_json($json, $object->getFileName(), $connect);
                        move_json($object->getPathName(), $object->getFileName());
                        break;
                    case 'jpeg':
                    case 'jpg':
                    case 'png':
						            move_image($object->getPathName(), $object->getFileName());
                        break;
                    case 'mp3':
                    case '3gpp':
                    case 'mp4':
						            move_audio($object->getPathName(), $object->getFileName());
						            break;
                    case 'pdf':
                    case 'json1':
                      $file = $object->getPathName();
                      delete_file($file);
                      break;
                    default:
                        break;
                }
            }
        }
    }
}


/* FUNCION ENCARGADA DE LEER TODO EL ARCHIVO JSON */
function process_json($json, $filename, $connect){
	  //print_r($json); die('');

    $identifier = $json['identifier'];
    $version = $json['version']. '';
    $zone = $json['zone']. '';
    $referenceNumber = $json['referenceNumber']. '';
    $state = $json['state']. '';
    $deviceSubmitDate = $json['deviceSubmitDate'];
    $deviceSubmitDateDate = $deviceSubmitDate['time']. '';
    $deviceSubmitDateZone = $deviceSubmitDate['zone']. '';
    $fecha_file = str_replace("-", "", explode("T", $deviceSubmitDateDate)[0]);
    $shiftedDeviceSubmitDate = $json['shiftedDeviceSubmitDate'].'';
    $serverReceiveDate = $json['serverReceiveDate'].'';

    $form = $json['form'];
    $form_identifier = $form['identifier'].'';
    $form_versionIdentifier = $form['versionIdentifier'].'';
    $form_name = $form['name'].'';
    $form_version = $form['version'].'';
    $form_formSpaceIdentifier = $form['formSpaceIdentifier'].'';
    $form_formSpaceName = $form['formSpaceName'].'';

    $user = $json['user'];
    $user_identifier = $user['identifier'].'';
    $user_username = $user['username'].'';
    $user_displayName = $user['displayName'].'';

    $geoStamp = $json['geoStamp'];
    $geoStamp_success = $geoStamp['success'].'';
    $geoStamp_captureTimestamp = $geoStamp['captureTimestamp'];
    $geoStamp_captureTimestamp_provided = $geoStamp_captureTimestamp['provided'];
    $geoStamp_captureTimestamp_provided_time = $geoStamp_captureTimestamp_provided['time'].'';;
    $geoStamp_captureTimestamp_provided_zone = $geoStamp_captureTimestamp_provided['zone'].'';;
    $geoStamp_captureTimestamp_shifted = $geoStamp_captureTimestamp['shifted'].'';;
    $geoStamp_errorMessage = $geoStamp['errorMessage'].'';;
    $geoStamp_source = $geoStamp['source'].'';;
    $geoStamp_coordinates = $geoStamp['coordinates'];
    $geoStamp_coordinates_latitude = $geoStamp_coordinates['latitude'].'';;
    $geoStamp_coordinates_longitude = $geoStamp_coordinates['longitude'].'';;
    $geoStamp_coordinates_altitude = $geoStamp_coordinates['altitude'].'';;
    $geoStamp_address = $geoStamp['address'].'';;

    $pages = $json['pages'];


    /* INFORMACION GENERAL */
    $nombre_json = $filename;
    $infGen_TipoRepote = "";
    $infGen_idOrden = "";
    $infGen_Geoloca = "";
    $infGen_HoraLlegada = "";
    $infGen_horaPro = "";
    $infGen_fecMant = "";
    $infGen_cliente = "";
    $infGen_id_cliente = "";
    $infGen_tipo_cli = "";
    $infGen_sucursal = "";
    $infGen_id_sucursal = "";
    $infGen_direccion = "";
    $infGen_refLugar = "";
    $infGen_Trabajo = "";
    $infGen_Observa = "";
    $infGen_tipoTrabajo = "";

    //INFORMACION DEL EQUIPOS
    $contEqui = 0;
    $infoEquipo_tipo = [];
    $infoEquipo_IDarea = [];
    $infoEquipo_area = [];
    $infoEquipo_nom_area = [];
    $infoEquipo_observa = [];

    $caract_num_eva = [];
    $caract_num_venti_eva = [];
    $caract_num_conde = [];
    $caract_num_venti_conde = [];

    //DETALLES DEL TRABAJO
    $especi_difAcceso = "";
    $especi_difAcceso_com = "";
    $especi_acceso_agua = "";
    $especi_acceso_agua_com = "";
    $especi_observa = "";
    $especi_fotos = "";

    //HERRAMIENTAS DE TRABAJO
    $herr_detalle = "";
    $herr_otros = "";
    $hora_salida = "";

    //ONSERVACIONES Generales
    $ObserGral_responsable = "";
    $ObserGral_cargo = "";
    $ObserGral_firma = "";
    $ObserGral_audio = "";
    $ObserGral_observa = "";

    foreach($pages as $key => $page_data){
        $pagina = $key+1;
        $pagina_nombre = $page_data['name'];
        $sql_muestra_causas = array();
        $answers = $page_data["answers"];

        foreach($answers as $answer){

            $label    = $answer['label'];
            $dataType = $answer['dataType'];
            $question = $answer['question'];
            $values   = $answer['values'];
            $labelita = limpiar(trim(strtolower($label)));

            if("informacion general" == limpiar(trim(strtolower($pagina_nombre)))){
              if(ltl($question) == "geolocalizacion"){
                $infGen_Geoloca = $values[0]['coordinates']['latitude'].','.$values[0]['coordinates']['longitude'];

                $salida_fecha = explode("T", $json["shiftedDeviceSubmitDate"])[0];
                $salida_hora = explode("-",explode("T", $json["shiftedDeviceSubmitDate"])[1])[0];
                $hora_salida = $salida_fecha." ".$salida_hora;
              }

              if(ltl($question) == "hora de llegada:"){
                $infGen_HoraLlegada = $values[0]['provided']['time'];
              }

              $infGen_horaPro = ltl($question) == "hora programada:" ? (isset($values[0]) ? $values[0] : '') : $infGen_horaPro;
              $infGen_fecMant = ltl($question) == "fecha de la revision:" ? (isset($values[0]) ? $values[0] : '') : $infGen_fecMant;

              //DATOS DEL CLIENTE
              $infGen_cliente = ltl($question) == "cliente:" ? (isset($values[0]) ? $values[0] : '') : $infGen_cliente;
              $infGen_id_cliente = ltl($question) == "cliente: - id" ? (isset($values[0]) ? $values[0] : '') : $infGen_id_cliente;
              $infGen_tipo_cli = ltl($question) == "cliente: - id_tipo_cliente" ? (isset($values[0]) ? $values[0] : '') : $infGen_tipo_cli;

              //SUCURSAL
              $infGen_sucursal = ltl($question) == "sucursal:" ? (isset($values[0]) ? $values[0] : '') : $infGen_sucursal;
              $infGen_id_sucursal = ltl($question) == "sucursal: - id" ? (isset($values[0]) ? $values[0] : '') : $infGen_id_sucursal;
              if(ltl($label) == "c:-d"){
                  $infGen_direccion = isset($values[0]) ? $values[0] : '';
              }
              $infGen_refLugar = ltl($question) == "referencia:" ? (isset($values[0]) ? $values[0] : '') : $infGen_refLugar;


              $infGen_Trabajo = ltl($question) == "detalle de trabajo:" ? (isset($values[0]) ? $values[0] : '') : $infGen_Trabajo;
              $infGen_Observa = ltl($question) == "observaciones:" ? (isset($values[0]) ? $values[0] : '') : $infGen_Observa;
              $infGen_tipoTrabajo = ltl($question) == "tipo de trabajo:" ? (isset($values[0]) ? $values[0] : '') : $infGen_tipoTrabajo;

              $infGen_idOrden = ltl($question) == "orden de trabajo - id" ? (isset($values[0]) ? $values[0] : '') : $infGen_idOrden;
            }

            /* INFORMACION DE LOS EQUIPOS */
            if("informacion equipo 1" == limpiar(trim(strtolower($pagina_nombre)))){

              if(ltl($question) == 'tipo de equipo:'){
                if(isset($values[0])){
                  $infoEquipo_tipo[0] = $values[0];
                  $contEqui++;
                }
              }

              if(ltl($question) == 'area que climatiza - id'){
                if(isset($values[0])){
                  $infoEquipo_IDarea[0] = $values[0];
                }
              }

              if(ltl($question) == 'area que climatiza'){
                if(isset($values[0])){
                  $infoEquipo_area[0] = $values[0];
                }
              }



              if(ltl($question) == 'nombre del area:'){
                if(isset($values[0])){
                  $infoEquipo_nom_area[0] = $values[0];
                }
              }

              if(ltl($question) == 'observaciones:'){
                if(isset($values[0])){
                  $infoEquipo_observa[0] = $values[0];
                }
              }

            }

            if("informacion equipo 2" == limpiar(trim(strtolower($pagina_nombre)))){

              if(ltl($question) == 'tipo de equipo:'){
                if(isset($values[0])){
                  $infoEquipo_tipo[1] = $values[0];
                  $contEqui++;
                }
              }

              if(ltl($question) == 'area que climatiza - id'){
                if(isset($values[0])){
                  $infoEquipo_IDarea[1] = $values[0];
                }
              }

              if(ltl($question) == 'area que climatiza'){
                if(isset($values[0])){
                  $infoEquipo_area[1] = $values[0];
                }
              }

              if(ltl($question) == 'nombre del area:'){
                if(isset($values[0])){
                  $infoEquipo_nom_area[1] = $values[0];
                }
              }

              if(ltl($question) == 'observaciones:'){
                if(isset($values[0])){
                  $infoEquipo_observa[1] = $values[0];
                }
              }

            }

            if("informacion equipo 3" == limpiar(trim(strtolower($pagina_nombre)))){

              if(ltl($question) == 'tipo de equipo:'){
                if(isset($values[0])){
                  $infoEquipo_tipo[2] = $values[0];
                  $contEqui++;
                }
              }

              if(ltl($question) == 'area que climatiza - id'){
                if(isset($values[0])){
                  $infoEquipo_IDarea[2] = $values[0];
                }
              }

              if(ltl($question) == 'area que climatiza'){
                if(isset($values[0])){
                  $infoEquipo_area[2] = $values[0];
                }
              }

              if(ltl($question) == 'nombre del area:'){
                if(isset($values[0])){
                  $infoEquipo_nom_area[2] = $values[0];
                }
              }

              if(ltl($question) == 'observaciones:'){
                if(isset($values[0])){
                  $infoEquipo_observa[2] = $values[0];
                }
              }

            }

            /* CARACTERISTICAS DE LOS EQUIPOS */
            if("caracteristicas equipo 1 (climatizacion)" == limpiar(trim(strtolower($pagina_nombre)))){
              if(limpiar($infoEquipo_tipo[0]) == 'CLIMATIZACION A' || limpiar($infoEquipo_tipo[0]) == 'CLIMATIZACION B'){

                if(ltl($question) == 'descripcion del equipo:'){
                  if(isset($values[0])){
                    $caract_desEquipo[0] = $values[0];
                  }
                }

                if(ltl($question) == 'capacidad (btu):'){
                  if(isset($values[0])){
                    $caract_BTU[0] = $values[0];
                  }
                }

                if(ltl($question) == 'cantidad:'){
                  if(isset($values[0])){
                    $caract_canti[0] = $values[0];
                  }
                }

                if(ltl($question) == 'observaciones:'){
                  if(isset($values[0])){
                    $caract_obser[0] = $values[0];
                  }
                }

                if(ltl($question) == 'agregar otra descripcion de equipo:'){
                  if(isset($values[0])){
                    $caract_otra[0] = $values[0];
                  }
                }
              }
            }

            if("caracteristicas equipo 2 (climatizacion)" == limpiar(trim(strtolower($pagina_nombre)))){
              if(limpiar($infoEquipo_tipo[1]) == 'CLIMATIZACION A' || limpiar($infoEquipo_tipo[1]) == 'CLIMATIZACION B'){

                if(ltl($question) == 'descripcion del equipo:'){
                  if(isset($values[0])){
                    $caract_desEquipo[1] = $values[0];
                  }
                }

                if(ltl($question) == 'capacidad (btu):'){
                  if(isset($values[0])){
                    $caract_BTU[1] = $values[0];
                  }
                }

                if(ltl($question) == 'cantidad:'){
                  if(isset($values[0])){
                    $caract_canti[1] = $values[0];
                  }
                }

                if(ltl($question) == 'observaciones:'){
                  if(isset($values[0])){
                    $caract_obser[1] = $values[0];
                  }
                }

                if(ltl($question) == 'agregar otra descripcion de equipo:'){
                  if(isset($values[0])){
                    $caract_otra[1] = $values[0];
                  }
                }
              }
            }

            if("caracteristicas equipo 3 (climatizacion)" == limpiar(trim(strtolower($pagina_nombre)))){
              if(limpiar($infoEquipo_tipo[2]) == 'CLIMATIZACION A' || limpiar($infoEquipo_tipo[2]) == 'CLIMATIZACION B'){

                if(ltl($question) == 'descripcion del equipo:'){
                  if(isset($values[0])){
                    $caract_desEquipo[2] = $values[0];
                  }
                }

                if(ltl($question) == 'capacidad (btu):'){
                  if(isset($values[0])){
                    $caract_BTU[2] = $values[0];
                  }
                }

                if(ltl($question) == 'cantidad:'){
                  if(isset($values[0])){
                    $caract_canti[2] = $values[0];
                  }
                }

                if(ltl($question) == 'observaciones:'){
                  if(isset($values[0])){
                    $caract_obser[2] = $values[0];
                  }
                }

                if(ltl($question) == 'agregar otra descripcion de equipo:'){
                  if(isset($values[0])){
                    $caract_otra[2] = $values[0];
                  }
                }
              }
            }

            if("caracteristicas equipo 1 (ventilacion)" == limpiar(trim(strtolower($pagina_nombre)))){
              if(limpiar($infoEquipo_tipo[0]) == 'VENTILACION'){

                if(ltl($question) == 'descripcion del equipo:'){
                  if(isset($values[0])){
                    $caract_desEquipo[0] = $values[0];
                  }
                }

                if(ltl($question) == 'capacidad (hp):'){
                  if(isset($values[0])){
                    $caract_HP[0] = $values[0];
                  }
                }

                if(ltl($question) == 'capacidad (cfm):'){
                  if(isset($values[0])){
                    $caract_CFM[0] = $values[0];
                  }
                }

                if(ltl($question) == 'cantidad:'){
                  if(isset($values[0])){
                    $caract_canti[0] = $values[0];
                  }
                }

                if(ltl($question) == 'observaciones:'){
                  if(isset($values[0])){
                    $caract_obser[0] = $values[0];
                  }
                }

                if(ltl($question) == 'agregar otra descripcion de equipo:'){
                  if(isset($values[0])){
                    $caract_otra[0] = $values[0];
                  }
                }
              }
            }

            if("caracteristicas equipo 2 (ventilacion)" == limpiar(trim(strtolower($pagina_nombre)))){
              if(limpiar($infoEquipo_tipo[1]) == 'VENTILACION'){

                if(ltl($question) == 'descripcion del equipo:'){
                  if(isset($values[0])){
                    $caract_desEquipo[1] = $values[0];
                  }
                }

                if(ltl($question) == 'capacidad (hp):'){
                  if(isset($values[0])){
                    $caract_HP[1] = $values[0];
                  }
                }

                if(ltl($question) == 'capacidad (cfm):'){
                  if(isset($values[0])){
                    $caract_CFM[1] = $values[0];
                  }
                }

                if(ltl($question) == 'cantidad:'){
                  if(isset($values[0])){
                    $caract_canti[1] = $values[0];
                  }
                }

                if(ltl($question) == 'observaciones:'){
                  if(isset($values[0])){
                    $caract_obser[1] = $values[0];
                  }
                }

                if(ltl($question) == 'agregar otra descripcion de equipo:'){
                  if(isset($values[0])){
                    $caract_otra[1] = $values[0];
                  }
                }
              }
            }

            if("caracteristicas equipo 3 (ventilacion)" == limpiar(trim(strtolower($pagina_nombre)))){
              if(limpiar($infoEquipo_tipo[2]) == 'VENTILACION'){

                if(ltl($question) == 'descripcion del equipo:'){
                  if(isset($values[0])){
                    $caract_desEquipo[2] = $values[0];
                  }
                }

                if(ltl($question) == 'capacidad (hp):'){
                  if(isset($values[0])){
                    $caract_HP[2] = $values[0];
                  }
                }

                if(ltl($question) == 'capacidad (cfm):'){
                  if(isset($values[0])){
                    $caract_CFM[2] = $values[0];
                  }
                }

                if(ltl($question) == 'cantidad:'){
                  if(isset($values[0])){
                    $caract_canti[2] = $values[0];
                  }
                }

                if(ltl($question) == 'observaciones:'){
                  if(isset($values[0])){
                    $caract_obser[2] = $values[0];
                  }
                }

                if(ltl($question) == 'agregar otra descripcion de equipo:'){
                  if(isset($values[0])){
                    $caract_otra[2] = $values[0];
                  }
                }
              }
            }

            if("caracteristicas equipo 1 (refrigeracion)" == limpiar(trim(strtolower($pagina_nombre)))){
              if(limpiar($infoEquipo_tipo[0]) == 'REFRIGERACION'){

                if(ltl($question) == 'descripcion del equipo:'){
                  if(isset($values[0])){
                    $caract_desEquipo[0] = $values[0];
                  }
                }

                if(ltl($question) == 'capacidad (hp):'){
                  if(isset($values[0])){
                    $caract_HP[0] = $values[0];
                  }
                }

                if(ltl($question) == 'cantidad:'){
                  if(isset($values[0])){
                    $caract_canti[0] = $values[0];
                  }
                }

                if(ltl($question) == 'cantidad evaporadores:'){
                  if(isset($values[0])){
                    $caract_num_eva[0] = $values[0];
                  }
                }

                if(ltl($question) == 'cantidad motor ventiladores (evaporadores):'){
                  if(isset($values[0])){
                    $caract_num_venti_eva[0] = $values[0];
                  }
                }

                if(ltl($question) == 'cantidad unidad condensadoras:'){
                  if(isset($values[0])){
                    $caract_num_conde[0] = $values[0];
                  }
                }

                if(ltl($question) == 'cantidad motor ventiladores (condensadoras):'){
                  if(isset($values[0])){
                    $caract_num_venti_conde[0] = $values[0];
                  }
                }

                if(ltl($question) == 'observaciones:'){
                  if(isset($values[0])){
                    $caract_obser[0] = $values[0];
                  }
                }

                if(ltl($question) == 'agregar otra descripcion de equipo:'){
                  if(isset($values[0])){
                    $caract_otra[0] = $values[0];
                  }
                }
              }
            }

            if("caracteristicas equipo 2 (refrigeracion)" == limpiar(trim(strtolower($pagina_nombre)))){
              if(limpiar($infoEquipo_tipo[1]) == 'REFRIGERACION'){

                if(ltl($question) == 'descripcion del equipo:'){
                  if(isset($values[0])){
                    $caract_desEquipo[1] = $values[0];
                  }
                }

                if(ltl($question) == 'capacidad (hp):'){
                  if(isset($values[0])){
                    $caract_HP[1] = $values[0];
                  }
                }

                if(ltl($question) == 'cantidad:'){
                  if(isset($values[0])){
                    $caract_canti[1] = $values[0];
                  }
                }

                if(ltl($question) == 'cantidad evaporadores:'){
                  if(isset($values[0])){
                    $caract_num_eva[1] = $values[0];
                  }
                }

                if(ltl($question) == 'cantidad motor ventiladores (evaporadores):'){
                  if(isset($values[0])){
                    $caract_num_venti_eva[1] = $values[0];
                  }
                }

                if(ltl($question) == 'cantidad unidad condensadoras:'){
                  if(isset($values[0])){
                    $caract_num_conde[1] = $values[0];
                  }
                }

                if(ltl($question) == 'cantidad motor ventiladores (condensadoras):'){
                  if(isset($values[0])){
                    $caract_num_venti_conde[1] = $values[0];
                  }
                }

                if(ltl($question) == 'observaciones:'){
                  if(isset($values[0])){
                    $caract_obser[1] = $values[0];
                  }
                }

                if(ltl($question) == 'agregar otra descripcion de equipo:'){
                  if(isset($values[0])){
                    $caract_otra[1] = $values[0];
                  }
                }
              }
            }

            if("caracteristicas equipo 3 (refrigeracion)" == limpiar(trim(strtolower($pagina_nombre)))){
              if(limpiar($infoEquipo_tipo[2]) == 'REFRIGERACION'){

                if(ltl($question) == 'descripcion del equipo:'){
                  if(isset($values[0])){
                    $caract_desEquipo[2] = $values[0];
                  }
                }

                if(ltl($question) == 'capacidad (hp):'){
                  if(isset($values[0])){
                    $caract_HP[2] = $values[0];
                  }
                }

                if(ltl($question) == 'cantidad:'){
                  if(isset($values[0])){
                    $caract_canti[2] = $values[0];
                  }
                }

                if(ltl($question) == 'cantidad evaporadores:'){
                  if(isset($values[0])){
                    $caract_num_eva[2] = $values[0];
                  }
                }

                if(ltl($question) == 'cantidad motor ventiladores (evaporadores):'){
                  if(isset($values[0])){
                    $caract_num_venti_eva[2] = $values[0];
                  }
                }

                if(ltl($question) == 'cantidad unidad condensadoras:'){
                  if(isset($values[0])){
                    $caract_num_conde[2] = $values[0];
                  }
                }

                if(ltl($question) == 'cantidad motor ventiladores (condensadoras):'){
                  if(isset($values[0])){
                    $caract_num_venti_conde[2] = $values[0];
                  }
                }

                if(ltl($question) == 'observaciones:'){
                  if(isset($values[0])){
                    $caract_obser[2] = $values[0];
                  }
                }

                if(ltl($question) == 'agregar otra descripcion de equipo:'){
                  if(isset($values[0])){
                    $caract_otra[2] = $values[0];
                  }
                }
              }
            }

            /* ESPECIFICACIONES DEL TRABAJO */
            if("especificaciones del trabajo" == limpiar(trim(strtolower($pagina_nombre)))){
              $especi_difAcceso = ltl($question) == "dificultad de acceso a equipos:" ? (isset($values[0]) ? $values[0] : '') : $especi_difAcceso;
              $especi_difAcceso_com = ltl($question) == "dificultad de acceso a equipos: comentarios" ? (isset($values[0]) ? $values[0] : '') : $especi_difAcceso_com;
              $especi_acceso_agua = ltl($question) == "acceso a puntos de agua y corriente:" ? (isset($values[0]) ? $values[0] : '') : $especi_acceso_agua;
              $especi_acceso_agua_com = ltl($question) == "acceso a puntos de agua y corriente: comentarios" ? (isset($values[0]) ? $values[0] : '') : $especi_acceso_agua_com;
              $especi_observa = ltl($question) == "observaciones:" ? (isset($values[0]) ? $values[0] : '') : $especi_observa;

              if(ltl($question) == 'captura de fotos'){
                if(isset($values[0]['filename']) != ''){
      						$especi_fotos .= $referenceNumber.'_'.espacios($values[0]['filename'])."|";
      					}
      					if(isset($values[1]['filename']) != ''){
      						$especi_fotos .= $referenceNumber.'_'.espacios($values[1]['filename'])."|";
      					}
      					if(isset($values[2]['filename']) != ''){
      						$especi_fotos .= $referenceNumber.'_'.espacios($values[2]['filename'])."|";
      					}
      					if(isset($values[3]['filename']) != ''){
      						$especi_fotos .= $referenceNumber.'_'.espacios($values[3]['filename'])."|";
      					}
      					if(isset($values[4]['filename']) != ''){
      						$especi_fotos .= $referenceNumber.'_'.espacios($values[4]['filename'])."|";
      					}
      					if(isset($values[5]['filename']) != ''){
      						$especi_fotos .= $referenceNumber.'_'.espacios($values[5]['filename'])."|";
      					}
      					if(isset($values[6]['filename']) != ''){
      						$especi_fotos .= $referenceNumber.'_'.espacios($values[6]['filename'])."|";
      					}
      					if(isset($values[7]['filename']) != ''){
      						$especi_fotos .= $referenceNumber.'_'.espacios($values[7]['filename'])."|";
      					}
      					if(isset($values[8]['filename']) != ''){
      						$especi_fotos .= $referenceNumber.'_'.espacios($values[8]['filename'])."|";
      					}
      					if(isset($values[9]['filename']) != ''){
      						$especi_fotos .= $referenceNumber.'_'.espacios($values[9]['filename'])."|";
      					}
              }

            }

            /* HERRAMIENTAS DEL TRABAJO */
            if("herramientas de trabajo" == limpiar(trim(strtolower($pagina_nombre)))){
              if(ltl($question) == 'herramientas necesarias para el trabajo:'){
      					foreach($values as $dato){
      						$herr_detalle .= $dato."<br>";
      					}
      				}

              $herr_otros = ltl($question) == 'si eligio la opcion otros por favor especificar:' ? (isset($values[0]) ? $values[0] : '') : $herr_otros;
            }

            /* OBSERVACIONES GENERALES Y FIRMA DEL CLIENTE */
            if("observaciones generales y firmas" == limpiar(trim(strtolower($pagina_nombre)))){
              if(ltl($question) == 'nombre del responsable que supervisa el trabajo:'){
                $ObserGral_responsable =  (isset($values[0]) ? $values[0] : '');
              }
              $ObserGral_cargo = ltl($question) == 'cargo:' ? (isset($values[0]) ? $values[0] : '') : $ObserGral_cargo;
              if(ltl($question) == 'firma del cliente o responsable'){
      					if(isset($values[0]['filename']))
      						$ObserGral_firma = $referenceNumber.'_'.$values[0]['filename'];
      				}

              if(ltl($question) == 'comentarios generales'){
      					if(isset($values[0]['filename'])){
      						$ext = pathinfo($values[0]['filename'], PATHINFO_EXTENSION);
      						$ObserGral_audio = $referenceNumber.'_'.$values[0]['filename'];#.'.'.$ext;
      					}
      				}

              $ObserGral_observa = ltl($question) == 'observaciones generales' ? (isset($values[0]) ? $values[0] : '') : $ObserGral_observa;
            }
        }
    }


    // MySQL
    $mysqli = $connect->db2;
    $id_usuario = 2;
    if(isset($user_username) && $user_username != "" && $user_username == "ernesto.polit"){
        $mysqli = $connect->db2;
        $id_usuario = 2;
    }

    for($x=0; $x < $contEqui; $x++){
      $sql = "INSERT INTO `reportes_revision_mantenimiento` SET
      `id_orden`='$infGen_idOrden',`geolocalizacion`='$infGen_Geoloca',`fecha_mante`='$infGen_fecMant',`hora_llegada`='$infGen_HoraLlegada', hora_salida = '{$hora_salida}',
      `hora_programada`='$infGen_horaPro',`cliente`='$infGen_cliente',`id_cliente`='$infGen_id_cliente',
      `tipo_cliente`='$infGen_tipo_cli',`sucursal`='$infGen_sucursal',`id_sucursal`='$infGen_id_sucursal',
      `direccion`='$infGen_direccion',`referencia`='$infGen_refLugar',`tipo_trabajo`='$infGen_tipoTrabajo',
      `detalle_trabajo`='$infGen_Trabajo',`infg_observa`='$infGen_Observa',numero_referencia = '{$referenceNumber}',


      `infequi_tipo`='$infoEquipo_tipo[$x]',`infequi_area_clima`='$infoEquipo_area[$x]',
      `infequi_area_nom`='$infoEquipo_nom_area[$x]',
      `infequi_area_id`='$infoEquipo_IDarea[$x]',`infequi_observa`='$infoEquipo_observa[$x]',

      `carac_desc_equi`='$caract_desEquipo[$x]',`carac_btu`='$caract_BTU[$x]',
      `carac_cantidad`='$caract_canti[$x]',`carac_observa`='$caract_obser[$x]',
      `carac_otra`='$caract_otra[$x]',
      carac_hp='$caract_HP[$x]',carac_cfm='$caract_CFM[$x]',
      caract_num_eva = '$caract_num_eva[$x]',
      caract_num_venti_eva = '$caract_num_venti_eva[$x]',
      caract_num_conde = '$caract_num_conde[$x]',
      caract_num_venti_conde = '$caract_num_venti_conde[$x]',

      `espec_dif_acceso`='$especi_difAcceso',`espec_dif_accesocom`='$especi_difAcceso_com',
      `espec_acceso_agua`='$especi_acceso_agua',`espec_acceso_aguacom`='$especi_acceso_agua_com',
      `espec_observa`='$especi_observa',`espec_fotos`='$especi_fotos',

      `herra_necesita`='$herr_detalle',`herra_otras`='$herr_otros',


      `observa_responsable`='$ObserGral_responsable',`observa_resp_cargo`='$ObserGral_cargo',
      `observa_firma`='$ObserGral_firma',`observa_audio`='$ObserGral_audio',`observa_notas`='$ObserGral_observa',


      `tipo_reporte`='REVISION ".limpiar($infoEquipo_tipo[$x])."',`nombre_json`='$nombre_json'";

      if($mysqli->query($sql)== TRUE){ }
      else {
        echo "Error: " . $last_cot . "<br>" . $mysqli->error;
      }
      #D($sql);
    }




	echo "<br>Inserto: ".$nombre_json;
	#die("NOE TERMINO ".$sql);
}


/* FUNCIONES QUE SON ECARGADAS DE MOVER LOS ARCHIVOS A LA CARPETA DE LEIDOS*/

function D($string){
  echo "<pre>".$string."</pre>";
}

function delete_file($file){
  if(!unlink($file)){
    echo "error";
  }
}

function move_json($file, $nameFile){
    if(!rename($file, __DIR__."/../reportes_vistos/revision_mantenimiento/$nameFile")){
        echo 'error';
    }
}

function move_image($file, $nameFile){
    $nameFile = utf8_encode($nameFile);
    if(!rename($file, __DIR__."/../reportes_vistos/revision_mantenimiento/".espacios($nameFile))){
        echo 'error';
    }
}

function move_audio($file, $nameFile){
    if(!rename($file, __DIR__."/../reportes_vistos/revision_mantenimiento/$nameFile")){
        echo 'error';
    }
}

/* FIN DEL BLOQUE DE FUNCIONES QUE SON ENCARGADAS DE MOVER LOS ARCHIVOS A LA CARPETA DE LEIDOS */



function limpiar($String){

	$String = str_replace(array('á','à','â','ã','ª','ä'),"a",$String);
    $String = str_replace(array('Á','À','Â','Ã','Ä'),"A",$String);
    $String = str_replace(array('Í','Ì','Î','Ï'),"I",$String);
    $String = str_replace(array('í','ì','î','ï'),"i",$String);
    $String = str_replace(array('é','è','ê','ë'),"e",$String);
    $String = str_replace(array('É','È','Ê','Ë'),"E",$String);
    $String = str_replace(array('ó','ò','ô','õ','ö','º'),"o",$String);
    $String = str_replace(array('Ó','Ò','Ô','Õ','Ö'),"O",$String);
    $String = str_replace(array('ú','ù','û','ü'),"u",$String);
    $String = str_replace(array('Ú','Ù','Û','Ü'),"U",$String);
    $String = str_replace(array('[','^','´','`','¨','~',']'),"",$String);
    $String = str_replace("ç","c",$String);
    $String = str_replace("Ç","C",$String);
    $String = str_replace("ñ","n",$String);
    $String = str_replace("Ñ","N",$String);
    $String = str_replace("Ý","Y",$String);
    $String = str_replace("ý","y",$String);

    $String = str_replace("&aacute;","a",$String);
    $String = str_replace("&Aacute;","A",$String);
    $String = str_replace("&eacute;","e",$String);
    $String = str_replace("&Eacute;","E",$String);
    $String = str_replace("&iacute;","i",$String);
    $String = str_replace("&Iacute;","I",$String);
    $String = str_replace("&oacute;","o",$String);
    $String = str_replace("&Oacute;","O",$String);
    $String = str_replace("&uacute;","u",$String);
    $String = str_replace("&Uacute;","U",$String);

    $String = str_replace("\u00C1;","Á",$String);
    $String = str_replace("\u00E1;","á",$String);
    $String = str_replace("\u00C9;","É",$String);
    $String = str_replace("\u00E9;","é",$String);
    $String = str_replace("\u00CD;","Í",$String);
    $String = str_replace("\u00ED;","í",$String);
    $String = str_replace("\u00D3;","Ó",$String);
    $String = str_replace("\u00F3;","ó",$String);
    $String = str_replace("\u00DA;","Ú",$String);
    $String = str_replace("\u00FA;","ú",$String);
    $String = str_replace("\u00DC;","Ü",$String);
    $String = str_replace("\u00FC;","ü",$String);
    $String = str_replace("\u00D1;","Ṅ",$String);
    $String = str_replace("\u00F1;","ñ",$String);

    return $String;
}

function ltl($s){
    return strtolower(limpiar(trim($s)));
}

function espacios($string){
  return limpiar(str_replace(" ", "_", $string));
}