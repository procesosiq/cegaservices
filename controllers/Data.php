<?php
class Data {
    private $conexion;
    private $session;
    private $apiProntoforms;

    public function __construct(){
        $this->conexion = new M_Conexion;
        $this->session = Session::getInstance();
        include 'ApiProntoforms.php';
        $this->apiProntoforms = new ApiProntoforms;
    }

    public function actualizar($json_name, $formspace){
        $path_file = "/home/procesosiq/public_html/cegaservices2/json/".$json_name.".json";
        $content = file_get_contents("http://cegaservices2.procesos-iq.com/recibe/ds/".$json_name.".php");
        file_put_contents($path_file, $content);
        #ejecutar curl
        $url_upload = "https://api.prontoforms.com/api/[version]/formspaces/".$formspace."/upload.json";
        $url_upload = str_replace("[version]", "1", $url_upload);

        $user = "a2141323001";
        $pass = "bNVk6pYVN/c3xCZDLJBbcEDqHPAvpftPEr+0ej2Lg15z5Ztvh1uYcRmb1Zf7Qpst";
        
        $cmd = 'curl -v -k -u user:password -X PUT --upload-file '.$path_file.' '.$url_upload;
        $cmd = str_replace("user", $user, $cmd);
        $cmd = str_replace("password", $pass, $cmd);
        $this->writeFile($cmd);
        exec($cmd, $status, $response);
        
        unlink($path_file);
        
        echo "Actualizado";
    }

    private function writeFile($text){
        $filename = "logs.txt";
        $time = date('Y-m-d H:i:s');
        $line = "\n{$time} cmd := {$text}";
        if(!file_put_contents($filename, $line, FILE_APPEND )){
            $this->conexion->link->query("INSERT INTO error_apipronforms SET texto = '{$line}'");
        }
    }

    public function ds_areas(){
        $this->actualizar("ds_areas", "192150025/sources/165338002");
    }

    public function ds_listadoRevisionInstalacion(){
        $this->actualizar("ds_listadoRevisionInstalacion", "192150025/sources/165340004");
    }

    public function ds_herramientas(){
        $this->actualizar("ds_herramientas", "192150025/sources/165340005");
    }

    public function ds_descriptionEquipo(){
        $this->actualizar("ds_descriptionEquipo", "192150025/sources/165340006");
    }

    public function ds_listadoEquiposVentilacionDetalleInstalacion(){
        $this->actualizar("ds_listadoEquiposVentilacionDetalleInstalacion", "192150025/sources/165338007");
    }
    public function ds_listadoEquiposClimatizacionBDetalleInstalacion(){
        $this->actualizar("ds_listadoEquiposClimatizacionBDetalleInstalacion", "192150025/sources/165338008");
    }

    public function ds_listadoSucursales(){
        $this->actualizar("ds_listadoSucursales", "192150025/sources/165338009");
    }

    public function ds_listadoEquiposClimatizacionADetalleInstalacion(){
        $this->actualizar("ds_listadoEquiposClimatizacionADetalleInstalacion", "192150025/sources/165338011");
    }

    public function ds_listadoEquiposRefrigeracionDetalleInstalacion(){
        $this->actualizar("ds_listadoEquiposRefrigeracionDetalleInstalacion", "192150025/sources/165338010");
    }

    public function ds_listadoEvaporadoresInstalacion(){
        $this->actualizar("ds_listadoEvaporadoresInstalacion", "192150025/sources/165339023");
    }

    public function ds_supervisores(){
        $this->actualizar("ds_supervisores", "192150025/sources/165339030");
    }

    public function ds_clientes(){
        $this->actualizar("ds_clientes", "192150025/sources/165339029");
    }

    public function ds_clientesCorrectivo(){
        $this->actualizar("ds_clientesCorrectivo", "192150025/sources/165339031");
    }

    public function ds_clientesInstalacion(){
        $this->actualizar("ds_clientesInstalacion", "192150025/sources/165338025");
    }

    public function ds_horasDia(){
        $this->actualizar("ds_horasDia", "192150025/sources/165339034");
    }

    public function ds_listadoEquipos(){
        $this->actualizar("ds_listadoEquipos", "192150025/sources/165339035");
    }

    public function ds_listadoRepuestos(){
        $this->actualizar("ds_listadoRepuestos", "192150025/sources/165339036");
    }

    public function ds_listadoEquiposClimatizacionADetalleCorrectivo(){
        $this->actualizar("ds_listadoEquiposClimatizacionADetalleCorrectivo", "192150025/sources/165339039");
    }

    public function ds_listadoEquiposClimatizacionADetalleRevisionCorrectivo(){
        $this->actualizar("ds_listadoEquiposClimatizacionADetalleRevisionCorrectivo", "192150025/sources/165340035");
    }

    public function ds_listadoEquiposClimatizacionBDetalleCorrectivo(){
        $this->actualizar("ds_listadoEquiposClimatizacionBDetalleCorrectivo", "192150025/sources/165340036");
    }

    public function ds_listadoRevisionMatenimiento(){
        $this->actualizar("ds_listadoRevisionMatenimiento", "192150025/sources/165364005");
    }

    public function ds_clientesMantenimiento(){
        $this->actualizar("ds_clientesMantenimiento", "192150025/sources/164370007");
    }

    public function ds_listadoEquiposClimatizacionADetalleMantenimiento(){
        $this->actualizar("ds_listadoEquiposClimatizacionADetalleMantenimiento", "192150025/sources/165339038");
    }

    public function ds_listadoEquiposClimatizacionBDetalleMantenimiento(){
        $this->actualizar("ds_listadoEquiposClimatizacionBDetalleMantenimiento", "192150025/sources/165364006");
    }

    public function ds_listadoEquiposVentilacionDetalleMantenimiento(){
        $this->actualizar("ds_listadoEquiposVentilacionDetalleMantenimiento", "192150025/sources/165364007");
    }

    public function ds_listadoEquiposRefrigeracionDetalleMantenimiento(){
        $this->actualizar("ds_listadoEquiposRefrigeracionDetalleMantenimiento", "192150025/sources/165364008");
    }

    public function ds_listadoHerramientas(){
        $this->actualizar("ds_listadoHerramientas", "192150025/sources/165339037");
    }

    public function ds_listadoEquiposRefrigeracionDetalleCorrectivo(){
        $this->actualizar("ds_listadoEquiposRefrigeracionDetalleCorrectivo", "192150025/sources/165338041");
    }

    public function ds_listadoEquiposClimatizacionBDetalleRevisionCorrectivo(){
        $this->actualizar("ds_listadoEquiposClimatizacionBDetalleRevisionCorrectivo", "192150025/sources/165339045");
    }

    public function ds_listadoRevisionCorrectivo(){
        $this->actualizar("ds_listadoRevisionCorrectivo", "192150025/sources/165365033");
    }

    public function ds_listadoEquiposVentilacionDetalleRevisionCorrectivo(){
        $this->actualizar("ds_listadoEquiposVentilacionDetalleRevisionCorrectivo", "192150025/sources/165365034");
    }

    public function ds_listadoEquiposRefrigeracionDetalleRevisionCorrectivo(){
        $this->actualizar("ds_listadoEquiposRefrigeracionDetalleRevisionCorrectivo", "192150025/sources/165366020");
    }

    public function ds_listadoEvaporadoresRevisionCorrectivo(){
        $this->actualizar("ds_listadoEvaporadoresRevisionCorrectivo", "192150025/sources/165366021");
    }

    public function ds_listadoEquiposVentilacionDetalleCorrectivo(){
        $this->actualizar("ds_listadoEquiposVentilacionDetalleCorrectivo", "192150025/sources/165366022");
    }

    public function ds_listadoEvaporadoresCorrectivo(){
        $this->actualizar("ds_listadoEvaporadoresCorrectivo", "192150025/sources/165365035");
    }

    public function ds_listadoMateriales(){
        $this->actualizar("ds_listadoMateriales", "192150025/sources/165498044");
    }
}
?>