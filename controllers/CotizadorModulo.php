
<?php
class CotizadorModulo
{

	private $conexion;
    private $session;

    public function __construct(){
        $this->conexion = new M_Conexion("dev_cegaservices");
        $this->session = Session::getInstance();
    }

    public function getAreas(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $sWhere = ""; 

        $sql = "SELECT ca.id, ca.nombre AS area, SUM(cd.total) AS total
                FROM cotizador_areas ca
                INNER JOIN cotizador_detalle cd ON cd.id_cotizacion = ca.id_cotizacion AND cd.id_area = ca.id
                WHERE cd.id_cotizacion = '{$params->id_cotizacion}'
                GROUP BY cd.id_area";
        $response->cat_areas = $this->conexion->queryAll($sql);
        //if(!$response->cat_areas) $response->cat_areas = array("id" => 0, "area" => "");
        return json_encode($response);
    }

    public function getTags(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $sWhere = ""; 

        $sql = "SELECT 
                SUM(total) AS total_equipos
                FROM cotizador_detalle 
                WHERE id_cotizacion = '{$params->id_cotizacion}' AND tipo = 'EQUIPOS'
                GROUP BY tipo";
        $response->tags->total_equipos = $this->conexion->queryRow($sql)->total_equipos;
        $sql = "SELECT 
                SUM(total) AS total_materiales
                FROM cotizador_detalle 
                WHERE id_cotizacion = '{$params->id_cotizacion}' AND tipo = 'MATERIALES'
                GROUP BY tipo";
        $response->tags->total_materiales = $this->conexion->queryRow($sql)->total_materiales;
        $sql = "SELECT 
                SUM(total) AS total_repuestos
                FROM cotizador_detalle 
                WHERE id_cotizacion = '{$params->id_cotizacion}' AND tipo = 'REPUESTOS'
                GROUP BY tipo";
        $response->tags->total_repuestos = $this->conexion->queryRow($sql)->total_repuestos;
        $sql = "SELECT 
                SUM(total) AS total_mano
                FROM cotizador_detalle 
                WHERE id_cotizacion = '{$params->id_cotizacion}' AND tipo = 'MANO'
                GROUP BY tipo";
        $response->tags->total_mano = $this->conexion->queryRow($sql)->total_mano;
        $sql = "SELECT 
                SUM(total) AS total
                FROM cotizador_detalle 
                WHERE id_cotizacion = '{$params->id_cotizacion}'";
        $response->tags->total = $this->conexion->queryRow($sql)->total;
        
        return json_encode($response);
    }


    public function maxArea(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $sWhere = ""; 

        $sql = "SELECT MAX(id) AS id, id_cotizacion, nombre AS area, status FROM cotizador_areas WHERE id_cotizacion = '{$params->id_cotizacion}'";
        $response->max_areas = $this->conexion->queryRow($sql);
        return json_encode($response);
    }



    //EQUIPOS
        public function detalleEquipoR(){
            $filters = (object)json_decode(file_get_contents("php://input"));
            $sWhere = ""; 
            $id_cotizacion = "";

            if($filters->id_cotizacion == 0){
                $sql = "SELECT MAX(id) AS cot FROM cotizaciones";
                $cot = $this->conexion->queryRow($sql)->cot;

                $id_cotizacion = $cot;

            } else {
                $id_cotizacion = $filters->id_cotizacion;
            }

            $sql = "SELECT 
                        cat_equipos_excel.nombre AS item,
                        cat_equipos_excel.capacidad,
                        cat_equipos_excel.unidad, 
                        cotizador_detalle.cantidad AS cantidad, 
                        cotizador_detalle.precio_unit,
                        cotizador_detalle.total AS total,
                        cotizador_detalle.id,
                        id_area
                FROM cotizador_detalle 
                            INNER JOIN cat_equipos_excel ON cat_equipos_excel.id = cotizador_detalle.id_item_equipo 
                WHERE cotizador_detalle.id_cotizacion = '{$id_cotizacion}' AND cotizador_detalle.id_area = '{$filters->id_area}'
                GROUP BY cotizador_detalle.precio_unit, cotizador_detalle.id_item_equipo";
            $response->detalle_equipo_r = $this->conexion->queryAll($sql);

            $total_sum_equipo_r = 0;
            foreach($response->detalle_equipo_r as $row){
                $total_sum_equipo_r += $row->total;
            }
            $response->sum_total = $total_sum_equipo_r;
            return json_encode($response);
        }

        public function tipoEquipoR(){
            $filters = (object)json_decode(file_get_contents("php://input"));
            $sWhere = ""; 

            $sql = "SELECT id AS id, nombre AS label FROM cat_equipos_excel GROUP BY nombre";
            $response->tipo_equipo_r = $this->conexion->queryAllSpecial($sql);
            return json_encode($response);
        }

        public function capacidadEquipoR(){
            $filters = (object)json_decode(file_get_contents("php://input"));
            $sWhere = ""; 
            if($filters->tipo_equipo_r_id != ""){
                $sWhere .= " AND id = '{$filters->tipo_equipo_r_id}'";
            }
            $sql = "SELECT IF(capacidad IS NULL,'',capacidad) AS capacidad FROM cat_equipos_excel WHERE 1 = 1 $sWhere";
            $response = $this->conexion->queryRow($sql);
            return json_encode($response);
        }

        public function precioUnitEquipoR(){
            $filters = (object)json_decode(file_get_contents("php://input"));
            $sWhere = ""; 
            if($filters->tipo_equipo_r_id != ""){
                $sWhere .=" AND id = '{$filters->tipo_equipo_r_id}'";
            }
            $sql = "SELECT IF(precio = '',0.00,precio) AS precio FROM cat_equipos_excel WHERE 1 = 1 $sWhere";
            $response = $this->conexion->queryRow($sql);
            return json_encode($response);
        }

        public function unidadEquipoR(){
            $filters = (object)json_decode(file_get_contents("php://input"));
            $sWhere = ""; 
            if($filters->tipo_equipo_r_id != ""){
                $sWhere .=" AND id = '{$filters->tipo_equipo_r_id}'";
            }
            $sql = "SELECT IF(unidad IS NULL,'',unidad) AS unidad FROM cat_equipos_excel WHERE 1 = 1 $sWhere";
            $response = $this->conexion->queryRow($sql);
            return json_encode($response);
        }

        //INSERT EQUIPOS
        public function insertEquipoR(){
            $params = (object)json_decode(file_get_contents("php://input"));

            $id_cotizacion = "";

            if($params->id_cotizacion == 0){
                $sql = "SELECT MAX(id) AS cot FROM cotizaciones";
                $cot = $this->conexion->queryRow($sql)->cot;

                $id_cotizacion = $cot + 1;

            } else {
                $id_cotizacion = $params->id_cotizacion;
            }

            $sql = "SELECT COUNT(1) AS cont FROM cotizador_areas WHERE id_cotizacion = '{$id_cotizacion}' AND nombre = '{$params->nombre_area}'";
            $count = $this->conexion->query($sql)->cont;

            if($count == 0){
                $sql = "INSERT INTO cotizador_areas
                        SET 
                            id_cotizacion = '{$id_cotizacion}',
                            nombre = '{$params->nombre_area}'";
                $this->conexion->query($sql);
                $id_area = $this->conexion->consultas(1, $sql);
            } else {
                $id_area = $params->id_area;
            }


            $total = $params->cantidad_equipo_r * $params->precio_equipo_r;
            $sql = "INSERT INTO cotizador_detalle 
                    SET 
                        id_cotizacion = '{$id_cotizacion}',
                        id_item_equipo = '{$params->tipo_equipo_r_id}', 
                        fecha = CURRENT_DATE, 
                        hora = CURRENT_TIME, 
                        cantidad = '{$params->cantidad_equipo_r}', 
                        precio_unit = '{$params->precio_equipo_r}', 
                        total = '{$total}',
                        id_area = '{$id_area}',
                        tipo = 'EQUIPOS'";
            $this->conexion->query($sql);

            /*$sql = "SELECT COUNT(1) AS count FROM cotizador_detalle WHERE precio_unit = '{$params->precio_equipo_r}' AND id_item = '{$params->tipo_equipo_r_id}' AND id_cotizacion = '{$params->id_cotizacion}'";
            $count = $this->conexion->queryRow($sql)->count;
            if($count > 0){
                $total = $params->cantidad_equipo_r * $params->precio_equipo_r;
                $sql = "UPDATE cotizador_detalle 
                        SET 
                            fecha = CURRENT_DATE, 
                            hora = CURRENT_TIME, 
                            cantidad = '{$params->cantidad_equipo_r}',  
                            total = '{$total}'
                            WHERE id_item = '{$params->tipo_equipo_r_id}' AND id_cotizacion = '{$params->id_cotizacion}'";
                    echo $sql;
                    $this->conexion->query($sql);
            } else {}*/
        }
        public function deleteEquipoR(){
            $params = (object)json_decode(file_get_contents("php://input"));

            $sql = "DELETE FROM cotizador_detalle WHERE id = '{$params->id_cotizador}' AND id_cotizacion = '{$params->id_cotizacion}'";
            echo $sql;
            $this->conexion->query($sql);
        }

    //END EQUIPOS
    
    //MATERIALES
        public function detalle(){
            $filters = (object)json_decode(file_get_contents("php://input"));
            $sWhere = ""; 
            $id_cotizacion = "";

            if($filters->id_cotizacion == 0){
                $sql = "SELECT MAX(id) AS cot FROM cotizaciones";
                $cot = $this->conexion->queryRow($sql)->cot;

                $id_cotizacion = $cot;

            } else {
                $id_cotizacion = $filters->id_cotizacion;
            }

            $sql = "SELECT 
                        cat_materiales.item AS item,
                        cat_materiales.descripcion AS capacidad,
                        cat_materiales.unidad, 
                        cotizador_detalle.cantidad AS cantidad, 
                        cotizador_detalle.precio_unit,
                        cotizador_detalle.total AS total,
                        cotizador_detalle.id
                    FROM cotizador_detalle 
                        INNER JOIN cat_materiales ON cat_materiales.id = cotizador_detalle.id_item 
                    WHERE 
                        cotizador_detalle.id_cotizacion = '{$filters->id_cotizacion}' 
                        AND cotizador_detalle.id_area = '{$filters->id_area}'
                    GROUP BY cotizador_detalle.precio_unit,cotizador_detalle.id_item";
            $response->detalle_materiales = $this->conexion->queryAll($sql);
            
            $total_sum_materiales = 0;
            foreach($response->detalle_materiales as $row){
                $total_sum_materiales += $row->total;
            }
            $response->sum_total = $total_sum_materiales;
            return json_encode($response);
        }

        public function tipoTrabajo(){
            $filters = (object)json_decode(file_get_contents("php://input"));
            $sWhere = ""; 

            $sql = "SELECT id AS id, item AS label FROM cat_materiales GROUP BY item";
            $response->tipo_trabajo = $this->conexion->queryAllSpecial($sql);
            return json_encode($response);
        }

        public function capacidad(){
            $filters = (object)json_decode(file_get_contents("php://input"));
            $sWhere = ""; 
            if($filters->tipo_trabajo_id != ""){
                $sWhere .= " AND id = '{$filters->tipo_trabajo_id}'";
            }
            $sql = "SELECT id AS id, descripcion AS label FROM cat_materiales WHERE 1 = 1 $sWhere";
            $response->capacidad = $this->conexion->queryAllSpecial($sql);
            return json_encode($response);
        }

        public function precioUnit(){
            $filters = (object)json_decode(file_get_contents("php://input"));
            $sWhere = ""; 
            if($filters->tipo_trabajo_id != ""){
                $sWhere .=" AND id = '{$filters->tipo_trabajo_id}'";
            }
            if($filters->capacidad != ""){
                $sWhere .=" AND descripcion = '".addslashes($filters->capacidad)."'";
            }
            $sql = "SELECT IF(precio = '',0.00,precio) AS precio FROM cat_materiales WHERE 1 = 1 $sWhere";
            $response = $this->conexion->queryRow($sql);
            return json_encode($response);
        }

        public function unidad(){
            $filters = (object)json_decode(file_get_contents("php://input"));
            $sWhere = ""; 
            if($filters->tipo_trabajo_id != ""){
                $sWhere .=" AND id = '{$filters->tipo_trabajo_id}'";
            }
            if($filters->capacidad != ""){
                $sWhere .=" AND descripcion = '".addslashes($filters->capacidad)."'";
            }
            $sql = "SELECT IF(unidad IS NULL,'',unidad) AS unidad FROM cat_materiales WHERE 1 = 1 $sWhere";
            $response = $this->conexion->queryRow($sql);
            return json_encode($response);
        }

        public function insertMateriales(){
            $params = (object)json_decode(file_get_contents("php://input"));

            $id_cotizacion = "";

            if($params->id_cotizacion == 0){
                $sql = "SELECT MAX(id) AS cot FROM cotizaciones";
                $cot = $this->conexion->queryRow($sql)->cot;

                $id_cotizacion = $cot + 1;

            } else {
                $id_cotizacion = $params->id_cotizacion;
            }

            $sql = "SELECT COUNT(1) AS cont FROM cotizador_areas WHERE id_cotizacion = '{$id_cotizacion}' AND nombre = '{$params->nombre_area}'";
            $count = $this->conexion->query($sql)->cont;

            if($count == 0){
                $sql = "INSERT INTO cotizador_areas
                        SET 
                            id_cotizacion = '{$id_cotizacion}',
                            nombre = '{$params->nombre_area}'";
                $this->conexion->query($sql);
                $id_area = $this->conexion->consultas(1, $sql);
            } else {
                $id_area = $params->id_area;
            }

            $total = $params->cantidad* $params->precio;
            $sql = "INSERT INTO cotizador_detalle 
                    SET 
                        id_cotizacion = '{$id_cotizacion}',
                        id_item = '{$params->tipo_trabajo_id}', 
                        fecha = CURRENT_DATE, 
                        hora = CURRENT_TIME, 
                        cantidad = '{$params->cantidad}', 
                        precio_unit = '{$params->precio}', 
                        total = '{$total}',
                        id_area = '{$id_area}',
                        tipo = 'MATERIALES'";
            $this->conexion->query($sql);
        }

        public function deleteMateriales(){
            $params = (object)json_decode(file_get_contents("php://input"));

            $sql = "DELETE FROM cotizador_detalle WHERE id = '{$params->id_cotizador}' AND id_cotizacion = '{$params->id_cotizacion}'";
            echo $sql;
            $this->conexion->query($sql);
        }

    //END MATERIALES

    //REPUESTOS
        public function detalleRepuestos(){
            $filters = (object)json_decode(file_get_contents("php://input"));
            $sWhere = "";

            $sql = "SELECT  cotizador_detalle.id,
                            cat_equiposTipo.id AS id_tipo,
                     		cat_equiposTipo.nombre AS item,
                     		(SELECT nombre FROM cat_equiposLista WHERE id = cotizador_detalle.id_descripcion_repuestos) AS item_lista,
                     		(SELECT nombre FROM cat_equiposPartes WHERE id = cotizador_detalle.id_partes_repuestos) AS item_parte,
                     		(SELECT descripcion AS nombre FROM cat_piezas WHERE id = cotizador_detalle.id_pieza_repuestos) AS item_pieza,
                     		cotizador_detalle.cantidad AS cantidad,
                     		cotizador_detalle.precio_unit,
                     		cotizador_detalle.total AS total
                     	FROM cotizador_detalle 
                     		INNER JOIN cat_equiposTipo ON cotizador_detalle.id_item_repuesto = cat_equiposTipo.id
                        WHERE cotizador_detalle.id_cotizacion = '{$filters->id_cotizacion}' AND cotizador_detalle.id_area = '{$filters->id_area}'
                        GROUP BY cotizador_detalle.precio_unit,cotizador_detalle.id_item_repuesto";
            $response->detalle_repuestos = $this->conexion->queryAll($sql);

            $total_sum_repuestos = 0;
            foreach($response->detalle_repuestos as $row){
                $total_sum_repuestos += $row->total;
            }
            $response->sum_total = $total_sum_repuestos;
            return json_encode($response);
        }

        public function tipoEquipos(){
            $filters = (object)json_decode(file_get_contents("php://input"));
            $sWhere = ""; 
         
            $sql = "SELECT id AS id, nombre AS label FROM cat_equiposTipo WHERE status = 1";
            $response->tipo_equipos = $this->conexion->queryAllSpecial($sql);
            return json_encode($response);
        }

        public function descripcionEquipos(){
            $filters = (object)json_decode(file_get_contents("php://input"));
            $sWhere = ""; 
            if($filters->tipo_equipo_id != ""){
                $sWhere .= " AND id_tipoequipo = {$filters->tipo_equipo_id}";
            }
            $sql = "SELECT id AS id, nombre AS label FROM cat_equiposLista WHERE 1 = 1 $sWhere";
            $response->descripcion_equipos = $this->conexion->queryAllSpecial($sql);
            return json_encode($response);
        }

        public function parteEquipos(){
            $filters = (object)json_decode(file_get_contents("php://input"));
            $sWhere = ""; 
            if($filters->tipo_equipo_id != ""){
                $sWhere .= " AND id_tipo_equipo = {$filters->tipo_equipo_id}";
            }
            $sql = "SELECT id AS id, nombre AS label FROM cat_equiposPartes WHERE 1 = 1 $sWhere";
            $response->parte_equipos = $this->conexion->queryAllSpecial($sql);
            return json_encode($response);
        }

        public function piezaEquipos(){
            $filters = (object)json_decode(file_get_contents("php://input"));
            $sWhere = "";

            if($filters->tipo_equipo_id != ""){
                $sWhere .= " AND id_tipoequipo = {$filters->tipo_equipo_id}";
            }
            if($filters->descripcion_id != ""){
                $sWhere .= " AND id_descripcion_equipo = {$filters->descripcion_id}";
            }
            if($filters->parte_id != ""){
                $sWhere .= " AND id_parte_equipo = {$filters->parte_id}";
            }
            $sql = "SELECT id AS id, descripcion AS label FROM cat_piezas WHERE 1 = 1 AND status = 1 $sWhere";
            $response->pieza_equipos = $this->conexion->queryAllSpecial($sql);
            return json_encode($response);  
        }

        public function unidadEquipos(){

        }

        public function precioEquipos(){

        }

        public function insertRepuestos(){
            $params = (object)json_decode(file_get_contents("php://input"));


            $id_cotizacion = "";

            if($params->id_cotizacion == 0){
                $sql = "SELECT MAX(id) AS cot FROM cotizaciones";
                $cot = $this->conexion->queryRow($sql)->cot;

                $id_cotizacion = $cot + 1;

            } else {
                $id_cotizacion = $params->id_cotizacion;
            }

            $sql = "SELECT COUNT(1) AS cont FROM cotizador_areas WHERE id_cotizacion = '{$id_cotizacion}' AND nombre = '{$params->nombre_area}'";
            $count = $this->conexion->query($sql)->cont;

            if($count == 0){
                $sql = "INSERT INTO cotizador_areas
                        SET 
                            id_cotizacion = '{$id_cotizacion}',
                            nombre = '{$params->nombre_area}'";
                $this->conexion->query($sql);
                $id_area = $this->conexion->consultas(1, $sql);
            } else {
                $id_area = $params->id_area;
            }

            $total = $params->cantidad * $params->precio;
            $sql = "INSERT INTO cotizador_detalle 
                    SET 
                        id_cotizacion = '{$id_cotizacion}',
                        id_item_repuesto = '{$params->tipo_equipo_id}',
                        id_descripcion_repuestos = '{$params->descripcion_id}',
                        id_partes_repuestos = '{$params->parte_id}',
                        id_pieza_repuestos = '{$params->pieza_id}', 
                        fecha = CURRENT_DATE, 
                        hora = CURRENT_TIME, 
                        cantidad = '{$params->cantidad}', 
                        precio_unit = '{$params->precio}', 
                        total = '{$total}',
                        id_area = '{$id_area}',
                        tipo = 'REPUESTOS'";
                        echo $sql;
            $this->conexion->query($sql);
        }

        public function deleteRepuestos(){
            $params = (object)json_decode(file_get_contents("php://input"));

            $sql = "DELETE FROM cotizador_detalle WHERE id = '{$params->id_cotizador}' AND id_cotizacion = '{$params->id_cotizacion}'";
            echo $sql;
            $this->conexion->query($sql);
        }
    //END RESPUESTOS

    //MANO DE OBRA
        public function detalleMano(){
            $filters = (object)json_decode(file_get_contents("php://input"));
            $sWhere = ""; 

            $sql = "SELECT 
                        cat_areas_materiales.nombre AS item,
                        cat_materiales.item AS descripcion,
                        cat_materiales.descripcion AS unidad,
                        cotizador_detalle.cantidad AS cantidad, 
                        cotizador_detalle.precio_unit,
                        cotizador_detalle.total AS total,
                        cotizador_detalle.id
                    FROM cotizador_detalle 
                        INNER JOIN cat_materiales ON cat_materiales.id = cotizador_detalle.id_item_mano
                        INNER JOIN cat_areas_materiales ON cat_areas_materiales.id = cat_materiales.id_area
                    WHERE cotizador_detalle.id_area = '{$filters->id_area}' AND cotizador_detalle.id_cotizacion = '{$filters->id_cotizacion}' 
                    AND (cat_materiales.id_area = 8 OR cat_materiales.id_area = 7)
                    GROUP BY cotizador_detalle.precio_unit,cotizador_detalle.id_item_mano,cat_materiales.id_area";
            $response->detalle_mano = $this->conexion->queryAll($sql);
            
            $total_sum_mano = 0;
            foreach($response->detalle_mano as $row){
                $total_sum_mano += $row->total;
            }
            $response->sum_total = $total_sum_mano;
            return json_encode($response);
        }

        public function areaMano(){
            $filters = (object)json_decode(file_get_contents("php://input"));
            $sWhere = ""; 

            $sql = "SELECT id AS id, nombre AS label FROM cat_areas_materiales WHERE id = 7 OR id = 8";
            $response->area_mano = $this->conexion->queryAllSpecial($sql);
            return json_encode($response);
        }

        public function descripcionMano(){
            $filters = (object)json_decode(file_get_contents("php://input"));
            $sWhere = "";
            if($filters->area_mano_id != ""){
                $sWhere .= " AND id_area = {$filters->area_mano_id}";
            }

            $sql = "SELECT id AS id, item AS label FROM cat_materiales WHERE 1 = 1 $sWhere";
            $response->descripcion_mano = $this->conexion->queryAllSpecial($sql);
            return json_encode($response);
        }

        public function precioUnitMano(){
            $filters = (object)json_decode(file_get_contents("php://input"));
            $sWhere = "";
            if($filters->descripcion_id){
                $sWhere .= " AND id = {$filters->descripcion_id}";
            }
            
            $sql = "SELECT IF(precio = '',0.00,precio) AS precio FROM cat_materiales WHERE 1 = 1 $sWhere";
            $response = $this->conexion->queryRow($sql);
            return json_encode($response);
        }

        public function unidadMano(){
            $filters = (object)json_decode(file_get_contents("php://input"));
            $sWhere = "";
            if($filters->descripcion_id){
                $sWhere .= " AND id = {$filters->descripcion_id}";
            }
            
            $sql = "SELECT IF(descripcion IS NULL,'',descripcion) AS unidad FROM cat_materiales WHERE 1 = 1 $sWhere";
            $response = $this->conexion->queryRow($sql);
            return json_encode($response);
        }

        public function insertMano(){
            $params = (object)json_decode(file_get_contents("php://input"));


            $id_cotizacion = "";

            if($params->id_cotizacion == 0){
                $sql = "SELECT MAX(id) AS cot FROM cotizaciones";
                $cot = $this->conexion->queryRow($sql)->cot;

                $id_cotizacion = $cot + 1;

            } else {
                $id_cotizacion = $params->id_cotizacion;
            }
            
            $sql = "SELECT COUNT(1) AS cont FROM cotizador_areas WHERE id_cotizacion = '{$id_cotizacion}' AND nombre = '{$params->nombre_area}'";
            $count = $this->conexion->query($sql)->cont;

            if($count == 0){
                $sql = "INSERT INTO cotizador_areas
                        SET 
                            id_cotizacion = '{$id_cotizacion}',
                            nombre = '{$params->nombre_area}'";
                $this->conexion->query($sql);
                $id_area = $this->conexion->consultas(1, $sql);
            } else {
                $id_area = $params->id_area;
            }

            $total = $params->cantidad* $params->precio;
            $sql = "INSERT INTO cotizador_detalle 
                    SET 
                        id_cotizacion = '{$id_cotizacion}',
                        id_item_mano = '{$params->descripcion_id}', 
                        fecha = CURRENT_DATE, 
                        hora = CURRENT_TIME, 
                        cantidad = '{$params->cantidad}', 
                        precio_unit = '{$params->precio}', 
                        total = '{$total}',
                        id_area = '{$id_area}',
                        tipo = 'MANO'";
            $this->conexion->query($sql);
        }

        public function deleteMano(){
            $params = (object)json_decode(file_get_contents("php://input"));

            $sql = "DELETE FROM cotizador_detalle WHERE id = '{$params->id_cotizador}' AND id_cotizacion = '{$params->id_cotizacion}'";
            echo $sql;
            $this->conexion->query($sql);
        }

    //END MANO DE OBRA
}