<?php

// include 'class.sesion.php';

#global 	$session = Session::getInstance();

class M_Conexion { 
	private $host="localhost";
    private $db_user="auditoriasbonita";
    private $db_pass="u[V(fTIUbcVb";
    private $db_name="cegaservices2";
    public $link; 

    public $insert = 1;
	public $selects = 2;

    public function __construct($db = null){
		$db_name = $db != null ? $db : $this->db_name;
        $this->link = new mysqli($this->host, $this->db_user, $this->db_pass, $db_name);
        if ( $this->link->connect_errno ) 
        { 
            echo "Fallo al conectar a MySQL: ". $this->link->connect_error; 
            return;
        }
        if (!mysqli_set_charset($this->link, "utf8")) {
	    	echo "Error al cargar utf8";
	    	return;
		}
    }

	public function query($sql){
		$this->link->query($sql);
	}

	public function getLastID(){
		return $this->link->insert_id;
	}

	public function queryAll($sql, $isArray = false){
		$sql = trim($sql);
		if($sql != ""){
			$res = $this->link->query($sql);
			$data = array();
			while($fila = $res->fetch_assoc()){
				if($isArray){
					$data[] = $fila;
				}else{
					$data[] = (object) $fila;
				}				
			}
			return $data;
		}
		return false;
	}

	public function queryRow($sql){
		$sql = trim($sql);
		if($sql != ""){
			$res = $this->link->query($sql);
			$data = (object)[];
			while($fila = $res->fetch_assoc()){
				$data = (object)$fila;
				break;
			}
			return $data;
		}
		return false;
	}
	
	public function Consultas($tipo,$sql){
		if($tipo == 1){
			if($this->link->query($sql)){
				$ids=$this->link->insert_id;
				return $ids;
			}else{
				print_r($this->link->error);
			}
		}
		else if($tipo == 2){
			$res = $this->link->query($sql);
			$datos = array();
			while($fila = $res->fetch_assoc()){
				$datos[] = $fila;
			}
			return $datos;
		}
	}

	public function queryAllSpecial($sql){
		$response = [];
		if($sql != ""){
			if($res = $this->link->query($sql)){
                while($row = $res->fetch_assoc()){
                    $row = (object) $row;
                    if(isset($row->id) && isset($row->label))
                        $response[$row->id] = $row->label;
                }
            }else{
                print_r($this->link->error);
            }
		}
		return $response;
	}
	
}

?>