<?php
/**
* 
*/
class reporte
{
    
    private $conexion;
    private $session;
    
    public function __construct(){
        $this->conexion = new M_Conexion;
        $this->session = Session::getInstance();
    }

    public function index(){
		$response = (object)[
            "success" => 400,
            "data" => ""
        ];

		$response->tipoCliente = $this->obtenerInformacion();

		$datos=[];
		$sql = "SELECT cat_equiposTipo.*, (SELECT COUNT(*) FROM cat_equipos
				join cat_clientes on cat_clientes.id = cat_equipos.id_cliente			 
				WHERE cat_equipos.id_tipo_equipo = cat_equiposTipo.id AND cat_clientes.status=1) AS equipos,
				(SELECT COUNT(*) FROM orden_trabajo WHERE orden_trabajo.status = 3) AS ordenes FROM cat_equiposTipo;";
		$res = $this->conexion->link->query($sql);
		$suma = 0;
		while($fila = $res->fetch_assoc()){
            $fila = (object)$fila;
			$datos2 = [];
			$sql = "SELECT id, nombre FROM cat_descripciones_equipos WHERE STATUS=1 AND id_tipoequipo =$fila->id";
			$res2 = $this->conexion->link->query($sql);
			while($fila2 = $res2->fetch_assoc()){
				$fila2 = (object)$fila2;
				$sql = "SELECT COUNT(*) as equipos FROM cat_equipos WHERE cat_equipos.id_tipo_equipo = $fila->id AND id_descripcion_equipo=$fila2->id;";
				$res3 = $this->conexion->link->query($sql);
				$res3 = $res3->fetch_object();
				$datos2[] = [
					"id" => $fila2->id,
					"nombre" => $fila2->nombre,
					"equipos" => $res3->equipos
				];						
			}
            $datos[] = [
             	"id" => $fila->id,
             	"nombre" => $fila->nombre,
             	"equipos" => $fila->equipos,
				"descripciones" => $datos2,
            ];
		}
		/* Ordenes Climatizacion A */
		$sql = "SELECT COUNT(*)AS total FROM man_caracteristicas_climatizacion_a
				JOIN orden_trabajo ON orden_trabajo.id = man_caracteristicas_climatizacion_a.id_orden
				WHERE man_caracteristicas_climatizacion_a.status=1 AND orden_trabajo.status = 3 
				GROUP BY man_caracteristicas_climatizacion_a.id_orden;";
        $res = $this->conexion->link->query($sql);
		$suma = $suma + $res->num_rows;
			
		$sql = "SELECT COUNT(*)AS total FROM cor_caracteristicas_climatizacion_a
				JOIN orden_trabajo ON orden_trabajo.id = cor_caracteristicas_climatizacion_a.id_orden
				WHERE cor_caracteristicas_climatizacion_a.status=1 AND orden_trabajo.status = 3 
				GROUP BY cor_caracteristicas_climatizacion_a.id_orden;";
        $res = $this->conexion->link->query($sql);
		$suma = $suma + $res->num_rows;
			
		$sql = "SELECT COUNT(*)AS total FROM ins_caracteristicas_climatizacion_a
				JOIN orden_trabajo ON orden_trabajo.id = ins_caracteristicas_climatizacion_a.id_orden
				WHERE ins_caracteristicas_climatizacion_a.status=1 AND orden_trabajo.status = 3 
				GROUP BY ins_caracteristicas_climatizacion_a.id_orden;";
        $res = $this->conexion->link->query($sql);
		$suma = $suma + $res->num_rows;
			
		$sql = "SELECT COUNT(*)AS total FROM rev_caracteristicas_climatizacion_a
				JOIN orden_trabajo ON orden_trabajo.id = rev_caracteristicas_climatizacion_a.id_orden
				WHERE rev_caracteristicas_climatizacion_a.status=1 AND orden_trabajo.status = 3 
				GROUP BY rev_caracteristicas_climatizacion_a.id_orden;";
        $res = $this->conexion->link->query($sql);
		$suma = $suma + $res->num_rows;
			
		$datos[0]['ordenes'] = $suma;
		
		$suma=0;

					/* Ordenes Climatizacion B */
		$sql = "SELECT COUNT(*)AS total FROM man_caracteristicas_climatizacion_b
				JOIN orden_trabajo ON orden_trabajo.id = man_caracteristicas_climatizacion_b.id_orden
				WHERE man_caracteristicas_climatizacion_b.status=1 AND orden_trabajo.status = 3 
				GROUP BY man_caracteristicas_climatizacion_b.id_orden;";
        $res = $this->conexion->link->query($sql);
		$suma = $suma + $res->num_rows;
		
		$sql = "SELECT COUNT(*)AS total FROM cor_caracteristicas_climatizacion_b
				JOIN orden_trabajo ON orden_trabajo.id = cor_caracteristicas_climatizacion_b.id_orden
				WHERE cor_caracteristicas_climatizacion_b.status=1 AND orden_trabajo.status = 3 
				GROUP BY cor_caracteristicas_climatizacion_b.id_orden;";
        $res = $this->conexion->link->query($sql);
		$suma = $suma + $res->num_rows;
		
		$sql = "SELECT COUNT(*)AS total FROM ins_caracteristicas_climatizacion_b
				JOIN orden_trabajo ON orden_trabajo.id = ins_caracteristicas_climatizacion_b.id_orden
				WHERE ins_caracteristicas_climatizacion_b.status=1 AND orden_trabajo.status = 3 
				GROUP BY ins_caracteristicas_climatizacion_b.id_orden;";
        $res = $this->conexion->link->query($sql);
		$suma = $suma + $res->num_rows;
		
		$sql = "SELECT COUNT(*)AS total FROM rev_caracteristicas_climatizacion_b
				JOIN orden_trabajo ON orden_trabajo.id = rev_caracteristicas_climatizacion_b.id_orden
				WHERE rev_caracteristicas_climatizacion_b.status=1 AND orden_trabajo.status = 3 
				GROUP BY rev_caracteristicas_climatizacion_b.id_orden;";
        $res = $this->conexion->link->query($sql);
		$suma = $suma + $res->num_rows;
		
		$datos[1]['ordenes'] = $suma;
		
		$suma=0;
		/* Ordenes Ventilacion */
		$sql = "SELECT COUNT(*)AS total FROM man_caracteristicas_ventilacion
				JOIN orden_trabajo ON orden_trabajo.id = man_caracteristicas_ventilacion.id_orden
				WHERE man_caracteristicas_ventilacion.status=1 AND orden_trabajo.status = 3 
				GROUP BY man_caracteristicas_ventilacion.id_orden;";
        $res = $this->conexion->link->query($sql);
		$suma = $suma + $res->num_rows;
		
		$sql = "SELECT COUNT(*)AS total FROM cor_caracteristicas_ventilacion
				JOIN orden_trabajo ON orden_trabajo.id = cor_caracteristicas_ventilacion.id_orden
				WHERE cor_caracteristicas_ventilacion.status=1 AND orden_trabajo.status = 3 
				GROUP BY cor_caracteristicas_ventilacion.id_orden;";
        $res = $this->conexion->link->query($sql);
		$suma = $suma + $res->num_rows;
		
		$sql = "SELECT COUNT(*)AS total FROM ins_caracteristicas_ventilacion
				JOIN orden_trabajo ON orden_trabajo.id = ins_caracteristicas_ventilacion.id_orden
				WHERE ins_caracteristicas_ventilacion.status=1 AND orden_trabajo.status = 3 
				GROUP BY ins_caracteristicas_ventilacion.id_orden;";
        $res = $this->conexion->link->query($sql);
		$suma = $suma + $res->num_rows;
		
		$sql = "SELECT COUNT(*)AS total FROM rev_caracteristicas_ventilacion
				JOIN orden_trabajo ON orden_trabajo.id = rev_caracteristicas_ventilacion.id_orden
				WHERE rev_caracteristicas_ventilacion.status=1 AND orden_trabajo.status = 3 
				GROUP BY rev_caracteristicas_ventilacion.id_orden;";
        $res = $this->conexion->link->query($sql);
		$suma = $suma + $res->num_rows;
		
		$datos[2]['ordenes'] = $suma;
		
		$suma=0;
					/* Ordenes Refrigeracion */
		$sql = "SELECT COUNT(*)AS total FROM man_caracteristicas_refrigeracion
				JOIN orden_trabajo ON orden_trabajo.id = man_caracteristicas_refrigeracion.id_orden
				WHERE man_caracteristicas_refrigeracion.status=1 AND orden_trabajo.status = 3 
				GROUP BY man_caracteristicas_refrigeracion.id_orden;";
        $res = $this->conexion->link->query($sql);
		$suma = $suma + $res->num_rows;
		
		$sql = "SELECT COUNT(*)AS total FROM cor_caracteristicas_refrigeracion
				JOIN orden_trabajo ON orden_trabajo.id = cor_caracteristicas_refrigeracion.id_orden
				WHERE cor_caracteristicas_refrigeracion.status=1 AND orden_trabajo.status = 3 
				GROUP BY cor_caracteristicas_refrigeracion.id_orden;";
        $res = $this->conexion->link->query($sql);
		$suma = $suma + $res->num_rows;
		
		$sql = "SELECT COUNT(*)AS total FROM ins_caracteristicas_refrigeracion
				JOIN orden_trabajo ON orden_trabajo.id = ins_caracteristicas_refrigeracion.id_orden
				WHERE ins_caracteristicas_refrigeracion.status=1 AND orden_trabajo.status = 3 
				GROUP BY ins_caracteristicas_refrigeracion.id_orden;";
        $res = $this->conexion->link->query($sql);
		$suma = $suma + $res->num_rows;
		
		$sql = "SELECT COUNT(*)AS total FROM rev_caracteristicas_refrigeracion
				JOIN orden_trabajo ON orden_trabajo.id = rev_caracteristicas_refrigeracion.id_orden
				WHERE rev_caracteristicas_refrigeracion.status=1 AND orden_trabajo.status = 3 
				GROUP BY rev_caracteristicas_refrigeracion.id_orden;";
        $res = $this->conexion->link->query($sql);
		$suma = $suma + $res->num_rows;
		
		$datos[3]['ordenes'] = $suma;
		
		$suma=0;

		$sql = "SELECT id, nombre, color, status, 'Supervisor' as type FROM cat_supervisores WHERE status = 1
				UNION ALL
				SELECT id, nombre, color, status, 'Asistente' as type FROM cat_auxiliares WHERE status = 1";
		$res = $this->conexion->link->query($sql);
		$response->supervisores = [];

		while($fila = $res->fetch_assoc()){
			$response->supervisores[] = $fila;
		}

		$response->tipoEquipo = $datos;
		return json_encode($response);
    }

	private function obtenerInformacion() {
		$infoSql = "SELECT 
						cct.id as idTipo,
						cct.nombre as nombreTipo,
						cc.id as idCliente,
						cc.nombre as nombreCliente,
						ROUND(AVG(IFNULL(IFNULL(cumplidas,0)/IFNULL(totales,0), 0))*100 ,2) as cumplimiento
					FROM orden_trabajo ot
					INNER JOIN cotizaciones ON ot.id_cotizacion = cotizaciones.id
					INNER JOIN cat_clientes cc ON cc.id = cotizaciones.id_cliente
					INNER JOIN cat_clientesTipo cct ON cct.id = cc.id_tipcli
					
					LEFT JOIN (SELECT id_orden, COUNT(*) as cumplidas FROM (
							(SELECT id_orden, 'correctivo' AS tipoDato FROM `reportes_r_correctivo`)
							UNION ALL 
							(SELECT id_orden, 'instalacion' AS tipoDato FROM `reportes_r_instalacion` ORDER BY order_time)
							UNION ALL
							(SELECT id_orden, 'revision_mantenimiento' AS tipoDato FROM `reportes_revision_mantenimiento`)
							UNION ALL
							(SELECT id_orden, 'mantenimiento' AS tipoDato FROM `reportes_r_mantenimiento` ORDER BY order_time)
							UNION ALL
							(SELECT id_orden, 'revision_instalacion' AS tipoDato FROM `reportes_revision_instalacion`)
							UNION ALL
							(SELECT id_orden, 'revision_correctivo' AS tipoDato FROM `reportes_revision_correctivo`)
						) AS tab1 
						GROUP BY id_orden) as cumplidas ON cumplidas.id_orden = ot.id
					
					LEFT JOIN (SELECT id_orden, COUNT(*) as totales FROM (
						#mantenimiento
						(SELECT 'MANTENIMIENTO' AS tipo_trabajo, CONCAT('MANTENIMIENTO - ', id_orden) AS tipo_id_orden, CONCAT(man_caracteristicas_climatizacion_a.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.`nombre`,' - ',man_caracteristicas_climatizacion_a.codigo,' - ',IF(marca IS NULL, '', marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM man_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE man_caracteristicas_climatizacion_a.status = 1 HAVING reg IS NOT NULL) 
						UNION 
						(SELECT 'MANTENIMIENTO' AS tipo_trabajo, CONCAT('MANTENIMIENTO - ', id_orden) AS tipo_id_orden, CONCAT(man_caracteristicas_climatizacion_b.`codigo`, '-', id_cliente) AS codigo_cliente,  id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.`nombre`,' - ',man_caracteristicas_climatizacion_b.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM man_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE man_caracteristicas_climatizacion_b.status = 1 HAVING reg IS NOT NULL)
						UNION
						(SELECT 'MANTENIMIENTO' AS tipo_trabajo, CONCAT('MANTENIMIENTO - ', id_orden) AS tipo_id_orden, CONCAT(man_caracteristicas_refrigeracion.`codigo`, '-', id_cliente) AS codigo_cliente,  id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.`nombre`,' - ',man_caracteristicas_refrigeracion.codigo,' - ',IF(marca_eva IS NULL,'',marca_eva),' - ',IF(capacidad_hp_eva IS NULL,'',capacidad_hp_eva),' - ',IF(modelo_eva IS NULL,'',modelo_eva),' - ',IF(serie_eva IS NULL,'',serie_eva)) AS reg FROM man_caracteristicas_refrigeracion INNER JOIN cat_areas ON id_area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON id_equipo_des = cat_descripciones_equipos.`id` WHERE man_caracteristicas_refrigeracion.status = 1 HAVING reg IS NOT NULL)
						UNION
						(SELECT 'MANTENIMIENTO' AS tipo_trabajo, CONCAT('MANTENIMIENTO - ', id_orden) AS tipo_id_orden, CONCAT(man_caracteristicas_ventilacion.`codigo`, '-', id_cliente) AS codigo_cliente,  id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.`nombre`,' - ',man_caracteristicas_ventilacion.codigo,' - ',IF(marca,'',marca),' - ',IF(capacidadHP IS NULL,'',capacidadHP),' - ',IF(capacidadCFN IS NULL,'',capacidadCFN),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM man_caracteristicas_ventilacion INNER JOIN cat_areas ON area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE man_caracteristicas_ventilacion.status = 1 HAVING reg IS NOT NULL)
						
						#instalacion
						UNION
						(SELECT 'INSTALACION' AS tipo_trabajo, CONCAT('INSTALACION - ', id_orden) AS tipo_id_orden, CONCAT(ins_caracteristicas_climatizacion_a.`codigo`, '-', id_cliente) AS codigo_cliente,  id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.`nombre`,' - ',ins_caracteristicas_climatizacion_a.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie,'',serie)) AS reg FROM ins_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE ins_caracteristicas_climatizacion_a.status = 1 HAVING reg IS NOT NULL) 
						UNION 
						(SELECT 'INSTALACION' AS tipo_trabajo, CONCAT('INSTALACION - ', id_orden) AS tipo_id_orden,  CONCAT(ins_caracteristicas_climatizacion_b.`codigo`, '-', id_cliente) AS codigo_cliente,  id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.`nombre`,' - ',ins_caracteristicas_climatizacion_b.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM ins_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE ins_caracteristicas_climatizacion_b.status = 1 HAVING reg IS NOT NULL)
						UNION
						(SELECT 'INSTALACION' AS tipo_trabajo, CONCAT('INSTALACION - ', id_orden) AS tipo_id_orden,  CONCAT(ins_caracteristicas_refrigeracion.`codigo`, '-', id_cliente) AS codigo_cliente,  id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.`nombre`,' - ',ins_caracteristicas_refrigeracion.codigo,' - ',IF(marca_eva IS NULL,'',marca_eva),' - ',IF(modelo_eva IS NULL,'',modelo_eva),' - ',IF(serie_eva IS NULL,'',serie_eva),' - '/*,IF(capacidad_hp_eva IS NULL,'',capacidad_hp_eva),' - ',IF(compresorCantidad IS NULL,'',compresorCantidad),' - ',IF(ventiladorCantidad IS NULL,'',ventiladorCantidad)*/) AS reg FROM ins_caracteristicas_refrigeracion INNER JOIN cat_areas ON id_area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON id_equipo_des = cat_descripciones_equipos.`id` WHERE ins_caracteristicas_refrigeracion.status = 1 HAVING reg IS NOT NULL)
						UNION
						(SELECT 'INSTALACION' AS tipo_trabajo, CONCAT('INSTALACION - ', id_orden) AS tipo_id_orden,  CONCAT(ins_caracteristicas_ventilacion.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.`nombre`,' - ',ins_caracteristicas_ventilacion.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadHP IS NULL,'',capacidadHP),' - ',IF(capacidadCFN IS NULL,'',capacidadCFN),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM ins_caracteristicas_ventilacion INNER JOIN cat_areas ON area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE ins_caracteristicas_ventilacion.status = 1 HAVING reg IS NOT NULL)
						
						#correctivo
						UNION
						(SELECT 'CORRECTIVO' AS tipo_trabajo, CONCAT('CORRECTIVO - ', id_orden) AS tipo_id_orden, CONCAT(cor_caracteristicas_climatizacion_a.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.nombre,' - ',cor_caracteristicas_climatizacion_a.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM cor_caracteristicas_climatizacion_a INNER JOIN cat_areas ON area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE cor_caracteristicas_climatizacion_a.status = 1 HAVING reg IS NOT NULL) 
						UNION 
						(SELECT 'CORRECTIVO' AS tipo_trabajo, CONCAT('CORRECTIVO - ', id_orden) AS tipo_id_orden, CONCAT(cor_caracteristicas_climatizacion_b.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.nombre,' - ',cor_caracteristicas_climatizacion_b.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM cor_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = area INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE cor_caracteristicas_climatizacion_b.status = 1 HAVING reg IS NOT NULL)
						UNION
						(SELECT 'CORRECTIVO' AS tipo_trabajo, CONCAT('CORRECTIVO - ', id_orden) AS tipo_id_orden, CONCAT(cor_caracteristicas_refrigeracion.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.nombre,' - ',cor_caracteristicas_refrigeracion.`codigo`,' - ',IF(marca_eva IS NULL,'',marca_eva),' - ',IF(modelo_eva IS NULL,'',modelo_eva),' - ',IF(serie_eva IS NULL,'',serie_eva),' - '/*,IF(compresorBTU IS NULL,'',compresorBTU),' - ',IF(compresorCantidad IS NULL,'',compresorCantidad),' - ',IF(ventiladorCantidad IS NULL,'',ventiladorCantidad)*/) AS reg FROM cor_caracteristicas_refrigeracion INNER JOIN cat_areas ON id_area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON id_equipo_des = cat_descripciones_equipos.`id` WHERE cor_caracteristicas_refrigeracion.status = 1 HAVING reg IS NOT NULL)
						UNION
						(SELECT 'CORRECTIVO' AS tipo_trabajo, CONCAT('CORRECTIVO - ', id_orden) AS tipo_id_orden, CONCAT(cor_caracteristicas_ventilacion.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.nombre,' - ',cor_caracteristicas_ventilacion.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadHP IS NULL,'',capacidadHP),' - ',IF(capacidadCFN IS NULL,'',capacidadHP),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM cor_caracteristicas_ventilacion INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE cor_caracteristicas_ventilacion.status = 1 HAVING reg IS NOT NULL)
						
						#revision
						UNION
						(SELECT 'REVISION' AS tipo_trabajo, CONCAT('REVISION - ', id_orden) AS tipo_id_orden, CONCAT(rev_caracteristicas_climatizacion_a.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.nombre,' - ',rev_caracteristicas_climatizacion_a.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM rev_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE rev_caracteristicas_climatizacion_a.status = 1) 
						UNION 
						(SELECT 'REVISION' AS tipo_trabajo, CONCAT('REVISION - ', id_orden) AS tipo_id_orden, CONCAT(rev_caracteristicas_climatizacion_b.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.nombre,' - ',rev_caracteristicas_climatizacion_b.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM rev_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE rev_caracteristicas_climatizacion_b.status = 1)
						UNION
						(SELECT 'REVISION' AS tipo_trabajo, CONCAT('REVISION - ', id_orden) AS tipo_id_orden, CONCAT(rev_caracteristicas_refrigeracion.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.nombre,' - ',rev_caracteristicas_refrigeracion.`codigo`,' - ',IF(marca_eva IS NULL,'',marca_eva),' - ',IF(modelo_eva IS NULL,'',modelo_eva),' - ',IF(serie_eva IS NULL,'',serie_eva),' - '/*,IF(compresorBTU IS NULL,'',compresorBTU),' - ',IF(compresorCantidad IS NULL,'',compresorCantidad),' - ',IF(ventiladorCantidad IS NULL,'',ventiladorCantidad)*/) AS reg FROM rev_caracteristicas_refrigeracion INNER JOIN cat_areas ON id_area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON id_equipo_des = cat_descripciones_equipos.`id` WHERE rev_caracteristicas_refrigeracion.status = 1)
						UNION
						(SELECT 'REVISION' AS tipo_trabajo, CONCAT('REVISION - ', id_orden) AS tipo_id_orden, CONCAT(rev_caracteristicas_ventilacion.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.nombre,' - ',rev_caracteristicas_ventilacion.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadHP IS NULL,'',capacidadHP),' - ',IF(capacidadCFN IS NULL,'',capacidadHP),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM rev_caracteristicas_ventilacion INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE rev_caracteristicas_ventilacion.status = 1)
						
					) AS tb 
					GROUP BY id_orden
					) as totales ON totales.id_orden = ot.id
					WHERE ot.status = 3 AND cc.status = 1
					GROUP BY cct.id, cct.nombre, cc.id, cc.nombre;";

		$res = $this->conexion->queryAll($infoSql);
		$info = [];
		foreach ($res as $fila) {
			if (array_key_exists($fila->idTipo, $info)) {
				$cliente = [
					"id" => $fila->idCliente,
					"nombre" => $fila->nombreCliente,
					"cumplimiento" => $fila->cumplimiento,
					"efectividad" => $this->getEfectividad("CURRENT_DATE", "", null, $fila->idCliente),
					"total" => 1,
				];
				array_push($info[$fila->idTipo]["clientes"], $cliente);
			} else {
				$cliente = [
					"id" => $fila->idCliente,
					"nombre" => $fila->nombreCliente,
					"cumplimiento" => $fila->cumplimiento,
					"efectividad" => $this->getEfectividad("CURRENT_DATE", "", null, $fila->idCliente),
					"total" => 1,
				];
				$tipo = [
					"id" => $fila->idTipo,
					"nombre" => $fila->nombreTipo,
					"clientes" => [],
				];
				array_push($tipo["clientes"], $cliente);
				$info[$fila->idTipo] = $tipo;
			}
		}

		foreach ($info as $tipo) {
			$info[$tipo["id"]]["total"] = count($tipo["clientes"]);
			$info[$tipo["id"]]["cumplimiento"] = $this->getCumplimientoNew("CURRENT_DATE", "", $tipo["id"]);
			$info[$tipo["id"]]["efectividad"] = $this->getEfectividad("CURRENT_DATE", "", $tipo["id"]);
		}

		return $info;
	}

    public function getEquiposByClient(){
		$postdata = (object)json_decode(file_get_contents("php://input"));
 		$response = (object)[
            "success" => 400,
            "data" => ""
        ];
		$datos=[];
		$sql = "SELECT cat_equiposTipo.*, (SELECT COUNT(*) FROM cat_equipos
				JOIN cat_clientes ON cat_clientes.id = cat_equipos.id_cliente			 
				WHERE cat_equipos.id_tipo_equipo = cat_equiposTipo.id AND cat_clientes.status=1 AND cat_clientes.id = $postdata->id) AS equipos,
				(SELECT COUNT(*) FROM orden_trabajo WHERE orden_trabajo.status = 3) AS ordenes FROM cat_equiposTipo;";

		$res = $this->conexion->link->query($sql);
		$suma = 0;
		while($fila = $res->fetch_assoc()){
			$fila = (object)$fila;
			$datos2 = [];
			$sql = "SELECT id, nombre FROM cat_descripciones_equipos WHERE STATUS=1 AND id_tipoequipo =$fila->id";
			$res2 = $this->conexion->link->query($sql);
			while($fila2 = $res2->fetch_assoc()){
				$fila2 = (object)$fila2;
				$sql = "SELECT COUNT(*) as equipos FROM cat_equipos 
						WHERE cat_equipos.id_tipo_equipo = $fila->id AND id_descripcion_equipo=$fila2->id and cat_equipos.id_cliente=$postdata->id;";
				$res3 = $this->conexion->link->query($sql);
				$res3 = $res3->fetch_object();
				$suma3=0;
				if($fila->id == 1){
					/* Ordenes Climatizacion A */
					$sql = "SELECT COUNT(*)AS total FROM man_caracteristicas_climatizacion_a
							JOIN orden_trabajo ON orden_trabajo.id = man_caracteristicas_climatizacion_a.id_orden
							JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
							WHERE man_caracteristicas_climatizacion_a.status=1 AND orden_trabajo.status = 3 
							AND cotizaciones.id_cliente = '$postdata->id' AND cat_equipos.id_descripcion_equipo = $fila2->id
							GROUP BY man_caracteristicas_climatizacion_a.id_orden;";
					$res5 = $this->conexion->link->query($sql);
					$suma3 = $suma3 + $res5->num_rows;
			
					$sql = "SELECT COUNT(*)AS total FROM cor_caracteristicas_climatizacion_a
							JOIN orden_trabajo ON orden_trabajo.id = cor_caracteristicas_climatizacion_a.id_orden
							JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
							WHERE cor_caracteristicas_climatizacion_a.status=1 AND orden_trabajo.status = 3 
							AND cotizaciones.id_cliente = '$postdata->id' AND cat_equipos.id_descripcion_equipo = $fila2->id
							GROUP BY cor_caracteristicas_climatizacion_a.id_orden;";
					$res5 = $this->conexion->link->query($sql);
					$suma3 = $suma3 + $res5->num_rows;
			
					$sql = "SELECT COUNT(*)AS total FROM ins_caracteristicas_climatizacion_a
							JOIN orden_trabajo ON orden_trabajo.id = ins_caracteristicas_climatizacion_a.id_orden
							JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
							WHERE ins_caracteristicas_climatizacion_a.status=1 AND orden_trabajo.status = 3 
							AND cotizaciones.id_cliente = '$postdata->id' AND cat_equipos.id_descripcion_equipo = $fila2->id
							GROUP BY ins_caracteristicas_climatizacion_a.id_orden;";
					$res5 = $this->conexion->link->query($sql);
					$suma3 = $suma3 + $res5->num_rows;
			
					$sql = "SELECT COUNT(*)AS total FROM rev_caracteristicas_climatizacion_a
							JOIN orden_trabajo ON orden_trabajo.id = rev_caracteristicas_climatizacion_a.id_orden
							JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
							WHERE rev_caracteristicas_climatizacion_a.status=1 AND orden_trabajo.status = 3 
							AND cotizaciones.id_cliente = '$postdata->id' AND cat_equipos.id_descripcion_equipo = $fila2->id
							GROUP BY rev_caracteristicas_climatizacion_a.id_orden;";
					$res5 = $this->conexion->link->query($sql);
					$suma3 = $suma3 + $res5->num_rows;
				}
				$datos2[] = [
					"id" => $fila2->id,
					"nombre" => $fila2->nombre,
					"equipos" => $res3->equipos,
					"ordenes" => $suma3
				];						
			}
            $datos[] = [
             	"id" => $fila->id,
             	"nombre" => $fila->nombre,
             	"equipos" => $fila->equipos,
				"descripciones" => $datos2,
            ];
		}
		
		/* Ordenes Climatizacion A */
		$sql = "SELECT COUNT(*)AS total FROM man_caracteristicas_climatizacion_a
				JOIN orden_trabajo ON orden_trabajo.id = man_caracteristicas_climatizacion_a.id_orden
				JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
				WHERE man_caracteristicas_climatizacion_a.status=1 AND orden_trabajo.status = 3 
				AND cotizaciones.id_cliente = '$postdata->id'
				GROUP BY man_caracteristicas_climatizacion_a.id_orden;";
		$res = $this->conexion->link->query($sql);
		$suma = $suma + $res->num_rows;
			
		$sql = "SELECT COUNT(*)AS total FROM cor_caracteristicas_climatizacion_a
				JOIN orden_trabajo ON orden_trabajo.id = cor_caracteristicas_climatizacion_a.id_orden
				JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
				WHERE cor_caracteristicas_climatizacion_a.status=1 AND orden_trabajo.status = 3 
				AND cotizaciones.id_cliente = '$postdata->id'
				GROUP BY cor_caracteristicas_climatizacion_a.id_orden;";
		$res = $this->conexion->link->query($sql);
		$suma = $suma + $res->num_rows;
			
		$sql = "SELECT COUNT(*)AS total FROM ins_caracteristicas_climatizacion_a
				JOIN orden_trabajo ON orden_trabajo.id = ins_caracteristicas_climatizacion_a.id_orden
				JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
				WHERE ins_caracteristicas_climatizacion_a.status=1 AND orden_trabajo.status = 3 
				AND cotizaciones.id_cliente = '$postdata->id'
				GROUP BY ins_caracteristicas_climatizacion_a.id_orden;";
		$res = $this->conexion->link->query($sql);
		$suma = $suma + $res->num_rows;
			
		$sql = "SELECT COUNT(*)AS total FROM rev_caracteristicas_climatizacion_a
				JOIN orden_trabajo ON orden_trabajo.id = rev_caracteristicas_climatizacion_a.id_orden
				JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
				WHERE rev_caracteristicas_climatizacion_a.status=1 AND orden_trabajo.status = 3 
				AND cotizaciones.id_cliente = '$postdata->id'
				GROUP BY rev_caracteristicas_climatizacion_a.id_orden;";
		$res = $this->conexion->link->query($sql);
		$suma = $suma + $res->num_rows;
			
		$datos[0]['ordenes'] = $suma;
			
		$suma=0;

		/* Ordenes Climatizacion B */
		$sql = "SELECT COUNT(*)AS total FROM man_caracteristicas_climatizacion_b
				JOIN orden_trabajo ON orden_trabajo.id = man_caracteristicas_climatizacion_b.id_orden
				JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
				WHERE man_caracteristicas_climatizacion_b.status=1 AND orden_trabajo.status = 3 
				AND cotizaciones.id_cliente = '$postdata->id'
				GROUP BY man_caracteristicas_climatizacion_b.id_orden;";
		$res = $this->conexion->link->query($sql);
		$suma = $suma + $res->num_rows;
			
		$sql = "SELECT COUNT(*)AS total FROM cor_caracteristicas_climatizacion_b
				JOIN orden_trabajo ON orden_trabajo.id = cor_caracteristicas_climatizacion_b.id_orden
				JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
				WHERE cor_caracteristicas_climatizacion_b.status=1 AND orden_trabajo.status = 3 
				AND cotizaciones.id_cliente = '$postdata->id'
				GROUP BY cor_caracteristicas_climatizacion_b.id_orden;";
		$res = $this->conexion->link->query($sql);
		$suma = $suma + $res->num_rows;
			
		$sql = "SELECT COUNT(*)AS total FROM ins_caracteristicas_climatizacion_b
				JOIN orden_trabajo ON orden_trabajo.id = ins_caracteristicas_climatizacion_b.id_orden
				JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
				WHERE ins_caracteristicas_climatizacion_b.status=1 AND orden_trabajo.status = 3 
				AND cotizaciones.id_cliente = '$postdata->id'
				GROUP BY ins_caracteristicas_climatizacion_b.id_orden;";
		$res = $this->conexion->link->query($sql);
		$suma = $suma + $res->num_rows;
			
		$sql = "SELECT COUNT(*)AS total FROM rev_caracteristicas_climatizacion_b
				JOIN orden_trabajo ON orden_trabajo.id = rev_caracteristicas_climatizacion_b.id_orden
				JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
				WHERE rev_caracteristicas_climatizacion_b.status=1 AND orden_trabajo.status = 3 
				AND cotizaciones.id_cliente = '$postdata->id'
				GROUP BY rev_caracteristicas_climatizacion_b.id_orden;";
		$res = $this->conexion->link->query($sql);
		$suma = $suma + $res->num_rows;
			
		$datos[1]['ordenes'] = $suma;
			
		$suma=0;
		/* Ordenes Ventilacion */
		$sql = "SELECT COUNT(*)AS total FROM man_caracteristicas_ventilacion
				JOIN orden_trabajo ON orden_trabajo.id = man_caracteristicas_ventilacion.id_orden
				JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
				WHERE man_caracteristicas_ventilacion.status=1 AND orden_trabajo.status = 3 
				AND cotizaciones.id_cliente = '$postdata->id'
				GROUP BY man_caracteristicas_ventilacion.id_orden;";
		$res = $this->conexion->link->query($sql);
		$suma = $suma + $res->num_rows;
			
		$sql = "SELECT COUNT(*)AS total FROM cor_caracteristicas_ventilacion
				JOIN orden_trabajo ON orden_trabajo.id = cor_caracteristicas_ventilacion.id_orden
				JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
				WHERE cor_caracteristicas_ventilacion.status=1 AND orden_trabajo.status = 3 
				AND cotizaciones.id_cliente = '$postdata->id'
				GROUP BY cor_caracteristicas_ventilacion.id_orden;";
		$res = $this->conexion->link->query($sql);
		$suma = $suma + $res->num_rows;
			
		$sql = "SELECT COUNT(*)AS total FROM ins_caracteristicas_ventilacion
				JOIN orden_trabajo ON orden_trabajo.id = ins_caracteristicas_ventilacion.id_orden
				JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
				WHERE ins_caracteristicas_ventilacion.status=1 AND orden_trabajo.status = 3 
				AND cotizaciones.id_cliente = '$postdata->id'
				GROUP BY ins_caracteristicas_ventilacion.id_orden;";
		$res = $this->conexion->link->query($sql);
		$suma = $suma + $res->num_rows;
			
		$sql = "SELECT COUNT(*)AS total FROM rev_caracteristicas_ventilacion
				JOIN orden_trabajo ON orden_trabajo.id = rev_caracteristicas_ventilacion.id_orden
				JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
				WHERE rev_caracteristicas_ventilacion.status=1 AND orden_trabajo.status = 3 
				AND cotizaciones.id_cliente = '$postdata->id'
				GROUP BY rev_caracteristicas_ventilacion.id_orden;";
		$res = $this->conexion->link->query($sql);
		$suma = $suma + $res->num_rows;
			
		$datos[2]['ordenes'] = $suma;
			
		$suma=0;
						/* Ordenes Refrigeracion */
		$sql = "SELECT COUNT(*)AS total FROM man_caracteristicas_refrigeracion
				JOIN orden_trabajo ON orden_trabajo.id = man_caracteristicas_refrigeracion.id_orden
				JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
				WHERE man_caracteristicas_refrigeracion.status=1 AND orden_trabajo.status = 3 
				AND cotizaciones.id_cliente = '$postdata->id'
				GROUP BY man_caracteristicas_refrigeracion.id_orden;";
		$res = $this->conexion->link->query($sql);
		$suma = $suma + $res->num_rows;
			
		$sql = "SELECT COUNT(*)AS total FROM cor_caracteristicas_refrigeracion
				JOIN orden_trabajo ON orden_trabajo.id = cor_caracteristicas_refrigeracion.id_orden
				JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
				WHERE cor_caracteristicas_refrigeracion.status=1 AND orden_trabajo.status = 3 
				AND cotizaciones.id_cliente = '$postdata->id'
				GROUP BY cor_caracteristicas_refrigeracion.id_orden;";
		$res = $this->conexion->link->query($sql);
		$suma = $suma + $res->num_rows;
			
		$sql = "SELECT COUNT(*)AS total FROM ins_caracteristicas_refrigeracion
				JOIN orden_trabajo ON orden_trabajo.id = ins_caracteristicas_refrigeracion.id_orden
				JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
				WHERE ins_caracteristicas_refrigeracion.status=1 AND orden_trabajo.status = 3 
				AND cotizaciones.id_cliente = '$postdata->id'
				GROUP BY ins_caracteristicas_refrigeracion.id_orden;";
		$res = $this->conexion->link->query($sql);
		$suma = $suma + $res->num_rows;
			
		$sql = "SELECT COUNT(*)AS total FROM rev_caracteristicas_refrigeracion
				JOIN orden_trabajo ON orden_trabajo.id = rev_caracteristicas_refrigeracion.id_orden
				JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
				WHERE rev_caracteristicas_refrigeracion.status = 1 AND orden_trabajo.status = 3 
				AND cotizaciones.id_cliente = '$postdata->id'
				GROUP BY rev_caracteristicas_refrigeracion.id_orden;";
		$res = $this->conexion->link->query($sql);
		$suma = $suma + $res->num_rows;
			
		$datos[3]['ordenes'] = $suma;
			
		$suma=0;
		$response->tipoEquipo = $datos;

		return json_encode($response);
	}

    public function general(){
		$sWhere = "";
        define("ORDEN_SIN_AGENDAR"       , 1);
        define("ORDEN_AGENDADA"          , 2);
        define("ORDEN_COMPLETADA"        , 3);

        define("COTIZACION_PENDIENTE"    , 0);
        define("COTIZACION_POR_APROBAR"  , 1);
        define("COTIZACION_APROBADA"     , 2);
        define("COTIZACION_POR_FACTURAR" , 3);

        $response = (object)[];
        $grafica = array();

        //nuevas ordenes y por facturar 
        $sql = "SELECT 
			(SELECT COUNT(*) FROM orden_trabajo WHERE status = 1 OR status = 2) AS nuevas, 
			(SELECT COUNT(*) FROM cotizaciones WHERE status = 3) AS por_facturar";
		$res = $this->conexion->link->query($sql)->fetch_assoc();
		$response->cuadros->nuevas_ordenes = $res["nuevas"];
		$response->cuadros->por_facturar = $res["por_facturar"];

		$start = date("Y-m-d");
		$end = date("Y-m-d");

        if(isset($_POST["start"]) && $_POST["start"] != ""){
        	$start = $_POST["start"];
        }
        if(isset($_POST["end"]) && $_POST["end"] != ""){
        	$end = $_POST["end"];
        }
        if($start == "CURRENT_DATE"){
    		$start = date("Y-m-d");
    	}
    	if($end == "CURRENT_DATE"){
    		$end = date("Y-m-d");
    	}

		$sWhere .= " AND fecha BETWEEN '{$start}' AND '{$end}'";

    	$indicador = null;
		if(isset($_POST["indicador"]) && $_POST["indicador"] != ""){
			$indicador = $_POST["indicador"];
			/*if($indicador == "rebotes"){
				$sWhere .= " AND codigo LIKE 'OR-%'";
			}else{
				$sWhere .= " AND codigo LIKE 'OT-%'";
			}*/
			#$sWhere .= " AND codigo LIKE 'OT-%'";
		}

		/*
		* 	get supervisores
		* */
		$sql = "SELECT id_responsable, tipo, 
						IF(tipo = 'Supervisor', 
						(SELECT UPPER(nombre) FROM cat_supervisores WHERE id = id_responsable), 
						(SELECT UPPER(nombre) FROM cat_auxiliares WHERE id = id_responsable)) AS nombre
				FROM orden_trabajo_responsables
				INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id
				WHERE 1=1 $sWhere
				GROUP BY tipo, id_responsable";
		$supervisores = [];
		$response->legends = [];

		$res = $this->conexion->link->query($sql);
		while($row = $res->fetch_assoc()){
			$supervisores[] = $row;
			$response->legends[] = $row["nombre"];
		}

		/*
		*	completado de las fechas dia por dia
		* */
		$segundos = strtotime($end) - strtotime($start);
		$interval = intval($segundos/60/60/24);
		$response->interval = $interval;

    	if($interval >= 0){
    		for($plus = 1; $plus <= $interval; $plus++){
	    		$current_date = date('Y-m-d', strtotime($start. ' + '.$plus.' days'));
				$response->fechas[] = $current_date;
				
				foreach($supervisores as $supervisor){
					switch($indicador){
						case "cumplimiento":
							#$response->grafica[$current_date][$supervisor["nombre"]] = $this->getCumplimientoSupervisor($current_date, $supervisor["id_responsable"], $supervisor["tipo"]);
							$response->grafica[$supervisor["nombre"]][] = $this->getCumplimientoSupervisor($current_date, $supervisor["id_responsable"], $supervisor["tipo"]);
							break;
						case "efectividad":
							#$response->grafica[$current_date][$supervisor["nombre"]] = $this->getEfectividadSupervisor($current_date, $supervisor["id_responsable"], $supervisor["tipo"]);
							$response->grafica[$supervisor["nombre"]][] = $this->getEfectividadSupervisor($current_date, $supervisor["id_responsable"], $supervisor["tipo"]);
							break;
						case "rebotes":
							$sql = "SELECT SUM(cant-1) as cant, id_responsable, tipo, fecha FROM (
									SELECT COUNT(*) as cant, id_responsable, tipo, fecha
									FROM orden_trabajo
									INNER JOIN orden_trabajo_responsables ON id_orden = orden_trabajo.id
									WHERE 1=1 $sWhere
									GROUP BY REPLACE(codigo, 'OR', 'OT')
									HAVING cant > 1 AND id_responsable = '{$supervisor["id_responsable"]}' AND tipo = '{$supervisor["tipo"]}' AND fecha = '{$current_date}') as tbl";
							$res = $this->conexion->link->query($sql)->fetch_assoc();
							$response->sql = $sql;
							$response->grafica[$supervisor["nombre"]][] = $res["cant"];
							$response->sql = $sql;
							break;
					}
				}
	    	}
	    }

		/*
		*	indicadores
		* */
		// efectividad 25%
        $response->cuadros->efectividad = $this->getEfectividad($start, $end);
        //cumplimiento 30%
        $response->cuadros->cumplimiento = $this->getCumplimientoNew($start, $end);

        /*
		*	cantidad de rebotes
		* */
		$sql = "SELECT SUM(cant-1) as cant FROM (
				SELECT COUNT(*) as cant
				FROM orden_trabajo
				INNER JOIN orden_trabajo_responsables ON id_orden = orden_trabajo.id
				WHERE 1=1 $sWhere
				GROUP BY REPLACE(codigo, 'OR', 'OT')
				HAVING cant > 1) as tbl";
		$res = $this->conexion->link->query($sql)->fetch_assoc();
		$response->cuadros->cantidad_rebotes = (int) $res["cant"];
		$response->cuadros->rebotes = 0;
		/*
		*	porcentaje de rebotes
		* */
		if($response->cuadros->cantidad_rebotes > 0){
			$response->cuadros->rebotes = 100;
		}

		/*
		*	porcentaje de rendimiento
		* */

		#efectividad 25%
		$efec = $response->cuadros->efectividad / 100 * 25;
		#cumplimiento 30%
		$cump = $response->cuadros->cumplimiento / 100 * 30;
		#rebotes 30%
		$rebt = 30 - ($response->cuadros->rebotes / 100 * 30);

		$response->cuadros->rendimiento = $efec + $cump + $rebt;
        return json_encode($response);
    }

    private function getCumplimientoSupervisor($current_date = "CURRENT_DATE", $supervisor = null, $type = ""){
    	$sWhere = "";
		if($supervisor != null && $type != ""){
    		$sWhere .= " AND tipo = '{$type}' AND (orden_trabajo_responsables.id_responsable = '{$supervisor}' ";
    	}
		if($type == "Asistente"){
			$sWhere .= " OR ids_auxiliares LIKE '%{$supervisor}%') ";
		}else{
			$sWhere .= ") ";
		}

    	$sql = "SELECT orden_trabajo.*, id_responsable
				FROM orden_trabajo 
    			INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id
				INNER JOIN orden_trabajo_responsables ON orden_trabajo_responsables.id_orden = orden_trabajo.id
    			WHERE orden_trabajo.status = 3 AND DATE(fecha_agendada) = '{$current_date}' $sWhere";
        $res = $this->conexion->link->query($sql);

        $orders_count = 0;
        $cumplimiento = 0;

        while($fila =  $res->fetch_assoc()){
        	$fila = (object) $fila;

			$ids_supervisores = implode(",", $fila->ids_responsables);
			if($fila->id_responsable == $supervisor || ($type == "Asistente" && in_array($supervisor, $ids_supervisores))){
				$orders_count++;

				$reporte_sql = "SELECT COUNT(*) as count FROM (
					(SELECT 'correctivo' AS tipoDato FROM `reportes_r_correctivo` WHERE id_orden IN({$fila->id}))
					UNION ALL 
					(SELECT 'instalacion' AS tipoDato FROM `reportes_r_instalacion`  WHERE id_orden IN({$fila->id}) ORDER BY order_time)
					UNION ALL
					(SELECT 'revision_mantenimiento' AS tipoDato FROM `reportes_revision_mantenimiento` WHERE id_orden IN({$fila->id}))
					UNION ALL
					(SELECT 'mantenimiento' AS tipoDato FROM `reportes_r_mantenimiento` WHERE id_orden IN({$fila->id}) ORDER BY order_time)
					UNION ALL
					(SELECT 'revision_instalacion' AS tipoDato FROM `reportes_revision_instalacion` WHERE id_orden IN({$fila->id}))
					UNION ALL
					(SELECT 'revision_correctivo' AS tipoDato FROM `reportes_revision_correctivo` WHERE id_orden IN({$fila->id}))
				) AS tab1";

				$r = $this->conexion->link->query($reporte_sql);
				while($fila2 = $r->fetch_assoc()){
					$count_report = $fila2["count"];
					$count_order = count($this->getEquipos($fila->id));
					if ($count_order > 0) 
						$c = ((100/$count_order) * $count_report);
					else
						$c = 0;
					$cumplimiento = number_format((double)$cumplimiento, 2, '.', '') + number_format((double)$c, 2, '.', '');
				}
			}
        }
		if ($orders_count == 0) return 0;
        $total = $cumplimiento / $orders_count;
        return ($total > 100) ? 100 : $total;
    }

	private function getCumplimientoNew($fechaInicio = "CURRENT_DATE", $fecha_fin = "", $tipoCliente = null, $idCliente = "") {
		$sWhere = "";
    	if ($tipoCliente != null) {
    		$sWhere .= " AND ot.tipo_cliente = '{$tipoCliente}' ";
    	}
    	if ($idCliente != null) {
    		$sWhere .= " AND cotizaciones.id_cliente = '{$idCliente}' ";
    	}
		if ($fecha_fin != "") {
			if ($fechaInicio != "CURRENT_DATE")
				$sWhere .= " AND ot.fecha BETWEEN '{$fechaInicio}' AND ";
			else
				$sWhere .= " AND ot.fecha BETWEEN {$fechaInicio} AND ";

			$sWhere.= " '{$fecha_fin}'";
		}
		
		$cumplimientoSql = "SELECT ROUND(AVG(IFNULL(IFNULL(cumplidas,0)/IFNULL(totales,0), 0))*100 ,2) as cumplimiento 
		FROM orden_trabajo ot
		INNER JOIN cotizaciones ON ot.id_cotizacion = cotizaciones.id
		
		LEFT JOIN (SELECT id_orden, COUNT(*) as cumplidas FROM (
				(SELECT id_orden, 'correctivo' AS tipoDato FROM `reportes_r_correctivo`)
				UNION ALL 
				(SELECT id_orden, 'instalacion' AS tipoDato FROM `reportes_r_instalacion` ORDER BY order_time)
				UNION ALL
				(SELECT id_orden, 'revision_mantenimiento' AS tipoDato FROM `reportes_revision_mantenimiento`)
				UNION ALL
				(SELECT id_orden, 'mantenimiento' AS tipoDato FROM `reportes_r_mantenimiento` ORDER BY order_time)
				UNION ALL
				(SELECT id_orden, 'revision_instalacion' AS tipoDato FROM `reportes_revision_instalacion`)
				UNION ALL
				(SELECT id_orden, 'revision_correctivo' AS tipoDato FROM `reportes_revision_correctivo`)
			) AS tab1 
			GROUP BY id_orden) as cumplidas ON cumplidas.id_orden = ot.id
		
		LEFT JOIN (SELECT id_orden, COUNT(*) as totales FROM (
			#mantenimiento
			(SELECT 'MANTENIMIENTO' AS tipo_trabajo, CONCAT('MANTENIMIENTO - ', id_orden) AS tipo_id_orden, CONCAT(man_caracteristicas_climatizacion_a.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.`nombre`,' - ',man_caracteristicas_climatizacion_a.codigo,' - ',IF(marca IS NULL, '', marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM man_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE man_caracteristicas_climatizacion_a.status = 1 HAVING reg IS NOT NULL) 
			UNION 
			(SELECT 'MANTENIMIENTO' AS tipo_trabajo, CONCAT('MANTENIMIENTO - ', id_orden) AS tipo_id_orden, CONCAT(man_caracteristicas_climatizacion_b.`codigo`, '-', id_cliente) AS codigo_cliente,  id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.`nombre`,' - ',man_caracteristicas_climatizacion_b.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM man_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE man_caracteristicas_climatizacion_b.status = 1 HAVING reg IS NOT NULL)
			UNION
			(SELECT 'MANTENIMIENTO' AS tipo_trabajo, CONCAT('MANTENIMIENTO - ', id_orden) AS tipo_id_orden, CONCAT(man_caracteristicas_refrigeracion.`codigo`, '-', id_cliente) AS codigo_cliente,  id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.`nombre`,' - ',man_caracteristicas_refrigeracion.codigo,' - ',IF(marca_eva IS NULL,'',marca_eva),' - ',IF(capacidad_hp_eva IS NULL,'',capacidad_hp_eva),' - ',IF(modelo_eva IS NULL,'',modelo_eva),' - ',IF(serie_eva IS NULL,'',serie_eva)) AS reg FROM man_caracteristicas_refrigeracion INNER JOIN cat_areas ON id_area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON id_equipo_des = cat_descripciones_equipos.`id` WHERE man_caracteristicas_refrigeracion.status = 1 HAVING reg IS NOT NULL)
			UNION
			(SELECT 'MANTENIMIENTO' AS tipo_trabajo, CONCAT('MANTENIMIENTO - ', id_orden) AS tipo_id_orden, CONCAT(man_caracteristicas_ventilacion.`codigo`, '-', id_cliente) AS codigo_cliente,  id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.`nombre`,' - ',man_caracteristicas_ventilacion.codigo,' - ',IF(marca,'',marca),' - ',IF(capacidadHP IS NULL,'',capacidadHP),' - ',IF(capacidadCFN IS NULL,'',capacidadCFN),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM man_caracteristicas_ventilacion INNER JOIN cat_areas ON area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE man_caracteristicas_ventilacion.status = 1 HAVING reg IS NOT NULL)
			
			#instalacion
			UNION
			(SELECT 'INSTALACION' AS tipo_trabajo, CONCAT('INSTALACION - ', id_orden) AS tipo_id_orden, CONCAT(ins_caracteristicas_climatizacion_a.`codigo`, '-', id_cliente) AS codigo_cliente,  id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.`nombre`,' - ',ins_caracteristicas_climatizacion_a.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie,'',serie)) AS reg FROM ins_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE ins_caracteristicas_climatizacion_a.status = 1 HAVING reg IS NOT NULL) 
			UNION 
			(SELECT 'INSTALACION' AS tipo_trabajo, CONCAT('INSTALACION - ', id_orden) AS tipo_id_orden,  CONCAT(ins_caracteristicas_climatizacion_b.`codigo`, '-', id_cliente) AS codigo_cliente,  id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.`nombre`,' - ',ins_caracteristicas_climatizacion_b.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM ins_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE ins_caracteristicas_climatizacion_b.status = 1 HAVING reg IS NOT NULL)
			UNION
			(SELECT 'INSTALACION' AS tipo_trabajo, CONCAT('INSTALACION - ', id_orden) AS tipo_id_orden,  CONCAT(ins_caracteristicas_refrigeracion.`codigo`, '-', id_cliente) AS codigo_cliente,  id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.`nombre`,' - ',ins_caracteristicas_refrigeracion.codigo,' - ',IF(marca_eva IS NULL,'',marca_eva),' - ',IF(modelo_eva IS NULL,'',modelo_eva),' - ',IF(serie_eva IS NULL,'',serie_eva),' - '/*,IF(capacidad_hp_eva IS NULL,'',capacidad_hp_eva),' - ',IF(compresorCantidad IS NULL,'',compresorCantidad),' - ',IF(ventiladorCantidad IS NULL,'',ventiladorCantidad)*/) AS reg FROM ins_caracteristicas_refrigeracion INNER JOIN cat_areas ON id_area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON id_equipo_des = cat_descripciones_equipos.`id` WHERE ins_caracteristicas_refrigeracion.status = 1 HAVING reg IS NOT NULL)
			UNION
			(SELECT 'INSTALACION' AS tipo_trabajo, CONCAT('INSTALACION - ', id_orden) AS tipo_id_orden,  CONCAT(ins_caracteristicas_ventilacion.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.`nombre`,' - ',ins_caracteristicas_ventilacion.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadHP IS NULL,'',capacidadHP),' - ',IF(capacidadCFN IS NULL,'',capacidadCFN),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM ins_caracteristicas_ventilacion INNER JOIN cat_areas ON area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE ins_caracteristicas_ventilacion.status = 1 HAVING reg IS NOT NULL)
			
			#correctivo
			UNION
			(SELECT 'CORRECTIVO' AS tipo_trabajo, CONCAT('CORRECTIVO - ', id_orden) AS tipo_id_orden, CONCAT(cor_caracteristicas_climatizacion_a.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.nombre,' - ',cor_caracteristicas_climatizacion_a.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM cor_caracteristicas_climatizacion_a INNER JOIN cat_areas ON area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE cor_caracteristicas_climatizacion_a.status = 1 HAVING reg IS NOT NULL) 
			UNION 
			(SELECT 'CORRECTIVO' AS tipo_trabajo, CONCAT('CORRECTIVO - ', id_orden) AS tipo_id_orden, CONCAT(cor_caracteristicas_climatizacion_b.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.nombre,' - ',cor_caracteristicas_climatizacion_b.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM cor_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = area INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE cor_caracteristicas_climatizacion_b.status = 1 HAVING reg IS NOT NULL)
			UNION
			(SELECT 'CORRECTIVO' AS tipo_trabajo, CONCAT('CORRECTIVO - ', id_orden) AS tipo_id_orden, CONCAT(cor_caracteristicas_refrigeracion.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.nombre,' - ',cor_caracteristicas_refrigeracion.`codigo`,' - ',IF(marca_eva IS NULL,'',marca_eva),' - ',IF(modelo_eva IS NULL,'',modelo_eva),' - ',IF(serie_eva IS NULL,'',serie_eva),' - '/*,IF(compresorBTU IS NULL,'',compresorBTU),' - ',IF(compresorCantidad IS NULL,'',compresorCantidad),' - ',IF(ventiladorCantidad IS NULL,'',ventiladorCantidad)*/) AS reg FROM cor_caracteristicas_refrigeracion INNER JOIN cat_areas ON id_area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON id_equipo_des = cat_descripciones_equipos.`id` WHERE cor_caracteristicas_refrigeracion.status = 1 HAVING reg IS NOT NULL)
			UNION
			(SELECT 'CORRECTIVO' AS tipo_trabajo, CONCAT('CORRECTIVO - ', id_orden) AS tipo_id_orden, CONCAT(cor_caracteristicas_ventilacion.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.nombre,' - ',cor_caracteristicas_ventilacion.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadHP IS NULL,'',capacidadHP),' - ',IF(capacidadCFN IS NULL,'',capacidadHP),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM cor_caracteristicas_ventilacion INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE cor_caracteristicas_ventilacion.status = 1 HAVING reg IS NOT NULL)
			
			#revision
			UNION
			(SELECT 'REVISION' AS tipo_trabajo, CONCAT('REVISION - ', id_orden) AS tipo_id_orden, CONCAT(rev_caracteristicas_climatizacion_a.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.nombre,' - ',rev_caracteristicas_climatizacion_a.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM rev_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE rev_caracteristicas_climatizacion_a.status = 1) 
			UNION 
			(SELECT 'REVISION' AS tipo_trabajo, CONCAT('REVISION - ', id_orden) AS tipo_id_orden, CONCAT(rev_caracteristicas_climatizacion_b.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.nombre,' - ',rev_caracteristicas_climatizacion_b.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM rev_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE rev_caracteristicas_climatizacion_b.status = 1)
			UNION
			(SELECT 'REVISION' AS tipo_trabajo, CONCAT('REVISION - ', id_orden) AS tipo_id_orden, CONCAT(rev_caracteristicas_refrigeracion.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.nombre,' - ',rev_caracteristicas_refrigeracion.`codigo`,' - ',IF(marca_eva IS NULL,'',marca_eva),' - ',IF(modelo_eva IS NULL,'',modelo_eva),' - ',IF(serie_eva IS NULL,'',serie_eva),' - '/*,IF(compresorBTU IS NULL,'',compresorBTU),' - ',IF(compresorCantidad IS NULL,'',compresorCantidad),' - ',IF(ventiladorCantidad IS NULL,'',ventiladorCantidad)*/) AS reg FROM rev_caracteristicas_refrigeracion INNER JOIN cat_areas ON id_area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON id_equipo_des = cat_descripciones_equipos.`id` WHERE rev_caracteristicas_refrigeracion.status = 1)
			UNION
			(SELECT 'REVISION' AS tipo_trabajo, CONCAT('REVISION - ', id_orden) AS tipo_id_orden, CONCAT(rev_caracteristicas_ventilacion.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.nombre,' - ',rev_caracteristicas_ventilacion.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadHP IS NULL,'',capacidadHP),' - ',IF(capacidadCFN IS NULL,'',capacidadHP),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM rev_caracteristicas_ventilacion INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE rev_caracteristicas_ventilacion.status = 1)
			
		) AS tb 
		GROUP BY id_orden
		) as totales ON totales.id_orden = ot.id
		WHERE ot.status = 3 $sWhere;";

		return $this->conexion->queryRow($cumplimientoSql)->cumplimiento;
	}
	
	private function getCumplimiento($date = "CURRENT_DATE", $limitDate = "", $tipo_cliente = null, $id_cliente = null){		
		$sWhere = "";
    	if($tipo_cliente != null){
    		$sWhere .= " AND orden_trabajo.tipo_cliente = '{$tipo_cliente}' ";
    	}
    	if($id_cliente != null){
    		$sWhere .= " AND cotizaciones.id_cliente = '{$id_cliente}' ";
    	}
    	// if($supervisor != null){
    	// 	$sWhere .= " AND grupo_trabajo = '{$supervisor}'";
    	// }
		if($limitDate != ""){
			if($date != "CURRENT_DATE")
				$sWhere .= " AND fecha BETWEEN '{$date}' AND ";
			else
				$sWhere .= " AND fecha BETWEEN {$date} AND ";

			$sWhere.= " '{$limitDate}'";
		}

    	$sql = "SELECT orden_trabajo.* FROM orden_trabajo 
    			INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id
    			WHERE orden_trabajo.status = 3 $sWhere";
				#AND fecha_agendada <= ".($date != "CURRENT_DATE" ? "DATE('{$date}')":"CURRENT_DATE")."
		$res = $this->conexion->link->query($sql);

        $orders_count = 0;
        $cumplimiento = 0;

        while($fila =  $res->fetch_assoc()){
        	$fila = (object) $fila;
        	$orders_count++;

        	$reporte_sql = "SELECT COUNT(*) as count FROM (
				(SELECT 'correctivo' AS tipoDato FROM `reportes_r_correctivo` WHERE id_orden IN({$fila->id}))
				UNION ALL 
				(SELECT 'instalacion' AS tipoDato FROM `reportes_r_instalacion`  WHERE id_orden IN({$fila->id}) ORDER BY order_time)
				UNION ALL
				(SELECT 'revision_mantenimiento' AS tipoDato FROM `reportes_revision_mantenimiento` WHERE id_orden IN({$fila->id}))
				UNION ALL
				(SELECT 'mantenimiento' AS tipoDato FROM `reportes_r_mantenimiento` WHERE id_orden IN({$fila->id}) ORDER BY order_time)
				UNION ALL
				(SELECT 'revision_instalacion' AS tipoDato FROM `reportes_revision_instalacion` WHERE id_orden IN({$fila->id}))
				UNION ALL
				(SELECT 'revision_correctivo' AS tipoDato FROM `reportes_revision_correctivo` WHERE id_orden IN({$fila->id}))
			) AS tab1";

			$r = $this->conexion->link->query($reporte_sql);
			while($fila2 = $r->fetch_assoc()){
				$count_report = $fila2["count"];
				$count_order = count($this->getEquipos($fila->id));
				if ($count_order > 0)
					$c = ((100/$count_order) * $count_report);
				else
					$c = 0;
				$cumplimiento = number_format((double)$cumplimiento, 2, '.', '') + number_format((double)$c, 2, '.', '');
			}
        }
		if ($orders_count == 0) return 0;
        $total = $cumplimiento / $orders_count;
        return ($total > 100) ? 100 : $total;
    }

    private function getEfectividadSupervisor($current_date = "CURRENT_DATE", $supervisor = null, $type = ""){
    	$sWhere = "";
    	if($supervisor != null && $type != ""){
    		$sWhere .= " AND tipo = '{$type}' AND (orden_trabajo_responsables.id_responsable = '{$supervisor}' ";
    	}
		if($type == "Asistente"){
			$sWhere .= " OR ids_auxiliares LIKE '%{$supervisor}%') ";
		}else{
			$sWhere .= ") ";
		}

		$sql = "SELECT orden_trabajo_responsables.id_orden, hora_llegada, order_time, hora_programada, tiempo_estimado, ids_auxiliares, orden_trabajo_responsables.id_responsable
        		FROM reportes_r_correctivo
				INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id
				INNER JOIN cotizaciones ON orden_trabajo.id_cotizacion = cotizaciones.id
				INNER JOIN orden_trabajo_responsables ON orden_trabajo_responsables.id_orden = orden_trabajo.id
				WHERE orden_trabajo.status = 3 AND DATE(fecha_agendada) = '{$current_date}' $sWhere GROUP BY orden_trabajo_responsables.id_orden";
        $res = $this->conexion->link->query($sql);
        $sum = 0;
        $totalRegs = 0;
        while($fila = $res->fetch_assoc()){
        	$fila = (object) $fila;
			
			$ids_supervisores = implode(",", $fila->ids_responsables);
			if($fila->id_responsable == $supervisor || ($type == "Asistente" && in_array($supervisor, $ids_supervisores))){
				$totalRegs++;
				$fila->hora_llegada = str_replace("T", " ", $fila->hora_llegada);
				$fila->hora_llegada = substr($fila->hora_llegada, 0, -6);

				$fila->hora_programada = explode(" ", $fila->hora_programada);
				if(trim($fila->hora_programada[1]) == "AM"){
					$hora = substr($fila->hora_programada[0], 0, -6);
				}else if(trim($fila->hora_programada[1] == "PM")){
					$hora = (int) substr($fila->hora_programada[0], 0, -6);
					$hora .= 12;
				}
				$time = $hora.substr($fila->hora_programada[0], -6);

				$ssqqll = "SELECT diff, tiempo_estimado, IF(diff <= CAST(tiempo_estimado AS TIME), TRUE, FALSE) AS duracion, IF(CAST(llegada AS TIME) <= CAST(hora_programada AS TIME), TRUE, FALSE) AS llegada
							FROM (SELECT TIMEDIFF('{$fila->order_time}', '{$fila->hora_llegada}') AS diff, '{$fila->tiempo_estimado}' AS tiempo_estimado, '{$time}' AS hora_programada, '{$fila->hora_llegada}' AS llegada) AS tbl";
				$r = $this->conexion->link->query($ssqqll);
				$r = $r->fetch_assoc();

				if($r["duracion"] == 1){
					$sum .= 0.5;
				}
				if($r["llegada"] == 1){
					$sum .= 0.5;
				}
			}
        }
		$total = ($sum / $totalRegs) * 100;
        return ($total > 100) ? 100 : $total;
    }

    private function getEfectividad($date = "CURRENT_DATE", $limitDate = "", $tipo_cliente = null, $id_cliente = null){
    	$sWhere = "";
    	if($tipo_cliente != null){
    		$sWhere .= " AND orden_trabajo.tipo_cliente = '{$tipo_cliente}' ";
    	}
    	if($id_cliente != null){
    		$sWhere .= " AND cotizaciones.id_cliente = '{$id_cliente}' ";
    	}
		if($limitDate != ""){
			if($date != "CURRENT_DATE")
				$sWhere .= " AND fecha BETWEEN '{$date}' AND ";
			else
				$sWhere .= " AND fecha BETWEEN {$date} AND ";

			$sWhere.= " '{$limitDate}'";
		}

    	$sql = "SELECT id_orden, hora_llegada, order_time, hora_programada, tiempo_estimado 
        		FROM reportes_r_correctivo
				INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id
				INNER JOIN cotizaciones ON orden_trabajo.id_cotizacion = cotizaciones.id
				WHERE orden_trabajo.status = 3 $sWhere
				GROUP BY id_orden";
        $res = $this->conexion->link->query($sql);
        $sum = 0;
        $totalRegs = 0;
        while($fila = $res->fetch_assoc()){
        	$fila = (object) $fila;
        	$totalRegs++;
        	$fila->hora_llegada = str_replace("T", " ", $fila->hora_llegada);
        	$fila->hora_llegada = substr($fila->hora_llegada, 0, -6);

        	$fila->hora_programada = explode(" ", $fila->hora_programada);
        	if(trim($fila->hora_programada[1]) == "AM"){
        		$hora = substr($fila->hora_programada[0], 0, -6);
        	}else if(trim($fila->hora_programada[1] == "PM")){
        		$hora = (int) substr($fila->hora_programada[0], 0, -6);
        		$hora .= 12;
        	}
        	$time = $hora.substr($fila->hora_programada[0], -6);

        	$ssqqll = "SELECT diff, tiempo_estimado, IF(diff <= CAST(tiempo_estimado AS TIME), TRUE, FALSE) AS duracion, IF(CAST(llegada AS TIME) <= CAST(hora_programada AS TIME), TRUE, FALSE) AS llegada
						FROM (SELECT TIMEDIFF('{$fila->order_time}', '{$fila->hora_llegada}') AS diff, '{$fila->tiempo_estimado}' AS tiempo_estimado, '{$time}' AS hora_programada, '{$fila->hora_llegada}' AS llegada) AS tbl";
			$r = $this->conexion->link->query($ssqqll);
			$r = $r->fetch_assoc();

			if($r["duracion"] == 1){
				$sum .= 0.5;
			}
			if($r["llegada"] == 1){
				$sum .= 0.5;
			}
        }
		if ($totalRegs == 0) return 0;
        $total = ($sum / $totalRegs) * 100;
        return ($total > 100) ? 100 : $total;
    }

    private function getEquipos($id_orden = 0){
    	$sql = "
			SELECT * FROM (
			
			#mantenimiento
			(SELECT 'MANTENIMIENTO' AS tipo_trabajo, CONCAT('MANTENIMIENTO - ', id_orden) AS tipo_id_orden, CONCAT(man_caracteristicas_climatizacion_a.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.`nombre`,' - ',man_caracteristicas_climatizacion_a.codigo,' - ',IF(marca IS NULL, '', marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM man_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE man_caracteristicas_climatizacion_a.status = 1 HAVING reg IS NOT NULL) 
			UNION 
			(SELECT 'MANTENIMIENTO' AS tipo_trabajo, CONCAT('MANTENIMIENTO - ', id_orden) AS tipo_id_orden, CONCAT(man_caracteristicas_climatizacion_b.`codigo`, '-', id_cliente) AS codigo_cliente,  id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.`nombre`,' - ',man_caracteristicas_climatizacion_b.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM man_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE man_caracteristicas_climatizacion_b.status = 1 HAVING reg IS NOT NULL)
			UNION
			(SELECT 'MANTENIMIENTO' AS tipo_trabajo, CONCAT('MANTENIMIENTO - ', id_orden) AS tipo_id_orden, CONCAT(man_caracteristicas_refrigeracion.`codigo`, '-', id_cliente) AS codigo_cliente,  id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.`nombre`,' - ',man_caracteristicas_refrigeracion.codigo,' - ',IF(marca_eva IS NULL,'',marca_eva),' - ',IF(capacidad_hp_eva IS NULL,'',capacidad_hp_eva),' - ',IF(modelo_eva IS NULL,'',modelo_eva),' - ',IF(serie_eva IS NULL,'',serie_eva)) AS reg FROM man_caracteristicas_refrigeracion INNER JOIN cat_areas ON id_area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON id_equipo_des = cat_descripciones_equipos.`id` WHERE man_caracteristicas_refrigeracion.status = 1 HAVING reg IS NOT NULL)
			UNION
			(SELECT 'MANTENIMIENTO' AS tipo_trabajo, CONCAT('MANTENIMIENTO - ', id_orden) AS tipo_id_orden, CONCAT(man_caracteristicas_ventilacion.`codigo`, '-', id_cliente) AS codigo_cliente,  id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.`nombre`,' - ',man_caracteristicas_ventilacion.codigo,' - ',IF(marca,'',marca),' - ',IF(capacidadHP IS NULL,'',capacidadHP),' - ',IF(capacidadCFN IS NULL,'',capacidadCFN),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM man_caracteristicas_ventilacion INNER JOIN cat_areas ON area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE man_caracteristicas_ventilacion.status = 1 HAVING reg IS NOT NULL)
			
			#instalacion
			UNION
			(SELECT 'INSTALACION' AS tipo_trabajo, CONCAT('INSTALACION - ', id_orden) AS tipo_id_orden, CONCAT(ins_caracteristicas_climatizacion_a.`codigo`, '-', id_cliente) AS codigo_cliente,  id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.`nombre`,' - ',ins_caracteristicas_climatizacion_a.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie,'',serie)) AS reg FROM ins_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE ins_caracteristicas_climatizacion_a.status = 1 HAVING reg IS NOT NULL) 
			UNION 
			(SELECT 'INSTALACION' AS tipo_trabajo, CONCAT('INSTALACION - ', id_orden) AS tipo_id_orden,  CONCAT(ins_caracteristicas_climatizacion_b.`codigo`, '-', id_cliente) AS codigo_cliente,  id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.`nombre`,' - ',ins_caracteristicas_climatizacion_b.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM ins_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE ins_caracteristicas_climatizacion_b.status = 1 HAVING reg IS NOT NULL)
			UNION
			(SELECT 'INSTALACION' AS tipo_trabajo, CONCAT('INSTALACION - ', id_orden) AS tipo_id_orden,  CONCAT(ins_caracteristicas_refrigeracion.`codigo`, '-', id_cliente) AS codigo_cliente,  id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.`nombre`,' - ',ins_caracteristicas_refrigeracion.codigo,' - ',IF(marca_eva IS NULL,'',marca_eva),' - ',IF(modelo_eva IS NULL,'',modelo_eva),' - ',IF(serie_eva IS NULL,'',serie_eva),' - '/*,IF(capacidad_hp_eva IS NULL,'',capacidad_hp_eva),' - ',IF(compresorCantidad IS NULL,'',compresorCantidad),' - ',IF(ventiladorCantidad IS NULL,'',ventiladorCantidad)*/) AS reg FROM ins_caracteristicas_refrigeracion INNER JOIN cat_areas ON id_area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON id_equipo_des = cat_descripciones_equipos.`id` WHERE ins_caracteristicas_refrigeracion.status = 1 HAVING reg IS NOT NULL)
			UNION
			(SELECT 'INSTALACION' AS tipo_trabajo, CONCAT('INSTALACION - ', id_orden) AS tipo_id_orden,  CONCAT(ins_caracteristicas_ventilacion.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.`nombre`,' - ',ins_caracteristicas_ventilacion.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadHP IS NULL,'',capacidadHP),' - ',IF(capacidadCFN IS NULL,'',capacidadCFN),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM ins_caracteristicas_ventilacion INNER JOIN cat_areas ON area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE ins_caracteristicas_ventilacion.status = 1 HAVING reg IS NOT NULL)
			
			#correctivo
			UNION
			(SELECT 'CORRECTIVO' AS tipo_trabajo, CONCAT('CORRECTIVO - ', id_orden) AS tipo_id_orden, CONCAT(cor_caracteristicas_climatizacion_a.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.nombre,' - ',cor_caracteristicas_climatizacion_a.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM cor_caracteristicas_climatizacion_a INNER JOIN cat_areas ON area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE cor_caracteristicas_climatizacion_a.status = 1 HAVING reg IS NOT NULL) 
			UNION 
			(SELECT 'CORRECTIVO' AS tipo_trabajo, CONCAT('CORRECTIVO - ', id_orden) AS tipo_id_orden, CONCAT(cor_caracteristicas_climatizacion_b.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.nombre,' - ',cor_caracteristicas_climatizacion_b.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM cor_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = area INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE cor_caracteristicas_climatizacion_b.status = 1 HAVING reg IS NOT NULL)
			UNION
			(SELECT 'CORRECTIVO' AS tipo_trabajo, CONCAT('CORRECTIVO - ', id_orden) AS tipo_id_orden, CONCAT(cor_caracteristicas_refrigeracion.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.nombre,' - ',cor_caracteristicas_refrigeracion.`codigo`,' - ',IF(marca_eva IS NULL,'',marca_eva),' - ',IF(modelo_eva IS NULL,'',modelo_eva),' - ',IF(serie_eva IS NULL,'',serie_eva),' - '/*,IF(compresorBTU IS NULL,'',compresorBTU),' - ',IF(compresorCantidad IS NULL,'',compresorCantidad),' - ',IF(ventiladorCantidad IS NULL,'',ventiladorCantidad)*/) AS reg FROM cor_caracteristicas_refrigeracion INNER JOIN cat_areas ON id_area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON id_equipo_des = cat_descripciones_equipos.`id` WHERE cor_caracteristicas_refrigeracion.status = 1 HAVING reg IS NOT NULL)
			UNION
			(SELECT 'CORRECTIVO' AS tipo_trabajo, CONCAT('CORRECTIVO - ', id_orden) AS tipo_id_orden, CONCAT(cor_caracteristicas_ventilacion.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.nombre,' - ',cor_caracteristicas_ventilacion.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadHP IS NULL,'',capacidadHP),' - ',IF(capacidadCFN IS NULL,'',capacidadHP),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM cor_caracteristicas_ventilacion INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE cor_caracteristicas_ventilacion.status = 1 HAVING reg IS NOT NULL)
			
			#revision
			UNION
			(SELECT 'REVISION' AS tipo_trabajo, CONCAT('REVISION - ', id_orden) AS tipo_id_orden, CONCAT(rev_caracteristicas_climatizacion_a.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.nombre,' - ',rev_caracteristicas_climatizacion_a.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM rev_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE rev_caracteristicas_climatizacion_a.status = 1) 
			UNION 
			(SELECT 'REVISION' AS tipo_trabajo, CONCAT('REVISION - ', id_orden) AS tipo_id_orden, CONCAT(rev_caracteristicas_climatizacion_b.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.nombre,' - ',rev_caracteristicas_climatizacion_b.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM rev_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE rev_caracteristicas_climatizacion_b.status = 1)
			UNION
			(SELECT 'REVISION' AS tipo_trabajo, CONCAT('REVISION - ', id_orden) AS tipo_id_orden, CONCAT(rev_caracteristicas_refrigeracion.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.nombre,' - ',rev_caracteristicas_refrigeracion.`codigo`,' - ',IF(marca_eva IS NULL,'',marca_eva),' - ',IF(modelo_eva IS NULL,'',modelo_eva),' - ',IF(serie_eva IS NULL,'',serie_eva),' - '/*,IF(compresorBTU IS NULL,'',compresorBTU),' - ',IF(compresorCantidad IS NULL,'',compresorCantidad),' - ',IF(ventiladorCantidad IS NULL,'',ventiladorCantidad)*/) AS reg FROM rev_caracteristicas_refrigeracion INNER JOIN cat_areas ON id_area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON id_equipo_des = cat_descripciones_equipos.`id` WHERE rev_caracteristicas_refrigeracion.status = 1)
			UNION
			(SELECT 'REVISION' AS tipo_trabajo, CONCAT('REVISION - ', id_orden) AS tipo_id_orden, CONCAT(rev_caracteristicas_ventilacion.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.nombre,' - ',rev_caracteristicas_ventilacion.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadHP IS NULL,'',capacidadHP),' - ',IF(capacidadCFN IS NULL,'',capacidadHP),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM rev_caracteristicas_ventilacion INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE rev_caracteristicas_ventilacion.status = 1)
			) AS tb HAVING id_orden = {$id_orden}
    	";
    	$res = $this->conexion->link->query($sql);
    	$d = array();
    	while($fila = $res->fetch_assoc()){
    		$fila = (object) $fila;
    		$d[] = $fila;
    	}
    	return $d;
    }
}