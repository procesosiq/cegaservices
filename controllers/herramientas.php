<?php

class herramientas { 
     
    private $conexion;
    private $session;
    
    public function __construct(){
        $this->conexion = new M_Conexion;
        $this->session = Session::getInstance();
    }
    
    public function index(){
        
        $datos = (object)$_REQUEST;
        
        $sWhere = "";
        $sOrder = " ORDER BY id";
        $DesAsc = "ASC";
        $sOrder .= " {$DesAsc}";
        $sLimit = "";
        // print_r($_POST);
        if(isset($_POST)){

                /*----------  ORDER BY ----------*/
                
                if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
                    $DesAsc = $_POST['order'][0]['dir'];
                    $sOrder = " ORDER BY id {$DesAsc}";
                }
                if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
                    $DesAsc = $_POST['order'][0]['dir'];
                    $sOrder = " ORDER BY herramienta {$DesAsc}";
                }
                if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
                    $DesAsc = $_POST['order'][0]['dir'];
                    $sOrder = " ORDER BY especificacion {$DesAsc}";
                }
                if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4){
                    $DesAsc = $_POST['order'][0]['dir'];
                    $sOrder = " ORDER BY status {$DesAsc}";
                }
                /*----------  WHERE ----------*/ 

                if(isset($_POST['order_id']) && trim($_POST['order_id']) != ""){
                    $sWhere .= " AND id = ".$_POST["order_id"];
                }
                if(isset($_POST['order_herramienta']) && trim($_POST['order_herramienta']) != ""){
                    $sWhere .= " AND herramienta LIKE '%".$_POST['order_herramienta']."%'";
                }
                if(isset($_POST['order_especificacion']) && trim($_POST['order_especificacion']) != ""){
                    $sWhere .= " AND especificacion LIKE '%".$_POST['order_especificacion']."%'";
                }
                if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
                    $sWhere .= " AND status = ".$_POST['order_status'];
                }

                /*----------  LIMIT  ----------*/
                if(isset($_POST['length']) && $_POST['length'] > 0){
                    $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
                }
            }

        $sql = "SELECT * FROM cat_herramientas_trabajo $sWhere $sOrder $sLimit";
        $res = $this->conexion->link->query($sql);
        while($fila = $res->fetch_assoc()){
            $fila = (object)$fila;
            #$fila->herramienta = utf8_decode($fila->herramienta);
            #$fila->especificacion = utf8_decode($fila->especificacion);
        
            $datos->data[] = array (
                '<input type="checkbox" name="id[]" value="'.$fila->id.'">',
                $fila->id,
                ($fila->herramienta!=null)?trim($fila->herramienta):'---',
                ($fila->especificacion!=null)?trim($fila->especificacion):'---',
                '<button class="btn btn-sm '.(($fila->status==1)?'green-jungle':'red').'" id="status">'.(($fila->status==1)?'ACTIVO':'INACTIVO').'</button>',
                '<button id="edit" class="btn btn-sm green btn-outline filter-submit margin-bottom" data-id_herramienta="'.$fila->id.'"><i class="fa fa-plus"></i> Editar</button>'
            );
        }

        $datos->recordsTotal = count($datos->data);
        #$datos->customActionMessage = "Informacion completada con exito";
        $datos->customActionStatus = "OK";

        return json_encode($datos);
    }

    public function getEquipo($id_equipo){
        $sql = "SELECT * FROM cat_equipos WHERE id = $id_equipo";
        $res = $this->conexion->link->query($sql);
        $datos = array();
        if($fila = $res->fetch_assoc()){
             $datos = (object)$fila;
        }
        return $datos;
    }
    
    public function GetSucursal($id){
        $sql = "SELECT razon_social AS nombre
        FROM cat_sucursales 
        WHERE id='$id'";
        $res = $this->conexion->link->query($sql);
        $datos = array();
        if($fila = $res->fetch_assoc()){
             $datos = (object)$fila;
        }
        return $datos;
    }
    
    public function GetCliente($id){
        $sql = "SELECT nombre,id_tipcli
        FROM cat_clientes 
        WHERE id='$id'";
        $res = $this->conexion->link->query($sql);
        $datosCli = array();
        if($fila = $res->fetch_assoc()){
             $datosCli = (object)$fila;
        }
        return $datosCli;
    }
    
    public function GetTipoEquipos(){
        $sql = "SELECT id,nombre FROM cat_equiposTipo WHERE status = 1 ORDER BY nombre";
        $res = $this->conexion->link->query($sql);
        $datos = array();
        while($fila = $res->fetch_assoc()){
             $datos[] = (object)$fila;
        }
        return $datos;
    }
    
    public function GetListaEqu(){ 
        $datos = (object)$_POST;
        $sql = "SELECT id,nombre FROM cat_equiposLista WHERE status = 1 AND id_tipoequipo='$datos->idTipE' ORDER BY nombre";
        $res = $this->conexion->link->query($sql);
        $datos = array();
        while($fila = $res->fetch_assoc()){
             $datos[] = $fila;
        }
        echo  json_encode($datos);
    }
    
    public function GetPartesEqu(){ 
        $datos = (object)$_POST;
        $sql = "SELECT id,nombre FROM cat_equiposPartes WHERE status = 1 AND id_listaequipo='$datos->idLisE' ORDER BY nombre";
        $res = $this->conexion->link->query($sql);
        $datos = array();
        while($fila = $res->fetch_assoc()){
             $datos[] = $fila;
        }
        echo  json_encode($datos);
    }

    public function getPiezas(){
        $datos = (object)$_POST;
        $sql = "SELECT * FROM cat_piezas WHERE id_parte_equipo = $datos->id_parte_equipo AND status = 1 ORDER BY id";
        $data = $this->conexion->link->query($sql);
        $datos = array();
        while($fila = $data->fetch_assoc()){
             $datos[] = $fila;
        }
        echo json_encode($datos);
    }

    public function getRefrigerantes(){
        $sql = "SELECT * FROM cat_refrigerantes WHERE status = 1 ORDER BY id";
        $data = $this->conexion->link->query($sql);
        $datos = array();
        while($fila = $data->fetch_assoc()){
             $datos[] = (object)$fila;
        }
        return $datos;
    }

    public function getMotores(){
        $sql = "SELECT * FROM cat_motores WHERE status = 1 ORDER BY id";
        $data = $this->conexion->link->query($sql);
        $datos = array();
        while($fila = $data->fetch_assoc()){
             $datos[] = (object)$fila;
        }
        return $datos;
    }
    
    public function GetCapacidadButu(){
        $sql = "SELECT id,nombre FROM cat_capacidadBUTU WHERE status = 1 ORDER BY nombre";
        $res = $this->conexion->link->query($sql);
        $datos = array();
        while($fila = $res->fetch_assoc()){
             $datos[] = (object)$fila;
        }
        return $datos;
    }
    
    public function GetCapacidadHP(){
        $sql = "SELECT id,nombre FROM cat_capacidadHP WHERE status = 1 ORDER BY nombre";
        $res = $this->conexion->link->query($sql);
        $datos = array();
        while($fila = $res->fetch_assoc()){
             $datos[] = (object)$fila;
        }
        return $datos;
    }
    
    public function GetAreaClima(){ 
        $datos = (object)$_POST;
        $sql = "SELECT id,nombre FROM cat_areas WHERE status = 1 AND id_tipoequipo='$datos->idTipE' AND id_tipocliente = '$datos->tipo_cliente' ORDER BY nombre";
        $res = $this->conexion->link->query($sql);
        $datos = array();
        while($fila = $res->fetch_assoc()){
             $datos[] = $fila;
        }
        echo  json_encode($datos);
    }

    public function getPartesEquipo(){
        $datos = (object)$_POST;
        $sql = "SELECT data_piezas FROM cat_equipos WHERE status = 1 AND id='$datos->id'";
        $res = $this->conexion->link->query($sql);
        $datos = array();
        while($fila = $res->fetch_assoc()){
             $datos[] = $fila;
        }
        $piezas = json_decode($datos[0]["data_piezas"], true);
        echo json_encode($piezas);
    }

    public function getAreasClimatiza($id_tipo_equipo, $id_tipo_clienete){
        $sql = "SELECT id,nombre FROM cat_areas WHERE status = 1 AND id_tipoequipo='$id_tipo_equipo' AND id_tipocliente = '$id_tipo_clienete' ORDER BY nombre";
        $res = $this->conexion->link->query($sql);
        $datos = array();
        while($fila = $res->fetch_assoc()){
             $datos[] = (object)$fila;
        }
        return $datos;
    }
    
    public function GetMarcas(){
        $sql = "SELECT id,nombre FROM cat_marcas WHERE status = 1 ORDER BY nombre";
        $res = $this->conexion->link->query($sql);
        $datos = array();
        while($fila = $res->fetch_assoc()){
             $datos[] = (object)$fila;
        }
        return $datos;
    }

    public function getDescripciones(){
        $sql = "SELECT * FROM cat_descripciones_equipos WHERE status = 1 ORDER BY id";
        $res = $this->conexion->link->query($sql);
        $datos = array();
        while($fila = $res->fetch_assoc()){
             $datos[] = (object)$fila;
        }
        return $datos;   
    }

    public function changeStatus(){
        $datos = (object)$_POST;
        $sql = "UPDATE cat_equipos SET status = 0 WHERE id = $datos->id";
        return $this->conexion->link->query($sql);
    }
    
    public function AddEquipo(){
        $datos = (object)$_POST;
        #print_r($datos);
        $datos->fecha = trim($datos->fecha);
		$fecha = substr($datos->fecha,6,4)."-".substr($datos->fecha,3,2)."-".substr($datos->fecha,0,2);

        $array_partes = json_decode($datos->partes);
        $piezas = array();
        foreach ($array_partes as $key => $value) {
            $piezas[] = (object)$value;
        }

        $json_piezas = array();
        
        foreach ($piezas as $llave => $valor) {
            $valor = (object)$valor;
            #foreach ($valor->id_pieza as $key => $value) {
                #$value = (object)$value;
            $json_piezas[] = array("id_pieza"=>$valor->id_pieza,"id_refrigerante"=>$valor->id_refrigerante,"id_motor"=>$valor->id_motor,"id_marca"=>$valor->id_marca,"id_capacidad"=>$valor->id_capacidad,"modelo"=>$valor->modelo,"serie"=>$valor->serie,"id_parte"=>$valor->id_parte,"capacidad"=>$valor->capacidad,"marca"=>$valor->marca,"parte"=>$valor->parte);
            #}
        }
        
        $sql="INSERT INTO cat_equipos SET
            id_cliente = $datos->id_cliente,
            id_usuario = '{$this->session->logged}',
            id_sucursal = $datos->id_sucursal,
            id_tipo_equipo = $datos->tipo_equipo,
            id_descripcion_equipo = $datos->id_descripcion,
            capacidad = '$datos->capacidad',
            nombre_area = '$datos->nombre_area',
            id_area_climatiza = $datos->area_climatiza,
            codigo = '$datos->codigo',
            fecha = '$fecha',
            data_piezas = '".json_encode($json_piezas)."';";

        $id = $this->conexion->Consultas(1, $sql);

        if($id > 0)
            echo $id;
        else
            echo 0;

        #print_r($sql);
    }

    public function UpdateEquipo(){
        $datos = (object)$_POST;
        #print_r($datos);
        $datos->fecha = trim($datos->fecha);
        $fecha = substr($datos->fecha,6,4)."-".substr($datos->fecha,3,2)."-".substr($datos->fecha,0,2);

        $array_partes = json_decode($datos->partes);
        $piezas = array();
        foreach ($array_partes as $key => $value) {
            $piezas[] = (object)$value;
        }

        $json_piezas = array();
        
        foreach ($piezas as $llave => $valor) {
            $valor = (object)$valor;
            $json_piezas[] = array("id_pieza"=>$valor->id_pieza,"id_refrigerante"=>$valor->id_refrigerante,"id_motor"=>$valor->id_motor,"id_marca"=>$valor->id_marca,"id_capacidad"=>$valor->id_capacidad,"modelo"=>$valor->modelo,"serie"=>$valor->serie,"id_parte"=>$valor->id_parte,"capacidad"=>$valor->capacidad,"marca"=>$valor->marca,"parte"=>$valor->parte);
        }
        
        $sql="UPDATE cat_equipos SET
        id_cliente = $datos->id_cliente,
        id_usuario = '{$this->session->logged}',
        id_sucursal = $datos->id_sucursal,
        id_tipo_equipo = $datos->tipo_equipo,
        id_descripcion_equipo = $datos->id_descripcion,
        capacidad = '$datos->capacidad',
        nombre_area = '$datos->nombre_area',
        id_area_climatiza = $datos->area_climatiza,
        codigo = '$datos->codigo',
        fecha = '$fecha',
        data_piezas = '".json_encode($json_piezas)."'
        WHERE id = $datos->id;";

        $this->conexion->link->query($sql);
        echo 1;
        #echo mysqli_affected_rows();
        #echo mysql_affected_rows();
        #print_r($sql);
    }
    
    public function getPartes(){
        $sql="SELECT * FROM cat_equiposPartes WHERE status = 1;";
        $data = $this->conexion->Consultas(2,$sql);
        return $data;
    }

    public function limpiar_string($string){
        $convert_from = array("á","Á","é","É","í","Í","ó","Ó","ú","Ú");
        $convert_to =   array("a","A","e","E","i","I","o","O","u","U");

        return str_replace($convert_from, $convert_to, $string);
    }
}
?>