<?php

/**
*
*/
class Configuracion
{

    private $conexion;
    private $session;

    public function __construct(){
        $this->conexion = new M_Conexion;
        $this->session = Session::getInstance();
    }

	 public function ListSupervisores(){
        extract($_POST);

        $sWhere = "";
        $sOrder = " ORDER BY id";
        $DesAsc = "DESC";
        $sOrder .= " {$DesAsc} ";
        $sLimit = "";
		$response = new stdClass;

        if(isset($_POST)){
            
            /*----------  ORDER BY ----------*/

            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 0){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY id {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY nombre {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY status {$DesAsc}";
            }

            /*----------  WHERE ----------*/

            if(isset($_POST['order_id']) && trim($_POST['order_id']) != ""){
                $sWhere .= " AND id = '".$_POST["order_id"]."'";
            }
            if(isset($_POST['order_name']) && trim($_POST['order_name']) != ""){
                $sWhere .= " AND nombre LIKE '%".$_POST["order_name"]."%'";
            }
            if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
                $sWhere .= " AND status = '".$_POST["order_status"]."'";
            }

            /*----------  LIMIT  ----------*/
            if(isset($_POST['length']) && $_POST['length'] > 0){
                $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
            }
        }

        $sql = "SELECT *, (SELECT COUNT(*) FROM cat_supervisores) AS totalRows FROM cat_supervisores
        WHERE 1=1 $sWhere $sOrder $sLimit";
        $response->recordsTotal = 0;
        $response->data = [];
        $res = $this->conexion->link->query($sql);
        while($fila = $res->fetch_assoc()){
            $fila = (object)$fila;
            $response->data[] = array (
                $fila->id,
                $fila->nombre,
                '<input id="color_'.$fila->id.'" class="form-control" type="color" value="'.$fila->color.'"/>',
                '<button class="btn '.(($fila->status == 1)?"green":"orange").'" id="status">'.(($fila->status == 1)?"Activo":"Inactivo").'</button>',
                '<button id="edit" class="btn blue">Editar</button><button id="delete" class="btn red">Eliminar</button>'
            );
			$response->recordsTotal = $fila->totalRows;
        }

        $response->recordsFiltered = count($response->data);
        $response->customActionMessage = "Informacion completada con exito";
        $response->customActionStatus = "OK";

        return json_encode($response);
    }

    public function addSupervisor(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(isset($postdata->id) && $postdata->id > 0){
            $sql = "UPDATE cat_supervisores SET nombre = '".$postdata->nombre."', color = '{$postdata->color}' WHERE id  = ".$postdata->id;
        }
        else{
            $sql = "INSERT INTO cat_supervisores SET nombre = '".$postdata->nombre."', color = '{$postdata->color}'";
        }
        $res = $this->conexion->link->query($sql);
        $this->refreshMantenimiento();
        return $res;
    }

    public function changeStatusSupervisor(){
        extract($_POST);
        $sql = "UPDATE cat_supervisores SET status = IF(status =  1, 0, 1)  WHERE id = '".$_POST["id"]."'";
        $res = $this->conexion->link->query($sql);
        $this->refreshMantenimiento();
        return $res;
    }

    public function deleteSupervisor(){
        extract($_POST);
        if(isset($_POST["id"]) && $_POST["id"] != ""){
            $sql = "DELETE FROM cat_supervisores WHERE id = '{$_POST['id']}'";
            $this->conexion->link->query($sql);
            $this->refreshMantenimiento();
            return true;
        }
        return false;
    }

    public function getSupervisores(){
        $sql = "SELECT * FROM cat_supervisores WHERE status = 1 ORDER BY id";
        $res = $this->conexion->link->query($sql);
        $d = array();
        while($fila = $res->fetch_assoc()){
            $d[] = $fila;
        }
        return json_encode($d);
    }

    public function getUsuarios(){
        $sql = "SELECT * FROM cat_usuarios WHERE status = 1 AND tipo_usuario = 2 ORDER BY id";
        $res = $this->conexion->link->query($sql);
        $d = array();
        while($fila = $res->fetch_assoc()){
            $d[] = $fila;
        }
        return json_encode($d);
    }

    public function ListUsuarios(){
        extract($_POST);

        $sWhere = "";
        $sOrder = " ORDER BY cat_usuarios.id";
        $DesAsc = "DESC";
        $sOrder .= " {$DesAsc} ";
        $sLimit = "";
        $response = new stdClass;

        if(isset($_POST)){
            
            /*----------  ORDER BY ----------*/

            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 0){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY cat_usuarios.id {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY cat_usuarios.nombre {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY cat_usuarios.email {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY cat_usuarios.movil {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY cat_usuarios.usuario {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 5){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY cat_usuarios.pass {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 6){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY (SELECT nombre FROM cat_tipos_usuario WHERE id = cat_usuarios.tipo_usuario) {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 7){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY cat_usuarios.status {$DesAsc}";
            }

            /*----------  WHERE ----------*/

            if(isset($_POST['order_id']) && trim($_POST['order_id']) != ""){
                $sWhere .= " AND cat_usuarios.id = '".$_POST["order_id"]."'";
            }
            if(isset($_POST['order_name']) && trim($_POST['order_name']) != ""){
                $sWhere .= " AND cat_usuarios.nombre LIKE '%".$_POST["order_name"]."%'";
            }
            if(isset($_POST['order_email']) && trim($_POST['order_email']) != ""){
                $sWhere .= " AND cat_usuarios.email LIKE '%".$_POST["order_email"]."%'";
            }
            if(isset($_POST['order_movil']) && trim($_POST['order_movil']) != ""){
                $sWhere .= " AND cat_usuarios.movil LIKE '%".$_POST["order_movil"]."%'";
            }
            if(isset($_POST['order_usuario']) && trim($_POST['order_usuario']) != ""){
                $sWhere .= " AND cat_usuarios.usuario LIKE '%".$_POST["order_usuario"]."%'";
            }
            if(isset($_POST['order_pass']) && trim($_POST['order_pass']) != ""){
                $sWhere .= " AND cat_usuarios.pass LIKE '%".$_POST["order_pass"]."%'";
            }
            if(isset($_POST['order_tipo']) && trim($_POST['order_tipo']) != ""){
                $sWhere .= " AND cat_tipos_usuario.nombre LIKE '%".$_POST["order_tipo"]."%'";
            }
            if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
                $sWhere .= " AND cat_usuarios.status = '".$_POST["order_status"]."'";
            }

            /*----------  LIMIT  ----------*/
            if(isset($_POST['length']) && $_POST['length'] > 0){
                $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
            }
        }

        $sql = "SELECT cat_usuarios.*, cat_usuarios.tipo_usuario_nombre AS tipo, id_supervisor
        FROM cat_usuarios
        LEFT JOIN cat_supervisores ON id_supervisor = cat_supervisores.id
        WHERE 1=1 $sWhere $sOrder $sLimit";
        $response->recordsTotal = 0;
        $response->data = [];
        $res = $this->conexion->link->query($sql);
        while($fila = $res->fetch_assoc()){
            $fila = (object)$fila;
            $response->data[] = array (
                $fila->id,
                $fila->nombre,
                $fila->email,
                $fila->movil,
                $fila->usuario,
                $fila->pass,
                $fila->tipo.'<input id="id_supervisor" type="hidden" value="'.$fila->id_supervisor.'"/>',
                '<button class="btn '.(($fila->status == 1)?"green":"orange").'" id="status">'.(($fila->status == 1)?"Activo":"Inactivo").'</button>',
                '<button id="edit" class="btn blue">Editar</button><button id="delete" class="btn red">Eliminar</button>'
            );
            $response->recordsTotal = $fila->totalRows;
        }

        $response->recordsFiltered = count($response->data);
        $response->customActionMessage = "Informacion completada con exito";
        $response->customActionStatus = "OK";

        return json_encode($response);
    }

    public function addUsuario(){
        $postdata = (object)json_decode(file_get_contents("php://input"));

        print_r($postdata);
        $tipo_usuario = "1";
        $nombre_tipo_usuario = "ADMINISTRADOR";



        if($postdata->tipo_usuario == 3){
            $tipo_usuario = "3";
            $nombre_tipo_usuario = "FACTURADOR";
        } else if($postdata->tipo_usuario == 4){
            $tipo_usuario = "1";
            $nombre_tipo_usuario = "OPERATIVO";
        } else if($postdata->tipo_usuario == 5){
            $tipo_usuario = "5";
            $nombre_tipo_usuario = "SUPERVISOR";
        } 

        if(isset($postdata->id) && $postdata->id > 0){
            $sql = "UPDATE cat_usuarios SET nombre = '".$postdata->nombre."', email = '{$postdata->email}', movil = '{$postdata->movil}', usuario = '{$postdata->usuario}', pass = '{$postdata->pass}', status = '1', tipo_usuario = '{$tipo_usuario}', tipo_usuario_nombre = '{$nombre_tipo_usuario}', id_supervisor = '{$postdata->supervisor}' WHERE id  = ".$postdata->id;
        }else{
            $sql = "INSERT INTO cat_usuarios SET nombre = '".$postdata->nombre."', email = '{$postdata->email}', movil = '{$postdata->movil}', usuario = '{$postdata->usuario}', pass = '{$postdata->pass}', status = '1', tipo_usuario = '{$tipo_usuario}', tipo_usuario_nombre = '{$nombre_tipo_usuario}', id_supervisor = '{$postdata->supervisor}'";
        }
        return $this->conexion->link->query($sql);
    }

    public function changeStatusUsuario(){
        extract($_POST);
        $sql = "UPDATE cat_usuarios SET status = IF(status =  1, 0, 1)  WHERE id = '".$_POST["id"]."' AND tipo_usuario = 2";
        return $this->conexion->link->query($sql);
    }

    public function deleteUsuario(){
        extract($_POST);
        if(isset($_POST["id"]) && $_POST["id"] != ""){
            $sql = "DELETE FROM cat_usuarios WHERE id = '{$_POST["id"]}'";
            $this->conexion->link->query($sql);
            return true;
        }
        return false;
    }

    public function getAuxiliares(){
        $sql = "SELECT * FROM cat_auxiliares WHERE status = 1 ORDER BY nombre";
        $res = $this->conexion->link->query($sql);
        $d = array();
        while($fila = $res->fetch_assoc()){
            $d[] = $fila;
        }
        return json_encode($d);
    }

    public function getVehiculos(){
        $sql = "SELECT * FROM cat_vehiculos WHERE status = 1 ORDER BY nombre";
        $res = $this->conexion->link->query($sql);
        $d = array();
        while($fila = $res->fetch_assoc()){
            $d[] = $fila;
        }
        return json_encode($d);
    }

    public function ListAuxiliares(){
        extract($_POST);

        $sWhere = "";
        $sOrder = " ORDER BY id";
        $DesAsc = "DESC";
        $sOrder .= " {$DesAsc} ";
        $sLimit = "";
        $response = new stdClass;

        if(isset($_POST)){
            
            /*----------  ORDER BY ----------*/

            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 0){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY id {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY nombre {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY status {$DesAsc}";
            }

            /*----------  WHERE ----------*/

            if(isset($_POST['order_id']) && trim($_POST['order_id']) != ""){
                $sWhere .= " AND id = '".$_POST["order_id"]."'";
            }
            if(isset($_POST['order_name']) && trim($_POST['order_name']) != ""){
                $sWhere .= " AND nombre LIKE '%".$_POST["order_name"]."%'";
            }
            if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
                $sWhere .= " AND status = '".$_POST["order_status"]."'";
            }

            /*----------  LIMIT  ----------*/
            if(isset($_POST['length']) && $_POST['length'] > 0){
                $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
            }
        }

        $sql = "SELECT *, (SELECT COUNT(*) FROM cat_auxiliares) AS totalRows FROM cat_auxiliares
        WHERE 1=1 $sWhere $sOrder $sLimit";
        $response->recordsTotal = 0;
        $response->data = [];       
        $res = $this->conexion->link->query($sql);
        while($fila = $res->fetch_assoc()){
            $fila = (object)$fila;
            $response->data[] = array (
                $fila->id,
                $fila->nombre,
                '<input id="color_'.$fila->id.'" class="form-control" type="color" value="'.$fila->color.'"/>',
                '<button class="btn '.(($fila->status == 1)?"green":"orange").'" id="status">'.(($fila->status == 1)?"Activo":"Inactivo").'</button>',
                '<button id="edit" class="btn blue">Editar</button><button id="delete" class="btn red">Eliminar</button>'
            );
            $response->recordsTotal = $fila->totalRows;
        }

        $response->recordsFiltered = count($response->data);
        $response->customActionMessage = "Informacion completada con exito";
        $response->customActionStatus = "OK";

        return json_encode($response);
    }

    public function addAuxiliar(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(isset($postdata->id) && $postdata->id > 0){
            $sql = "UPDATE cat_auxiliares SET nombre = '".$postdata->nombre."', color = '{$postdata->color}' WHERE id  = ".$postdata->id;
        }
        else{
            $sql = "INSERT INTO cat_auxiliares SET nombre = '".$postdata->nombre."', color = '{$postdata->color}'";
        }
        return $this->conexion->link->query($sql);
    }

    public function changeStatusAuxiliar(){
        extract($_POST);
        $sql = "UPDATE cat_auxiliares SET status = IF(status =  1, 0, 1)  WHERE id = '".$_POST["id"]."'";
        return $this->conexion->link->query($sql);
    }

    public function deleteAuxiliar(){
        extract($_POST);
        if(isset($_POST["id"]) && $_POST["id"] != ""){
            $sql = "DELETE FROM cat_auxiliares WHERE id = '{$_POST['id']}'";
            $this->conexion->link->query($sql);
            return true;
        }
        return false;
    }

    public function ListVehiculos(){
        extract($_POST);

        $sWhere = "";
        $sOrder = " ORDER BY id";
        $DesAsc = "DESC";
        $sOrder .= " {$DesAsc} ";
        $sLimit = "";
        $response = new stdClass;

        if(isset($_POST)){
            
            /*----------  ORDER BY ----------*/

            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 0){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY id {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY nombre {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY status {$DesAsc}";
            }

            /*----------  WHERE ----------*/

            if(isset($_POST['order_id']) && trim($_POST['order_id']) != ""){
                $sWhere .= " AND id = '".$_POST["order_id"]."'";
            }
            if(isset($_POST['order_name']) && trim($_POST['order_name']) != ""){
                $sWhere .= " AND nombre LIKE '%".$_POST["order_name"]."%'";
            }
            if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
                $sWhere .= " AND status = '".$_POST["order_status"]."'";
            }

            /*----------  LIMIT  ----------*/
            if(isset($_POST['length']) && $_POST['length'] > 0){
                $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
            }
        }

        $sql = "SELECT *, (SELECT COUNT(*) FROM cat_vehiculos) AS totalRows FROM cat_vehiculos
        WHERE 1=1 $sWhere $sOrder $sLimit";
        $response->recordsTotal = 0;
        $response->data = [];       
        $res = $this->conexion->link->query($sql);
        while($fila = $res->fetch_assoc()){
            $fila = (object)$fila;
            $response->data[] = array (
                $fila->id,
                $fila->nombre,
                '<button class="btn '.(($fila->status == 1)?"green":"orange").'" id="status">'.(($fila->status == 1)?"Activo":"Inactivo").'</button>',
                '<button id="edit" class="btn blue">Editar</button><button id="delete" class="btn red">Eliminar</button>'
            );
            $response->recordsTotal = $fila->totalRows;
        }

        $response->recordsFiltered = count($response->data);
        $response->customActionMessage = "Informacion completada con exito";
        $response->customActionStatus = "OK";

        return json_encode($response);
    }

    public function addVehiculo(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(isset($postdata->id) && $postdata->id > 0){
            $sql = "UPDATE cat_vehiculos SET nombre = '".$postdata->nombre."' WHERE id  = ".$postdata->id;
        }
        else{
            $sql = "INSERT INTO cat_vehiculos SET nombre = '".$postdata->nombre."'";
        }
        return $this->conexion->link->query($sql);
    }

    public function changeStatusVehiculo(){
        extract($_POST);
        $sql = "UPDATE cat_vehiculos SET status = IF(status =  1, 0, 1)  WHERE id = '".$_POST["id"]."'";
        return $this->conexion->link->query($sql);
    }

    public function deleteVehiculo(){
        extract($_POST);
        if(isset($_POST["id"]) && $_POST["id"] != ""){
            $sql = "DELETE FROM cat_vehiculos WHERE id = '{$_POST['id']}'";
            $this->conexion->link->query($sql);
            return true;
        }
        return false;
    }

    public function ListDescripcionesEquipos(){
        extract($_POST);

        $sWhere = "";
        $sOrder = " ORDER BY cat_descripciones_equipos.id";
        $DesAsc = "DESC";
        $sOrder .= " {$DesAsc} ";
        $sLimit = "";
        $response = new stdClass;

        if(isset($_POST)){
            
            /*----------  ORDER BY ----------*/

            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 0){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY cat_descripciones_equipos.id {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY cat_descripciones_equipos.nombre {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY cat_equiposTipo.nombre {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY cat_descripciones_equipos.status {$DesAsc}";
            }

            /*----------  WHERE ----------*/

            if(isset($_POST['order_id']) && trim($_POST['order_id']) != ""){
                $sWhere .= " AND cat_descripciones_equipos.id = '".$_POST["order_id"]."'";
            }
            if(isset($_POST['order_tipo']) && trim($_POST['order_tipo']) != ""){
                $sWhere .= " AND cat_equiposTipo.nombre LIKE '%".$_POST["order_tipo"]."%'";
            }
            if(isset($_POST['order_name']) && trim($_POST['order_name']) != ""){
                $sWhere .= " AND cat_descripciones_equipos.nombre LIKE '%".$_POST["order_name"]."%'";
            }
            if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
                $sWhere .= " AND cat_descripciones_equipos.status = '".$_POST["order_status"]."'";
            }

            /*----------  LIMIT  ----------*/
            if(isset($_POST['length']) && $_POST['length'] > 0){
                $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
            }
        }

        $sql = "SELECT cat_descripciones_equipos.*, cat_equiposTipo.nombre as tipo, (SELECT COUNT(*) FROM cat_descripciones_equipos) AS totalRows FROM cat_descripciones_equipos
        INNER JOIN cat_equiposTipo ON cat_descripciones_equipos.id_tipoequipo = cat_equiposTipo.id
        WHERE 1=1 $sWhere $sOrder $sLimit";
        $response->recordsTotal = 0;
        $response->data = [];       
        $res = $this->conexion->link->query($sql);
        while($fila = $res->fetch_assoc()){
            $fila = (object)$fila;
            $response->data[] = array (
                $fila->id,
                $fila->nombre,
                $fila->tipo,
                '<button class="btn '.(($fila->status == 1)?"green":"orange").'" id="status">'.(($fila->status == 1)?"Activo":"Inactivo").'</button>',
                '<button id="edit" class="btn blue">Editar</button><button id="delete" class="btn red">Eliminar</button>'
            );
            $response->recordsTotal = $fila->totalRows;
        }

        $response->recordsFiltered = count($response->data);
        $response->customActionMessage = "Informacion completada con exito";
        $response->customActionStatus = "OK";

        return json_encode($response);
    }

    public function getTiposEquipo(){
        $sql = "SELECT * FROM cat_equiposTipo WHERE status = 1 ORDER BY id";
        $res = $this->conexion->link->query($sql);
        $d = array();
        while($fila = $res->fetch_assoc()){
            $d[] = $fila;
        }
        return json_encode($d);
    }

    public function getPartesEquipo(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $sql = "SELECT * FROM cat_equiposPartes WHERE status = 1 ORDER BY id";
        $res = $this->conexion->link->query($sql);
        $d = array();
        while($fila = $res->fetch_assoc()){
            $d[] = $fila;
        }
        return json_encode($d);   
    }

    public function addDescripcion(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(isset($postdata->id) && $postdata->id > 0 && isset($postdata->id_tipoequipo) && $postdata->id_tipoequipo > 0){
            $sql = "UPDATE cat_descripciones_equipos SET nombre = '".$postdata->nombre."', id_tipoequipo = '".$postdata->id_tipoequipo."' WHERE id  = ".$postdata->id;
        }
        else{
            $sql = "INSERT INTO cat_descripciones_equipos SET nombre = '".$postdata->nombre."', id_tipoequipo = '".$postdata->id_tipoequipo."'";
        }
        $res = $this->conexion->link->query($sql);
        $this->refreshMantenimiento();
        return $res;
    }

    public function changeStatusDescripcion(){
        extract($_POST);
        $sql = "UPDATE cat_descripciones_equipos SET status = IF(status =  1, 0, 1)  WHERE id = '".$_POST["id"]."'";
        $res = $this->conexion->link->query($sql);
        $this->refreshMantenimiento();
        return $res;
    }

    public function deleteDescripcion(){
        extract($_POST);
        if(isset($_POST["id"]) && $_POST["id"] != ""){
            $sql = "DELETE FROM cat_descripciones_equipos WHERE id = '{$_POST['id']}'";
            $this->conexion->link->query($sql);
            $this->refreshMantenimiento();
            return true;
        }
        return false;
    }

    public function ListMarcas(){
        extract($_POST);

        $sWhere = "";
        $sOrder = " ORDER BY id";
        $DesAsc = "DESC";
        $sOrder .= " {$DesAsc} ";
        $sLimit = "";
        $response = new stdClass;

        if(isset($_POST)){
            
            /*----------  ORDER BY ----------*/

            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 0){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY id {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY nombre {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY status {$DesAsc}";
            }

            /*----------  WHERE ----------*/

            if(isset($_POST['order_id']) && trim($_POST['order_id']) != ""){
                $sWhere .= " AND id = '".$_POST["order_id"]."'";
            }
            if(isset($_POST['order_name']) && trim($_POST['order_name']) != ""){
                $sWhere .= " AND nombre LIKE '%".$_POST["order_name"]."%'";
            }
            if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
                $sWhere .= " AND status = '".$_POST["order_status"]."'";
            }

            /*----------  LIMIT  ----------*/
            if(isset($_POST['length']) && $_POST['length'] > 0){
                $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
            }
        }

        $sql = "SELECT *, (SELECT COUNT(*) FROM cat_marcas) AS totalRows FROM cat_marcas
        WHERE 1=1 $sWhere $sOrder $sLimit";
        $response->recordsTotal = 0;
        $response->data = [];       
        $res = $this->conexion->link->query($sql);
        while($fila = $res->fetch_assoc()){
            $fila = (object)$fila;
            $response->data[] = array (
                $fila->id,
                $fila->nombre,
                '<button class="btn '.(($fila->status == 1)?"green":"orange").'" id="status">'.(($fila->status == 1)?"Activo":"Inactivo").'</button>',
                '<button id="edit" class="btn blue">Editar</button><button id="delete" class="btn red">Eliminar</button>'
            );
            $response->recordsTotal = $fila->totalRows;
        }

        $response->recordsFiltered = count($response->data);
        $response->customActionMessage = "Informacion completada con exito";
        $response->customActionStatus = "OK";

        return json_encode($response);
    }

    public function addMarca(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(isset($postdata->id) && $postdata->id > 0 && isset($postdata->id_tipoequipo) && $postdata->id_tipoequipo > 0){
            $sql = "UPDATE cat_marcas SET nombre = '".$postdata->nombre."' WHERE id  = ".$postdata->id;
        }
        else{
            $sql = "INSERT INTO cat_marcas SET nombre = '".$postdata->nombre."'";
        }
        $res = $this->conexion->link->query($sql);
        $this->refreshMantenimiento();
        return $res;
    }

    public function changeStatusMarca(){
        extract($_POST);
        $sql = "UPDATE cat_marcas SET status = IF(status =  1, 0, 1)  WHERE id = '".$_POST["id"]."'";
        $res = $this->conexion->link->query($sql);
        $this->refreshMantenimiento();
        return $res;
    }

    public function deleteMarca(){
        extract($_POST);
        if(isset($_POST["id"]) && $_POST["id"] != ""){
            $sql = "DELETE FROM cat_marcas WHERE id = '{$_POST['id']}'";
            $this->conexion->link->query($sql);
            $this->refreshMantenimiento();
            return true;
        }
        return false;
    }

    public function ListAreas(){
        extract($_POST);

        $sWhere = "";
        $sOrder = " ORDER BY cat_areas.id";
        $DesAsc = "DESC";
        $sOrder .= " {$DesAsc} ";
        $sLimit = "";
        $response = new stdClass;

        if(isset($_POST)){
            
            /*----------  ORDER BY ----------*/

            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 0){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY cat_areas.id {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY cat_areas.nombre {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY cat_equiposTipo.nombre {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY cat_areas.status {$DesAsc}";
            }

            /*----------  WHERE ----------*/

            if(isset($_POST['order_id']) && trim($_POST['order_id']) != ""){
                $sWhere .= " AND cat_areas.id = '".$_POST["order_id"]."'";
            }
            if(isset($_POST['order_name']) && trim($_POST['order_name']) != ""){
                $sWhere .= " AND cat_areas.nombre LIKE '%".$_POST["order_name"]."%'";
            }
            if(isset($_POST['order_tipo']) && trim($_POST['order_tipo']) != ""){
                $sWhere .= " AND cat_equiposTipo.nombre LIKE '%".$_POST["order_tipo"]."%'";
            }
            if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
                $sWhere .= " AND cat_areas.status = '".$_POST["order_status"]."'";
            }

            /*----------  LIMIT  ----------*/
            if(isset($_POST['length']) && $_POST['length'] > 0){
                $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
            }
        }

        $sql = "SELECT cat_areas.*, cat_equiposTipo.nombre as tipo_equipo, (SELECT COUNT(*) FROM cat_areas) AS totalRows FROM cat_areas
        INNER JOIN cat_equiposTipo ON cat_equiposTipo.id = id_tipoequipo
        WHERE 1=1 $sWhere $sOrder $sLimit";
        $response->recordsTotal = 0;
        $response->data = [];       
        $res = $this->conexion->link->query($sql);
        while($fila = $res->fetch_assoc()){
            $fila = (object)$fila;
            $response->data[] = array (
                $fila->id,
                $fila->nombre,
                $fila->tipo_equipo,
                '<button class="btn '.(($fila->status == 1)?"green":"orange").'" id="status">'.(($fila->status == 1)?"Activo":"Inactivo").'</button>',
                '<button id="edit" class="btn blue">Editar</button><button id="delete" class="btn red">Eliminar</button>'
            );
            $response->recordsTotal = $fila->totalRows;
        }

        $response->recordsFiltered = count($response->data);
        $response->customActionMessage = "Informacion completada con exito";
        $response->customActionStatus = "OK";

        return json_encode($response);
    }

    public function addArea(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(isset($postdata->id) && $postdata->id > 0 && isset($postdata->id_tipoequipo) && $postdata->id_tipoequipo > 0){
            $sql = "UPDATE cat_areas SET nombre = '".$postdata->nombre."', id_tipoequipo = '".$postdata->id_tipoequipo."' WHERE id  = ".$postdata->id;
        }
        else{
            $sql = "INSERT INTO cat_areas SET nombre = '".$postdata->nombre."', id_tipoequipo = '".$postdata->id_tipoequipo."'";
        }
        $res = $this->conexion->link->query($sql);
        $this->refreshMantenimiento();
        return $res;
    }

    public function changeStatusArea(){
        extract($_POST);
        $sql = "UPDATE cat_areas SET status = IF(status =  1, 0, 1)  WHERE id = '".$_POST["id"]."'";
        $res = $this->conexion->link->query($sql);
        $this->refreshMantenimiento();
        return $res;
    }

    public function deleteArea(){
        extract($_POST);
        if(isset($_POST["id"]) && $_POST["id"] != ""){
            $sql = "DELETE FROM cat_areas WHERE id = '{$_POST['id']}'";
            $this->conexion->link->query($sql);
            $this->refreshMantenimiento();
            return true;
        }
        return false;
    }

    public function ListMotores(){
        extract($_POST);

        $sWhere = "";
        $sOrder = " ORDER BY id";
        $DesAsc = "DESC";
        $sOrder .= " {$DesAsc} ";
        $sLimit = "";
        $response = new stdClass;

        if(isset($_POST)){
            
            /*----------  ORDER BY ----------*/

            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 0){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY id {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY descripcion {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY status {$DesAsc}";
            }

            /*----------  WHERE ----------*/

            if(isset($_POST['order_id']) && trim($_POST['order_id']) != ""){
                $sWhere .= " AND id = '".$_POST["order_id"]."'";
            }
            if(isset($_POST['order_name']) && trim($_POST['order_name']) != ""){
                $sWhere .= " AND descripcion LIKE '%".$_POST["order_name"]."%'";
            }
            if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
                $sWhere .= " AND status = '".$_POST["order_status"]."'";
            }

            /*----------  LIMIT  ----------*/
            if(isset($_POST['length']) && $_POST['length'] > 0){
                $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
            }
        }

        $sql = "SELECT *, (SELECT COUNT(*) FROM cat_motores) AS totalRows FROM cat_motores
        WHERE 1=1 $sWhere $sOrder $sLimit";
        $response->recordsTotal = 0;
        $response->data = [];       
        $res = $this->conexion->link->query($sql);
        while($fila = $res->fetch_assoc()){
            $fila = (object)$fila;
            $response->data[] = array (
                $fila->id,
                $fila->descripcion,
                '<button class="btn '.(($fila->status == 1)?"green":"orange").'" id="status">'.(($fila->status == 1)?"Activo":"Inactivo").'</button>',
                '<button id="edit" class="btn blue">Editar</button><button id="delete" class="btn red">Eliminar</button>'
            );
            $response->recordsTotal = $fila->totalRows;
        }

        $response->recordsFiltered = count($response->data);
        $response->customActionMessage = "Informacion completada con exito";
        $response->customActionStatus = "OK";

        return json_encode($response);
    }

    public function addMotor(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(isset($postdata->id) && ((int)$postdata->id) > 0){
            $sql = "UPDATE cat_motores SET nombre = '".$postdata->nombre."' WHERE id = {$postdata->id}";
        }
        else{
            $sql = "INSERT INTO cat_motores SET nombre = '".$postdata->nombre."'";
        }
        $res = $this->conexion->link->query($sql);
        $this->refreshMantenimiento();
        return $res;
    }

    public function changeStatusMotor(){
        extract($_POST);
        $sql = "UPDATE cat_motores SET status = IF(status =  1, 0, 1)  WHERE id = '".$_POST["id"]."'";
        $res = $this->conexion->link->query($sql);
        $this->refreshMantenimiento();
        return $res;
    }

    public function deleteMotor(){
        extract($_POST);
        if(isset($_POST["id"]) && $_POST["id"] != ""){
            $sql = "DELETE FROM cat_motores WHERE id = '{$_POST['id']}'";
            $this->conexion->link->query($sql);
            $this->refreshMantenimiento();
            return true;
        }
        return false;
    }

    public function ListRefrigerantes(){
        extract($_POST);

        $sWhere = "";
        $sOrder = " ORDER BY id";
        $DesAsc = "DESC";
        $sOrder .= " {$DesAsc} ";
        $sLimit = "";
        $response = new stdClass;

        if(isset($_POST)){
            
            /*----------  ORDER BY ----------*/

            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 0){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY id {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY descripcion {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY status {$DesAsc}";
            }

            /*----------  WHERE ----------*/

            if(isset($_POST['order_id']) && trim($_POST['order_id']) != ""){
                $sWhere .= " AND id = '".$_POST["order_id"]."'";
            }
            if(isset($_POST['order_name']) && trim($_POST['order_name']) != ""){
                $sWhere .= " AND descripcion LIKE '%".$_POST["order_name"]."%'";
            }
            if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
                $sWhere .= " AND status = '".$_POST["order_status"]."'";
            }

            /*----------  LIMIT  ----------*/
            if(isset($_POST['length']) && $_POST['length'] > 0){
                $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
            }
        }

        $sql = "SELECT *, (SELECT COUNT(*) FROM cat_refrigerantes) AS totalRows FROM cat_refrigerantes
        WHERE 1=1 $sWhere $sOrder $sLimit";
        $response->recordsTotal = 0;
        $response->data = [];       
        $res = $this->conexion->link->query($sql);
        while($fila = $res->fetch_assoc()){
            $fila = (object)$fila;
            $response->data[] = array (
                $fila->id,
                $fila->descripcion,
                '<button class="btn '.(($fila->status == 1)?"green":"orange").'" id="status">'.(($fila->status == 1)?"Activo":"Inactivo").'</button>',
                '<button id="edit" class="btn blue">Editar</button><button id="delete" class="btn red">Eliminar</button>'
            );
            $response->recordsTotal = $fila->totalRows;
        }

        $response->recordsFiltered = count($response->data);
        $response->customActionMessage = "Informacion completada con exito";
        $response->customActionStatus = "OK";

        return json_encode($response);
    }

    public function addRefrigerante(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(isset($postdata->id) && ((int)$postdata->id > 0)) {
            $sql = "UPDATE cat_refrigerantes SET descripcion = '".$postdata->nombre."' WHERE id = '{$postdata->id}'";
        }
        else{
            $sql = "INSERT INTO cat_refrigerantes SET descripcion = '".$postdata->nombre."'";
        }
        $res = $this->conexion->link->query($sql);
        $this->refreshMantenimiento();
        return $res;
    }

    public function changeStatusRefrigerante(){
        extract($_POST);
        $sql = "UPDATE cat_refrigerantes SET status = IF(status =  1, 0, 1)  WHERE id = '".$_POST["id"]."'";
        $res = $this->conexion->link->query($sql);
        $this->refreshMantenimiento();
        return $res;
    }

    public function deleteRefrigerante(){
        extract($_POST);
        if(isset($_POST["id"]) && $_POST["id"] != ""){
            $sql = "DELETE FROM cat_refrigerantes WHERE id = '{$_POST['id']}'";
            $this->conexion->link->query($sql);
            $this->refreshMantenimiento();
            return true;
        }
        return false;
    }

    public function ListPiezas(){
        extract($_POST);

        $sWhere = "";
        $sOrder = " ORDER BY cat_piezas.id";
        $DesAsc = "DESC";
        $sOrder .= " {$DesAsc} ";
        $sLimit = "";
        $response = new stdClass;

        if(isset($_POST)){
            
            /*----------  ORDER BY ----------*/

            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 0){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY cat_piezas.id {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY cat_piezas.descripcion {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY cat_equiposPartes.nombre {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY cat_equiposTipo.nombre {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY cat_piezas.status {$DesAsc}";
            }

            /*----------  WHERE ----------*/

            if(isset($_POST['order_id']) && trim($_POST['order_id']) != ""){
                $sWhere .= " AND cat_piezas.id = '".$_POST["order_id"]."'";
            }
            if(isset($_POST['order_name']) && trim($_POST['order_name']) != ""){
                $sWhere .= " AND cat_piezas.descripcion LIKE '%".$_POST["order_name"]."%'";
            }
            if(isset($_POST['order_parte']) && trim($_POST['order_parte']) != ""){
                $sWhere .= " AND cat_equiposPartes.nombre LIKE '%".$_POST["order_parte"]."%'";
            }
            if(isset($_POST['order_tipo']) && trim($_POST['order_tipo']) != ""){
                $sWhere .= " AND cat_equiposTipo.nombre LIKE '%".$_POST["order_tipo"]."%'";
            }
            if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
                $sWhere .= " AND cat_piezas.status = '".$_POST["order_status"]."'";
            }

            /*----------  LIMIT  ----------*/
            if(isset($_POST['length']) && $_POST['length'] > 0){
                $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
            }
        }

        $sql = "SELECT cat_piezas.*, cat_equiposTipo.nombre as tipo_equipo, cat_equiposPartes.nombre as parte, (SELECT COUNT(*) FROM cat_piezas) AS totalRows FROM cat_piezas
        INNER JOIN cat_equiposTipo ON cat_piezas.id_tipoequipo = cat_equiposTipo.id
        INNER JOIN cat_equiposPartes ON cat_piezas.id_parte_equipo = cat_equiposPartes.id
        WHERE 1=1 $sWhere $sOrder $sLimit";
        $response->recordsTotal = 0;
        $response->data = [];       
        $res = $this->conexion->link->query($sql);
        while($fila = $res->fetch_assoc()){
            $fila = (object)$fila;
            $response->data[] = array (
                $fila->id,
                $fila->descripcion,
                $fila->parte,
                $fila->tipo_equipo,
                '<button class="btn '.(($fila->status == 1)?"green":"orange").'" id="status">'.(($fila->status == 1)?"Activo":"Inactivo").'</button>',
                '<button id="edit" class="btn blue">Editar</button><button id="delete" class="btn red">Eliminar</button>'
            );
            $response->recordsTotal = $fila->totalRows;
        }

        $response->recordsFiltered = count($response->data);
        $response->customActionMessage = "Informacion completada con exito";
        $response->customActionStatus = "OK";

        return json_encode($response);
    }

    public function addPieza(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(isset($postdata->id) && ((int)$postdata->id > 0) && isset($postdata->id_tipoequipo) && ((int)$postdata->id_tipoequipo) > 0 && isset($postdata->id_parte_equipo) && ((int)$postdata->id_parte_equipo) > 0) {
            $sql = "UPDATE cat_piezas SET descripcion = '".$postdata->nombre."', id_tipoequipo = '{$postdata->id_tipoequipo}', id_parte_equipo = '{$postdata->id_parte_equipo}' WHERE id = '{$postdata->id}'";
        }
        else{
            $sql = "INSERT INTO cat_piezas SET descripcion = '".$postdata->nombre."', id_tipoequipo = '{$postdata->id_tipoequipo}', id_parte_equipo = '{$postdata->id_parte_equipo}'";
        }
        $res = $this->conexion->link->query($sql);
        $this->refreshMantenimiento();
        return $res;
    }

    public function changeStatusPieza(){
        extract($_POST);
        $sql = "UPDATE cat_piezas SET status = IF(status =  1, 0, 1)  WHERE id = '".$_POST["id"]."'";
        $res = $this->conexion->link->query($sql);
        $this->refreshMantenimiento();
        return $res;
    }

    public function deletePieza(){
        extract($_POST);
        if(isset($_POST["id"]) && $_POST["id"] != ""){
            $sql = "DELETE FROM cat_piezas WHERE id = '{$_POST['id']}'";
            $this->conexion->link->query($sql);
            $this->refreshMantenimiento();
            return true;
        }
        return false;
    }

    public function ListHerramientas(){
        extract($_POST);

        $sWhere = "";
        $sOrder = " ORDER BY id";
        $DesAsc = "DESC";
        $sOrder .= " {$DesAsc} ";
        $sLimit = "";
        $response = new stdClass;

        if(isset($_POST)){
            
            /*----------  ORDER BY ----------*/

            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 0){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY id {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY herramienta {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY status {$DesAsc}";
            }

            /*----------  WHERE ----------*/

            if(isset($_POST['order_id']) && trim($_POST['order_id']) != ""){
                $sWhere .= " AND id = '".$_POST["order_id"]."'";
            }
            if(isset($_POST['order_name']) && trim($_POST['order_name']) != ""){
                $sWhere .= " AND herramienta LIKE '%".$_POST["order_name"]."%'";
            }
            if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
                $sWhere .= " AND status = '".$_POST["order_status"]."'";
            }

            /*----------  LIMIT  ----------*/
            if(isset($_POST['length']) && $_POST['length'] > 0){
                $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
            }
        }

        $sql = "SELECT *, (SELECT COUNT(*) FROM cat_herramientas_trabajo) AS totalRows FROM cat_herramientas_trabajo
        WHERE 1=1 $sWhere $sOrder $sLimit";
        $response->recordsTotal = 0;
        $response->data = [];       
        $res = $this->conexion->link->query($sql);
        while($fila = $res->fetch_assoc()){
            $fila = (object)$fila;
            $response->data[] = array (
                $fila->id,
                $fila->herramienta,
                '<button class="btn '.(($fila->status == 1)?"green":"orange").'" id="status">'.(($fila->status == 1)?"Activo":"Inactivo").'</button>',
                '<button id="edit" class="btn blue">Editar</button><button id="delete" class="btn red">Eliminar</button>'
            );
            $response->recordsTotal = $fila->totalRows;
        }

        $response->recordsFiltered = count($response->data);
        $response->customActionMessage = "Informacion completada con exito";
        $response->customActionStatus = "OK";

        return json_encode($response);
    }

    public function addHerramienta(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(isset($postdata->id) && ((int)$postdata->id) > 0){
            $sql = "UPDATE cat_herramientas_trabajo SET herramienta = '".$postdata->nombre."' WHERE id = {$postdata->id}";
        }
        else{
            $sql = "INSERT INTO cat_herramientas_trabajo SET herramienta = '".$postdata->nombre."'";
        }
        $res = $this->conexion->link->query($sql);
        $this->refreshMantenimiento();
        return $res;
    }

    public function changeStatusHerramienta(){
        extract($_POST);
        $sql = "UPDATE cat_herramientas_trabajo SET status = IF(status =  1, 0, 1)  WHERE id = '".$_POST["id"]."'";
        $res = $this->conexion->link->query($sql);
        $this->refreshMantenimiento();
        return $res;
    }

    public function deleteHerramienta(){
        extract($_POST);
        if(isset($_POST["id"]) && $_POST["id"] != ""){
            $sql = "DELETE FROM cat_herramientas_trabajo WHERE id = '{$_POST['id']}'";
            $this->conexion->link->query($sql);
            $this->refreshMantenimiento();
            return true;
        }
        return false;
    }

    public function ListMateriales(){
        extract($_POST);

        $sWhere = "";
        $sOrder = " ORDER BY id";
        $DesAsc = "DESC";
        $sOrder .= " {$DesAsc} ";
        $sLimit = "";
        $response = new stdClass;

        if(isset($_POST)){
            
            /*----------  ORDER BY ----------*/

            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 0){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY id {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY item {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY descripcion {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY unidad {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY status {$DesAsc}";
            }

            /*----------  WHERE ----------*/

            if(isset($_POST['order_id']) && trim($_POST['order_id']) != ""){
                $sWhere .= " AND id = '".$_POST["order_id"]."'";
            }
            if(isset($_POST['order_item']) && trim($_POST['order_item']) != ""){
                $sWhere .= " AND item LIKE '%".$_POST["order_item"]."%'";
            }
            if(isset($_POST['order_descripcion']) && trim($_POST['order_descripcion']) != ""){
                $sWhere .= " AND descripcion LIKE '%".$_POST["order_descripcion"]."%'";
            }
            if(isset($_POST['order_unidad']) && trim($_POST['order_unidad']) != ""){
                $sWhere .= " AND unidad LIKE '%".$_POST["order_unidad"]."%'";
            }
            if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
                $sWhere .= " AND status = '".$_POST["order_status"]."'";
            }

            /*----------  LIMIT  ----------*/
            if(isset($_POST['length']) && $_POST['length'] > 0){
                $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
            }
        }

        $sql = "SELECT *, (SELECT COUNT(*) FROM cat_materiales) AS totalRows FROM cat_materiales
        WHERE 1=1 $sWhere $sOrder $sLimit";
        $response->recordsTotal = 0;
        $response->data = [];       
        $res = $this->conexion->link->query($sql);
        while($fila = $res->fetch_assoc()){
            $fila = (object)$fila;
            $response->data[] = array (
                $fila->id,
                $fila->item,
                $fila->descripcion,
                $fila->unidad,
                '<button class="btn '.(($fila->status == 1)?"green":"orange").'" id="status">'.(($fila->status == 1)?"Activo":"Inactivo").'</button>',
                '<button id="edit" class="btn blue">Editar</button><button id="delete" class="btn red">Eliminar</button>'
            );
            $response->recordsTotal = $fila->totalRows;
        }

        $response->recordsFiltered = count($response->data);
        $response->customActionMessage = "Informacion completada con exito";
        $response->customActionStatus = "OK";

        return json_encode($response);
    }

    public function addMaterial(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(isset($postdata->id) && ((int)$postdata->id) > 0){
            $sql = "UPDATE cat_materiales SET item = '".$postdata->item."', descripcion = '{$postdata->nombre}', unidad = '{$postdata->unidad}' WHERE id = {$postdata->id}";
        }
        else{
            $sql = "INSERT INTO cat_materiales SET item = '".$postdata->item."', descripcion = '{$postdata->nombre}', unidad = '{$postdata->unidad}'";
        }
        $res = $this->conexion->link->query($sql);
        $this->refreshMantenimiento();
        return $res;
    }

    public function changeStatusMaterial(){
        extract($_POST);
        $sql = "UPDATE cat_materiales SET status = IF(status =  1, 0, 1)  WHERE id = '".$_POST["id"]."'";
        $res = $this->conexion->link->query($sql);
        $this->refreshMantenimiento();
        return $res;
    }

    public function deleteMaterial(){
        extract($_POST);
        if(isset($_POST["id"]) && $_POST["id"] != ""){
            $sql = "DELETE FROM cat_materiales WHERE id = '{$_POST['id']}'";
            $this->conexion->link->query($sql);
            $this->refreshMantenimiento();
            return true;
        }
        return false;
    }

    private function refreshMantenimiento(){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://cegaservices2.procesos-iq.com/controllers/index.php?accion=ApiProntoforms.refreshMantenimiento");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,"");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch);
        curl_close ($ch);
    }
}

?>
