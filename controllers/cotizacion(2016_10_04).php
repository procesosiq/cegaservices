<?php
/**
* 
*/
class cotizacion
{
	
	private $conexion;
    private $session;
    
    public function __construct(){
        $this->conexion = new M_Conexion;
        $this->session = Session::getInstance();
    }

    public function index(){
    	$response = (object)[];
    	$response->tipo_cliente = $this->getTypeClient();
    	$response->tipo_trabajo = $this->getTypeTrabajo();

    	return json_encode($response);
    }

    public function ListCotizacion(){
        extract($_POST);
        
        $sWhere = "";
        $sOrder = " ORDER BY id";
        $DesAsc = "DESC";
        $sOrder .= " {$DesAsc}";
        $sLimit = "";
        $response = (object)[];
        if(isset($_POST)){

            /*----------  ORDER BY ----------*/
            
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY folio {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY fecha_status {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY (SELECT nombre FROM cat_clientes WHERE id = id_cliente) {$DesAsc}";
            }
            
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 5){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY total {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 6){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY status {$DesAsc}";
            }
            /*----------  ORDER BY ----------*/

            if(isset($_POST['order_id']) && trim($_POST['order_id']) != ""){
                $sWhere .= " AND folio like '".$_POST["order_id"]."%'";
            }               
            if((isset($_POST['order_date_from']) && trim($_POST['order_date_from']) != "") && (isset($_POST['order_date_to']) && trim($_POST['order_date_to']) != "")){
                $sWhere .= " AND fecha_status BETWEEN '".$_POST["order_date_from"]."' AND '".$_POST["order_date_to"]."'";
            }
            if(isset($_POST['order_customer_name']) && trim($_POST['order_customer_name']) != ""){
                $sWhere .= " AND (SELECT nombre FROM cat_clientes WHERE id = id_cliente) LIKE '%".$_POST['order_customer_name']."%'";
            }
            if(isset($_POST['order_t_work']) && trim($_POST['order_t_work']) != ""){
                $sWhere .= " AND id = ".$_POST["order_id"];
            }
            if((isset($_POST['order_price_to']) && trim($_POST['order_price_to']) != "") && (isset($_POST['order_price_from']) && trim($_POST['order_price_from']) != "")){
                $sWhere .= " AND total BETWEEN '".$_POST["order_price_to"]."' AND '".$_POST["order_price_from"]."'";
            }
            if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
                $sWhere .= " AND status = ".$_POST['order_status'];
            }
            // if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
            //     $sWhere .= " AND cat_equipos.`status` = ".$_POST["order_status"];
            // }

            /*----------  LIMIT  ----------*/
            if(isset($_POST['length']) && $_POST['length'] > 0){
                $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
            }
        }

        $sql = "SELECT * , '' AS tipo_trabajo,
            (SELECT nombre FROM cat_clientes WHERE id = id_cliente) AS cliente,
            (SELECT COUNT(*) FROM cat_clientes WHERE status <= 2 ) AS totalRows
            FROM cotizaciones WHERE 1=1
        $sWhere $sOrder $sLimit";
        //echo "<script>alert($sql);</script>";
        $labelStatus = [
            0 =>  '<button class="btn btn-sm bg-red-thunderbird bg-font-red-thunderbird" id="status">Pendiente</button>',
            1 =>  '<button class="btn btn-sm bg-yellow-gold bg-font-yellow-gold" id="status">Por Aprobar</button>',
            2 =>  '<span class="btn btn-sm bg-green-jungle bg-font-green-jungle" id="status">Aprobado</span>',
            3 =>  '<button class="btn btn-sm bg-yellow-lemon bg-font-yellow-lemon" id="status">Por facturar</button>',
			4 =>  '<span class="btn btn-sm bg-purple-studio bg-font-purple-studio" id="status">Facturada</span>',
        ];
        $response->recordsTotal = 0;
        $response->data = [];
		/*$response->data[] = array (
                $sql,
                $sql,
                $sql,
                $sql,
                $sql,
                $sql,
				'',
				'',
				);*/
        $res = $this->conexion->link->query($sql);
        while($fila = $res->fetch_assoc()){
            $fila = (object)$fila;
			$folio = str_split($fila->folio);
			$ini=9;
			for($i=0;$i<count($folio);$i++){ 
				if($folio[$i]>0){
					$ini = $i;
					$i =count($folio) + 1;
				} 
			} 
			//$folio = count($folio);
			$folio = substr($fila->folio, $ini);
			if($folio == 0)
				$folio=$fila->id;
			if($fila->status<2){
            $response->data[] = array (
                $fila->id,
                $folio,
                $fila->fecha_status,
                $fila->cliente,
                $this->getDetailsCotizacion($fila->id),
                '$ ' . number_format($fila->total, 2, '.', ','),
                $labelStatus[$fila->status],
                '<button id="edit" class="btn btn-sm green btn-outline filter-submit margin-bottom" onclick="location.href=\'cotizacion?id='.$fila->id.'\'"><i class="fa fa-seach"></i> Editar</button>'
            );}
			else{
				$response->data[] = array (
                $fila->id,
                $folio,
                $fila->fecha_status,
                $fila->cliente,
                $this->getDetailsCotizacion($fila->id),
                '$ ' . number_format($fila->total, 2, '.', ','),
                $labelStatus[$fila->status],
				'<button class="btn btn-sm green btn-outline filter-submit margin-bottom" onclick="location.href=\'pdfCotizacion.php?id='.$fila->id.'\'"><i class="fa fa-seach"></i>PDF</button>
				<a class="dt-button buttons-pdf buttons-html5 btn green btn-outline" href="\cotizacion?id='.$fila->id.'&pdf=1" tabindex="0" aria-controls="datatable_ajax">
                                                <span>PDF<i class="fa fa-check" aria-hidden="true"></i></span>


                                            </a>');
			}
			
            $response->recordsTotal = $fila->totalRows;
        }

        $response->recordsFiltered = count($response->data);
        #$response->customActionMessage = $sql;
        $response->customActionStatus = "OK";
		
        return json_encode($response);
    }

    private function getDetailsCotizacion($id = 0){
        $span = "<div style='width: 300px;'>";
        if($id > 0){
            $sql = "SELECT (SELECT descripcion FROM cat_tipos_trabajo WHERE id = tipo_trabajo) AS label , 
                (SELECT clase FROM cat_tipos_trabajo WHERE id = tipo_trabajo) AS class
            FROM cotizaciones_detalle
            WHERE id_cotizacion = {$id} 
            GROUP BY cotizaciones_detalle.id";
            $res = $this->conexion->link->query($sql);
            $count = 0;
            $span .= "<div class='row' style='padding: .5em;'>";
            while($fila = $res->fetch_assoc()){
                $fila = (object)$fila;
                $fila->label = substr($fila->label , 0 , 3);
                // if($count == 1) 
                $span .= '<div class="col-md-3"><span class="form-control label '.$fila->class.'">'.wordwrap($fila->label).'</span></div>';
                // if($count%2 != 0) 
                $count++;
            }
        }
        $span .= "</div>";
        $span .= '</div>';
        return $span;
    }

    public function getTypeClient(){
    	$datos = [];
    	$sql = 'SELECT * FROM cat_clientesTipo';
    	$res = $this->conexion->link->query($sql);
        while($fila = $res->fetch_assoc()){
             $fila = (object)$fila;
             $datos[] = [
             	"id" => $fila->id,
             	"nombre" => $fila->nombre
             ];
        }

      	return ($datos);
    }

    public function getTypeTrabajo(){
      $datos = [];
      $sql = 'SELECT * FROM cat_tipos_trabajo';
      $res = $this->conexion->link->query($sql);
        while($fila = $res->fetch_assoc()){
             $fila = (object)$fila;
             $datos[] = [
              "id" => $fila->id,
              "name" => $fila->descripcion,
              "class" => $fila->clase
             ];
        }

        return ($datos);
    }

    public function save(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        //print_r($postdata);
        $response = (object)[];
        if($postdata->id_client > 0){
            $postdata->observaciones = addslashes($postdata->observaciones);
             $sql = "INSERT INTO cotizaciones SET
                    id_cliente = '{$postdata->id_client}',
                    abonos = '{$postdata->params->abonos}',
                    tiempo = '{$postdata->params->tiempo}',
                    id_sucursal = '{$postdata->params->id_sucursal}',
                    forma_pago = '{$postdata->params->forma_pago}',
                    observaciones = '{$postdata->observaciones}',
                    subtotal = '{$postdata->params->subtotal}',
                    descuento = '{$postdata->params->descuentoo}',
                    iva = '{$postdata->params->iva}',
                    total = '{$postdata->params->total}',
                    id_usuario = '{$this->session->logged}',
                    fecha_status = CURRENT_DATE,
                    fecha_create = CURRENT_DATE";
            // print $sql;
            $id = $this->conexion->Consultas(1,$sql);
            if((int)$id > 0){
                $folio = str_pad($id , 10 ,"0" , STR_PAD_LEFT);
                $response->id = $id;
                $response->folio = $folio;
                // $response->fecha = date("F j, Y \a\t g:ia");;

                $dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
                $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
                 
                $response->fecha = $dias[date('w')]." ".date('d')." de ".$meses[date('n')-1].". ". date("h:m A");

                $this->conexion->link->query("UPDATE cotizaciones SET folio = '".str_pad($id , 10 ,"0" , STR_PAD_LEFT)."' WHERE id = $id");
                foreach ($postdata->details as $key => $value) {
                   $value->des_trabajo = trim($value->des_trabajo);
				   if($value->tipo_trabajo=='3'){
					   $sql_update = "UPDATE cotizaciones SET status = 2 where id = '{$id}'";
					  $this->conexion->link->query($sql_update);
				   }
				   $total = $value->count * $value->price;
						$desc = $value->desc / 100;
						$desc = $total*$desc;
						$total = $total - $desc;
                   $sql_detalle = "INSERT INTO cotizaciones_detalle SET
                    id_cotizacion = '{$id}',
                    tipo_trabajo = '{$value->tipo_trabajo}',
                    des_trabajo = '{$value->des}',
                    cantidad = '{$value->count}',
                    descuento = '{$value->desc}',
                    unidad = '{$value->uni}',
                    precio = '{$value->price}',
                    total = ".$total;
                    $this->conexion->Consultas(1,$sql_detalle);
                    // print $sql_detalle;
                }
            }$sql = "SELECT cotizaciones.*, cat_clientes.id_tipcli, cat_clientes.direccion FROM cotizaciones JOIN cat_clientes ON cat_clientes.id = cotizaciones.id_cliente WHERE cotizaciones.id = '{$id}'";
            $res = $this->conexion->link->query($sql);
            if($fila = $res->fetch_assoc()){
                 $datos = (object)$fila;
                 if($datos->status == 2){
					 $sql = "UPDATE cotizaciones SET fecha_aprobacion = CURRENT_DATE WHERE id = '{$id}'";
					$this->conexion->link->query($sql);
					 $sql3 = "SELECT cat_tipos_trabajo.descripcion FROM cotizaciones_detalle JOIN cat_tipos_trabajo ON cat_tipos_trabajo.id = cotizaciones_detalle.tipo_trabajo WHERE cotizaciones_detalle.id_cotizacion = '{$datos->id}'";
					$res3 = $this->conexion->link->query($sql3);
					$tipo_trabajo = '';
                        while($datas = $res3->fetch_assoc()){
							if($tipo_trabajo!='')
								$tipo_trabajo = $tipo_trabajo.', ';
                            $tipo_trabajo = $tipo_trabajo.$datas['descripcion'];
                        }
                    $folio = str_pad($datos->id , 3 ,"0" , STR_PAD_LEFT);
					$folio = "OT-".$folio;
                    $sql_orden="INSERT INTO orden_trabajo SET id_cotizacion = '{$datos->id}', cliente = '{$datos->cliente}', tipo_cliente = '{$datos->id_tipcli}', 
					direccion='{$datos->direccion}', tipo_trabajo='{$tipo_trabajo}', fecha='{$datos->fecha_status}', tiempo_estimado='', 
					observaciones= '{$datos->observaciones}', fecha_create = CURRENT_DATE, tipo_registro='{$datos->tipo_registro}', codigo = '{$folio}'";
                    
					$this->conexion->link->query($sql_orden);
					
             

                 }
            }
			header("Location: http://cegaservices2.procesos-iq.com/cotizacion?id=$id");
            return json_encode($response);
        }
    }

    public function edit(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        //print_r($postdata);
        //exit;
        $response = (object)[];
        if($postdata->params->total > 0 && (int)$postdata->id_client > 0){
            $postdata->observaciones = addslashes($postdata->observaciones);
              $sql = "UPDATE cotizaciones SET
                    id_cliente = '{$postdata->id_client}',
					abonos = '{$postdata->params->abonos}',					
					tiempo = '{$postdata->params->tiempo}',
                    id_sucursal = '{$postdata->params->id_sucursal}',
                    forma_pago = '{$postdata->params->forma_pago}',
                    observaciones = '{$postdata->observaciones}',
                    subtotal = '{$postdata->params->subtotal}',
                    descuento = '{$postdata->params->descuentoo}',
                    iva = '{$postdata->params->iva}',
                    total = '{$postdata->params->total}',
                    fecha_status = CURRENT_DATE
                    WHERE id = " . $postdata->id_cotizacion;
            // print $sql;
            $this->conexion->link->query($sql);
            //if((int)$id > 0){
                    $id = $postdata->id_cotizacion;

                // borrar detalle
                $sqlD = "DELETE FROM cotizaciones_detalle WHERE id_cotizacion = " . $postdata->id_cotizacion;
				$this->conexion->link->query($sqlD);

                $folio = str_pad($id , 10 ,"0" , STR_PAD_LEFT);
                $response->id = $id;
                $response->folio = $folio;
                // $response->fecha = date("F j, Y \a\t g:ia");;

                $dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
                $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
                 
                $response->fecha = $dias[date('w')]." ".date('d')." de ".$meses[date('n')-1].". ". date("h:m A");
                //print_r($postdata->details);
				//exit();
                
				foreach ($postdata->details as $key => $value) {
					
                   $value->des_trabajo = trim($value->des_trabajo);
				   $total = $value->count * $value->price;
						$desc = $value->desc / 100;
						$desc = $total*$desc;
						$total = $total - $desc;
                   $sql_detalle = "INSERT INTO cotizaciones_detalle SET
                    id_cotizacion = '{$id}',
                    tipo_trabajo = '{$value->tipo_trabajo}',
                    des_trabajo = '{$value->des}',
                    cantidad = '{$value->count}',
					descuento = '{$value->desc}',
                    unidad = '{$value->uni}',
                    precio = '{$value->price}',
                    total = ".$total;
                    $this->conexion->Consultas(1,$sql_detalle);
                    // print $sql_detalle;
                }
            //}

            return json_encode($response);
        }
    }

    public function saveCliente(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        // print_r($postdata);
        if($postdata->ruc != ""){
            $sql = "INSERT INTO cat_clientes SET
                    nombre = '{$postdata->nombre_cliente}',
                    email = '{$postdata->email}',
                    id_tipcli = '{$postdata->tipo_cliente}',
                    razon_social = '{$postdata->razon_social}',
                    ruc = '{$postdata->ruc}',
                    telefono = '{$postdata->telefono}',
                    id_usuario = '{$this->session->logged}',
                    fecha = CURRENT_DATE";
            // print $sql;
            $ids = $this->conexion->Consultas(1,$sql);
			$sql="INSERT INTO cat_clientes_contactos SET id_cliente='{$ids}' , nombre='',correo='{$postdata->email}',cargo='',
        telefono='',area=''";
			$this->conexion->link->query($sql);
			
            if((int)$ids > 0){
                return ($this->GetClienteID($ids));
            }
        }

        return ["Error" => "400"];
    }
    
	public function copiaCotizacion(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
             /*$sql = "INSERT INTO cotizaciones SET
                    id_cliente = '{$postdata->id_client}',
                    abonos = '{$postdata->params->abonos}',
                    tiempo = '{$postdata->params->tiempo}',
                    descuento_porcentaje = '{$postdata->params->discount}',
                    id_sucursal = '{$postdata->params->id_sucursal}',
                    forma_pago = '{$postdata->params->forma_pago}',
                    observaciones = '{$postdata->observaciones}',
                    subtotal = '{$postdata->params->subtotal}',
                    iva = '{$postdata->params->iva}',
                    descuento = '{$postdata->params->_discount}',
                    total = '{$postdata->params->total}',
                    id_usuario = '{$this->session->logged}',
                    fecha_status = CURRENT_DATE,
                    fecha_create = CURRENT_DATE";
            // print $sql;
            $id = $this->conexion->Consultas(1,$sql);*/
			$sql = "SELECT cotizaciones.* from cotizaciones WHERE cotizaciones.id = '{$postdata->id}'";
            $res = $this->conexion->link->query($sql);
            $res = $res->fetch_object();
			$sql = "SELECT COUNT(*) AS cantidad from cotizaciones WHERE folio like '{$res->folio} -%'";
            $res2 = $this->conexion->link->query($sql);
            $res2 = $res2->fetch_object();
			$cantidad= $res2->cantidad + 1;
			$folio = $res->folio.' - '.$cantidad;
			$sql = "INSERT INTO cotizaciones SET
                    id_cliente = '{$res->id_cliente}',
                    abonos = '{$res->abonos}',
                    tiempo = '{$res->tiempo}',
                    id_sucursal = '{$res->id_sucursal}',
                    forma_pago = '{$res->forma_pago}',
                    observaciones = '{$res->observaciones}',
                    subtotal = '{$res->subtotal}',
                    iva = '{$res->iva}',
                    folio = '{$folio}',
					status = 1,
                    total = '{$res->total}',
                    id_usuario = '{$this->session->logged}',
                    fecha_status = CURRENT_DATE,
                    fecha_create = CURRENT_DATE";
			$id = $this->conexion->Consultas(1,$sql);
			//return $id;
			$sql = "SELECT cotizaciones_detalle.* from cotizaciones_detalle WHERE id_cotizacion = '{$postdata->id}'";
			
            $res3 = $this->conexion->link->query($sql);
			
			while($datas = $res3->fetch_assoc()){
				$total = $value->count * $value->price;
						$desc = $value->desc / 100;
						$desc = $total*$desc;
						$total = $total - $desc;
				
							$sql_detalle = "INSERT INTO cotizaciones_detalle SET
                    id_cotizacion = '".$id."',
                    tipo_trabajo = '".$datas['tipo_trabajo']."',
                    des_trabajo = '".$datas['des_trabajo']."',
                    cantidad = '".$datas['cantidad']."',
                    precio = '".$datas['precio']."',
                    total = '".$total."'";
                    $this->conexion->Consultas(1,$sql_detalle);
                        }
            
        return json_encode($id);
    }

    private function GetClienteID($id){
        // print_r($postdata);
        if(isset($id)){
            $datos = (object)[];
            $sql = "SELECT *,id AS id_cliente
            FROM cat_clientes 
            WHERE id='$id'";
            $res = $this->conexion->link->query($sql);
            if($fila = $res->fetch_assoc()){
                 $datos = (object)$fila;
            }
           
            return json_encode($datos);
        }
    }

	public function GetCliente(){
		$postdata = (object)json_decode(file_get_contents("php://input"));
		// print_r($postdata);
        $response = (object)[
            "success" => 400,
            "data" => ""
        ];
		if(isset($postdata->ruc)){
			$datos = (object)[];
	        $sql = "SELECT *
	        FROM cat_clientes 
	        WHERE ruc='$postdata->ruc'";
	        $res = $this->conexion->link->query($sql);
	        if($fila = $res->fetch_assoc()){
	             $datos = (object)$fila;


                 // contacto principal
                 $contacto_email = "";
                 $sql_contacto ="SELECT correo FROM cat_clientes_contactos WHERE id_cliente = {$datos->id}";
                 $res = $this->conexion->link->query($sql_contacto);
                 if($res->num_rows > 0){
                    $datas = $res->fetch_assoc();
                    $contacto_email = $datas['correo'];
                 }

                 // sucursales
                 $sql_sucursal ="SELECT * FROM cat_sucursales WHERE status = 1 AND id_cliente = '{$datos->id}'";
                 $res = $this->conexion->link->query($sql_sucursal);
				 $sql_facturacion ="SELECT cat_clientes.id,cat_clientes_facturacion.ruc,  cat_clientes.nombre, 
cat_clientes_facturacion.rs AS razon_social, cat_clientes.email, cat_clientes_facturacion.telefono, cat_clientes_facturacion.id AS id_raz, cat_clientes.id_tipcli
FROM cat_clientes JOIN cat_clientes_facturacion ON cat_clientes.id = cat_clientes_facturacion.id_cliente
WHERE cat_clientes.id = '{$datos->id}' AND cat_clientes_facturacion.status=1";
                 $res2 = $this->conexion->link->query($sql_facturacion);
                 if($res->num_rows > 0){
                    $response->success = 300;
                    $response->rows = $res->num_rows;
                    if($res->num_rows > 1){
                        while($datas = $res->fetch_assoc()){
                            $datas['correo_contacto'] = $contacto_email;
                            $response->data[] = (object)$datas;
                        }
                    }else{
                        $datas = $res->fetch_assoc();
                        $datas['correo_contacto'] = $contacto_email;
                        $response->data[] = (object)$datas;
                    }
                 }else if($res2->num_rows>0){
					 $response->success = 250;
					 while($datas = $res2->fetch_assoc()){
                            $datas['correo_contacto'] = $contacto_email;
                            $response->data[] = (object)$datas;
                        }
				 }				 
				 else{
                    $response->success = 200;
                    $datos->correo_contacto = $contacto_email;
                    $response->data[] = (object)$datos;
                 }
	        }
	       
	        return json_encode($response);
		}
    }

	public function GetClienteNombre(){
		$postdata = (object)json_decode(file_get_contents("php://input"));
		// print_r($postdata);
        $response = (object)[
            "success" => 400,
            "data" => ""
        ];
		
		if(isset($postdata->nombre_cliente)){
			$datos = [];

	        $sql = "SELECT * FROM cat_clientes WHERE nombre like '$postdata->nombre_cliente%'";
	        $res = $this->conexion->link->query($sql);
	        while($fila = $res->fetch_assoc()){
             $fila = (object)$fila;
             $datos[] = [
             	"id" => $fila->id,
             	"nombre" => $fila->nombre,
             	"ruc" => $fila->ruc,
             ];
			
			}
			//print_r($datos);
	       $response->data2 = $sql;
	       $response->data = $datos;
	       $response->ti = 2;
	        
		}
		return json_encode($response);
    }

    public function getSucursal(){

    }

    public function sendMailConfirmation(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        //print_r($postdata);
        $response = (object)[
            "success" => 400
        ];

        $destino = "ing.mhernandez@gmail.com";
        //$destino = $postdata->correo;
        if( !is_null(sendEmail($destino, "CEGA Services [Cotización]", ('Cotización con ID: ' . $postdata->id_cotizacion), ('Cotización con ID: <b>'.$postdata->id_cotizacion.'</b>'), "manuelhernandez@outlook.com", "MI NOMBRE")) )
            $response->email = "Enviado";
        else
            $response->email = "Error";

        return json_encode($response);
    }

    public function changeStatus(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(!isset($postdata->id)) $postdata = (object)$_POST;
		
        if($postdata->id > 0){
            $sql = "UPDATE cotizaciones SET status = status+1 WHERE id = '{$postdata->id}'";
            $this->conexion->link->query($sql);
            $sql = "SELECT cotizaciones.*, cat_clientes.id_tipcli, cat_clientes.direccion FROM cotizaciones JOIN cat_clientes ON cat_clientes.id = cotizaciones.id_cliente WHERE cotizaciones.id = '{$postdata->id}'";
            $res = $this->conexion->link->query($sql);
            if($fila = $res->fetch_assoc()){
                 $datos = (object)$fila;
                 if($datos->status == 2){
					 $sql = "UPDATE cotizaciones SET fecha_aprobacion = CURRENT_DATE WHERE id = '{$postdata->id}'";
					$this->conexion->link->query($sql);
					 $sql3 = "SELECT cat_tipos_trabajo.descripcion FROM cotizaciones_detalle JOIN cat_tipos_trabajo ON cat_tipos_trabajo.id = cotizaciones_detalle.tipo_trabajo WHERE cotizaciones_detalle.id_cotizacion = '{$datos->id}'";
					$res3 = $this->conexion->link->query($sql3);
					$tipo_trabajo = '';
                        while($datas = $res3->fetch_assoc()){
							if($tipo_trabajo!='')
								$tipo_trabajo = $tipo_trabajo.', ';
                            $tipo_trabajo = $tipo_trabajo.$datas['descripcion'];
                        }
						$folio2 = str_split($datos->folio);
					if(count($folio2)>9)
					{$ini=9;
			for($i=0;$i<count($folio2);$i++){ 
				if($folio2[$i]>0){
					$ini = $i;
					$i =count($folio2) + 1;
				} 
			} 
			//$folio = count($folio);
			$folio2 = substr($datos->folio, $ini);
			$folio = "OT-".$folio2;
			}
					else{
                    $folio = str_pad($datos->id , 3 ,"0" , STR_PAD_LEFT);
					$folio = "OT-".$folio;}
                    $sql_orden="INSERT INTO orden_trabajo SET id_cotizacion = '{$datos->id}', cliente = '{$datos->cliente}', tipo_cliente = '{$datos->id_tipcli}', 
					direccion='{$datos->direccion}', tipo_trabajo='{$tipo_trabajo}', fecha='{$datos->fecha_status}', tiempo_estimado='', 
					observaciones= '', fecha_create = CURRENT_DATE, tipo_registro='{$datos->tipo_registro}', codigo = '{$folio}'";
                    
					$this->conexion->link->query($sql_orden);
					
             

                 }
            }
        }
        return 1;
    }

    public function GetCotizacion($id = 0){
        /*
        [{"tipo_trabajo":"1","des_trabajo":"\n\n
        CORRECTIVO\n\n",
        "class_trabajo":"bg-red-thunderbird bg-font-red-thunderbird",
        "des":"1",
        "count":"1",
        "price":"1",
        "edit":0},
        */
        $postdata = (object)json_decode(file_get_contents("php://input"));
        //print_r($postdata);
        if($postdata->id_cotizacion){
            $sql = "SELECT cotizaciones.*, cat_usuarios.nombre AS usuario FROM cotizaciones 
JOIN cat_usuarios ON cat_usuarios.id = cotizaciones.id_usuario WHERE cotizaciones.id ='{$postdata->id_cotizacion}'";
            $res = $this->conexion->link->query($sql);
            if($fila = $res->fetch_assoc()){
				//status
				$sta = $fila['status'];
                // fecha
                $dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
                $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
                $fecha_w = date("w",strtotime($fila['fecha_create']));
                $fecha_d = date("d",strtotime($fila['fecha_create']));
                $fecha_n = date("n",strtotime($fila['fecha_create']));
                $fecha_h = date("h:m A",strtotime($fila['fecha_create']));
                $fecha = $dias[$fecha_w]." ".$fecha_d." de ".$meses[$fecha_n-1].". " . $fecha_h;

                // sucursal
                $id_sucursal = $fila['id_sucursal'];
				$id_facturacion = $fila['id_facturacion'];
                if(!empty($id_sucursal)){
                    $sqlS = "SELECT s.*, c.nombre as nombre_cliente FROM cat_sucursales AS s INNER JOIN cat_clientes AS c ON c.id = s.id_cliente WHERE s.id = " . $id_sucursal;
                    $resS = $this->conexion->link->query($sqlS);
                    $filaS = $resS->fetch_assoc();
                }
				else if(!empty($id_facturacion)){
					$sqlF = "SELECT * FROM cat_clientes_facturacion WHERE id = " . $id_facturacion;
                    $resF = $this->conexion->link->query($sqlF);
                    $filaF = $resF->fetch_assoc();
				}
                else{
                    $resF = array();
                    $resS = array();
                }

                // contacto principal
                 $contacto_email = "";
                 $sql_contacto ="SELECT correo FROM cat_clientes_contactos WHERE id_cliente = " . $fila['id_cliente'];
                 $res = $this->conexion->link->query($sql_contacto);
                 if($res->num_rows > 0){
                    $datas = $res->fetch_assoc();
                    $contacto_email = $datas['correo'];
                 }

                $sql = "SELECT * FROM cotizaciones_detalle WHERE id_cotizacion = '{$postdata->id_cotizacion}'";
                $resD = $this->conexion->link->query($sql);
                if($resD->num_rows > 0){
                    $c = 0;
                    if($resD->num_rows > 0){
                        while($filaD = $resD->fetch_assoc()){
                            // buscar tipo trabajo
                            $sqlT = "SELECT descripcion, clase FROM cat_tipos_trabajo WHERE id = " . $filaD['tipo_trabajo'];
                            $resT = $this->conexion->link->query($sqlT);
                            $filaT = $resT->fetch_assoc();

                            // rellenar el detalle
                            $fila['details'][$c]['tipo_trabajo'] = $filaD['tipo_trabajo'];
                            $fila['details'][$c]['des_trabajo'] = $filaT['descripcion'];
                            $fila['details'][$c]['class_trabajo'] = $filaT['clase'];
                            $fila['details'][$c]['des'] = $filaD['des_trabajo'];
                            $fila['details'][$c]['uni'] = $filaD['unidad'];
                            $fila['details'][$c]['desc'] = $filaD['descuento'];
                            $fila['details'][$c]['count'] = $filaD['cantidad'];
                            $fila['details'][$c]['price'] = $filaD['precio'];
                            $fila['details'][$c]['edit'] = 0;
                            $c++;
                        }
                    }
                    else{
                        $filaD = $resD->fetch_assoc();

                        // buscar tipo trabajo
                        $sqlT = "SELECT descripcion, clase FROM cat_tipos_trabajo WHERE id = " . $filaD['tipo_trabajo'];
                        $resT = $this->conexion->link->query($sqlT);
                        $filaT = $resT->fetch_assoc();

                        // rellenar el detalle
                        $fila['details'][$c]['tipo_trabajo'] = $filaD['tipo_trabajo'];
                        $fila['details'][$c]['des_trabajo'] = $filaT['descripcion'];
                        $fila['details'][$c]['class_trabajo'] = $filaT['clase'];
                        $fila['details'][$c]['des'] = $filaD['des_trabajo'];
                        $fila['details'][$c]['count'] = $filaD['cantidad'];
                        $fila['details'][$c]['price'] = $filaD['precio'];
                        $fila['details'][$c]['edit'] = 0;
                    }
                }
                else{
                    $fila['details'] = array();
                }
                
                $datos = (object)$fila;
             }
        }

        $cliente = $this->GetClienteID($datos->id_cliente);
        $response = (object)[
            "success" => 400,
            "datos" => $datos,
            "sucursal" => (object) $filaS,
            "facturacion" => (object) $filaF,
            "contacto_email" => $contacto_email
        ];
        $response->cliente = json_decode($cliente);
        $response->fecha = $fecha;
        $response->status1 = $sta;
        return json_encode($response);
    }

}


?>