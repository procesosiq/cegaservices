<?php 
	
	include './controllers/class.sesion.php';
    $session = Session::getInstance();
    $session = (object)$session;
    // echo $session->nombre;
    if(!isset($session->logged)){
        header('Location: http://cegaservices2.procesos-iq.com/login.php');
    }

    if(isset($_GET["page"])){
        if($_GET["page"] != ""){
            $page = $_GET["page"];
            if ($page != "logout" || $page != "index" || $page != ""){
                include 'loader.php';
            }
        }else{
            $page = "404";
        }


    }else{
       $page = "index"; 
    }

    if(isset($_GET["page"])){
        if($_GET["page"] == "logout"){
            Session::getInstance()->kill();
            header('Location: http://cegaservices2.procesos-iq.com/login.php');
        }
    }
	require_once("dompdf/dompdf_config.inc.php");
	$conexion = mysql_connect("procesos-iq.com","auditoriasbonita","u[V(fTIUbcVb");
	mysql_select_db("cegaservices2",$conexion);
	$consulta=mysql_query("SELECT * FROM cotizaciones where id=".$_GET['id']);
	$dato=mysql_fetch_array($consulta);
	$consulta=mysql_query("SELECT * FROM cat_clientes where id=".$dato['id_cliente']);
	$cliente=mysql_fetch_array($consulta);
	$consulta=mysql_query("SELECT cotizaciones_detalle.*, cat_tipos_trabajo.descripcion FROM cotizaciones_detalle JOIN cat_tipos_trabajo ON cat_tipos_trabajo.id =cotizaciones_detalle.tipo_trabajo WHERE id_cotizacion=".$_GET['id']);
	//$detalles=mysql_fetch_array($consulta);
	//print_r($detalles);
	//exit();
	
$codigoHTML='
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Cotizacion-'.$_GET['id'].'</title>
    <style type="text/css">

.clearfix:after {
  content: "";
  display: table;
  clear: both;
}

a {
  color: #0087C3;
  text-decoration: none;
}

body {
  position: relative;
  width: 16cm;  
  height: 29.7cm; 
  margin: 0 auto; 
  color: black;
  background: #FFFFFF; 
  font-family: Arial, sans-serif; 
  font-size: 14px; 
  font-family: SourceSansPro;
}

header {
  padding: 10px 0;
  margin-bottom: 20px;
}

#logo {
  float: left;
  margin-top: 8px;
}

#empresa {
  margin-top: 30px;
  text-align:center;
  font-weight: bold;
}

#numero {
	margin-top:20px;
text-align:center;
font-size:20px;
font-weight: bold;
}

#logo img {
  height: 40px;
}

#corp {
  float: right;
  text-align: right;
}

#corp img {
  height: 50px;  
  margin-top: 8px;
}


#client {
  float: left;
}


h2.name {
  font-size: 1.4em;
  font-weight: normal;
  margin: 0;
}

.invoice {
  float: right;
  text-align: right;
}

table {
  width: 100%;
}

table th,
table td {
  background: #FFFFFF;
  text-align: center;
  border: 2px solid #000000;
}

table th {
  font-weight: bold;
  background: #c4d79b;
}



#thanks{
  font-size: 1em;
}

#notices{
  padding-left: 6px;
  border-left: 6px solid #0087C3;  
}

#notices .notice {
  font-size: 1.2em;
}
li span { font-weight: normal; }
footer {
  color: #777777;
  width: 100%;
  height: 30px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #AAAAAA;
  padding: 8px 0;
  text-align: center;
}

</style>
  </head>
  <body>
    <header class="clearfix">
      <div id="logo">
        <img src="cegaservice.png">
      </div>
      <div id="corp">
        <img src="cegacorp.png">
      </div>
      </div>
	  <div id="empresa">
        <i>Una empresa del grupo</i>
      </div>
	  <div id="numero">
        '.$dato['folio'].'
      </div>
    </header>
    <main>
      <div id="details" class="clearfix">
        <div id="client">
          <div style="font-weight: bold;">PARA:</div>
          <div>TELF:</div>
        </div>
		<div style="float:left; margin-left:65px">
          <div style="font-weight: bold;">'.$cliente['nombre'].'</div>
          <div>'.$cliente['telefono'].'</div>
        </div>
		<div class="invoice">
          <div style="font-weight: bold;">'.$dato['fecha_create'].'</div>
          <div>'.$dato['fecha_aprovacion'].'</div>
        </div>
		<div class="invoice" style="margin-right:150px">
          <div><i>Fecha Cotización:</i></div>
          <div><i>Fecha Cotización:</i></div>
          <div><i>Fecha Aprobación:</i></div>
        </div>
		<div style="margin-top: 70px; float:left">
	  De nuestras consideraciones:
	  <br>
	  <br>
	  Por medio de la presente detallamos la cotización para los servicios y equipos solicitados:
	  </div>	  	
      </div>
      <table border="0" cellspacing="0" cellpadding="0"  style="margin-top:130px">
        <thead>
          <tr>
            <th width="10%">CANTIDAD</th>
            <th width="20%" >TIPO TRABAJO</th>
            <th width="40%" >DESCRIPCION</th>
            <th width="15%">V.UNITARIO</th>
            <th width="15%">V.TOTAL</th>
          </tr>
        </thead>
        <tbody>';
		$sub = 0;
		$consulta=mysql_query("SELECT cotizaciones_detalle.*, cat_tipos_trabajo.descripcion FROM cotizaciones_detalle JOIN cat_tipos_trabajo ON cat_tipos_trabajo.id =cotizaciones_detalle.tipo_trabajo WHERE id_cotizacion=".$_GET['id']);
		while($detalles=mysql_fetch_array($consulta)){
$codigoHTML.='
      <tr>
            <td width="10%">'.$detalles['cantidad'].'</th>
            <td width="20%">'.$detalles['descripcion'].'</th>
            <td width="40%" >'.$detalles['des_trabajo'].'</td>
            <td width="15%">$ '.number_format($detalles['precio'], 2, '.', '').'</td>
            <td width="15%">$ '.number_format($detalles['total'], 2, '.', '').'</td>
          </tr>';
		  
      } 
		$sub = $dato['subtotal'];
		$descuento = $dato['descuento'];
        $iva = $dato['iva'];  
		$total = $dato['total'];
    $codigoHTML.='</tbody>
		<tfoot>
          <tr>
            <td colspan="4" style="text-align:right; "><strong>SUBTOTAL</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td>$ '.number_format($sub, 2, '.', '').'</td>
          </tr>
          <tr>
            <td colspan="4" style="text-align:right; margin-right:10px"><strong>Descuento</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td>$ '.number_format($descuento, 2, '.', '').'</td>
          </tr>
		  <tr>
            <td colspan="4" style="text-align:right; margin-right:10px"><strong>IVA 14%</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td>$ '.number_format($iva, 2, '.', '').'</td>
          </tr>
          <tr>
            <td colspan="4" style="text-align:right; margin-right:10px"><strong>TOTAL&nbsp;</strong>&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td>$ '.number_format($total, 2, '.', '').'</td>
          </tr>
        </tfoot>
      </table>
	  <br>
      <div id="thanks"><strong>OBSERVACIONES:</strong>'.$dato['observaciones'].'</div>
	  <br>
	  
            <div width="100%" style="text-align:left; border: 2px solid #000000;"><strong>La oferta no incluye:</strong>
			<br>
			<div style="margin-left:60px">
			<li>Transporte fuera de la ciudad de Guayaquil (al menos que se indique cobro de viaticos).</li>
			<li>Trabajos de albañilería, pintura, u otra adecuación civil</li>
			<li>Apertura, sellado, Impermeabilización, de boquetes por losa, tumbado o paredes para paso de ductos, tubería y demás requerimientos para instalación de material y equipos.</li>
			<li>Acometidas eléctricas, breakers y puntos para termostatos y drenajes.</li>
			<li>Cualquier otro trabajo, equipo y/o material no estipulado en la oferta.</li>
			</div>
			</div>

            <div width="100%" style="text-align:left; border: 2px solid #000000;"><strong>Validez de la oferta:</strong>
			<br>
			<div style="margin-left:60px">
			<li>15 días o hasta agotar stock de equipos y/o materiales</li>
			</div>
			</div>

            <div width="100%" style="text-align:left; border: 2px solid #000000;"><strong>Forma de pago:</strong>
			<br>
			<div style="margin-left:20px">
			<ol type="a" style="font-weight: bold;">
			<li><span>Precio establecido con descuento para pago directo por medio de cheque o transferencia bancaria a favor de <strong> CEGASERVICES S.A.</strong></span></li>
			<li><span>Disponible pago con tarjeta de crédito DINERS, VISA, MASTERCARD para el cual no aplica el descuento otorgado y cuyo costo final será confirmado una ve indicado medio de pago con tarjeta de crédito.</span></li>
			</ol>
			</div>
			</div>

            <div width="100%" style="text-align:left; border: 2px solid #000000;"><strong>Garantía:</strong>
			<br>
			<div style="margin-left:60px">
			<li><strong>Mano de obra:</strong><br>
			<u>Mantenimientos:</u> Para equipos de requerimiento de mantenimiento mensual o bi-mensual garantía de 15 días, para trimestrales o más se otorga garantía de 1 mes en los trabajos realizados. No incluye fallas por desgaste o uso de las partes / equipo que pueden presentarse después de un mantenimiento.
			<br>
			<u>Instalación y correctivos: </u>3 semanas de garantía ya que las fallas en este rubro se presentan siempre en las primeras 3 semanas de realizado un trabajo.</li>
			<li><strong>Repuestos:</strong> Garantía por fallas de fabricación de 3 meses para compresores, turbinas y motores eléctricos y de 1 mes para partes y componentes eléctricos menores.</li>
			<li><strong>Equipos ECOX:</strong> partes y piezas 1 año, compresor 3 años por fallas de fabricación</li>
			</div>
			<i>Para todas las áreas, la garantía no cubre fallas por problemas eléctricos, variaciones de voltaje, mala instalación y manipulación
			(si no realizado por personal calificado por CEGASERVICES S.A.), falta de mantenimiento, mal manejo o uso que <u>incluye el no
			seguir las recomendaciones indicadas por le empresa.</u></i>
			</div>

            <div width="100%" style="text-align:left; border: 2px solid #000000;"><strong>Tiempo aproximado de trabajo:</strong>
			<br>
			<div style="margin-left:60px">
			<li>Ingreso y ejecucíon según coordinación con cliente</li>
			<li>Tiempo aproximado de instalación básica de 2 a 3 horas por equipo</li>
			</div>
			</div>
			<br>
      <div>
        <div>Atentamente,</div><br>
        <div>Daniela Ayerve Y.</div>
        <div><strong><i>CEGASERVICES S.A.</li></strong></div>
        <div>RUC: 0992273399001</div>
        <div>TELF: 04 226 1122</div>
      </div>
    </main>
  </body>
</html>';

$codigoHTML=utf8_decode($codigoHTML);
$dompdf=new DOMPDF();
$dompdf->load_html($codigoHTML);
ini_set("memory_limit","128M");
$dompdf->render();
$dompdf->stream("Cotización-".$_GET['id'].".pdf");
?>