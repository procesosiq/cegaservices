<?php
class ConfigZonas {

	private $conexion;
    private $session;

    public function __construct(){
        $this->conexion = new M_Conexion;
        $this->session = Session::getInstance();
    }

    public function insert(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        $sWhere = "";

        if($params->id_zona > 0){
            $sWhere = " WHERE id = '{$params->id_zona}'";
        }

        $sql = "INSERT INTO cat_zonas
                SET 
                nombre = '{$params->zona}',
                status = '{$params->status}'
                $sWhere";
        if($params->id_zona > 0){
            $sql = str_replace("INSERT INTO", "UPDATE", $sql);
        }
        $this->conexion->query($sql);
        $response->status = 200;
        return json_encode($response);
    }

    public function eliminar(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;

        $sql = "UPDATE cat_zonas
                SET 
                status = 'Eliminado'
                WHERE id = '{$params->id_zona}'";
        $this->conexion->query($sql);
        $response->status = 200;
        return json_encode($response);
    }

    public function zonas(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        
        $sWhere = ""; 

        $sql = "SELECT id, nombre, status FROM cat_zonas WHERE status != 'Eliminado'";
        $response->zonas = $this->conexion->queryAll($sql);
        return json_encode($response);
    }

    public function show(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        
        $sql = "SELECT
                id AS id_zona,
                nombre AS zona,
                status
                FROM cat_zonas
                WHERE status != 'Eliminado' AND id = '{$params->id_zona}'";
        $response->data = $this->conexion->queryRow($sql);
        return json_encode($response);
    }

}