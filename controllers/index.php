<?php
	/*----------  MICRO FRAMEWORK V1  ----------*/
	
	include 'class.sesion.php';
	$session = Session::getInstance();
	$session = (object)$session;
	$options = $_REQUEST['accion'];
	$controller = (object)[
		"controller" => "",
		"method" => "index"
	];
 
    if(!isset($session->logged) && explode(".",$options)[0] != 'Data'){
        die("Acceso no Autorizado");
    }

	if(!isset($_REQUEST['accion'])){
		die("Peticion desconocida");
	}

	if(strpos($options , ".")){
		$option = explode(".",$options);
		$controller->controller = $option[0];
		$controller->method = $option[1];
	}else{
		$controller->controller = $options;
	}
	
	$controlName = $controller->controller;
	$path = './'.$controlName.'.php';
	include 'conexion.php';
	// include 'ApiProntoforms.php';
	include($path);


	$objectController = new $controlName();
	// print_r($objectController);
	// print_r($controller->method);

	$response = $objectController->{$controller->method}();

	if(is_array($response) || is_object($response)){
		return $response;
	}else{
		echo $response;
	}
?>