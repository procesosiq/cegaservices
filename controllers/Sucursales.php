<?php
class Sucursales {

	private $conexion;
    private $session;

    public function __construct(){
        $this->conexion = new M_Conexion;
        $this->session = Session::getInstance();
    }

    public function getSucursal(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        
        $sql = "SELECT 
                    id, 
                    nombre_contacto AS nombre, 
                    razon_social, 
                    ruc AS cedula, 
                    telefono,
                    direccion, 
                    id_tipcli AS tipo_cliente,
                    (SELECT COUNT(1) FROM cat_equipos WHERE id_sucursal = {$params->id}) AS equipos
                FROM cat_sucursales 
                WHERE id = '{$params->id}'";
        $response->sucursal = $this->conexion->queryRow($sql);
        return json_encode($response);
    }

    public function getContactos(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        
        $sql = "SELECT 
                    id, 
                    id_sucursal, 
                    principal, 
                    nombre, 
                    cargo, 
                    telefono, 
                    correo, 
                    area 
                FROM cat_sucursales_contactos
                WHERE id_sucursal = '{$params->id}'";
        $response->table_contactos = $this->conexion->queryAll($sql);
        return json_encode($response);
    }

    public function getFacturacion(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        
        $sql = "SELECT 
                    id, 
                    id_sucursal, 
                    principal,
                    rs AS razon_social, 
                    ruc AS cedula, 
                    direccion, 
                    telefono, 
                    correo
                FROM cat_sucursales_facturacion
                WHERE id_sucursal = '{$params->id}'";
        $response->table_facturacion = $this->conexion->queryAll($sql);
        return json_encode($response);
    }

    public function insertAll(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        $response->status = 400;

        if($params->data->id_cliente > 0){
            $sql = "INSERT INTO cat_sucursales
                    SET
                    id_cliente = '{$params->data->id_cliente}',
                    nombre_contacto = '{$params->sucursal->nombre}',
                    id_tipcli = '{$params->sucursal->tipo_cliente}',
                    razon_social = '{$params->sucursal->razon_social}',
                    ruc = '{$params->sucursal->cedula}',
                    direccion = '{$params->sucursal->direccion}',
                    telefono = '{$params->sucursal->telefono}',
                    fecha = CURRENT_DATE(),
                    id_usuario = '{$this->session->logged}'";
            $this->conexion->query($sql);
            $id_sucursal = $this->conexion->getLastID();

            if($id_sucursal > 0){
                if($params->contacto->nombre != ""){
                    $sql = "INSERT INTO cat_sucursales_contactos
                            SET
                            id_sucursal = '{$id_sucursal}',
                            nombre = '{$params->contacto->nombre}',
                            cargo = '{$params->contacto->cargo}',
                            telefono = '{$params->contacto->telefono}',
                            correo = '{$params->contacto->correo}',
                            area = '{$params->contacto->area}'";
                    $this->conexion->query($sql);
                }
                if($params->facturacion->razon_social != ""){
                    $sql = "INSERT INTO cat_sucursales_facturacion
                            SET
                            id_sucursal = '{$id_sucursal}',
                            rs = '{$params->facturacion->razon_social}',
                            ruc = '{$params->facturacion->cedula}',
                            direccion = '{$params->facturacion->direccion}',
                            telefono = '{$params->facturacion->telefono}',
                            correo = '{$params->facturacion->correo}'";
                    $this->conexion->query($sql);
                }
            }
            $response->status = 200;
        }
        
        return json_encode($response);
    }

    public function insertContacto(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;

        $sql = "INSERT INTO cat_sucursales_contactos
                SET
                id_sucursal = '{$params->id_sucursal}',
                nombre = '{$params->nombre}',
                cargo = '{$params->cargo}',
                telefono = '{$params->telefono}',
                correo = '{$params->correo}',
                area = '{$params->area}'";
        $this->conexion->query($sql);
        $response->status = 200;
        return json_encode($response);
    }

    public function insertFacturacion(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;

        $sql = "INSERT INTO cat_sucursales_facturacion
                SET
                id_sucursal = '{$params->id_sucursal}',
                rs = '{$params->razon_social}',
                ruc = '{$params->cedula}',
                direccion = '{$params->direccion}',
                telefono = '{$params->telefono}',
                correo = '{$params->correo}'";
        $this->conexion->query($sql);
        $response->status = 200;
        return json_encode($response);
    }

    public function editSucursal(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;

        $sql = "UPDATE cat_sucursales
                SET
                nombre_contacto = '{$params->nombre}',
                id_tipcli = '{$params->tipo_cliente}',
                razon_social = '{$params->razon_social}',
                ruc = '{$params->cedula}',
                direccion = '{$params->direccion}',
                telefono = '{$params->telefono}'
                WHERE id = '{$params->id}'";
        $this->conexion->query($sql);
        $response->status = 200;
        return json_encode($response);
    }

    public function editContacto(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;

        $sql = "UPDATE cat_sucursales_contactos
                SET
                nombre = '{$params->nombre}',
                cargo = '{$params->cargo}',
                telefono = '{$params->telefono}',
                correo = '{$params->correo}',
                area = '{$params->area}'
                WHERE id = '{$params->id}'";
        $this->conexion->query($sql);
        $response->status = 200;
        return json_encode($response);
    }

    public function editFacturacion(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;

        $sql = "UPDATE cat_sucursales_facturacion
                SET
                rs = '{$params->razon_social}',
                ruc = '{$params->cedula}',
                direccion = '{$params->direccion}',
                telefono = '{$params->telefono}',
                correo = '{$params->correo}'
                WHERE id = '{$params->id}'";
        $this->conexion->query($sql);
        $response->status = 200;
        return json_encode($response);
    }

    public function eliminarContacto(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;

        $sql = "DELETE FROM cat_sucursales_contactos WHERE id = '{$params->id}'";
        $this->conexion->query($sql);
        $response->status = 200;
        return json_encode($response);
    }

    public function eliminarFacturacion(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;

        $sql = "DELETE FROM cat_sucursales_facturacion WHERE id = '{$params->id}'";
        $this->conexion->query($sql);
        $response->status = 200;
        return json_encode($response);
    }

    public function principalContacto(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        $sql = "UPDATE cat_sucursales_contactos SET principal = '{$params->principal}' WHERE id = '{$params->id}'";
        $this->conexion->query($sql);
        $response->status = 200;
        return json_encode($response);
    }

    public function principalFacturacion(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        $sql = "UPDATE cat_sucursales_facturacion SET principal = '{$params->principal}' WHERE id = '{$params->id}'";
        $this->conexion->query($sql);
        $response->status = 200;
        return json_encode($response);
    }

    public function getDataPrincipal(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        
        $sql = "SELECT
                    rs AS razon_social,
                    ruc AS cedula,
                    direccion,
                    telefono,
                    correo AS email
                FROM cat_clientes_facturacion
                WHERE id = '{$params->id_cliente}' AND principal = 1 LIMIT 1";
        $response->facturacion = $this->conexion->queryRow($sql);
        return json_encode($response);
    }
}