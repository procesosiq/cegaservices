<?php
class ConfigTiposTrabajos {

	private $conexion;
    private $session;

    public function __construct(){
        $this->conexion = new M_Conexion;
        $this->session = Session::getInstance();
    }

    public function insert(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        $sWhere = "";

        if($params->id_trabajo > 0){
            $sWhere = " WHERE id = '{$params->id_trabajo}'";
        }

        $sql = "INSERT INTO cat_tipos_trabajo
                SET 
                descripcion = '{$params->trabajo}',
                status = '{$params->status}',
                clase = '{$params->color}'
                $sWhere";
        if($params->id_trabajo > 0){
            $sql = str_replace("INSERT INTO", "UPDATE", $sql);
        }
        $this->conexion->query($sql);
        $response->status = 200;
        return json_encode($response);
    }

    public function eliminar(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;

        $sql = "UPDATE cat_tipos_trabajo
                SET 
                status = 'Eliminado'
                WHERE id = '{$params->id_trabajo}'";
        $this->conexion->query($sql);
        $response->status = 200;
        return json_encode($response);
    }

    public function trabajos(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        
        $sWhere = ""; 

        $sql = "SELECT id, descripcion, clase AS color, status FROM cat_tipos_trabajo WHERE status != 'Eliminado'";
        $response->trabajos = $this->conexion->queryAll($sql);
        return json_encode($response);
    }

    public function colores(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        
        $sWhere = ""; 

        $sql = "SELECT id, codigo AS label, codigo AS color, status FROM cat_colores WHERE status = 'Activo'";
        $response->colores = $this->conexion->queryAll($sql);
        return json_encode($response);
    }

    public function show(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        
        $sql = "SELECT
                id AS id_trabajo,
                descripcion AS trabajo,
                clase AS color,
                status
                FROM cat_tipos_trabajo
                WHERE status != 'Eliminado' AND id = '{$params->id_trabajo}'";
        $response->data = $this->conexion->queryRow($sql);
        return json_encode($response);
    }

}