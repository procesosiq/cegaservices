<?php
class TrabajosEjecutados {

	private $conexion;
    private $session;

    public function __construct(){
        $this->conexion = new M_Conexion;
        $this->session = Session::getInstance();
    }

    public function listado(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;

        $sql = "SELECT * FROM (
                    SELECT 
                        reportes_r_correctivo.id AS id_trabajo, 
                        orden_trabajo.id AS id_orden, 
                        orden_trabajo.id_cotizacion, 
                        orden_trabajo.status, 
                        reportes_r_correctivo.numero_referencia, 
                        reportes_r_correctivo.fecha_correctivo AS fecha, 
                        reportes_r_correctivo.hora_llegada, 
                        reportes_r_correctivo.hora_salida, 
                        reportes_r_correctivo.cliente,
                        orden_trabajo.codigo AS orden
                    FROM reportes_r_correctivo
                    INNER JOIN orden_trabajo ON orden_trabajo.id = reportes_r_correctivo.id_orden
                    WHERE reportes_r_correctivo.numero_referencia IS NOT NULL GROUP BY reportes_r_correctivo.numero_referencia
                    UNION ALL
                    SELECT 
                        reportes_r_instalacion.id AS id_trabajo, 
                        orden_trabajo.id AS id_orden, 
                        orden_trabajo.id_cotizacion, 
                        orden_trabajo.status,
                        reportes_r_instalacion.numero_referencia, 
                        reportes_r_instalacion.fecha_instalacion AS fecha, 
                        reportes_r_instalacion.hora_llegada, 
                        reportes_r_instalacion.hora_salida, 
                        reportes_r_instalacion.cliente,
                        orden_trabajo.codigo AS orden
                    FROM reportes_r_instalacion
                    INNER JOIN orden_trabajo ON orden_trabajo.id = reportes_r_instalacion.id_orden
                    WHERE reportes_r_instalacion.numero_referencia IS NOT NULL GROUP BY reportes_r_instalacion.numero_referencia
                    UNION ALL
                    SELECT 
                        reportes_r_mantenimiento.id AS id_trabajo, 
                        orden_trabajo.id AS id_orden,
                        orden_trabajo.id_cotizacion, 
                        orden_trabajo.status, 
                        reportes_r_mantenimiento.numero_referencia, 
                        reportes_r_mantenimiento.fecha_mante AS fecha, 
                        reportes_r_mantenimiento.hora_llegada, 
                        reportes_r_mantenimiento.hora_salida, 
                        reportes_r_mantenimiento.cliente,
                        orden_trabajo.codigo AS orden
                    FROM reportes_r_mantenimiento
                    INNER JOIN orden_trabajo ON orden_trabajo.id = reportes_r_mantenimiento.id_orden
                    WHERE reportes_r_mantenimiento.numero_referencia IS NOT NULL GROUP BY reportes_r_mantenimiento.numero_referencia
                    UNION ALL
                    SELECT 
                        reportes_revision_correctivo.id AS id_trabajo, 
                        orden_trabajo.id AS id_orden, 
                        orden_trabajo.id_cotizacion, 
                        orden_trabajo.status,
                        reportes_revision_correctivo.numero_referencia, 
                        reportes_revision_correctivo.fecha_instalacion AS fecha, 
                        reportes_revision_correctivo.hora_llegada, 
                        reportes_revision_correctivo.hora_salida, 
                        reportes_revision_correctivo.cliente,
                        orden_trabajo.codigo AS orden
                    FROM reportes_revision_correctivo
                    INNER JOIN orden_trabajo ON orden_trabajo.id = reportes_revision_correctivo.id_orden
                    WHERE reportes_revision_correctivo.numero_referencia IS NOT NULL GROUP BY reportes_revision_correctivo.numero_referencia
                    UNION ALL
                    SELECT 
                        reportes_revision_instalacion.id AS id_trabajo, 
                        orden_trabajo.id AS id_orden, 
                        orden_trabajo.id_cotizacion, 
                        orden_trabajo.status,
                        reportes_revision_instalacion.numero_referencia, 
                        reportes_revision_instalacion.fecha_instalacion AS fecha, 
                        reportes_revision_instalacion.hora_llegada, 
                        reportes_revision_instalacion.hora_salida, 
                        reportes_revision_instalacion.cliente,
                        orden_trabajo.codigo AS orden
                    FROM reportes_revision_instalacion
                    INNER JOIN orden_trabajo ON orden_trabajo.id = reportes_revision_instalacion.id_orden
                    WHERE reportes_revision_instalacion.numero_referencia IS NOT NULL GROUP BY reportes_revision_instalacion.numero_referencia
                    UNION ALL
                    SELECT 
                        reportes_revision_mantenimiento.id AS id_trabajo, 
                        orden_trabajo.id AS id_orden, 
                        orden_trabajo.id_cotizacion, 
                        orden_trabajo.status,
                        reportes_revision_mantenimiento.numero_referencia, 
                        reportes_revision_mantenimiento.fecha_mante AS fecha, 
                        reportes_revision_mantenimiento.hora_llegada, 
                        reportes_revision_mantenimiento.hora_salida, 
                        reportes_revision_mantenimiento.cliente,
                        orden_trabajo.codigo AS orden
                    FROM reportes_revision_mantenimiento
                    INNER JOIN orden_trabajo ON orden_trabajo.id = reportes_revision_mantenimiento.id_orden
                    WHERE reportes_revision_mantenimiento.numero_referencia IS NOT NULL GROUP BY reportes_revision_mantenimiento.numero_referencia
            ) AS tbl GROUP BY id_orden ORDER BY fecha DESC";
        $response->list = $this->conexion->queryAll($sql);
        foreach($response->list as $row){
            $sql = "SELECT * FROM (
                        SELECT 
                            reportes_r_correctivo.id AS id_trabajo, 
                            orden_trabajo.id AS id_orden, 
                            reportes_r_correctivo.numero_referencia, 
                            reportes_r_correctivo.fecha_correctivo AS fecha, 
                            reportes_r_correctivo.hora_llegada, 
                            reportes_r_correctivo.hora_salida, 
                            reportes_r_correctivo.cliente,
                            orden_trabajo.codigo AS orden,
                            'CORRECTIVO' AS tipo_trabajo,
                            'reportes_r_correctivo' AS tabla
                        FROM reportes_r_correctivo
                        INNER JOIN orden_trabajo ON orden_trabajo.id = reportes_r_correctivo.id_orden
                        WHERE reportes_r_correctivo.numero_referencia IS NOT NULL AND orden_trabajo.id = {$row->id_orden}
                        GROUP BY reportes_r_correctivo.numero_referencia
                        UNION ALL
                        SELECT 
                            reportes_r_instalacion.id AS id_trabajo, 
                            orden_trabajo.id AS id_orden, 
                            reportes_r_instalacion.numero_referencia, 
                            reportes_r_instalacion.fecha_instalacion AS fecha, 
                            reportes_r_instalacion.hora_llegada, 
                            reportes_r_instalacion.hora_salida, 
                            reportes_r_instalacion.cliente,
                            orden_trabajo.codigo AS orden,
                            'INSTALACIÓN' AS tipo_trabajo,
                            'reportes_r_instalacion' AS tabla
                        FROM reportes_r_instalacion
                        INNER JOIN orden_trabajo ON orden_trabajo.id = reportes_r_instalacion.id_orden
                        WHERE reportes_r_instalacion.numero_referencia IS NOT NULL AND orden_trabajo.id = {$row->id_orden}
                        GROUP BY reportes_r_instalacion.numero_referencia
                        UNION ALL
                        SELECT 
                            reportes_r_mantenimiento.id AS id_trabajo, 
                            orden_trabajo.id AS id_orden, 
                            reportes_r_mantenimiento.numero_referencia, 
                            reportes_r_mantenimiento.fecha_mante AS fecha, 
                            reportes_r_mantenimiento.hora_llegada, 
                            reportes_r_mantenimiento.hora_salida, 
                            reportes_r_mantenimiento.cliente,
                            orden_trabajo.codigo AS orden,
                            'MANTENIMIENTO' AS tipo_trabajo,
                            'reportes_r_mantenimiento' AS tabla
                        FROM reportes_r_mantenimiento
                        INNER JOIN orden_trabajo ON orden_trabajo.id = reportes_r_mantenimiento.id_orden
                        WHERE reportes_r_mantenimiento.numero_referencia IS NOT NULL AND orden_trabajo.id = {$row->id_orden} 
                        GROUP BY reportes_r_mantenimiento.numero_referencia
                        UNION ALL
                        SELECT 
                            reportes_revision_correctivo.id AS id_trabajo, 
                            orden_trabajo.id AS id_orden, 
                            reportes_revision_correctivo.numero_referencia, 
                            reportes_revision_correctivo.fecha_instalacion AS fecha, 
                            reportes_revision_correctivo.hora_llegada, 
                            reportes_revision_correctivo.hora_salida, 
                            reportes_revision_correctivo.cliente,
                            orden_trabajo.codigo AS orden,
                            'REVISIÓN CORRECTIVO' AS tipo_trabajo,
                            'reportes_revision_correctivo' AS tabla
                        FROM reportes_revision_correctivo
                        INNER JOIN orden_trabajo ON orden_trabajo.id = reportes_revision_correctivo.id_orden
                        WHERE reportes_revision_correctivo.numero_referencia IS NOT NULL AND orden_trabajo.id = {$row->id_orden} 
                        GROUP BY reportes_revision_correctivo.numero_referencia
                        UNION ALL
                        SELECT 
                            reportes_revision_instalacion.id AS id_trabajo, 
                            orden_trabajo.id AS id_orden, 
                            reportes_revision_instalacion.numero_referencia, 
                            reportes_revision_instalacion.fecha_instalacion AS fecha, 
                            reportes_revision_instalacion.hora_llegada, 
                            reportes_revision_instalacion.hora_salida, 
                            reportes_revision_instalacion.cliente,
                            orden_trabajo.codigo AS orden,
                            'REVISIÓN INSTALACIÓN' AS tipo_trabajo,
                            'reportes_revision_instalacion' AS tabla
                        FROM reportes_revision_instalacion
                        INNER JOIN orden_trabajo ON orden_trabajo.id = reportes_revision_instalacion.id_orden
                        WHERE reportes_revision_instalacion.numero_referencia IS NOT NULL AND orden_trabajo.id = {$row->id_orden} 
                        GROUP BY reportes_revision_instalacion.numero_referencia
                        UNION ALL
                        SELECT 
                            reportes_revision_mantenimiento.id AS id_trabajo, 
                            orden_trabajo.id AS id_orden, 
                            reportes_revision_mantenimiento.numero_referencia, 
                            reportes_revision_mantenimiento.fecha_mante AS fecha, 
                            reportes_revision_mantenimiento.hora_llegada, 
                            reportes_revision_mantenimiento.hora_salida, 
                            reportes_revision_mantenimiento.cliente,
                            orden_trabajo.codigo AS orden,
                            'REVISIÓN MANTENIMIENTO' AS tipo_trabajo,
                            'reportes_revision_mantenimiento' AS tabla
                        FROM reportes_revision_mantenimiento
                        INNER JOIN orden_trabajo ON orden_trabajo.id = reportes_revision_mantenimiento.id_orden
                        WHERE reportes_revision_mantenimiento.numero_referencia IS NOT NULL AND orden_trabajo.id = {$row->id_orden} 
                        GROUP BY reportes_revision_mantenimiento.numero_referencia
                ) AS tbl";
            $row->detalle = $this->conexion->queryAll($sql);
            foreach($row->detalle as $detalle){
                if($detalle->tipo_trabajo == 'INSTALACION' || $detalle->tipo_trabajo == 'MANTENIMIENTO' || $detalle->tipo_trabajo == 'CORRECTIVO'){
                    $detalle->hora_llegada = explode("-", explode("T", $detalle->hora_llegada)[1])[0];
                } else {
                    $detalle->hora_llegada = explode("-", $detalle->hora_llegada)[0];
                }

                $detalle->hora_salida = explode(" ", $detalle->hora_salida)[1];
            }
        }
        return json_encode($response);
    }
}