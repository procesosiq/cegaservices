<?php

// session_start();
include 'class.sesion.php';
include 'conexion.php';



class Aceso  { 

	private $conexion;
	private $session;


    public function __construct(){
    	$this->session = Session::getInstance();
        $this->conexion = new M_Conexion;

    }

	private function redirect($url = "login.php"){
		header('Location: http://cegaservices2.procesos-iq.com/'.$url);
	}

	public function login(){
		
		$data = [
			'user' => preg_replace("~[^\-\_\@\.a-z0-9]~i", "", trim(strtolower($_POST["username"]))),
			'pass' => preg_replace("/[^a-z0-9\,\.\-\_]/i", "", $_POST["password"])
		];

		if (empty($data['user']) OR empty($data['pass'])) {
			$this->redirect("login.php");
		}

		$consulta = "SELECT * FROM cat_usuarios WHERE usuario = '".$data['user']."'  AND pass = '".$data['pass']."';";
		$result = array();

		$result = $this->conexion->Consultas(2, $consulta);
		$filas = count($result);

		if($filas > 0){
			$account = $result[0];
			$_SESSION["CEGASERVICE"] = $account['id'];
			$this->session = new Session($account['usuario'] , $account['id']);
			$this->session->logged = $account['id'];
			$this->session->id = $account['id'];
			$this->session->tipo_usuario = $account['tipo_usuario'];
			$this->session->tipo_usuario_nombre = $account['tipo_usuario_nombre'];
			$this->session->nombre = $account['nombre'];
			$this->session->id_supervisor = $account['id_supervisor'];

			if($account['tipo_usuario'] == 1 || $account['tipo_usuario'] == 4){
				$this->redirect("clienteList");
			}else if($account['tipo_usuario'] == 5){
				$this->redirect("readCalendar");
			}else if($account['tipo_usuario'] == 3){
				$this->redirect("cotizacionList");
			}
		}else{
			$this->redirect("login.php");
		}

	}

	private function getDetailAccountCliente(){
		$sql_client = "SELECT * FROM cat_clientes WHERE id_usuario = '{$this->session->logged}' AND status > 0";
		$cliente = (object)[];
		$cliente = (object)$this->conexion->Consultas(2, $sql_client);

		return $cliente;
	}

	// public function is_logged() {
	// 	return isset($this->session->logged);
	// }

	// public function if_not_logged_redirect() {
	// 	if (!$this->is_logged()) {
	// 		$this->redirect("login.php");
	// 	}
	// }

	// public function if_logged_redirect() {
	// 	if ($this->is_logged()) {
	// 		$this->redirect("index.php");
	// 	}
	// }

	public function logout(){

		$this->session->kill();

	}

}



$aceso = new Aceso;
$aceso->login();
// if($aceso->login()){

// 	$_SESSION["login"] = 1;

	

//     exit;

// }else{

// 	header('Location: http://cegaservices2.procesos-iq.com/login.php');

//     exit;

// }





?>