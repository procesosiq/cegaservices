<?php

class Reportes { 
     
    private $conexion;
    
    public function __construct(){
        $this->conexion = new M_Conexion;
        $this->session = Session::getInstance();
    }
    
    public function index(){

        $sWhere = "";
        $sOrder = " ORDER BY nombre";
        $DesAsc = "ASC";
        $sOrder .= " {$DesAsc}";
        $sLimit = "";
        // print_r($_POST);
        if(isset($_POST)){

            /*----------  ORDER BY ----------*/
            
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY cat_clientes.id {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY cat_clientes.fecha {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY cat_clientes.nombre {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY id_tipcli {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 5){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY (SELECT COUNT(*) FROM cat_sucursales WHERE id_cliente=cat_clientes.id) {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 6){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY COUNT(cat_equipos.id) {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 7){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY cat_clientes.status {$DesAsc}";
            }
            /*----------  ORDER BY ----------*/

            if(isset($_POST['order_id']) && trim($_POST['order_id']) != ""){
                $sWhere .= " AND cat_clientes.id = ".$_POST["order_id"];
            }               
            if((isset($_POST['order_date_from']) && trim($_POST['order_date_from']) != "") && (isset($_POST['order_date_to']) && trim($_POST['order_date_to']) != "")){
                $sWhere .= " AND cat_clientes.fecha BETWEEN '".$_POST["order_date_from"]."' AND '".$_POST["order_date_to"]."'";
            }
            if(isset($_POST['order_customer_name']) && trim($_POST['order_customer_name']) != ""){
                $sWhere .= " AND nombre LIKE '%".$_POST['order_customer_name']."%'";
            }
            if(isset($_POST['order_ship_to']) && trim($_POST['order_ship_to']) != ""){
                $sWhere .= " AND (SELECT nombre FROM cat_clientesTipo WHERE id=id_tipcli)  LIKE '%".$_POST['order_ship_to']."%'";
            }
            if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
                $sWhere .= " AND cat_clientes.status = ".$_POST["order_status"];
            }

            /*----------  LIMIT  ----------*/
            if(isset($_POST['length']) && $_POST['length'] > 0){
                $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
            }
        }#validaciones


        $sql = "SELECT cat_clientes.id,DATE_FORMAT(cat_clientes.fecha,'%d/%m/%Y') AS fecha,nombre,
        (SELECT nombre FROM cat_clientesTipo WHERE id=id_tipcli) AS tipo_cliente,
        (SELECT COUNT(*) FROM cat_sucursales WHERE id_cliente=cat_clientes.id) AS totSucursal,sucursales,
        COUNT(cat_equipos.id) AS totEquipos,IF(cat_clientes.STATUS=1,'Activo','Inactivo') AS edocli,
        (SELECT COUNT(*) FROM cat_clientes WHERE id_usuario = '{$this->session->logged}') AS totalRows
        FROM cat_clientes
        LEFT JOIN cat_equipos ON cat_clientes.id = id_cliente
        WHERE 1=1 AND cat_clientes.id_usuario = '{$this->session->logged}' $sWhere 
        GROUP BY cat_clientes.id
        $sOrder $sLimit";
        // print $sql;
        $res = $this->conexion->link->query($sql);

        $datos = (object)[];
        $datos->data = [];
        while($fila = $res->fetch_assoc()){
            $fila = (object)$fila;
            
            $botonn="";
            if($fila->sucursales=='SI'){
                $botonn='<button id="sucu" class="btn btn-sm green btn-outline filter-submit margin-bottom"><i class="fa fa-plus"></i> Sucursales</button>';
            }
            else{
                $botonn='<button id="equi" class="btn btn-sm green btn-outline filter-submit margin-bottom"><i class="fa fa-plus"></i> Equipos</button>';
            }
            
            $datos->data[] = array (
                $fila->id,
                $fila->id,
                $fila->fecha,
                strtoupper($fila->nombre),
                strtoupper($fila->tipo_cliente),
                $fila->totSucursal,
                $fila->totEquipos,
                ($fila->edocli == 'Inactivo') ? '<button id="status" class="btn btn-sm red-thunderbird">'.$fila->edocli.'</button>' : '<button class="btn btn-sm green-jungle" id="status">'.$fila->edocli.'</button>',
                '<button id="edit" class="btn btn-sm green btn-outline filter-submit margin-bottom"><i class="fa fa-plus"></i> Editar</button>',
                $botonn
            );

            $datos->recordsTotal = $fila->totalRows;
        }
    }

    public function ListInventarios(){
        $sWhere = "";
        $sSupervisor = "";
        $sOrder = " ORDER BY nombre";
        $DesAsc = "ASC";
        $sOrder .= " {$DesAsc}";
        $sLimit = "";
        
        if(isset($_POST)){
            /*----------  ORDER BY ----------*/
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 0){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY fecha_agendada {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY nombre {$DesAsc}";
            }   
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY cliente {$DesAsc}";
            }   
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY codigo {$DesAsc}";
            }   
            /*if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY cat_clientes.nombre {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY id_tipcli {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 5){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY (SELECT COUNT(*) FROM cat_sucursales WHERE id_cliente=cat_clientes.id) {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 6){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY COUNT(cat_equipos.id) {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 7){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY cat_clientes.status {$DesAsc}";
            }*/
            /*----------  WHERE ----------*/
            if(isset($_POST['order_supervisor']) && trim($_POST['order_supervisor']) != ""){
                $sSupervisor .= " AND cat_supervisores.nombre LIKE '%".$_POST["order_supervisor"]."%' ";
            }
            if(isset($_POST['order_ot']) && trim($_POST['order_ot']) != ""){
                $sSupervisor .= " AND orden_trabajo.codigo LIKE '%".$_POST["order_ot"]."%' ";
            }
            if(isset($_POST['order_cliente']) && trim($_POST['order_cliente']) != ""){
                $sSupervisor .= " AND cat_clientes.nombre LIKE '%".$_POST["order_cliente"]."%' ";
            }

            /*if(isset($_POST['from_date']) && trim($_POST['from_date']) != "" 
                && isset($_POST['to_date']) && trim($_POST['to_date']) != ""){
                $sSupervisor .= " AND DATE(fecha_agendada) >= '".$_POST["from_date"]."' ";
                $sSupervisor .= " AND DATE(fecha_agendada) <= '".$_POST["to_date"]."' ";
            }

            /*----------  LIMIT  ----------*/
            if(isset($_POST['length']) && $_POST['length'] > 0){
                $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
            }
        }

        $sql = "
            SELECT * FROM (
                SELECT GROUP_CONCAT(orden_trabajo.id SEPARATOR ',') AS cc, 
                    cat_supervisores.nombre, 
                    cat_clientes.nombre as cliente,
                    orden_trabajo.codigo,
                    DATE(fecha_agendada) as fecha_agendada, 
                    (SELECT COUNT(*) FROM orden_trabajo 
                        INNER JOIN cat_supervisores ON grupo_trabajo = cat_supervisores.id
                        INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id
                        INNER JOIN cat_clientes ON id_cliente = cat_clientes.id
                        WHERE 1=1 {$sSupervisor}) AS totalRows
                FROM orden_trabajo
                INNER JOIN cat_supervisores ON grupo_trabajo = cat_supervisores.id
                INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id
                INNER JOIN cat_clientes ON id_cliente = cat_clientes.id
                WHERE 1=1 {$sSupervisor}
                GROUP BY grupo_trabajo
            ) as tbl
            $sOrder";

        $res = $this->conexion->link->query($sql);
        $datos = (object)[];
        $totalRows = 0;

        while($fila = $res->fetch_assoc()){
            $ids = $fila["cc"];
            $items = array();
            $totalRows = $fila["totalRows"];

            $sql2 = "
            SELECT * FROM (
                (SELECT mat_item1_sel, mat_item1_des, mat_item1_uni, mat_item1_cant, mat_item2_sel, mat_item2_des, mat_item2_uni, mat_item2_cant, mat_item3_sel, mat_item3_des, mat_item3_uni, mat_item3_cant, mat_item4_sel, mat_item4_des, mat_item4_uni, mat_item4_cant, mat_item5_sel, mat_item5_des, mat_item5_uni, mat_item5_cant, mat_item6_sel, mat_item6_des, mat_item6_uni, mat_item6_cant, mat_item7_sel, mat_item7_des, mat_item7_uni, mat_item7_cant, mat_item8_sel, mat_item8_des, mat_item8_uni, mat_item8_cant, mat_item9_sel, mat_item9_des, mat_item9_uni, mat_item9_cant, mat_item10_sel, mat_item10_des, mat_item10_uni, mat_item10_cant, id_orden, fecha_correctivo as fecha, (SELECT responsable FROM orden_trabajo WHERE id = id_orden) as id_supervisor, (SELECT codigo FROM orden_trabajo WHERE id = id_orden) as ot, cliente FROM `reportes_r_correctivo` WHERE id_orden IN({$ids}))
                UNION ALL 
                (SELECT mat_item1_sel, mat_item1_des, mat_item1_uni, mat_item1_cant, mat_item2_sel, mat_item2_des, mat_item2_uni, mat_item2_cant, mat_item3_sel, mat_item3_des, mat_item3_uni, mat_item3_cant, mat_item4_sel, mat_item4_des, mat_item4_uni, mat_item4_cant, mat_item5_sel, mat_item5_des, mat_item5_uni, mat_item5_cant, mat_item6_sel, mat_item6_des, mat_item6_uni, mat_item6_cant, mat_item7_sel, mat_item7_des, mat_item7_uni, mat_item7_cant, mat_item8_sel, mat_item8_des, mat_item8_uni, mat_item8_cant, mat_item9_sel, mat_item9_des, mat_item9_uni, mat_item9_cant, mat_item10_sel, mat_item10_des, mat_item10_uni, mat_item10_cant, id_orden, fecha_instalacion as fecha, (SELECT responsable FROM orden_trabajo WHERE id = id_orden) as id_supervisor, (SELECT codigo FROM orden_trabajo WHERE id = id_orden) as ot, cliente FROM `reportes_r_instalacion`  WHERE id_orden IN({$ids}))
                UNION ALL
                (SELECT mat_item1_sel, mat_item1_des, mat_item1_uni, mat_item1_cant, mat_item2_sel, mat_item2_des, mat_item2_uni, mat_item2_cant, mat_item3_sel, mat_item3_des, mat_item3_uni, mat_item3_cant, mat_item4_sel, mat_item4_des, mat_item4_uni, mat_item4_cant, mat_item5_sel, mat_item5_des, mat_item5_uni, mat_item5_cant, mat_item6_sel, mat_item6_des, mat_item6_uni, mat_item6_cant, mat_item7_sel, mat_item7_des, mat_item7_uni, mat_item7_cant, mat_item8_sel, mat_item8_des, mat_item8_uni, mat_item8_cant, mat_item9_sel, mat_item9_des, mat_item9_uni, mat_item9_cant, mat_item10_sel, mat_item10_des, mat_item10_uni, mat_item10_cant, id_orden, fecha_mante as fecha, (SELECT responsable FROM orden_trabajo WHERE id = id_orden) as id_supervisor, (SELECT codigo FROM orden_trabajo WHERE id = id_orden) as ot, cliente FROM `reportes_r_mantenimiento` WHERE id_orden IN({$ids}))
                UNION ALL
                (SELECT mat_item1_sel, mat_item1_des, mat_item1_uni, mat_item1_cant, mat_item2_sel, mat_item2_des, mat_item2_uni, mat_item2_cant, mat_item3_sel, mat_item3_des, mat_item3_uni, mat_item3_cant, mat_item4_sel, mat_item4_des, mat_item4_uni, mat_item4_cant, mat_item5_sel, mat_item5_des, mat_item5_uni, mat_item5_cant, mat_item6_sel, mat_item6_des, mat_item6_uni, mat_item6_cant, mat_item7_sel, mat_item7_des, mat_item7_uni, mat_item7_cant, mat_item8_sel, mat_item8_des, mat_item8_uni, mat_item8_cant, mat_item9_sel, mat_item9_des, mat_item9_uni, mat_item9_cant, mat_item10_sel, mat_item10_des, mat_item10_uni, mat_item10_cant, id_orden, fecha_instalacion as fecha, (SELECT responsable FROM orden_trabajo WHERE id = id_orden) as id_supervisor, (SELECT codigo FROM orden_trabajo WHERE id = id_orden) as ot, cliente FROM `reportes_revision_instalacion` WHERE id_orden IN({$ids}))
                UNION ALL
                (SELECT mat_item1_sel, mat_item1_des, mat_item1_uni, mat_item1_cant, mat_item2_sel, mat_item2_des, mat_item2_uni, mat_item2_cant, mat_item3_sel, mat_item3_des, mat_item3_uni, mat_item3_cant, mat_item4_sel, mat_item4_des, mat_item4_uni, mat_item4_cant, mat_item5_sel, mat_item5_des, mat_item5_uni, mat_item5_cant, mat_item6_sel, mat_item6_des, mat_item6_uni, mat_item6_cant, mat_item7_sel, mat_item7_des, mat_item7_uni, mat_item7_cant, mat_item8_sel, mat_item8_des, mat_item8_uni, mat_item8_cant, mat_item9_sel, mat_item9_des, mat_item9_uni, mat_item9_cant, mat_item10_sel, mat_item10_des, mat_item10_uni, mat_item10_cant, id_orden, fecha_instalacion as fecha, (SELECT responsable FROM orden_trabajo WHERE id = id_orden) as id_supervisor, (SELECT codigo FROM orden_trabajo WHERE id = id_orden) as ot, cliente FROM `reportes_revision_correctivo` WHERE id_orden IN({$ids}))
            ) AS tab
            WHERE 1=1 {$sWhere}
            ORDER BY fecha";

            $res2 = $this->conexion->link->query($sql2);
            while($fila2 = $res2->fetch_assoc()){
                if($fila2["mat_item1_sel"] != "")
                    $items[] = array("sel" => $fila2["mat_item1_sel"], "des" => $fila2["mat_item1_des"], "uni" => $fila2["mat_item1_uni"], "cant" => $fila2["mat_item1_cant"], "fecha" => $fila2["fecha"], "supervisor" => $fila2["id_supervisor"], "cliente" => $fila2["cliente"], "ot" => $fila2["ot"]);
                if($fila2["mat_item2_sel"] != "")
                    $items[] = array("sel" => $fila2["mat_item2_sel"], "des" => $fila2["mat_item2_des"], "uni" => $fila2["mat_item2_uni"], "cant" => $fila2["mat_item2_cant"], "fecha" => $fila2["fecha"], "supervisor" => $fila2["id_supervisor"], "cliente" => $fila2["cliente"], "ot" => $fila2["ot"]);
                if($fila2["mat_item3_sel"] != "")
                    $items[] = array("sel" => $fila2["mat_item3_sel"], "des" => $fila2["mat_item3_des"], "uni" => $fila2["mat_item3_uni"], "cant" => $fila2["mat_item3_cant"], "fecha" => $fila2["fecha"], "supervisor" => $fila2["id_supervisor"], "cliente" => $fila2["cliente"], "ot" => $fila2["ot"]);
                if($fila2["mat_item4_sel"] != "")
                    $items[] = array("sel" => $fila2["mat_item4_sel"], "des" => $fila2["mat_item4_des"], "uni" => $fila2["mat_item4_uni"], "cant" => $fila2["mat_item4_cant"], "fecha" => $fila2["fecha"], "supervisor" => $fila2["id_supervisor"], "cliente" => $fila2["cliente"], "ot" => $fila2["ot"]);
                if($fila2["mat_item5_sel"] != "")
                    $items[] = array("sel" => $fila2["mat_item5_sel"], "des" => $fila2["mat_item5_des"], "uni" => $fila2["mat_item5_uni"], "cant" => $fila2["mat_item5_cant"], "fecha" => $fila2["fecha"], "supervisor" => $fila2["id_supervisor"], "cliente" => $fila2["cliente"], "ot" => $fila2["ot"]);
                if($fila2["mat_item6_sel"] != "")
                    $items[] = array("sel" => $fila2["mat_item6_sel"], "des" => $fila2["mat_item6_des"], "uni" => $fila2["mat_item6_uni"], "cant" => $fila2["mat_item6_cant"], "fecha" => $fila2["fecha"], "supervisor" => $fila2["id_supervisor"], "cliente" => $fila2["cliente"], "ot" => $fila2["ot"]);
                if($fila2["mat_item7_sel"] != "")
                    $items[] = array("sel" => $fila2["mat_item7_sel"], "des" => $fila2["mat_item7_des"], "uni" => $fila2["mat_item7_uni"], "cant" => $fila2["mat_item7_cant"], "fecha" => $fila2["fecha"], "supervisor" => $fila2["id_supervisor"], "cliente" => $fila2["cliente"], "ot" => $fila2["ot"]);
                if($fila2["mat_item8_sel"] != "")
                    $items[] = array("sel" => $fila2["mat_item8_sel"], "des" => $fila2["mat_item8_des"], "uni" => $fila2["mat_item8_uni"], "cant" => $fila2["mat_item8_cant"], "fecha" => $fila2["fecha"], "supervisor" => $fila2["id_supervisor"], "cliente" => $fila2["cliente"], "ot" => $fila2["ot"]);
                if($fila2["mat_item9_sel"] != "")
                    $items[] = array("sel" => $fila2["mat_item9_sel"], "des" => $fila2["mat_item9_des"], "uni" => $fila2["mat_item9_uni"], "cant" => $fila2["mat_item9_cant"], "fecha" => $fila2["fecha"], "supervisor" => $fila2["id_supervisor"], "cliente" => $fila2["cliente"], "ot" => $fila2["ot"]);
                if($fila2["mat_item10_sel"] != "")
                    $items[] = array("sel" => $fila2["mat_item10_sel"], "des" => $fila2["mat_item10_des"], "uni" => $fila2["mat_item10_uni"], "cant" => $fila2["mat_item10_cant"], "fecha" => $fila2["fecha"], "supervisor" => $fila2["id_supervisor"], "cliente" => $fila2["cliente"], "ot" => $fila2["ot"]);
            }

            $datos->data = array();
            for($x = 0; $x < count($items); $x++){
                if(isset($items[$x]["sel"]) && $items[$x]["sel"] != ""){
                    $item = (object) $items[$x];
                    $item->cant = (int) $item->cant;

                    for($y = $x+1; $y < count($items); $y++){
                        if($item->sel == $items[$y]["sel"] && $item->uni == $items[$y]["uni"] && $item->des == $items[$y]["des"] && $item->fecha == $items[$y]["fecha"]){
                            $item->cant = ($item->cant * 1) + ($items[$y]["cant"] * 1);
                            unset($items[$y]);
                        }
                    }
                    $entregado = "SELECT IFNULL(SUM(cantidad_requerida), 0) as material FROM orden_trabajo_materiales
                                    INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id
                                    WHERE DATE(fecha_agendada) = '{$item->fecha}' AND seleccion = '{$item->sel}' AND orden_trabajo_materiales.status = 1
                                    GROUP BY fecha_agendada";
                    $entregado = $this->conexion->link->query($entregado);
                    $entregado = $entregado->fetch_assoc();
                    $item->entregado = ($entregado["material"] != null) ? $entregado["material"] : 0;
                    $item->saldo = (($item->entregado - $item->cant) > 0) ? $item->entregado - $item->cant : 0;
                    
                    $datos->rows[] = $item;
                }
            }
        }

        $formato = 'Y-m-d';
        if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4){
            $DesAsc = $_POST['order'][0]['dir'];
            if(strtoupper($DesAsc) == "DESC"){ // DECENDENTE
                usort($datos->rows, function($a, $b){
                    $c = strcmp($b->sel, $a->sel);
                    return $c;
                });
            }else{ // ACENDENTE
                usort($datos->rows, function($a, $b){
                    $c = strcmp($a->sel, $b->sel);
                    return $c;
                });
            }
        }

        $datos->data = array();
        $datos->recordsTotal = 0;
        for($x = 0; $x < count($datos->rows); $x++){
            $item = (object) $datos->rows[$x];

            $filtrado = false;

            if(isset($_POST['from_date']) && trim($_POST['from_date']) != "" 
                && isset($_POST['to_date']) && trim($_POST['to_date']) != ""){ #filtro de fecha
                $fecha_item = DateTime::createFromFormat($formato, $item->fecha);
                $fecha_from = DateTime::createFromFormat($formato, $_POST['from_date']);
                $fecha_to = DateTime::createFromFormat($formato, $_POST['to_date']);
                if(!($fecha_item >= $fecha_from && $fecha_item <= $fecha_to)){
                    $filtrado = true;
                }
            }
            if(isset($_POST["order_item"]) && $_POST["order_item"] != ""){ #filtro de item
                if(strpos(strtolower($item->sel), strtolower($_POST["order_item"])) === false){
                    $filtrado = true;
                }
            }
            if(isset($_POST["order_descripcion"]) && $_POST["order_descripcion"] != ""){ #filtro de descripcion
                if(strpos(strtolower($item->des), strtolower($_POST["order_descripcion"])) === false){
                    $filtrado = true;
                }
            }

            if(!$filtrado){
                $datos->data[] = array(
                    $item->fecha,
                    $item->ot,
                    $item->cliente,
                    $item->supervisor,
                    $item->sel,
                    $item->des,
                    $item->uni,
                    $item->entregado,
                    $item->cant,
                    $item->saldo,
                    '');
            }
        }
        unset($datos->rows);
        $datos->recordsTotal = $totalRows;
        $datos->customActionStatus = "OK";
        return json_encode($datos);
    }

    public function historicoEquipos(){
        $filters = (object)$_POST;
        // print_r($filters);
        if(isset($_POST)){

            /*----------  ORDER BY ----------*/
            
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY equipos.id {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY equipos.fecha {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY equipos.codigo {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY equipos.tipo_trabajo {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 5){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY equipos.responsable {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 6){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY equipos.status {$DesAsc}";
            }
            /*----------  ORDER BY ----------*/
            $sWhere = "";
            if(isset($_POST['order_id']) && trim($_POST['order_id']) != ""){
                $sWhere .= " AND cat_clientes.id = ".$_POST["order_id"];
            }               
            if((isset($_POST['order_date_from']) && trim($_POST['order_date_from']) != "") && (isset($_POST['order_date_to']) && trim($_POST['order_date_to']) != "")){
                $sWhere .= " AND cat_clientes.fecha BETWEEN '".$_POST["order_date_from"]."' AND '".$_POST["order_date_to"]."'";
            }
            if(isset($_POST['order_customer_name']) && trim($_POST['order_customer_name']) != ""){
                $sWhere .= " AND nombre LIKE '%".$_POST['order_customer_name']."%'";
            }
            if(isset($_POST['order_ship_to']) && trim($_POST['order_ship_to']) != ""){
                $sWhere .= " AND (SELECT nombre FROM cat_clientesTipo WHERE id=id_tipcli)  LIKE '%".$_POST['order_ship_to']."%'";
            }
            if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
                $sWhere .= " AND cat_clientes.status = ".$_POST["order_status"];
            }

            /*----------  LIMIT  ----------*/
            if(isset($_POST['length']) && $_POST['length'] > 0){
                $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
            }
        }

        $datos = new stdClass;
        $datos->data = [];
        $datos->recordsTotal = 0;
        $sucursalWhere = "";
        $sWhereCliente = "";
        $sWhere_revision = "";

        if(isset($filters->cliente) && $filters->cliente != ""){
            $sWhere .= " AND equipos.id_cliente = {$filters->cliente}";
            $sWhereCliente .= " AND cotizaciones.id_cliente = {$filters->cliente}";
            $sWhere_revision .= " AND cotizaciones.id_cliente = {$filters->cliente}";
            
            if(isset($filters->codigo) && $filters->codigo != ""){
                $sWhere .= " AND equipos.codigo_equipo = '{$filters->codigo}'";
            }
            
            if(isset($filters->sucursal) && $filters->sucursal != ""){
                $sucursalWhere .= "AND equipos.id_sucursal = '{$filters->sucursal}'";
            }

            if(isset($filters->tTrabajo) && $filters->tTrabajo != "" && $filters->tTrabajo > 0){
                $sql = "SELECT descripcion FROM cat_tipos_trabajo WHERE id = '{$filters->tTrabajo}'";
                $trabajo = $this->conexion->queryRow($sql)->descripcion;

                $sWhere .= " AND tipo_trabajo LIKE '%{$trabajo}%'";     
            }

            $sql = "SELECT equipos.id, DATE(equipos.fecha) AS fecha, equipos.codigo, codigo_equipo, equipos.tipo_trabajo, equipos.responsable AS supervisor,
                    CASE 
                    WHEN equipos.status = 1 THEN 'Por Agendar'
                    WHEN equipos.status = 2 THEN 'Agendada'
                    WHEN equipos.status = 3 THEN 'Finalizado'
                    END AS STATUS , equipos.status AS statusValue
                    FROM cotizaciones
                    INNER JOIN (
                        (SELECT orden_trabajo.*, 'N/A' AS codigo_equipo, cotizaciones.id_cliente, cotizaciones.id_sucursal FROM orden_trabajo INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id WHERE orden_trabajo.tipo_trabajo LIKE '%REVISION INSTALACION%' $sWhereCliente) 
                        UNION
                        (SELECT orden_trabajo.*, 'N/A' AS codigo_equipo, cotizaciones.id_cliente, cotizaciones.id_sucursal FROM orden_trabajo INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id WHERE orden_trabajo.tipo_trabajo LIKE '%REVISION MANTENIMIENTO%' $sWhereCliente) 
                        UNION
                        #mantenimiento
                        (SELECT orden_trabajo.*, man_caracteristicas_climatizacion_a.codigo AS codigo_equipo, cotizaciones.id_cliente, cotizaciones.id_sucursal FROM man_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE man_caracteristicas_climatizacion_a.status = 1) 
                        UNION 
                        (SELECT orden_trabajo.*, man_caracteristicas_climatizacion_b.codigo AS codigo_equipo, cotizaciones.id_cliente, cotizaciones.id_sucursal FROM man_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE man_caracteristicas_climatizacion_b.status = 1)
                        UNION
                        (SELECT orden_trabajo.*, man_caracteristicas_refrigeracion.codigo AS codigo_equipo, cotizaciones.id_cliente, cotizaciones.id_sucursal FROM man_caracteristicas_refrigeracion INNER JOIN cat_areas ON id_area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON id_equipo_des = cat_descripciones_equipos.`id` WHERE man_caracteristicas_refrigeracion.status = 1)
                        UNION
                        (SELECT orden_trabajo.*, man_caracteristicas_ventilacion.codigo AS codigo_equipo, cotizaciones.id_cliente, cotizaciones.id_sucursal FROM man_caracteristicas_ventilacion INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE man_caracteristicas_ventilacion.status = 1)
                        #instalacion
                        UNION
                        (SELECT orden_trabajo.*, ins_caracteristicas_climatizacion_a.codigo AS codigo_equipo, cotizaciones.id_cliente, cotizaciones.id_sucursal FROM ins_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE ins_caracteristicas_climatizacion_a.status = 1) 
                        UNION 
                        (SELECT orden_trabajo.*, ins_caracteristicas_climatizacion_b.codigo AS codigo_equipo, cotizaciones.id_cliente, cotizaciones.id_sucursal FROM ins_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE ins_caracteristicas_climatizacion_b.status = 1)
                        UNION
                        (SELECT orden_trabajo.*,ins_caracteristicas_refrigeracion.codigo AS codigo_equipo, cotizaciones.id_cliente, cotizaciones.id_sucursal  FROM ins_caracteristicas_refrigeracion INNER JOIN cat_areas ON id_area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON id_equipo_des = cat_descripciones_equipos.`id` WHERE ins_caracteristicas_refrigeracion.status = 1)
                        UNION
                        (SELECT orden_trabajo.*, ins_caracteristicas_ventilacion.codigo AS codigo_equipo, cotizaciones.id_cliente, cotizaciones.id_sucursal FROM ins_caracteristicas_ventilacion INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE ins_caracteristicas_ventilacion.status = 1)
                        #correctivo
                        UNION
                        (SELECT orden_trabajo.*, cor_caracteristicas_climatizacion_a.codigo AS codigo_equipo, cotizaciones.id_cliente, cotizaciones.id_sucursal FROM cor_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE cor_caracteristicas_climatizacion_a.status = 1) 
                        UNION 
                        (SELECT orden_trabajo.*, cor_caracteristicas_climatizacion_b.codigo AS codigo_equipo, cotizaciones.id_cliente, cotizaciones.id_sucursal FROM cor_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE cor_caracteristicas_climatizacion_b.status = 1)
                        UNION
                        (SELECT orden_trabajo.*, cor_caracteristicas_refrigeracion.codigo AS codigo_equipo, cotizaciones.id_cliente, cotizaciones.id_sucursal FROM cor_caracteristicas_refrigeracion INNER JOIN cat_areas ON id_area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON id_equipo_des = cat_descripciones_equipos.`id` WHERE cor_caracteristicas_refrigeracion.status = 1)
                        UNION
                        (SELECT orden_trabajo.*, cor_caracteristicas_ventilacion.codigo AS codigo_equipo, cotizaciones.id_cliente, cotizaciones.id_sucursal FROM cor_caracteristicas_ventilacion INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE cor_caracteristicas_ventilacion.status = 1)
                    ) AS equipos ON equipos.id_cotizacion = cotizaciones.id
                    INNER JOIN cat_clientes ON cat_clientes.id = cotizaciones.`id_cliente`
                    #LEFT JOIN cat_sucursales ON cat_sucursales.id_cliente = cotizaciones.id_sucursal
                    LEFT JOIN cat_equipos ON cat_equipos.id_cliente = cat_clientes.id
                    WHERE equipos.status = 3 $sWhere $sucursalWhere
                    GROUP BY equipos.codigo_equipo, equipos.id";
                //echo $sql;
                     $res = $this->conexion->link->query($sql);
            $count = 0;
            $labelStatus = [
                1 =>  '<span class="btn btn-sm bg-red-thunderbird bg-font-red-thunderbird" id="status">Por Agendar</span>',
                2 =>  '<span class="btn btn-sm bg-yellow-gold bg-font-yellow-gold" id="status">Agendada</span>',
                3 =>  '<span class="btn btn-sm bg-green-jungle bg-font-green-jungle" id="status">Finalizada</span>',
            ];
            while($fila = $res->fetch_assoc()){
                $fila = (object)$fila;
                $count++;

                $res2 = $this->conexion->link->query("SELECT id_cotizacion FROM orden_trabajo WHERE id = {$fila->id}");
                $id_cotizacion = 0;
                while($fila2 = $res2->fetch_assoc()){
                	$id_cotizacion = $fila2["id_cotizacion"];
                }

                $buttonStatus = [
	            	1 => '<a id="read" class="btn btn-sm blue btn-outline filter-submit margin-bottom" href="ordenesTrabajo?id='.$id_cotizacion.'&codigo='.$fila->codigo.'"><i class="fa fa-plus"></i> Ver</a>',
	            	2 => '<a id="read" class="btn btn-sm blue btn-outline filter-submit margin-bottom" href="ordenesTrabajo?id='.$id_cotizacion.'&codigo='.$fila->codigo.'"><i class="fa fa-plus"></i> Ver</a>',
	            	3 => '<a id="imprimir" class="btn btn-sm green btn-outline filter-submit margin-bottom" href="print_reporte.php?id='.$id_cotizacion.'" target="_blank" ><i class="fa fa-plus"></i> PDF</a>'
	            ];
                $datos->data[] = array (
                    $count,
                    $fila->fecha,
                    strtoupper($fila->codigo),
                    strtoupper($fila->codigo_equipo),
                    strtoupper($fila->tipo_trabajo),
                    strtoupper($fila->supervisor),
                    $labelStatus[$fila->statusValue],
                    $buttonStatus[$fila->statusValue],
                );

            }
            $datos->recordsTotal = $count;
            $datos->customActionStatus = "OK";
        }

        return json_encode($datos);
    }

    public function getFilters(){
        $postdata = (object)json_decode(file_get_contents("php://input"));

        $response = $this->setFilters();

        return json_encode($response);
    }

    private function setFilters(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $whereCliente = "";
        $whereDesc = "";
        $whereEquipo = "";
        if(isset($postdata->cliente) && $postdata->cliente != 0){
            $whereCliente = " AND id_cliente = '{$postdata->cliente}'";
        }
        if(isset($postdata->descripcion) && $postdata->descripcion != "" && $postdata->descripcion > 0){
            $whereDesc = " AND id IN (SELECT id_tipoequipo FROM cat_descripciones_equipos WHERE id = '{$postdata->descripcion}')";
        }
        if(isset($postdata->tEquipo) && $postdata->tEquipo != "" && $postdata->tEquipo > 0){
            $whereEquipo = " AND id_tipoequipo = {$postdata->tEquipo}";
        }

        $sql_tipo_clientes = "SELECT id ,nombre FROM cat_clientesTipo";
        $sql_clientes = "SELECT id ,nombre FROM cat_clientes WHERE status = 1 ORDER BY nombre";
        $sql_sucursales = "SELECT id , id_cliente ,nombre_contacto AS nombre FROM cat_sucursales WHERE status = 1 $whereCliente ORDER BY nombre_contacto";
        $sql_tipoEquipos = "SELECT id ,nombre FROM cat_equiposTipo WHERE status = 1 $whereDesc";
        $sql_descripcionEquipos = "SELECT id , id_tipoequipo ,nombre FROM cat_descripciones_equipos WHERE status = 1 $whereEquipo";
        $sql_tipoTrabajo = "SELECT id ,descripcion AS nombre  FROM cat_tipos_trabajo";
        $sql_marcas = "SELECT id ,nombre FROM cat_marcas WHERE status = 1 AND nombre != '' ORDER BY nombre";
        $sql_btu = "SELECT id , nombre FROM cat_capacidadBUTU WHERE status = 1";
        $sql_supervisores = "SELECT id, nombre FROM cat_supervisores WHERE status = 1 ORDER BY nombre";
        $response = new stdClass;
        $response->Tipoclientes = $this->OptionsSelect($sql_tipo_clientes);
        $response->clientes = $this->OptionsSelect($sql_clientes);
        $response->sucursales = $this->OptionsSelect($sql_sucursales);
        $response->tipoEquipos = $this->OptionsSelect($sql_tipoEquipos);
        $response->descripcionEquipos = $this->OptionsSelect($sql_descripcionEquipos);
        $response->tipoTrabajo = $this->OptionsSelect($sql_tipoTrabajo);
        $response->marcas = $this->OptionsSelect($sql_marcas);
        $response->btu = $this->OptionsSelect($sql_btu);
        $response->supervisores = $this->OptionsSelect($sql_supervisores);

        return $response;
    }

    private function OptionsSelect($sql){
        $res = $this->conexion->link->query($sql);
        $datos[] = ["id" => 0, "nombre" => "TODOS"];
        while($fila = $res->fetch_assoc()){
            $fila = (object)$fila;
            $datos[] = [
                "id" => $fila->id,
                "nombre" => $fila->nombre,
            ];
        }
        return $datos;
    }

    public function getClientesEquipos(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $wClientes = "";
        $wTipocliente = "";
        $wSucursal = "";
        $wEquipos = "";
        $wtrabajos = "";
        $sWhere = "";
        $postdata->sucursal = (int) $postdata->sucursal;
        if(isset($postdata->Tipocliente) && $postdata->Tipocliente != "" && $postdata->Tipocliente != 0){
            $wTipocliente .= " AND id = '{$postdata->Tipocliente}'";
        }
        if(isset($postdata->tEquipo) && $postdata->tEquipo != "" && $postdata->tEquipo != 0){
            $sWhere .= " AND cat_equipos.id_tipo_equipo = '{$postdata->tEquipo}'";
        }
        if(isset($postdata->cliente) && $postdata->cliente != "" && $postdata->cliente > 0){
            $wClientes .= " AND cat_clientes.id = '{$postdata->cliente}'";
        }
        if(isset($filters->tTrabajo) && $filters->tTrabajo != "" && $filters->tTrabajo > 0){
            $sql = "SELECT descripcion FROM cat_tipos_trabajo WHERE id = '{$filters->tTrabajo}'";
            $trabajo = $this->conexion->queryRow($sql)->descripcion;
            $wtrabajos = " AND orden_trabajo.tipo_trabajo LIKE '%{$trabajo}%'";     
        }
        if(isset($postdata->sucursal) && $postdata->sucursal != "" && $postdata->sucursal > 0){
            $wEquipos .= " AND cat_equipos.id_sucursal = '{$postdata->sucursal}'";
        }
        if(isset($postdata->tEquipo) && $postdata->tEquipo != "" && $postdata->tEquipo != 0){
            $wEquipos .= " AND cat_equipos.id_tipo_equipo = '{$postdata->tEquipo}'";
        }
        if(isset($postdata->descripcion) && $postdata->descripcion != "" && $postdata->descripcion != 0){
            $wEquipos .= " AND cat_equipos.id_descripcion_equipo = '{$postdata->descripcion}'";
        }
        if(isset($postdata->sucursal) && $postdata->sucursal != "" && $postdata->sucursal > 0){
            $wSucursal .= " AND cat_sucursales.id = '{$postdata->sucursal}'";
        }

        $sql_main = "SELECT id , nombre , SUM(equipos) AS total FROM (
            (SELECT id_tipcli AS id,  (SELECT nombre FROM cat_clientesTipo WHERE id = cat_clientes.id_tipcli) AS nombre, (SELECT nombre FROM cat_clientesTipo WHERE id = cat_clientes.id_tipcli) AS tipoCliente,
                    IFNULL((SELECT COUNT(*) 
                        FROM cotizaciones 
                        INNER JOIN orden_trabajo ON id_cotizacion = cotizaciones.id 
                        LEFT JOIN cat_equipos ON cat_equipos.id_cliente = cotizaciones.id_cliente
                        WHERE cotizaciones.id_cliente = cat_clientes.id $wEquipos GROUP BY cotizaciones.id_cliente), 0) AS total,
                    IFNULL((SELECT COUNT(*) FROM cat_equipos WHERE id_cliente = cat_clientes.id $wEquipos GROUP BY cat_equipos.id_cliente), 0) AS equipos
                    FROM cat_clientes
                    WHERE cat_clientes.status = 1 $wClientes $wtrabajos
                    HAVING total > 0
                    ORDER BY (SELECT nombre FROM cat_clientesTipo WHERE id = cat_clientes.id_tipcli))) AS detail
            WHERE 1 = 1 {$wTipocliente}
            GROUP BY nombre";
        
        $res_main = $this->conexion->link->query($sql_main);
        $datos = array();
        while($fila_main = $res_main->fetch_assoc()){
            $fila_main = (object)$fila_main;
            $sql = "SELECT cat_clientes.id, UPPER(cat_clientes.nombre) AS nombre, 
                        IFNULL((SELECT COUNT(*) FROM cat_equipos WHERE id_cliente = cat_clientes.id $wEquipos GROUP BY cat_equipos.id_cliente), 0) AS total
                FROM cat_clientes
                WHERE cat_clientes.status = 1 AND cat_clientes.id_tipcli = {$fila_main->id} $wClientes
                ORDER BY cat_clientes.nombre";
            $res = $this->conexion->link->query($sql);

            while($fila = $res->fetch_assoc()){
                $fila = (object) $fila;
                $fila->sucursales = $this->queryAll("SELECT id, UPPER(nombre_contacto) AS nombre_contacto,
                        (SELECT COUNT(*) FROM cat_equipos WHERE id_sucursal = cat_sucursales.id AND id_cliente = {$fila->id} $wEquipos) AS total_equipos 
                        FROM cat_sucursales 
                        WHERE id_cliente = ".$fila->id." AND status = 1 $wSucursal");
                if(count($fila->sucursales) <= 0){
                    $fila->sucursales[] = [
                        "nombre_contacto" => $fila->nombre,
                        "total_equipos" => (double)$fila->total,
                        "not_sucursal" => 1
                    ];
                }

                $fila_main->clientes[] = $fila;
            }
            $datos[] = $fila_main;
        }
        return json_encode($datos);
    }

    public function getEquiposTipos(){
        $postdata = (object)json_decode(file_get_contents("php://input"));

        $sWhere = "";
        $sucursal = "";
        if(isset($postdata->tEquipo) && $postdata->tEquipo != ""){
            $sWhere .= " AND cat_equipos.id_tipo_equipo = '{$postdata->tEquipo}'";
        }

        if((int)($postdata->sucursal) > 0){
            $sWhere .= " AND cat_equipos.id_sucursal = ".$postdata->sucursal."";
            $sucursal .= " AND cotizaciones.id_sucursal = ".$postdata->sucursal."";
        }

        /*if($postdata->fecha_inicio != "" && $postdata->fecha_fin != ""){
            $sWhere .= " AND codigos.fecha_fin BETWEEN '{$postdata->fecha_inicio}' AND '{$postdata->fecha_fin}'";
        }*/

        $sql = "SELECT 
                    cat_equiposTipo.id,
                    cat_equiposTipo.`nombre`, 
                    (SELECT COUNT(*) FROM cat_equipos WHERE id_tipo_equipo = cat_equiposTipo.id) AS total
                FROM cat_equiposTipo
                HAVING total > 0";
        $response = array();
        $data = $this->queryAll($sql);
        foreach ($data as $tipo) {
            $sql = "SELECT cat_equipos.codigo, cat_descripciones_equipos.`nombre`, capacidad, data_piezas, COUNT(1) AS existencia
                    FROM 
                    (
                        (
                            SELECT orden_trabajo.*, cotizaciones.id_cliente, man_caracteristicas_climatizacion_a.codigo AS codigo_e, cotizaciones.id_sucursal FROM man_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE man_caracteristicas_climatizacion_a.status = 1 AND cotizaciones.id_cliente = ".$postdata->cliente." $sucursal)
                            UNION 
                            (SELECT orden_trabajo.*, cotizaciones.id_cliente, man_caracteristicas_climatizacion_b.codigo AS codigo_e, cotizaciones.id_sucursal FROM man_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE man_caracteristicas_climatizacion_b.status = 1 AND cotizaciones.id_cliente = ".$postdata->cliente." $sucursal)
                            UNION
                            (SELECT orden_trabajo.*, cotizaciones.id_cliente, man_caracteristicas_refrigeracion.codigo AS codigo_e, cotizaciones.id_sucursal FROM man_caracteristicas_refrigeracion INNER JOIN cat_areas ON id_area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON id_equipo_des = cat_descripciones_equipos.`id` WHERE man_caracteristicas_refrigeracion.status = 1 AND cotizaciones.id_cliente = ".$postdata->cliente." $sucursal)
                            UNION
                            (SELECT orden_trabajo.*, cotizaciones.id_cliente, man_caracteristicas_ventilacion.codigo AS codigo_e, cotizaciones.id_sucursal FROM man_caracteristicas_ventilacion INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE man_caracteristicas_ventilacion.status = 1 AND cotizaciones.id_cliente = ".$postdata->cliente." $sucursal)
                            UNION
                            (SELECT orden_trabajo.*, cotizaciones.id_cliente, ins_caracteristicas_climatizacion_a.codigo AS codigo_e, cotizaciones.id_sucursal FROM ins_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE ins_caracteristicas_climatizacion_a.status = 1 AND cotizaciones.id_cliente = ".$postdata->cliente." $sucursal) 
                            UNION 
                            (SELECT orden_trabajo.*, cotizaciones.id_cliente, ins_caracteristicas_climatizacion_b.codigo AS codigo_e, cotizaciones.id_sucursal FROM ins_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE ins_caracteristicas_climatizacion_b.status = 1 AND cotizaciones.id_cliente = ".$postdata->cliente." $sucursal)
                            UNION
                            (SELECT orden_trabajo.*, cotizaciones.id_cliente, ins_caracteristicas_refrigeracion.codigo AS codigo_e, cotizaciones.id_sucursal FROM ins_caracteristicas_refrigeracion INNER JOIN cat_areas ON id_area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON id_equipo_des = cat_descripciones_equipos.`id` WHERE ins_caracteristicas_refrigeracion.status = 1 AND cotizaciones.id_cliente = ".$postdata->cliente." $sucursal)
                            UNION
                            (SELECT orden_trabajo.*, cotizaciones.id_cliente, ins_caracteristicas_ventilacion.codigo AS codigo_e, cotizaciones.id_sucursal FROM ins_caracteristicas_ventilacion INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE ins_caracteristicas_ventilacion.status = 1 AND cotizaciones.id_cliente = ".$postdata->cliente." $sucursal)
                            UNION
                            (SELECT orden_trabajo.*, cotizaciones.id_cliente, cor_caracteristicas_climatizacion_a.codigo AS codigo_e, cotizaciones.id_sucursal FROM cor_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE cor_caracteristicas_climatizacion_a.status = 1 AND cotizaciones.id_cliente = ".$postdata->cliente." $sucursal)
                            UNION 
                            (SELECT orden_trabajo.*, cotizaciones.id_cliente, cor_caracteristicas_climatizacion_b.codigo AS codigo_e, cotizaciones.id_sucursal FROM cor_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE cor_caracteristicas_climatizacion_b.status = 1 AND cotizaciones.id_cliente = ".$postdata->cliente." $sucursal)
                            UNION
                            (SELECT orden_trabajo.*, cotizaciones.id_cliente, cor_caracteristicas_refrigeracion.codigo AS codigo_e, cotizaciones.id_sucursal FROM cor_caracteristicas_refrigeracion INNER JOIN cat_areas ON id_area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON id_equipo_des = cat_descripciones_equipos.`id` WHERE cor_caracteristicas_refrigeracion.status = 1 AND cotizaciones.id_cliente = ".$postdata->cliente." $sucursal)
                            UNION
                            (SELECT orden_trabajo.*, cotizaciones.id_cliente, cor_caracteristicas_ventilacion.codigo AS codigo_e, cotizaciones.id_sucursal FROM cor_caracteristicas_ventilacion INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE cor_caracteristicas_ventilacion.status = 1 AND cotizaciones.id_cliente = ".$postdata->cliente." $sucursal)
                        ) AS codigos
                    LEFT JOIN cat_equipos ON cat_equipos.codigo = codigos.codigo_e
                    INNER JOIN cat_clientes ON cat_equipos.`id_cliente` = cat_clientes.`id`
                    INNER JOIN cat_equiposTipo ON cat_equipos.id_tipo_equipo = cat_equiposTipo.`id`
                    INNER JOIN cat_descripciones_equipos ON cat_equipos.`id_descripcion_equipo` = cat_descripciones_equipos.id
                    WHERE cat_equipos.id_cliente = ".$postdata->cliente." AND cat_equipos.id_tipo_equipo = ".$tipo->id." $sWhere
                    GROUP BY codigo";
            //echo $sql;
            $tipo->equipos = $this->queryAll($sql);

            $equipos = array();

            foreach($tipo->equipos as $equipo){
                $equipo->data_piezas = json_decode($equipo->data_piezas, true);
                if(count($equipo->data_piezas)>0)
                    $equipo->marca = $equipo->data_piezas[0]["marca"];
                else
                    $equipo->marca = "S/N";

                unset($equipo->data_piezas);
                $equipos[] = $equipo;
            }
            if(count($equipos) > 0){
                $tipo->equipos = $equipos;
                $response[] = $tipo;
            }
        }
        return json_encode($response);
    }

    private function queryAll($sql){
        $datos = array();
        $res = $this->conexion->link->query($sql);
        while($fila = $res->fetch_assoc()){
            $fila = (object)$fila;
            $datos[] = $fila;
        }
        return $datos;
    }

    private function getEquipos($id_equipo){
        $sql = "
        SELECT * FROM (
                    (SELECT orden_trabajo.* FROM man_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE man_caracteristicas_climatizacion_a.status = 1) 
                     UNION 
                     (SELECT orden_trabajo.* FROM man_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE man_caracteristicas_climatizacion_b.status = 1)
                     UNION
                     (SELECT orden_trabajo.* FROM man_caracteristicas_refrigeracion INNER JOIN cat_areas ON id_area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON id_equipo_des = cat_descripciones_equipos.`id` WHERE man_caracteristicas_refrigeracion.status = 1)
                     UNION
                     (SELECT orden_trabajo.* FROM man_caracteristicas_ventilacion INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE man_caracteristicas_ventilacion.status = 1)
                     #instalacion
                     UNION
                     (SELECT orden_trabajo.* FROM ins_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE ins_caracteristicas_climatizacion_a.status = 1) 
                     UNION 
                     (SELECT orden_trabajo.* FROM ins_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE ins_caracteristicas_climatizacion_b.status = 1)
                     UNION
                     (SELECT orden_trabajo.*  FROM ins_caracteristicas_refrigeracion INNER JOIN cat_areas ON id_area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON id_equipo_des = cat_descripciones_equipos.`id` WHERE ins_caracteristicas_refrigeracion.status = 1)
                     UNION
                     (SELECT orden_trabajo.* FROM ins_caracteristicas_ventilacion INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE ins_caracteristicas_ventilacion.status = 1)
                     #correctivo
                     UNION
                     (SELECT orden_trabajo.* FROM cor_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE cor_caracteristicas_climatizacion_a.status = 1) 
                     UNION 
                     (SELECT orden_trabajo.* FROM cor_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE cor_caracteristicas_climatizacion_b.status = 1)
                     UNION
                     (SELECT orden_trabajo.* FROM cor_caracteristicas_refrigeracion INNER JOIN cat_areas ON id_area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON id_equipo_des = cat_descripciones_equipos.`id` WHERE cor_caracteristicas_refrigeracion.status = 1)
                     UNION
                     (SELECT orden_trabajo.* FROM cor_caracteristicas_ventilacion INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE cor_caracteristicas_ventilacion.status = 1)
                    (SELECT orden_trabajo.* FROM man_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE man_caracteristicas_climatizacion_a.status = 1) 
                     UNION 
                     (SELECT orden_trabajo.* FROM man_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE man_caracteristicas_climatizacion_b.status = 1)
                     UNION
                     (SELECT orden_trabajo.* FROM man_caracteristicas_refrigeracion INNER JOIN cat_areas ON id_area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON id_equipo_des = cat_descripciones_equipos.`id` WHERE man_caracteristicas_refrigeracion.status = 1)
                     UNION
                     (SELECT orden_trabajo.* FROM man_caracteristicas_ventilacion INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE man_caracteristicas_ventilacion.status = 1)
                     #instalacion
                     UNION
                     (SELECT orden_trabajo.* FROM ins_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE ins_caracteristicas_climatizacion_a.status = 1) 
                     UNION 
                     (SELECT orden_trabajo.* FROM ins_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE ins_caracteristicas_climatizacion_b.status = 1)
                     UNION
                     (SELECT orden_trabajo.*  FROM ins_caracteristicas_refrigeracion INNER JOIN cat_areas ON id_area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON id_equipo_des = cat_descripciones_equipos.`id` WHERE ins_caracteristicas_refrigeracion.status = 1)
                     UNION
                     (SELECT orden_trabajo.* FROM ins_caracteristicas_ventilacion INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE ins_caracteristicas_ventilacion.status = 1)
                     #correctivo
                     UNION
                     (SELECT orden_trabajo.* FROM cor_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE cor_caracteristicas_climatizacion_a.status = 1) 
                     UNION 
                     (SELECT orden_trabajo.* FROM cor_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE cor_caracteristicas_climatizacion_b.status = 1)
                     UNION
                     (SELECT orden_trabajo.* FROM cor_caracteristicas_refrigeracion INNER JOIN cat_areas ON id_area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON id_equipo_des = cat_descripciones_equipos.`id` WHERE cor_caracteristicas_refrigeracion.status = 1)
                     UNION
                     (SELECT orden_trabajo.* FROM cor_caracteristicas_ventilacion INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE cor_caracteristicas_ventilacion.status = 1)
            ) AS equipos
            HAVING equipos.id = 
         ";

    }
    public function updateReporte(){
         $postdata = (object)$_POST;

        $sql = html_entity_decode($postdata->querys);
        $this->conexion->query($sql);
        echo 1;
    }
}