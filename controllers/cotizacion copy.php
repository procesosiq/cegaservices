<?php

/**
* 
*/
class cotizacion
{
	
	private $conexion;
    private $session;
    
    public function __construct(){
        $this->conexion = new M_Conexion;
        $this->session = Session::getInstance();
    }

    public function index(){
    	$response = (object)[];
    	$response->tipo_cliente = $this->getTypeClient();
    	$response->tipo_trabajo = $this->getTypeTrabajo();

    	return json_encode($response);
    }

    public function ListCotizacion(){
        extract($_POST);
        
        $sWhere = "";
        $sOrder = " ORDER BY id";
        $DesAsc = "ASC";
        $sOrder .= " {$DesAsc}";
        $sLimit = "";
        $response = (object)[];
        if(isset($_POST)){

            /*----------  ORDER BY ----------*/
            
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY id {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY fecha_status {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY id_cliente {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY tipo_trabajo {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 5){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY total {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 6){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY status {$DesAsc}";
            }
            /*----------  ORDER BY ----------*/

            if(isset($_POST['order_id']) && trim($_POST['order_id']) != ""){
                $sWhere .= " AND id = ".$_POST["order_id"];
            }               
            if((isset($_POST['order_date_from']) && trim($_POST['order_date_from']) != "") && (isset($_POST['order_date_to']) && trim($_POST['order_date_to']) != "")){
                $sWhere .= " AND fecha_status BETWEEN '".$_POST["order_date_from"]."' AND '".$_POST["order_date_to"]."'";
            }
            if(isset($_POST['order_customer_name']) && trim($_POST['order_customer_name']) != ""){
                $sWhere .= " AND (SELECT nombre FROM cat_clientes WHERE id = id_cliente) AS cliente LIKE '%".$_POST['order_customer_name']."%'";
            }
            if(isset($_POST['order_t_work']) && trim($_POST['order_t_work']) != ""){
                // $sWhere .= " AND total BETWEEN '".$_POST["order_t_work"]."' AND '".$_POST["order_t_work"]."'";
            }
            if((isset($_POST['order_price_to']) && trim($_POST['order_price_to']) != "") && (isset($_POST['order_price_from']) && trim($_POST['order_price_from']) != "")){
                $sWhere .= " AND total BETWEEN '".$_POST["order_price_to"]."' AND '".$_POST["order_price_from"]."'";
            }
            if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
                $sWhere .= " AND status = ".$_POST['order_status'];
            }
            // if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
            //     $sWhere .= " AND cat_equipos.`status` = ".$_POST["order_status"];
            // }

            /*----------  LIMIT  ----------*/
            if(isset($_POST['length']) && $_POST['length'] > 0){
                $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
            }
        }

        $sql = "SELECT * , '' AS tipo_trabajo,
            (SELECT nombre FROM cat_clientes WHERE id = id_cliente) AS cliente,
            (SELECT COUNT(*) FROM cat_clientes WHERE status <= 2 ) AS totalRows
            FROM cotizaciones WHERE id_usuario = '{$this->session->logged}' 
        $sWhere $sOrder $sLimit";
        // print_r($sql);
        $labelStatus = [
            0 =>  '<button class="btn btn-sm bg-red-thunderbird bg-font-red-thunderbird" id="status">Pendiente</button>',
            1 =>  '<button class="btn btn-sm bg-yellow-gold bg-font-yellow-gold" id="status">Por Aprobar</button>',
            2 =>  '<button class="btn btn-sm bg-green-jungle bg-font-green-jungle" id="status">Aprobado</button>',
        ];
        $response->recordsTotal = 0;
        $response->data = [];
        $res = $this->conexion->link->query($sql);
        while($fila = $res->fetch_assoc()){
            $fila = (object)$fila;

            $response->data[] = array (
                $fila->id,
                $fila->id,
                $fila->fecha_status,
                $fila->cliente,
                $this->getDetailsCotizacion($fila->id),
                '$ ' . number_format($fila->total, 2, '.', ','),
                $labelStatus[$fila->status],
                '<button id="edit" class="btn btn-sm green btn-outline filter-submit margin-bottom"><i class="fa fa-seach"></i> Ver</button>'
            );

            $response->recordsTotal = $fila->totalRows;
        }

        $response->recordsFiltered = count($response->data);
        #response->customActionMessage = "Informacion completada con exito";
        $response->customActionStatus = "OK";

        return json_encode($response);
    }

    private function getDetailsCotizacion($id = 0){
        $span = "<div style='width: 300px;'>";
        if($id > 0){
            $sql = "SELECT (SELECT descripcion FROM cat_tipos_trabajo WHERE id = tipo_trabajo) AS label , 
                (SELECT clase FROM cat_tipos_trabajo WHERE id = tipo_trabajo) AS class
            FROM cotizaciones_detalle
            WHERE id_cotizacion = {$id} 
            GROUP BY cotizaciones_detalle.id";
            $res = $this->conexion->link->query($sql);
            $count = 0;
            while($fila = $res->fetch_assoc()){
                $fila = (object)$fila;
                if($count%2 == 0) $span .= "<div class='row' style='padding: .5em;'>";
                $span .= '<div class="col-md-6"><span class="form-control label '.$fila->class.'">'.$fila->label.'</span></div>';
                if($count%2 != 0) $span .= "</div>";
                $count++;
            }
        }
        $span .= '</div>';
        return $span;
    }

    public function getTypeClient(){
    	$datos = [];
    	$sql = 'SELECT * FROM cat_clientesTipo';
    	$res = $this->conexion->link->query($sql);
        while($fila = $res->fetch_assoc()){
             $fila = (object)$fila;
             $datos[] = [
             	"id" => $fila->id,
             	"nombre" => $fila->nombre
             ];
        }

      	return ($datos);
    }

    public function getTypeTrabajo(){
      $datos = [];
      $sql = 'SELECT * FROM cat_tipos_trabajo';
      $res = $this->conexion->link->query($sql);
        while($fila = $res->fetch_assoc()){
             $fila = (object)$fila;
             $datos[] = [
              "id" => $fila->id,
              "name" => $fila->descripcion,
              "class" => $fila->clase
             ];
        }

        return ($datos);
    }

    public function save(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        // print_r($postdata);
        $response = (object)[];
        if($postdata->params->total > 0 && $postdata->id_client > 0){
            $postdata->observaciones = addslashes($postdata->observaciones);
             $sql = "INSERT INTO cotizaciones SET
                    id_cliente = '{$postdata->id_client}',
                    forma_pago = '{$postdata->params->forma_pago}',
                    observaciones = '{$postdata->observaciones}',
                    subtotal = '{$postdata->params->subtotal}',
                    iva = '{$postdata->params->iva}',
                    descuento = '{$postdata->params->_discount}',
                    total = '{$postdata->params->total}',
                    id_usuario = '{$this->session->logged}',
                    fecha_status = CURRENT_DATE,
                    fecha_create = CURRENT_TIMESTAMP";
            // print $sql;
            $id = $this->conexion->Consultas(1,$sql);
            if((int)$id > 0){
                $folio = str_pad($id , 10 ,"0" , STR_PAD_LEFT);
                $response->id = $id;
                $response->folio = $folio;
                $response->fecha = date("F j, Y \a\t g:ia");;
                $this->conexion->link->query("UPDATE cotizaciones SET folio = '".str_pad($id , 10 ,"0" , STR_PAD_LEFT)."' WHERE id = $id");
                foreach ($postdata->details as $key => $value) {
                   $value->des_trabajo = trim($value->des_trabajo);
                   $sql_detalle = "INSERT INTO cotizaciones_detalle SET
                    id_cotizacion = '{$id}',
                    tipo_trabajo = '{$value->tipo_trabajo}',
                    des_trabajo = '{$value->des}',
                    cantidad = '{$value->count}',
                    precio = '{$value->price}',
                    total = ".($value->count * $value->price);
                    $this->conexion->Consultas(1,$sql_detalle);
                    // print $sql_detalle;
                }
            }

            return json_encode($response);
        }
    }

    public function saveCliente(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        // print_r($postdata);
        if($postdata->ruc != ""){
            $sql = "INSERT INTO cat_clientes SET
                    nombre = '{$postdata->nombre_cliente}',
                    email = '{$postdata->email}',
                    id_tipcli = '{$postdata->tipo_cliente}',
                    razon_social = '{$postdata->razon_social}',
                    ruc = '{$postdata->ruc}',
                    telefono = '{$postdata->telefono}',
                    id_usuario = '{$this->session->logged}',
                    fecha = CURRENT_DATE";
            // print $sql;
            $ids = $this->conexion->Consultas(1,$sql);
            if((int)$ids > 0){
                return ($this->GetClienteID($ids));
            }
        }

        return ["Error" => "400"];
    }

    private function GetClienteID($id){
        // print_r($postdata);
        if(isset($id)){
            $datos = (object)[];
            $sql = "SELECT *
            FROM cat_clientes 
            WHERE id='$id'";
            $res = $this->conexion->link->query($sql);
            if($fila = $res->fetch_assoc()){
                 $datos = (object)$fila;
            }
           
            return json_encode($datos);
        }
    }

	public function GetCliente(){
		$postdata = (object)json_decode(file_get_contents("php://input"));
		// print_r($postdata);
		if(isset($postdata->ruc)){
			$datos = (object)[];
	        $sql = "SELECT *
	        FROM cat_clientes 
	        WHERE ruc='$postdata->ruc'";
	        $res = $this->conexion->link->query($sql);
	        if($fila = $res->fetch_assoc()){
	             $datos = (object)$fila;
	        }
	       
	        return json_encode($datos);
		}
    }

    public function changeStatus(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(!isset($postdata->id)) $postdata = (object)$_POST;
        if($postdata->id > 0){
            $sql = "UPDATE cotizaciones SET status = status+1 WHERE id = '{$postdata->id}'";
            $this->conexion->link->query($sql);
            $sql = "SELECT * FROM cotizaciones WHERE id = '{$postdata->id}'";
            $res = $this->conexion->link->query($sql);
            if($fila = $res->fetch_assoc()){
                 $datos = (object)$fila;
                 if($fila->status >= 2){
                    $sql_orden=" INSERT INTO orden_trabajo
                                 SELECT null ,cotizaciones.id ,id_cliente , '' ,'','' , fecha_status , '' , cotizaciones.observaciones , 0
                                 FROM cotizaciones ,orden_trabajo
                                 WHERE cotizaciones.id = {$postdata->id} AND orden_trabajo.id_cotizacion != cotizaciones.id";
                    $this->conexion->link->query($sql_orden);
                 }
            }
        }
        return 1;
    }
}


?>