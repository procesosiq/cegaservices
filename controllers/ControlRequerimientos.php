<?php
class ControlRequerimientos {

	private $conexion;
    private $session;

    public function __construct(){
        $this->conexion = new M_Conexion;
        $this->session = Session::getInstance();
    }

    public function insert(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        $sWhere = "";
        $receptor = "";
        $editor = "";

        if($params->id_requerimiento > 0){
            $editor = " ultimo_edito = '{$this->session->nombre}',";
            $sWhere = " WHERE id = '{$params->id_requerimiento}'";
        } else {
            $receptor = " receptor = '{$this->session->nombre}',";
        }

        $sql = "SELECT nombre FROM cat_clientes WHERE id = '{$params->cliente}'";
        $nombre_cliente = $this->conexion->queryRow($sql)->nombre;

        if($params->sucursal == 0){
            $nombre_sucursal = 'N/A';
        } else {
            $sql = "SELECT nombre_contacto FROM cat_sucursales WHERE id = '{$params->sucursal}'";
            $nombre_sucursal = $this->conexion->queryRow($sql)->nombre_contacto;
        }

        if($params->personal == 0){
            $nombre_personal = 'N/A';
        } else {
            $sql = "SELECT nombre FROM cat_supervisores WHERE id = '{$params->personal}'";
            $nombre_personal = $this->conexion->queryRow($sql)->nombre;
        }

        foreach($params->auxiliar as $row){
            if($row == 0){
                $nombre_auxiliares = 'N/A';
            } else {
                $sql = "SELECT nombre FROM cat_auxiliares WHERE id = '{$row}'";
                $nombre_auxiliares .= $this->conexion->queryRow($sql)->nombre.", ";
            }
        }

        foreach($params->auxiliar as $row){
            $auxiliar .= $row.", ";
        }

        $sql = "INSERT INTO control_requerimientos
                SET 
                id_cliente = '{$params->cliente}',
                cliente = UPPER('{$nombre_cliente}'),
                id_sucursal = '{$params->sucursal}',
                sucursal = UPPER('{$nombre_sucursal}'),
                niv_prioridad = '{$params->niv_prioridad}',
                fecha_recepcion = '{$params->fecha_recepcion}',
                $receptor
                $editor
                fecha_confirmacion = '{$params->fecha_confirmacion}',
                tipo_trabajo = UPPER('{$params->tipo_trabajo}'),
                semana = '{$params->semana}',
                fecha_programada = '{$params->fecha_programada}',
                hora = '{$params->hora}',
                zona = UPPER('{$params->zona}'),
                mes = '{$params->mes}',
                id_personal = '{$params->personal}',
                personal = UPPER('{$params->nombre_personal}'),
                id_auxiliar = '{$auxiliar}',
                auxiliares = UPPER('{$nombre_auxiliares}'),
                estado = UPPER('{$params->estado}')
                $sWhere";
        if($params->id_requerimiento > 0){
            $sql = str_replace("INSERT INTO", "UPDATE", $sql);
        }
        $this->conexion->query($sql);
        $response->status = 200;
        return json_encode($response);
    }

    public function update(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;

        $sql = "UPDATE control_requerimientos
                SET 
                estado = UPPER('{$params->estado}')
                WHERE id = '{$params->id}'";
        $this->conexion->query($sql);
        $response->status = 200;
        return json_encode($response);
    }

    public function select(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        
        $sWhere = ""; 

        $sql = "SELECT 
                    control_requerimientos.id,
                    control_requerimientos.id_cliente,
                    UPPER(control_requerimientos.cliente) AS cliente,
                    UPPER(control_requerimientos.sucursal) AS sucursal,
                    control_requerimientos.niv_prioridad,
                    IF(control_requerimientos.fecha_recepcion = '0000-00-00', '', control_requerimientos.fecha_recepcion) AS fecha_recepcion,
                    control_requerimientos.receptor,
                    IF( control_requerimientos.fecha_confirmacion = '0000-00-00', '',  control_requerimientos.fecha_confirmacion) AS fecha_confirmacion,
                    cat_tipos_trabajo.descripcion AS tipo_trabajo,
                    cat_tipos_trabajo.clase AS color_trabajo,
                    control_requerimientos.semana,
                    IF(control_requerimientos.fecha_programada = '0000-00-00', '', control_requerimientos.fecha_programada) AS fecha_programada,
                    IF(control_requerimientos.hora = '00:00:00', '', control_requerimientos.hora) AS hora,
                    control_requerimientos.zona,
                    IF(control_requerimientos.mes != '',
                    CASE control_requerimientos.mes + 1
                        WHEN 1 THEN 'ENERO'
                        WHEN 2 THEN 'FEBRERO'
                        WHEN 3 THEN 'MARZO'
                        WHEN 4 THEN 'ABRIL'
                        WHEN 5 THEN 'MAYO'
                        WHEN 6 THEN 'JUNIO'
                        WHEN 7 THEN 'JULIO'
                        WHEN 8 THEN 'AGOSTO'
                        WHEN 9 THEN 'SEPTIEMBRE'
                        WHEN 10 THEN 'OCTUBRE'
                        WHEN 11 THEN 'NOVIEMBRE'
                        WHEN 12 THEN 'DICIEMBRE'
                    END, '') AS mes,
                    cat_supervisores.nombre AS personal,
                    cat_supervisores.color AS color_personal,
                    IF(control_requerimientos.id_auxiliar = 0 , 'N/A', IF(control_requerimientos.auxiliares IS NOT NULL , control_requerimientos.auxiliares , cat_auxiliares.nombre)) AS auxiliar,
                    cat_auxiliares.color AS color_auxiliar,
                    control_requerimientos.estado
                FROM control_requerimientos
                LEFT JOIN cat_tipos_trabajo ON cat_tipos_trabajo.id = control_requerimientos.tipo_trabajo
                LEFT JOIN cat_supervisores ON cat_supervisores.id = control_requerimientos.id_personal
                LEFT JOIN cat_auxiliares ON cat_auxiliares.id = control_requerimientos.id_auxiliar
                WHERE control_requerimientos.status = 'Activo' ORDER BY control_requerimientos.fecha_recepcion DESC";
        $response->data = $this->conexion->queryAll($sql);
        foreach($response->data as $row){   
            $sql = "SELECT color FROM cat_estados_req WHERE nombre = '{$row->estado}'";
            $row->color_estado = $this->conexion->queryRow($sql)->color;
        }
        return json_encode($response);
    }

    public function clientes(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        
        $sWhere = ""; 

        $sql = "SELECT 
                    id, 
                    UPPER(nombre) AS label 
                FROM cat_clientes 
                WHERE STATUS = 1";
        $response->clientes = $this->conexion->queryAll($sql);
        return json_encode($response);
    }

    public function sucursales(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;

        $sql = "SELECT 
                    id, 
                    UPPER(nombre_contacto) AS label 
                FROM cat_sucursales
                WHERE STATUS = 1 AND id_cliente = '{$params->cliente}'";
        $response->sucursales = $this->conexion->queryAll($sql);
        if(!$response->sucursales){
            $response->sucursales[] = ["id" => "0", "label" => "N/A"];
        }
        return json_encode($response);
    }

    public function tiposTrabajos(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        
        $sWhere = ""; 

        $sql = "SELECT id, descripcion AS label, clase AS color FROM cat_tipos_trabajo WHERE status = 'Activo'";
        $response->tipos_trabajos = $this->conexion->queryAll($sql);
        return json_encode($response);
    }

    public function zonas(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        
        $sWhere = ""; 

        $sql = "SELECT nombre AS id, nombre AS label FROM cat_zonas WHERE status = 'Activo'";
        $response->zonas = $this->conexion->queryAll($sql);
        return json_encode($response);
    }

    public function estados(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        
        $sWhere = ""; 

        $sql = "SELECT id, nombre AS label, color, status FROM cat_estados_req WHERE status != 'Eliminado'";
        $response->estados = $this->conexion->queryAll($sql);
        return json_encode($response);
    }

    public function personal(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        
        $sWhere = ""; 

        $sql = "SELECT id, nombre AS label, color FROM cat_supervisores WHERE STATUS = 1 GROUP BY nombre";
        $response->personal = $this->conexion->queryAll($sql);
        return json_encode($response);
    }

    public function auxiliar(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        
        $sWhere = ""; 

        $sql = "SELECT id, nombre AS label, color FROM cat_auxiliares WHERE STATUS = 1 GROUP BY nombre";
        $response->auxiliar = $this->conexion->queryAll($sql);
        return json_encode($response);
    }

    public function selectObservaciones(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        $sWhere = "";

        $sql = "SELECT 
                    fecha, 
                    hora, 
                    UPPER(observaciones) AS observaciones, 
                    CONCAT(fecha,' ',hora) AS timestamp, 
                    UPPER(usuario) AS usuario 
                FROM control_requerimientos_observaciones WHERE id_cliente = {$params->id_cliente} AND id_requerimiento = '{$params->id}'";
        $response->observaciones = $this->conexion->queryAll($sql);
        return json_encode($response);
    }

    public function insertObservaciones(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        $sWhere = "";

        $sql = "SELECT nombre FROM cat_clientes WHERE id = '{$params->cliente}'";
        $nombre_cliente = $this->conexion->queryRow($sql)->nombre;
        
        $sql = "INSERT INTO control_requerimientos_observaciones
                SET
                id_requerimiento = '{$params->id}',
                id_cliente = '{$params->id_cliente}',
                cliente = UPPER('{$nombre_cliente}'),
                fecha = '{$params->fecha_observacion}',
                hora = '{$params->hora_observaciones}',
                observaciones = UPPER('{$params->observacion}'),
                usuario = UPPER('{$this->session->nombre}')";
        $this->conexion->query($sql);
        $response->status = 200;
        return json_encode($response);
    }

    public function show(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        
        $sql = "SELECT
                id AS id_requerimiento,
                id_cliente AS cliente,
                niv_prioridad,
                fecha_recepcion,
                fecha_confirmacion,
                tipo_trabajo,
                semana,
                fecha_programada,
                hora,
                zona,
                mes,
                id_personal AS personal,
                id_auxiliar AS auxiliar,
                estado
                FROM control_requerimientos WHERE status = 'Activo' AND id = '{$params->id_requerimiento}'";
        $response->data = $this->conexion->queryRow($sql);
        return json_encode($response);
    }
}