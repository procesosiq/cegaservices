<?php
class ConfigEstadoReq {

	private $conexion;
    private $session;

    public function __construct(){
        $this->conexion = new M_Conexion;
        $this->session = Session::getInstance();
    }

    public function insert(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        $sWhere = "";

        if($params->id_estado > 0){
            $sWhere = " WHERE id = '{$params->id_estado}'";
        }

        $sql = "INSERT INTO cat_estados_req
                SET 
                nombre = '{$params->estado}',
                status = '{$params->status}',
                color = '{$params->color}'
                $sWhere";
        if($params->id_estado > 0){
            $sql = str_replace("INSERT INTO", "UPDATE", $sql);
        }
        $this->conexion->query($sql);
        $response->status = 200;
        return json_encode($response);
    }

    public function eliminar(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;

        $sql = "UPDATE cat_estados_req
                SET 
                status = 'Eliminado'
                WHERE id = '{$params->id_estado}'";
        $this->conexion->query($sql);
        $response->status = 200;
        return json_encode($response);
    }

    public function estados(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        
        $sWhere = ""; 

        $sql = "SELECT id, nombre, color, status FROM cat_estados_req WHERE status != 'Eliminado'";
        $response->estados = $this->conexion->queryAll($sql);
        return json_encode($response);
    }

    public function colores(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        
        $sWhere = ""; 

        $sql = "SELECT id, nombre AS label, codigo AS color, status FROM cat_colores WHERE status = 'Activo'";
        $response->colores = $this->conexion->queryAll($sql);
        return json_encode($response);
    }

    public function show(){
        $params = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        
        $sql = "SELECT
                id AS id_estado,
                nombre AS estado,
                color,
                status
                FROM cat_estados_req
                WHERE status != 'Eliminado' AND id = '{$params->id_estado}'";
        $response->data = $this->conexion->queryRow($sql);
        return json_encode($response);
    }

}