<?php

class equipos { 
     
    private $conexion;
    private $session;
    
    public function __construct(){
        $this->conexion = new M_Conexion;
        $this->session = Session::getInstance();
    }
    
    public function index(){
        
        $data = (object)$_REQUEST;
		
        $sWhere = "";
        $sOrder = " ORDER BY id";
        $DesAsc = "ASC";
        $sOrder .= " {$DesAsc}";
        $sLimit = "";
        // print_r($_POST);
        if(isset($_POST)){

                /*----------  ORDER BY ----------*/
                
                if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
                    $DesAsc = $_POST['order'][0]['dir'];
                    $sOrder = " ORDER BY cat_equipos.id {$DesAsc}";
                }
                if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
                    $DesAsc = $_POST['order'][0]['dir'];
                    $sOrder = " ORDER BY fecha {$DesAsc}";
                }
                if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
                    $DesAsc = $_POST['order'][0]['dir'];
                    $sOrder = " ORDER BY cat_equiposTipo.`nombre` {$DesAsc}";
                }
                if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4){
                    $DesAsc = $_POST['order'][0]['dir'];
                    $sOrder = " ORDER BY cat_descripciones_equipos.nombre {$DesAsc}";
                }
                if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 5){
                    $DesAsc = $_POST['order'][0]['dir'];
                    $sOrder = " ORDER BY cat_equipos.capacidad {$DesAsc}";
                }
                if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 6){
                    $DesAsc = $_POST['order'][0]['dir'];
                    $sOrder = " ORDER BY cat_areas.`nombre` {$DesAsc}";
                }
                if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 7){
                    $DesAsc = $_POST['order'][0]['dir'];
                    $sOrder = " ORDER BY cat_equipos.`codigo` {$DesAsc}";
                }
                if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 8){
                    $DesAsc = $_POST['order'][0]['dir'];
                    $sOrder = " ORDER BY cat_equipos.status {$DesAsc}";
                }
                /*----------  WHERE ----------*/ 

                if(isset($_POST['order_id']) && trim($_POST['order_id']) != ""){
                    $sWhere .= " AND cat_equipos.id = ".$_POST["order_id"];
                }               
                if((isset($_POST['order_date_from']) && trim($_POST['order_date_from']) != "") && (isset($_POST['order_date_to']) && trim($_POST['order_date_to']) != "")){
                    $sWhere .= " AND cat_equipos.fecha BETWEEN '".$_POST["order_date_from"]."' AND '".$_POST["order_date_to"]."'";
                }
                if(isset($_POST['search_t_equipo']) && trim($_POST['search_t_equipo']) != ""){
                    $sWhere .= " AND cat_equiposTipo.`nombre` LIKE '%".$_POST['search_t_equipo']."%'";
                }
                if(isset($_POST['search_descripcion_equipo']) && trim($_POST['search_descripcion_equipo']) != ""){
                    $sWhere .= " AND cat_descripciones_equipos.`nombre` LIKE '%".$_POST['search_descripcion_equipo']."%'";
                }
                if(isset($_POST['search_capacidad']) && trim($_POST['search_capacidad']) != ""){
                    $sWhere .= " AND cat_equipos.capacidad LIKE '%".$_POST['search_capacidad']."%'";
                }
                if(isset($_POST['search_a_climatiza']) && trim($_POST['search_a_climatiza']) != ""){
                    $sWhere .= " AND cat_areas.`nombre` LIKE '%".$_POST['search_a_climatiza']."%'";
                }
                if(isset($_POST['search_codigo']) && trim($_POST['search_codigo']) != ""){
                    $sWhere .= " AND cat_equipos.`codigo` LIKE '%".$_POST['search_codigo']."%'";
                }
                if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
                    $sWhere .= " AND cat_equipos.`status` = ".$_POST["order_status"];
                }

                /*----------  LIMIT  ----------*/
                if(isset($_POST['length']) && $_POST['length'] > 0){
                    $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
                }
            }
		
		if($data->idsuc=="")
			$data->idsuc=0;
        $sql = "SELECT cat_equipos.id,
        DATE_FORMAT(cat_equipos.fecha,'%d/%m/%Y') AS fecha,
        cat_equiposTipo.`nombre` AS t_equipo, 
        cat_descripciones_equipos.nombre AS descripcion,
        nombre_area,
        capacidad, 
        data_piezas,
        cat_equipos.codigo, 
        cat_equipos.`status`,
        (SELECT COUNT(*) FROM cat_equipos WHERE id_cliente='$data->idcli') AS totalRows
        FROM cat_equipos
        INNER JOIN cat_descripciones_equipos ON id_descripcion_equipo = cat_descripciones_equipos.id
        INNER JOIN cat_equiposTipo ON cat_equipos.`id_tipo_equipo`=cat_equiposTipo.`id`
        INNER JOIN cat_areas ON cat_equipos.`id_area_climatiza`=cat_areas.`id` 
        WHERE cat_equipos.id_cliente='$data->idcli' and cat_equipos.id_sucursal = '$data->idsuc' $sWhere $sOrder $sLimit";
        $res = $this->conexion->link->query($sql);
        //print_r($_GET);
        $datos = (object)[];
        $datos->data = [];
        while($fila = $res->fetch_assoc()){
            $fila = (object)$fila;

            $marca = "";
            $data_piezas = json_decode($fila->data_piezas);
            foreach($data_piezas as $data){
            	if($data->parte == "CONDENSADOR"){
            		$marca = $data->marca;
            	}
            }
        
            $datos->data[] = array (
                $fila->id,
                $fila->codigo,
                $fila->nombre_area,
                $marca,
                #$fila->fecha,
                #$fila->t_equipo,
                strtoupper($fila->descripcion),
                $fila->capacidad,
                #utf8_decode($fila->areaa),
                #strtoupper($fila->codigo),
                '<button class="btn btn-sm '.(($fila->status==1)?'green-jungle':'red').'" id="status">'.(($fila->status==1)?'ACTIVO':'INACTIVO').'</button>',
                '<button id="edit" class="btn btn-sm green btn-outline filter-submit margin-bottom" data-id_equipo="'.$fila->id.'"><i class="fa fa-plus"></i> Editar</button>'
            );
            $datos->recordsTotal = $fila->totalRows;
        }

        $datos->recordsFiltered = count($datos->data);
        #$datos->customActionMessage = "Informacion completada con exito";
        $datos->customActionStatus = "OK";
		
        return json_encode($datos);
    }

    public function getEquipo($id_equipo){
        $sql = "SELECT * FROM cat_equipos WHERE id = $id_equipo";
        $res = $this->conexion->link->query($sql);
        $datos = array();
        if($fila = $res->fetch_assoc()){
             $datos = (object)$fila;
        }
        return $datos;
    }
    
    public function GetSucursal($id){
        $sql = "SELECT nombre_contacto AS nombre
        FROM cat_sucursales 
        WHERE id='$id'";
        $res = $this->conexion->link->query($sql);
        $datos = array();
        if($fila = $res->fetch_assoc()){
             $datos = (object)$fila;
        }
        return $datos;
    }
    
    public function GetCliente($id){
        $sql = "SELECT nombre,id_tipcli
        FROM cat_clientes 
        WHERE id='$id'";
        $res = $this->conexion->link->query($sql);
        $datosCli = array();
        if($fila = $res->fetch_assoc()){
             $datosCli = (object)$fila;
        }
        return $datosCli;
    }
    
    public function GetTipoEquipos(){
        $sql = "SELECT id,nombre FROM cat_equiposTipo WHERE status = 1 ORDER BY nombre";
        $res = $this->conexion->link->query($sql);
        $datos = array();
        while($fila = $res->fetch_assoc()){
             $datos[] = (object)$fila;
        }
        return $datos;
    }
    
    public function GetListaEqu(){ 
        $datos = (object)$_POST;
        $sql = "SELECT id,nombre FROM cat_equiposLista WHERE status = 1 AND id_tipoequipo='$datos->idTipE' ORDER BY nombre";
        $res = $this->conexion->link->query($sql);
        $datos = array();
        while($fila = $res->fetch_assoc()){
             $datos[] = (object)$fila;
        }
        return json_encode($datos);
    }
    
    public function GetPartesEqu(){ 
        $datos = (object)$_POST;
        $sql = "SELECT id,nombre FROM cat_equiposPartes WHERE status = 1 AND id_listaequipo='$datos->idLisE' ORDER BY nombre";
        $res = $this->conexion->link->query($sql);
        $datos = array();
        while($fila = $res->fetch_assoc()){
             $datos[] = $fila;
        }
        return json_encode($datos);
    }

    public function getPiezas(){
        $datos = (object)$_POST;
        $sql = "SELECT * FROM cat_piezas WHERE id_parte_equipo = $datos->id_parte_equipo AND status = 1 ORDER BY id";
        $data = $this->conexion->link->query($sql);
        $datos = array();
        while($fila = $data->fetch_assoc()){
             $datos[] = $fila;
        }
        return json_encode($datos);
    }

    public function getRefrigerantes(){
        $sql = "SELECT * FROM cat_refrigerantes WHERE status = 1 ORDER BY id";
        $data = $this->conexion->link->query($sql);
        $datos = array();
        while($fila = $data->fetch_assoc()){
             $datos[] = (object)$fila;
        }
        return $datos;
    }

    public function getMotores(){
        $sql = "SELECT * FROM cat_motores WHERE status = 1 ORDER BY id";
        $data = $this->conexion->link->query($sql);
        $datos = array();
        while($fila = $data->fetch_assoc()){
             $datos[] = (object)$fila;
        }
        return $datos;
    }
    
    public function GetCapacidadButu(){
        $sql = "SELECT id,nombre FROM cat_capacidadBUTU WHERE status = 1 ORDER BY nombre";
        $res = $this->conexion->link->query($sql);
        $datos = array();
        while($fila = $res->fetch_assoc()){
             $datos[] = (object)$fila;
        }
        return $datos;
    }
    
    public function GetCapacidadHP(){
        $sql = "SELECT id,nombre FROM cat_capacidadHP WHERE status = 1 ORDER BY nombre";
        $res = $this->conexion->link->query($sql);
        $datos = array();
        while($fila = $res->fetch_assoc()){
             $datos[] = (object)$fila;
        }
        return $datos;
    }
    
    public function GetAreaClima(){ 
        $datos = (object)$_POST;
        $sql = "SELECT id,nombre FROM cat_areas WHERE status = 1 AND id_tipoequipo='$datos->idTipE' AND id_tipocliente = '$datos->tipo_cliente' ORDER BY nombre";
        $res = $this->conexion->link->query($sql);
        $datos = array();
        while($fila = $res->fetch_assoc()){
            $fila["nombre"] = utf8_decode(trim($fila["nombre"]));
            $datos[] = $fila;
        }
        return json_encode($datos);
    }

    public function getPartesEquipo(){
        $datos = (object)$_POST;
        if(isset($datos->id) && $datos->id > 0){
            $sql = "SELECT data_piezas FROM cat_equipos WHERE status = 1 AND id='$datos->id'";
            $res = $this->conexion->link->query($sql);
            $datos = array();
            while($fila = $res->fetch_assoc()){
                 $datos[] = $fila;
            }
            $piezas = json_decode($datos[0]["data_piezas"], true);
            
            $i = 1;
            foreach ($piezas as $key => $value) {
                if(trim($piezas[$key]["parte"]) == "EVAPORADOR"){
                    $piezas[$key]["parte_display"] = "EVAPORADOR #".$i;
                    $i++;
                }else{
                    $piezas[$key]["parte_display"] = $piezas[$key]["parte"];
                }
            }
            return json_encode($piezas);
        }
    }

    public function getAreasClimatiza($id_tipo_equipo, $id_tipo_clienete){
        $sql = "SELECT id,nombre, group_concat(id separator ',') AS ids FROM cat_areas WHERE status = 1 AND id_tipoequipo = '$id_tipo_equipo' GROUP BY nombre ORDER BY nombre";
        $res = $this->conexion->link->query($sql);
        $datos = array();
        while($fila = $res->fetch_assoc()){
             $datos[] = (object)$fila;
        }
        return $datos;
    }
    
    public function GetMarcas(){
        $sql = "SELECT id,nombre FROM cat_marcas WHERE status = 1 ORDER BY nombre";
        $res = $this->conexion->link->query($sql);
        $datos = array();
        while($fila = $res->fetch_assoc()){
             $datos[] = (object)$fila;
        }
        return $datos;
    }

    public function getDescripciones(){
        $sql = "SELECT * FROM cat_descripciones_equipos WHERE status = 1 ORDER BY id";
        $res = $this->conexion->link->query($sql);
        $datos = array();
        while($fila = $res->fetch_assoc()){
             $datos[] = (object)$fila;
        }
        return $datos;   
    }

    public function changeStatus(){
        $datos = (object)$_POST;
        $sql = "UPDATE cat_equipos SET status=IF(status , 0 , 1) WHERE id = $datos->id";
        return $this->conexion->link->query($sql);
    }
    
    public function AddEquipo(){
        $datos = (object)$_POST;
        #print_r($datos);
        $datos->fecha = trim($datos->fecha);
		$fecha = substr($datos->fecha,6,4)."-".substr($datos->fecha,3,2)."-".substr($datos->fecha,0,2);

        $array_partes = json_decode($datos->partes);
        $piezas = array();
        foreach ($array_partes as $key => $value) {
            $piezas[] = (object)$value;
        }

        $json_piezas = array();
        
        foreach ($piezas as $llave => $valor) {
            $valor = (object)$valor;
            #foreach ($valor->id_pieza as $key => $value) {
                #$value = (object)$value;
            $json_piezas[] = array("id_pieza"=>$valor->id_pieza,"id_refrigerante"=>$valor->id_refrigerante,"id_motor"=>$valor->id_motor,"id_marca"=>$valor->id_marca,"id_capacidad"=>$valor->id_capacidad,"modelo"=>$valor->modelo,"serie"=>$valor->serie,"id_parte"=>$valor->id_parte,"capacidad"=>$valor->capacidad,"marca"=>$valor->marca,"parte"=>$valor->parte , "status" => $valor->status);
            #}
        }
        
        $sql="INSERT INTO cat_equipos SET
        id_cliente = $datos->id_cliente,
        id_usuario = '{$this->session->logged}',
        id_sucursal = '{$datos->id_sucursal}',
        id_tipo_equipo = $datos->tipo_equipo,
        id_descripcion_equipo = $datos->id_descripcion,
        capacidad = '$datos->capacidad',
        nombre_area = '$datos->nombre_area',
        id_area_climatiza = $datos->area_climatiza,
        codigo = '$datos->codigo',
        fecha = CURRENT_DATE,
        data_piezas = '".json_encode($json_piezas)."';";

        $id = $this->conexion->Consultas(1, $sql);

        if($id > 0)
            echo $id;
        else
            echo 0;

        #print_r($sql);
    }

    public function UpdateEquipo(){
        $datos = (object)$_POST;
        #print_r($datos);
        $datos->fecha = trim($datos->fecha);
        $fecha = substr($datos->fecha,6,4)."-".substr($datos->fecha,3,2)."-".substr($datos->fecha,0,2);

        $array_partes = json_decode($datos->partes);
        $piezas = array();
        foreach ($array_partes as $key => $value) {
            $piezas[] = (object)$value;
        }

        $json_piezas = array();
        
        foreach ($piezas as $llave => $valor) {
            $valor = (object)$valor;
            $json_piezas[] = array("id_pieza"=>$valor->id_pieza,"id_refrigerante"=>$valor->id_refrigerante,"id_motor"=>$valor->id_motor,"id_marca"=>$valor->id_marca,"id_capacidad"=>$valor->id_capacidad,"modelo"=>$valor->modelo,"serie"=>$valor->serie,"id_parte"=>$valor->id_parte,"capacidad"=>$valor->capacidad,"marca"=>$valor->marca,"parte"=>$valor->parte, "status" => $valor->status);
        }
        
        if((int)$datos->id > 0){
            $sql="UPDATE cat_equipos SET
            id_cliente = $datos->id_cliente,
            id_sucursal = '{$datos->id_sucursal}',
            id_tipo_equipo = $datos->tipo_equipo,
            id_descripcion_equipo = $datos->id_descripcion,
            capacidad = '".trim($datos->capacidad)."',
            nombre_area = '$datos->nombre_area',
            id_area_climatiza = $datos->area_climatiza,
            codigo = '$datos->codigo',
            data_piezas = '".json_encode($json_piezas)."'
            WHERE id = $datos->id";
            // print $sql;
            $this->conexion->link->query($sql);
        }
        echo 1;
        #echo mysqli_affected_rows();
        #echo mysql_affected_rows();
        #print_r($sql);
    }
    
    public function getPartes(){
        $datos = (object)$_POST;
        $data = [];
        $sql="SELECT * FROM cat_equiposPartes WHERE status = 1 AND id_tipo_equipo = $datos->idLisE;";
        $res = $this->conexion->link->query($sql);
         while($fila = $res->fetch_assoc()){
            $fila = (object)$fila;
            if($fila->id_equipo_lista > 0){
                if($fila->id_equipo_lista == $datos->id_descripcion){
                    $data[] = $fila;
                }
            }else{
                $data[] = $fila;
            }
         }
        return json_encode($data);
    }
}
?>