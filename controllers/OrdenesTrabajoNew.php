<?php
class OrdenesTrabajoNew {

    private $conexion;
    private $session;
    private $apiProntoforms;

    public function __construct(){
        $this->conexion = new M_Conexion;
        $this->session = Session::getInstance();
        include 'ApiProntoforms.php';
        $this->apiProntoforms = new ApiProntoforms;
        include 'Data.php';
        $this->dataOrigenes = new ApiProntoforms;
    }


    public function index(){
    	 $datos = (object)$_POST;

        $sWhere = "";
        $sOrder = " ORDER BY id";
        $DesAsc = "DESC";
        $sOrder .= " {$DesAsc}";
        $sLimit = "";
        // print_r($_POST);
        if(isset($_POST)){

            /*----------  ORDER BY ----------*/

            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY id {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY fecha {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY cliente {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY  tipo_cliente {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 5){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY tipo_trabajo {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 6){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY num_equipos {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 7){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY sta {$DesAsc}";
            }
            /*----------  ORDER BY ----------*/

            if(isset($_POST['order_id']) && trim($_POST['order_id']) != ""){
                $sWhere .= " AND id LIKE '%".$_POST["order_id"]."'";
            }
            if((isset($_POST['order_date_from']) && trim($_POST['order_date_from']) != "") && (isset($_POST['order_date_to']) && trim($_POST['order_date_to']) != "")){
                $sWhere .= " AND fecha BETWEEN '".$_POST["order_date_from"]."' AND '".$_POST["order_date_to"]."'";
            }
            if(isset($_POST['search_cliente']) && trim($_POST['search_cliente']) != ""){
                $sWhere .= " AND cliente LIKE '%".$_POST['search_cliente']."%'";
            }
            if(isset($_POST['search_tipo_cliente']) && trim($_POST['search_tipo_cliente']) != ""){
                $sWhere .= " AND (SELECT nombre FROM cat_clientesTipo WHERE id = tipo_cliente) LIKE '%".$_POST['search_tipo_cliente']."%'";
            }
            if(isset($_POST['search_tipo_trabajo']) && trim($_POST['search_tipo_trabajo']) != ""){
                $sWhere .= " AND tipo_trabajo LIKE '%".$_POST['search_tipo_trabajo']."%'";
            }
            if(isset($_POST['order_numero_equipo']) && trim($_POST['order_numero_equipo']) != ""){
                $sWhere .= " AND num_equipos LIKE '%".$_POST['order_numero_equipo']."%'";
            }
            if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
                $sWhere .= " AND sta = ".$_POST['order_status'];
            }
            // if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
            //     $sWhere .= " AND cat_equipos.`status` = ".$_POST["order_status"];
            // }

            /*----------  LIMIT  ----------*/
            if(isset($_POST['length']) && $_POST['length'] > 0){
                $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
            }
        }

        $sql = "SELECT cotizaciones.id, orden_trabajo.fecha_agendada AS fecha,
                    (SELECT nombre FROM cat_clientes WHERE id = id_cliente) AS cliente,
                    (SELECT cat_clientesTipo.nombre FROM cat_clientes INNER JOIN cat_clientesTipo ON id_tipcli = cat_clientesTipo.id WHERE cat_clientes.id = id_cliente) AS tipo_cliente, '' AS tipo_trabajo, 0 AS num_equipos, cotizaciones.status, orden_trabajo.status AS sta 
                FROM cotizaciones
                JOIN orden_trabajo ON orden_trabajo.id_cotizacion = cotizaciones.id
                WHERE cotizaciones.status >= 2
                $sWhere $sOrder $sLimit";
        $res = $this->conexion->link->query($sql);
        $response = (object)[];

        
        
        while($fila = $res->fetch_assoc()){
            $fila = (object)$fila;
            $response->data[] = array (
                '<input type="checkbox" name="id[]" value="'.$fila->id.'">',
                $fila->id,
                $fila->fecha,
                $fila->cliente,
                $fila->tipo_cliente,
                $this->getDetailsCotizacion($fila->id),
                $this->getNumEquipos($fila->id),
                '<button class="btn btn-sm '.(($fila->sta==2)?'bg-green-jungle bg-font-green-jungle':'bg-yellow-gold bg-font-yellow-gold').'" id="status">'.(($fila->sta==2)?'AGENDADA':'SIN AGENDAR').'</button>',
                '<button id="edit" class="btn btn-sm green btn-outline filter-submit margin-bottom"><i class="fa fa-plus"></i> Editar</button>'
            );
        }

        $response->recordsTotal = count($response->data);
        $response->customActionMessage = $sql;
        $response->customActionStatus = "OK";

        return json_encode($response);
    }

	 public function ListOrden(){
        extract($_POST);

        $sWhere = "";
        $sOrder = " ORDER BY orden_trabajo.id";
        $DesAsc = "DESC";
        $sOrder .= " {$DesAsc}";
        $sLimit = "";
		$response = (object)[];
        if(isset($_POST)){

            if(isset($_POST['order_id']) && trim($_POST['order_id']) != ""){
                $sWhere .= " AND orden_trabajo.codigo LIKE '%".$_POST["order_id"]."%'";
            }
            if((isset($_POST['order_date_from']) && trim($_POST['order_date_from']) != "") && (isset($_POST['order_date_to']) && trim($_POST['order_date_to']) != "")){
                if($_POST["order_date_from"] == $_POST["order_date_to"])
					$sWhere .= " AND fecha_agendada like '".$_POST["order_date_from"]."%'";
				else
				$sWhere .= " AND fecha BETWEEN '".$_POST["order_date_from"]."' AND '".$_POST["order_date_to"]."'";
            }
            if(isset($_POST['search_cliente']) && trim($_POST['search_cliente']) != ""){
                $sWhere .= " AND (SELECT nombre FROM cat_clientes WHERE id = id_cliente) LIKE '%".$_POST['search_cliente']."%'";
            }
            if(isset($_POST['search_tipo_cliente']) && trim($_POST['search_tipo_cliente']) != ""){
                $sWhere .= " AND (SELECT nombre FROM cat_clientesTipo WHERE id = tipo_cliente) LIKE '%".$_POST['search_tipo_cliente']."%'";
            }
            if(isset($_POST['search_tipo_trabajo']) && trim($_POST['search_tipo_trabajo']) != ""){
                $sWhere .= " AND tipo_trabajo LIKE '%".$_POST['search_tipo_trabajo']."%'";
            }
            if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
                $sWhere .= " AND orden_trabajo.status  = ".$_POST['order_status'];
            }

            /*----------  LIMIT  ----------*/
            if(isset($_POST['length']) && $_POST['length'] > 0){
                $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
            }
        }
        $sql = "SELECT cotizaciones.id, orden_trabajo.fecha_agendada AS fecha,
            IFNULL(
                    CONCAT( (SELECT nombre FROM cat_clientes WHERE id = id_cliente),' (SUC. ',(SELECT nombre_contacto FROM cat_sucursales WHERE id = id_sucursal),')'),
                    (SELECT nombre FROM cat_clientes WHERE id = id_cliente)
                ) AS cliente,
            (SELECT cat_clientesTipo.nombre FROM cat_clientes INNER JOIN cat_clientesTipo
            ON id_tipcli = cat_clientesTipo.id WHERE cat_clientes.id = id_cliente) AS tipo_cliente,
            '' AS tipo_trabajo, 0 AS num_equipos, cotizaciones.status, orden_trabajo.status AS sta, orden_trabajo.codigo FROM cotizaciones
            JOIN orden_trabajo ON orden_trabajo.id_cotizacion = cotizaciones.id
            WHERE cotizaciones.status >= 2
        $sWhere $sOrder $sLimit";

        $labelStatus = [
            1 =>  '<span class="btn btn-sm bg-red-thunderbird bg-font-red-thunderbird" id="status">Por Agendar</span>',
            2 =>  '<button class="btn btn-sm bg-yellow-gold bg-font-yellow-gold" id="status">Agendada</button>',
            3 =>  '<span class="btn btn-sm bg-green-jungle bg-font-green-jungle" id="status">Finalizada</span>',
        ];
		$editStatus = [
            1 =>  '<button id="edit" class="btn btn-sm green btn-outline filter-submit margin-bottom"><i class="fa fa-plus"></i> Editar</button>',
            2 =>  '<button id="edit" class="btn btn-sm green btn-outline filter-submit margin-bottom"><i class="fa fa-plus"></i> Editar</button>',
            3 =>  '<button id="garantia" class="btn btn-sm green btn-outline filter-submit margin-bottom"><i class="fa fa-plus"></i> Rebote</button>',
        ];
        $response->recordsTotal = 0;
        $response->data = [];
        $buttonAditional = "";
        $res = $this->conexion->link->query($sql);
        while($fila = $res->fetch_assoc()){
            $fila = (object)$fila;
            if(count(explode("-", $fila->codigo)) == 2){
                if($fila->sta == 3){
                    $buttonAditional = '<a id="imprimir" class="btn btn-sm green btn-outline filter-submit margin-bottom" href="print_reporte.php?id='.$fila->id.'" target="_blank"><iclass="fa fa-plus"></i>PDF</a> 
                                        <a class="fa fa-download" href="download_reporte.php?id='.$fila->id.'"></a>';
                }else{
                    $buttonAditional = "";
                }

                $response->data[] = array (
                    $fila->id,
                    $fila->codigo,
                    $fila->fecha,
                    $fila->cliente,
                    $fila->tipo_cliente,
                    $this->getDetailsCotizacion($fila->id),
                    $this->getNumEquipos($fila->id),
                    $labelStatus[$fila->sta],
                    $editStatus[$fila->sta].$buttonAditional
                );
    			$response->recordsTotal = $fila->totalRows;
            }
        }


        $response->recordsFiltered = count($response->data);
        $response->customActionMessage = "Informacion completada con exito";
        $response->customActionStatus = "OK";

        return json_encode($response);
    }

    private function actualizarOrigenes(){
        $this->dataOrigenes->ds_areas();
        $this->dataOrigenes->ds_listadoRevisionInstalacion();
        $this->dataOrigenes->ds_herramientas();
        $this->dataOrigenes->ds_descriptionEquipo();
        $this->dataOrigenes->ds_supervisores();
    }

    private function getDetailsCotizacion($id = 0){
        $span = "<div style='width: 180px;'>";
        if($id > 0){
            $sql = "SELECT (SELECT descripcion FROM cat_tipos_trabajo WHERE id = tipo_trabajo) AS label ,
                (SELECT clase FROM cat_tipos_trabajo WHERE id = tipo_trabajo) AS class
            FROM cotizaciones_detalle
            WHERE id_cotizacion = {$id}
            GROUP BY cotizaciones_detalle.id";
            $res = $this->conexion->link->query($sql);
            $count = 0;
            $span .= "<div class='row' style='padding: 2px;'> <div class='col-md-3'>";
            while($fila = $res->fetch_assoc()){
                $fila = (object) $fila;
                $new_label = "";
                foreach(split(" ", $fila->label) as $index){
                    $new_label .= substr($index , 0 , 3).". ";
                }
                $fila->label = substr($new_label, 0, -2);
                // if($count == 1)
                $span .= '<span class="label '.$fila->class.'" style="margin:2px">'.wordwrap($fila->label).'</span>';
                // if($count%2 != 0)
                $count++;
            }
        }
        $span .= "</div>";
        $span .= "</div>";
        $span .= '</div>';
        return $span;
    }

    public function save(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if((int)$data->idOrder>0){
            $mysqli->real_query("UPDATE orden_trabajo SET cliente='$data->cliente',
            direccion='$data->direccion',tiempo_estimado='$data->tiempo',tipo_cliente='$data->tipo_cliente',
            tipo_trabajo='$data->tipo_trabajo',fecha='$data->fecha' WHERE id='$data->idOrder';");
            $ids=$data->idOrder;

            $mysqli->real_query("DELETE FROM orden_trabajo_detalle WHERE id_orden='$data->idOrder';");
            foreach($data->areas as $areaa){
                $areaa = (array)$areaa;
                $mysqli->real_query("INSERT INTO orden_trabajo_detalle SET id_orden='$ids',orden_trabajo_detalle.`area`='".$areaa['area']."',
                tipo_equipo='".$areaa['tequipo']."',
                des_equipo='".$areaa['desequipo']."',tipo_trabajo='$data->tipo_trabajo'");
            }
            return $ids;
        }
        else{
            $mysqli->real_query("INSERT INTO orden_trabajo SET cliente='$data->cliente',
            direccion='$data->direccion',tiempo_estimado='$data->tiempo',tipo_cliente='$data->tipo_cliente',
            tipo_trabajo='$data->tipo_trabajo',fecha='$data->fecha';");
            $ids=$mysqli->insert_id;
            // print_r($data);
            foreach($data->areas as $areaa){
                $areaa = (array)$areaa;
                $mysqli->real_query("INSERT INTO orden_trabajo_detalle SET id_orden='$ids',orden_trabajo_detalle.`area`='".$areaa['area']."',
                tipo_equipo='".$areaa['tequipo']."',des_equipo='".$areaa['desequipo']."',tipo_trabajo='$data->tipo_trabajo'");
            }
            return $ids;
        }

        /* REFRESH PRONTOFORMS */
        $isMantenimiento = $this->conexion->queryRow("SELECT id FROM orden_trabajo WHERE tipo_trabajo LIKE '%MANTENIMIENTO%' AND tipo_trabajo != 'REVISION MANTENIMIENTO' AND id = '{$ids}'");
        $this->actualizarOrigenes();
    }

    public function saveEvent(){
        $postdata = (object)$_POST;
        if($postdata->idorden >  0){
            $sql="UPDATE orden_trabajo SET fecha = '{$postdata->start}' , tiempo_estimado = '{$postdata->end}' , status = 1 WHERE id='$postdata->idorden'";
            $this->conexion->Consultas(1,$sql);
        }

        /* REFRESH PRONTOFORMS */
        $isMantenimiento = $this->conexion->queryRow("SELECT id FROM orden_trabajo WHERE tipo_trabajo LIKE '%MANTENIMIENTO%' AND tipo_trabajo != 'REVISION MANTENIMIENTO' AND id = '{$postdata->idorden}'");
        if($isMantenimiento){
            $this->apiProntoforms->refreshMantenimiento();
            $this->actualizarOrigenes();
        }
        return true;
    }

    public function edit(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        if($postdata->id_orden > 0){
            $sql = "SELECT *, orden_trabajo.observaciones AS obser, cat_clientes.id as id_c , orden_trabajo.id AS id_orden FROM orden_trabajo
                    INNER JOIN cotizaciones
                    ON orden_trabajo.id_cotizacion = cotizaciones.id
                    INNER JOIN cat_clientes
                    ON  cotizaciones.id_cliente = cat_clientes.id
                    WHERE cotizaciones.id ='{$postdata->id_orden}' AND orden_trabajo.codigo='{$postdata->codigo}'";
            $res = $this->conexion->link->query($sql);
            $res = $res->fetch_object();
			if($res->sucursales=="SI"){
				$sql2 = "SELECT cat_sucursales.* FROM cat_sucursales
                            JOIN cat_clientes ON cat_clientes.id = cat_sucursales.id_cliente
                            JOIN cotizaciones ON cat_sucursales.id = cotizaciones.id_sucursal
                            JOIN orden_trabajo ON orden_trabajo.id_cotizacion = cotizaciones.id
                            WHERE cotizaciones.id ='{$postdata->id_orden}' AND orden_trabajo.codigo='{$postdata->codigo}'";
				$res2 = $this->conexion->link->query($sql2);
				$res2 = $res2->fetch_object();
				$res->direccion = $res2->direccion;
				$res->id_tipcli = $res2->id_tipcli;
				$res->sucursaaaal = $res2->nombre_contacto;
			}
			else{
				$res->sucursaaaal = "Sin sucursal";
			}

            $response->data = $res;
            $materiales = "SELECT orden_trabajo_materiales.id, orden_trabajo_materiales.seleccion AS item, orden_trabajo_materiales.descripcion
                AS descripcion_item, orden_trabajo_materiales.unidad, orden_trabajo_materiales.cantidad_requerida AS cantidad, orden_trabajo_materiales.codigo as codigo FROM orden_trabajo_materiales
                JOIN orden_trabajo
                ON orden_trabajo.id = orden_trabajo_materiales.id_orden
                JOIN cotizaciones
                ON  cotizaciones.id = orden_trabajo.id_cotizacion
                WHERE cotizaciones.id ='{$postdata->id_orden}' AND orden_trabajo_materiales.status=1
				AND orden_trabajo.codigo='{$postdata->codigo}'";
            $materiales = $this->conexion->link->query($materiales);
            $response->materiales = [];
			while($fila = $materiales->fetch_assoc()){
                $fila = (object)$fila;
                $response->materiales[] = array ('id'=>$fila->id,
                    'codigo'=>$fila->codigo,
                    'item'=>$fila->item,
                    'descripcion_item'=>$fila->descripcion_item,
                    'unidad'=>$fila->unidad,
                    'cantidad'=>$fila->cantidad,
    				'edit'=>0
                );
			}
            $herramientas = "SELECT orden_trabajo_herramientas.* FROM orden_trabajo_herramientas
                    JOIN orden_trabajo
                    ON orden_trabajo.id = orden_trabajo_herramientas.id_orden
                    JOIN cotizaciones
                    ON  cotizaciones.id = orden_trabajo.id_cotizacion
                    WHERE cotizaciones.id ='{$postdata->id_orden}' and orden_trabajo_herramientas.status=1
					and orden_trabajo.codigo='{$postdata->codigo}'";
			$herramientas = $this->conexion->link->query($herramientas);
			$response->herramientas = [];
			while($fila = $herramientas->fetch_assoc()){
                $fila = (object)$fila;
                $response->herramientas[] = array ('id'=>$fila->id,
                    'id_orden'=>$fila->id_orden,
                    'herramienta'=>$fila->herramientas,
                    'requerimientos'=>$fila->requerimientos,
    				'edit'=>0
                );
			}

			$insclimA = "SELECT ins_caracteristicas_climatizacion_a.*, cat_descripciones_equipos.nombre AS des, cat_areas.nombre AS are FROM ins_caracteristicas_climatizacion_a
                JOIN orden_trabajo ON orden_trabajo.id = ins_caracteristicas_climatizacion_a.id_orden
                JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = ins_caracteristicas_climatizacion_a.descripcion
                JOIN cat_areas ON cat_areas.id = ins_caracteristicas_climatizacion_a.area
                WHERE orden_trabajo.id_cotizacion  = {$postdata->id_orden} AND ins_caracteristicas_climatizacion_a.status=1 and orden_trabajo.codigo='{$postdata->codigo}'";
			$insclimA = $this->conexion->link->query($insclimA);
			$response->insclimA = [];
			while($fila = $insclimA->fetch_assoc()){
                $fila = (object)$fila;
                $response->insclimA[] = array ('id'=>$fila->id,
                    'area'=>$fila->are,
                    'desequipo'=>$fila->des,
                    'codigo'=>$fila->codigo,
                    'marca'=>$fila->marca,
                    'capacidadBTU'=>$fila->capacidadBTU,
                    'modelo'=>$fila->modelo,
                    'serie'=>$fila->serie,
    				'edit'=>0
                );
			}

			$manclimA = "SELECT man_caracteristicas_climatizacion_a.*, cat_descripciones_equipos.nombre AS des, cat_areas.nombre AS are FROM man_caracteristicas_climatizacion_a
                JOIN orden_trabajo ON orden_trabajo.id = man_caracteristicas_climatizacion_a.id_orden
                JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = man_caracteristicas_climatizacion_a.descripcion
                JOIN cat_areas ON cat_areas.id = man_caracteristicas_climatizacion_a.area
                WHERE orden_trabajo.id_cotizacion  = {$postdata->id_orden} AND man_caracteristicas_climatizacion_a.status=1
                and orden_trabajo.codigo='{$postdata->codigo}'";
			$manclimA = $this->conexion->link->query($manclimA);
			$response->manclimA = [];
			while($fila = $manclimA->fetch_assoc()){
                $fila = (object)$fila;
                $response->manclimA[] = array ('id'=>$fila->id,
                    'area'=>$fila->are,
                    'desequipo'=>$fila->des,
                    'codigo'=>$fila->codigo,
                    'marca'=>$fila->marca,
                    'capacidadBTU'=>$fila->capacidadBTU,
                    'modelo'=>$fila->modelo,
                    'serie'=>$fila->serie,
    				'edit'=>0
                );
			}

			$corclimA = "SELECT cor_caracteristicas_climatizacion_a.*, cat_descripciones_equipos.nombre AS des, cat_areas.nombre AS are FROM cor_caracteristicas_climatizacion_a
                JOIN orden_trabajo ON orden_trabajo.id = cor_caracteristicas_climatizacion_a.id_orden
                JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = cor_caracteristicas_climatizacion_a.descripcion
                JOIN cat_areas ON cat_areas.id = cor_caracteristicas_climatizacion_a.area
                WHERE orden_trabajo.id_cotizacion  = {$postdata->id_orden} AND cor_caracteristicas_climatizacion_a.status=1
                and orden_trabajo.codigo='{$postdata->codigo}'";
			$corclimA = $this->conexion->link->query($corclimA);
			$response->corclimA = [];
			while($fila = $corclimA->fetch_assoc()){
                $fila = (object)$fila;
                $response->corclimA[] = array ('id'=>$fila->id,
                    'area'=>$fila->are,
                    'desequipo'=>$fila->des,
                    'codigo'=>$fila->codigo,
                    'marca'=>$fila->marca,
                    'capacidadBTU'=>$fila->capacidadBTU,
                    'modelo'=>$fila->modelo,
                    'serie'=>$fila->serie,
    				'edit'=>0
                );
			}

			$revclimA = "SELECT rev_caracteristicas_climatizacion_a.*, cat_descripciones_equipos.nombre AS des,
    			cat_areas.nombre AS are FROM rev_caracteristicas_climatizacion_a
    			JOIN orden_trabajo ON orden_trabajo.id = rev_caracteristicas_climatizacion_a.id_orden
    			JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = rev_caracteristicas_climatizacion_a.descripcion
    			JOIN cat_areas ON cat_areas.id = rev_caracteristicas_climatizacion_a.area
    			WHERE rev_caracteristicas_climatizacion_a.`tipo`='CORRECTIVO' AND
    			orden_trabajo.id_cotizacion  = {$postdata->id_orden} AND rev_caracteristicas_climatizacion_a.status=1
    			and orden_trabajo.codigo='{$postdata->codigo}'";
			$revclimA = $this->conexion->link->query($revclimA);
			$response->revclimA = [];
			while($fila = $revclimA->fetch_assoc()){
                $fila = (object)$fila;
                $response->revclimA[] = array ('id'=>$fila->id,
                    'area'=>$fila->are,
                    'desequipo'=>$fila->des,
                    'codigo'=>$fila->codigo,
                    'marca'=>$fila->marca,
                    'capacidadBTU'=>$fila->capacidadBTU,
                    'modelo'=>$fila->modelo,
                    'serie'=>$fila->serie,
    				'edit'=>0
                );
			}

			$insclimB = "SELECT ins_caracteristicas_climatizacion_b.*, cat_descripciones_equipos.nombre AS des, cat_areas.nombre AS are FROM ins_caracteristicas_climatizacion_b
                JOIN orden_trabajo ON orden_trabajo.id = ins_caracteristicas_climatizacion_b.id_orden
                JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = ins_caracteristicas_climatizacion_b.descripcion
                JOIN cat_areas ON cat_areas.id = ins_caracteristicas_climatizacion_b.area
                WHERE orden_trabajo.id_cotizacion  = {$postdata->id_orden} AND ins_caracteristicas_climatizacion_b.status=1
                and orden_trabajo.codigo='{$postdata->codigo}'";
            /*$insclimB = "SELECT 
                        cat_descripciones_equipos.nombre AS des, 
                        cat_areas.nombre AS are, 
                        cat_equipos.codigo,
                        data_piezas
                FROM cat_equipos
                JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = cat_equipos.id_descripcion_equipo
                JOIN cat_areas ON cat_areas.id = cat_equipos.area
                WHERE cat_equipos.status = 1";*/
			$insclimB = $this->conexion->link->query($insclimB);
			$response->insclimB = [];
			while($fila = $insclimB->fetch_assoc()){
                $fila = (object)$fila;
                /*GET DATA PIEZAS*/
                /*$data_piezas = json_decode($fila->data_piezas);
                $evaporador = (object) [
                    "marca" => "",
                    "capacidad" => "",
                    "modelo" => "",
                    "serie" => ""
                ];
                $evaporador = (object) [
                    "marca" => "",
                    "capacidad" => "",
                    "modelo" => "",
                    "serie" => ""
                ];
                foreach($data_piezas as $pieza){
                    if($pieza->parte == "EVAPORADOR"){

                    }
                    if($pieza->parte == "CONDENSADOR"){

                    }
                }
                /*GET DATA PIEZAS*/

                $response->insclimB[] = array ('id'=>$fila->id,
                    'area'=>$fila->are,
                    'desequipo'=>$fila->des,
                    'codigo'=>$fila->codigo,
                    'marcaE'=>$fila->marca,
                    'capacidadBTUE'=>$fila->capacidadBTU,
                    'modeloE'=>$fila->modelo,
                    'serieE'=>$fila->serie,
    				'marcaC'=>$fila->condensadorMarca,
                    'capacidadBTUC'=>$fila->condensadorBTU,
                    'modeloC'=>$fila->condensadorModelo,
                    'serieC'=>$fila->condensadorSerie,
    				'edit'=>0
                );
			}
			$manclimB = "SELECT man_caracteristicas_climatizacion_b.*, cat_descripciones_equipos.nombre AS des, cat_areas.nombre AS are FROM man_caracteristicas_climatizacion_b
                JOIN orden_trabajo ON orden_trabajo.id = man_caracteristicas_climatizacion_b.id_orden
                JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = man_caracteristicas_climatizacion_b.descripcion
                JOIN cat_areas ON cat_areas.id = man_caracteristicas_climatizacion_b.area
                WHERE orden_trabajo.id_cotizacion  = {$postdata->id_orden} AND man_caracteristicas_climatizacion_b.status=1
                and orden_trabajo.codigo='{$postdata->codigo}'";

			$manclimB = $this->conexion->link->query($manclimB);
			$response->manclimB = [];
			while($fila = $manclimB->fetch_assoc()){
                $fila = (object)$fila;
                $response->manclimB[] = array ('id'=>$fila->id,
                    'area'=>$fila->are,
                    'desequipo'=>$fila->des,
                    'codigo'=>$fila->codigo,
                    'marcaE'=>$fila->marca,
                    'capacidadBTUE'=>$fila->capacidadBTU,
                    'modeloE'=>$fila->modelo,
                    'serieE'=>$fila->serie,
    				'marcaC'=>$fila->condensadorMarca,
                    'capacidadBTUC'=>$fila->condensadorBTU,
                    'modeloC'=>$fila->condensadorModelo,
                    'serieC'=>$fila->condensadorSerie,
    				'edit'=>0
                );
			}

			$corclimB = "SELECT cor_caracteristicas_climatizacion_b.*, cat_descripciones_equipos.nombre AS des, cat_areas.nombre AS are FROM cor_caracteristicas_climatizacion_b
                JOIN orden_trabajo ON orden_trabajo.id = cor_caracteristicas_climatizacion_b.id_orden
                JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = cor_caracteristicas_climatizacion_b.descripcion
                JOIN cat_areas ON cat_areas.id = cor_caracteristicas_climatizacion_b.area
                WHERE orden_trabajo.id_cotizacion  = {$postdata->id_orden} AND cor_caracteristicas_climatizacion_b.status=1
                and orden_trabajo.codigo='{$postdata->codigo}'";

			$corclimB = $this->conexion->link->query($corclimB);
			$response->corclimB = [];
			while($fila = $corclimB->fetch_assoc()){
                $fila = (object)$fila;
                $response->corclimB[] = array ('id'=>$fila->id,
                    'area'=>$fila->are,
                    'desequipo'=>$fila->des,
                    'codigo'=>$fila->codigo,
                    'marcaE'=>$fila->marca,
                    'capacidadBTUE'=>$fila->capacidadBTU,
                    'modeloE'=>$fila->modelo,
                    'serieE'=>$fila->serie,
    				'marcaC'=>$fila->condensadorMarca,
                    'capacidadBTUC'=>$fila->condensadorBTU,
                    'modeloC'=>$fila->condensadorModelo,
                    'serieC'=>$fila->condensadorSerie,
    				'edit'=>0
                );
			}


			$revclimB = "SELECT rev_caracteristicas_climatizacion_b.*, cat_descripciones_equipos.nombre AS des,
    			cat_areas.nombre AS are FROM rev_caracteristicas_climatizacion_b
    			JOIN orden_trabajo ON orden_trabajo.id = rev_caracteristicas_climatizacion_b.id_orden
    			JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = rev_caracteristicas_climatizacion_b.descripcion
    			JOIN cat_areas ON cat_areas.id = rev_caracteristicas_climatizacion_b.area
    			WHERE `rev_caracteristicas_climatizacion_b`.`tipo`='CORRECTIVO' AND
    			orden_trabajo.id_cotizacion  = {$postdata->id_orden} AND rev_caracteristicas_climatizacion_b.status=1
    			AND orden_trabajo.codigo='{$postdata->codigo}'";

			$revclimB = $this->conexion->link->query($revclimB);
			$response->revclimB = [];
			while($fila = $revclimB->fetch_assoc()){
                $fila = (object)$fila;
                $response->revclimB[] = array ('id'=>$fila->id,
                    'area'=>$fila->are,
                    'desequipo'=>$fila->des,
                    'codigo'=>$fila->codigo,
                    'marcaE'=>$fila->marca,
                    'capacidadBTUE'=>$fila->capacidadBTU,
                    'modeloE'=>$fila->modelo,
                    'serieE'=>$fila->serie,
    				'marcaC'=>$fila->condensadorMarca,
                    'capacidadBTUC'=>$fila->condensadorBTU,
                    'modeloC'=>$fila->condensadorModelo,
                    'serieC'=>$fila->condensadorSerie,
    				'edit'=>0
                );
            }
            
            $sucursal = " id_sucursal = ''";
            if($res->sucursaaaal != "Sin sucursal"){
                $sucursal = " id_sucursal = {$res->id_sucursal}";
            }

			$equiposclimA = "SELECT cat_equipos.id, cat_equipos.codigo, cat_equipos.nombre_area, cat_descripciones_equipos.nombre 
                            FROM cat_equipos 
                            INNER JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = cat_equipos.id_descripcion_equipo 
                            INNER JOIN cat_clientes ON cat_clientes.id = cat_equipos.id_cliente
                            WHERE cat_equipos.id_tipo_equipo = 1 AND id_cliente = {$res->id_cliente} AND ($sucursal OR cat_clientes.sucursales = 'NO')";
			$equiposclimA = $this->conexion->link->query($equiposclimA);
			$response->equiposclimA = [];
			while($fila = $equiposclimA->fetch_assoc()){
                $fila = (object)$fila;
                $response->equiposclimA[] = array ('id'=>$fila->id,
                    'nombre'=> $fila->codigo .' - '.$fila->nombre.' - '.$fila->nombre_area,
                );
            }

			$equiposclimB = "SELECT cat_equipos.id, cat_equipos.codigo, cat_equipos.nombre_area, cat_descripciones_equipos.nombre 
                            FROM cat_equipos 
                            LEFT JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = cat_equipos.id_descripcion_equipo 
                            LEFT JOIN cat_clientes ON cat_clientes.id = cat_equipos.id_cliente
                            WHERE cat_equipos.id_tipo_equipo = 2 AND id_cliente = {$res->id_cliente} AND ($sucursal OR cat_clientes.sucursales = 'NO')";
			$equiposclimB = $this->conexion->link->query($equiposclimB);
			$response->equiposclimB = [];
			while($fila = $equiposclimB->fetch_assoc()){
                $fila = (object)$fila;
                $response->equiposclimB[] = array ('id'=>$fila->id,
                    'nombre'=> $fila->codigo .' - '.$fila->nombre.' - '.$fila->nombre_area,
                );
			}

			$equiposRefri = "SELECT cat_equipos.id, cat_equipos.codigo, cat_equipos.nombre_area, cat_descripciones_equipos.nombre 
                            FROM cat_equipos 
                            INNER JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = cat_equipos.id_descripcion_equipo 
                            INNER JOIN cat_clientes ON cat_clientes.id = cat_equipos.id_cliente
                            WHERE cat_equipos.id_tipo_equipo = 4 AND id_cliente = {$res->id_cliente} AND ($sucursal OR cat_clientes.sucursales = 'NO')";
			$equiposRefri = $this->conexion->link->query($equiposRefri);
			$response->equiposRefri = [];
			while($fila = $equiposRefri->fetch_assoc()){
                $fila = (object)$fila;
                $response->equiposRefri[] = array ('id'=>$fila->id,
                    'nombre'=> $fila->codigo .' - '.$fila->nombre.' - '.$fila->nombre_area,
                );
			}

			$equiposVentilacion = "SELECT cat_equipos.id, cat_equipos.codigo, cat_equipos.nombre_area, cat_descripciones_equipos.nombre 
                                    FROM cat_equipos 
                                    INNER JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = cat_equipos.id_descripcion_equipo 
                                    INNER JOIN cat_clientes ON cat_clientes.id = cat_equipos.id_cliente
                                    WHERE cat_equipos.id_tipo_equipo = 3 AND id_cliente = {$res->id_cliente} AND ($sucursal OR cat_clientes.sucursales = 'NO')";
			$equiposVentilacion = $this->conexion->link->query($equiposVentilacion);
			$response->equiposVentilacion = [];
			while($fila = $equiposVentilacion->fetch_assoc()){
                $fila = (object)$fila;
                $response->equiposVentilacion[] = array ('id'=>$fila->id,
                    'nombre'=> $fila->codigo .' - '.$fila->nombre.' - '.$fila->nombre_area,
                );
			}
            $insventi = "SELECT ins_caracteristicas_ventilacion.*, cat_descripciones_equipos.nombre AS des, cat_areas.nombre AS are FROM ins_caracteristicas_ventilacion
                JOIN orden_trabajo ON orden_trabajo.id = ins_caracteristicas_ventilacion.id_orden
                JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = ins_caracteristicas_ventilacion.descripcion
                JOIN cat_areas ON cat_areas.id = ins_caracteristicas_ventilacion.area
                WHERE orden_trabajo.id_cotizacion  = {$postdata->id_orden} AND ins_caracteristicas_ventilacion.status=1
                and orden_trabajo.codigo='{$postdata->codigo}'";

			$insventi = $this->conexion->link->query($insventi);
			$response->insVentilacion = [];
			while($fila = $insventi->fetch_assoc()){
                $fila = (object)$fila;
                $response->insVentilacion[] = array ('id'=>$fila->id,
                    'area'=>$fila->are,
                    'desequipo'=>$fila->des,
                    'codigo'=>$fila->codigo,
                    'marca'=>$fila->marca,
                    'capacidadHP'=>$fila->capacidadHP,
                    'capacidadCFN'=>$fila->capacidadCFN,
                    'modelo'=>$fila->condensadorModelo,
                    'serie'=>$fila->condensadorSerie,
    				'edit'=>0
                );
			}
			$manVentilacion = "SELECT man_caracteristicas_ventilacion.*, cat_descripciones_equipos.nombre AS des, cat_areas.nombre AS are FROM man_caracteristicas_ventilacion
                JOIN orden_trabajo ON orden_trabajo.id = man_caracteristicas_ventilacion.id_orden
                JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = man_caracteristicas_ventilacion.descripcion
                JOIN cat_areas ON cat_areas.id = man_caracteristicas_ventilacion.area
                WHERE orden_trabajo.id_cotizacion  = {$postdata->id_orden} AND man_caracteristicas_ventilacion.status=1
                and orden_trabajo.codigo='{$postdata->codigo}'";

			$manVentilacion = $this->conexion->link->query($manVentilacion);
			$response->manVentilacion = [];
			while($fila = $manVentilacion->fetch_assoc()){
                $fila = (object)$fila;
                $response->manVentilacion[] = array ('id'=>$fila->id,
                    'area'=>$fila->are,
                    'desequipo'=>$fila->des,
                    'codigo'=>$fila->codigo,
                    'marca'=>$fila->marca,
                    'capacidadHP'=>$fila->capacidadHP,
                    'capacidadCFN'=>$fila->capacidadCFN,
                    'modelo'=>$fila->modelo,
                    'serie'=>$fila->serie,
    				'edit'=>0
                );
			}
			$corVentilacion = "SELECT cor_caracteristicas_ventilacion.*, cat_descripciones_equipos.nombre AS des, cat_areas.nombre AS are FROM cor_caracteristicas_ventilacion
                JOIN orden_trabajo ON orden_trabajo.id = cor_caracteristicas_ventilacion.id_orden
                JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = cor_caracteristicas_ventilacion.descripcion
                JOIN cat_areas ON cat_areas.id = cor_caracteristicas_ventilacion.area
                WHERE orden_trabajo.id_cotizacion  = {$postdata->id_orden} AND cor_caracteristicas_ventilacion.status=1
                and orden_trabajo.codigo='{$postdata->codigo}'";

			$corVentilacion = $this->conexion->link->query($corVentilacion);
			$response->corVentilacion = [];
    			while($fila = $corVentilacion->fetch_assoc()){
                $fila = (object)$fila;
                $response->corVentilacion[] = array ('id'=>$fila->id,
                    'area'=>$fila->are,
                    'desequipo'=>$fila->des,
                    'codigo'=>$fila->codigo,
                    'marca'=>$fila->marca,
                    'capacidadHP'=>$fila->capacidadHP,
                    'capacidadCFN'=>$fila->capacidadCFN,
                    'modelo'=>$fila->modelo,
                    'serie'=>$fila->serie,
    				'edit'=>0
                );
			}


			$revVentilacion = "SELECT rev_caracteristicas_ventilacion.*, cat_descripciones_equipos.nombre AS des,
    			cat_areas.nombre AS are FROM rev_caracteristicas_ventilacion
    			JOIN orden_trabajo ON orden_trabajo.id = rev_caracteristicas_ventilacion.id_orden
    			JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = rev_caracteristicas_ventilacion.descripcion
    			JOIN cat_areas ON cat_areas.id = rev_caracteristicas_ventilacion.area
    			WHERE `rev_caracteristicas_ventilacion`.`tipo`='CORRECTIVO' AND
    			orden_trabajo.id_cotizacion  = {$postdata->id_orden} AND rev_caracteristicas_ventilacion.status=1
    			and orden_trabajo.codigo='{$postdata->codigo}'";

			$revVentilacion = $this->conexion->link->query($revVentilacion);
			$response->revVentilacion = [];
			while($fila = $revVentilacion->fetch_assoc()){
                $fila = (object)$fila;
                $response->revVentilacion[] = array ('id'=>$fila->id,
                    'area'=>$fila->are,
                    'desequipo'=>$fila->des,
                    'codigo'=>$fila->codigo,
                    'marca'=>$fila->marca,
                    'capacidadHP'=>$fila->capacidadHP,
                    'capacidadCFN'=>$fila->capacidadCFN,
                    'modelo'=>$fila->condensadorModelo,
                    'serie'=>$fila->condensadorSerie,
    				'edit'=>0
                );
			}

			$insRefri = "SELECT ins_caracteristicas_refrigeracion.*, cat_descripciones_equipos.nombre AS des, cat_areas.nombre AS are FROM ins_caracteristicas_refrigeracion
                JOIN orden_trabajo ON orden_trabajo.id = ins_caracteristicas_refrigeracion.id_orden
                JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = ins_caracteristicas_refrigeracion.id_equipo_des
                JOIN cat_areas ON cat_areas.id = ins_caracteristicas_refrigeracion.id_area
                WHERE orden_trabajo.id_cotizacion  = {$postdata->id_orden} AND ins_caracteristicas_refrigeracion.status=1
                and orden_trabajo.codigo='{$postdata->codigo}'";

			$insRefri = $this->conexion->link->query($insRefri);
			$response->insRefri = [];
			while($fila = $insRefri->fetch_assoc()){
                $fila = (object)$fila;
                $response->insRefri[] = array ('id'=>$fila->id,
                    'area'=>$fila->are,
                    'desequipo'=>$fila->des,
                    'codigo'=>$fila->codigo,
    				'marca_eva'=>$fila->marca_eva,
    				'capacidad_hp_eva'=>$fila->capacidad_hp_eva,
    				'modelo_eva'=>$fila->modelo_eva,
    				'serie_eva'=>$fila->serie_eva,
    				'marca_conde'=>$fila->marca_conde,
    				'capacidad_hp_conde'=>$fila->capacidad_hp_conde,
    				'modelo_conde'=>$fila->modelo_conde,
    				'serie_conde'=>$fila->serie_conde,
    				'edit'=>0
                );
			}
			$manRefri = "SELECT man_caracteristicas_refrigeracion.*, cat_descripciones_equipos.nombre AS des, cat_areas.nombre AS are FROM man_caracteristicas_refrigeracion
                JOIN orden_trabajo ON orden_trabajo.id = man_caracteristicas_refrigeracion.id_orden
                JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = man_caracteristicas_refrigeracion.id_equipo_des
                JOIN cat_areas ON cat_areas.id = man_caracteristicas_refrigeracion.id_area
                WHERE orden_trabajo.id_cotizacion  = {$postdata->id_orden} AND man_caracteristicas_refrigeracion.status=1
                and orden_trabajo.codigo='{$postdata->codigo}'";

			$manRefri = $this->conexion->link->query($manRefri);
			$response->manRefri = [];
			while($fila = $manRefri->fetch_assoc()){
                $fila = (object)$fila;
                $response->manRefri[] = array ('id'=>$fila->id,
                    'area'=>$fila->are,
                    'desequipo'=>$fila->des,
                    'codigo'=>$fila->codigo,
    				'marca_eva'=>$fila->marca_eva,
    				'capacidad_hp_eva'=>$fila->capacidad_hp_eva,
    				'modelo_eva'=>$fila->modelo_eva,
    				'serie_eva'=>$fila->serie_eva,
    				'marca_conde'=>$fila->marca_conde,
    				'capacidad_hp_conde'=>$fila->capacidad_hp_conde,
    				'modelo_conde'=>$fila->modelo_conde,
    				'serie_conde'=>$fila->serie_conde,
    				'edit'=>0
                );
			}
			$corRefri = "SELECT cor_caracteristicas_refrigeracion.*, cat_descripciones_equipos.nombre AS des, cat_areas.nombre AS are FROM cor_caracteristicas_refrigeracion
                JOIN orden_trabajo ON orden_trabajo.id = cor_caracteristicas_refrigeracion.id_orden
                JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = cor_caracteristicas_refrigeracion.id_equipo_des
                JOIN cat_areas ON cat_areas.id = cor_caracteristicas_refrigeracion.id_area
                WHERE orden_trabajo.id_cotizacion  = {$postdata->id_orden} AND cor_caracteristicas_refrigeracion.status=1
                and orden_trabajo.codigo='{$postdata->codigo}'";

			$corRefri = $this->conexion->link->query($corRefri);
			$response->corRefri = [];
			while($fila = $corRefri->fetch_assoc()){
                $fila = (object)$fila;
                $response->corRefri[] = array ('id'=>$fila->id,
                    'area'=>$fila->are,
                    'desequipo'=>$fila->des,
                    'codigo'=>$fila->codigo,
    				'marca_eva'=>$fila->marca_eva,
    				'capacidad_hp_eva'=>$fila->capacidad_hp_eva,
    				'modelo_eva'=>$fila->modelo_eva,
    				'serie_eva'=>$fila->serie_eva,
    				'marca_conde'=>$fila->marca_conde,
    				'capacidad_hp_conde'=>$fila->capacidad_hp_conde,
    				'modelo_conde'=>$fila->modelo_conde,
    				'serie_conde'=>$fila->serie_conde,
    				'edit'=>0
                );
			}

			$repuestos = "SELECT orden_trabajo_repuestos.* FROM orden_trabajo_repuestos
                    JOIN orden_trabajo
                    ON orden_trabajo.id = orden_trabajo_repuestos.id_orden
                    JOIN cotizaciones
                    ON  cotizaciones.id = orden_trabajo.id_cotizacion
                    WHERE cotizaciones.id ='{$postdata->id_orden}' and orden_trabajo_repuestos.status=1
					and orden_trabajo.codigo='{$postdata->codigo}'";
            $repuestos = $this->conexion->link->query($repuestos);
            $response->repuestos = [];
			while($fila = $repuestos->fetch_assoc()){
                $fila = (object)$fila;
                $response->repuestos[] = array ('id'=>$fila->id,
                    'codigo'=>$fila->codigo,
                    'parte'=>$fila->parte,
                    'item'=>$fila->item,
                    'descripcion'=>$fila->descripcion,
                    'cantidad'=>$fila->cantidad,
    				'edit'=>0
                );
            }

			$revRefri = "SELECT rev_caracteristicas_refrigeracion.*, cat_descripciones_equipos.nombre AS des,
    			cat_areas.nombre AS are FROM rev_caracteristicas_refrigeracion
    			JOIN orden_trabajo ON orden_trabajo.id = rev_caracteristicas_refrigeracion.id_orden
    			JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = rev_caracteristicas_refrigeracion.id_equipo_des
    			JOIN cat_areas ON cat_areas.id = rev_caracteristicas_refrigeracion.id_area
    			WHERE `rev_caracteristicas_refrigeracion`.`tipo`='CORRECTIVO' AND
    			orden_trabajo.id_cotizacion  = {$postdata->id_orden} AND rev_caracteristicas_refrigeracion.status=1
    			and orden_trabajo.codigo='{$postdata->codigo}'";

			$revRefri = $this->conexion->link->query($revRefri);
			$response->revRefri = [];
			while($fila = $revRefri->fetch_assoc()){
                $fila = (object)$fila;
                $response->revRefri[] = array ('id'=>$fila->id,
                'area'=>$fila->are,
                'desequipo'=>$fila->des,
                'codigo'=>$fila->codigo,
                'marca_eva'=>$fila->marca_eva,
                'capacidad_hp_eva'=>$fila->capacidad_hp_eva,
                'modelo_eva'=>$fila->modelo_eva,
                'serie_eva'=>$fila->serie_eva,
                'marca_conde'=>$fila->marca_conde,
                'capacidad_hp_conde'=>$fila->capacidad_hp_conde,
                'modelo_conde'=>$fila->modelo_conde,
                'serie_conde'=>$fila->serie_conde,
                'edit'=>0
                );
			}

            /*$equiposCliente = "SELECT cat_equipos.id, 
                                        cat_areas.nombre as area, 
                                        cat_descripciones_equipos.nombre AS des, cat_equipo.codigo,
                                        
                            FROM cat_equipos 
                            INNER JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = cat_equipos.id_descripcion_equipo
    			            INNER JOIN cat_areas ON cat_areas.id = cat_equipos.id_area _climatiza
                            WHERE id_cliente = '{$res->id_c}' AND status = 1";*/


			$herramientas2 = "SELECT * FROM cat_herramientas_trabajo WHERE STATUS = 1";

			$herramientas2 = $this->conexion->link->query($herramientas2);
			$response->herramientas2 = [];
			while($fila = $herramientas2->fetch_assoc()){
                $fila = (object)$fila;
                $response->herramientas2[] = array ('id'=>$fila->id,
                    'herramienta'=>$fila->herramienta,
    				'edit'=>0
                );
			}

			$materiales2 = "SELECT * FROM cat_materiales WHERE STATUS = 1";

			$materiales2 = $this->conexion->link->query($materiales2);
			$response->materiales2 = [];
			while($fila = $materiales2->fetch_assoc()){
                $fila = (object)$fila;
                $response->materiales2[] = array ('id'=>$fila->id,
                    'item'=>$fila->item.' - '.$fila->descripcion,
    				'edit'=>0
                );
			}

            $this->apiProntoforms->refreshMantenimiento();

			$response->equiposVal = $this->getEquiposVal($postdata->id_orden);

            $response->cliente = $this->getClients($res->id_cotizacion);
        }
        return json_encode($response);
    }

	public function getEquiposVal($id){
			$equiposVal = "SELECT cat_descripciones_equipos.nombre, ins_caracteristicas_climatizacion_a.codigo
				FROM ins_caracteristicas_climatizacion_a
				JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = ins_caracteristicas_climatizacion_a.descripcion
				JOIN orden_trabajo ON orden_trabajo.id = ins_caracteristicas_climatizacion_a.id_orden
				JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
				WHERE cotizaciones.id = {$id} AND ins_caracteristicas_climatizacion_a.status = 1";

			$equiposVal = $this->conexion->link->query($equiposVal);
			$requiposVal = [];
			while($fila = $equiposVal->fetch_assoc()){
                $fila = (object)$fila;
                $requiposVal[] = array ('nombre'=>$fila->nombre,
                    'codigo'=>$fila->codigo,
    				'dos'=>$fila->nombre." - ".$fila->codigo,
                );
			}
			$equiposVal = "SELECT cat_descripciones_equipos.nombre, ins_caracteristicas_climatizacion_b.codigo
    			FROM ins_caracteristicas_climatizacion_b
    			JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = ins_caracteristicas_climatizacion_b.descripcion
    			JOIN orden_trabajo ON orden_trabajo.id = ins_caracteristicas_climatizacion_b.id_orden
    			JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
    			WHERE cotizaciones.id = {$id} AND ins_caracteristicas_climatizacion_b.status = 1";

			$equiposVal = $this->conexion->link->query($equiposVal);
			while($fila = $equiposVal->fetch_assoc()){
                $fila = (object)$fila;
                $requiposVal[] = array ('nombre'=>$fila->nombre,
                    'codigo'=>$fila->codigo,
    				'dos'=>$fila->nombre." - ".$fila->codigo,
                );
			}
			$equiposVal = "SELECT cat_descripciones_equipos.nombre, ins_caracteristicas_ventilacion.codigo
    			FROM ins_caracteristicas_ventilacion
    			JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = ins_caracteristicas_ventilacion.descripcion
    			JOIN orden_trabajo ON orden_trabajo.id = ins_caracteristicas_ventilacion.id_orden
    			JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
    			WHERE cotizaciones.id = {$id} AND ins_caracteristicas_ventilacion.status = 1";

			$equiposVal = $this->conexion->link->query($equiposVal);
			while($fila = $equiposVal->fetch_assoc()){
                $fila = (object)$fila;
                $requiposVal[] = array ('nombre'=>$fila->nombre,
                    'codigo'=>$fila->codigo,
    				'dos'=>$fila->nombre." - ".$fila->codigo,
                );
			}
			$equiposVal = "SELECT cat_descripciones_equipos.nombre, ins_caracteristicas_refrigeracion.codigo
    			FROM ins_caracteristicas_refrigeracion
    			JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = ins_caracteristicas_refrigeracion.id_equipo_des
    			JOIN orden_trabajo ON orden_trabajo.id = ins_caracteristicas_refrigeracion.id_orden
    			JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
    			WHERE cotizaciones.id = {$id} AND ins_caracteristicas_refrigeracion.status = 1";

			$equiposVal = $this->conexion->link->query($equiposVal);
			while($fila = $equiposVal->fetch_assoc()){
                $fila = (object)$fila;
                $requiposVal[] = array ('nombre'=>$fila->nombre,
                    'codigo'=>$fila->codigo,
    				'dos'=>$fila->nombre." - ".$fila->codigo,
                );
			}
			$equiposVal = "SELECT cat_descripciones_equipos.nombre, cor_caracteristicas_climatizacion_a.codigo
				FROM cor_caracteristicas_climatizacion_a
				JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = cor_caracteristicas_climatizacion_a.descripcion
				JOIN orden_trabajo ON orden_trabajo.id = cor_caracteristicas_climatizacion_a.id_orden
				JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
				WHERE cotizaciones.id = {$id} AND cor_caracteristicas_climatizacion_a.status = 1";

			$equiposVal = $this->conexion->link->query($equiposVal);
			while($fila = $equiposVal->fetch_assoc()){
                $fila = (object)$fila;
                $requiposVal[] = array ('nombre'=>$fila->nombre,
                    'codigo'=>$fila->codigo,
    				'dos'=>$fila->nombre." - ".$fila->codigo,
                );
			}
			$equiposVal = "SELECT cat_descripciones_equipos.nombre, cor_caracteristicas_climatizacion_b.codigo
			FROM cor_caracteristicas_climatizacion_b
			JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = cor_caracteristicas_climatizacion_b.descripcion
			JOIN orden_trabajo ON orden_trabajo.id = cor_caracteristicas_climatizacion_b.id_orden
			JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
			WHERE cotizaciones.id = {$id} AND cor_caracteristicas_climatizacion_b.status = 1";

			$equiposVal = $this->conexion->link->query($equiposVal);
			while($fila = $equiposVal->fetch_assoc()){
                $fila = (object)$fila;
                $requiposVal[] = array ('nombre'=>$fila->nombre,
                    'codigo'=>$fila->codigo,
    				'dos'=>$fila->nombre." - ".$fila->codigo,
                );
			}
			$equiposVal = "SELECT cat_descripciones_equipos.nombre, cor_caracteristicas_ventilacion.codigo
    			FROM cor_caracteristicas_ventilacion
    			JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = cor_caracteristicas_ventilacion.descripcion
    			JOIN orden_trabajo ON orden_trabajo.id = cor_caracteristicas_ventilacion.id_orden
    			JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
    			WHERE cotizaciones.id = {$id} AND cor_caracteristicas_ventilacion.status = 1";

			$equiposVal = $this->conexion->link->query($equiposVal);
			while($fila = $equiposVal->fetch_assoc()){
                $fila = (object)$fila;
                $requiposVal[] = array ('nombre'=>$fila->nombre,
                    'codigo'=>$fila->codigo,
    				'dos'=>$fila->nombre." - ".$fila->codigo,
                );
			}
			$equiposVal = "SELECT cat_descripciones_equipos.nombre, cor_caracteristicas_refrigeracion.codigo
    			FROM cor_caracteristicas_refrigeracion
    			JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = cor_caracteristicas_refrigeracion.id_equipo_des
    			JOIN orden_trabajo ON orden_trabajo.id = cor_caracteristicas_refrigeracion.id_orden
    			JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
    			WHERE cotizaciones.id = {$id} AND cor_caracteristicas_refrigeracion.status = 1";

			$equiposVal = $this->conexion->link->query($equiposVal);
			while($fila = $equiposVal->fetch_assoc()){
                $fila = (object)$fila;
                $requiposVal[] = array ('nombre'=>$fila->nombre,
                    'codigo'=>$fila->codigo,
    				'dos'=>$fila->nombre." - ".$fila->codigo,
                );
			}
			$equiposVal = "SELECT cat_descripciones_equipos.nombre, man_caracteristicas_climatizacion_a.codigo
				FROM man_caracteristicas_climatizacion_a
				JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = man_caracteristicas_climatizacion_a.descripcion
				JOIN orden_trabajo ON orden_trabajo.id = man_caracteristicas_climatizacion_a.id_orden
				JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
				WHERE cotizaciones.id = {$id} AND man_caracteristicas_climatizacion_a.status = 1";

			$equiposVal = $this->conexion->link->query($equiposVal);
			while($fila = $equiposVal->fetch_assoc()){
            $fila = (object)$fila;
            $requiposVal[] = array ('nombre'=>$fila->nombre,
                'codigo'=>$fila->codigo,
				'dos'=>$fila->nombre." - ".$fila->codigo,
            );
			}
			$equiposVal = "SELECT cat_descripciones_equipos.nombre, man_caracteristicas_climatizacion_b.codigo
			FROM man_caracteristicas_climatizacion_b
			JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = man_caracteristicas_climatizacion_b.descripcion
			JOIN orden_trabajo ON orden_trabajo.id = man_caracteristicas_climatizacion_b.id_orden
			JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
			WHERE cotizaciones.id = {$id} AND man_caracteristicas_climatizacion_b.status = 1";

			$equiposVal = $this->conexion->link->query($equiposVal);
			while($fila = $equiposVal->fetch_assoc()){
            $fila = (object)$fila;
            $requiposVal[] = array ('nombre'=>$fila->nombre,
                'codigo'=>$fila->codigo,
				'dos'=>$fila->nombre." - ".$fila->codigo,
            );
			}
			$equiposVal = "SELECT cat_descripciones_equipos.nombre, man_caracteristicas_ventilacion.codigo
			FROM man_caracteristicas_ventilacion
			JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = man_caracteristicas_ventilacion.descripcion
			JOIN orden_trabajo ON orden_trabajo.id = man_caracteristicas_ventilacion.id_orden
			JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
			WHERE cotizaciones.id = {$id} AND man_caracteristicas_ventilacion.status = 1";

			$equiposVal = $this->conexion->link->query($equiposVal);
			while($fila = $equiposVal->fetch_assoc()){
            $fila = (object)$fila;
            $requiposVal[] = array ('nombre'=>$fila->nombre,
                'codigo'=>$fila->codigo,
				'dos'=>$fila->nombre." - ".$fila->codigo,
            );
			}
			$equiposVal = "SELECT cat_descripciones_equipos.nombre, man_caracteristicas_refrigeracion.codigo
			FROM man_caracteristicas_refrigeracion
			JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = man_caracteristicas_refrigeracion.id_equipo_des
			JOIN orden_trabajo ON orden_trabajo.id = man_caracteristicas_refrigeracion.id_orden
			JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
			WHERE cotizaciones.id = {$id} AND man_caracteristicas_refrigeracion.status = 1";

			$equiposVal = $this->conexion->link->query($equiposVal);
			while($fila = $equiposVal->fetch_assoc()){
            $fila = (object)$fila;
            $requiposVal[] = array ('nombre'=>$fila->nombre,
                'codigo'=>$fila->codigo,
				'dos'=>$fila->nombre." - ".$fila->codigo,
            );
			}
			$equiposVal = "SELECT cat_descripciones_equipos.nombre, rev_caracteristicas_climatizacion_a.codigo
				FROM rev_caracteristicas_climatizacion_a
				JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = rev_caracteristicas_climatizacion_a.descripcion
				JOIN orden_trabajo ON orden_trabajo.id = rev_caracteristicas_climatizacion_a.id_orden
				JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
				WHERE cotizaciones.id = {$id} AND rev_caracteristicas_climatizacion_a.status = 1";

			$equiposVal = $this->conexion->link->query($equiposVal);
			while($fila = $equiposVal->fetch_assoc()){
            $fila = (object)$fila;
            $requiposVal[] = array ('nombre'=>$fila->nombre,
                'codigo'=>$fila->codigo,
				'dos'=>$fila->nombre." - ".$fila->codigo,
            );
			}
			$equiposVal = "SELECT cat_descripciones_equipos.nombre, rev_caracteristicas_climatizacion_b.codigo
			FROM rev_caracteristicas_climatizacion_b
			JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = rev_caracteristicas_climatizacion_b.descripcion
			JOIN orden_trabajo ON orden_trabajo.id = rev_caracteristicas_climatizacion_b.id_orden
			JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
			WHERE cotizaciones.id = {$id} AND rev_caracteristicas_climatizacion_b.status = 1";

			$equiposVal = $this->conexion->link->query($equiposVal);
			while($fila = $equiposVal->fetch_assoc()){
            $fila = (object)$fila;
            $requiposVal[] = array ('nombre'=>$fila->nombre,
                'codigo'=>$fila->codigo,
				'dos'=>$fila->nombre." - ".$fila->codigo,
            );
			}
			$equiposVal = "SELECT cat_descripciones_equipos.nombre, rev_caracteristicas_ventilacion.codigo
			FROM rev_caracteristicas_ventilacion
			JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = rev_caracteristicas_ventilacion.descripcion
			JOIN orden_trabajo ON orden_trabajo.id = rev_caracteristicas_ventilacion.id_orden
			JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
			WHERE cotizaciones.id = {$id} AND rev_caracteristicas_ventilacion.status = 1";

			$equiposVal = $this->conexion->link->query($equiposVal);
			while($fila = $equiposVal->fetch_assoc()){
            $fila = (object)$fila;
            $requiposVal[] = array ('nombre'=>$fila->nombre,
                'codigo'=>$fila->codigo,
				'dos'=>$fila->nombre." - ".$fila->codigo,
            );
			}
			$equiposVal = "SELECT cat_descripciones_equipos.nombre, rev_caracteristicas_refrigeracion.codigo
			FROM rev_caracteristicas_refrigeracion
			JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = rev_caracteristicas_refrigeracion.id_equipo_des
			JOIN orden_trabajo ON orden_trabajo.id = rev_caracteristicas_refrigeracion.id_orden
			JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
			WHERE cotizaciones.id = {$id} AND rev_caracteristicas_refrigeracion.status = 1";

			$equiposVal = $this->conexion->link->query($equiposVal);
			while($fila = $equiposVal->fetch_assoc()){
                $fila = (object)$fila;
                $requiposVal[] = array ('nombre'=>$fila->nombre,
                    'codigo'=>$fila->codigo,
    				'dos'=>$fila->nombre." - ".$fila->codigo,
                );
			}
			$requiposVal = $this->limpiarArray($requiposVal);

			return $requiposVal;
	}

	public function limpiarArray($array){
        $retorno=null;
        if($array!=null){
            $retorno[0]=$array[0];
        }
        for($i=1;$i<count($array);$i++){
            $repetido=false;
            $elemento=$array[$i];
            for($j=0;$j<count($retorno) && !$repetido;$j++){
                if($elemento==$retorno[$j]){
                    $repetido=true;
                }
            }
            if(!$repetido){
                $retorno[]=$elemento;
            }
        }
        return $retorno;
    }

	public function addMaterial(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        if($postdata->id_cotizacion > 0){
            $sql = "SELECT orden_trabajo.* FROM orden_trabajo
                    INNER JOIN cotizaciones
                    ON orden_trabajo.id_cotizacion = cotizaciones.id
                    INNER JOIN cat_clientes
                    ON  cotizaciones.id_cliente = cat_clientes.id
                    WHERE cotizaciones.id ='{$postdata->id_cotizacion}' and orden_trabajo.codigo='{$postdata->codigoOrden}' ";
            $res = $this->conexion->link->query($sql);
            $res = $res->fetch_object();
			$sql = "SELECT * FROM cat_materiales where id = {$postdata->item}";
            $res2 = $this->conexion->link->query($sql);
            $res2 = $res2->fetch_object();
			$sql ='INSERT INTO orden_trabajo_materiales SET id_orden ="'.$res->id.'", seleccion="'.$res2->item.'", descripcion="'.$res2->descripcion.'",
			unidad="'.$res2->unidad.'", cantidad_requerida="'.$postdata->cantidad.'", codigo="'.$postdata->codigo.'";';
			//$response->sql = $sql;
			$this->conexion->link->query($sql);
            $materiales = "SELECT orden_trabajo_materiales.id, orden_trabajo_materiales.seleccion AS item, orden_trabajo_materiales.descripcion
                            AS descripcion_item, orden_trabajo_materiales.unidad, orden_trabajo_materiales.cantidad_requerida AS cantidad, orden_trabajo_materiales.codigo as codigo FROM orden_trabajo_materiales
                            JOIN orden_trabajo
                            ON orden_trabajo.id = orden_trabajo_materiales.id_orden
                            JOIN cotizaciones
                            ON  cotizaciones.id = orden_trabajo.id_cotizacion
                            WHERE cotizaciones.id ='{$postdata->id_cotizacion}' and orden_trabajo_materiales.status=1
                            and orden_trabajo.codigo='{$postdata->codigoOrden}'";
            $materiales = $this->conexion->link->query($materiales);
            $response->materiales = [];
			while($fila = $materiales->fetch_assoc()){
                $fila = (object)$fila;
                $response->materiales[] = array ('id'=>$fila->id,
                    'codigo'=>$fila->codigo,
                    'item'=>$fila->item,
                    'descripcion_item'=>$fila->descripcion_item,
                    'unidad'=>$fila->unidad,
                    'cantidad'=>$fila->cantidad,
    				'edit'=>0
                );
            }
        }
        /* REFRESH PRONTOFORMS */
        /*$isMantenimiento = $this->conexion->queryRow("SELECT id FROM orden_trabajo WHERE tipo_trabajo LIKE '%MANTENIMIENTO%' AND tipo_trabajo != 'REVISION MANTENIMIENTO' AND codigo = '{$postdata->codigoOrden}'");
        if($isMantenimiento){
            $this->apiProntoforms->refreshMantenimiento();
        }*/
        return json_encode($response);
    }

	public function addRepuesto(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        if($postdata->id_cotizacion > 0){
            $sql = "SELECT orden_trabajo.* FROM orden_trabajo
                    INNER JOIN cotizaciones
                    ON orden_trabajo.id_cotizacion = cotizaciones.id
                    INNER JOIN cat_clientes
                    ON  cotizaciones.id_cliente = cat_clientes.id
                    WHERE cotizaciones.id ='{$postdata->id_cotizacion}' and orden_trabajo.codigo='{$postdata->codigoOrden}'";
            $res = $this->conexion->link->query($sql);
            $res = $res->fetch_object();
			$sql ='INSERT INTO orden_trabajo_repuestos SET id_orden ="'.$res->id.'", codigo="'.$postdata->codigo.'", parte="'.$postdata->parte.'",
			item="'.$postdata->item.'", cantidad="'.$postdata->cantidad.'", descripcion="'.$postdata->descripcion.'";';
			//$response->sql = $sql;
			$this->conexion->link->query($sql);
            $repuestos = "SELECT orden_trabajo_repuestos.* FROM orden_trabajo_repuestos
                    JOIN orden_trabajo
                    ON orden_trabajo.id = orden_trabajo_repuestos.id_orden
                    JOIN cotizaciones
                    ON  cotizaciones.id = orden_trabajo.id_cotizacion
                    WHERE cotizaciones.id ='{$postdata->id_cotizacion}' and orden_trabajo_repuestos.status=1
					and orden_trabajo.codigo='{$postdata->codigoOrden}'";
            $repuestos = $this->conexion->link->query($repuestos);
            $response->repuestos = [];
			while($fila = $repuestos->fetch_assoc()){
                $fila = (object)$fila;
                $response->repuestos[] = array ('id'=>$fila->id,
                    'codigo'=>$fila->codigo,
                    'parte'=>$fila->parte,
                    'descripcion'=>$fila->descripcion,
                    'item'=>$fila->item,
                    'cantidad'=>$fila->cantidad,
                    'edit'=>0
                );
            }
        }
        /* REFRESH PRONTOFORMS */
        /*$isMantenimiento = $this->conexion->queryRow("SELECT id FROM orden_trabajo WHERE tipo_trabajo LIKE '%MANTENIMIENTO%' AND tipo_trabajo != 'REVISION MANTENIMIENTO' AND codigo = '{$postdata->codigoOrden}'");
        if($isMantenimiento){
            $this->apiProntoforms->refreshMantenimiento();
        }*/
        return json_encode($response);
    }

	public function agregarEquipoClimaA(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        if($postdata->id_cotizacion > 0){
			$sql = "SELECT orden_trabajo.* FROM orden_trabajo
                    INNER JOIN cotizaciones
                    ON orden_trabajo.id_cotizacion = cotizaciones.id
                    INNER JOIN cat_clientes
                    ON  cotizaciones.id_cliente = cat_clientes.id
                    WHERE cotizaciones.id ='{$postdata->id_cotizacion}'";
            $res2 = $this->conexion->link->query($sql);
            $res2 = $res2->fetch_object();
            $sql = "SELECT * FROM cat_equipos WHERE id ='{$postdata->equipo}'";
            $res = $this->conexion->link->query($sql);
            $res = $res->fetch_object();
			$datos = json_decode($res->data_piezas);
            // print_r($datos);
			$datosE=[];
			for($x=0; $x<count($datos); $x++){
    			if($datos[$x]->parte=='EVAPORADOR' || $datos[$x]->parte == 'EVAPORADOR / CONDENSADOR'){
        			$datosE=$datos[$x];
        			$x = count($datos)+1;
    			}
			}

            // $sql_insert = "";
			$sql_insert = "INSERT INTO {$postdata->table} SET id_orden = {$res2->id}, descripcion = '{$res->id_descripcion_equipo}'
			, codigo = '{$res->codigo}', marca = '{$datosE->marca}', capacidadBTU = '{$datosE->capacidad}', modelo = '{$datosE->modelo}'
			, serie = '{$datosE->serie}', area='{$res->id_area_climatiza}';";

            $response->insert = $this->conexion->link->query($sql_insert);

			$insclimA = "SELECT {$postdata->table}.*, cat_descripciones_equipos.nombre AS des, cat_areas.nombre AS are 
                        FROM {$postdata->table}
                        JOIN orden_trabajo ON orden_trabajo.id = {$postdata->table}.id_orden
                        JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = {$postdata->table}.descripcion
                        JOIN cat_areas ON cat_areas.id = {$postdata->table}.area
                        WHERE orden_trabajo.id_cotizacion  = {$postdata->id_cotizacion} AND {$postdata->table}.status = 1";
			$insclimA = $this->conexion->link->query($insclimA);
            
			$response->insclimA = [];
			while($fila = $insclimA->fetch_assoc()){
                $fila = (object)$fila;
                $response->insclimA[] = array ('id'=>$fila->id,
                    'area'=>$fila->are,
                    'desequipo'=>$fila->des,
                    'codigo'=>$fila->codigo,
                    'marca'=>$fila->marca,
                    'capacidadBTU'=>$fila->capacidadBTU,
                    'modelo'=>$fila->modelo,
                    'serie'=>$fila->serie,
    				'edit'=>0
                );
			}

        }
		$response->equiposVal = $this->getEquiposVal($postdata->id_cotizacion);
        /*if(strpos($postdata->table, "man_caracteristicas") !== false){
            $this->apiProntoforms->refreshMantenimiento();
        }*/
        return json_encode($response);
    }

	public function addHerramienta(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        if($postdata->id_cotizacion > 0){
            $sql = "SELECT orden_trabajo.* FROM orden_trabajo
                    INNER JOIN cotizaciones
                    ON orden_trabajo.id_cotizacion = cotizaciones.id
                    INNER JOIN cat_clientes
                    ON  cotizaciones.id_cliente = cat_clientes.id
                    WHERE cotizaciones.id ='{$postdata->id_cotizacion}' and orden_trabajo.codigo='{$postdata->codigoOrden}'";
            $res = $this->conexion->link->query($sql);
            $res = $res->fetch_object();
			$sql = "SELECT * from cat_herramientas_trabajo where id = {$postdata->herramienta}";
            $res2 = $this->conexion->link->query($sql);
            $res2 = $res2->fetch_object();
			$sql ="INSERT INTO orden_trabajo_herramientas SET id_orden ='{$res->id}', herramientas='{$res2->herramienta}', requerimientos='{$res2->especificacion}';";
			//$response->sql = $sql;
			$this->conexion->link->query($sql);
            $herramientas = "SELECT orden_trabajo_herramientas.* FROM orden_trabajo_herramientas
                    JOIN orden_trabajo
                    ON orden_trabajo.id = orden_trabajo_herramientas.id_orden
                    JOIN cotizaciones
                    ON  cotizaciones.id = orden_trabajo.id_cotizacion
                    WHERE cotizaciones.id ='{$postdata->id_cotizacion}' and orden_trabajo_herramientas.status=1
					and orden_trabajo.codigo='{$postdata->codigoOrden}'";
			$herramientas = $this->conexion->link->query($herramientas);
			$response->herramientas = [];
			while($fila = $herramientas->fetch_assoc()){
            $fila = (object)$fila;
            $response->herramientas[] = array ('id'=>$fila->id,
                'id_orden'=>$fila->id_orden,
                'herramienta'=>$fila->herramientas,
                'requerimientos'=>$fila->requerimientos,
				'edit'=>0
            );
			}

        }
        //$this->apiProntoforms->refreshMantenimiento();
        return json_encode($response);
    }

	public function addinsclimB(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        if($postdata->id_cotizacion > 0){
			$sql = "SELECT orden_trabajo.* FROM orden_trabajo
                    INNER JOIN cotizaciones
                    ON orden_trabajo.id_cotizacion = cotizaciones.id
                    INNER JOIN cat_clientes
                    ON  cotizaciones.id_cliente = cat_clientes.id
                    WHERE cotizaciones.id ='{$postdata->id_cotizacion}'";
            $res2 = $this->conexion->link->query($sql);
            $res2 = $res2->fetch_object();
            $sql = "SELECT * FROM cat_equipos WHERE id ='{$postdata->equipo}'";
            $res = $this->conexion->link->query($sql);
            $res = $res->fetch_object();
			$datos = json_decode($res->data_piezas);
			$datosE = [];
			$datosC = [];
			for($x=0; $x<count($datos); $x++){
                if($datos[$x]->parte=='EVAPORADOR'){
                    $datosE=$datos[$x];
                    $x = count($datos)+1;
                }
			}
			
            for($x=0; $x<count($datos); $x++){
                if($datos[$x]->parte=='CONDENSADOR'){
                    $datosC=$datos[$x];
                    $x = count($datos)+1;
                }
			}

			$sql = "INSERT INTO {$postdata->table} SET id_orden = {$res2->id}, id_equipo = '{$postdata->equipo}', descripcion = '{$res->id_descripcion_equipo}'
			, codigo = '{$res->codigo}', marca = '{$datosE->marca}', capacidadBTU = '{$datosE->capacidad}', modelo = '{$datosE->modelo}'
			, serie = '{$datosE->serie}', condensadorMarca = '{$datosC->marca}', condensadorBTU = '{$datosC->capacidad}', condensadorModelo = '{$datosC->modelo}'
			, condensadorSerie = '{$datosC->serie}', area='{$res->id_area_climatiza}'";
            $this->conexion->link->query($sql);
			$insclimB = "SELECT {$postdata->table}.*, cat_descripciones_equipos.nombre AS des, cat_areas.nombre AS are 
                FROM {$postdata->table}
                JOIN orden_trabajo ON orden_trabajo.id = {$postdata->table}.id_orden
                JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = {$postdata->table}.descripcion
                JOIN cat_areas ON cat_areas.id = {$postdata->table}.area
                WHERE orden_trabajo.id_cotizacion  = {$postdata->id_cotizacion} AND {$postdata->table}.status=1";
			$insclimB = $this->conexion->link->query($insclimB);
			$response->insclimB = [];
			while($fila = $insclimB->fetch_assoc()){
                $fila = (object)$fila;
                $response->insclimB[] = array (
                    'id'=>$fila->id,
                    'area'=>$fila->are,
                    'desequipo'=>$fila->des,
                    'codigo'=>$fila->codigo,
                    'marcaE'=>$fila->marca,
                    'capacidadBTUE'=>$fila->capacidadBTU,
                    'modeloE'=>$fila->modelo,
                    'serieE'=>$fila->serie,
                    'marcaC'=>$fila->condensadorMarca,
                    'capacidadBTUC'=>$fila->condensadorBTU,
                    'modeloC'=>$fila->condensadorModelo,
                    'serieC'=>$fila->condensadorSerie,
                    'edit'=>0
                );
			}

        }
		$response->equiposVal = $this->getEquiposVal($postdata->id_cotizacion);
        /*if(strpos($postdata->table, "man_caracteristicas") !== false){
            $this->apiProntoforms->refreshMantenimiento();
        }*/
        return json_encode($response);
    }

	public function addinsVenti(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        if($postdata->id_cotizacion > 0){
			$sql = "SELECT orden_trabajo.* FROM orden_trabajo
                    INNER JOIN cotizaciones
                    ON orden_trabajo.id_cotizacion = cotizaciones.id
                    INNER JOIN cat_clientes
                    ON  cotizaciones.id_cliente = cat_clientes.id
                    WHERE cotizaciones.id ='{$postdata->id_cotizacion}'";
            $res2 = $this->conexion->link->query($sql);
            $res2 = $res2->fetch_object();
            $sql = "SELECT * FROM cat_equipos WHERE id ='{$postdata->equipo}'";
            $res = $this->conexion->link->query($sql);
            $res = $res->fetch_object();
			$datos = json_decode($res->data_piezas);
            // print_r($datos);
			for($x=0; $x<count($datos); $x++){
			if($datos[$x]->parte!=''){
    			$datosC=$datos[$x];
    			$x = count($datos)+1;
			}

			}
			$sql = "INSERT INTO {$postdata->table} SET id_orden = {$res2->id}, descripcion = '{$res->id_descripcion_equipo}'
			, codigo = '{$res->codigo}', marca = '{$datosC->marca}', modelo = '{$datosC->modelo}', capacidadHP = '{$datosC->capacidad}'
			, serie = '{$datosC->serie}', area='{$res->id_area_climatiza}'";

            $this->conexion->link->query($sql);
			$insventi = "SELECT {$postdata->table}.* , {$postdata->table}.serie AS condensadorSerie ,{$postdata->table}.modelo AS condensadorModelo, cat_descripciones_equipos.nombre AS des, cat_areas.nombre AS are FROM {$postdata->table}
                JOIN orden_trabajo ON orden_trabajo.id = {$postdata->table}.id_orden
                JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = {$postdata->table}.descripcion
                JOIN cat_areas ON cat_areas.id = {$postdata->table}.area
                WHERE orden_trabajo.id_cotizacion  = {$postdata->id_cotizacion} AND {$postdata->table}.status=1";

			$insventi = $this->conexion->link->query($insventi);
			$response->insVentilacion = [];
			while($fila = $insventi->fetch_assoc()){
            $fila = (object)$fila;
            $response->insVentilacion[] = array ('id'=>$fila->id,
                'area'=>$fila->are,
                'desequipo'=>$fila->des,
                'codigo'=>$fila->codigo,
                'marca'=>$fila->marca,
                'capacidadHP'=>$fila->capacidadHP,
                'capacidadCFN'=>$fila->capacidadCFN,
                'modelo'=>$fila->modelo,
                'serie'=>$fila->serie,
				'edit'=>0
            );
			}

        }
		$response->equiposVal = $this->getEquiposVal($postdata->id_cotizacion);
        /*if(strpos($postdata->table, "man_caracteristicas") !== false){
            $this->apiProntoforms->refreshMantenimiento();
        }*/
        return json_encode($response);
    }

	public function addinsRefri(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;

        if($postdata->id_cotizacion > 0){

			$sql = "SELECT orden_trabajo.* FROM orden_trabajo
			INNER JOIN cotizaciones
			ON orden_trabajo.id_cotizacion = cotizaciones.id
			INNER JOIN cat_clientes
			ON  cotizaciones.id_cliente = cat_clientes.id
			WHERE cotizaciones.id ='{$postdata->id_cotizacion}'";
            $res2 = $this->conexion->link->query($sql);
            $res2 = $res2->fetch_object();

			$sql = "SELECT * FROM cat_equipos WHERE id ='{$postdata->equipo}'";
            $res = $this->conexion->link->query($sql);
            $res = $res->fetch_object();
            $datos = json_decode($res->data_piezas);

            $datosE = [];
            $datosC = [];
            for($x=0; $x<count($datos); $x++){
                if($datos[$x]->parte == 'EVAPORADOR'){
                    $datosE=$datos[$x];
                    $x = count($datos)+1;
                }
            }

            for($x=0; $x<count($datos); $x++){
                if($datos[$x]->parte=='CONDENSADOR'){
                    $datosC=$datos[$x];
                    $x = count($datos)+1;
                }
            }

			$sql = "INSERT INTO {$postdata->table} SET id_orden = {$res2->id},
    			id_area='{$res->id_area_climatiza}',id_equipo_des = '{$res->id_descripcion_equipo}',
                codigo = '{$res->codigo}',
                marca_eva = '{$datosE->marca}', modelo_eva = '{$datosE->modelo}', serie_eva = '{$datosE->serie}',
    			capacidad_hp_eva = '{$datosE->capacidad}',
                marca_conde = '{$datosC->marca}', modelo_conde = '{$datosC->modelo}', serie_conde = '{$datosC->serie}',
    			capacidad_hp_conde = '{$datosC->capacidad}'";
            $this->conexion->link->query($sql);

			$insRefri = "SELECT {$postdata->table}.*,
    			cat_descripciones_equipos.nombre AS des, cat_areas.nombre AS are FROM {$postdata->table}
    			JOIN orden_trabajo ON orden_trabajo.id = {$postdata->table}.id_orden
    			JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = {$postdata->table}.id_equipo_des
    			JOIN cat_areas ON cat_areas.id = {$postdata->table}.id_area
    			WHERE orden_trabajo.id_cotizacion  = {$postdata->id_cotizacion} AND {$postdata->table}.status=1";

			$insRefri = $this->conexion->link->query($insRefri);
			$response->insRefri = [];
			while($fila = $insRefri->fetch_assoc()){

            $fila = (object)$fila;
            $response->insRefri[] = array ('id'=>$fila->id,
                'area'=>$fila->are,
                'desequipo'=>$fila->des,
                'codigo'=>$fila->codigo,
				'marca_eva'=>$fila->marca_eva,
				'capacidad_hp_eva'=>$fila->capacidad_hp_eva,
				'modelo_eva'=>$fila->modelo_eva,
				'serie_eva'=>$fila->serie_eva,
				'marca_conde'=>$fila->marca_conde,
				'capacidad_hp_conde'=>$fila->capacidad_hp_conde,
				'modelo_conde'=>$fila->modelo_conde,
				'serie_conde'=>$fila->serie_conde,
				'edit'=>0
            );
			}

        }


		$response->equiposVal = $this->getEquiposVal($postdata->id_cotizacion);
        /*if(strpos($postdata->table, "man_caracteristicas") !== false){
            $this->apiProntoforms->refreshMantenimiento();
        }*/
		return json_encode($response);
    }

	public function editMaterial(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        if($postdata->id_cotizacion > 0){
            $sql ="UPDATE orden_trabajo_materiales SET cantidad_requerida='{$postdata->cantidad}' where id = {$postdata->id_material};";
			//$response->sql = $sql;
			$this->conexion->link->query($sql);
            $materiales = "SELECT orden_trabajo_materiales.id, orden_trabajo_materiales.seleccion AS item, orden_trabajo_materiales.descripcion
                            AS descripcion_item, orden_trabajo_materiales.unidad, orden_trabajo_materiales.cantidad_requerida AS cantidad, orden_trabajo_materiales.codigo as codigo FROM orden_trabajo_materiales
                            JOIN orden_trabajo
                            ON orden_trabajo.id = orden_trabajo_materiales.id_orden
                            JOIN cotizaciones
                            ON  cotizaciones.id = orden_trabajo.id_cotizacion
                            WHERE cotizaciones.id ='{$postdata->id_cotizacion}' and orden_trabajo_materiales.status=1
                            and orden_trabajo.codigo='{$postdata->codigoOrden}'";
            $materiales = $this->conexion->link->query($materiales);
            $response->materiales = [];
			while($fila = $materiales->fetch_assoc()){
            $fila = (object)$fila;
            $response->materiales[] = array ('id'=>$fila->id,
                'codigo'=>$fila->codigo,
                'item'=>$fila->item,
                'descripcion_item'=>$fila->descripcion_item,
                'unidad'=>$fila->unidad,
                'cantidad'=>$fila->cantidad,
				'edit'=>0
            );
        }

        }
        //$this->apiProntoforms->refreshMantenimiento();
        return json_encode($response);
    }

	public function editRepuesto(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        if($postdata->id > 0){
            $sql ="UPDATE orden_trabajo_repuestos SET cantidad='{$postdata->cantidad}' where id = {$postdata->id};";
			//$response->sql = $sql;
			$this->conexion->link->query($sql);
            $repuestos = "SELECT orden_trabajo_repuestos.* FROM orden_trabajo_repuestos
                    JOIN orden_trabajo
                    ON orden_trabajo.id = orden_trabajo_repuestos.id_orden
                    JOIN cotizaciones
                    ON  cotizaciones.id = orden_trabajo.id_cotizacion
                    WHERE cotizaciones.id ='{$postdata->id_cotizacion}' and orden_trabajo_repuestos.status=1
					and orden_trabajo.codigo='{$postdata->codigoOrden}'";
            $repuestos = $this->conexion->link->query($repuestos);
            $response->repuestos = [];
			while($fila = $repuestos->fetch_assoc()){
                $fila = (object)$fila;
                $response->repuestos[] = array ('id'=>$fila->id,
                    'codigo'=>$fila->codigo,
                    'parte'=>$fila->parte,
                    'descripcion'=>$fila->descripcion,
                    'item'=>$fila->item,
                    'cantidad'=>$fila->cantidad,
                    'edit'=>0
                );
            }
        }
        //$this->apiProntoforms->refreshMantenimiento();
        return json_encode($response);
    }

	public function editHerramienta(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        if($postdata->id_cotizacion > 0){
            $sql ="UPDATE orden_trabajo_herramientas SET herramientas='{$postdata->herramienta}', requerimientos='{$postdata->requerimientos}' where id = {$postdata->id_herramienta};";
			//$response->sql = $sql;
			$this->conexion->link->query($sql);
            $herramientas = "SELECT orden_trabajo_herramientas.* FROM orden_trabajo_herramientas
                    JOIN orden_trabajo
                    ON orden_trabajo.id = orden_trabajo_herramientas.id_orden
                    JOIN cotizaciones
                    ON  cotizaciones.id = orden_trabajo.id_cotizacion
                    WHERE cotizaciones.id ='{$postdata->id_cotizacion}' and orden_trabajo_herramientas.status=1
					and orden_trabajo.codigo='{$postdata->codigoOrden}'";
			$herramientas = $this->conexion->link->query($herramientas);
			$response->herramientas = [];
			while($fila = $herramientas->fetch_assoc()){
                $fila = (object)$fila;
                $response->herramientas[] = array ('id'=>$fila->id,
                    'id_orden'=>$fila->id_orden,
                    'herramienta'=>$fila->herramientas,
                    'requerimientos'=>$fila->requerimientos,
                    'edit'=>0
                );
			}
            //$this->apiProntoforms->refreshMantenimiento();
        }
        return json_encode($response);
    }

	public function deleteMaterial(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        if($postdata->id_material > 0){
			$sql ="UPDATE orden_trabajo_materiales SET status ='0' where id ='{$postdata->id_material}'";
			//$response->sql = $sql;
			$this->conexion->link->query($sql);
            $materiales = "SELECT orden_trabajo_materiales.id, orden_trabajo_materiales.seleccion AS item, orden_trabajo_materiales.descripcion
                            AS descripcion_item, orden_trabajo_materiales.unidad, orden_trabajo_materiales.cantidad_requerida AS cantidad, orden_trabajo_materiales.codigo as codigo FROM orden_trabajo_materiales
                            JOIN orden_trabajo
                            ON orden_trabajo.id = orden_trabajo_materiales.id_orden
                            JOIN cotizaciones
                            ON  cotizaciones.id = orden_trabajo.id_cotizacion
                            WHERE cotizaciones.id ='{$postdata->id_cotizacion}' and orden_trabajo_materiales.status=1
                            and orden_trabajo.codigo='{$postdata->codigoOrden}'";
            $materiales = $this->conexion->link->query($materiales);
            $response->materiales = [];
			while($fila = $materiales->fetch_assoc()){
                $fila = (object)$fila;
                $response->materiales[] = array ('id'=>$fila->id,
                    'codigo'=>$fila->codigo,
                    'item'=>$fila->item,
                    'descripcion_item'=>$fila->descripcion_item,
                    'unidad'=>$fila->unidad,
                    'cantidad'=>$fila->cantidad,
                    'edit'=>0
                );
            }
            //$this->apiProntoforms->refreshMantenimiento();
        }
        return json_encode($response);
    }

	public function deleteRepuesto(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        if($postdata->id > 0){
			$sql ="UPDATE orden_trabajo_repuestos SET status ='0' where id ='{$postdata->id}'";
			//$response->sql = $sql;
			$this->conexion->link->query($sql);
            $repuestos = "SELECT orden_trabajo_repuestos.* FROM orden_trabajo_repuestos
                    JOIN orden_trabajo
                    ON orden_trabajo.id = orden_trabajo_repuestos.id_orden
                    JOIN cotizaciones
                    ON  cotizaciones.id = orden_trabajo.id_cotizacion
                    WHERE cotizaciones.id ='{$postdata->id_cotizacion}' and orden_trabajo_repuestos.status=1
					and orden_trabajo.codigo='{$postdata->codigoOrden}'";
            $repuestos = $this->conexion->link->query($repuestos);
            $response->repuestos = [];
			while($fila = $repuestos->fetch_assoc()){
                $fila = (object)$fila;
                $response->repuestos[] = array ('id'=>$fila->id,
                    'codigo'=>$fila->codigo,
                    'parte'=>$fila->parte,
                    'descripcion'=>$fila->descripcion,
                    'item'=>$fila->item,
                    'cantidad'=>$fila->cantidad,
                    'edit'=>0
                );
            }
            //$this->apiProntoforms->refreshMantenimiento();
        }
        return json_encode($response);
    }

	public function calendario(){
        $response->calendario = [];
        
		if($_POST['grupo'] == 0 && $_POST["tipo"] == ''){
            $sql = "SELECT 
                        orden_trabajo.id AS id_table_orden,
                        orden_trabajo.*, 
                        cat_vehiculos.nombre AS vehiculo,
                        cat_clientes.nombre AS nomCli, 
                        IF(
                            (SELECT tipo FROM orden_trabajo_responsables WHERE id = orden_trabajo.id) = 'Supervisor',
                            (SELECT color FROM cat_supervisores WHERE id = (SELECT id_responsable FROM orden_trabajo_responsables WHERE id_orden = orden_trabajo.id)),
                            (SELECT color FROM cat_auxiliares WHERE id = (SELECT id_responsable FROM orden_trabajo_responsables WHERE id_orden = orden_trabajo.id AND tipo = 'Asistente'))
                        ) as color,
                        orden_trabajo.observaciones AS detalle_trabajo
                    FROM orden_trabajo
                    LEFT JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
                    LEFT JOIN cat_clientes ON cat_clientes.id = cotizaciones.id_cliente  
                    LEFT JOIN cat_vehiculos ON cat_vehiculos.id = orden_trabajo.id_vehiculo
                    WHERE orden_trabajo.status = 2";

        } else if($_POST['grupo'] == 0 && $_POST["tipo"] != ''){
            $sql = "SELECT 
                        orden_trabajo.id AS id_table_orden,
                        orden_trabajo.*, 
                        cat_vehiculos.nombre AS vehiculo,
                        cat_clientes.nombre AS nomCli, 
                        IF(
                            (SELECT tipo FROM orden_trabajo_responsables WHERE id = orden_trabajo.id) = 'Supervisor',
                            (SELECT color FROM cat_supervisores WHERE id = (SELECT id_responsable FROM orden_trabajo_responsables WHERE id_orden = orden_trabajo.id)),
                            (SELECT color FROM cat_auxiliares WHERE id = (SELECT id_responsable FROM orden_trabajo_responsables WHERE id_orden = orden_trabajo.id AND tipo = 'Asistente'))
                        ) as color,
                        orden_trabajo.observaciones AS detalle_trabajo
                    FROM orden_trabajo
                    LEFT JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
                    LEFT JOIN cat_clientes ON cat_clientes.id = cotizaciones.id_cliente  
                    LEFT JOIN cat_vehiculos ON cat_vehiculos.id = orden_trabajo.id_vehiculo
                    WHERE orden_trabajo.status = 2 AND (SELECT tipo FROM orden_trabajo_responsables WHERE id_orden = orden_trabajo.id) = '{$_POST["tipo"]}'";
        
        } else if($responsable["tipo"] == "Supervisor"){
                $sql = "SELECT 
                            orden_trabajo.id AS id_table_orden,
                            orden_trabajo.*, 
                            cat_vehiculos.nombre AS vehiculo,
                            cat_clientes.nombre AS nomCli, 
                            (SELECT color FROM cat_supervisores WHERE id = '{$_POST["grupo"]}') as color,
                            orden_trabajo.observaciones AS detalle_trabajo
                        FROM orden_trabajo
                        LEFT JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
                        LEFT JOIN cat_clientes ON cat_clientes.id = cotizaciones.id_cliente  
                        LEFT JOIN orden_trabajo_responsables ON orden_trabajo_responsables.id_orden = orden_trabajo.id
                        LEFT JOIN cat_vehiculos ON cat_vehiculos.id = orden_trabajo.id_vehiculo
                        WHERE id_responsable = '{$_POST["grupo"]}' AND tipo = '{$_POST["tipo"]}' AND orden_trabajo.status = 2";
        } else {
                $sql = "SELECT 
                            orden_trabajo.id AS id_table_orden,
                            orden_trabajo.*, 
                            cat_vehiculos.nombre AS vehiculo,
                            cat_clientes.nombre AS nomCli, 
                            (SELECT color FROM cat_auxiliares WHERE id = '{$_POST["grupo"]}') as color,
                            orden_trabajo.observaciones AS detalle_trabajo
                        FROM orden_trabajo
                        LEFT JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
                        LEFT JOIN cat_clientes ON cat_clientes.id = cotizaciones.id_cliente  
                        LEFT JOIN orden_trabajo_responsables ON orden_trabajo_responsables.id_orden = orden_trabajo.id
                        LEFT JOIN cat_vehiculos ON cat_vehiculos.id = orden_trabajo.id_vehiculo
                        WHERE id_responsable = '{$_POST["grupo"]}' AND tipo = '{$_POST["tipo"]}' AND orden_trabajo.status = 2";
        }
        $res = $this->conexion->queryAll($sql);
        foreach($res as $fila){
            
            $auxiliares = explode(',', $fila->ids_auxiliares);
            $aux = "";
            foreach($auxiliares as $id){
                if($id > 0){
                    $sql = "SELECT nombre FROM cat_auxiliares WHERE id = '{$id}'";
                    $aux .= $this->conexion->queryRow($sql)->nombre. " / ";
                }
            }

            $sql = "
                    #REVISIONES
                    (SELECT 'REVISION' AS tipo_trabajo, CONCAT('REVISION - ', id_orden) AS tipo_id_orden, CONCAT(rev_caracteristicas_climatizacion_a.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.`nombre`,' - ',rev_caracteristicas_climatizacion_a.codigo,' - ',IF(marca IS NULL, '', marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM rev_caracteristicas_climatizacion_a LEFT JOIN cat_areas ON AREA = cat_areas.id LEFT JOIN orden_trabajo ON id_orden = orden_trabajo.id LEFT JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` LEFT JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE orden_trabajo.id = '{$fila->id}' AND rev_caracteristicas_climatizacion_a.status = 1 HAVING reg IS NOT NULL) 
                    UNION
                    (SELECT 'REVISION' AS tipo_trabajo, CONCAT('REVISION - ', id_orden) AS tipo_id_orden, CONCAT(rev_caracteristicas_climatizacion_b.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.`nombre`,' - ',rev_caracteristicas_climatizacion_b.codigo,' - ',IF(marca IS NULL, '', marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM rev_caracteristicas_climatizacion_b LEFT JOIN cat_areas ON AREA = cat_areas.id LEFT JOIN orden_trabajo ON id_orden = orden_trabajo.id LEFT JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` LEFT JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE orden_trabajo.id = '{$fila->id}' AND rev_caracteristicas_climatizacion_b.status = 1 HAVING reg IS NOT NULL) 
                    UNION ALL
                    (SELECT 'REVISION' AS tipo_trabajo, CONCAT('REVISION - ', id_orden) AS tipo_id_orden, CONCAT(rev_caracteristicas_refrigeracion.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.`nombre`,' - ',rev_caracteristicas_refrigeracion.codigo,' - ',IF(marca_eva IS NULL, '', marca_eva),' - ',IF(capacidad_hp_eva IS NULL,'',capacidad_hp_eva),' - ',IF(modelo_eva IS NULL,'',modelo_eva),' - ',IF(serie_eva IS NULL,'',serie_eva), IF(marca_conde IS NULL, '', marca_conde),' - ',IF(capacidad_hp_conde IS NULL,'',capacidad_hp_conde),' - ',IF(modelo_conde IS NULL,'',modelo_conde),' - ',IF(serie_conde IS NULL,'',serie_conde)) AS reg FROM rev_caracteristicas_refrigeracion LEFT JOIN cat_areas ON id_area = cat_areas.id LEFT JOIN orden_trabajo ON id_orden = orden_trabajo.id  LEFT JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` LEFT JOIN cat_descripciones_equipos ON id_equipo_des = cat_descripciones_equipos.`id`  WHERE orden_trabajo.id = '{$fila->id}' AND rev_caracteristicas_refrigeracion.status = 1 HAVING reg IS NOT NULL) 
                    UNION ALL
                    (SELECT 'REVISION' AS tipo_trabajo, CONCAT('REVISION - ', id_orden) AS tipo_id_orden, CONCAT(rev_caracteristicas_ventilacion.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.`nombre`,' - ',rev_caracteristicas_ventilacion.codigo,' - ',IF(marca IS NULL, '', marca),' - ',IF(capacidadHP IS NULL,'',capacidadHP),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM rev_caracteristicas_ventilacion LEFT JOIN cat_areas ON AREA = cat_areas.id LEFT JOIN orden_trabajo ON id_orden = orden_trabajo.id LEFT JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` LEFT JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE orden_trabajo.id = '{$fila->id}' AND rev_caracteristicas_ventilacion.status = 1 HAVING reg IS NOT NULL) 
                    UNION
                    #mantenimiento
                    (SELECT 'MANTENIMIENTO' AS tipo_trabajo, CONCAT('MANTENIMIENTO - ', id_orden) AS tipo_id_orden, CONCAT(man_caracteristicas_climatizacion_a.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.`nombre`,' - ',man_caracteristicas_climatizacion_a.codigo,' - ',IF(marca IS NULL, '', marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM man_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE orden_trabajo.id = '{$fila->id}' AND man_caracteristicas_climatizacion_a.status = 1 HAVING reg IS NOT NULL) 
                    UNION 
                    (SELECT 'MANTENIMIENTO' AS tipo_trabajo, CONCAT('MANTENIMIENTO - ', id_orden) AS tipo_id_orden, CONCAT(man_caracteristicas_climatizacion_b.`codigo`, '-', id_cliente) AS codigo_cliente,  id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.`nombre`,' - ',man_caracteristicas_climatizacion_b.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM man_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE orden_trabajo.id = '{$fila->id}' AND man_caracteristicas_climatizacion_b.status = 1 HAVING reg IS NOT NULL)
                    UNION
                    (SELECT 'MANTENIMIENTO' AS tipo_trabajo, CONCAT('MANTENIMIENTO - ', id_orden) AS tipo_id_orden, CONCAT(man_caracteristicas_refrigeracion.`codigo`, '-', id_cliente) AS codigo_cliente,  id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.`nombre`,' - ',man_caracteristicas_refrigeracion.codigo,' - ',IF(marca_eva IS NULL,'',marca_eva),' - ',IF(capacidad_hp_eva IS NULL,'',capacidad_hp_eva),' - ',IF(modelo_eva IS NULL,'',modelo_eva),' - ',IF(serie_eva IS NULL,'',serie_eva)) AS reg FROM man_caracteristicas_refrigeracion INNER JOIN cat_areas ON id_area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON id_equipo_des = cat_descripciones_equipos.`id` WHERE orden_trabajo.id = '{$fila->id}' AND man_caracteristicas_refrigeracion.status = 1 HAVING reg IS NOT NULL)
                    UNION
                    (SELECT 'MANTENIMIENTO' AS tipo_trabajo, CONCAT('MANTENIMIENTO - ', id_orden) AS tipo_id_orden, CONCAT(man_caracteristicas_ventilacion.`codigo`, '-', id_cliente) AS codigo_cliente,  id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.`nombre`,' - ',man_caracteristicas_ventilacion.codigo,' - ',IF(marca,'',marca),' - ',IF(capacidadHP IS NULL,'',capacidadHP),' - ',IF(capacidadCFN IS NULL,'',capacidadCFN),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM man_caracteristicas_ventilacion INNER JOIN cat_areas ON area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE orden_trabajo.id = '{$fila->id}' AND  man_caracteristicas_ventilacion.status = 1 HAVING reg IS NOT NULL)
                    #instalacion
                    UNION
                    (SELECT 'INSTALACION' AS tipo_trabajo, CONCAT('INSTALACION - ', id_orden) AS tipo_id_orden, CONCAT(ins_caracteristicas_climatizacion_a.`codigo`, '-', id_cliente) AS codigo_cliente,  id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.`nombre`,' - ',ins_caracteristicas_climatizacion_a.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie,'',serie)) AS reg FROM ins_caracteristicas_climatizacion_a INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE orden_trabajo.id = '{$fila->id}' AND ins_caracteristicas_climatizacion_a.status = 1 HAVING reg IS NOT NULL) 
                    UNION 
                    (SELECT 'INSTALACION' AS tipo_trabajo, CONCAT('INSTALACION - ', id_orden) AS tipo_id_orden,  CONCAT(ins_caracteristicas_climatizacion_b.`codigo`, '-', id_cliente) AS codigo_cliente,  id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.`nombre`,' - ',ins_caracteristicas_climatizacion_b.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM ins_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = AREA INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE orden_trabajo.id = '{$fila->id}' AND ins_caracteristicas_climatizacion_b.status = 1 HAVING reg IS NOT NULL)
                    UNION
                    (SELECT 'INSTALACION' AS tipo_trabajo, CONCAT('INSTALACION - ', id_orden) AS tipo_id_orden,  CONCAT(ins_caracteristicas_refrigeracion.`codigo`, '-', id_cliente) AS codigo_cliente,  id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.`nombre`,' - ',ins_caracteristicas_refrigeracion.codigo,' - ',IF(marca_eva IS NULL,'',marca_eva),' - ',IF(modelo_eva IS NULL,'',modelo_eva),' - ',IF(serie_eva IS NULL,'',serie_eva),' - ') AS reg FROM ins_caracteristicas_refrigeracion INNER JOIN cat_areas ON id_area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON id_equipo_des = cat_descripciones_equipos.`id` WHERE orden_trabajo.id = '{$fila->id}' AND ins_caracteristicas_refrigeracion.status = 1 HAVING reg IS NOT NULL)
                    UNION
                    (SELECT 'INSTALACION' AS tipo_trabajo, CONCAT('INSTALACION - ', id_orden) AS tipo_id_orden,  CONCAT(ins_caracteristicas_ventilacion.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.`nombre`,' - ',ins_caracteristicas_ventilacion.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadHP IS NULL,'',capacidadHP),' - ',IF(capacidadCFN IS NULL,'',capacidadCFN),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM ins_caracteristicas_ventilacion INNER JOIN cat_areas ON area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE orden_trabajo.id = '{$fila->id}' AND ins_caracteristicas_ventilacion.status = 1 HAVING reg IS NOT NULL)
                    #correctivo
                    UNION
                    (SELECT 'CORRECTIVO' AS tipo_trabajo, CONCAT('CORRECTIVO - ', id_orden) AS tipo_id_orden, CONCAT(cor_caracteristicas_climatizacion_a.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.nombre,' - ',cor_caracteristicas_climatizacion_a.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM cor_caracteristicas_climatizacion_a INNER JOIN cat_areas ON area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE orden_trabajo.id = '{$fila->id}' AND cor_caracteristicas_climatizacion_a.status = 1 HAVING reg IS NOT NULL) 
                    UNION 
                    (SELECT 'CORRECTIVO' AS tipo_trabajo, CONCAT('CORRECTIVO - ', id_orden) AS tipo_id_orden, CONCAT(cor_caracteristicas_climatizacion_b.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.`nombre`,' - ',cat_descripciones_equipos.nombre,' - ',cor_caracteristicas_climatizacion_b.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadBTU IS NULL,'',capacidadBTU),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM cor_caracteristicas_climatizacion_b INNER JOIN cat_areas ON cat_areas.id = area INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE orden_trabajo.id = '{$fila->id}' AND cor_caracteristicas_climatizacion_b.status = 1 HAVING reg IS NOT NULL)
                    UNION
                    (SELECT 'CORRECTIVO' AS tipo_trabajo, CONCAT('CORRECTIVO - ', id_orden) AS tipo_id_orden, CONCAT(cor_caracteristicas_refrigeracion.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.nombre,' - ',cor_caracteristicas_refrigeracion.`codigo`,' - ',IF(marca_eva IS NULL,'',marca_eva),' - ',IF(modelo_eva IS NULL,'',modelo_eva),' - ',IF(serie_eva IS NULL,'',serie_eva),' - ') AS reg FROM cor_caracteristicas_refrigeracion INNER JOIN cat_areas ON id_area = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON id_equipo_des = cat_descripciones_equipos.`id` WHERE orden_trabajo.id = '{$fila->id}' AND cor_caracteristicas_refrigeracion.status = 1 HAVING reg IS NOT NULL)
                    UNION
                    (SELECT 'CORRECTIVO' AS tipo_trabajo, CONCAT('CORRECTIVO - ', id_orden) AS tipo_id_orden, CONCAT(cor_caracteristicas_ventilacion.`codigo`, '-', id_cliente) AS codigo_cliente, id_orden, CONCAT(cat_areas.nombre,' - ',cat_descripciones_equipos.nombre,' - ',cor_caracteristicas_ventilacion.codigo,' - ',IF(marca IS NULL,'',marca),' - ',IF(capacidadHP IS NULL,'',capacidadHP),' - ',IF(capacidadCFN IS NULL,'',capacidadHP),' - ',IF(modelo IS NULL,'',modelo),' - ',IF(serie IS NULL,'',serie)) AS reg FROM cor_caracteristicas_ventilacion INNER JOIN cat_areas ON AREA = cat_areas.id INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.`id` INNER JOIN cat_descripciones_equipos ON descripcion = cat_descripciones_equipos.`id` WHERE orden_trabajo.id = '{$fila->id}' AND cor_caracteristicas_ventilacion.status = 1 HAVING reg IS NOT NULL)
                ";
            $result = $this->conexion->queryAll($sql);
            foreach($result as $row){
                $fila->list_equipos .= "<br>".$row->reg;
            }

            $sql = "SELECT 
                        cat_equipos.`codigo`, 
                        cotizaciones.`id_cliente`, 
                        CONCAT(cat_equipos.`codigo`, '-', cotizaciones.`id_cliente`) AS codigo_cliente, 
                        CONCAT(cat_equipos.codigo,' - ',seleccion,' - ',descripcion,' - ',unidad,' - ',cantidad_requerida) AS descripcion, 
                        id_orden  
                    FROM orden_trabajo_materiales 
                    INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id 
                    INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id
                    INNER JOIN cat_equipos ON orden_trabajo_materiales.`codigo` = cat_equipos.`codigo`
                    WHERE orden_trabajo_materiales.id_orden = '{$fila->id}' AND cat_equipos.`id_cliente` = cotizaciones.`id_cliente` AND orden_trabajo_materiales.status = 1
                    HAVING descripcion IS NOT NULL";
            $result = $this->conexion->queryAll($sql);
            foreach($result as $row){
                $fila->list_materiales .= "<br>".$row->descripcion;
            }


            $sql = "SELECT 
                        cat_equipos.`codigo`,
                        cotizaciones.`id_cliente`, 
                        CONCAT(cat_equipos.`codigo`, '-', cotizaciones.`id_cliente`) AS codigo_cliente,
                        CONCAT(cat_equipos.codigo,' - ',parte,' - ',descripcion,' - ',item,' - ',cantidad) AS descripcion, id_orden 
                    FROM orden_trabajo_repuestos 
                    INNER JOIN orden_trabajo ON id_orden = orden_trabajo.id 
                    INNER JOIN cotizaciones ON id_cotizacion = cotizaciones.id
                    INNER JOIN cat_equipos ON orden_trabajo_repuestos.`codigo` = cat_equipos.`codigo`
                    WHERE orden_trabajo_repuestos.id_orden = '{$fila->id}' AND cat_equipos.`id_cliente` = cotizaciones.`id_cliente` AND orden_trabajo_repuestos.status = 1
                    HAVING descripcion IS NOT NULL";
            $result = $this->conexion->queryAll($sql);
            foreach($result as $row){
                $fila->list_repuestos .= "<br>".$row->descripcion;
            }


            $sql = "SELECT 
                        CONCAT(herramientas,' - ',requerimientos) AS herramientas, 
                        id_orden 
                    FROM orden_trabajo_herramientas 
                    WHERE id_orden = '{$fila->id}' AND orden_trabajo_herramientas.status = 1 HAVING herramientas IS NOT NULL";
            $result = $this->conexion->queryAll($sql);
            foreach($result as $row){
                $fila->list_herramientas .= "<br>".$row->herramientas;
            }
            
            $response->calendario[] = array (
                'id'=>$fila->codigo.' - '.$fila->nomCli,
                'fecha_agendada'=>$fila->fecha_agendada,
				'tipo_trabajo'=>$fila->tipo_trabajo,
				'cliente'=>$fila->nomCli,
				'fecha_fin'=>$fila->fecha_fin,
				'grupo_trabajo'=>$fila->grupo_trabajo,
                'observaciones'=>$fila->observaciones == null ? '' : $fila->observaciones,
                'vehiculo'=>$fila->vehiculo,
                'supervisor'=>$fila->responsable,
                'auxiliares'=>$aux,
                'color'=>$fila->color,
                'detalle_trabajo'=>$fila->detalle_trabajo == null ? '' : $fila->detalle_trabajo,
                'list_equipos'=>$fila->list_equipos == null ? 'SIN ASIGNAR' : $fila->list_equipos,
                'list_materiales'=>$fila->list_materiales == null ? 'SIN ASIGNAR' : $fila->list_materiales,
                'list_repuestos'=>$fila->list_repuestos == null ? 'SIN ASIGNAR' : $fila->list_repuestos,
                'list_herramientas'=>$fila->list_herramientas == null ? 'SIN ASIGNAR' : $fila->list_herramientas
            );   
        }
        return json_encode($response);
    }

	public function editCalendar(){

		if($_POST['grupo']==0){
            $sql ="UPDATE orden_trabajo SET responsable = '{$_POST['supervisor']}', fecha_agendada='{$_POST['fecha']}', fecha_fin= ADDTIME('{$_POST['fecha']}', tiempo_estimado), status = 2, ids_auxiliares = '{$_POST['ids_auxiliares']}', id_vehiculo = '{$_POST['id_vehiculo']}' where codigo = '{$_POST['id']}';";

            if($this->conexion->link->query("SELECT * FROM orden_trabajo_responsables WHERE (SELECT codigo FROM orden_trabajo WHERE id = id_orden) = '{$_POST['id']}' ")->fetch_assoc()){
                $sql2 = "UPDATE orden_trabajo_responsables SET responsable = '{$_POST['supervisor']}' 
                        WHERE (SELECT codigo FROM orden_trabajo WHERE id = id_orden) = '{$_POST['id']}'";
                $this->conexion->link->query($sql2);
            }else{
                $sql2 = "UPDATE orden_trabajo_responsables SET responsable = '{$_POST['supervisor']}' 
                        WHERE (SELECT codigo FROM orden_trabajo WHERE id = id_orden) = '{$_POST['id']}'";
                $this->conexion->link->query($sql2);
            }
        }
		else{
            $sql ="UPDATE orden_trabajo SET responsable = '{$_POST['supervisor']}', grupo_trabajo = '{$_POST['grupo']}', fecha_agendada='{$_POST['fecha']}', fecha_fin= ADDTIME('{$_POST['fecha']}', tiempo_estimado), status = 2, ids_auxiliares = '{$_POST['ids_auxiliares']}', id_vehiculo = '{$_POST['id_vehiculo']}' where codigo = '{$_POST['id']}';";

            #tabla orden_trabajo_responsable ya esta creada?
            $res = $this->conexion->link->query("SELECT * FROM orden_trabajo_responsables WHERE (SELECT codigo FROM orden_trabajo WHERE id = id_orden) = '{$_POST['id']}' ");
            if($responsable = $res->fetch_assoc()){
                #actualizar responsable (Supervisor o Asistente)
                $sql2 = "UPDATE orden_trabajo_responsables SET responsable = '{$_POST['supervisor']}', id_responsable = '{$_POST['grupo']}', tipo = '{$_POST['tipo_supervisor']}', id_orden = (SELECT id FROM orden_trabajo where codigo = '{$_POST['id']}')
                        WHERE (SELECT codigo FROM orden_trabajo WHERE id = id_orden) = '{$_POST['id']}'";
                $this->conexion->Consultas(1, $sql2);
            }else{
                #actualizar responsable (Supervisor o Asistente)
                $sql2 = "INSERT INTO orden_trabajo_responsables SET responsable = '{$_POST['supervisor']}', id_responsable = '{$_POST['grupo']}', tipo = '{$_POST['tipo_supervisor']}', id_orden = (SELECT id FROM orden_trabajo where codigo = '{$_POST['id']}')";
                $this->conexion->Consultas(1, $sql2);
            }
        }

		$this->conexion->link->query($sql);
        $response->data = $sql;
        
        /* REFRESH PRONTOFORMS */
        /*$isMantenimiento = $this->conexion->queryRow("SELECT id FROM orden_trabajo WHERE tipo_trabajo LIKE '%MANTENIMIENTO%' AND tipo_trabajo != 'REVISION MANTENIMIENTO' AND codigo = '{$_POST['id']}'");
        if($isMantenimiento){
            $this->apiProntoforms->refreshMantenimiento();
        }*/

        return json_encode($response);
    }

	public function removeCalendar(){
        $sql ="UPDATE orden_trabajo SET fecha_agendada='', fecha_fin= '', status = 1, grupo_trabajo=0 where codigo = '{$_POST['id']}';";
        $this->conexion->link->query($sql);
        $response->data = $sql;
        
        /* REFRESH PRONTOFORMS */
        $isMantenimiento = $this->conexion->queryRow("SELECT id FROM orden_trabajo WHERE tipo_trabajo LIKE '%MANTENIMIENTO%' AND tipo_trabajo != 'REVISION MANTENIMIENTO' AND codigo = '{$_POST['id']}'");
        if($isMantenimiento){
            $this->apiProntoforms->refreshMantenimiento();
        }

        return json_encode($response);
    }

	public function changeEquipo(){
        $postdata = (object)json_decode(file_get_contents("php://input"));

        $sql = "SELECT cat_equiposPartes.* 
            FROM cat_equiposPartes
            JOIN cat_equipos ON cat_equipos.id_tipo_equipo = cat_equiposPartes.id_tipo_equipo
            WHERE cat_equiposPartes.status = 1 AND cat_equipos.codigo = '$postdata->codigo' GROUP BY id;";
        $res = $this->conexion->link->query($sql);
        $datos = array();
        while($fila = $res->fetch_assoc()){
                $datos[] = $fila;
        }
        return json_encode($datos);
    }

	public function getPiezas(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $datos = array();

        $sql = "SELECT cat_piezas.* 
            FROM cat_piezas
            JOIN cat_equipos ON cat_piezas.id_tipoequipo = cat_equipos.id_tipo_equipo
            WHERE cat_piezas.status = 1 AND cat_equipos.codigo = '$postdata->codigo' AND id_parte_equipo = $postdata->id_parte
            GROUP BY cat_piezas.descripcion;";
        $res = $this->conexion->link->query($sql);

        while($fila = $res->fetch_assoc()){
                $datos[] = $fila;
        }
        return json_encode($datos);
    }

	public function calendarioEvent(){
        $calendario = "SELECT orden_trabajo.codigo, cat_clientes.nombre, orden_trabajo.id as id_order, orden_trabajo.observaciones
                        FROM orden_trabajo
                        JOIN cotizaciones ON cotizaciones.id = orden_trabajo.id_cotizacion
                        JOIN cat_clientes ON cat_clientes.id = cotizaciones.id_cliente
                        WHERE orden_trabajo.status = 1 AND orden_trabajo.observaciones != ''
                        ORDER BY cotizaciones.id";
        $calendario = $this->conexion->link->query($calendario);
        $response->calendario = [];
        while($fila = $calendario->fetch_assoc()){
            $fila = (object)$fila;
            $response->calendario[] = array ('id'=>$fila->codigo.' - '.$fila->nombre, 'id_orden' => $fila->id_order);
        }
        return json_encode($response);
    }

	public function deleteinsclimA(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        if($postdata->id > 0){
			$sql ="UPDATE {$postdata->table} SET status ='0' where id='{$postdata->id}'";
			//$response->sql = $sql;
			$this->conexion->link->query($sql);
            $insclimA = "SELECT {$postdata->table}.*, cat_descripciones_equipos.nombre AS des, cat_areas.nombre AS are FROM {$postdata->table}
                JOIN orden_trabajo ON orden_trabajo.id = {$postdata->table}.id_orden
                JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = {$postdata->table}.descripcion
                JOIN cat_areas ON cat_areas.id = {$postdata->table}.area
                WHERE orden_trabajo.id_cotizacion  = {$postdata->id_cotizacion} AND {$postdata->table}.status=1";
			$insclimA = $this->conexion->link->query($insclimA);
			$response->insclimA = [];
			while($fila = $insclimA->fetch_assoc()){
            $fila = (object)$fila;
            $response->insclimA[] = array ('id'=>$fila->id,
                'area'=>$fila->are,
                'desequipo'=>$fila->des,
                'codigo'=>$fila->codigo,
                'marca'=>$fila->marca,
                'capacidadBTU'=>$fila->capacidadBTU,
                'modelo'=>$fila->modelo,
                'serie'=>$fila->serie,
				'edit'=>0
            );
			}

        }
		$response->equiposVal = $this->getEquiposVal($postdata->id_cotizacion);
        return json_encode($response);
    }

	public function editDatos(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        print_r($postdata);
        $response = new stdClass;
        if($postdata->id_cotizacion > 0){
			$sql ="SELECT * FROM orden_trabajo WHERE id_cotizacion = {$postdata->id_cotizacion} and codigo = '{$postdata->codigo}'";
			$res = $this->conexion->link->query($sql);
			$res = $res->fetch_object();

            $sql ="UPDATE orden_trabajo SET fecha_fin= ADDTIME(fecha_agendada, '{$postdata->tiempo_estimado}'), tiempo_estimado = '{$postdata->tiempo_estimado}', observaciones = '{$postdata->observaciones}' where id = '{$res->id}'";
            $this->conexion->link->query($sql);
            
            $sql ="UPDATE cotizaciones SET id_contacto = '{$postdata->contacto}' where id = '{$postdata->id_cotizacion}'";
            $this->conexion->link->query($sql);
        }
        return json_encode($response);
    }

	public function deleteinsVentilacion(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        if($postdata->id > 0){
			$sql ="UPDATE {$postdata->table} SET status ='0' where id='{$postdata->id}'";
			$this->conexion->link->query($sql);
            $insventi = "SELECT {$postdata->table}.*, cat_descripciones_equipos.nombre AS des, cat_areas.nombre AS are FROM {$postdata->table}
                JOIN orden_trabajo ON orden_trabajo.id = {$postdata->table}.id_orden
                JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = {$postdata->table}.descripcion
                JOIN cat_areas ON cat_areas.id = {$postdata->table}.area
                WHERE orden_trabajo.id_cotizacion  = {$postdata->id_cotizacion} AND {$postdata->table}.status=1";

			$insventi = $this->conexion->link->query($insventi);
			$response->insVentilacion = [];
			while($fila = $insventi->fetch_assoc()){
            $fila = (object)$fila;
            $response->insVentilacion[] = array ('id'=>$fila->id,
                'area'=>$fila->are,
                'desequipo'=>$fila->des,
                'codigo'=>$fila->codigo,
                'marca'=>$fila->marca,
                'capacidadHP'=>$fila->capacidadHP,
                'capacidadCFN'=>$fila->capacidadCFN,
                'modelo'=>$fila->condensadorModelo,
                'serie'=>$fila->condensadorSerie,
				'edit'=>0
            );
			}

        }
		$response->equiposVal = $this->getEquiposVal($postdata->id_cotizacion);
        return json_encode($response);
    }

	public function deleteinsRefri(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        if($postdata->id > 0){
			$sql ="UPDATE {$postdata->table} SET status ='0' where id='{$postdata->id}'";
			//$response->sql = $sql;
			$this->conexion->link->query($sql);
            $insRefri = "SELECT {$postdata->table}.*, cat_descripciones_equipos.nombre AS des, cat_areas.nombre AS are FROM {$postdata->table}
                JOIN orden_trabajo ON orden_trabajo.id = {$postdata->table}.id_orden
                JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = {$postdata->table}.id_equipo_des
                JOIN cat_areas ON cat_areas.id = {$postdata->table}.id_area
                WHERE orden_trabajo.id_cotizacion  = {$postdata->id_cotizacion} AND {$postdata->table}.status=1";

			$insRefri = $this->conexion->link->query($insRefri);
			$response->insRefri = [];
			while($fila = $insRefri->fetch_assoc()){
            $fila = (object)$fila;
            $response->insRefri[] = array ('id'=>$fila->id,
                'area'=>$fila->are,
                'desequipo'=>$fila->des,
                'codigo'=>$fila->codigo,
				'marca_eva'=>$fila->marca_eva,
				'capacidad_hp_eva'=>$fila->capacidad_hp_eva,
				'modelo_eva'=>$fila->modelo_eva,
				'serie_eva'=>$fila->serie_eva,
				'marca_conde'=>$fila->marca_conde,
				'capacidad_hp_conde'=>$fila->capacidad_hp_conde,
				'modelo_conde'=>$fila->modelo_conde,
				'serie_conde'=>$fila->serie_conde,
				'edit'=>0
            );
			}

        }
		$response->equiposVal = $this->getEquiposVal($postdata->id_cotizacion);
        return json_encode($response);
    }

	public function deleteinsclimB(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        if($postdata->id > 0){
			$sql ="UPDATE {$postdata->table} SET status ='0' where id='{$postdata->id}'";
			//$response->sql = $sql;
			$this->conexion->link->query($sql);
            $insclimB = "SELECT {$postdata->table}.*, cat_descripciones_equipos.nombre AS des, cat_areas.nombre AS are FROM {$postdata->table}
                JOIN orden_trabajo ON orden_trabajo.id = {$postdata->table}.id_orden
                JOIN cat_descripciones_equipos ON cat_descripciones_equipos.id = {$postdata->table}.descripcion
                JOIN cat_areas ON cat_areas.id = {$postdata->table}.area
                WHERE orden_trabajo.id_cotizacion  = {$postdata->id_cotizacion} AND {$postdata->table}.status=1";
			$insclimB = $this->conexion->link->query($insclimB);
			$response->insclimB = [];
			while($fila = $insclimB->fetch_assoc()){
            $fila = (object)$fila;
            $response->insclimB[] = array ('id'=>$fila->id,
                'area'=>$fila->are,
                'desequipo'=>$fila->des,
                'codigo'=>$fila->codigo,
                'marcaE'=>$fila->marca,
                'capacidadBTUE'=>$fila->capacidadBTU,
                'modeloE'=>$fila->modelo,
                'serieE'=>$fila->serie,
				'marcaC'=>$fila->condensadorMarca,
                'capacidadBTUC'=>$fila->condensadorBTU,
                'modeloC'=>$fila->condensadorModelo,
                'serieC'=>$fila->condensadorSerie,
				'edit'=>0
            );
			}

        }
		$response->equiposVal = $this->getEquiposVal($postdata->id_cotizacion);
        return json_encode($response);
    }

	public function deleteHerramienta(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        if($postdata->id_herramienta > 0){
			$sql ="UPDATE orden_trabajo_herramientas SET status ='0' where id='{$postdata->id_herramienta}'";
			//$response->sql = $sql;
			$this->conexion->link->query($sql);
            $herramientas = "SELECT orden_trabajo_herramientas.* FROM orden_trabajo_herramientas
                    JOIN orden_trabajo
                    ON orden_trabajo.id = orden_trabajo_herramientas.id_orden
                    JOIN cotizaciones
                    ON  cotizaciones.id = orden_trabajo.id_cotizacion
                    WHERE cotizaciones.id ='{$postdata->id_cotizacion}' and orden_trabajo_herramientas.status=1
					and orden_trabajo.codigo='{$postdata->codigoOrden}'";
			$herramientas = $this->conexion->link->query($herramientas);
			$response->herramientas = [];
			while($fila = $herramientas->fetch_assoc()){
            $fila = (object)$fila;
            $response->herramientas[] = array ('id'=>$fila->id,
                'id_orden'=>$fila->id_orden,
                'herramienta'=>$fila->herramientas,
                'requerimientos'=>$fila->requerimientos,
				'edit'=>0
            );
			}
        }
        return json_encode($response);
    }

    private function getClients($id_cotizacion){
        $response = new stdClass;
        
        if($id_cotizacion > 0){
            $sql = "SELECT cat_clientes.*, cotizaciones.id_contacto As contacto
                    FROM cat_clientes
                    INNER JOIN cotizaciones ON cat_clientes.id = cotizaciones.id_cliente
                    WHERE cotizaciones.id = {$id_cotizacion}";
            $response = $this->conexion->queryRow($sql);

            if($response->sucursales == "SI"){
				$sql = "SELECT cat_sucursales.* FROM cat_sucursales
                        JOIN cat_clientes ON cat_clientes.id = cat_sucursales.id_cliente
                        JOIN cotizaciones ON cat_sucursales.id = cotizaciones.id_sucursal
                        WHERE cotizaciones.id = {$id_cotizacion}";
				$result = $this->conexion->queryRow($sql);
				$response->direccion = $result->direccion;
                $response->id_tipcli = $result->id_tipcli;
                
                $sql = "SELECT 
                            cat_sucursales_contactos.id, 
                            CONCAT(cat_sucursales_contactos.nombre,' / ',cat_sucursales_contactos.telefono,' / ',cat_sucursales_contactos.correo) AS label
                        FROM cat_sucursales_contactos
                        INNER JOIN cotizaciones ON cat_sucursales_contactos.id_sucursal = cotizaciones.id_sucursal
                        WHERE cotizaciones.id = {$id_cotizacion}";
                $response->contactos = $this->conexion->queryAll($sql);

            } else {

                $sql = "SELECT 
                            cat_clientes_contactos.id, 
                            CONCAT(cat_clientes_contactos.nombre,' / ',cat_clientes_contactos.telefono,' / ',cat_clientes_contactos.correo) AS label
                        FROM cat_clientes_contactos
                        INNER JOIN cotizaciones ON cat_clientes_contactos.id_cliente = cotizaciones.id_cliente
                        WHERE cotizaciones.id = {$id_cotizacion}";
                $response->contactos = $this->conexion->queryAll($sql); 
            }
        }
        return $response;
    }

    public function ChangeStatus(){
        $datos = (object)$_POST;

        $sql="UPDATE orden_trabajo SET status=3 WHERE id_cotizacion='$datos->idorden'";
        $this->conexion->Consultas(1,$sql);
		/*$sql="UPDATE cotizaciones SET status=3 WHERE id='$datos->idorden'";
        $this->conexion->Consultas(1,$sql);*/

        /* REFRESH PRONTOFORMS */
        /*$isMantenimiento = $this->conexion->queryRow("SELECT id FROM orden_trabajo WHERE tipo_trabajo LIKE '%MANTENIMIENTO%' AND tipo_trabajo != 'REVISION MANTENIMIENTO' AND id_cotizacion = '{$datos->idorden}'");
        if($isMantenimiento){
            $this->apiProntoforms->refreshMantenimiento();
        }*/
        return 1;
    }

    public function CrearGarantia(){
        $datos = (object)$_POST;
		$querysClimA = [
            0 =>  'cor_caracteristicas_climatizacion_a',
            1 =>  'ins_caracteristicas_climatizacion_a',
            2 =>  'man_caracteristicas_climatizacion_a',
            3 =>  'rev_caracteristicas_climatizacion_a',

        ];
		$querysClimB = [
            4 =>  'cor_caracteristicas_climatizacion_b',
            5 =>  'ins_caracteristicas_climatizacion_b',
            6 =>  'man_caracteristicas_climatizacion_b',
            7 =>  'rev_caracteristicas_climatizacion_b',
        ];
		$querysRef = [
			8 =>  'cor_caracteristicas_refrigeracion',
            9 =>  'ins_caracteristicas_refrigeracion',
            10 =>  'man_caracteristicas_refrigeracion',
            11 =>  'rev_caracteristicas_refrigeracion',
        ];
		$querysVen = [
			12 =>  'cor_caracteristicas_ventilacion',
            13 =>  'ins_caracteristicas_ventilacion',
            14 =>  'man_caracteristicas_ventilacion',
            15 =>  'rev_caracteristicas_ventilacion',
        ];
		//print_r($datos);
        $sql = "SELECT * from orden_trabajo where id_cotizacion = $datos->idorden and codigo = '$datos->codigo'";
		$res = $this->conexion->link->query($sql);
        $res = $res->fetch_object();
		$codigo = explode("OT", $res->codigo);
		$codigo = "OR".$codigo[1];
		//print_r(explode("OT", $res->codigo));
		$sql = "INSERT INTO orden_trabajo SET id_cotizacion = '$res->id_cotizacion',cliente = '$res->cliente',
		tipo_cliente = '$res->tipo_cliente',direccion = '$res->direccion',tipo_trabajo = '$res->tipo_trabajo',
		tiempo_estimado = '$res->tiempo_estimado',observaciones = '$res->observaciones',tipo_registro = '$res->tipo_registro',fecha_create = CURRENT_DATE,
		codigo = '$codigo'";
		$this->conexion->link->query($sql);
		$sql = "SELECT id from orden_trabajo where id_cotizacion = $datos->idorden and codigo = '$codigo'";
		$res3 = $this->conexion->link->query($sql);
        $res3 = $res3->fetch_object();
		foreach($querysClimA as $query){
			 $sql = "SELECT * FROM ".$query." WHERE id_orden = ".$res->id." AND status=1";
			 $res2 = $this->conexion->link->query($sql);
			 while($fila = $res2->fetch_assoc()){
				$fila = (object)$fila;
				$sql2 = "INSERT INTO ".$query." SET area = '$fila->area',descripcion = '$fila->descripcion',codigo = '$fila->codigo',
				marca = '$fila->marca',capacidadBTU = '$fila->capacidadBTU',modelo = '$fila->modelo',serie = '$fila->serie',id_orden='$res3->id'";
			 //print_r($sql2);
				$this->conexion->link->query($sql2);
			 }
		}
		foreach($querysClimB as $query){
			 $sql = "SELECT * FROM ".$query." WHERE id_orden = ".$res->id." AND status=1";
			 $res2 = $this->conexion->link->query($sql);
			 while($fila = $res2->fetch_assoc()){
				$fila = (object)$fila;
				$sql2 = "INSERT INTO ".$query." SET id_orden='$res3->id', area = '$fila->area',descripcion = '$fila->descripcion',codigo = '$fila->codigo',
				evaporador = '$fila->evaporador',marca = '$fila->marca',capacidadBTU = '$fila->capacidadBTU',modelo = '$fila->modelo',
				serie = '$fila->serie', condensador='$fila->condensador', condensadorMarca='$fila->condensadorMarca', condensadorBTU='$fila->condensadorBTU',
				condensadorSerie = '$fila->condensadorSerie'";
			 //print_r($sql2);
				$this->conexion->link->query($sql2);
			 }
		}
		foreach($querysRef as $query){
			 $sql = "SELECT * FROM ".$query." WHERE id_orden = ".$res->id." AND status=1";
			 $res2 = $this->conexion->link->query($sql);
			 while($fila = $res2->fetch_assoc()){
				$fila = (object)$fila;
				$sql2 = "INSERT INTO ".$query." SET id_orden='$res3->id', area = '$fila->area',descripcion = '$fila->descripcion',codigo = '$fila->codigo',
				marca = '$fila->marca',modelo = '$fila->modelo',serie = '$fila->serie', compresorBTU='$fila->compresorBTU',
				compresorCantidad='$fila->compresorCantidad', ventiladorHP='$fila->ventiladorHP',
				ventiladorCantidad = '$fila->ventiladorCantidad'";
			 //print_r($sql2);
				$this->conexion->link->query($sql2);
			 }
		}
		foreach($querysVen as $query){
			 $sql = "SELECT * FROM ".$query." WHERE id_orden = ".$res->id." AND status=1";
			 $res2 = $this->conexion->link->query($sql);
			 while($fila = $res2->fetch_assoc()){
				$fila = (object)$fila;
				$sql2 = "INSERT INTO ".$query." SET id_orden='$res3->id', area = '$fila->area',descripcion = '$fila->descripcion',codigo = '$fila->codigo',
				marca = '$fila->marca',modelo = '$fila->modelo',serie = '$fila->serie', capacidadHP='$fila->capacidadHP',
				capacidadCFN='$fila->capacidadCFN'";
			 //print_r($sql2);
				$this->conexion->link->query($sql2);
			 }
		}
        //$sql="UPDATE orden_trabajo SET status=3 WHERE id_cotizacion='$datos->idorden'";
        //$this->conexion->Consultas(1,$sql);
		//$sql="UPDATE cotizaciones SET status=3 WHERE id='$datos->idorden'";
        //$this->conexion->Consultas(1,$sql);


        /* REFRESH PRONTOFORMS */
        /*$isMantenimiento = $this->conexion->queryRow("SELECT id FROM orden_trabajo WHERE tipo_trabajo LIKE '%MANTENIMIENTO%' AND tipo_trabajo != 'REVISION MANTENIMIENTO' AND id = '{$res3->id}'");
        if($isMantenimiento){
            $this->apiProntoforms->refreshMantenimiento();
        }*/
        return 1;
    }

    public function GetEvents(){
        $sql = "SELECT orden_trabajo.id, (SELECT nombre FROM cat_clientes WHERE id = cliente) AS title,
                (SELECT GROUP_CONCAT(DISTINCT CONCAT_WS( ' : ',cat_tipos_trabajo.descripcion,clase)  ORDER BY cat_tipos_trabajo.id DESC SEPARATOR ' , ')
                FROM cotizaciones_detalle , cat_tipos_trabajo
                WHERE tipo_trabajo = cat_tipos_trabajo.id AND id_cotizacion = orden_trabajo.id_cotizacion) as clase , fecha AS start, tiempo_estimado AS end,
                observaciones AS description, (SELECT direccion FROM cat_clientes WHERE id = cliente) AS direccion, cliente, tipo_cliente FROM orden_trabajo
                LEFT JOIN cat_tipos_trabajo ON tipo_trabajo = cat_tipos_trabajo.id
                WHERE 1=1
                GROUP BY orden_trabajo.id";
        $res = $this->conexion->link->query($sql);
        while($fila = $res->fetch_assoc()){
            $data[] = (object)$fila;
        }
        return json_encode($data);
    }

    public function GetEventsSinFecha(){
        $sql = "SELECT orden_trabajo.id, (SELECT nombre FROM cat_clientes WHERE id = cliente) AS title,
                (SELECT GROUP_CONCAT(DISTINCT CONCAT_WS( ' : ',cat_tipos_trabajo.descripcion,clase)  ORDER BY cat_tipos_trabajo.id DESC SEPARATOR ' , ')
                FROM cotizaciones_detalle , cat_tipos_trabajo
                WHERE tipo_trabajo = cat_tipos_trabajo.id AND id_cotizacion = orden_trabajo.id_cotizacion) as clase , fecha AS start, tiempo_estimado AS end,
                observaciones AS description, (SELECT direccion FROM cat_clientes WHERE id = cliente) AS direccion, cliente, tipo_cliente FROM orden_trabajo
                LEFT JOIN cat_tipos_trabajo ON tipo_trabajo = cat_tipos_trabajo.id
                WHERE 1=1
                GROUP BY orden_trabajo.id";
        $res = $this->conexion->link->query($sql);
        $details = "";
        $detalle = "";
        $detalle2 = "";
        if($res){
            while($fila = $res->fetch_assoc()){
                $fila = (object)$fila;
                if($fila->clase != ""){
                    $detalle2 = [];
                    $detalle = explode(",", $fila->clase);
                    if(count($detalle) > 0){
                        foreach ($detalle as $value) {
                            $detalle2[] = explode(":", $value);
                        }
                    }else{
                        $detalle2 = explode(":", $detalle);
                    }
                    $details = $detalle2;
                }
                $data[] = [
                    "id" => $fila->id,
                    "title" => $fila->title,
                    "details" => $details,
                    "start" => $fila->start,
                    "end" => $fila->end,
                    "description" => $fila->description,
                    "direccion" => $fila->direccion,
                ];
            }
        }
        return json_encode($data);
    }

    public function getNumEquipos($id_orden){
        $sql = "SELECT COUNT(id) AS num_equipos FROM cotizaciones_detalle WHERE id_cotizacion = $id_orden";
        $res = $this->conexion->Consultas(2, $sql);
        return $res[0]["num_equipos"];
    }

    private function refreshMantenimiento(){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://cegaservices2.procesos-iq.com/controllers/index.php?accion=ApiProntoforms.refreshMantenimiento");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,"");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch);
        curl_close ($ch);
        return $server_output;
    }
}