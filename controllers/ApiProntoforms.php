<?php

class ApiProntoforms { 

    public function ping(){
        $info = getdate();
        $date = $info['mday'];
        $month = $info['mon'];
        $year = $info['year'];
        $hour = $info['hours'];
        $min = $info['minutes'];
        $sec = $info['seconds'];
        return $date;
    }

    public function execute(){
        $function = getString('func' , '');
        $this->{$function}();
    }

    private $origenes = [
        "areas" =>                      ["file" => "ds_areas", "formspace" => "formspaces/192150025/sources/164370003"], #revision_instalacion, revision_mantenimiento, instalacion, mantenimiento
        "clientes" =>                   ["file" => "ds_clientes", "formspace" => "formspaces/192150025/sources/164370004" ], #cronograma
        "clientes_correctivo" =>        ["file" => "ds_clientesCorrectivo", "formspace" => "formspaces/192150025/sources/164370005"], #correctivo
        "clientes_instalacion" =>       ["file" => "ds_clientesInstalacion", "formspace" =>"formspaces/192150025/sources/164370006"], #instalacion
        "clientes_mantenimiento" =>     ["file" => "ds_clientesMantenimiento", "formspace" => "formspaces/192150025/sources/164370007"], #mantenimiento
        "descripcion_equipos" =>        ["file" => "ds_descriptionEquipo", "formspace" => "formspaces/192150025/sources/164370008"], #revision_instalacion, revision_mantenimiento, revision_correctivo, instalacion, mantenimiento, correctivo
        "herramientas" =>               ["file" => "ds_herramientas", "formspace" => "formspaces/192150025/sources/164370009"], #revision_instalacion, revision_mantenimiento, revision_correctivo
        "horas_dia" =>                  ["file" => "ds_horasDia", "formspace" => "formspaces/192150025/sources/164370010"], #cronograma
        "listado_equipos" =>            ["file" => "ds_listadoEquipos", "formspace" => "formspaces/192150025/sources/164368004"], #cronograma
        "listado_herramientas" =>       ["file" => "ds_listadoHerramientas", "formspace" => "formspaces/192150025/sources/164368005"], #cronograma
        "listado_repuestos" =>          ["file" => "ds_listadoRepuestos", "formspace" => "formspaces/192150025/sources/164368006"], #cronograma
        "listado_equipos_climatizacion_a_instalacion" =>                    ["file" => "ds_listadoEquiposClimatizacionADetalleInstalacion", "formspace" => "formspaces/192150025/sources/164368007"], #instalacion
        "listado_equipos_climatizacion_a_mantenimiento" =>                  ["file" => "ds_listadoEquiposClimatizacionADetalleMantenimiento", "formspace" => "formspaces/192150025/sources/164368008"], #mantenimiento
        "listado_equipos_climatizacion_a_detalle_revision_correctivo" =>    ["file" => "ds_listadoEquiposClimatizacionADetalleCorrectivo", "formspace" => "formspaces/192150025/sources/164368009"], #revision_correctivo
        "listado_equipos_climatizacion_b_correctivo" =>             ["file" => "ds_listadoEquiposClimatizacionBDetalleCorrectivo", "formspace" => "formspaces/192150025/sources/164368010"], #correctivo
        "listado_equipos_climatizacion_b_instalacion" =>            ["file" => "ds_listadoEquiposClimatizacionBDetalleInstalacion", "formspace" => "formspaces/192150025/sources/164368011"], #instalacion
        "listado_equipos_climatizacion_b_mantenimiento" =>          ["file" => "ds_listadoEquiposClimatizacionBDetalleMantenimiento", "formspace" => "formspaces/192150025/sources/164368012"], #mantenimiento
        "listado_equipos_climatizacion_b_revision_correctivo" =>    ["file" => "ds_listadoEquiposClimatizacionBDetalleRevisionCorrectivo", "formspace" => "formspaces/192150025/sources/164368013"], #revision_correctivo
        "listado_equipos_refigeracion_correctivo" =>                ["file" => "ds_listadoEquiposRefrigeracionDetalleCorrectivo", "formspace" => "formspaces/192150025/sources/164368014"], #correctivo
        "listado_equipos_refigeracion_instalacion" =>               ["file" => "ds_listadoEquiposRefrigeracionDetalleInstalacion", "formspace" => "formspaces/192150025/sources/164368015"], #instalacion
        "listado_equipos_refigeracion_mantenimiento" =>             ["file" => "ds_listadoEquiposRefrigeracionDetalleMantenimiento", "formspace" => "formspaces/192150025/sources/164368016"], #mantenimiento
        "listado_equipos_refigeracion_revision_correctivo" =>       ["file" => "ds_listadoEquiposRefrigeracionDetalleRevisionCorrectivo", "formspace" => "formspaces/192150025/sources/164368017"], #revision_correctiva
        "listado_equipos_ventilacion_correctivo" =>                 ["file" => "ds_listadoEquiposVentilacionDetalleCorrectivo", "formspace" => "formspaces/192150025/sources/164368018"], #correctivo
        "listado_equipos_ventilacion_instalacion" =>                ["file" => "ds_listadoEquiposVentilacionDetalleInstalacion", "formspace" => "formspaces/192150025/sources/164368019"], #instalacion
        "listado_equipos_ventilacion_mantenimiento" =>              ["file" => "ds_listadoEquiposVentilacionDetalleMantenimiento", "formspace" => "formspaces/192150025/sources/164368020"], #mantenimiento
        "listado_equipos_ventilacion_revision_correctivo" =>        ["file" => "ds_listadoEquiposVentilacionDetalleRevisionCorrectivo", "formspace" => "formspaces/192150025/sources/164368021"], #revision_correctivo
        "listado_evaporadores_correctivo" =>                        ["file" => "ds_listadoEvaporadoresCorrectivo", "formspace" => "formspaces/192150025/sources/164368022"], #correctivo
        "listado_evaporadores_instalacion" =>           ["file" => "ds_listadoEvaporadoresInstalacion", "formspace" => "formspaces/192150025/sources/164368024"], #instalacion
        "listado_evaporadores_revision_correctivo" =>   ["file" => "ds_listadoEquiposVentilacionDetalleRevisionCorrectivo", "formspace" => "formspaces/192150025/sources/164368025"], #revision_correctivo
        "listado_materiales" =>                         ["file" => "ds_listadoMateriales", "formspace" => "formspaces/192150025/sources/164368026"], #cronograma
        "listado_revision_correctivo" =>                ["file" => "ds_listadoRevisionCorrectivo", "formspace" => "formspaces/192150025/sources/164368027"], #revision_correctivo
        "listado_revision_instalacion" =>               ["file" => "ds_listadoRevisionInstalacion", "formspace" => "formspaces/192150025/sources/164368028"], #revision_instalacion
        "listado_revision_mantenimiento" =>             ["file" => "ds_listadoRevisionMatenimiento", "formspace" => "formspaces/192150025/sources/164368029"], #revision_mantenimiento
        "listado_sucursales" =>     ["file" => "ds_listadoSucursales", "formspace" => "formspaces/192150025/sources/164368030"], #cronograma, instalacion, mantenimiento, correctivo
        "supervisores" =>           ["file" => "ds_supervisores", "formspace" => "formspaces/192150025/sources/164365011"] #cronograma, revision_instalacion, revision_mantenimiento, revision_correctivo, instalacion, mantenimiento, correctivo
    ];
     
    private $formularios = [
        "instalacion" => [
            "origenes" => [
                "clientes_instalacion",
                "descripcion_equipos",
                "listado_equipos_climatizacion_a_instalacion",
                "listado_equipos_climatizacion_b_instalacion",
                "listado_equipos_refigeracion_instalacion",
                "listado_equipos_ventilacion_instalacion",
                "listado_evaporadores_instalacion",
                "listado_sucursales",
                "supervisores",
            ]
        ],
        "mantenimiento" => [
            "origenes" => [
                "clientes_mantenimiento", 
                "descripcion_equipos", 
                "listado_equipos_climatizacion_a_mantenimiento", 
                "listado_equipos_climatizacion_b_mantenimiento", 
                "listado_equipos_ventilacion_mantenimiento", 
                "listado_equipos_refigeracion_mantenimiento", 
                "listado_sucursales", 
                "supervisores",
                "listado_herramientas"
            ]
        ],
        "correctivo" => [
            "origenes" => [
                "clientes_correctivo", 
                "listado_equipos_climatizacion_b_correctivo", 
                "listado_equipos_refigeracion_correctivo", 
                "listado_equipos_ventilacion_correctivo", 
                "listado_evaporadores_correctivo", 
                "supervisores", 
                "listado_sucursales",
            ]
        ],
        "revision_instalacion" => [
            "origenes" => [
                "descripcion_equipos", 
                "herramientas", 
                "listado_revision_instalacion", 
                "supervisores",
            ]
        ],
        "revision_mantenimiento" => [
            "origenes" => [
                "descripcion_equipos", 
                "herramientas", 
                "listado_revision_mantenimiento", 
                "supervisores",
            ]
        ],
        "revision_correctivo" => [
            "origenes" => [
                "descripcion_equipos", 
                "herramientas", 
                "listado_equipos_climatizacion_a_detalle_revision_correctivo", 
                "listado_equipos_climatizacion_b_revision_correctivo", 
                "listado_equipos_ventilacion_revision_correctivo", 
                "listado_evaporadores_revision_correctivo", 
                "listado_revision_correctivo", 
                "supervisores",
            ]
        ],
        "cronograma" => []
    ];
    
    public function __construct(){
        
    }
    
    public function refreshInstalacion(){
        foreach($this->formularios["instalacion"]["origenes"] as $origin){
            #generar archivo
            $path_file = "/home/procesosiq/public_html/cegaservices2/controllers/".$this->origenes[$origin]["file"].".json";
            $content = file_get_contents("http://cegaservices2.procesos-iq.com/recibe/ds/".$this->origenes[$origin]["file"].".php");
            file_put_contents($path_file, $content);

            #ejecutar curl
            $url_upload = "https://api.prontoforms.com/api/[version]/".$this->origenes[$origin]["formspace"]."/upload.json";
            $url_upload = str_replace("[version]", "1", $url_upload);

            $user = "a2141323001";
            $pass = "bNVk6pYVN/c3xCZDLJBbcEDqHPAvpftPEr+0ej2Lg15z5Ztvh1uYcRmb1Zf7Qpst";
            
            $cmd = 'curl -v -k -u user:password -X PUT --upload-file '.$path_file.' '.$url_upload;
            $cmd = str_replace("user", $user, $cmd);
            $cmd = str_replace("password", $pass, $cmd);            
            exec($cmd);
            unlink($path_file);

            // $ch = curl_init();
            // curl_setopt($ch, CURLOPT_URL, $url_upload);
            // curl_setopt($ch, CURLOPT_POST, 1);
            // curl_setopt($ch, CURLOPT_POSTFIELDS, "");
            // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            // $server_output = curl_exec ($ch);
            // curl_close ($ch);
            // echo json_encode($server_output);
        }
    }

    public function refreshMantenimiento(){
        #$output = shell_exec('php -f /home/procesosiq/public_html/cegaservices2/origenes_datos/actualizar_mantenimiento.php >/dev/null 2>&1');
        
        foreach($this->formularios["mantenimiento"]["origenes"] as $origin){
            #generar archivo
            $path_file = "/home/procesosiq/public_html/cegaservices2/controllers/".$this->origenes[$origin]["file"].".json";
            $content = file_get_contents("http://cegaservices2.procesos-iq.com/recibe/ds/".$this->origenes[$origin]["file"].".php");
            file_put_contents($path_file, $content);

            #ejecutar curl
            $url_upload = "https://api.prontoforms.com/api/[version]/".$this->origenes[$origin]["formspace"]."/upload.json";
            $url_upload = str_replace("[version]", "1", $url_upload);

            $user = "a2141323001";
            $pass = "bNVk6pYVN/c3xCZDLJBbcEDqHPAvpftPEr+0ej2Lg15z5Ztvh1uYcRmb1Zf7Qpst";
            
            $cmd = 'curl -v -k -u user:password -X PUT --upload-file '.$path_file.' '.$url_upload;
            $cmd = str_replace("user", $user, $cmd);
            $cmd = str_replace("password", $pass, $cmd);
            exec($cmd);
            unlink($path_file);

            // $ch = curl_init();
            // curl_setopt($ch, CURLOPT_URL, $url_upload);
            // curl_setopt($ch, CURLOPT_POST, 1);
            // curl_setopt($ch, CURLOPT_POSTFIELDS, "");
            // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            // $server_output = curl_exec ($ch);
            // curl_close ($ch);
            // echo json_encode($server_output);
        }
    }
}
?>