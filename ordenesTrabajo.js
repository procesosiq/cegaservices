    /*==================================
    =            CALENDARIO            =
    ==================================*/
    var calendar = {
        fullcalendarOrder : $('#calendar_object'),
        config : function(Loadevents){

            if (!jQuery().fullCalendar) {
                throw new Error("No existe el plugin de calendario")
                return;
            }

            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();

            if (App.isRTL()) {
                if (this.fullcalendarOrder.parents(".portlet").width() <= 720) {
                    this.fullcalendarOrder.addClass("mobile");
                    h = {
                        right: 'title, prev, next',
                        center: '',
                        left: 'agendaDay, agendaWeek, month, today'
                    };
                } else {
                    this.fullcalendarOrder.removeClass("mobile");
                    h = {
                        right: 'title',
                        center: '',
                        left: 'agendaDay, agendaWeek, month, today, prev,next'
                    };
                }
            } else {
                if (this.fullcalendarOrder.parents(".portlet").width() <= 720) {
                    this.fullcalendarOrder.addClass("mobile");
                    h = {
                        left: 'title, prev, next',
                        center: '',
                        right: 'today,month,agendaWeek,agendaDay'
                    };
                } else {
                    this.fullcalendarOrder.removeClass("mobile");
                    h = {
                        left: 'title',
                        center: '',
                        right: 'prev,next,today,month,agendaWeek,agendaDay'
                    };
                }
            }

            $.ajax({
                async : false,
                type: "POST",
                url: "controllers/index.php",
                data: "accion=Ordenes.GetEvents",
                success: function(msg){
                    // console.log(msg);
                    Loadevents = eval(msg);
                    // console.log(Loadevents);
                }
            });

            ahttp.post("./controllers/index.php?accion=Ordenes.GetEventsSinFecha" , function(r , b){
                b();
                if(r){
                    for (var i = 0; i < r.length; i++) {
                        addEvent(r[i]);
                    }
                }
            });

            var events = Loadevents; 

            var initDrag = function(el , data) {
                // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
                // it doesn't need to have a start or end
                var eventObject = {
                    id: data.id, 
                    title: data.title, 
                    description: data.description, 
                    start : data.start,
                    end : data.end,
                };
                // console.log(eventObject)
                // store the Event Object in the DOM element so we can get to it later
                el.data('eventObject', eventObject);
                // make the event draggable using jQuery UI
                el.draggable({
                    zIndex: 999,
                    revert: true, // will cause the event to go back to its
                    revertDuration: 0 //  original position after the drag
                });
            };

            var addEvent = function(data) {
                if(data){
                    var detalle = "";
                    var html = '<div class="external-event label" style="color:#333 !important"><h4  style="text-align:left">' + data.title + '</h4>';
                    var spans = [];
                    if(data.details.length > 0){
                        for (var i = data.details.length - 1; i >= 0; i--) {
                            spans.push('<p style="text-align:left"><span class="label ' + data.details[i][1] + '">' + data.details[i][0] + '</span></p>');
                        }
                    }
                    html += spans.join("");
                    html +=  '</div>';
                    detalle = $(html);
                    jQuery('#event_box').append(detalle);
                    initDrag(detalle , data);
                }
            };

            var saveEvent = function(data){
                var details = data || {};
                console.log(details);
                if(Object.keys(details).length > 0){
                    var data = {
                        idorden : details.id,
                        start : details.start,
                        end : details.end || details.start,
                    }
                     ahttp.post("./controllers/index.php?accion=Ordenes.saveEvent" , function(r , b){
                        b();
                        console.log(r);
                    } ,data);
                }
            }

            // $('#external-events div.external-event').each(function() {
            //     initDrag($(this));
            // });

            // $('#event_add').unbind('click').click(function() {
            //     var title = $('#event_title').val();
            //     addEvent(title);
            // });

            //predefined events
            $('#event_box').html("");

            return { //re-initialize the calendar
                header: h,
                defaultView: 'month', // change default view with available options from http://arshaw.com/fullcalendar/docs/views/Available_Views/ 
                slotMinutes: 15,
                lang: 'es',
                editable: true,
                droppable: true, // this allows things to be dropped onto the calendar !!!
                drop: function(date, allDay) { // this function is called when something is dropped

                    // retrieve the dropped element's stored Event Object
                    var originalEventObject = $(this).data('eventObject');
                    // we need to copy it, so that multiple events don't have a reference to the same object
                    var copiedEventObject = $.extend({}, originalEventObject);

                    // assign it the date that was reported
                    copiedEventObject.start = date;
                    copiedEventObject.allDay = allDay;
                    copiedEventObject.className = $(this).attr("data-class");
                    // render the event on the calendar
                    // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                    calendar.fullcalendarOrder.fullCalendar('renderEvent', copiedEventObject, true);
                    copiedEventObject.start = date.format();
                    saveEvent(copiedEventObject);
                    $(this).remove();
                    // is the "remove after drop" checkbox checked?
                    // if ($('#drop-remove').is(':checked')) {
                    //     // if so, remove the element from the "Draggable Events" list
                    //     $(this).remove();
                    // }
                },
                events: events,
                eventClick: function(calEvent, jsEvent, view) {
                    var msj = 'Evento : ' + calEvent.title + ' <br> Descripción : ' + calEvent.description + ' <br> Fecha : ' + calEvent.start.format("YYYY-MM-DD HH:mm");
                    alert(msj, 'Ordenes' , 'success');
                    // change the border color just for fun
                    $(this).css('border-color', 'red');

                },
                eventDrop: function(event, delta, revertFunc) {

                    alert(' El evento ' + event.title + " ha sido movido " + event.start.format("YYYY-MM-DD HH:mm") , 'Ordenes' , 'info');
                    if (!confirm("Esta seguro de cambiar el evento?")) {
                        revertFunc();
                    }else{
                        var start = event.start.format("YYYY-MM-DD HH:mm");
                        var end = event.start.format("YYYY-MM-DD HH:mm");
                        if(event.end){
                            end = event.end.format("YYYY-MM-DD HH:mm");
                        }
                        var data = {
                            id : event.id,
                            end : end,
                            start : start
                        }
                        saveEvent(data);
                    }

                },
                eventResize: function(event, delta, revertFunc) {

                    alert(event.title + " end is now " + event.end.format(), 'Ordenes' , 'info');
                    if (!confirm("Esta seguro de cambiar el evento?")) {
                        revertFunc();
                    }else{
                        // console.log(event.start.format());
                        // console.log(event.end.format());
                        // console.log(event.start.format("YYYY-MM-DD HH:mm"));
                        // console.log(event.end.format("YYYY-MM-DD HH:mm"));
                        var data = {
                            id : event.id,
                            end : event.end.format("YYYY-MM-DD HH:mm"),
                            start : event.start.format("YYYY-MM-DD HH:mm")
                        }
                        saveEvent(data);
                    }

                }
            }
        },
        init : function(){
            // console.log(this.config());
            var config = this.config();
            this.fullcalendarOrder.fullCalendar('destroy'); // destroy the calendar
            this.fullcalendarOrder.fullCalendar(config);
        },
        reload : function(events){
            this.fullcalendarOrder.fullCalendar('destroy'); // destroy the calendar
            if(events)
                this.fullcalendarOrder.fullCalendar(this.config(events));
            else
                this.fullcalendarOrder.fullCalendar(this.config());
        }
    }
    calendar.init();
    /*=====  End of CALENDARIO  ======*/

    var TableDatatablesAjax = function () {

    var initPickers = function () {
        //init date pickers
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            autoclose: true
        });
    }

    var handleRecords = function () {

        var grid = new Datatable();

        grid.init({
            src: $("#datatable_ajax"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 

                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js). 
                // So when dropdowns used the scrollable div should be removed. 
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                
                "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                            [10, 20, 50, 100, 150, -1],
                            [10, 20, 50, 100, 150, "Todos"] // change per page values here
                ],
                "language": {
                    "lengthMenu": "Vista de _MENU_ registros por pagina",
                    "zeroRecords": "No se encontro ningun registro",
                    "info": "Pagina _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros disponibles",
                    "infoFiltered": "(filtrado de un total de _MAX_ registros)"
                },
                "pageLength": 10, // default record count per page
                "ajax": {
                    "url": "./controllers/index.php?accion=Ordenes", // ajax source
                },
                "order": [
                    [1, "asc"]
                ],// set first column as a default sort by asc
                "buttons": [
                            { extend: 'print', className: 'btn dark btn-outline' },
                            // { extend: 'copy', className: 'btn red btn-outline' },
                            { extend: 'pdf', className: 'btn green btn-outline' },
                            { extend: 'excel', className: 'btn yellow btn-outline ' },
                            { extend: 'csv', className: 'btn purple btn-outline ' }
                            // { extend: 'colvis', className: 'btn dark btn-outline', text: 'Columns'}
                            /*{
                                text: 'Nueva Orden',
                                className: 'btn blue btn-outline',
                                action: function ( e, dt, node, config ) {
                                    $('a[href="#registro"]').tab('show') 
                                }
                            }*/
                        ],
                "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l>><'table-scrollable't><'row'<'col-md-5 col-sm-12'i>>", // horizobtal scrollable datatable
            }
        });

        // handle group actionsubmit button click
        grid.getTableWrapper().on('click', '.table-group-action-submit', function (e) {
            e.preventDefault();
            var action = $(".table-group-action-input", grid.getTableWrapper());
            if (action.val() != "" && grid.getSelectedRowsCount() > 0) {
                grid.setAjaxParam("customActionType", "group_action");
                grid.setAjaxParam("customActionName", action.val());
                grid.setAjaxParam("id", grid.getSelectedRows());
                grid.getDataTable().ajax.reload();
                grid.clearAjaxParams();
            } else if (action.val() == "") {
                App.alert({
                    type: 'danger',
                    icon: 'warning',
                    message: 'Please select an action',
                    container: grid.getTableWrapper(),
                    place: 'prepend'
                });
            } else if (grid.getSelectedRowsCount() === 0) {
                App.alert({
                    type: 'danger',
                    icon: 'warning',
                    message: 'No record selected',
                    container: grid.getTableWrapper(),
                    place: 'prepend'
                });
            }
        });

        grid.setAjaxParam("customActionType", "group_action");
        grid.getDataTable().ajax.reload();
        grid.clearAjaxParams();

        $('#datatable_ajax_tools > li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            grid.getDataTable().button(action).trigger();
        });

        $('#datatable_ajax tbody').on( 'click', 'button', function () {
            // console.log(grid.table.getSelectedRows());
            // console.log($(this).parents('tr'));
            // console.log(grid.table.getDataTable());
            var btnid = $(this).prop("id");            
            var data = grid.getDataTable().row( $(this).parents('tr') ).data();

             if(data.length > 0){
                if(btnid == "edit"){
                    $("#oculto").val(data[1]);
                    angular.element($('#orderController')).scope().get(data[1]);
                    return false;
                    $('a[href="#registro"]').tab('show');
                }else if(btnid == "status"){
                    /*if(confirm("¿Esta seguro de cambiar el registro?")){
                         $.ajax({
                            type: "POST",
                            url: "controllers/index.php",
                            data: "accion=Ordenes.ChangeStatus&idorden="+data[1],
                            success: function(msg){
                                alert("Registro cambiado" , "CLIENTES" , 'success' , function(){
                                    grid.getDataTable().ajax.reload();
                                });
                            }
                        });
                    }*/
                }
             }
        } );

        $('.clockface_2').clockface({
            format: 'hh:mm',
            trigger: 'manual'
        });

        $('#clockface_2_toggle').click(function (e) {
            e.stopPropagation();
            $('#clockface_2').clockface('toggle');
        });

        $('#summernote_1').summernote({height: 300});
    }

    return {

        //main function to initiate the module
        init: function () {

            initPickers();
            handleRecords();
        }

    };

}();

jQuery(document).ready(function() {
    TableDatatablesAjax.init();
});

    // var app = angular.module('app', []);
    app.controller('orderController', ['$scope','$http','$interval','client','$controller', function($scope,$http,$interval,client, $controller){
        $scope.formData = {
        	areas : [],
        	roles : [],
            herramientas : [],
            materiales : [],
        	trabajos_tipos : ['Correctivo' , 'Instalacion' , 'Mantenimiento']
        };

        $scope.get = function(id){
            $scope.clear();
            if(id > 0){
                $scope.editOrder(id);
            }
        }

        $scope.addRow = function(){
        	var data = {
        		area : $("#area").val(),
        		tequipo : $("#tipo_equipo").val(),
        		desequipo : $("#des_equipo").val()
        	}
        	if(data.area != 0 && data.tequipo != 0 && data.desequipo != 0){
        		$scope.formData.areas.push(data);
                $("#area").val("");
                $("#tipo_equipo").val("");
                $("#des_equipo").val("");
        	}else{
                alert("Favor de completar los campos");
            }
        }

        $scope.addRowHerramientas = function(){
            var data = {
                herramienta : $("#herramientas_herramienta").val(),
                especificacion : $("#herramientas_especificacion").val()
            }
            if(data.herramienta != "" && data.especificacion != ""){
                $scope.formData.herramientas.push(data);
                $("#herramientas_herramienta").val("");
                $("#herramientas_especificacion").val("");
            }else{
                alert("Favor de completar los campos");
            }
        }

        $scope.addRowMateriales = function(){
            var data = {
                item : $("#materiales_item").val(),
                descripcion_item : $("#materiales_descripcion_item").val(),
                unidad : $("#materiales_unidad").val()
            }
            if(data.item != "" && data.descripcion_item != "" && data.unidad != ""){
                $scope.formData.materiales.push(data);
                $("#materiales_item").val("");
                $("#materiales_descripcion_item").val("");
                $("#materiales_unidad").val("");
            }else{
                alert("Favor de completar los campos");
            }
        }

        $scope.deleteArea = function(area , index){
        	var data = area || {};
        	if(data.area){
        		try{
        			delete $scope.formData.areas[index];
        			$scope.formData.areas.splice(index , 1);
        		}catch(e){
        			throw Error("Error al Elminar");
        		}
        	}
        }

        $scope.deleteMaterial = function(material , index){
            var data = material || {};
            if(data.item){
                try{
                    delete $scope.formData.materiales[index];
                    $scope.formData.materiales.splice(index , 1);
                }catch(e){
                    throw Error("Error al Elminar");
                }
            }
        }

        $scope.deleteHerramienta = function(herramienta , index){
            var data = herramienta || {};
            if(data.herramienta){
                try{
                    delete $scope.formData.herramientas[index];
                    $scope.formData.herramientas.splice(index , 1);
                }catch(e){
                    throw Error("Error al Elminar");
                }
            }
        }

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var tab = $(e.target).attr("href");
            if(tab == "#calendar_tab"){
                calendar.reload();
            }
        });

        $scope.editOrder = function(id){
            if(id > 0){
                $scope.formData.opt = "";
                $scope.formData.idOrder = id;
                client.post("./controllers/index.php?accion=Ordenes.edit" , $scope.getOrder , $scope.formData);
            }
        }

        $scope.getOrder = function(r , b){
            b();
            if(r && $scope.formData.idOrder > 0){
                $("a[href='#registro']").tab("show");
                //console.log(r);
                //$scope.formData.idOrder = r.data.id;
                $scope.formData.id_cliente = r.data.id_cliente;
                $scope.formData.tiempo = '';
                $scope.formData.fecha = '';
                $scope.formData.cliente = r.data.cliente;
                $scope.formData.tipo_cliente = r.data.tipo_cliente;
                $scope.formData.direccion = '';
                $scope.formData.tipos_trabajo = r.data.tipos_trabajo;
                $scope.formData.descripcionTrabajo = '';
                $scope.formData.areas = [];
                $scope.formData.materiales = [];
                $scope.formData.herramientas = [];
                $scope.formData.areas.length = 0;
            }
        }

        $scope.clear = function(){
            $scope.formData.idOrder = 0;
            $scope.formData.tiempo = 0;
            $scope.formData.fecha = '';
            $scope.formData.cliente = '';
            $scope.formData.tipo_cliente = '';
            $scope.formData.direccion = '';
            $scope.formData.tipo_trabajo = '';
            $scope.formData.descripcionTrabajo = '';
            $scope.formData.areas = [];
            $scope.formData.areas.length = 0;

            $("#idOrder").val("");
            $("#tiempo").val("");
            $("#fecha ").val("");
            $("#cliente ").val("");
            $("#tipo_cliente ").val("");
            $("#direccion ").val("");
            $("#tipo_trabajo ").val("");
            $("#descripcionTrabajo ").val("");
        }

        $(".save").on("click" , function(){
            var tiempo = $('#clockface_2').clockface('getTime');
            var descripcionTrabajo = $('#summernote_1').code();

            if(!$scope.formData.fecha || $scope.formData.fecha == ''){
                alert('Falta fecha' , 'Ordenes' ,'error');
                return false;
            }
            if(!$scope.formData.cliente || $scope.formData.cliente == ''){
                alert('Falta cliente' , 'Ordenes' ,'error');
                return false;
            }
            if(!$scope.formData.tipo_cliente || $scope.formData.tipo_cliente == ''){
                alert('Falta tipo de cliente' , 'Ordenes' ,'error');
                return false;
            }
            if(!$scope.formData.tipo_trabajo || $scope.formData.tipo_trabajo == ''){
                alert('Falta tipo de trabajo' , 'Ordenes' ,'error');
                return false;
            }
            if(!$scope.formData.direccion || $scope.formData.direccion == ''){
                alert('Falta direccion' , 'Ordenes' ,'error');
                return false;
            }
            if(tiempo == ''){
                alert('Falta fecha' , 'Ordenes' ,'error');
                return false;
            }

            
            $scope.formData.tiempo = tiempo;
            $scope.formData.opt = "ADD_ORDEN";
            $scope.formData.descripcionTrabajo = descripcionTrabajo;
            client.post("./controllers/index.php?accion=Ordenes.save" , $scope.success , $scope.formData);
        });

        $scope.success = function(r , b){
            b();
            if(r){
                alert('Orden Registrada con clave # '+r , 'Ordenes' , 'success', function(){
                    $scope.clear();
                    TableDatatablesAjax.init();
                    $("a[href='#listado']").tab("show");
                })
            }
        }

    }]);