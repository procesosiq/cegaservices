var load = {
		block : function(tableList){
		var config = (tableList && tableList != "") ? { target: '#' + tableList, animate: true } : { animate: true };
		App.blockUI(config);
		},
		unblock: function(tableList) { 
			var config = (tableList && tableList != "") ? '#' + tableList : '';
			App.unblockUI(config) 
		}
};

/////////// PLUGINS JQUERY
$.fn.serializeObject = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

////////// FACTORY PARA LAS PETICIONES HTTP
app.service('bonitaHttp', function ($http) {
	var _this = this;
	_this.debug = false;
	_this.params = {
		url : "",
		method : "",
		header : {},
		data : {},
		callback : "" ,
		required : function(){
			if(!this.url) throw Error("url is Required");
			if(!this.method) throw Error("Metodo is Required");
			if(!typeof this.callback == 'function') throw Error("Callback is Required");
		},
    block : function(){
      load.block(_this.target);
    },
    unblock: function() { load.unblock(_this.target); }
	}

	function call(url , method , callback,  data, header){
		_this.params.url = url;
		_this.params.method = method;
		_this.params.header = header;
		_this.params.data = data;
		_this.params.callback = callback;
		_this.params.required();
		_this.params.block();

		var dataConfig = {};
		dataConfig.url = _this.params.url;
		dataConfig.method = _this.params.method;
		dataConfig.header = _this.params.header;
		if(dataConfig.method == "get" ){
			dataConfig.params = _this.params.data;
		}else{
			dataConfig.data = _this.params.data;
		}
		// console.log(dataConfig);
		$http({
                url : dataConfig.url  , 
                method : dataConfig.method ,
                headers : dataConfig.header,
                data : dataConfig.data,
                params : dataConfig.params
            }).then(function(r){
            	// console.log(r);
            	// console.log(r.data);
			    callback(r.data , _this.params.unblock);
			}, function(result){
				CallbackError( _this.params.unblock , result);
			})
	};

	function CallbackError(block , error){
		block();
		if(_this.debug){
			if(error.status == 500 ){
				throw new Error(error.statusText);
			}
		}
	}

	_this.target  = "";

	_this.get = function(url , callback, data , header){
		if(angular.isObject(data)){

		}
		return call(url , "get" , callback, data , header)
	}

	_this.post = function(url , callback, data , header){
		return call(url , "post" , callback, data , header)
	}

	_this.put = function(url , callback, data , header){
		return call(url , "put" , callback, data , header)
	}

	_this.delete = function(url , callback, data , header){
		return call(url , "delete" , callback, data , header)
	}

	return _this;

})
////////// FACTORY PARA LAS PETICIONES HTTP