/*==============================================
	=            Modificacion del Alert            =
	==============================================*/
	window.old_alert = window.alert; 
	window.delay = 1000;
	window.alert = function(msj , categoria , typeMsj , callback){
		var titulo = typeof categoria == "undefined" ? "Notificacion" : categoria;
		var method = typeof typeMsj == "undefined" ? "error" : typeMsj;
		// var position = method == "error" ? "toast-top-full-width" : "toast-top-right";
		var functionCallback = typeof callback == "undefined" ? null : callback;
		var delay = window.delay;
		toastr.options = {
		  "closeButton": true,
		  "positionClass": "toast-top-full-width",
		  "onclick": functionCallback,
		  "showDuration": "1000",
		  "hideDuration": delay,
		  "timeOut": "5000",
		  "extendedTimeOut": "1000",
		  "showEasing": "swing",
		  "hideEasing": "linear",
		  "showMethod": "fadeIn",
		  "hideMethod": "fadeOut"
		}

		toastr[method](msj, titulo);

		if(callback){
			setTimeout(callback, delay);
		}
	}
			
	
	/*=====  End of Modificacion del Alert  ======*/
	/*==================================
	=            CALENDARIO            =
	==================================*/
	var calendar = {
		fullcalendarOrder : $('#calendar'),
		config : function(Loadevents){

			if (!jQuery().fullCalendar) {
				throw new Error("No existe el plugin de calendario")
                return;
            }

            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();

			if (App.isRTL()) {
                if (this.fullcalendarOrder.parents(".portlet").width() <= 720) {
                    this.fullcalendarOrder.addClass("mobile");
                    h = {
                        right: 'title, prev, next',
                        center: '',
                        left: 'agendaDay, agendaWeek, month, today'
                    };
                } else {
                    this.fullcalendarOrder.removeClass("mobile");
                    h = {
                        right: 'title',
                        center: '',
                        left: 'agendaDay, agendaWeek, month, today, prev,next'
                    };
                }
            } else {
                if (this.fullcalendarOrder.parents(".portlet").width() <= 720) {
                    this.fullcalendarOrder.addClass("mobile");
                    h = {
                        left: 'title, prev, next',
                        center: '',
                        right: 'today,month,agendaWeek,agendaDay'
                    };
                } else {
                    this.fullcalendarOrder.removeClass("mobile");
                    h = {
                        left: 'title',
                        center: '',
                        right: 'prev,next,today,month,agendaWeek,agendaDay'
                    };
                }
            }

            var events = Loadevents || [{
                    title: 'Click for Google',
                    start: new Date(y, m, 28),
                    end: new Date(y, m, 29),
                    backgroundColor: App.getBrandColor('yellow'),
                    description : "Prueba de Evento",
                    // url: 'http://google.com/',
                }];

                // console.log(events)

			return { //re-initialize the calendar
                header: h,
                defaultView: 'month', // change default view with available options from http://arshaw.com/fullcalendar/docs/views/Available_Views/ 
                slotMinutes: 15,
                lang: 'es',
                editable: true,
                droppable: false, // this allows things to be dropped onto the calendar !!!
                events: events,
                eventClick: function(calEvent, jsEvent, view) {
                	var msj = 'Evento : ' + calEvent.title + ' <br> Descripcion : ' + calEvent.description + ' <br> Fecha : ' + calEvent.start.format("YYYY-MM-DD HH:mm");
					alert(msj, 'Ordenes' , 'success');
					// change the border color just for fun
					$(this).css('border-color', 'red');

				},
				eventDrop: function(event, delta, revertFunc) {

			        alert(' El evento ' + event.title + " ha sido movido " + event.start.format("YYYY-MM-DD HH:mm") , 'Ordenes' , 'info');

			        if (!confirm("Esta seguro de cambiar el evento?")) {
			            revertFunc();
			        }

				}
            }
		},
		init : function(){
			// console.log(this.config());
			var config = this.config();
			this.fullcalendarOrder.fullCalendar('destroy'); // destroy the calendar
            this.fullcalendarOrder.fullCalendar(config);
		},
		reload : function(events){
			this.fullcalendarOrder.fullCalendar('destroy'); // destroy the calendar
			if(events)
				this.fullcalendarOrder.fullCalendar(this.config(events));
			else
				this.fullcalendarOrder.fullCalendar(this.config());
		}
	}
	calendar.init();
	/*=====  End of CALENDARIO  ======*/
	
	var app = angular.module('cegaservice', []);

	app.controller('orderController', ['$scope','$http','$interval','bonitaHttp','$controller', function($scope,$http,$interval,bonitaHttp, $controller){

		$scope.init = function(){
			var data = {
				opt : 'LISTA_ORDENES_CALENDAR'
			}
			bonitaHttp.post("resp_demo.php", $scope.printCalendar , data);
			$interval($scope.reloadData, 50000);
		}

		$scope.reloadData = function(){
			var data = {
				opt : 'LISTA_ORDENES_CALENDAR'
			}
			bonitaHttp.post("resp_demo.php", $scope.printCalendar , data);
		}

		$scope.printCalendar = function(r , b){
			b();
			if(r){
				var events = [] , data = {} , fecha ;
				for(var d in r.data){
					
					fecha = moment(r.data[d][2]).format("YYYY-MM-DD HH:mm");
					// fecha = moment(r.data[d][2]).format("id");
					events.push({
						title :  r.data[d][5],
						start : new Date(fecha),
						end : new Date(fecha),
						backgroundColor : App.getBrandColor('yellow'),
						description : "Orden tipo "+ r.data[d][5]+ " para el Cliente "+r.data[d][3] ,
					});
					console.log(events)
				}
				calendar.reload(events);
			}
		}

	}])





