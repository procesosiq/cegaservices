/*==============================================
	=            Modificacion del Alert            =
	==============================================*/
	window.old_alert = window.alert; 
	window.delay = 1000;
	window.alert = function(msj , categoria , typeMsj , callback){
		var titulo = typeof categoria == "undefined" ? "Notificacion" : categoria;
		var method = typeof typeMsj == "undefined" ? "error" : typeMsj;
		// var position = method == "error" ? "toast-top-full-width" : "toast-top-right";
		var functionCallback = typeof callback == "undefined" ? null : callback;
		var delay = window.delay;
		toastr.options = {
		  "closeButton": true,
		  "positionClass": "toast-top-full-width",
		  "onclick": functionCallback,
		  "showDuration": "1000",
		  "hideDuration": delay,
		  "timeOut": "5000",
		  "extendedTimeOut": "1000",
		  "showEasing": "swing",
		  "hideEasing": "linear",
		  "showMethod": "fadeIn",
		  "hideMethod": "fadeOut"
		}

		toastr[method](msj, titulo);

		if(callback){
			setTimeout(callback, delay);
		}
	}
			
	
	/*=====  End of Modificacion del Alert  ======*/
			
	/*==================================
	=            TABLA GRID            =
	==================================*/
		var grid = {
			table : {},
			init : function(){
				this.table = new Datatable();
				this.table.init({
		            src: $("#datatable_ajax"),
		            onSuccess: function (grid, response) {
		                // grid:        grid object
		                // response:    json object of server side ajax response
		                // execute some code after table records loaded
		            },
		            onError: function (grid) {
		                // execute some code on network or other general error  
		            },
		            onDataLoad: function(grid) {
		                // execute some code on ajax data load
		            },
		            loadingMessage: 'Loading...',
		            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 

		                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
		                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js). 
		                // So when dropdowns used the scrollable div should be removed. 
		                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
		                
		                "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

		                "lengthMenu": [
		                    [10, 20, 50, 100, 150, -1],
		                    [10, 20, 50, 100, 150, "Todos"] // change per page values here
		                ],
		                "pageLength": 10, // default record count per page
		                "ajax": {
		                    "url": "./resp_demo.php?opt=LISTA_ORDENES", // ajax source
		                },
		                "order": [
		                    [1, "asc"]
		                ]// set first column as a default sort by asc,
		                ,"columnDefs": [ {
				            "targets": -1,
				            "data": null,
				            "defaultContent": '<button type="button" class="btn btn-sm green btn-outline margin-bottom">Editar</button>'
				        } ]
		            }
		        });

				this.table.getDataTable().ajax.reload();
		        this.table.clearAjaxParams();
			},
			reload : function(){
				console.log("Entro")
				this.table.getDataTable().ajax.reload();
		        this.table.clearAjaxParams();
			}
		}

	grid.init();
	/*=====  End of TABLA GRID  ======*/
	

	var app = angular.module('cegaservice', ['checklist-model']);

    app.controller('orderController', ['$scope','$http','$interval','bonitaHttp','$controller', function($scope,$http,$interval,bonitaHttp, $controller){
        $scope.formData = {
        	idOrder : 0,
        	tiempo : 0,
        	descripcionTrabajo : '',
        	areas : [],
        	roles : [],
        	trabajos_tipos : ['Correctivo' , 'Instalacion' , 'Mantenimiento']
        };

	 	$('#datatable_ajax tbody').on( 'click', 'button', function () {
	 		// console.log(grid.table.getSelectedRows());
	   //      var data = grid.table.row( $(this).parents('tr') ).data();
	   //      alert( data );
	    } );

	 	$scope.init = function(){
	 		console.log("Recarga")
	 		 $interval($scope.reloadData, 50000);
	 	}

	 	$scope.reloadData = function(){
	 		grid.reload();
	 	}

        $scope.editOrder = function(id){
        	console.log(id);
        	if(id > 0){
	        	$scope.formData.opt = "ID_ORDEN";
	        	$scope.formData.idOrder = id;
	        	bonitaHttp.post("resp_demo.php" , $scope.getOrder , $scope.formData);
        	}
        }

        $scope.getOrder = function(r , b){
        	b();
        	if(r && $scope.formData.idOrder > 0){
        		$("a[href='#registro']").tab("show");
        		console.log(r);
				// $scope.formData.idOrder = 0;
				// $scope.formData.tiempo = 0;
				// $scope.formData.fecha = '';
				// $scope.formData.cliente = '';
				// $scope.formData.tipo_cliente = '';
				// $scope.formData.direccion = '';
				// $scope.formData.tipo_trabajo = '';
				// $scope.formData.descripcionTrabajo = '';
				// $scope.formData.areas = [];
				// $scope.formData.areas.length = 0;
        	}
        }

        $scope.clear = function(){
        	$scope.formData.idOrder = 0;
        	$scope.formData.tiempo = 0;
        	$scope.formData.fecha = '';
        	$scope.formData.cliente = '';
        	$scope.formData.tipo_cliente = '';
        	$scope.formData.direccion = '';
        	$scope.formData.tipo_trabajo = '';
        	$scope.formData.descripcionTrabajo = '';
        	$scope.formData.areas = [];
        	$scope.formData.areas.length = 0;

			$("#idOrder").val("");
        	$("#tiempo").val("");
        	$("#fecha ").val("");
        	$("#cliente ").val("");
        	$("#tipo_cliente ").val("");
        	$("#direccion ").val("");
        	$("#tipo_trabajo ").val("");
        	$("#descripcionTrabajo ").val("");
        }

        $scope.addRow = function(){
        	var data = {
        		area : $("#area").val(),
        		tequipo : $("#tipo_equipo").val(),
        		
        	}

        	if(data){
        		$scope.formData.areas.push(data);
        		$("#tipo_equipo").val("");
        		$("#area").val("");
        		$("#des_equipo").val("");
        	}
        }

        $scope.deleteArea = function(area , index){
        	if(confirm("Esta seguro de eliminar el registro?")){
	        	// var data = area || {};
	        	if(index > -1){
	        		try{
	        			delete $scope.formData.areas[index];
	        			$scope.formData.areas.splice(index , 1);       			
	        		}catch(e){
	        			co
	        			throw Error("Error al Elminar");
	        		}
	        	}
        	}
        }

        $(".save").on("click" , function(){
        	var tiempo = $('#clockface_2').clockface('getTime');
        	var descripcionTrabajo = $('#summernote_1').code();

        	if(!$scope.formData.fecha || $scope.formData.fecha == ''){
        		alert('Falta fecha' , 'Ordenes' ,'error');
        		return false;
        	}
			if(!$scope.formData.cliente || $scope.formData.cliente == ''){
        		alert('Falta cliente' , 'Ordenes' ,'error');
        		return false;
        	}
        	if(!$scope.formData.tipo_cliente || $scope.formData.tipo_cliente == ''){
        		alert('Falta tipo de cliente' , 'Ordenes' ,'error');
        		return false;
        	}
			if(!$scope.formData.tipo_trabajo || $scope.formData.tipo_trabajo == ''){
        		alert('Falta tipo de trabajo' , 'Ordenes' ,'error');
        		return false;
        	}
			if(!$scope.formData.direccion || $scope.formData.direccion == ''){
        		alert('Falta direccion' , 'Ordenes' ,'error');
        		return false;
        	}
			if(tiempo == ''){
        		alert('Falta fecha' , 'Ordenes' ,'error');
        		return false;
        	}

        	
        	$scope.formData.tiempo = tiempo;
        	$scope.formData.opt = "ADD_ORDEN";
        	$scope.formData.descripcionTrabajo = descripcionTrabajo;
            bonitaHttp.post("resp_demo.php?OPT=ADD_ORDEN" , $scope.success , $scope.formData);
        });

        $scope.success = function(r , b){
        	b();
        	if(r){
        		alert('Orden Registrada con clave # '+r , 'Ordenes' , 'success', function(){
        			$scope.clear();
        			grid.reload();
        			$("a[href='#listado']").tab("show");
        		})
        	}
        }

    }])