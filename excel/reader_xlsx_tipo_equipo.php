<?php
header('Content-Type: text/html; charset=utf-8');

ini_set('display_errors',1);
error_reporting(E_ALL);

include '../controllers/conexion.php';
include 'simplexlsx.class.php';

$path = realpath('./');

$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
foreach($objects as $name => $object){
    if('.' != $object->getFileName() && '..' != $object->getFileName()){
        $pos1 = strpos($object->getPathName(), "BASE DE DATOS - EQUIPOS Y MATERIALES.xlsx");

        if($pos1 !== false){
            $ext = pathinfo($object->getPathName(), PATHINFO_EXTENSION);
            if('xlsx' == $ext){
                switch ($ext){

                    case 'xlsx':
                        $excel = trim(file_get_contents($object->getPathName()));
                        procesar_xlsx($excel, $object->getFileName());
                        break;

                    default:
                        break;
                }
            }
        }
    }
}


function eliminar_archivo($ruta){
    unlink($ruta);
}

function procesar_xlsx($excel, $file_name){
    $xlsx = new SimpleXLSX($file_name);
    $conexion = new M_Conexion;
    $clientes = $xlsx->sheetNames();
    // print_r($clientes);
    $cancel = false;
    $registro = 0;
    $var = 0;
    $bandera=false;
    $rows = $xlsx->rows(3);
    $piezas = array();
    $add_piezas = array();
    foreach ($rows as $key => $fila) {
        $tipos = array();
        $partes = array();
        $refrigerantes = array();
        $motores = array();
        if($key > 0){
            foreach ($fila as $key2 => $celda) {
                // print_r($fila);
                $get_ids_tipo_equipo = "SELECT * FROM cat_equiposTipo WHERE nombre = '{$fila[0]}'";
                $data = $conexion->Consultas(2, $get_ids_tipo_equipo);
                $id_tipo_equipo = isset($data[0]["id"]) ?$data[0]["id"]:1;
                // print_r($data);
                #partes
                $sql = "SELECT * FROM cat_equiposPartes WHERE nombre = '".$fila[2]."' AND id_tipo_equipo = '".$id_tipo_equipo."';";
                $data = $conexion->Consultas(2, $sql)[0]["id"];
                // print $sql." - ".$data."<br>";

                $sql = "SELECT * FROM cat_equiposLista WHERE nombre = '".$fila[1]."' AND id_tipoequipo = '".$id_tipo_equipo."';";
                $dataLista = $conexion->Consultas(2, $sql)[0]["id"];
                // print $sql." - ".$dataLista."<br>";
                // print_r($info);
                // print $sql." - ".$data."<br>";
                #TIPO DE EQUIPO
                $cancel = false;
                if($key2==0){
                    foreach ($tipos as $key_tipo => $tipo) {
                        if($tipo==trim($celda))
                            $cancel = true;
                    }
                    if(!$cancel)
                        $tipos[(($var==0)?1:$var)] = trim($celda);
                }
                #PARTES DE EQUIPO
                $cancel = false;
                if($key2==2){
                    foreach ($partes as $key_partes => $value_partes) {
                        if(trim($celda) == $value_partes){
                            $cancel = true;
                        }
                    }
                    if(!$cancel)
                        $partes[(($var==0)?1:$var)]["nombre"] = trim($celda);
                }
                #PIEZAS
                $cancel = false;
                if($key2==3){
                    $cancel = false;
                    // foreach ($add_piezas as $key_piezas => $value_piezas) {
                    //     if(trim($celda) == $value_piezas["pieza"] && $value_piezas["id_tipoequipo"] != $id_tipo_equipo)
                    //         $cancel = true;
                    // }
                    // if(!$cancel)
                        $add_piezas[] = array("pieza"=>trim($celda), "id_tipoequipo"=>$id_tipo_equipo, "id_parte_equipo"=>$data , "id_descripcion_equipo" => $dataLista);
                }
                #REFRIGERANTES
                /*$calcel = false;
                if($key2==4){
                    $cancel = false;
                    foreach ($refrigerantes as $key_refrigerantes => $value_refrigerantes) {
                        if(trim($celda) == $value_refrigerantes)
                            $cancel = true;
                    }
                    if(!$cancel)
                        $refrigerantes[] = trim($celda);
                }
                #MOTORES
                $calcel = false;
                if($key2==5){
                    $cancel = false;
                    foreach ($motores as $key_motores => $value_motores) {
                        if(trim($celda) == $value_motores)
                            $cancel = true;
                    }
                    if(!$cancel)
                        $motores[] = trim($celda);
                }*/
                
                if($key>0){
                    if($rows[$key][1] != $rows[$key-1][1] && !$bandera){
                        if($key2==3){
                            $var++;
                            $piezas[$var][] = array("pieza"=>trim($celda), "refrigerante"=>trim($fila[$key2+1]), "motor"=>trim($fila[$key2+2]));
                            $bandera = true;
                        }
                    }
                    else{
                        if($key2==3){
                            $bandera = false;
                            $piezas[$var][] = array("pieza"=>trim($celda), "refrigerante"=>trim($fila[$key2+1]), "motor"=>trim($fila[$key2+2]));
                        }
                    }
                }
            }

            #$sql = (isset($partes[$var]["nombre"]))?"SELECT * FROM cat_equiposPartes WHERE nombre = '".$partes[$var]["nombre"]."'":'SELECT count(1) as id';
            #$ids_partes = $conexion->Consultas(2, $sql)[0]["id"];

            $get_ids_piezas = "SELECT * FROM cat_piezas WHERE descripcion = '{$fila[3]}'";
            #$id_pieza = $conexion->Consultas(2, $get_ids_piezas)[0]["id"];

            $get_ids_refrigerante = "SELECT * FROM cat_refrigerantes WHERE descripcion = '{$fila[4]}'";
            #$data = $conexion->Consultas(2, $get_ids_refrigerante);
            #$id_refrigerante = ((isset($data[0]))?$data[0]["id"]:'');

            $get_ids_motor = "SELECT * FROM cat_motores WHERE descripcion = '{$fila[5]}'";
            #$data = $conexion->Consultas(2, $get_ids_motor);
            #$id_motor = ((isset($data[0]))?$data[0]["id"]:'');

            #$id_piezas[$var][] = array("id_pieza"=>$id_pieza, "id_refrigerante"=>$id_refrigerante, "id_motor"=>$id_motor);
            #$info[$var]["nombre"] = $fila[1];
            #$info[$var]["id_tipo_equipo"] = $id_tipo_equipo[0];
            #$info[$var]["id_partes"] = $ids_partes;

            /*if($key>0)
            if($fila[1] != $rows[$key-1][1]){
                echo $sql_insert = "INSERT INTO equiposLista(id_tipoequipo,nombre,ids_partes,ids_piezas,ids_refrigerante) VALUES($id_tipo_equipo[0], '$fila[1]','$ids_partes','".json_encode($id_piezas[$var])."','$id_refrigerante','$id_motor')<br><br>";
            }
            else{
                if($key2 == 3){
                    #$piezas[$var][] = trim($celda);
                }
            }*/
        }
    }

    foreach ($add_piezas as $key => $value) {
        // echo json_encode($value)."<br>";
        $sql = "INSERT INTO cat_piezas(descripcion, id_tipoequipo, id_parte_equipo , id_descripcion_equipo) VALUES('".$value["pieza"]."', ".$value["id_tipoequipo"].", ".$value["id_parte_equipo"]." , ".$value["id_descripcion_equipo"].");";
        print $sql . "<br>";
        // $conexion->Consultas(1, $sql);
    }

    foreach ($piezas as $key => $value) {
        #echo json_encode($id_piezas[$key])."<br><br>";
        #echo $sql_insert = "INSERT INTO cat_equiposLista(id_tipoequipo,nombre,id_partes,piezas) VALUES(".$info[$key]['id_tipo_equipo'].", '".$info[$key]['nombre']."',".$info[$key]["id_partes"].",'".json_encode($id_piezas[$key])."');<br><br>";
        #$conexion->Consultas(1, $sql_insert);
    }


    foreach ($xlsx->rows(4) as $key => $fila) {
        foreach ($fila as $key2 => $celda) {
            if($key2==0 && $key>0){
                $celda = trim($celda);
                $sql = "INSERT INTO cat_marcas(nombre) VALUES('$celda');";
                #$conexion->Consultas(1,$sql);
            }
        }
    }

    $reg_areas = array();
    $registro = 0;
    foreach ($xlsx->rows(5) as $key => $fila) {
        foreach ($fila as $key2 => $celda) {
            if($key>0){
                if($key2==0){
                    $reg_areas[$registro]["tipo_equipo"] = $celda;
                }
                if($key2==1){
                    $reg_areas[$registro]["tipo_cliente"] = $celda;
                }
                if($key2==2){
                    $reg_areas[$registro]["area_climatiza"] = $celda;
                    $registro++;
                }
            }   
        }
    }
    $sql = "";
    foreach ($reg_areas as $key => $value) {
        $sql = "SELECT * FROM cat_equiposTipo WHERE nombre = '".$value['tipo_equipo']."';";
        $tipo_equipo = 0;#$conexion->Consultas(2, $sql)[0]["id"];

        $sql = "SELECT * FROM cat_clientesTipo WHERE nombre = '".$value['tipo_cliente']."'";
        $tipo_cliente = 0;#$conexion->Consultas(2, $sql)[0]["id"];

        $sql = "INSERT INTO cat_areas(id_tipoequipo, id_tipocliente, nombre) VALUES(".(isset($tipo_equipo)?$tipo_equipo:'1').", $tipo_cliente, '".$value['area_climatiza']."');";
        #echo $sql."<br>";
        #$conexion->Consultas(1, $sql);
    }
}

?>