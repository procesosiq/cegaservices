<?php
header('Content-Type: text/html; charset=utf-8');

ini_set('display_errors',1);
error_reporting(E_ALL);

/** DATABASE */
$mysqli = @new mysqli("localhost", "auditoriasbonita", "u[V(fTIUbcVb", "demo");
#$mysqli = @new mysqli("localhost", "root", "", "auditoriasbonita");
if (mysqli_connect_errno()) {
    printf("Falló la conexión: %s\n", mysqli_connect_error());
    exit();
}
$mysqli->set_charset("utf8");

/** Directorio de los JSON */
$path = realpath('./');

$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
foreach($objects as $name => $object){
    if('.' != $object->getFileName() && '..' != $object->getFileName() && $path != $object->getPath()){
        $pos1 = strpos($object->getPath(), "bonitajson_");

        if($pos1 !== false){
            $ext = pathinfo($object->getPathName(), PATHINFO_EXTENSION);
            if('json' == $ext || 'jpeg' == $ext || 'jpg' == $ext || 'png' == $ext || 'mp3' == $ext || '3gpp' == $ext){
                switch ($ext) {
                    case 'json':
                        /* READ AND MOVE JSON */
                        $json = json_decode(trim(file_get_contents($object->getPathName())), true);
                        process_json($json, $object->getFileName(), $mysqli);
                        move_json($object->getPathName(), $object->getFileName());
                        break;

                    case 'jpeg':
                    case 'jpg':
                    case 'png':
                        /* MOVE JSON */
                        move_image($object->getPathName(), $object->getFileName());
                        break;

                    case '3gpp':
                    case 'mp3':
                        /* MOVE AUDIO */
                        move_audio($object->getPathName(), $object->getFileName());
                        break;

                    default:
                        break;
                }
            }
        }
    }
}
$mysqli->close();

function move_json($file, $nameFile){
    echo $file;
    echo $nameFile;
    // die();
    if(!rename($file, __DIR__."/../reportes_vistos/json/$nameFile")){
        echo 'error';
    }
}

function move_image($file, $nameFile){
    // die();
    if(!rename($file, __DIR__."/../reportes_vistos/image/$nameFile")){
        echo 'error';
    }
}

function move_audio($file, $nameFile){
    // die();
    if(!rename($file, __DIR__."/../reportes_vistos/audio/$nameFile")){
        echo 'error';
    }
}

function process_json($json, $filename, $mysqli){
    $identifier = $json['identifier'];
    $version = $json['version']. '';;
    $zone = $json['zone']. '';;
    $referenceNumber = $json['referenceNumber']. '';;
    $state = $json['state']. '';;
    $deviceSubmitDate = $json['deviceSubmitDate'];
    $deviceSubmitDateDate = $deviceSubmitDate['time']. '';;
    $deviceSubmitDateZone = $deviceSubmitDate['zone']. '';;
    $fecha_file = str_replace("-", "", explode("T", $deviceSubmitDateDate)[0]);

    $shiftedDeviceSubmitDate = $json['shiftedDeviceSubmitDate'].'';;
    $serverReceiveDate = $json['serverReceiveDate'].'';;
    $form = $json['form'];
    $form_identifier = $form['identifier'].'';;
    $form_versionIdentifier = $form['versionIdentifier'].'';;
    $form_name = $form['name'].'';;
    $form_version = $form['version'].'';;
    $form_formSpaceIdentifier = $form['formSpaceIdentifier'].'';;
    $form_formSpaceName = $form['formSpaceName'].'';;

    $user = $json['user'];
    $user_identifier = $user['identifier'].'';;
    $user_username = $user['username'].'';;
    $user_displayName = $user['displayName'].'';;

    $geoStamp = $json['geoStamp'];
    $geoStamp_success = $geoStamp['success'].'';;
    $geoStamp_captureTimestamp = $geoStamp['captureTimestamp'];
    $geoStamp_captureTimestamp_provided = $geoStamp_captureTimestamp['provided'];
    $geoStamp_captureTimestamp_provided_time = $geoStamp_captureTimestamp_provided['time'].'';;
    $geoStamp_captureTimestamp_provided_zone = $geoStamp_captureTimestamp_provided['zone'].'';;
    $geoStamp_captureTimestamp_shifted = $geoStamp_captureTimestamp['shifted'].'';;
    $geoStamp_errorMessage = $geoStamp['errorMessage'].'';;
    $geoStamp_source = $geoStamp['source'].'';;
    $geoStamp_coordinates = $geoStamp['coordinates'];
    $geoStamp_coordinates_latitude = $geoStamp_coordinates['latitude'].'';;
    $geoStamp_coordinates_longitude = $geoStamp_coordinates['longitude'].'';;
    $geoStamp_coordinates_altitude = $geoStamp_coordinates['altitude'].'';;
    $geoStamp_address = $geoStamp['address'].'';;
    $pages = $json['pages'];

    $cliente = "";
    $tipoCliente = "";
    $direccion = "";
    $tipoTrabajo = "";

    foreach($pages as $key => $page_data){
        $pagina = $key+1;
        $pagina_nombre = $page_data['name'];
        $sql_muestra_causas = array();
        $answers = $page_data["answers"];

        foreach($answers as $answer){

            $label    = $answer['label'];
            $dataType = $answer['dataType'];
            $question = $answer['question'];
            $values   = $answer['values'];
            $agregar = false;

            $labelita = limpiar(trim(strtolower($label)));

            if($pagina_nombre == "Información General"){
                if($labelita == 'cliente')
                    $cliente = "'".(isset($values[0]) ? $values[0] : '')."'";
                if($labelita == 'tipo de cliente')
                    $tipoCliente = "'".(isset($values[0]) ? $values[0] : '')."'";
                if($labelita == 'direccion')
                    $direccion = "'".(isset($values[0]) ? $values[0] : '')."'";
                if($labelita == 'tipo de trabajo')
                    $tipoTrabajo = "'".(isset($values[0]) ? $values[0] : '')."'";
                if($question == "Fecha y Hora de la Revisión:")
                    $fecha = "'".str_replace("T", " ", $values[0]['provided']['time'])."'";
            }
            if(strpos($pagina_nombre,"Información del Área #") !== false){
                if($question == "Descripción del Área:"){
                    $areas[] = "'".(isset($values[0]) ? $values[0] : '')."'";
                }
                if($question == "Tipo de Equipo:"){
                    $tiposEquipos[] = "'".(isset($values[0]) ? $values[0] : '')."'";
                }
                if($question == "Descripción del Equipo:"){
                    $descripcionEquipo[] = "'".(isset($values[0]) ? $values[0] : '')."'";
                }
            }
            if($pagina_nombre == "Observaciones Generales"){
                if($labelita == "obs. generales"){
                    $observaciones =  "'".(isset($values[0]) ? $values[0] : '')."'";
                }
            }
        }
    }

    $consulta = "INSERT INTO orden_trabajo(cliente,tipo_cliente,direccion,tipo_trabajo,fecha,observaciones) values($cliente,$tipoCliente,$direccion,$tipoTrabajo,$fecha,$observaciones);";
    $mysqli->query($consulta);
    $id_orden = $mysqli->insert_id;

    foreach($areas as $x => $item){
        $area = $areas[$x];
        $tipo_equipo = $tiposEquipos[$x];
        $descripcion_equipo = $descripcionEquipo[$x];
        if($area != "''" || $tipo_equipo != "''" || $descripcion_equipo != "''"){
            $sub_consulta = "INSERT INTO orden_trabajo_detalle(id_orden,area,tipo_equipo,des_equipo,tipo_trabajo) values($id_orden,$area,$tipo_equipo,$descripcion_equipo,$tipoTrabajo);";
            $mysqli->query($sub_consulta);
        }
    }
}

function limpiar($String){
    $String = str_replace(array('á','à','â','ã','ª','ä'),"a",$String);
    $String = str_replace(array('Á','À','Â','Ã','Ä'),"A",$String);
    $String = str_replace(array('Í','Ì','Î','Ï'),"I",$String);
    $String = str_replace(array('í','ì','î','ï'),"i",$String);
    $String = str_replace(array('é','è','ê','ë'),"e",$String);
    $String = str_replace(array('É','È','Ê','Ë'),"E",$String);
    $String = str_replace(array('ó','ò','ô','õ','ö','º'),"o",$String);
    $String = str_replace(array('Ó','Ò','Ô','Õ','Ö'),"O",$String);
    $String = str_replace(array('ú','ù','û','ü'),"u",$String);
    $String = str_replace(array('Ú','Ù','Û','Ü'),"U",$String);
    $String = str_replace(array('[','^','´','`','¨','~',']'),"",$String);
    $String = str_replace("ç","c",$String);
    $String = str_replace("Ç","C",$String);
    $String = str_replace("ñ","n",$String);
    $String = str_replace("Ñ","N",$String);
    $String = str_replace("Ý","Y",$String);
    $String = str_replace("ý","y",$String);
     
    $String = str_replace("&aacute;","a",$String);
    $String = str_replace("&Aacute;","A",$String);
    $String = str_replace("&eacute;","e",$String);
    $String = str_replace("&Eacute;","E",$String);
    $String = str_replace("&iacute;","i",$String);
    $String = str_replace("&Iacute;","I",$String);
    $String = str_replace("&oacute;","o",$String);
    $String = str_replace("&Oacute;","O",$String);
    $String = str_replace("&uacute;","u",$String);
    $String = str_replace("&Uacute;","U",$String);
    return $String;
}

?>