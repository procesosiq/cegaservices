<?php
header('Content-Type: text/json; charset=utf-8');

$mysqli = @new mysqli("localhost", "auditoriasbonita", "u[V(fTIUbcVb", "demo");
if (mysqli_connect_errno()) {
    printf("Falló la conexión: %s\n", mysqli_connect_error());
    exit();
}
$mysqli->set_charset("utf8");

function lista_ordenes($data,$mysqli){
	
	$records["data"] = array();
    $resultado = $mysqli->query("SELECT id,cliente,tipo_cliente,tipo_trabajo,DATE_FORMAT(fecha,'%d/%m/%Y') AS fecha,'0' AS num_equipos 
	FROM orden_trabajo");
	$cont=0;
    while($fila = $resultado->fetch_assoc()){ $cont++;	
		$records["data"][] = array('<input type="checkbox" name="chk_'.$cont.'" id="chk_'.$cont.'" />',
		$cont,$fila[fecha],$fila[cliente],$fila[tipo_cliente],$fila[tipo_trabajo],
		$fila[num_equipos],'<select name="s_'.$cont.'" id="s_'.$cont.'"><option value="1">Activo</option></select>','<button class="btn btn-sm green btn-outline filter-submit margin-bottom">Ver</button>');
    }
	$records["recordsTotal"] = $cont;
    return json_encode($records);
}

function add_ordenes($data,$mysqli){
	$mysqli->real_query("INSERT INTO orden_trabajo SET cliente='$data->cliente',
	direccion='$data->direccion',tiempo_estimado='$data->tiempo',tipo_cliente='$data->tipo_cliente',
	tipo_trabajo='$data->tipo_trabajo',fecha='$data->fecha';");
	$ids=$mysqli->insert_id;
	
	foreach($data->areas as $areaa){ 
		$mysqli->real_query("INSERT INTO orden_trabajo_detalle SET id_orden='$ids',orden_trabajo_detalle.`area`='".$areaa['area']."',
		tipo_equipo='".$areaa['tequipo']."',des_equipo='".$areaa['desequipo']."',tipo_trabajo='$data->tipo_trabajo'");
	}
	echo 1;
}	

function add_area_tmp($data,$mysqli){
	$mysqli->real_query("INSERT INTO orden_trabajo_tmp SET orden_trabajo_tmp.`area`='$data->area',
	tipo_equipo='$data->tipo_equipo',des_equipo='$data->des_equipo',tipo_trabajo='$data->trabajo_tipo'");
	echo 1;
}	

$postdata = (object)$_REQUEST;
if(isset($postdata->opt) && $postdata->opt == "LISTA_ORDENES"){
    $result = lista_ordenes($postdata,$mysqli);
    echo $result;
}

$postdata = (object)json_decode(file_get_contents("php://input"));
if(isset($postdata->opt) && $postdata->opt == "ADD_ORDEN"){
    $result = add_ordenes($postdata,$mysqli);
    echo $result;
}
if(isset($postdata->opt) && $postdata->opt == "ADD_AREA"){
    $result = add_area_tmp($postdata,$mysqli);
    echo $result;
}