	var app = angular.module('cegaservice', ['checklist-model']);

    app.controller('orderController', ['$scope','$http','$interval','bonitaHttp','$controller', function($scope,$http,$interval,bonitaHttp, $controller){
        $scope.formData = {
        	areas : [],
        	roles : [],
        	trabajos_tipos : ['Correctivo' , 'Instalacion' , 'Mantenimiento']
        };

        $scope.addRow = function(){
        	var data = {
        		area : $("#area").val(),
        		tequipo : $("#tipo_equipo").val(),
        		desequipo : $("#des_equipo").val()
        	}

        	if(data){
        		console.log(data);
        		$scope.formData.areas.push(data);
        	}
        }

        $scope.deleteArea = function(area , index){
        	var data = area || {};
        	if(data.area){
        		try{
        			console.log(data);
        			delete $scope.formData.areas[index];
        			$scope.formData.areas.slice(index , 1);
        		}catch(e){
        			throw Error("Error al Elminar");
        		}
        	}
        }

        $(".save").on("click" , function(){
            console.log($scope.formData);
        })

    }])