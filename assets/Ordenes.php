<?php

/**
* 
*/
class Ordenes
{
	
    private $conexion;
    private $session;
    
    public function __construct(){
        $this->conexion = new M_Conexion;
        // $this->session = Session::getInstance();
    }
    

    public function index(){
    	 $datos = (object)$_POST;
        
        $sWhere = "";
        $sOrder = " ORDER BY nombre";
        $DesAsc = "ASC";
        $sOrder .= " {$DesAsc}";
        $sLimit = "";
        // print_r($_POST);
        if(isset($_POST)){

            /*----------  ORDER BY ----------*/
            
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY id {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY fecha {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY cliente {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY  tipo_cliente {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 5){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY tipo_trabajo {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 6){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY num_equipos {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 7){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY status {$DesAsc}";
            }
            /*----------  ORDER BY ----------*/

            if(isset($_POST['order_id']) && trim($_POST['order_id']) != ""){
                $sWhere .= " AND id = ".$_POST["order_id"];
            }               
            if((isset($_POST['order_date_from']) && trim($_POST['order_date_from']) != "") && (isset($_POST['order_date_to']) && trim($_POST['order_date_to']) != "")){
                $sWhere .= " AND fecha BETWEEN '".$_POST["order_date_from"]."' AND '".$_POST["order_date_to"]."'";
            }
            if(isset($_POST['search_t_equipo']) && trim($_POST['search_t_equipo']) != ""){
                $sWhere .= " AND cliente LIKE '%".$_POST['search_t_equipo']."%'";
            }
            if(isset($_POST['order_ship_to']) && trim($_POST['order_ship_to']) != ""){
                $sWhere .= " AND (SELECT nombre FROM cat_clientesTipo WHERE id = tipo_cliente) LIKE '%".$_POST['order_ship_to']."%'";
            }
            if(isset($_POST['order_tipo_trabajo']) && trim($_POST['order_tipo_trabajo']) != ""){
                $sWhere .= " AND tipo_trabajo LIKE '%".$_POST['order_tipo_trabajo']."%'";
            }
            if(isset($_POST['order_numero_equipo']) && trim($_POST['order_numero_equipo']) != ""){
                $sWhere .= " AND num_equipos LIKE '%".$_POST['order_numero_equipo']."%'";
            }
            if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
                $sWhere .= " AND status = ".$_POST['order_status'];
            }
            // if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
            //     $sWhere .= " AND cat_equipos.`status` = ".$_POST["order_status"];
            // }

            /*----------  LIMIT  ----------*/
            if(isset($_POST['length']) && $_POST['length'] > 0){
                $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
            }
        }

        $sql = "SELECT id,cliente,(SELECT nombre FROM cat_clientesTipo WHERE id = tipo_cliente) as tipo_cliente,
        tipo_trabajo,DATE_FORMAT(fecha,'%Y-%m-%d') AS fecha,
        '0' AS num_equipos  , status
        FROM orden_trabajo 
        WHERE 1 = 1 
        $sWhere $sOrder $sLimit";
        $res = $this->conexion->link->query($sql);
        #print_r($sql);
        $response = (object)[];
        while($fila = $res->fetch_assoc()){
            $fila = (object)$fila;
        
            $response->data[] = array (
                '<input type="checkbox" name="id[]" value="'.$fila->id.'">',
                $fila->id,
                $fila->fecha,
                $fila->cliente,
                $fila->tipo_cliente,
                $fila->tipo_trabajo,
                $fila->num_equipos,
                '<button class="btn btn-sm '.(($fila->status==1)?'green-jungle':'red').'" id="status">'.(($fila->status==1)?'ACTIVO':'INACTIVO').'</button>',
                '<button id="edit" class="btn btn-sm green btn-outline filter-submit margin-bottom"><i class="fa fa-plus"></i> Editar</button>'
            );
        }

        $response->recordsTotal = count($response->data);
        #response->customActionMessage = "Informacion completada con exito";
        $response->customActionStatus = "OK";

        return json_encode($response);
    }

    public function save(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if((int)$data->idOrder>0){
            $mysqli->real_query("UPDATE orden_trabajo SET cliente='$data->cliente',
            direccion='$data->direccion',tiempo_estimado='$data->tiempo',tipo_cliente='$data->tipo_cliente',
            tipo_trabajo='$data->tipo_trabajo',fecha='$data->fecha' WHERE id='$data->idOrder';");
            $ids=$data->idOrder;
            
            $mysqli->real_query("DELETE FROM orden_trabajo_detalle WHERE id_orden='$data->idOrder';");
            foreach($data->areas as $areaa){ 
                $areaa = (array)$areaa;
                $mysqli->real_query("INSERT INTO orden_trabajo_detalle SET id_orden='$ids',orden_trabajo_detalle.`area`='".$areaa['area']."',
                tipo_equipo='".$areaa['tequipo']."',des_equipo='".$areaa['desequipo']."',tipo_trabajo='$data->tipo_trabajo'");
            }
            echo $ids;
        }
        else{
            $mysqli->real_query("INSERT INTO orden_trabajo SET cliente='$data->cliente',
            direccion='$data->direccion',tiempo_estimado='$data->tiempo',tipo_cliente='$data->tipo_cliente',
            tipo_trabajo='$data->tipo_trabajo',fecha='$data->fecha';");
            $ids=$mysqli->insert_id;
            // print_r($data);
            foreach($data->areas as $areaa){ 
                $areaa = (array)$areaa;
                $mysqli->real_query("INSERT INTO orden_trabajo_detalle SET id_orden='$ids',orden_trabajo_detalle.`area`='".$areaa['area']."',
                tipo_equipo='".$areaa['tequipo']."',des_equipo='".$areaa['desequipo']."',tipo_trabajo='$data->tipo_trabajo'");
            }
            echo $ids;
        }
    }


    public function edit(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if($postdata->idOrder){
            $resultado = $mysqli->query("SELECT id,cliente,tipo_cliente,direccion,
            tipo_trabajo,DATE_FORMAT(fecha,'%Y-%m-%d') AS fecha,tiempo_estimado,
            FROM orden_trabajo WHERE id='$data->idOrder'");
            $ordenes = array();
            while($fila = $resultado->fetch_assoc()){ $cont++;  
                $ordenes[] = $fila;
            }
            
            $resultado = $mysqli->query("SELECT * FROM orden_trabajo_detalle WHERE id_orden='$data->idOrder'");
            while($fila = $resultado->fetch_assoc()){ $cont++;  
                $ordenes[] = $fila;
            }
        }
        return json_encode($ordenes);
    }


    public function ChangeStatus(){
        $datos = (object)$_POST;
        
        $sql="UPDATE orden_trabajo SET status=IF(status , 0 , 1) WHERE id='$datos->idorden'";
        $this->conexion->Consultas(1,$sql);
        echo true;
    }
}

?>