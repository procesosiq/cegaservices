<?php

function limpiar($String)
{
    $String = str_replace("u00C1", "Á", $String);
    $String = str_replace("u00c1", "Á", $String);
    $String = str_replace("u00E1", "á", $String);
    $String = str_replace("u00e1", "á", $String);
    $String = str_replace("u00c9", "É", $String);
    $String = str_replace("u00C9", "É", $String);
    $String = str_replace("u00E9", "é", $String);
    $String = str_replace("u00e9", "é", $String);
    $String = str_replace("u00CD", "Í", $String);
    $String = str_replace("u00cd", "Í", $String);
    $String = str_replace("u00ED", "í", $String);
    $String = str_replace("u00D3", "Ó", $String);
    $String = str_replace("u00d3", "Ó", $String);
    $String = str_replace("u00F3", "ó", $String);
    $String = str_replace("u00f3", "ó", $String);
    $String = str_replace("u00DA", "Ú", $String);
    $String = str_replace("u00dA", "Ú", $String);
    $String = str_replace("u00FA", "ú", $String);
    $String = str_replace("u00fA", "ú", $String);
    $String = str_replace("u00F1", "Ñ", $String);
    $String = str_replace("u00f1", "Ñ", $String);
    return $String;
}

require_once("dompdf/dompdf_config.inc.php");
$host="procesos-iq.com";
$db_user="auditoriasbonita";
$db_pass="u[V(fTIUbcVb";
$db_name="cegaservices2";

$link = new mysqli($host, $db_user, $db_pass, $db_name);
if($link->connect_errno){ 
	echo "Fallo al conectar a MySQL: ". $link->connect_error; 
	return;
}

$ids_orden = "";

$sql = "SELECT id FROM orden_trabajo WHERE id_cotizacion='$_GET[id]'";
$res = $link->query($sql);
while($rowid = $res->fetch_assoc()){
	$ids_orden .= $rowid["id"].",";
}

if($ids_orden != ""){
	$ids_orden = substr($ids_orden,0,-1);
}
else
	$ids_orden = 0;

$conEqui = 0;
$sql = "SELECT * FROM (
(SELECT 
  'correctivo' AS tipoDato,geolocalizacion,
  fecha_correctivo AS fecTrabajo,
  hora_llegada,
  cliente,
  hora_programada,
  sucursal,
  direccion,
  referencia,
  detalle_trabajo,
  observacion1,
  order_time,
  responsable,
  cargo_responsable,
  img_firma,
  observa_fin,
  audio_comentario_gral,
  'reportes_r_correctivo' as tbl,
  id
FROM `reportes_r_correctivo` 
WHERE id_orden IN($ids_orden) ORDER BY order_time)


UNION ALL 


(SELECT 
  'instalacion' AS tipoDato,geolocalizacion,
  fecha_instalacion AS fecTrabajo,
  hora_llegada,
  cliente,
  hora_programada,
  sucursal,
  direccion,
  referencia,
  detalle_trabajo,
  observacion1,
  order_time,
  responsable,
  cargo_responsable,
  img_firma,
  observa_fin,
  audio_comentario_gral,
  'reportes_r_instalacion' as tbl,
  id
FROM `reportes_r_instalacion` 
WHERE id_orden IN($ids_orden) ORDER BY order_time)

UNION ALL

(SELECT 
  'revision_mantenimiento' AS tipoDato,
  `geolocalizacion`,
  `fecha_mante` AS fecTrabajo,
  `hora_llegada`,
  `cliente`,
  `hora_programada`,`sucursal`,
  `direccion`,
  `referencia`,
  `detalle_trabajo`,
  `infg_observa` AS observacion1,
  `observa_responsable` AS responsable,
  `observa_resp_cargo` AS cargo_responsable,
  '' as order_time,
  `observa_firma` AS img_firma,
  `observa_notas` AS observa_fin,
  `observa_audio` AS audio_comentario_gral,
  'reportes_revision_mantenimiento' as tbl,
  id
FROM `reportes_revision_mantenimiento` 
WHERE id_orden IN($ids_orden) ORDER BY order_time)

UNION ALL

(SELECT 
  'mantenimiento' AS tipoDato,geolocalizacion,
  fecha_mante AS fecTrabajo,
  hora_llegada,
  cliente,
  hora_programada,
  sucursal,
  direccion,
  referencia,
  detalle_trabajo,
  observacion1,
  order_time,
  responsable,
  cargo_responsable,
  img_firma,
  observa_fin,
  audio_comentario_gral,
  'reportes_r_mantenimiento' as tbl,
  id
FROM `reportes_r_mantenimiento` 
WHERE id_orden IN($ids_orden) ORDER BY order_time)

UNION ALL

(SELECT 
  'revision_instalacion' AS tipoDato,geolocalizacion,
  fecha_instalacion AS fecTrabajo,
  hora_llegada,
  cliente,
  hora_programada,
  sucursal,
  direccion,
  referencia,
  detalle_trabajo,
  observacion1,
  order_time,
  responsable,
  cargo_responsable,
  img_firma,
  '' as observa_fin,
  audio_comentario_gral,
  'reportes_revision_instalacion' as tbl,
  id
FROM `reportes_revision_instalacion` 
WHERE id_orden IN($ids_orden) ORDER BY order_time)

UNION ALL

(SELECT 
  'revision_correctivo' AS tipoDato,geolocalizacion,
  fecha_instalacion AS fecTrabajo,
  hora_llegada,
  cliente,
  hora_programada,
  sucursal,
  direccion,
  referencia,
  detalle_trabajo,
  observacion1,
  order_time,
  responsable,
  cargo_responsable,
  img_firma,
  '' as observa_fin,
  audio_comentario_gral,
  'reportes_revision_correctivo' as tbl,
  id
FROM `reportes_revision_correctivo` 
WHERE id_orden IN($ids_orden) ORDER BY order_time)

) AS tab1 ORDER BY order_time
";

$sql_firma = "SELECT * FROM (
(SELECT 
  'correctivo' AS tipoDato,geolocalizacion,
  fecha_correctivo AS fecTrabajo,
  hora_llegada,
  cliente,
  hora_programada,
  sucursal,
  direccion,
  referencia,
  detalle_trabajo,
  observacion1,
  order_time,
  responsable,
  cargo_responsable,
  img_firma,
  observa_fin,
  audio_comentario_gral 
FROM `reportes_r_correctivo` 
WHERE id_orden IN($ids_orden) AND img_firma != '' ORDER BY order_time)


UNION ALL 


(SELECT 'instalacion' AS tipoDato,geolocalizacion,
fecha_instalacion AS fecTrabajo,
hora_llegada,
cliente,
hora_programada,
sucursal,
direccion,
referencia,
detalle_trabajo,
observacion1,
order_time,
responsable,
cargo_responsable,
img_firma,
observa_fin,
audio_comentario_gral 

FROM `reportes_r_instalacion` 
WHERE id_orden IN($ids_orden) AND img_firma != '' ORDER BY order_time)

UNION ALL

(SELECT 'revision_mantenimiento' AS tipoDato,`geolocalizacion`,
`fecha_mante` AS fecTrabajo,`hora_llegada`,`cliente`,
`hora_programada`,`sucursal`,`direccion`,`referencia`,`detalle_trabajo`,
`infg_observa` AS observacion1,
'' as order_time,
`observa_responsable` AS responsable,
`observa_resp_cargo` AS cargo_responsable,
`observa_firma` AS img_firma,
`observa_notas` AS observa_fin,
`observa_audio` AS audio_comentario_gral


FROM `reportes_revision_mantenimiento` 
WHERE id_orden IN($ids_orden) ORDER BY order_time)

UNION ALL

(SELECT 'mantenimiento' AS tipoDato,geolocalizacion,
fecha_mante AS fecTrabajo,
hora_llegada,
cliente,
hora_programada,
sucursal,
direccion,
referencia,
detalle_trabajo,
observacion1,
order_time,
responsable,
cargo_responsable,
img_firma,
observa_fin,
audio_comentario_gral 

FROM `reportes_r_mantenimiento` 
WHERE id_orden IN($ids_orden) AND img_firma != '' ORDER BY order_time)

UNION ALL

(SELECT 'revision_instalacion' AS tipoDato,geolocalizacion,
fecha_instalacion AS fecTrabajo,
hora_llegada,
cliente,
hora_programada,
sucursal,
direccion,
referencia,
detalle_trabajo,
observacion1,
order_time,
responsable,
cargo_responsable,
img_firma,
observa_fin,
audio_comentario_gral 

FROM `reportes_revision_instalacion` 
WHERE id_orden IN($ids_orden) AND img_firma != '' ORDER BY order_time)

UNION ALL

(SELECT 'revision_correctivo' AS tipoDato,geolocalizacion,
fecha_instalacion AS fecTrabajo,
hora_llegada,
cliente,
hora_programada,
sucursal,
direccion,
referencia,
detalle_trabajo,
observacion1,
order_time,
responsable,
cargo_responsable,
img_firma,
observa_fin,
audio_comentario_gral 

FROM `reportes_revision_correctivo` 
WHERE id_orden IN($ids_orden) AND img_firma != '' ORDER BY order_time)
) AS tab1";

$res = $link->query($sql);
$rowEnc = $res->fetch_assoc();
$tipo = $rowEnc["tipoDato"];
$row2 = [];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script src="http://cdn.procesos-iq.com/global/plugins/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};

$(document).ready(function(){
    var editado = false;
    var editados = {}

    $(document.body).on('keyup','.editor', function() {
        if(!editado){
            editado = true
            $("#guardar").removeClass("hide")
        }
        var _column = $(this).attr("data-column")
        var newValue = $(this)[0].innerHTML
        var _table = $(this).attr("data-table")
        var _id = $(this).attr("data-id")

        if(_column == "herramientas"){
            newValue = newValue.replaceAll("<br>", "|")
        }
        if(!editados[_table]){
            editados[_table] = []
        }
        if(!editados[_table][_id]){
            editados[_table][_id] = {}
        }
        editados[_table][_id][_column] = { table : _table, id : _id, value : newValue, column : _column }
        //console.log(editados)
    });

    $("#guardar").click(function(){
        var querys = ""
        $.each(editados, function(tabla, values){
            var sql = "UPDATE " + tabla + " SET";
            $.each(values, function(_id, values_id){
                if(values_id != undefined){
                    $.each(values_id, function(campo, valor){
                        console.log(valor)
                        sql += " " + campo + " = '" + valor.value + "',"
                    })
                    sql = sql.substring(0, sql.length - 1);
                    querys += sql + " WHERE id = " + _id +";";
                }
            })
        })
        /*var sql="",table=" ",campos=" ",where="";
        $("table tr td").each(function(indice){
            if($(this).attr('data-table')!= undefined && $(this).attr('data-table')!=''){
                table=' update  '+$(this).attr('data-table')+' set  ';
            }
            
            if($(this).attr('data-column')!=undefined){
                campos+=$(this).attr('data-column')+'='+'"'+$(this).html().trim()+'",';
            }
        });
        campos = campos.replace('&nbsp;',' ');
        campos = campos.replace('nbsp;',' ');
        sql += table + campos.slice(0,-1) + " where id=<?= $rowEnc["id"] ?>";*/
        $.ajax({
            type:'post',
            url: "controllers/index.php?accion=Reportes.updateReporte",
            data:'querys='+querys,
            success:function(data){
                if(data>=0){
                    alert('Se ha actualizado con exito');
                    location.reload();
                }
            }
        });
    })
})
</script>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>REPORTES</title>
<style type="text/css">
body{
	background-color:#999;
}
.lblone{
	font-family:Arial, Helvetica, sans-serif;
	font-weight:bold;
	font-size:12px;
}
.lbltwo{
	font-family:Arial, Helvetica, sans-serif;
	font-weight:bold;
	font-size:12px;
	color:#002E5B;
}
.lblthree{
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
	border-bottom:1px dotted #999999;
}
.lbltitle{
	font-family:Arial, Helvetica, sans-serif;
	font-weight:bold;
	font-size:14px;
	border-bottom: 3px solid #035719;
	color:#035719;
}
.tblwithe{
	background-color:#FFF;
}
.separa{
	border-top: 10px solid #D09B35;
}
.btn {
    float: right;
    margin: 5px;
    box-shadow: none!important;
    display: inline-block;
    margin-bottom: 0;
    font-weight: 400;
    text-align: center;
    vertical-align: middle;
    touch-action: manipulation;
    cursor: pointer;
    border: 1px solid transparent;
    background: #26C281!important;
    color: #FFF!important;
    padding: 5px 10px;
    font-size: 12px;
    line-height: 1.5;
}
.hide {
    display : none;
}
</style>
</head>

<body>
<div style="width:100%; height:100%;" align="center">
<div style="width:910px; background-color:#FFF; padding:5px;">

<!-- ENCABEZADO DEL FORMATO -->

<?php if($rowEnc["geolocalizacion"] != ""){ ?>
    <table width="840" border="0" align="center" cellpadding="0" cellspacing="0" class="tblwithe">
      <tr>
        <td>
            <img src="logocega.jpg" width="275" height="80" />
            <button class="btn hide" id="guardar">Guardar</button>
        </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td colspan="2" class="lbltitle">INFORMACION GENERAL</td>
            </tr>
            <tr>
              <td width="29%" height="21" class="lblone">Geolocalizaci&oacute;n</td>
              <td width="71%" class="lblthree"><a href="https://www.google.com/maps?q=<?php echo $rowEnc["geolocalizacion"];?>" target="_blank">Ver Mapa</a></td>
            </tr>
            <tr>
              <td class="lblone" height="21">Fecha de Trabajo</td>
              <td class="lblthree"><?=$rowEnc["fecTrabajo"]?></td>
            </tr>
            <tr>
              <td class="lblone" height="21">Hora de Llegada</td>
              <td class="lblthree"><?=$rowEnc["hora_llegada"]?></td>
            </tr>
            <tr>
              <td class="lblone" height="21">Cliente</td>
              <td class="lblthree"><?=utf8_encode($rowEnc["cliente"])?></td>
            </tr>
            <tr>
              <td class="lblone" height="21">Hora Programada</td>
              <td class="lblthree"><?=$rowEnc["hora_programada"]?></td>
            </tr>
            <tr>
              <td class="lblone" height="21">Sucursal</td>
              <td class="lblthree editor" contenteditable="true" data-id="<?= $rowEnc["id"] ?>" data-table="<?= $rowEnc["tbl"] ?>" data-column="sucursal"><?=utf8_encode($rowEnc["sucursal"])?></td>
            </tr>
            <tr>
              <td class="lblone" height="21">Direcci&oacute;n Sucursal</td>
              <td class="lblthree editor" contenteditable="true" data-id="<?= $rowEnc["id"] ?>" data-table="<?= $rowEnc["tbl"] ?>" data-column="direccion"><?=utf8_encode($rowEnc["direccion"])?></td>
            </tr>
            <tr>
              <td class="lblone" height="21">Referencia</td>
              <td class="lblthree editor" contenteditable="true" data-id = "<?= $rowEnc["id"] ?>" data-table="<?= $rowEnc["tbl"] ?>" data-column="referencia"><?=utf8_encode($rowEnc["referencia"])?></td>
            </tr>
            <tr>
              <td class="lblone" height="21">Detalle del Trabajo</td>
              <td class="lblthree editor" contenteditable="true" data-id = "<?= $rowEnc["id"] ?>" data-table="<?= $rowEnc["tbl"] ?>" data-column="detalle_trabajo"><?=utf8_encode($rowEnc["detalle_trabajo"])?></td>
            </tr>
          <?php
    	  if($rowEnc["observacion1"] != ''){ ?>
            <tr>
              <td class="lblone" height="21">Observaciones</td>
              <td class="lblthree editor" contenteditable="true" data-id = "<?= $rowEnc["id"] ?>" data-table="<?= $rowEnc["tbl"] ?>" data-column="observacion1"><?=utf8_encode($rowEnc["observacion1"])?></td>
            </tr>
        <?php }?>
        </table>
      </td>
    </tr>
    </table>

<?php }?>

<!-- FINAL DEL ENCABEZADO DEL FORMATO -->


<!-- --------------------------------------------------------------------------------------------------------------------------------- -->


<!-- CORRECTIVO -->

<?php 

$sql = "SELECT * FROM `reportes_r_correctivo` WHERE id_orden IN($ids_orden) ORDER BY order_time";
#echo $sql;
$res = $link->query($sql);

while($row = $res->fetch_assoc()){
	$conEqui++;
?>

<?php if($row["tipo_reporte"] == "CORRECTIVO B"){?>
<table width="840" border="0" align="center" cellpadding="0" cellspacing="0" class="tblwithe">
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="separa">&nbsp;</td>
    </tr>
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2" class="lbltitle">INFORMACION DEL EQUIPO #<?php echo $conEqui;?></td>
                </tr>
                <tr>
                    <td width="29%" class="lblone" height="21">Tipo de Equipo</td>
                    <td width="71%" class="lblthree" data-id = "<?= $rowEnc["id"] ?>" data-table="<?= $rowEnc["tbl"] ?>" data-column="tipo_equipo"><?=utf8_encode($row["tipo_equipo"])?></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2" class="lbltitle">CARACTERISTICAS EQUIPO (CLIMATIZACION B)</td>
                </tr>
                <?php
                if($row["codigo_equipo"] != ''): ?>
                <tr>
                    <td width="29%" class="lblone" height="21">C&oacute;digo del Equipo</td>
                    <td width="71%" class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="codigo_equipo"><?=utf8_encode($row["codigo_equipo"])?></td>
                </tr>
                <?php endif; ?>
      
      <?php
	  if($row["desc_equipo"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n del Equipo</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="desc_equipo"><?=utf8_encode($row["desc_equipo"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if(trim($row["area_clima"]) != ''){ ?>
      <tr>
        <td class="lblone" height="21">Area Que Climatiza</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="area_clima"><?=utf8_encode($row["area_clima"])?></td>
      </tr>
      <?php }?>
      
       <?php
	  if($row["nombre_area"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Nombre del Area</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="nombre_area"><?=utf8_encode($row["nombre_area"])?></td>
      </tr>
      <?php }?>
      <tr>
        <td class="lbltwo" height="21">Evaporador</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <?php
	  if($row["marca_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Marca</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="marca_eva"><?=utf8_encode($row["marca_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["btu_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Capacidad (BTU)</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="btu_eva"><?=utf8_encode($row["btu_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["modelo_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Modelo</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="modelo_eva"><?=utf8_encode($row["modelo_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["serie_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Serie</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="serie_eva"><?=utf8_encode($row["serie_eva"])?></td>
      </tr>
      <?php }?>
      <tr>
        <td class="lbltwo" height="21">Condensador</td>
        <td>&nbsp;</td>
      </tr>
      
      <?php
	  if($row["marca_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Marca</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="marca_conde"><?=utf8_encode($row["marca_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["btu_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Capacidad (BTU)</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="btu_conde"><?=utf8_encode($row["btu_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["modelo_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Modelo</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="modelo_conde"><?=utf8_encode($row["modelo_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["serie_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Serie</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="serie_conde"><?=utf8_encode($row["serie_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["observa2"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="observa2"><?=utf8_encode($row["observa2"])?></td>
      </tr>
      <?php }?>
      
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">CORRECTIVO EQUIPO (CLIMATIZACION B)</td>
      </tr>
      <tr>
        <td height="21" class="lbltwo">Evaporador</td>
        <td>&nbsp;</td>
      </tr>
      
      <?php
	  if($row["correc_newrep_eva"] != ''){ ?>
      <tr>
        <td width="29%" height="21" valign="top" class="lblone">Instalaci&oacute;n Repuestos (Nuevo)</td>
        <td width="71%" class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="correc_newrep_eva"><?=utf8_encode($row["correc_newrep_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["correc_reparep_eva"] != ''){ ?>
      <tr>
        <td height="21" valign="top" class="lblone">Instalaci&oacute;n Repuestos (Reparados)</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="correc_reparep_eva"><?=utf8_encode($row["correc_reparep_eva"])?></td>
      </tr>
      <?php }?>
      
      <tr>
        <td height="21" class="lbltwo">Condensador</td>
        <td>&nbsp;</td>
      </tr>
      
      <?php
	  if($row["correc_newrep_conde"] != ''){ ?>
      <tr>
        <td height="21" valign="top" class="lblone">Instalaci&oacute;n Repuestos (Nuevo)</td> 
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="correc_newrep_conde"><?=utf8_encode($row["correc_newrep_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["correc_reparep_conde"] != ''){ ?>
      <tr>
        <td height="21" valign="top" class="lblone">Instalaci&oacute;n Repuestos (Reparados)</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="correc_reparep_conde"><?=utf8_encode($row["correc_reparep_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["correc_audio"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Comentarios</td> 
        <td class="lblthree"><a href="reportes_vistos/correctivo/<?php echo $row["correc_audio"];?>">Audio de Comentarios</a></td>
      </tr>
      <?php }?>
      
       <?php
	  if($row["correc_descrip"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n del Trabajo Realizado</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="correc_descrip"><?=utf8_encode($row["correc_descrip"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["correc_fotos"] != ''){ ?>
      <tr>
        <td class="lbltwo" height="21">Captura de Fotos</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" class="lblone">
        <?php
        	if($row["correc_fotos"] != ''){
				$fotos = explode("|", substr($row["correc_fotos"],0,-1));
				for($i=0;$i<count($fotos);$i++){
					?><a href="reportes_vistos/correctivo/<?= $fotos[$i] ?>"><img src="reportes_vistos/correctivo/<?= $fotos[$i] ?>" width="143" height="147" />&nbsp;<?
				}
			}
		?>
        </td>
        </tr>
     <?php }?>
     
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <?php
	  if($row["mat_item1_sel"] != ''){ ?>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">MATERIALES REQUERIDOS</td>
      </tr>
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #1</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mar_item1_sel"><?=utf8_encode($row["mat_item1_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item1_des"><?=utf8_encode($row["mat_item1_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item1_uni"><?=utf8_encode($row["mat_item1_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item1_cant"><?=utf8_encode($row["mat_item1_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item1_obser"><?=utf8_encode($row["mat_item1_obser"])?></td>

      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item2_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #2</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item2_sel"><?=utf8_encode($row["mat_item2_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item2_des"><?=utf8_encode($row["mat_item2_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item2_uni"><?=utf8_encode($row["mat_item2_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item2_cant"><?=utf8_encode($row["mat_item2_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item2_obser"><?=utf8_encode($row["mat_item2_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item3_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #3</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item3_sel"><?=utf8_encode($row["mat_item3_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item3_des"><?=utf8_encode($row["mat_item3_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item3_uni"><?=utf8_encode($row["mat_item3_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item3_cant"><?=utf8_encode($row["mat_item3_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item3_obser"><?=utf8_encode($row["mat_item3_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item4_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #4</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item4_sel"><?=utf8_encode($row["mat_item4_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item4_des"><?=utf8_encode($row["mat_item4_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item4_uni"><?=utf8_encode($row["mat_item4_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item4_cant"><?=utf8_encode($row["mat_item4_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item4_obser"><?=utf8_encode($row["mat_item4_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item5_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #5</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item5_sel"><?=utf8_encode($row["mat_item5_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item5_des"><?=utf8_encode($row["mat_item5_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item5_uni"><?=utf8_encode($row["mat_item5_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item5_cant"><?=utf8_encode($row["mat_item5_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item5_obser"><?=utf8_encode($row["mat_item5_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item6_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #6</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item6_sel"><?=utf8_encode($row["mat_item6_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item6_des"><?=utf8_encode($row["mat_item6_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item6_uni"><?=utf8_encode($row["mat_item6_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item6_cant"><?=utf8_encode($row["mat_item6_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item6_obser"><?=utf8_encode($row["mat_item6_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item7_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #7</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item7_sel"><?=utf8_encode($row["mat_item7_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item7_des"><?=utf8_encode($row["mat_item7_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item7_uni"><?=utf8_encode($row["mat_item7_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item7_cant"><?=utf8_encode($row["mat_item7_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item7_obser"><?=utf8_encode($row["mat_item7_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item8_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #8</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item8_sel"><?=utf8_encode($row["mat_item8_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item8_des"><?=utf8_encode($row["mat_item8_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item8_uni"><?=utf8_encode($row["mat_item8_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item8_cant"><?=utf8_encode($row["mat_item8_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item8_obser"><?=utf8_encode($row["mat_item8_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item9_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #9</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item9_sel"><?=utf8_encode($row["mat_item9_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item9_des"><?=utf8_encode($row["mat_item9_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item9_uni"><?=utf8_encode($row["mat_item9_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item9_cant"><?=utf8_encode($row["mat_item9_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item9_obser"><?=utf8_encode($row["mat_item9_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item10_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #10</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item10_sel"><?=utf8_encode($row["mat_item10_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item10_des"><?=utf8_encode($row["mat_item10_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item10_uni"><?=utf8_encode($row["mat_item10_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item10_cant"><?=utf8_encode($row["mat_item10_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item10_obser"><?=utf8_encode($row["mat_item10_obser"])?></td>
      </tr>
    </table></td>
  </tr>
   <?php }?>
   <tr>
     <td>&nbsp;</td>
   </tr>
   <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">PRUEBAS FINALES DEL EQUIPO</td>
      </tr>
      <tr>
        <td width="29%" class="lblone" height="21">Tipo de Prueba (Horas)</td>
        <td width="71%" class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="pf_prueba_hora"><?=utf8_encode($row["pf_prueba_hora"])?></td>
      </tr>
      <tr>
        <td width="29%" class="lblone" height="21">Observaciones</td>
        <td width="71%" class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="pf_observa"><?=utf8_encode($row["pf_observa"])?></td>
      </tr>
    </table></td>
  </tr>
   
</table>

  <?php }?>
 
<?php if($row["tipo_reporte"] == "CORRECTIVO A"){?>
<table width="840" border="0" align="center" cellpadding="0" cellspacing="0" class="tblwithe">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="separa">&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">INFORMACION DEL EQUIPO #<?php echo $conEqui;?></td>
      </tr>
      <tr>
        <td width="29%" class="lblone" height="21">Tipo de Equipo</td>
        <td width="71%" class="lblthree" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="tipo_equipo"><?=utf8_encode($row["tipo_equipo"])?></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">CARACTERISTICAS EQUIPO (CLIMATIZACION A)</td>
      </tr>
      <?php
	  if($row["codigo_equipo"] != ''){ ?>
      <tr>
        <td width="29%" class="lblone" height="21">C&oacute;digo del Equipo</td>
        <td width="71%" class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="codigo_equipo"><?=utf8_encode($row["codigo_equipo"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["desc_equipo"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n del Equipo</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="desc_equipo"><?=utf8_encode($row["desc_equipo"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if(trim($row["area_clima"]) != ''){ ?>
      <tr>
        <td class="lblone" height="21">Area Que Climatiza</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="area_clima"><?=utf8_encode($row["area_clima"])?></td>
      </tr>
      <?php }?>
      
       <?php
	  if($row["nombre_area"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Nombre del Area</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="nombre_area"><?=utf8_encode($row["nombre_area"])?></td>
      </tr>
      <?php }?>
      <tr>
        <td class="lbltwo" height="21">Evaporador / Condensador</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <?php
	  if($row["marca_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Marca</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="marca_eva"><?=utf8_encode($row["marca_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["btu_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Capacidad (BTU)</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="btu_eva"><?=utf8_encode($row["btu_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["modelo_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Modelo</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="modelo_eva"><?=utf8_encode($row["modelo_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["serie_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Serie</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="serie_eva"><?=utf8_encode($row["serie_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["observa2"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="observa2"><?=utf8_encode($row["observa2"])?></td>
      </tr>
      <?php }?>
      
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">CORRECTIVO EQUIPO (CLIMATIZACION A)</td>
      </tr>
      <tr>
        <td height="21" class="lbltwo">Evaporador / Condensador</td>
        <td>&nbsp;</td>
      </tr>
      
      <?php
	  if($row["correc_newrep_eva"] != ''){ ?>
      <tr>
        <td width="29%" height="21" valign="top" class="lblone">Instalaci&oacute;n Repuestos (Nuevo)</td>
        <td width="71%" class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="correc_newrep_eva"><?=utf8_encode($row["correc_newrep_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["correc_reparep_eva"] != ''){ ?>
      <tr>
        <td height="21" valign="top" class="lblone">Instalaci&oacute;n Repuestos (Reparados)</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="correc_reparep_eva"><?=utf8_encode($row["correc_reparep_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["correc_audio"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Comentarios</td> 
        <td class="lblthree"><a href="reportes_vistos/correctivo/<?php echo $row["correc_audio"];?>">Audio de Comentarios</a></td>
      </tr>
      <?php }?>
      
       <?php
	  if($row["correc_descrip"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n del Trabajo Realizado</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="correc_descrip"><?=utf8_encode($row["correc_descrip"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["correc_fotos"] != ''){ ?>
      <tr>
        <td class="lbltwo" height="21">Captura de Fotos</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" class="lblone">
        <?php
        	if($row["correc_fotos"] != ''){
				$fotos = explode("|", substr($row["correc_fotos"],0,-1));
				for($i=0;$i<count($fotos);$i++){
					?><a href="reportes_vistos/correctivo/<?= $fotos[$i] ?>"><img src="reportes_vistos/correctivo/<?= $fotos[$i] ?>" width="143" height="147" /></a>&nbsp;<?
				}
			}
		?>
        </td>
        </tr>
     <?php }?>
     
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <?php
	  if($row["mat_item1_sel"] != ''){ ?>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">MATERIALES REQUERIDOS</td>
      </tr>
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #1</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item1_sel"><?=utf8_encode($row["mat_item1_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item1_des"><?=utf8_encode($row["mat_item1_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item1_uni"><?=utf8_encode($row["mat_item1_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item1_cant"><?=utf8_encode($row["mat_item1_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item1_obser"><?=utf8_encode($row["mat_item1_obser"])?></td>

      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item2_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #2</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item2_sel"><?=utf8_encode($row["mat_item2_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item2_des"><?=utf8_encode($row["mat_item2_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item2_uni"><?=utf8_encode($row["mat_item2_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item2_cant"><?=utf8_encode($row["mat_item2_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item2_obser"><?=utf8_encode($row["mat_item2_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item3_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #3</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item3_sel"><?=utf8_encode($row["mat_item3_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item3_des"><?=utf8_encode($row["mat_item3_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item3_uni"><?=utf8_encode($row["mat_item3_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item3_cant"><?=utf8_encode($row["mat_item3_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item3_obser"><?=utf8_encode($row["mat_item3_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item4_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #4</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item4_sel"><?=utf8_encode($row["mat_item4_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item4_des"><?=utf8_encode($row["mat_item4_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item4_uni"><?=utf8_encode($row["mat_item4_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item4_cant"><?=utf8_encode($row["mat_item4_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item4_obser"><?=utf8_encode($row["mat_item4_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item5_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #5</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item5_sel"><?=utf8_encode($row["mat_item5_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item5_des"><?=utf8_encode($row["mat_item5_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item5_uni"><?=utf8_encode($row["mat_item5_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item5_cant"><?=utf8_encode($row["mat_item5_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item5_obser"><?=utf8_encode($row["mat_item5_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item6_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #6</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item6_sel"><?=utf8_encode($row["mat_item6_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item6_des"><?=utf8_encode($row["mat_item6_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item6_uni"><?=utf8_encode($row["mat_item6_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item6_cant"><?=utf8_encode($row["mat_item6_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item6_obser"><?=utf8_encode($row["mat_item6_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item7_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #7</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item7_sel"><?=utf8_encode($row["mat_item7_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item7_des"><?=utf8_encode($row["mat_item7_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item7_uni"><?=utf8_encode($row["mat_item7_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item7_cant"><?=utf8_encode($row["mat_item7_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item7_obser"><?=utf8_encode($row["mat_item7_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item8_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #8</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item8_sel"><?=utf8_encode($row["mat_item8_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item8_des"><?=utf8_encode($row["mat_item8_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item8_uni"><?=utf8_encode($row["mat_item8_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item8_cant"><?=utf8_encode($row["mat_item8_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item8_obser"><?=utf8_encode($row["mat_item8_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item9_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #9</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item9_sel"><?=utf8_encode($row["mat_item9_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item9_des"><?=utf8_encode($row["mat_item9_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item9_uni"><?=utf8_encode($row["mat_item9_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item9_cant"><?=utf8_encode($row["mat_item9_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item9_obser"><?=utf8_encode($row["mat_item9_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item10_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #10</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item10_sel"><?=utf8_encode($row["mat_item10_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item10_des"><?=utf8_encode($row["mat_item10_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item10_uni"><?=utf8_encode($row["mat_item10_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item10_cant"><?=utf8_encode($row["mat_item10_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item10_obser"><?=utf8_encode($row["mat_item10_obser"])?></td>
      </tr>
    </table></td>
  </tr>
   <?php }?>
   
   <tr>
     <td>&nbsp;</td>
   </tr>
   <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">PRUEBAS FINALES DEL EQUIPO</td>
      </tr>
      <tr>
        <td width="29%" class="lblone" height="21">Tipo de Prueba (Horas)</td>
        <td width="71%" class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="pf_prueba_hora"><?=utf8_encode($row["pf_prueba_hora"])?></td>
      </tr>
      <tr>
        <td width="29%" class="lblone" height="21">Observaciones</td>
        <td width="71%" class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="pf_observa"><?=utf8_encode($row["pf_observa"])?></td>
      </tr>
    </table></td>
  </tr>
   
</table>

  <?php }?>
  
<?php if($row["tipo_reporte"] == "REFRIGERACION"){?>
<table width="840" border="0" align="center" cellpadding="0" cellspacing="0" class="tblwithe">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="separa">&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">INFORMACION DEL EQUIPO #<?php echo $conEqui;?></td>
      </tr>
      <tr>
        <td width="29%" class="lblone" height="21">Tipo de Equipo</td>
        <td width="71%" class="lblthree" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="tipo_equipo"><?=utf8_encode($row["tipo_equipo"])?></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">CARACTERISTICAS EQUIPO (REFRIGERACI&Oacute;N)</td>
      </tr>
      <?php
	  if($row["codigo_equipo"] != ''){ ?>
      <tr>
        <td width="29%" class="lblone" height="21">C&oacute;digo del Equipo</td>
        <td width="71%" class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="codigo_equipo"><?=utf8_encode($row["codigo_equipo"])?></td>
      </tr>
      <?php }?>
      <?php
	  if($row["desc_equipo"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n del Equipo</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="desc_equipo"><?=utf8_encode($row["desc_equipo"])?></td>
      </tr>
      <?php }?>
      <?php
	  if($row["marca_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Marca</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="marca_eva"><?=utf8_encode($row["marca_eva"])?></td>
      </tr>
      <?php }?>
      <?php
	  if($row["capacidad_hp_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Capacidad (HP)</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="capacidad_hp_eva"><?=utf8_encode($row["capacidad_hp_eva"])?></td>
      </tr>
      <?php }?>
      <?php
	  if($row["modelo_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Modelo</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="modelo_eva"><?=utf8_encode($row["modelo_eva"])?></td>
      </tr>
      <?php }?>
      <?php
	  if($row["serie_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Serie</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="serie_eva"><?=utf8_encode($row["serie_eva"])?></td>
      </tr>
      <?php }?>
      <?php
	  if($row["observa2"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="observa2"><?=utf8_encode($row["observa2"])?></td>
      </tr>
      <?php }?>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">CORRECTIVO EQUIPO (REFRIGERACI&Oacute;N)</td>
      </tr>
      <?php
	  if($row["correc_newrep_eva"] != ''){ ?>
      <tr>
        <td width="29%" height="21" valign="top" class="lblone">Instalaci&oacute;n Repuestos (Nuevo)</td>
        <td width="71%" class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="correc_newrep_eva"><?=utf8_encode($row["correc_newrep_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["correc_reparep_eva"] != ''){ ?>
      <tr>
        <td height="21" valign="top" class="lblone">Instalaci&oacute;n Repuestos (Reparados)</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="correc_reparep_eva"><?=utf8_encode($row["correc_reparep_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["correc_audio"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Comentarios</td> 
        <td class="lblthree"><a href="reportes_vistos/correctivo/<?php echo $row["correc_audio"];?>">Audio de Comentarios</a></td>
      </tr>
      <?php }?>
      
       <?php
	  if($row["correc_descrip"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n del Trabajo Realizado</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="correc_descrip"><?=utf8_encode($row["correc_descrip"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["correc_fotos"] != ''){ ?>
      <tr>
        <td class="lbltwo" height="21">Captura de Fotos</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" class="lblone">
        <?php
        	if($row["correc_fotos"] != ''){
				$fotos = explode("|", substr($row["correc_fotos"],0,-1));
				for($i=0;$i<count($fotos);$i++){
					?><a href="reportes_vistos/correctivo/<?= $fotos[$i] ?>"><img src="reportes_vistos/correctivo/<?= $fotos[$i] ?>" width="143" height="147"/></a>&nbsp;<?
				}
			}
		?>
        </td>
        </tr>
     <?php }?>
     
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <?php
	  if($row["mat_item1_sel"] != ''){ ?>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">MATERIALES REQUERIDOS</td>
      </tr>
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #1</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item1_sel"><?=utf8_encode($row["mat_item1_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item1_des"><?=utf8_encode($row["mat_item1_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item1_uni"><?=utf8_encode($row["mat_item1_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item1_cant"><?=utf8_encode($row["mat_item1_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item1_obser"><?=utf8_encode($row["mat_item1_obser"])?></td>

      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item2_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #2</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item2_sel"><?=utf8_encode($row["mat_item2_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item2_des"><?=utf8_encode($row["mat_item2_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item2_uni"><?=utf8_encode($row["mat_item2_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item2_cant"><?=utf8_encode($row["mat_item2_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item2_obser"><?=utf8_encode($row["mat_item2_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item3_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #3</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item3_sel"><?=utf8_encode($row["mat_item3_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item3_des"><?=utf8_encode($row["mat_item3_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item3_uni"><?=utf8_encode($row["mat_item3_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item3_cant"><?=utf8_encode($row["mat_item3_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item3_obser"><?=utf8_encode($row["mat_item3_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item4_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #4</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item4_sel"><?=utf8_encode($row["mat_item4_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item4_des"><?=utf8_encode($row["mat_item4_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item4_uni"><?=utf8_encode($row["mat_item4_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item4_cant"><?=utf8_encode($row["mat_item4_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item4_obser"><?=utf8_encode($row["mat_item4_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item5_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #5</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item5_sel"><?=utf8_encode($row["mat_item5_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item5_des"><?=utf8_encode($row["mat_item5_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item5_uni"><?=utf8_encode($row["mat_item5_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item5_cant"><?=utf8_encode($row["mat_item5_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item5_obser"><?=utf8_encode($row["mat_item5_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item6_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #6</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item6_sel"><?=utf8_encode($row["mat_item6_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item6_des"><?=utf8_encode($row["mat_item6_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item6_uni"><?=utf8_encode($row["mat_item6_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item6_cant"><?=utf8_encode($row["mat_item6_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item6_obser"><?=utf8_encode($row["mat_item6_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item7_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #7</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item7_sel"><?=utf8_encode($row["mat_item7_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item7_des"><?=utf8_encode($row["mat_item7_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item7_uni"><?=utf8_encode($row["mat_item7_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item7_cant"><?=utf8_encode($row["mat_item7_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item7_obser"><?=utf8_encode($row["mat_item7_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item8_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #8</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item8_sel"><?=utf8_encode($row["mat_item8_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item8_des"><?=utf8_encode($row["mat_item8_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item8_uni"><?=utf8_encode($row["mat_item8_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item8_cant"><?=utf8_encode($row["mat_item8_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item8_obser"><?=utf8_encode($row["mat_item8_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item9_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #9</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item9_sel"><?=utf8_encode($row["mat_item9_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item9_des"><?=utf8_encode($row["mat_item9_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item9_uni"><?=utf8_encode($row["mat_item9_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item9_cant"><?=utf8_encode($row["mat_item9_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item9_obser"><?=utf8_encode($row["mat_item9_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item10_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #10</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item10_sel"><?=utf8_encode($row["mat_item10_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item10_des"><?=utf8_encode($row["mat_item10_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item10_uni"><?=utf8_encode($row["mat_item10_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item10_cant"><?=utf8_encode($row["mat_item10_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item10_obser"><?=utf8_encode($row["mat_item10_obser"])?></td>
      </tr>
    </table></td>
  </tr>
   <?php }?>
   <tr>
     <td>&nbsp;</td>
   </tr>
   <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">PRUEBAS FINALES DEL EQUIPO</td>
      </tr>
      <tr>
        <td width="29%" class="lblone" height="21">Tipo de Prueba (Horas)</td>
        <td width="71%" class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="pf_prueba_hora"><?=utf8_encode($row["pf_prueba_hora"])?></td>
      </tr>
      <tr>
        <td width="29%" class="lblone" height="21">Observaciones</td>
        <td width="71%" class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="pf_observa"><?=utf8_encode($row["pf_observa"])?></td>
      </tr>
    </table></td>
  </tr>
</table>

  <?php }?>
  <?php if($row["tipo_reporte"] == "VENTILACION"){?>
<table width="840" border="0" align="center" cellpadding="0" cellspacing="0" class="tblwithe">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="separa">&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">INFORMACION DEL EQUIPO #<?php echo $conEqui;?></td>
      </tr>
      <tr>
        <td width="29%" class="lblone" height="21">Tipo de Equipo</td>
        <td width="71%" class="lblthree" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="tipo_equipo"><?=utf8_encode($row["tipo_equipo"])?></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">CARACTERISTICAS EQUIPO (VENTILACI&Oacute;N)</td>
      </tr>
      <?php
	  if($row["codigo_equipo"] != ''){ ?>
      <tr>
        <td width="29%" class="lblone" height="21">C&oacute;digo del Equipo</td>
        <td width="71%" class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="codigo_equipo"><?=utf8_encode($row["codigo_equipo"])?></td>
      </tr>
      <?php }?>
      <?php
	  if($row["desc_equipo"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n del Equipo</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="desc_equipo"><?=utf8_encode($row["desc_equipo"])?></td>
      </tr>
      <?php }?>
      <?php
	  if($row["marca_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Marca</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="marca_eva"><?=utf8_encode($row["marca_eva"])?></td>
      </tr>
      <?php }?>
      <?php
	  if($row["capacidad_hp_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Capacidad (HP)</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="capacidad_hp_eva"><?=utf8_encode($row["capacidad_hp_eva"])?></td>
      </tr>
      <?php }?>
      <?php
	  if($row["modelo_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Modelo</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="modelo_eva"><?=utf8_encode($row["modelo_eva"])?></td>
      </tr>
      <?php }?>
      <?php
	  if($row["serie_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Serie</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="serie_eva"><?=utf8_encode($row["serie_eva"])?></td>
      </tr>
      <?php }?>
      <?php
	  if($row["observa2"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="observa2"><?=utf8_encode($row["observa2"])?></td>
      </tr>
      <?php }?>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">CORRECTIVO EQUIPO (VENTILACI&Oacute;N)</td>
      </tr>
      <?php
	  if($row["correc_newrep_eva"] != ''){ ?>
      <tr>
        <td width="29%" height="21" valign="top" class="lblone">Instalaci&oacute;n Repuestos (Nuevo)</td>
        <td width="71%" class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="correc_newrep_eva"><?=utf8_encode($row["correc_newrep_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["correc_reparep_eva"] != ''){ ?>
      <tr>
        <td height="21" valign="top" class="lblone">Instalaci&oacute;n Repuestos (Reparados)</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="correc_reparep_eva"><?=utf8_encode($row["correc_reparep_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["correc_audio"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Comentarios</td> 
        <td class="lblthree"><a href="reportes_vistos/correctivo/<?php echo $row["correc_audio"];?>">Audio de Comentarios</a></td>
      </tr>
      <?php }?>
      
       <?php
	  if($row["correc_descrip"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n del Trabajo Realizado</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="correc_descrip"><?=utf8_encode($row["correc_descrip"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["correc_fotos"] != ''){ ?>
      <tr>
        <td class="lbltwo" height="21">Captura de Fotos</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" class="lblone">
        <?php
        	if($row["correc_fotos"] != ''){
				$fotos = explode("|", substr($row["correc_fotos"],0,-1));
				for($i=0;$i<count($fotos);$i++){
					?><a href="reportes_vistos/correctivo/<?= $fotos[$i] ?>"><img src="reportes_vistos/correctivo/<?= $fotos[$i] ?>" width="143" height="147"/></a>&nbsp;<?
				}
			}
		?>
        </td>
        </tr>
     <?php }?>
     
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <?php
	  if($row["mat_item1_sel"] != ''){ ?>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">MATERIALES REQUERIDOS</td>
      </tr>
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #1</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item1_sel"><?=utf8_encode($row["mat_item1_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item1_des"><?=utf8_encode($row["mat_item1_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item1_uni"><?=utf8_encode($row["mat_item1_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item1_cant"><?=utf8_encode($row["mat_item1_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item1_obser"><?=utf8_encode($row["mat_item1_obser"])?></td>

      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item2_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #2</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item2_sel"><?=utf8_encode($row["mat_item2_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item2_des"><?=utf8_encode($row["mat_item2_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item2_uni"><?=utf8_encode($row["mat_item2_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item2_cant"><?=utf8_encode($row["mat_item2_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item2_obser"><?=utf8_encode($row["mat_item2_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item3_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #3</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item3_sel"><?=utf8_encode($row["mat_item3_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item3_des"><?=utf8_encode($row["mat_item3_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item3_uni"><?=utf8_encode($row["mat_item3_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item3_cant"><?=utf8_encode($row["mat_item3_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item3_obser"><?=utf8_encode($row["mat_item3_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item4_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #4</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item4_sel"><?=utf8_encode($row["mat_item4_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item4_des"><?=utf8_encode($row["mat_item4_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item4_uni"><?=utf8_encode($row["mat_item4_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item4_cant"><?=utf8_encode($row["mat_item4_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item4_obser"><?=utf8_encode($row["mat_item4_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item5_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #5</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item5_sel"><?=utf8_encode($row["mat_item5_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item5_des"><?=utf8_encode($row["mat_item5_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item5_uni"><?=utf8_encode($row["mat_item5_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item5_cant"><?=utf8_encode($row["mat_item5_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item5_obser"><?=utf8_encode($row["mat_item5_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item6_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #6</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item6_sel"><?=utf8_encode($row["mat_item6_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item6_des"><?=utf8_encode($row["mat_item6_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item6_uni"><?=utf8_encode($row["mat_item6_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item6_cant"><?=utf8_encode($row["mat_item6_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item6_obser"><?=utf8_encode($row["mat_item6_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item7_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #7</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item7_sel"><?=utf8_encode($row["mat_item7_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item7_des"><?=utf8_encode($row["mat_item7_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item7_uni"><?=utf8_encode($row["mat_item7_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item7_cant"><?=utf8_encode($row["mat_item7_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item7_obser"><?=utf8_encode($row["mat_item7_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item8_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #8</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item8_sel"><?=utf8_encode($row["mat_item8_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item8_des"><?=utf8_encode($row["mat_item8_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item8_uni"><?=utf8_encode($row["mat_item8_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item8_cant"><?=utf8_encode($row["mat_item8_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item8_obser><?=utf8_encode($row["mat_item8_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item9_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #9</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item9_sel"><?=utf8_encode($row["mat_item9_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item9_des"><?=utf8_encode($row["mat_item9_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item9_uni"><?=utf8_encode($row["mat_item9_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item9_cant"><?=utf8_encode($row["mat_item9_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item9_obser"><?=utf8_encode($row["mat_item9_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item10_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #10</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item10_sel"><?=utf8_encode($row["mat_item10_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item10_des"><?=utf8_encode($row["mat_item10_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item10_uni"><?=utf8_encode($row["mat_item10_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item10_cant"><?=utf8_encode($row["mat_item10_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="mat_item10_obser"><?=utf8_encode($row["mat_item10_obser"])?></td>
      </tr>
    </table></td>
  </tr>
   <?php }?>
   <tr>
     <td>&nbsp;</td>
   </tr>
   <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">PRUEBAS FINALES DEL EQUIPO</td>
      </tr>
      <tr>
        <td width="29%" class="lblone" height="21">Tipo de Prueba (Horas)</td>
        <td width="71%" class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="pf_prueba_hora"><?=utf8_encode($row["pf_prueba_hora"])?></td>
      </tr>
      <tr>
        <td width="29%" class="lblone" height="21">Observaciones</td>
        <td width="71%" class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_correctivo" data-column="pf_observa"><?=utf8_encode($row["pf_observa"])?></td>
      </tr>
    </table></td>
  </tr>
</table>

  <?php }?>

<?php }?>
  

<!-- END OF CORRECTIVO-->



<!-- --------------------------------------------------------------------------------------------------------------------------------- -->



<!-- INSTALACION -->

<?php 
$sql = "SELECT * FROM `reportes_r_instalacion` WHERE id_orden IN($ids_orden) ORDER BY order_time";

$res = $link->query($sql);

while($row = $res->fetch_assoc()){
	$conEqui++;
?>


<?php if($row["tipo_reporte"] == "INSTALACION B" || utf8_encode($row["tipo_reporte"]) == "CLIMATIZACIÓN B"){?>
<table width="840" border="0" align="center" cellpadding="0" cellspacing="0" class="tblwithe">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="separa">&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">INFORMACION DEL EQUIPO #<?php echo $conEqui;?></td>
      </tr>
      <tr>
        <td width="29%" class="lblone" height="21">Tipo de Equipo</td>
        <td width="71%" class="lblthree"><?=utf8_encode($row["tipo_equipo"])?></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">CARACTERISTICAS EQUIPO (CLIMATIZACION B)</td>
      </tr>
      <?php
	  if($row["codigo_equipo"] != ''){ ?>
      <tr>
        <td width="29%" class="lblone" height="21">C&oacute;digo del Equipo</td>
        <td width="71%" class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="tipo_equipo"><?=utf8_encode($row["tipo_equipo"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["desc_equipo"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n del Equipo</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="desc_equipo">><?=utf8_encode($row["desc_equipo"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if(trim($row["area_clima"]) != ''){ ?>
      <tr>
        <td class="lblone" height="21">Area Que Climatiza</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="area_clima"><?=utf8_encode($row["area_clima"])?></td>
      </tr>
      <?php }?>
      
       <?php
	  if($row["nombre_area"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Nombre del Area</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="nombre_area"><?=utf8_encode($row["nombre_area"])?></td>
      </tr>
      <?php }?>
      <tr>
        <td class="lbltwo" height="21">Evaporador</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <?php
	  if($row["marca_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Marca</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="marca_eva"><?=utf8_encode($row["marca_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["btu_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Capacidad (BTU)</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="btu_eva"><?=utf8_encode($row["btu_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["modelo_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Modelo</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="modelo_eva"><?=utf8_encode($row["modelo_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["serie_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Serie</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="serie_eva"><?=utf8_encode($row["serie_eva"])?></td>
      </tr>
      <?php }?>
      <tr>
        <td class="lbltwo" height="21">Condensador</td>
        <td>&nbsp;</td>
      </tr>
      
      <?php
	  if($row["marca_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Marca</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="marca_conde"><?=utf8_encode($row["marca_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["btu_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Capacidad (BTU)</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="btu_conde"><?=utf8_encode($row["btu_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["modelo_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Modelo</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="modelo_conde"><?=utf8_encode($row["modelo_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["serie_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Serie</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="serie_conde"><?=utf8_encode($row["serie_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["observa2"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="observa2"><?=utf8_encode($row["observa2"])?></td>
      </tr>
      <?php }?>
      
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">INSTALACION EQUIPO (CLIMATIZACION B)</td>
      </tr>
      <tr>
        <td width="29%" height="21" class="lblone">Puntos El&eacute;ctricos</td>
        <td width="71%" class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_pun_elec"><?=utf8_encode($row["inst_pun_elec"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Puntos El&eacute;ctricos Comentarios</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_pun_elec_com"><?=utf8_encode($row["inst_pun_elec_com"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Puntos de Drenaje</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_drena"><?=utf8_encode($row["inst_drena"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Puntos de Drenaje Comentarios</td> 
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_drena_com"><?=utf8_encode($row["inst_drena_com"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Puntos Tuberia de Cobre</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_tube"><?=utf8_encode($row["inst_tube"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Puntos Tuberia de Cobre Comentarios</td> 
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_tube_com"><?=utf8_encode($row["inst_tube_com"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Instalaci&oacute;n de Evaporador</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_eva"><?=utf8_encode($row["inst_eva"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Instalaci&oacute;n de Evaporador Comentarios</td> 
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_eva_com"><?=utf8_encode($row["inst_eva_com"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Instalaci&oacute;n Condensador</td>
        <td class="lblthree"><?=utf8_encode($row["inst_conde"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Instalaci&oacute;n Condensador Comentarios</td> 
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_conde_com"><?=utf8_encode($row["inst_conde_com"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Conexiones Terminadas</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_conec"><?=utf8_encode($row["inst_conec"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Conexiones Terminadas Comentarios</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_conec_com"><?=utf8_encode($row["inst_conec_com"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Prueba de Drenaje del Equipo</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_prue_dre"><?=utf8_encode($row["inst_prue_dre"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Prueba de Drenaje del Equipo Comen.</td> 
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_prue_dre_com"><?=utf8_encode($row["inst_prue_dre_com"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_observa"><?=utf8_encode($row["inst_observa"])?></td>
      </tr>
      <?php
	  if($row["inst_fotos"] != ''){ ?>
      <tr>
        <td class="lbltwo" height="21">Captura de Fotos</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" class="lblone">
        <?php
        	if($row["inst_fotos"] != ''){
				$fotos = explode("|", substr($row["inst_fotos"],0,-1));
				for($i=0;$i<count($fotos);$i++){
					?><a href="reportes_vistos/instalacion/<?= $fotos[$i] ?>"><img src="reportes_vistos/instalacion/<?= $fotos[$i] ?>" width="143" height="147"/></a>&nbsp;<?
				}
			}
		?>
        </td>
        </tr>
     <?php }?>
     
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">PRUEBAS FINALES EQUIPO (CLIMATIZACION B)</td>
      </tr>
      <tr>
        <td width="29%" height="21" class="lblone">Tiempo de Vac&iacute;o</td>
        <td width="71%" class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="pf_pregunta_1"><?=utf8_encode($row["pf_pregunta_1"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Prueba de Arranque y Mantenimiento</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="pf_pregunta_2"><?=utf8_encode($row["pf_pregunta_2"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Temperatura Final (°C)</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="pf_pregunta_3"><?=utf8_encode($row["pf_pregunta_3"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="pf_observa"><?=utf8_encode($row["pf_observa"])?></td>
      </tr>
      <tr>
        <td class="lbltwo" height="21">Imagenes de la Instalaci&oacute;n</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td height="21" colspan="2" class="lblone">
        <?php
        	if($row["pf_fotos"] != ''){
				$fotos = explode("|", substr($row["pf_fotos"],0,-1));
				for($i=0;$i<count($fotos);$i++){
					?><a href="reportes_vistos/instalacion/<?= utf8_encode($fotos[$i]) ?>"><img src="reportes_vistos/instalacion/<?= utf8_encode($fotos[$i]) ?>" width="143" height="147"/></a>&nbsp;<?
				}
			}
		?>
        </td>
        </tr>
    </table></td>
  </tr>
  <?php
	  if($row["mat_item1_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">MATERIALES REQUERIDOS</td>
      </tr>
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #1</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item1_sel"><?=utf8_encode($row["mat_item1_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item1_des"><?=utf8_encode($row["mat_item1_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item1_uni"><?=utf8_encode($row["mat_item1_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item1_cant"><?=utf8_encode($row["mat_item1_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item1_obser"><?=utf8_encode($row["mat_item1_obser"])?></td>

      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item2_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #2</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item2_sel"><?=utf8_encode($row["mat_item2_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item2_des"><?=utf8_encode($row["mat_item2_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item2_uni"><?=utf8_encode($row["mat_item2_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item2_cant"><?=utf8_encode($row["mat_item2_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item2_obser"><?=utf8_encode($row["mat_item2_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item3_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #3</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item3_sel"><?=utf8_encode($row["mat_item3_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item3_des">><?=utf8_encode($row["mat_item3_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item3_uni"><?=utf8_encode($row["mat_item3_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item3_cant">><?=utf8_encode($row["mat_item3_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item3_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item4_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #4</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item4_sel"><?=utf8_encode($row["mat_item4_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item4_des"><?=utf8_encode($row["mat_item4_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item4_uni"><?=utf8_encode($row["mat_item4_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item4_cant"><?=utf8_encode($row["mat_item4_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item4_obser"><?=utf8_encode($row["mat_item4_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item5_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #5</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item5_sel"><?=utf8_encode($row["mat_item5_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item5_des"><?=utf8_encode($row["mat_item5_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item5_uni"><?=utf8_encode($row["mat_item5_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item5_cant"><?=utf8_encode($row["mat_item5_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item5_obser"><?=utf8_encode($row["mat_item5_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item6_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #6</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item6_sel"><?=utf8_encode($row["mat_item6_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item6_des"><?=utf8_encode($row["mat_item6_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item6_uni"><?=utf8_encode($row["mat_item6_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item6_cant"><?=utf8_encode($row["mat_item6_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item6_obser"><?=utf8_encode($row["mat_item6_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item7_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #7</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item7_sel"><?=utf8_encode($row["mat_item7_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item7_des"><?=utf8_encode($row["mat_item7_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item7_uni"><?=utf8_encode($row["mat_item7_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item7_cant"><?=utf8_encode($row["mat_item7_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item7_obser"><?=utf8_encode($row["mat_item7_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item8_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #8</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item8_sel"><?=utf8_encode($row["mat_item8_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item8_des"><?=utf8_encode($row["mat_item8_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item8_uni"><?=utf8_encode($row["mat_item8_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item8_cant"><?=utf8_encode($row["mat_item8_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item8_obser"><?=utf8_encode($row["mat_item8_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item9_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #9</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item9_sel"><?=utf8_encode($row["mat_item9_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item9_des"><?=utf8_encode($row["mat_item9_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item9_uni"><?=utf8_encode($row["mat_item9_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item9_cant"><?=utf8_encode($row["mat_item9_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item9_obser"><?=utf8_encode($row["mat_item9_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item10_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #10</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item10_sel"><?=utf8_encode($row["mat_item10_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item10_des"><?=utf8_encode($row["mat_item10_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item10_uni"><?=utf8_encode($row["mat_item10_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item10_uni"><?=utf8_encode($row["mat_item10_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item10_obser"><?=utf8_encode($row["mat_item10_obser"])?></td>
      </tr>
    </table></td>
  </tr>
   <?php }?>
</table>

<?php }?>

<?php if($row["tipo_reporte"] == "INSTALACION A" || $row["tipo_reporte"] == "CLIMATIZACIÓN B"){?>
<table width="840" border="0" align="center" cellpadding="0" cellspacing="0" class="tblwithe">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="separa">&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">INFORMACION DEL EQUIPO #<?php echo $conEqui;?></td>
      </tr>
      <tr>
        <td width="29%" class="lblone" height="21">Tipo de Equipo</td>
        <td width="71%" class="lblthree"><?=utf8_encode($row["tipo_equipo"])?></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">CARACTERISTICAS EQUIPO (CLIMATIZACION A)</td>
      </tr>
      <?php
	  if($row["codigo_equipo"] != ''){ ?>
      <tr>
        <td width="29%" class="lblone" height="21">C&oacute;digo del Equipo</td>
        <td width="71%" class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="codigo_equipo"><?=utf8_encode($row["codigo_equipo"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["desc_equipo"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n del Equipo</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="desc_equipo"><?=utf8_encode($row["desc_equipo"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if(trim($row["area_clima"]) != ''){ ?>
      <tr>
        <td class="lblone" height="21">Area Que Climatiza</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="area_clima"><?=utf8_encode($row["area_clima"])?></td>
      </tr>
      <?php }?>
      
       <?php
	  if($row["nombre_area"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Nombre del Area</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="nombre_area"><?=utf8_encode($row["nombre_area"])?></td>
      </tr>
      <?php }?>
      <tr>
        <td class="lbltwo" height="21">Evaporador / Condensador</td>
        <td class="lblthree" width="71%">&nbsp;</td>
      </tr>
      
      
      <?php
	  if($row["btu_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Capacidad (BTU)</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="btu_eva"><?=utf8_encode($row["btu_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["observa2"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="observa2"><?=utf8_encode($row["observa2"])?></td>
      </tr>
      <?php }?>
      
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">INSTALACION EQUIPO (CLIMATIZACION A)</td>
      </tr>
      <tr>
        <td width="29%" height="21" class="lblone">Puntos El&eacute;ctricos</td>
        <td width="71%" class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_pun_elec"><?=utf8_encode($row["inst_pun_elec"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Puntos El&eacute;ctricos Comentarios</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_pun_elec_com"><?=utf8_encode($row["inst_pun_elec_com"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Puntos de Drenaje</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_drena"><?=utf8_encode($row["inst_drena"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Puntos de Drenaje Comentarios</td> 
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_drena_com"><?=utf8_encode($row["inst_drena_com"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Puntos Tuberia de Cobre</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_tube"><?=utf8_encode($row["inst_tube"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Puntos Tuberia de Cobre Comentarios</td> 
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_tube_com"><?=utf8_encode($row["inst_tube_com"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Instalaci&oacute;n de Evaporador</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_eva"><?=utf8_encode($row["inst_eva"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Instalaci&oacute;n de Evaporador Comentarios</td> 
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_eva_com"><?=utf8_encode($row["inst_eva_com"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Instalaci&oacute;n Condensador</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_conde"><?=utf8_encode($row["inst_conde"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Instalaci&oacute;n Condensador Comentarios</td> 
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_conde_com"><?=utf8_encode($row["inst_conde_com"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Conexiones Terminadas</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_conec"><?=utf8_encode($row["inst_conec"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Conexiones Terminadas Comentarios</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_conec_com"><?=utf8_encode($row["inst_conec_com"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Prueba de Drenaje del Equipo</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_prue_dre"><?=utf8_encode($row["inst_prue_dre"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Prueba de Drenaje del Equipo Comen.</td> 
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_prue_dre_com"><?=utf8_encode($row["inst_prue_dre_com"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_observa"><?=utf8_encode($row["inst_observa"])?></td>
      </tr>
      <?php
	  if($row["inst_fotos"] != ''){ ?>
      <tr>
        <td class="lbltwo" height="21">Captura de Fotos</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" class="lblone">
        <?php
        	if($row["inst_fotos"] != ''){
				$fotos = explode("|", substr($row["inst_fotos"],0,-1));
				for($i=0;$i<count($fotos);$i++){
					?><a href="reportes_vistos/instalacion/<?= $fotos[$i] ?>"><img src="reportes_vistos/instalacion/<?= $fotos[$i] ?>" width="143" height="147"/></a>&nbsp;<?
				}
			}
		?>
        </td>
        </tr>
     <?php }?>
     
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">PRUEBAS FINALES EQUIPO (CLIMATIZACION A)</td>
      </tr>
      <tr>
        <td width="29%" height="21" class="lblone">Tiempo de Vac&iacute;o</td>
        <td width="71%" class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="pf_pregunta_1"><?=utf8_encode($row["pf_pregunta_1"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Prueba de Arranque y Mantenimiento</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="pf_pregunta_2"><?=utf8_encode($row["pf_pregunta_2"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Temperatura Final (°C)</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="pf_pregunta_3"><?=utf8_encode($row["pf_pregunta_3"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="pf_observa"><?=utf8_encode($row["pf_observa"])?></td>
      </tr>
      <tr>
        <td class="lbltwo" height="21">Imagenes de la Instalaci&oacute;n</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td height="21" colspan="2" class="lblone">
        <?php
        	if($row["pf_fotos"] != ''){
				$fotos = explode("|", substr($row["pf_fotos"],0,-1));
				for($i=0;$i<count($fotos);$i++){
					?><a href="reportes_vistos/instalacion/<?= utf8_encode($fotos[$i]) ?>"><img src="reportes_vistos/instalacion/<?= utf8_encode($fotos[$i]) ?>" width="143" height="147"/></a>&nbsp;<?
				}
			}
		?>
        </td>
        </tr>
    </table></td>
  </tr>
  <?php
	  if($row["mat_item1_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">MATERIALES REQUERIDOS</td>
      </tr>
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #1</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item1_sel"><?=utf8_encode($row["mat_item1_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item1_des"><?=utf8_encode($row["mat_item1_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item1_uni"><?=utf8_encode($row["mat_item1_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item1_cant"><?=utf8_encode($row["mat_item1_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item1_obser"><?=utf8_encode($row["mat_item1_obser"])?></td>

      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item2_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #2</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item2_sel"><?=utf8_encode($row["mat_item2_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item2_des"><?=utf8_encode($row["mat_item2_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item2_uni"><?=utf8_encode($row["mat_item2_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item2_cant"><?=utf8_encode($row["mat_item2_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item2_obser"><?=utf8_encode($row["mat_item2_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item3_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #3</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item3_sel"><?=utf8_encode($row["mat_item3_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item3_des"><?=utf8_encode($row["mat_item3_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item3_uni"><?=utf8_encode($row["mat_item3_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item3_cant"><?=utf8_encode($row["mat_item3_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item3_obser"><?=utf8_encode($row["mat_item3_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item4_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #4</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item4_sel"><?=utf8_encode($row["mat_item4_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item4_des"><?=utf8_encode($row["mat_item4_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item4_uni"><?=utf8_encode($row["mat_item4_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item4_cant"><?=utf8_encode($row["mat_item4_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item4_obser"><?=utf8_encode($row["mat_item4_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item5_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #5</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item5_sel"><?=utf8_encode($row["mat_item5_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item5_des"><?=utf8_encode($row["mat_item5_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item5_uni"><?=utf8_encode($row["mat_item5_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item5_cant"><?=utf8_encode($row["mat_item5_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item5_obser"><?=utf8_encode($row["mat_item5_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item6_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #6</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item6_sel"><?=utf8_encode($row["mat_item6_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item6_des"><?=utf8_encode($row["mat_item6_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item6_uni"><?=utf8_encode($row["mat_item6_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item6_cant"><?=utf8_encode($row["mat_item6_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item6_obser"><?=utf8_encode($row["mat_item6_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item7_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #7</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item7_sel"><?=utf8_encode($row["mat_item7_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item7_des"><?=utf8_encode($row["mat_item7_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item7_uni"><?=utf8_encode($row["mat_item7_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item7_cant"><?=utf8_encode($row["mat_item7_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item7_obser"><?=utf8_encode($row["mat_item7_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item8_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #8</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item8_sel"><?=utf8_encode($row["mat_item8_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item8_des"><?=utf8_encode($row["mat_item8_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item8_uni"><?=utf8_encode($row["mat_item8_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item8_cant"><?=utf8_encode($row["mat_item8_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item8_obser"><?=utf8_encode($row["mat_item8_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item9_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #9</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item9_sel"><?=utf8_encode($row["mat_item9_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item9_des"><?=utf8_encode($row["mat_item9_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item9_uni"><?=utf8_encode($row["mat_item9_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item9_cant"><?=utf8_encode($row["mat_item9_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item9_obser"><?=utf8_encode($row["mat_item9_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item10_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #10</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item10_sel"><?=utf8_encode($row["mat_item10_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item10_des"><?=utf8_encode($row["mat_item10_des"])?></td>
      </tr>
      <tr>

        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="marca_eva"><?=utf8_encode($row["mat_item10_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item10_cant"><?=utf8_encode($row["mat_item10_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item10_obser"><?=utf8_encode($row["mat_item10_obser"])?></td>
      </tr>
    </table></td>
  </tr>
   <?php }?>
</table>

  <?php }?>

<?php if($row["tipo_reporte"] == "VENTILACION"){?>
<table width="840" border="0" align="center" cellpadding="0" cellspacing="0" class="tblwithe">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="separa">&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">INFORMACION DEL EQUIPO #<?php echo $conEqui;?></td>
      </tr>
      <tr>
        <td width="29%" class="lblone" height="21">Tipo de Equipo</td>
        <td width="71%" class="lblthree"><?=utf8_encode($row["tipo_equipo"])?></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">CARACTERISTICAS EQUIPO (VENTILACI&Oacute;N)</td>
      </tr>
      <?php
	  if($row["codigo_equipo"] != ''){ ?>
      <tr>
        <td width="29%" class="lblone" height="21">C&oacute;digo del Equipo</td>
        <td width="71%" class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="codigo_equipo"><?=utf8_encode($row["codigo_equipo"])?></td>
      </tr>
      <?php }?>
      <?php
	  if($row["desc_equipo"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n del Equipo</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="desc_equipo"><?=utf8_encode($row["desc_equipo"])?></td>
      </tr>
      <?php }?>
      <?php
	  if($row["marca_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Marca</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="marca_eva"><?=utf8_encode($row["marca_eva"])?></td>
      </tr>
      <?php }?>
      <?php
	  if($row["capacidad_hp_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Capacidad (HP)</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="capacidad_hp_eva"><?=utf8_encode($row["capacidad_hp_eva"])?></td>
      </tr>
      <?php }?>
      <?php
	  if($row["modelo_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Modelo</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="modelo_eva"><?=utf8_encode($row["modelo_eva"])?></td>
      </tr>
      <?php }?>
      <?php
	  if($row["serie_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Serie</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="serie_eva"><?=utf8_encode($row["serie_eva"])?></td>
      </tr>
      <?php }?>
      <?php
	  if($row["observa2"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="observa2"><?=utf8_encode($row["observa2"])?></td>
      </tr>
      <?php }?>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">INSTALACION EQUIPO (VENTILACI&Oacute;N)</td>
      </tr>
      <tr>
        <td width="29%" height="21" class="lblone">Puntos El&eacute;ctricos</td>
        <td width="71%" class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_pun_elec"><?=utf8_encode($row["inst_pun_elec"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Puntos El&eacute;ctricos Comentarios</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_pun_elec_com"><?=utf8_encode($row["inst_pun_elec_com"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Adecuaciones para Instalació&oacute;n:</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_adecuaciones"><?=utf8_encode($row["inst_adecuaciones"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Adecuaciones para Instalaci&oacute;n <br />Comentarios</td> 
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_adecuaciones_com"><?=utf8_encode($row["inst_adecuaciones_com"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Instalaci&oacute;n Equipos</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_equipos"><?=utf8_encode($row["inst_equipos"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Instalaci&oacute;n Equipos Comentarios</td> 
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_equipos_com"><?=utf8_encode($row["inst_equipos_com"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Instalaci&oacute;n Ductos</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_ductos"><?=utf8_encode($row["inst_ductos"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Instalaci&oacute;n Ductos Comentarios</td> 
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_ductos_com"><?=utf8_encode($row["inst_ductos_com"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Conexiones Terminadas</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_conec"><?=utf8_encode($row["inst_conec"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Conexiones Terminadas Comentarios</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_conec_com"><?=utf8_encode($row["inst_conec_com"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_observa"><?=utf8_encode($row["inst_observa"])?></td>
      </tr>
      <?php
	  if($row["inst_fotos"] != ''){ ?>
      <tr>
        <td class="lbltwo" height="21">Captura de Fotos</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" class="lblone">
        <?php
        	if($row["inst_fotos"] != ''){
				$fotos = explode("|", substr($row["inst_fotos"],0,-1));
				for($i=0;$i<count($fotos);$i++){
					?><a href="reportes_vistos/instalacion/<?= $fotos[$i] ?>"><img src="reportes_vistos/instalacion/<?= $fotos[$i] ?>" width="143" height="147"/></a>&nbsp;<?
				}
			}
		?>
        </td>
        </tr>
     <?php }?>
     
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">PRUEBAS FINALES EQUIPO (CLIMATIZACION A)</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Prueba de Arranque y Mantenimiento</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="pf_pregunta_2"><?=utf8_encode($row["pf_pregunta_2"])?></td>
      </tr>
      <tr>
        <td class="lbltwo" height="21">Imagenes de la Instalaci&oacute;n</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td height="21" colspan="2" class="lblone">
        <?php
        	if($row["pf_fotos"] != ''){
				$fotos = explode("|", substr($row["pf_fotos"],0,-1));
				for($i=0;$i<count($fotos);$i++){
					?><a href="reportes_vistos/instalacion/<?= utf8_encode($fotos[$i]) ?>"><img src="reportes_vistos/instalacion/<?= utf8_encode($fotos[$i]) ?>" width="143" height="147"/></a>&nbsp;<?
				}
			}
		?>
        </td>
        </tr>
    </table></td>
  </tr>
  <?php
	  if($row["mat_item1_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">MATERIALES REQUERIDOS</td>
      </tr>
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #1</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item1_sel"><?=utf8_encode($row["mat_item1_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="marca_eva"><?=utf8_encode($row["mat_item1_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item1_uni"><?=utf8_encode($row["mat_item1_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item1_cant"><?=utf8_encode($row["mat_item1_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item1_obser"><?=utf8_encode($row["mat_item1_obser"])?></td>

      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item2_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #2</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item2_sel"><?=utf8_encode($row["mat_item2_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item2_des"><?=utf8_encode($row["mat_item2_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item2_uni"><?=utf8_encode($row["mat_item2_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item2_cant"><?=utf8_encode($row["mat_item2_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item2_obser"><?=utf8_encode($row["mat_item2_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item3_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #3</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item3_sel"><?=utf8_encode($row["mat_item3_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item3_des"><?=utf8_encode($row["mat_item3_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item3_uni"><?=utf8_encode($row["mat_item3_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item3_cant"><?=utf8_encode($row["mat_item3_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item3_obser"><?=utf8_encode($row["mat_item3_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item4_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #4</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item4_sel"><?=utf8_encode($row["mat_item4_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item4_des"><?=utf8_encode($row["mat_item4_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item4_uni"><?=utf8_encode($row["mat_item4_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item4_cant"><?=utf8_encode($row["mat_item4_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item4_obser"><?=utf8_encode($row["mat_item4_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item5_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #5</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item5_sel"><?=utf8_encode($row["mat_item5_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item5_des"><?=utf8_encode($row["mat_item5_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item5_uni"><?=utf8_encode($row["mat_item5_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item5_cant"><?=utf8_encode($row["mat_item5_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item5_obser"><?=utf8_encode($row["mat_item5_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item6_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #6</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item6_sel"><?=utf8_encode($row["mat_item6_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item6_des"><?=utf8_encode($row["mat_item6_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item6_uni"><?=utf8_encode($row["mat_item6_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item6_cant"><?=utf8_encode($row["mat_item6_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item6_obser"><?=utf8_encode($row["mat_item6_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item7_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #7</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item7_sel"><?=utf8_encode($row["mat_item7_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item7_des"><?=utf8_encode($row["mat_item7_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item7_uni"><?=utf8_encode($row["mat_item7_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item7_cant"><?=utf8_encode($row["mat_item7_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item7_obser"><?=utf8_encode($row["mat_item7_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item8_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #8</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item8_sel"><?=utf8_encode($row["mat_item8_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item8_des"><?=utf8_encode($row["mat_item8_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item8_uni"><?=utf8_encode($row["mat_item8_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item8_cant"><?=utf8_encode($row["mat_item8_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item8_obser"><?=utf8_encode($row["mat_item8_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item9_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #9</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item9_sel"><?=utf8_encode($row["mat_item9_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item9_des"><?=utf8_encode($row["mat_item9_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item9_uni"><?=utf8_encode($row["mat_item9_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item9_cant"><?=utf8_encode($row["mat_item9_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item9_obser"><?=utf8_encode($row["mat_item9_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item10_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #10</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item10_sel"><?=utf8_encode($row["mat_item10_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item10_des"><?=utf8_encode($row["mat_item10_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item10_uni"><?=utf8_encode($row["mat_item10_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item10_cant"><?=utf8_encode($row["mat_item10_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item10_obser"><?=utf8_encode($row["mat_item10_obser"])?></td>
      </tr>
    </table></td>
  </tr>
   <?php }?>
</table>

  <?php }?>

<?php if($row["tipo_reporte"] == "REFRIGERACION"){?>
<table width="840" border="0" align="center" cellpadding="0" cellspacing="0" class="tblwithe">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="separa">&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">INFORMACION DEL EQUIPO #<?php echo $conEqui;?></td>
      </tr>
      <tr>
        <td width="29%" class="lblone" height="21">Tipo de Equipo</td>
        <td width="71%" class="lblthree"><?=utf8_encode($row["tipo_equipo"])?></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">CARACTERISTICAS EQUIPO (REFRIGERACI&Oacute;N)</td>
      </tr>
      <?php
    if($row["codigo_equipo"] != ''){ ?>
      <tr>
        <td width="29%" class="lblone" height="21">C&oacute;digo del Equipo</td>
        <td width="71%" class="lblthree"><?=utf8_encode($row["codigo_equipo"])?></td>
      </tr>
      <?php }?>
      <?php
    if($row["desc_equipo"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n del Equipo</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="desc_equipo"><?=utf8_encode($row["desc_equipo"])?></td>
      </tr>
      <?php }?>
      <?php
    if($row["marca_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Marca</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="marca_eva"><?=utf8_encode($row["marca_eva"])?></td>
      </tr>
      <?php }?>
    <?php
    if($row["modelo_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Modelo</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="modelo_eva"><?=utf8_encode($row["modelo_eva"])?></td>
      </tr>
    <?php }?>
    <?php
    if($row["serie_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Serie</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="serie_eva"><?=utf8_encode($row["serie_eva"])?></td>
      </tr>
    <?php }?>
    <?php
    if($row["capacidad_hp_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Capacidad Compresor (HP/BTU): </td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="capacidad_hp_eva"><?=utf8_encode($row["capacidad_hp_eva"])?></td>
      </tr>
      <?php }?>
      <?php
    if($row["cant_compres"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Cantidad Compresores: </td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="cant_compres"><?=utf8_encode($row["cant_compres"])?></td>
      </tr>
      <?php }?>
      <?php
       if($row["capacidad_vent_hp"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Capacidad Motor Ventilador (HP): </td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="capacidad_vent_hp"><?=utf8_encode($row["capacidad_vent_hp"])?></td>
      </tr>
      <?php }?>
      <?php
       if($row["cant_motor_vent"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Cantidad Motor Ventilador:  </td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="cant_motor_vent"><?=utf8_encode($row["cant_motor_vent"])?></td>
      </tr>
      <?php }?>
      <?php
    if($row["observa2"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="observa2"><?=utf8_encode($row["observa2"])?></td>
      </tr>
      <?php }?>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">INSTALACION EQUIPO (REFRIGERACI&Oacute;N)</td>
      </tr>
      <tr>
        <td width="29%" height="21" class="lblone">Puntos El&eacute;ctricos</td>
        <td width="71%" class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_pun_elec"><?=utf8_encode($row["inst_pun_elec"])?></td>
      </tr>
      <?php if($row["inst_pun_elec_com"] != ""): ?>
      <tr>
        <td class="lblone" height="21">Puntos El&eacute;ctricos Comentarios</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_pun_elec_com"><?=utf8_encode($row["inst_pun_elec_com"])?></td>
      </tr>
      <?php endif; ?>
      <tr>
        <td class="lblone" height="21">Instalación Evaporador: </td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_equipos"><?=utf8_encode($row["inst_equipos"])?></td>
      </tr>
      <?php if($row["inst_equipos_com"] != ""): ?>
      <tr>
        <td class="lblone" height="21">Instalación Evaporador Comentarios: </td> 
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_equipos_com"><?=utf8_encode($row["inst_equipos_com"])?></td>
      </tr>
  	  <?php endif; ?>
  	  <?php if($row["inst_ductos"] != ""): ?>
      <tr>
        <td class="lblone" height="21">Instalación Condensador:</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_ductos"><?=utf8_encode($row["inst_ductos"])?></td>
      </tr>
      <?php endif; ?>
      <?php if($row["inst_ductos_com"] != ""): ?>
      <tr>
        <td class="lblone" height="21">Instalación Condensador Comentarios: </td> 
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_ductos_com"><?=utf8_encode($row["inst_ductos_com"])?></td>
      </tr>
      <?php endif; ?>
      <tr>
        <td class="lblone" height="21">Conexiones Terminadas</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_conec"><?=utf8_encode($row["inst_conec"])?></td>
      </tr>
      <?php if($row["inst_conec_com"] != ""): ?>
      <tr>
        <td class="lblone" height="21">Conexiones Terminadas Comentarios:</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_conec_com"><?=utf8_encode($row["inst_conec_com"])?></td>
      </tr>
  	  <?php endif;?>
  	  <?php if($row["inst_adecuaciones"] != ""): ?>
      <tr>
        <td class="lblone" height="21">Prueba de Drenaje con el Equipo:</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_adecuaciones"><?=utf8_encode($row["inst_adecuaciones"])?></td>
      </tr>
      <?php endif; ?>
      <?php if($row["inst_adecuaciones_com"] != ""): ?>
      <tr>
        <td class="lblone" height="21">Prueba de Drenaje con el Equipo Comentarios:</td> 
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_adecuaciones_com"><?=utf8_encode($row["inst_adecuaciones_com"])?></td>
      </tr>
      <?php endif; ?>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_observa"><?=utf8_encode($row["inst_observa"])?></td>
      </tr>
      <?php
    if($row["inst_fotos"] != ''){ ?>
      <tr>
        <td class="lbltwo" height="21">Captura de Fotos</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" class="lblone">
        <?php
          if($row["inst_fotos"] != ''){
        $fotos = explode("|", substr($row["inst_fotos"],0,-1));
        for($i=0;$i<count($fotos);$i++){
          ?><a href="reportes_vistos/instalacion/<?= $fotos[$i] ?>"><img src="reportes_vistos/instalacion/<?= $fotos[$i] ?>" width="143" height="147"/></a>&nbsp;<?
        }
      }
    ?>
        </td>
        </tr>
     <?php }?>
     
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">PRUEBAS FINALES EQUIPO (REFRIGERACION)</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Prueba de Arranque y Mantenimiento</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="pf_pregunta_2"><?=utf8_encode($row["pf_pregunta_2"])?></td>
      </tr>
      <tr>
        <td width="29%" class="lblone" height="21">Tiempo de Vacío:</td>
        <td width="71%" class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="inst_tiempo_vacio"><?=utf8_encode($row["inst_tiempo_vacio"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Prueba de Arranque y Funcionamiento: </td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="prueba_arranq_funcionamiento"><?=utf8_encode($row["prueba_arranq_funcionamiento"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Temperatura Final (◦C): </td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="prueba_temperatura"><?=utf8_encode($row["prueba_temperatura"])?></td>
      </tr>
      <tr>
        <td class="lbltwo" height="21">Imagenes de la Instalaci&oacute;n</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td height="21" colspan="2" class="lblone">
        <?php
          	if($row["pf_fotos"] != ''){
        		$fotos = explode("|", substr($row["pf_fotos"],0,-1));
        		for($i=0; $i< count($fotos); $i++){?>
        			<a href="reportes_vistos/instalacion/<?= utf8_encode($fotos[$i]) ?>"><img src="reportes_vistos/instalacion/<?= utf8_encode($fotos[$i]) ?>" width="143" height="147"/></a>&nbsp;
        	  <?}
        	}
    	?>
        </td>
        </tr>
    </table></td>
  </tr>
  <?php
    if($row["mat_item1_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">MATERIALES REQUERIDOS</td>
      </tr>
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #1</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item1_sel"><?=utf8_encode($row["mat_item1_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item1_des"><?=utf8_encode($row["mat_item1_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item1_uni"><?=utf8_encode($row["mat_item1_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item1_cant"><?=utf8_encode($row["mat_item1_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item1_obser"><?=utf8_encode($row["mat_item1_obser"])?></td>

      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
    if($row["mat_item2_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #2</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item2_sel"><?=utf8_encode($row["mat_item2_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item2_des"><?=utf8_encode($row["mat_item2_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item2_uni"><?=utf8_encode($row["mat_item2_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item2_cant"><?=utf8_encode($row["mat_item2_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item2_obser"><?=utf8_encode($row["mat_item2_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
    if($row["mat_item3_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #3</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item3_sel"><?=utf8_encode($row["mat_item3_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item3_des"><?=utf8_encode($row["mat_item3_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item3_uni"><?=utf8_encode($row["mat_item3_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item3_cant"><?=utf8_encode($row["mat_item3_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item3_obser"><?=utf8_encode($row["mat_item3_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
    if($row["mat_item4_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #4</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item4_sel"><?=utf8_encode($row["mat_item4_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item4_des"><?=utf8_encode($row["mat_item4_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item4_uni"><?=utf8_encode($row["mat_item4_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item4_cant"><?=utf8_encode($row["mat_item4_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item4_obser"><?=utf8_encode($row["mat_item4_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
    if($row["mat_item5_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #5</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item5_sel"><?=utf8_encode($row["mat_item5_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item5_des"><?=utf8_encode($row["mat_item5_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item5_uni"><?=utf8_encode($row["mat_item5_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item5_cant"><?=utf8_encode($row["mat_item5_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item5_obser"><?=utf8_encode($row["mat_item5_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
    if($row["mat_item6_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #6</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item6_sel"><?=utf8_encode($row["mat_item6_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item6_des"><?=utf8_encode($row["mat_item6_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item6_uni"><?=utf8_encode($row["mat_item6_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item6_cant"><?=utf8_encode($row["mat_item6_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item6_obser"><?=utf8_encode($row["mat_item6_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
    if($row["mat_item7_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #7</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item7_sel"><?=utf8_encode($row["mat_item7_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item7_des"><?=utf8_encode($row["mat_item7_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item7_uni"><?=utf8_encode($row["mat_item7_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item7_cant"><?=utf8_encode($row["mat_item7_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item7_obser"><?=utf8_encode($row["mat_item7_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
    if($row["mat_item8_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #8</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item8_sel"><?=utf8_encode($row["mat_item8_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item8_des"><?=utf8_encode($row["mat_item8_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item8_uni"><?=utf8_encode($row["mat_item8_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item8_cant"><?=utf8_encode($row["mat_item8_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item8_obser"><?=utf8_encode($row["mat_item8_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
    if($row["mat_item9_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #9</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item9_sel"><?=utf8_encode($row["mat_item9_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item9_des"><?=utf8_encode($row["mat_item9_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item9_uni"><?=utf8_encode($row["mat_item9_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item9_cant"><?=utf8_encode($row["mat_item9_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item9_obser"><?=utf8_encode($row["mat_item9_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
    if($row["mat_item10_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #10</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item10_sel"><?=utf8_encode($row["mat_item10_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item10_des"><?=utf8_encode($row["mat_item10_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item10_uni"><?=utf8_encode($row["mat_item10_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item10_cant"><?=utf8_encode($row["mat_item10_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_instalacion" data-column="mat_item10_obser"><?=utf8_encode($row["mat_item10_obser"])?></td>
      </tr>
    </table></td>
  </tr>
   <?php }?>
</table>
  
<?php }?>


<?php }?>

<!-- END INSTALACION -->




<!-- --------------------------------------------------------------------------------------------------------------------------------- -->



<!-- MANTENIMIENTO -->

<?php 
$sql = "SELECT * FROM `reportes_r_mantenimiento` WHERE id_orden IN($ids_orden) ORDER BY order_time";

$res = $link->query($sql);

while($row = $res->fetch_assoc()){
	$conEqui++;
?>

<?php if($row["tipo_reporte"] == "DIAGNOSTICO B"){?>
<table width="840" border="0" align="center" cellpadding="0" cellspacing="0" class="tblwithe">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="separa">&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">INFORMACION DEL EQUIPO #<?php echo $conEqui;?></td>
      </tr>
      <tr>
        <td width="29%" class="lblone" height="21">Tipo de Equipo</td>
        <td width="71%" class="lblthree"><?=utf8_encode($row["tipo_equipo"])?></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">CARACTERISTICAS EQUIPO (CLIMATIZACION B)</td>
      </tr>
      <?php
	  if($row["codigo_equipo"] != ''){ ?>
      <tr>
        <td width="29%" class="lblone" height="21">C&oacute;digo del Equipo</td>
        <td width="71%" class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="codigo_equipo"><?=utf8_encode($row["codigo_equipo"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["desc_equipo"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n del Equipo</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="desc_equipo"><?=utf8_encode($row["desc_equipo"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if(trim($row["area_clima"]) != ''){ ?>
      <tr>
        <td class="lblone" height="21">Area Que Climatiza</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="area_clima"><?=utf8_encode($row["area_clima"])?></td>
      </tr>
      <?php }?>
      
       <?php
	  if($row["nombre_area"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Nombre del Area</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="nombre_area"><?=utf8_encode($row["nombre_area"])?></td>
      </tr>
      <?php }?>
      <tr>
        <td class="lbltwo" height="21">Evaporador</td>
        <td class="lblthree" width="71%">&nbsp;</td>
      </tr>
      <?php
	  if($row["marca_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Marca</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="marca_eva"><?=utf8_encode($row["marca_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["btu_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Capacidad (BTU)</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="btu_eva"><?=utf8_encode($row["btu_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["modelo_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Modelo</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="modelo_eva"><?=utf8_encode($row["modelo_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["serie_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Serie</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="serie_eva"><?=utf8_encode($row["serie_eva"])?></td>
      </tr>
      <?php }?>
      <tr>
        <td class="lbltwo" height="21">Condensador</td>
        <td class="lblthree">&nbsp;</td>
      </tr>
      
      <?php
	  if($row["marca_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Marca</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="marca_conde"><?=utf8_encode($row["marca_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["btu_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Capacidad (BTU)</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="btu_conde"><?=utf8_encode($row["btu_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["modelo_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Modelo</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="modelo_conde"><?=utf8_encode($row["modelo_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["serie_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Serie</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="serie_conde"><?=utf8_encode($row["serie_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["observa2"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="observa2"><?=utf8_encode($row["observa2"])?></td>
      </tr>
      <?php }?>
      
      <tr>
        <td class="lbltwo" height="21">Pruebas Iniciales</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="pruebas_ini"><?=utf8_encode($row["pruebas_ini"])?></td>
      </tr>
      <?php
	  if($row["equipo_operativo"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">¿Equipo Operativo?</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="equipo_operativo"><?=utf8_encode($row["equipo_operativo"])?></td>
      </tr>
      <?php }?>
      
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">DIAGNOSTICO (CLIMATIZACIÓN B)</td>
      </tr>
      <tr>
        <td width="29%" class="lbltwo" height="21">Evaporador</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      
      <?php
	  if($row["diag_parno_opera"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Partes no Operativas (Reparaci&oacute;n)</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="diag_parno_opera"><?=utf8_encode($row["diag_parno_opera"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["diag_parno_opera2"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Partes no Operativas (Cambio)</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="diag_parno_opera2"><?=utf8_encode($row["diag_parno_opera2"])?></td>
      </tr>
      <?php }?>
      
      <tr>
        <td class="lbltwo" height="21">Condensador</td>
        <td class="lblthree">&nbsp;</td>
      </tr>
      
      <?php
	  if($row["diag_parno_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Partes no Operativas (Reparaci&oacute;n)</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="diag_parno_eva"><?=utf8_encode($row["diag_parno_eva"])?></td>
      </tr>
       <?php }?>
       
      <?php
	  if($row["diag_parno_eva2"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Partes no Operativas (Cambio)</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="diag_parno_eva2"><?=utf8_encode($row["diag_parno_eva2"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["diag_tiem_tra"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Tiempo de Trabajo</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="diag_tiem_tra"><?=utf8_encode($row["diag_tiem_tra"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["diag_observa"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="diag_observa"><?=utf8_encode($row["diag_observa"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["diag_audio"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Comentarios</td>
        <td class="lblthree"><a href="reportes_vistos/mantenimiento/<?php echo $row["diag_audio"];?>">Audio de Comentarios</a></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["diag_fotos"] != ''){ ?>
      <tr>
        <td class="lbltwo" height="21">Captura de Fotos</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" class="lblone">
        <?php
        	if($row["diag_fotos"] != ''){
				$fotos = explode("|", substr($row["diag_fotos"],0,-1));
				for($i=0;$i<count($fotos);$i++){
					?><a href="reportes_vistos/mantenimiento/<?= $fotos[$i] ?>"><img src="reportes_vistos/mantenimiento/<?= $fotos[$i] ?>" width="143" height="147"/></a>&nbsp;<?
				}
			}
		?>
        </td>
        </tr>
     <?php }?>
     
    </table></td>
  </tr>
  
  <?php
	  if($row["mat_item1_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">MATERIALES REQUERIDOS</td>
      </tr>
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #1</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item1_sel"><?=utf8_encode($row["mat_item1_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item1_des"><?=utf8_encode($row["mat_item1_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item1_uni"><?=utf8_encode($row["mat_item1_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item1_cant"><?=utf8_encode($row["mat_item1_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item1_obser"><?=utf8_encode($row["mat_item1_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item2_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #2</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item2_sel"><?=utf8_encode($row["mat_item2_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item2_des"><?=utf8_encode($row["mat_item2_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item2_uni"><?=utf8_encode($row["mat_item2_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item2_cant"><?=utf8_encode($row["mat_item2_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item2_obser"><?=utf8_encode($row["mat_item2_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item3_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #3</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item3_sel"><?=utf8_encode($row["mat_item3_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item3_des"><?=utf8_encode($row["mat_item3_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item3_uni"><?=utf8_encode($row["mat_item3_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item3_cant"><?=utf8_encode($row["mat_item3_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item3_obser"><?=utf8_encode($row["mat_item3_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item4_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #4</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item4_sel"><?=utf8_encode($row["mat_item4_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item4_des"><?=utf8_encode($row["mat_item4_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item4_uni"><?=utf8_encode($row["mat_item4_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item4_cant"><?=utf8_encode($row["mat_item4_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item4_obser"><?=utf8_encode($row["mat_item4_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item5_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #5</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item5_sel"><?=utf8_encode($row["mat_item5_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item5_des"><?=utf8_encode($row["mat_item5_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item5_uni"><?=utf8_encode($row["mat_item5_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item5_cant"><?=utf8_encode($row["mat_item5_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item5_obser"><?=utf8_encode($row["mat_item5_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item6_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #6</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item6_sel"><?=utf8_encode($row["mat_item6_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item6_des"><?=utf8_encode($row["mat_item6_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item6_uni"><?=utf8_encode($row["mat_item6_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item6_cant"><?=utf8_encode($row["mat_item6_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item6_obser"><?=utf8_encode($row["mat_item6_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item7_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #7</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item7_sel"><?=utf8_encode($row["mat_item7_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item7_des"><?=utf8_encode($row["mat_item7_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item7_uni"><?=utf8_encode($row["mat_item7_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item7_cant"><?=utf8_encode($row["mat_item7_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item7_obser"><?=utf8_encode($row["mat_item7_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item8_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #8</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item8_sel"><?=utf8_encode($row["mat_item8_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item8_des"><?=utf8_encode($row["mat_item8_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item8_uni"><?=utf8_encode($row["mat_item8_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item8_cant"><?=utf8_encode($row["mat_item8_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item8_obser"><?=utf8_encode($row["mat_item8_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item9_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #9</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item9_sel"><?=utf8_encode($row["mat_item9_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item9_des"><?=utf8_encode($row["mat_item9_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item9_uni"><?=utf8_encode($row["mat_item9_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item9_cant"><?=utf8_encode($row["mat_item9_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item9_obser"><?=utf8_encode($row["mat_item9_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item10_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #10</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item10_sel"><?=utf8_encode($row["mat_item10_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item10_des"><?=utf8_encode($row["mat_item10_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item10_uni"><?=utf8_encode($row["mat_item10_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item10_cant"><?=utf8_encode($row["mat_item10_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item10_obser"><?=utf8_encode($row["mat_item10_obser"])?></td>
      </tr>
    </table></td>
  </tr>
   <?php }?>
</table>

  <?php }?>
  
  <?php if($row["tipo_reporte"] == "DIAGNOSTICO A"){?>
<table width="840" border="0" align="center" cellpadding="0" cellspacing="0" class="tblwithe">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="separa">&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">INFORMACION DEL EQUIPO #<?php echo $conEqui;?></td>
      </tr>
      <tr>
        <td width="29%" class="lblone" height="21">Tipo de Equipo</td>
        <td width="71%" class="lblthree"><?=utf8_encode($row["tipo_equipo"])?></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">CARACTERISTICAS EQUIPO (CLIMATIZACION A)</td>
      </tr>
      <?php
	  if($row["codigo_equipo"] != ''){ ?>
      <tr>
        <td width="29%" class="lblone" height="21">C&oacute;digo del Equipo</td>
        <td width="71%" class="lblthree"><?=utf8_encode($row["codigo_equipo"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["desc_equipo"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n del Equipo</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="desc_equipo"><?=utf8_encode($row["desc_equipo"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if(trim($row["area_clima"]) != ''){ ?>
      <tr>
        <td class="lblone" height="21">Area Que Climatiza</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="area_clima"><?=utf8_encode($row["area_clima"])?></td>
      </tr>
      <?php }?>
      
       <?php
	  if($row["nombre_area"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Nombre del Area</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="nombre_area"><?=utf8_encode($row["nombre_area"])?></td>
      </tr>
      <?php }?>
      <tr>
        <td class="lbltwo" height="21">Evaporador / Condensador</td>
        <td class="lblthree" width="71%">&nbsp;</td>
      </tr>
      
      <?php
	  if($row["btu_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Capacidad (BTU)</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="btu_eva"><?=utf8_encode($row["btu_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["observa2"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="observa2"><?=utf8_encode($row["observa2"])?></td>
      </tr>
      <?php }?>
      
      <tr>
        <td class="lbltwo" height="21">Pruebas Iniciales</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="pruebas_ini"><?=utf8_encode($row["pruebas_ini"])?></td>
      </tr>
      <?php
	  if($row["equipo_operativo"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">¿Equipo Operativo?</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="equipo_operativo"><?=utf8_encode($row["equipo_operativo"])?></td>
      </tr>
      <?php }?>
      
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">DIAGNOSTICO (CLIMATIZACIÓN A)</td>
      </tr>
      <tr>
        <td width="29%" class="lbltwo" height="21">Evaporador / Condensador</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      
      <?php
	  if($row["diag_parno_opera"] != ''){ ?>
      <tr>
        <td height="21" valign="top" class="lblone">Partes no Operativas (Reparaci&oacute;n)</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="diag_parno_opera"><?=utf8_encode($row["diag_parno_opera"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["diag_parno_opera2"] != ''){ ?>
      <tr>
        <td height="21" valign="top" class="lblone">Partes no Operativas (Cambio)</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="diag_parno_opera2"><?=utf8_encode($row["diag_parno_opera2"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["diag_tiem_tra"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Tiempo de Trabajo</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="diag_tiem_tra"><?=utf8_encode($row["diag_tiem_tra"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["diag_observa"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="diag_observa"><?=utf8_encode($row["diag_observa"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["diag_audio"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Comentarios</td>
        <td class="lblthree"><a href="reportes_vistos/mantenimiento/<?php echo $row["diag_audio"];?>">Audio de Comentarios</a></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["diag_fotos"] != ''){ ?>
      <tr>
        <td class="lbltwo" height="21">Captura de Fotos</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" class="lblone">
        <?php
        	if($row["diag_fotos"] != ''){
				$fotos = explode("|", substr($row["diag_fotos"],0,-1));
				for($i=0;$i<count($fotos);$i++){
					?><a href="reportes_vistos/mantenimiento/<?= $fotos[$i] ?>"><img src="reportes_vistos/mantenimiento/<?= $fotos[$i] ?>" width="143" height="147"/></a>&nbsp;<?
				}
			}
		?>
        </td>
        </tr>
     <?php }?>
     
    </table></td>
  </tr>
  
  <?php
	  if($row["mat_item1_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">MATERIALES REQUERIDOS</td>
      </tr>
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #1</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item1_sel"><?=utf8_encode($row["mat_item1_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item1_des"><?=utf8_encode($row["mat_item1_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item1_uni"><?=utf8_encode($row["mat_item1_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item1_cant"><?=utf8_encode($row["mat_item1_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item1_obser"><?=utf8_encode($row["mat_item1_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item2_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo">Item #2</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item2_sel"><?=utf8_encode($row["mat_item2_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item2_des"><?=utf8_encode($row["mat_item2_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item2_uni"><?=utf8_encode($row["mat_item2_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item2_cant"><?=utf8_encode($row["mat_item2_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item2_obser"><?=utf8_encode($row["mat_item2_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item3_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #3</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item3_sel"><?=utf8_encode($row["mat_item3_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item3_des"><?=utf8_encode($row["mat_item3_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item3_uni"><?=utf8_encode($row["mat_item3_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item3_cant"><?=utf8_encode($row["mat_item3_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item3_obser"><?=utf8_encode($row["mat_item3_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item4_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #4</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item4_sel"><?=utf8_encode($row["mat_item4_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item4_des"><?=utf8_encode($row["mat_item4_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item4_uni"><?=utf8_encode($row["mat_item4_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item4_cant"><?=utf8_encode($row["mat_item4_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item4_obser"><?=utf8_encode($row["mat_item4_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item5_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #5</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item5_sel"><?=utf8_encode($row["mat_item5_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item5_des"><?=utf8_encode($row["mat_item5_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item5_uni"><?=utf8_encode($row["mat_item5_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item5_cant"><?=utf8_encode($row["mat_item5_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item5_obser"><?=utf8_encode($row["mat_item5_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item6_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #6</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item6_sel"><?=utf8_encode($row["mat_item6_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item6_des"utf8_encode($row["mat_item6_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item6_uni"><?=utf8_encode($row["mat_item6_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item6_cant"><?=utf8_encode($row["mat_item6_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item6_obser"><?=utf8_encode($row["mat_item6_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item7_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #7</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item7_sel"><?=utf8_encode($row["mat_item7_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item7_des"><?=utf8_encode($row["mat_item7_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item7_uni"><?=utf8_encode($row["mat_item7_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item7_cant"><?=utf8_encode($row["mat_item7_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item7_obser"><?=utf8_encode($row["mat_item7_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item8_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #8</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item8_sel"><?=utf8_encode($row["mat_item8_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item8_des"><?=utf8_encode($row["mat_item8_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item8_uni"><?=utf8_encode($row["mat_item8_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item8_cant"><?=utf8_encode($row["mat_item8_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item8_obser"><?=utf8_encode($row["mat_item8_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item9_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #9</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>

      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item9_sel"><?=utf8_encode($row["mat_item9_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item9_des"><?=utf8_encode($row["mat_item9_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item9_uni"><?=utf8_encode($row["mat_item9_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item9_cant"><?=utf8_encode($row["mat_item9_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item9_obser"><?=utf8_encode($row["mat_item9_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item10_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #10</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item10_sel"><?=utf8_encode($row["mat_item10_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item10_des"><?=utf8_encode($row["mat_item10_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item10_uni"><?=utf8_encode($row["mat_item10_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item10_cant"><?=utf8_encode($row["mat_item10_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item10_obser"><?=utf8_encode($row["mat_item10_obser"])?></td>
      </tr>
    </table></td>
  </tr>
   <?php }?>
   
	<?php /*if($row["herr_detalle"] != '' || $row["herr_otros"] != ''){ ?>
  	<tr>
    	<td>&nbsp;</td>
  	</tr>
  	<tr>
    	<td>
    		<table width="100%" border="0" cellspacing="0" cellpadding="0">
      			<tr>
        			<td colspan="2" class="lbltitle">HERRAMIENTAS DE TRABAJO</td>
      			</tr>
      			<?php
	  			if($row["herr_detalle"] != ''){ ?>
      			<tr>
        			<td width="29%" valign="top" class="lblone" height="21">Herramientas Necesarias</td>
        			<td width="71%" class="lblthree"><?=utf8_encode($row["herr_detalle"])?></td>
      			</tr>
      			<?php }?>
      
      			<?php
	  			if($row["herr_otros"] != ''){ ?>
      			<tr>
        			<td class="lblone" height="21">Si eligi&oacute; la opci&oacute;n OTROS por favor <br />
          			especificar</td>
        			<td class="lblthree"><?=utf8_encode($row["herr_otros"])?></td>
      			</tr>
      			<?php } ?>
    		</table>
    	</td>
  	</tr>
  	<?php } ?>

</table>

  <?php }?>
  
  <?php if($row["tipo_reporte"] == "DIAGNOSTICO VENTILACION"){?>
<table width="840" border="0" align="center" cellpadding="0" cellspacing="0" class="tblwithe">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="separa">&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">INFORMACION DEL EQUIPO #<?php echo $conEqui;?></td>
      </tr>
      <tr>
        <td width="29%" class="lblone" height="21">Tipo de Equipo</td>
        <td width="71%" class="lblthree"><?=utf8_encode($row["tipo_equipo"])?></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">CARACTERISTICAS EQUIPO (VENTILACI&Oacute;N)</td>
      </tr>
      <?php
	  if($row["codigo_equipo"] != ''){ ?>
      <tr>
        <td width="29%" class="lblone" height="21">C&oacute;digo del Equipo</td>
        <td width="71%" class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="codigo_equipo"><?=utf8_encode($row["codigo_equipo"])?></td>
      </tr>
      <?php }?>
      <?php
	  if($row["desc_equipo"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n del Equipo</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="desc_equipo"><?=utf8_encode($row["desc_equipo"])?></td>
      </tr>
      <?php }?>
      <?php
	  if($row["marca_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Marca</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="marca_eva"><?=utf8_encode($row["marca_eva"])?></td>
      </tr>
      <?php }?>
      <?php
	  if($row["capacidad_hp_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Capacidad (HP)</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="capacidad_hp_eva"><?=utf8_encode($row["capacidad_hp_eva"])?></td>
      </tr>
      <?php }?>
      <?php
	  if($row["modelo_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Modelo</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="modelo_eva"><?=utf8_encode($row["modelo_eva"])?></td>
      </tr>
      <?php }?>
      <?php
	  if($row["serie_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Serie</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="serie_eva"><?=utf8_encode($row["serie_eva"])?></td>
      </tr>
      <?php }?>
      <?php
	  if($row["observa2"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="observa2"><?=utf8_encode($row["observa2"])?></td>
      </tr>
      <?php }?>
      <tr>
        <td class="lbltwo" height="21">Pruebas Iniciales</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="pruebas_ini"><?=utf8_encode($row["pruebas_ini"])?></td>
      </tr>
      <?php
	  if($row["equipo_operativo"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">¿Equipo Operativo?</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="equipo_operativo"><?=utf8_encode($row["equipo_operativo"])?></td>
      </tr>
      <?php }?>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">DIAGNOSTICO (VENTILACI&Oacute;N)</td>
      </tr>
      
      <?php
	  if($row["diag_parno_opera"] != ''){ ?>
      <tr>
        <td width="29%" height="21" valign="top" class="lblone">Partes no Operativas (Reparaci&oacute;n)</td>
        <td width="71%" class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="diag_parno_opera"><?=utf8_encode($row["diag_parno_opera"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["diag_parno_opera2"] != ''){ ?>
      <tr>
        <td height="21" valign="top" class="lblone">Partes no Operativas (Cambio)</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="diag_parno_opera2"><?=utf8_encode($row["diag_parno_opera2"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["diag_tiem_tra"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Tiempo de Trabajo</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="diag_tiem_tra"><?=utf8_encode($row["diag_tiem_tra"])?></td>
      </tr>
      <?php }?>
      
      <?php

	  if($row["diag_observa"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="diag_observa"><?=utf8_encode($row["diag_observa"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["diag_audio"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Comentarios</td>
        <td class="lblthree"><a href="reportes_vistos/mantenimiento/<?php echo $row["diag_audio"];?>">Audio de Comentarios</a></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["diag_fotos"] != ''){ ?>
      <tr>
        <td class="lbltwo" height="21">Captura de Fotos</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" class="lblone">
        <?php
        	if($row["diag_fotos"] != ''){
				$fotos = explode("|", substr($row["diag_fotos"],0,-1));
				for($i=0;$i<count($fotos);$i++){
					?><a href="reportes_vistos/mantenimiento/<?= $fotos[$i] ?>"><img src="reportes_vistos/mantenimiento/<?= $fotos[$i] ?>" width="143" height="147" /></a>&nbsp;<?
				}
			}
		?>
        </td>
        </tr>
     <?php }?>
     
    </table></td>
  </tr>
  
  <?php
	  if($row["mat_item1_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">MATERIALES REQUERIDOS</td>
      </tr>
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #1</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item1_sel"><?=utf8_encode($row["mat_item1_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item1_des"><?=utf8_encode($row["mat_item1_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item1_uni"><?=utf8_encode($row["mat_item1_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item1_cant"><?=utf8_encode($row["mat_item1_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item1_obser"><?=utf8_encode($row["mat_item1_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item2_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo">Item #2</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item2_sel"><?=utf8_encode($row["mat_item2_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item2_des"><?=utf8_encode($row["mat_item2_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item2_uni"><?=utf8_encode($row["mat_item2_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item2_cant"><?=utf8_encode($row["mat_item2_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item2_obser"><?=utf8_encode($row["mat_item2_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item3_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #3</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item3_sel"><?=utf8_encode($row["mat_item3_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item3_des"><?=utf8_encode($row["mat_item3_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item3_uni"><?=utf8_encode($row["mat_item3_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item3_cant"><?=utf8_encode($row["mat_item3_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item3_obser"><?=utf8_encode($row["mat_item3_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item4_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #4</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item4_sel"><?=utf8_encode($row["mat_item4_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item4_des"><?=utf8_encode($row["mat_item4_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item4_uni"><?=utf8_encode($row["mat_item4_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item4_cant"><?=utf8_encode($row["mat_item4_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item4_obser"><?=utf8_encode($row["mat_item4_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item5_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #5</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item5_sel"><?=utf8_encode($row["mat_item5_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item5_des"><?=utf8_encode($row["mat_item5_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item5_uni"><?=utf8_encode($row["mat_item5_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item5_cant"><?=utf8_encode($row["mat_item5_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item5_obser"><?=utf8_encode($row["mat_item5_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item6_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #6</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item6_sel"><?=utf8_encode($row["mat_item6_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item6_des"><?=utf8_encode($row["mat_item6_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item6_uni"><?=utf8_encode($row["mat_item6_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item6_cant"><?=utf8_encode($row["mat_item6_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item6_obser"><?=utf8_encode($row["mat_item6_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item7_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #7</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item7_sel"><?=utf8_encode($row["mat_item7_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item7_des"><?=utf8_encode($row["mat_item7_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item7_uni"><?=utf8_encode($row["mat_item7_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item7_cant"><?=utf8_encode($row["mat_item7_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item7_obser"><?=utf8_encode($row["mat_item7_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item8_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #8</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item8_sel"><?=utf8_encode($row["mat_item8_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item8_des"><?=utf8_encode($row["mat_item8_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item8_uni"><?=utf8_encode($row["mat_item8_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item8_cant"><?=utf8_encode($row["mat_item8_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item8_obser"><?=utf8_encode($row["mat_item8_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item9_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #9</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>

      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item9_sel"><?=utf8_encode($row["mat_item9_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item9_des"><?=utf8_encode($row["mat_item9_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item9_uni"><?=utf8_encode($row["mat_item9_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item9_cant"><?=utf8_encode($row["mat_item9_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item3_cant"><?=utf8_encode($row["mat_item3_cant"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item10_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #10</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item10_sel"><?=utf8_encode($row["mat_item10_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item10_des"><?=utf8_encode($row["mat_item10_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item10_uni"><?=utf8_encode($row["mat_item10_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item10_cant"><?=utf8_encode($row["mat_item10_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item10_obser"><?=utf8_encode($row["mat_item10_obser"])?></td>
      </tr>
    </table></td>
  </tr>
   <?php }?>
   
   <?php
	 /* if($row["herr_detalle"] != '' || $row["herr_otros"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">HERRAMIENTAS DE TRABAJO</td>
      </tr>
      <?php
	  if($row["herr_detalle"] != ''){ ?>
      <tr>
        <td width="29%" valign="top" class="lblone" height="21">Herramientas Necesarias</td>
        <td width="71%" class="lblthree"><?=utf8_encode($row["herr_detalle"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["herr_otros"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Si eligi&oacute; la opci&oacute;n OTROS por favor <br />
          especificar</td>
        <td class="lblthree"><?=utf8_encode($row["herr_otros"])?></td>
      </tr>
      <?php }?>
      
    </table></td>
  </tr>
  <?php }*/ ?>

</table>

  <?php }?>
  
  <?php if($row["tipo_reporte"] == "DIAGNOSTICO REFRIGERACION"){?>
<table width="840" border="0" align="center" cellpadding="0" cellspacing="0" class="tblwithe">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="separa">&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">INFORMACION DEL EQUIPO #<?php echo $conEqui;?></td>
      </tr>
      <tr>
        <td width="29%" class="lblone" height="21">Tipo de Equipo</td>
        <td width="71%" class="lblthree"><?=utf8_encode($row["tipo_equipo"])?></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">CARACTERISTICAS EQUIPO (REFRIGERACI&Oacute;N)</td>
      </tr>
      <?php
	  if($row["codigo_equipo"] != ''){ ?>
      <tr>
        <td width="29%" class="lblone" height="21">C&oacute;digo del Equipo</td>
        <td width="71%" class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="codigo_equipo"><?=utf8_encode($row["codigo_equipo"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["desc_equipo"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n del Equipo</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="desc_equipo"><?=utf8_encode($row["desc_equipo"])?></td>
      </tr>
      <?php }?>
      
      <tr>
        <td class="lbltwo" height="21">Evaporador</td>
        <td class="lblthree" width="71%">&nbsp;</td>
      </tr>
      <?php
	  if($row["marca_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Marca</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="marca_eva"><?=utf8_encode($row["marca_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["btu_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Capacidad (HP)</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="btu_eva"><?=utf8_encode($row["btu_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["modelo_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Modelo</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="modelo_eva"><?=utf8_encode($row["modelo_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["serie_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Serie</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="serie_eva"><?=utf8_encode($row["serie_eva"])?></td>
      </tr>
      <?php }?>
      <tr>
        <td class="lbltwo" height="21">Condensador</td>
        <td class="lblthree">&nbsp;</td>
      </tr>
      
      <?php
	  if($row["marca_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Marca</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="marca_conde"><?=utf8_encode($row["marca_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["btu_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Capacidad (HP)</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="btu_conde"><?=utf8_encode($row["btu_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["modelo_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Modelo</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="modelo_conde"><?=utf8_encode($row["modelo_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["serie_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Serie</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="serie_conde"><?=utf8_encode($row["serie_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["observa2"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="observa2"><?=utf8_encode($row["observa2"])?></td>
      </tr>
      <?php }?>
      
      <tr>
        <td class="lbltwo" height="21">Pruebas Iniciales</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="pruebas_ini"><?=utf8_encode($row["pruebas_ini"])?></td>
      </tr>

      <?php
	  if($row["equipo_operativo"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">¿Equipo Operativo?</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="equipo_operativo"><?=utf8_encode($row["equipo_operativo"])?></td>
      </tr>

      <?php }?>
      
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">DIAGNOSTICO (REFRIGERACI&Oacute;N)</td>
      </tr>
      <tr>
        <td width="29%" height="21" class="lbltwo">Evaporador</td>
        <td width="71%">&nbsp;</td>
      </tr>
      
      <?php
	  if($row["diag_parno_opera"] != ''){ ?>
      <tr>
        <td height="21" valign="top" class="lblone">Partes no Operativas (Reparaci&oacute;n)</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="diag_parno_opera"><?=utf8_encode($row["diag_parno_opera"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["diag_parno_opera2"] != ''){ ?>
      <tr>
        <td height="21" valign="top" class="lblone">Partes no Operativas (Cambio)</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="diag_parno_opera2"><?=utf8_encode($row["diag_parno_opera2"])?></td>
      </tr>
      <?php }?>
      
      <tr>
        <td height="21" class="lbltwo">Condensador</td>
        <td>&nbsp;</td>
      </tr>
      
      <?php
	  if($row["diag_parno_eva"] != ''){ ?>
      <tr>
        <td height="21" valign="top" class="lblone">Partes no Operativas (Reparaci&oacute;n)</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="diag_parno_eva"><?=utf8_encode($row["diag_parno_eva"])?></td>
      </tr>
       <?php }?>
       
      <?php
	  if($row["diag_parno_eva2"] != ''){ ?>
      <tr>
        <td height="21" valign="top" class="lblone">Partes no Operativas (Cambio)</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="diag_parno_eva2"><?=utf8_encode($row["diag_parno_eva2"])?></td>
      </tr>
      <?php }?>
      
      <tr>
        <td height="21" class="lbltwo">Estructura C&aacute;mara de Fr&iacute;o</td>
        <td></td>
      </tr>

      <?php
	  if($row["diag_parno_camara_r"] != ''){ ?>
      <tr>
        <td height="21" valign="top" class="lblone">Partes no Operativas (Reparaci&oacute;n)</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="diag_parno_camara_r"><?=utf8_encode($row["diag_parno_camara_r"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["diag_parno_camara_c"] != ''){ ?>
      <tr>
        <td height="21" valign="top" class="lblone">Partes no Operativas (Cambio)</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="diag_parno_camara_c"><?=utf8_encode($row["diag_parno_camara_c"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["diag_tiem_tra"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Tiempo de Trabajo</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="diag_tiem_tra"><?=utf8_encode($row["diag_tiem_tra"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["diag_observa"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="diag_observa"><?=utf8_encode($row["diag_observa"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["diag_audio"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Comentarios</td>
        <td class="lblthree"><a href="reportes_vistos/mantenimiento/<?php echo $row["diag_audio"];?>">Audio de Comentarios</a></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["diag_fotos"] != ''){ ?>
      <tr>
        <td class="lbltwo" height="21">Captura de Fotos</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" class="lblone">
        <?php
        	if($row["diag_fotos"] != ''){
				$fotos = explode("|", substr($row["diag_fotos"],0,-1));
				for($i=0;$i<count($fotos);$i++){
					?><a href="reportes_vistos/mantenimiento/<?= $fotos[$i] ?>"><img src="reportes_vistos/mantenimiento/<?= $fotos[$i] ?>" width="143" height="147"/></a>&nbsp;<?
				}
			}
		?>
        </td>
        </tr>
     <?php }?>
     
    </table></td>
  </tr>
  
  <?php
	  if($row["mat_item1_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">MATERIALES REQUERIDOS</td>
      </tr>
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #1</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item1_sel"><?=utf8_encode($row["mat_item1_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item1_des"><?=utf8_encode($row["mat_item1_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item1_uni"><?=utf8_encode($row["mat_item1_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item1_cant"><?=utf8_encode($row["mat_item1_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item1_obser"><?=utf8_encode($row["mat_item1_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item2_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #2</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item2_sel"><?=utf8_encode($row["mat_item2_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item2_des"><?=utf8_encode($row["mat_item2_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item2_uni"><?=utf8_encode($row["mat_item2_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item2_cant"><?=utf8_encode($row["mat_item2_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item2_obser"><?=utf8_encode($row["mat_item2_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item3_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #3</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item3_sel"><?=utf8_encode($row["mat_item3_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item3_des"><?=utf8_encode($row["mat_item3_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item3_uni"><?=utf8_encode($row["mat_item3_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item3_cant"><?=utf8_encode($row["mat_item3_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item3_obser"><?=utf8_encode($row["mat_item3_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item4_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #4</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item4_sel"><?=utf8_encode($row["mat_item4_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item4_des"><?=utf8_encode($row["mat_item4_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item4_uni"><?=utf8_encode($row["mat_item4_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item4_cant"><?=utf8_encode($row["mat_item4_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item4_obser"><?=utf8_encode($row["mat_item4_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item5_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #5</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item5_sel"><?=utf8_encode($row["mat_item5_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item5_des"><?=utf8_encode($row["mat_item5_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item5_uni"><?=utf8_encode($row["mat_item5_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item5_cant"><?=utf8_encode($row["mat_item5_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item5_obser"><?=utf8_encode($row["mat_item5_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item6_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #6</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item6_sel"><?=utf8_encode($row["mat_item6_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item6_des"><?=utf8_encode($row["mat_item6_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item6_uni"><?=utf8_encode($row["mat_item6_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item6_cant"><?=utf8_encode($row["mat_item6_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item6_obser"><?=utf8_encode($row["mat_item6_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item7_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #7</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item7_sel"><?=utf8_encode($row["mat_item7_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item7_des"><?=utf8_encode($row["mat_item7_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item7_uni"><?=utf8_encode($row["mat_item7_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item7_cant"><?=utf8_encode($row["mat_item7_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item7_obser"><?=utf8_encode($row["mat_item7_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item8_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #8</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item8_sel"><?=utf8_encode($row["mat_item8_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item8_des"><?=utf8_encode($row["mat_item8_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item8_uni"><?=utf8_encode($row["mat_item8_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item8_cant"><?=utf8_encode($row["mat_item8_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item8_obser"><?=utf8_encode($row["mat_item8_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item9_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #9</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item9_sel"><?=utf8_encode($row["mat_item9_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item9_des"><?=utf8_encode($row["mat_item9_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item9_uni"><?=utf8_encode($row["mat_item9_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item9_cant"><?=utf8_encode($row["mat_item9_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item9_obser"><?=utf8_encode($row["mat_item9_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item10_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #10</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item10_sel"><?=utf8_encode($row["mat_item10_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item10_des"><?=utf8_encode($row["mat_item10_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item10_uni"><?=utf8_encode($row["mat_item10_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item10_cant"><?=utf8_encode($row["mat_item10_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="mat_item10_obser"><?=utf8_encode($row["mat_item10_obser"])?></td>
      </tr>
    </table></td>
  </tr>
   <?php }?>
   
   <?php
	/*  if($row["herr_detalle"] != '' || $row["herr_otros"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">HERRAMIENTAS DE TRABAJO</td>
      </tr>
      <?php
	  if($row["herr_detalle"] != ''){ ?>
      <tr>
        <td width="29%" valign="top" class="lblone" height="21">Herramientas Necesarias</td>
        <td width="71%" class="lblthree"><?=utf8_encode($row["herr_detalle"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["herr_otros"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Si eligi&oacute; la opci&oacute;n OTROS por favor <br />
          especificar</td>
        <td class="lblthree"><?=utf8_encode($row["herr_otros"])?></td>
      </tr>
      <?php }?>
      
    </table></td>
  </tr>
  <?php }*/ ?>
</table>

  <?php }?>
  
  
  <?php if($row["tipo_reporte"] == "MANTENIMIENTO B"){?>
<table width="840" border="0" align="center" cellpadding="0" cellspacing="0" class="tblwithe">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="separa">&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">INFORMACION DEL EQUIPO #<?php echo $conEqui;?></td>
      </tr>
      <tr>
        <td width="29%" class="lblone" height="21">Tipo de Equipo</td>
        <td width="71%" class="lblthree"><?=utf8_encode($row["tipo_equipo"])?></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">CARACTERISTICAS EQUIPO (CLIMATIZACION B)</td>
      </tr>
      <?php
	  if($row["codigo_equipo"] != ''){ ?>
      <tr>
        <td width="29%" class="lblone" height="21">C&oacute;digo del Equipo</td>
        <td width="71%" class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="codigo_equipo"><?=utf8_encode($row["codigo_equipo"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["desc_equipo"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n del Equipo</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="desc_equipo"><?=utf8_encode($row["desc_equipo"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if(trim($row["area_clima"]) != ''){ ?>
      <tr>
        <td class="lblone" height="21">Area Que Climatiza</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="area_clima"><?=utf8_encode($row["area_clima"])?></td>
      </tr>
      <?php }?>
      
       <?php
	  if($row["nombre_area"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Nombre del Area</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="nombre_area"><?=utf8_encode($row["nombre_area"])?></td>
      </tr>
      <?php }?>
      <tr>
        <td class="lbltwo" height="21">Evaporador</td>
        <td class="lblthree" width="71%">&nbsp;</td>
      </tr>
      <?php
	  if($row["marca_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Marca</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="marca_eva"><?=utf8_encode($row["marca_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["btu_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Capacidad (BTU)</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="btu_eva"><?=utf8_encode($row["btu_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["modelo_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Modelo</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="modelo_eva"><?=utf8_encode($row["modelo_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["serie_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Serie</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="serie_eva"><?=utf8_encode($row["serie_eva"])?></td>
      </tr>
      <?php }?>
      <tr>
        <td class="lbltwo" height="21">Condensador</td>
        <td class="lblthree">&nbsp;</td>
      </tr>
      
      <?php
	  if($row["marca_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Marca</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="marca_conde"><?=utf8_encode($row["marca_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["btu_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Capacidad (BTU)</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="btu_conde"><?=utf8_encode($row["btu_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["modelo_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Modelo</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="modelo_conde"><?=utf8_encode($row["modelo_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["serie_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Serie</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="serie_conde"><?=utf8_encode($row["serie_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["observa2"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="observa2"><?=utf8_encode($row["observa2"])?></td>
      </tr>
      <?php }?>
      
      <tr>
        <td class="lbltwo" height="21">Pruebas Iniciales</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="pruebas_ini"><?=utf8_encode($row["pruebas_ini"])?></td>
      </tr>
      <?php
	  if($row["equipo_operativo"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">¿Equipo Operativo?</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="equipo_operativo"><?=utf8_encode($row["equipo_operativo"])?></td>
      </tr>
      <?php }?>
      
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">MANTENIMIENTO (CLIMATIZACIÓN B)</td>
      </tr>
      <tr>
        <td width="29%" class="lbltwo" height="21">Evaporador</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      <?php
	  if($row["limpia_serpin_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Serpert&iacute;n</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="limpia_serpin_eva"><?=utf8_encode($row["limpia_serpin_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_filtro_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Filtros</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="limpia_filtro_eva"><?=utf8_encode($row["limpia_filtro_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_filtro_com_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Filtros - Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="limpia_filtro_com_eva"><?=utf8_encode($row["limpia_filtro_com_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_carca_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Carcaza</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="limpia_carca_eva"><?=utf8_encode($row["limpia_carca_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_carca_com_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Carcaza - Comentarios</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="limpia_carca_com_eva"><?=utf8_encode($row["limpia_carca_com_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_motor_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza y Lubricaci&oacute;n Motores</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="limpia_motor_eva"><?=utf8_encode($row["limpia_motor_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_motor_com_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza y Lubricaci&oacute;n Motores - Comen.</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="limpia_motor_com_eva"><?=utf8_encode($row["limpia_motor_com_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_elec_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Parte El&eacute;ctrica</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="limpia_elec_eva"><?=utf8_encode($row["limpia_elec_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_elec_com_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Parte El&eacute;ctrica - Comen.</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="limpia_elec_com_eva"><?=utf8_encode($row["limpia_elec_com_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_drenaje"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Drenaje</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="limpia_drenaje"><?=utf8_encode($row["limpia_drenaje"])?></td>
      </tr>
      <?php }?>
      
      <tr>
        <td class="lbltwo" height="21">Condensador</td>
        <td class="lblthree">&nbsp;</td>
      </tr>
      
	  <?php
	  if($row["limpia_serpin_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Serpert&iacute;n</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="limpia_serpin_conde"><?=utf8_encode($row["limpia_serpin_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_carca_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Carcaza</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="limpia_carca_conde"><?=utf8_encode($row["limpia_carca_conde"])?></td>
      </tr>
      <?php }?>
      
       <?php
	  if($row["limpia_carca_com_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Carcaza - Comentarios</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="limpia_carca_com_conde"><?=utf8_encode($row["limpia_carca_com_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_motor_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza y Lubricaci&oacute;n Motores</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="limpia_motor_conde"><?=utf8_encode($row["limpia_motor_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_motor_com_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza y Lubricaci&oacute;n Motores - Comen.</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="limpia_motor_com_conde"><?=utf8_encode($row["limpia_motor_com_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_elec_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Parte El&eacute;ctrica</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="limpia_elec_conde"><?=utf8_encode($row["limpia_elec_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_elec_com_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Parte El&eacute;ctrica - Comen.</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="limpia_elec_com_conde"><?=utf8_encode($row["limpia_elec_com_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["condensador_mant"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="condensador_mant"><?=utf8_encode($row["condensador_mant"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["mante_fotos"] != ''){ ?>
      <tr>
        <td class="lbltwo" height="21">Captura de Fotos</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" class="lblone">
        <?php
        	if($row["mante_fotos"] != ''){
				$fotos = explode("|", substr($row["mante_fotos"],0,-1));
				for($i=0;$i<count($fotos);$i++){
					?><a href="reportes_vistos/mantenimiento/<?= $fotos[$i] ?>"><img src="reportes_vistos/mantenimiento/<?= $fotos[$i] ?>" width="143" height="147"/></a>&nbsp;<?
				}
			}
		?>
        </td>
        </tr>
        <?php }?>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">PRUEBAS FINALES EQUIPO (CLIMATIZACION)</td>
      </tr>
      <?php
	  if($row["pf_pregunta_1"] != ''){ ?>
      <tr>
        <td width="33%" class="lblone" height="21">¿El Equipo Enciende y Funciona?</td>
        <td width="67%" class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="pf_pregunta_1"><?=utf8_encode($row["pf_pregunta_1"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["pf_pregunta_2"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">¿El Equipo Enciende y Funciona? - Comen.</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="pf_pregunta_2"><?=utf8_encode($row["pf_pregunta_2"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["pf_pregunta_3"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Prueba de Arranque y Funcionamiento</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="pf_pregunta_3"><?=utf8_encode($row["pf_pregunta_3"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["pf_pregunta_4"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">¿Se Realiz&oacute; la Prueba de Drenaje?</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="pf_pregunta_4"><?=utf8_encode($row["pf_pregunta_4"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["pf_pregunta_5"] != ''){ ?>
      <tr>
        <td class="lblone">¿Se Realiz&oacute; la Prueba de Drenaje? - Comen.</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="pf_pregunta_5"><?=utf8_encode($row["pf_pregunta_5"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["pf_observa"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Observaciones Generales del Equipo</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="pf_observa"><?=utf8_encode($row["pf_observa"])?></td>
      </tr>
      <?php }?>
    </table></td>
  </tr>
</table>
  <?php }?>
  
  <?php if($row["tipo_reporte"] == "MANTENIMIENTO A"){?>
<table width="840" border="0" align="center" cellpadding="0" cellspacing="0" class="tblwithe">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="separa">&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">INFORMACION DEL EQUIPO #<?php echo $conEqui;?></td>
      </tr>
      <tr>
        <td width="29%" class="lblone" height="21">Tipo de Equipo</td>
        <td width="71%" class="lblthree"><?=utf8_encode($row["tipo_equipo"])?></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">CARACTERISTICAS EQUIPO (CLIMATIZACION A)</td>
      </tr>
      <?php
	  if($row["codigo_equipo"] != ''){ ?>
      <tr>
        <td width="29%" class="lblone" height="21">C&oacute;digo del Equipo</td>
        <td width="71%" class="lblthree" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="codigo_equipo"><?=utf8_encode($row["codigo_equipo"])?></td>
      </tr>
      <?php }?>
      <?php
	  if($row["desc_equipo"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n del Equipo</td>
        <td class="lblthree" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="desc_equipo"><?=utf8_encode($row["desc_equipo"])?></td>
      </tr>
      <?php }?>
      <?php
	  if(trim($row["area_clima"]) != ''){ ?>
      <tr>
        <td class="lblone" height="21">Area Que Climatiza</td>
        <td class="lblthree" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="area_clima"><?=utf8_encode($row["area_clima"])?></td>
      </tr>
      <?php }?>
      <?php
	  if($row["nombre_area"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Nombre del Area</td>
        <td class="lblthree" data-id = "<?= $row["id"] ?>" data-table="reportes_r_mantenimiento" data-column="nombre_area"><?=utf8_encode($row["nombre_area"])?></td>
      </tr>
      <?php }?>
      <tr>
        <td class="lbltwo" height="21">Evaporador / Condensador</td>
        <td class="lblthree" width="71%">&nbsp;</td>
      </tr>
      <?php
	  if($row["btu_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Capacidad (BTU)</td>
        <td class="lblthree"><?=utf8_encode($row["btu_eva"])?></td>
      </tr>
      <?php }?>
      <?php
	  if($row["observa2"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree"><?=utf8_encode($row["observa2"])?></td>
      </tr>
      <?php }?>
      <tr>
        <td class="lbltwo" height="21">Pruebas Iniciales</td>
        <td class="lblthree"><?=utf8_encode($row["pruebas_ini"])?></td>
      </tr>
      <?php
	  if($row["equipo_operativo"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">¿Equipo Operativo?</td>
        <td class="lblthree"><?=utf8_encode($row["equipo_operativo"])?></td>
      </tr>
      <?php }?>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">MANTENIMIENTO (CLIMATIZACIÓN A)</td>
      </tr>
      <tr>
        <td width="29%" class="lbltwo" height="21">Evaporador / Condensador</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      <?php
	  if($row["limpia_serpin_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Serpert&iacute;n</td>
        <td class="lblthree"><?=utf8_encode($row["limpia_serpin_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_filtro_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Filtros</td>
        <td class="lblthree"><?=utf8_encode($row["limpia_filtro_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_filtro_com_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Filtros - Observaciones</td>
        <td class="lblthree"><?=utf8_encode($row["limpia_filtro_com_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_carca_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Carcaza</td>
        <td class="lblthree"><?=utf8_encode($row["limpia_carca_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_carca_com_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Carcaza - Comentarios</td>
        <td class="lblthree"><?=utf8_encode($row["limpia_carca_com_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_motor_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza y Lubricaci&oacute;n Motores</td>
        <td class="lblthree"><?=utf8_encode($row["limpia_motor_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_motor_com_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza y Lubricaci&oacute;n Motores - Comen.</td>
        <td class="lblthree"><?=utf8_encode($row["limpia_motor_com_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_elec_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Parte El&eacute;ctrica</td>
        <td class="lblthree"><?=utf8_encode($row["limpia_elec_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_elec_com_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Parte El&eacute;ctrica - Comen.</td>
        <td class="lblthree"><?=utf8_encode($row["limpia_elec_com_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_drenaje"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Drenaje</td>
        <td class="lblthree"><?=utf8_encode($row["limpia_drenaje"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["condensador_mant"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree"><?=utf8_encode($row["condensador_mant"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["mante_fotos"] != ''){ ?>
      <tr>
        <td class="lbltwo" height="21">Captura de Fotos</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" class="lblone">
        <?php
        	if($row["mante_fotos"] != ''){
				$fotos = explode("|", substr($row["mante_fotos"],0,-1));
				for($i=0;$i<count($fotos);$i++){
					?><a href="reportes_vistos/mantenimiento/<?= $fotos[$i] ?>"><img src="reportes_vistos/mantenimiento/<?= $fotos[$i] ?>" width="143" height="147" /></a>&nbsp;<?
				}
			}
		?>
        </td>
        </tr>
        <?php }?>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">PRUEBAS FINALES EQUIPO (CLIMATIZACION)</td>
      </tr>
      <?php
	  if($row["pf_pregunta_1"] != ''){ ?>
      <tr>
        <td width="33%" class="lblone" height="21">¿El Equipo Enciende y Funciona?</td>
        <td width="67%" class="lblthree"><?=utf8_encode($row["pf_pregunta_1"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["pf_pregunta_2"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">¿El Equipo Enciende y Funciona? - Comen.</td>
        <td class="lblthree"><?=utf8_encode($row["pf_pregunta_2"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["pf_pregunta_3"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Prueba de Arranque y Funcionamiento</td>
        <td class="lblthree"><?=utf8_encode($row["pf_pregunta_3"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["pf_pregunta_4"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">¿Se Realiz&oacute; la Prueba de Drenaje?</td>
        <td class="lblthree"><?=utf8_encode($row["pf_pregunta_4"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["pf_pregunta_5"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">¿Se Realiz&oacute; la Prueba de Drenaje? - Comen.</td>
        <td class="lblthree"><?=utf8_encode($row["pf_pregunta_5"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["pf_observa"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Observaciones Generales del Equipo</td>
        <td class="lblthree"><?=utf8_encode($row["pf_observa"])?></td>
      </tr>
      <?php }?>
    </table></td>
  </tr>
</table>
  <?php }?>
  
  <?php if($row["tipo_reporte"] == "MANTENIMIENTO VENTILACION"){?>
<table width="840" border="0" align="center" cellpadding="0" cellspacing="0" class="tblwithe">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="separa">&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">INFORMACION DEL EQUIPO #<?php echo $conEqui;?></td>
      </tr>
      <tr>
        <td width="29%" class="lblone" height="21">Tipo de Equipo</td>
        <td width="71%" class="lblthree"><?=utf8_encode($row["tipo_equipo"])?></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">CARACTERISTICAS EQUIPO (VENTILACI&Oacute;N)</td>
      </tr>
      <?php
	  if($row["codigo_equipo"] != ''){ ?>
      <tr>
        <td width="29%" class="lblone" height="21">C&oacute;digo del Equipo</td>
        <td width="71%" class="lblthree"><?=utf8_encode($row["codigo_equipo"])?></td>
      </tr>
      <?php }?>
      <?php
	  if($row["desc_equipo"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n del Equipo</td>
        <td class="lblthree"><?=utf8_encode($row["desc_equipo"])?></td>
      </tr>
      <?php }?>
      
       <?php
	  if($row["marca_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Marca</td>
        <td class="lblthree"><?=utf8_encode($row["marca_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["capacidad_hp_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Capacidad (HP)</td>
        <td class="lblthree"><?=utf8_encode($row["capacidad_hp_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["modelo_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Modelo</td>
        <td class="lblthree"><?=utf8_encode($row["modelo_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["serie_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Serie</td>
        <td class="lblthree"><?=utf8_encode($row["serie_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["observa2"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree"><?=utf8_encode($row["observa2"])?></td>
      </tr>
      <?php }?>
      <tr>
        <td class="lbltwo" height="21">Pruebas Iniciales</td>
        <td class="lblthree"><?=utf8_encode($row["pruebas_ini"])?></td>
      </tr>
      <?php
	  if($row["equipo_operativo"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">¿Equipo Operativo?</td>
        <td class="lblthree"><?=utf8_encode($row["equipo_operativo"])?></td>
      </tr>
      <?php }?>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">MANTENIMIENTO (VENTILACI&Oacute;N)</td>
      </tr>
      <tr>
        <td width="29%" class="lbltwo" height="21">Evaporador / Condensador</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
      
      
      <?php
	  if($row["limpia_filtro_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Filtros</td>
        <td class="lblthree"><?=utf8_encode($row["limpia_filtro_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_filtro_com_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Filtros - Observaciones</td>
        <td class="lblthree"><?=utf8_encode($row["limpia_filtro_com_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_carca_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Carcaza</td>
        <td class="lblthree"><?=utf8_encode($row["limpia_carca_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_carca_com_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Carcaza - Comentarios</td>
        <td class="lblthree"><?=utf8_encode($row["limpia_carca_com_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_motor_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza y Lubricaci&oacute;n Motores</td>
        <td class="lblthree"><?=utf8_encode($row["limpia_motor_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_motor_com_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza y Lubricaci&oacute;n Motores - Comen.</td>
        <td class="lblthree"><?=utf8_encode($row["limpia_motor_com_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_elec_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Parte El&eacute;ctrica</td>
        <td class="lblthree"><?=utf8_encode($row["limpia_elec_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_elec_com_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Parte El&eacute;ctrica - Comen.</td>
        <td class="lblthree"><?=utf8_encode($row["limpia_elec_com_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["ajuste_band_polea"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Ajuste de Bandas y Poleas</td>
        <td class="lblthree"><?=utf8_encode($row["ajuste_band_polea"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["ajuste_band_polea_com"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Ajuste de Bandas y Poleas Comentarios</td>
        <td class="lblthree"><?=utf8_encode($row["ajuste_band_polea_com"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["lim_lubri_roda"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza y Lubricaci&oacute;n de Rodamientos</td>
        <td class="lblthree"><?=utf8_encode($row["lim_lubri_roda"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["lim_lubri_roda_com"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza y Lubricaci&oacute;n de Rodamientos<br />Comentarios</td>
        <td class="lblthree"><?=utf8_encode($row["lim_lubri_roda_com"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["condensador_mant"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree"><?=utf8_encode($row["condensador_mant"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["mante_fotos"] != ''){ ?>
      <tr>
        <td class="lbltwo" height="21">Captura de Fotos</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" class="lblone">
        <?php
        	if($row["mante_fotos"] != ''){
				$fotos = explode("|", substr($row["mante_fotos"],0,-1));
				for($i=0;$i<count($fotos);$i++){
					?><a href="reportes_vistos/mantenimiento/<?= $fotos[$i] ?>"><img src="reportes_vistos/mantenimiento/<?= $fotos[$i] ?>" width="143" height="147" /></a>&nbsp;<?
				}
			}
		?>
        </td>
        </tr>
        <?php }?>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">PRUEBAS FINALES EQUIPO (VENTILACI&Oacute;N)</td>
      </tr>
      <?php
	  if($row["pf_pregunta_1"] != ''){ ?>
      <tr>
        <td width="33%" class="lblone" height="21">¿El Equipo Enciende y Funciona?</td>
        <td width="67%" class="lblthree"><?=utf8_encode($row["pf_pregunta_1"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["pf_pregunta_2"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">¿El Equipo Enciende y Funciona? - Comen.</td>
        <td class="lblthree"><?=utf8_encode($row["pf_pregunta_2"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["pf_pregunta_3"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Prueba de Arranque y Funcionamiento</td>
        <td class="lblthree"><?=utf8_encode($row["pf_pregunta_3"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["pf_observa"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Observaciones Generales del Equipo</td>
        <td class="lblthree"><?=utf8_encode($row["pf_observa"])?></td>
      </tr>
      <?php }?>
    </table></td>
  </tr>
</table>
  <?php }?>
  
  <?php if($row["tipo_reporte"] == "MANTENIMIENTO REFRIGERACION"){?>
<table width="840" border="0" align="center" cellpadding="0" cellspacing="0" class="tblwithe">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="separa">&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">INFORMACION DEL EQUIPO #<?php echo $conEqui;?></td>
      </tr>
      <tr>
        <td width="29%" class="lblone" height="21">Tipo de Equipo</td>
        <td width="71%" class="lblthree"><?=utf8_encode($row["tipo_equipo"])?></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">CARACTERISTICAS EQUIPO (REFRIGERACI&Oacute;N)</td>
      </tr>
      <?php
	  if($row["codigo_equipo"] != ''){ ?>
      <tr>
        <td width="29%" class="lblone" height="21">C&oacute;digo del Equipo</td>
        <td width="71%" class="lblthree"><?=utf8_encode($row["codigo_equipo"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["desc_equipo"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n del Equipo</td>
        <td class="lblthree"><?=utf8_encode($row["desc_equipo"])?></td>
      </tr>
      <?php }?>
      
      <tr>
        <td class="lbltwo" height="21">Evaporador</td>
        <td class="lblthree" width="71%">&nbsp;</td>
      </tr>
      <?php
	  if($row["marca_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Marca</td>
        <td class="lblthree"><?=utf8_encode($row["marca_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["btu_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Capacidad (HP)</td>
        <td class="lblthree"><?=utf8_encode($row["capacidad_hp_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["modelo_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Modelo</td>
        <td class="lblthree"><?=utf8_encode($row["modelo_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["serie_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Serie</td>
        <td class="lblthree"><?=utf8_encode($row["serie_eva"])?></td>
      </tr>
      <?php }?>
      <tr>
        <td class="lbltwo" height="21">Condensador</td>
        <td class="lblthree">&nbsp;</td>
      </tr>
      
      <?php
	  if($row["marca_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Marca</td>
        <td class="lblthree"><?=utf8_encode($row["marca_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["btu_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Capacidad (HP)</td>
        <td class="lblthree"><?=utf8_encode($row["capacidad_hp_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["modelo_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Modelo</td>
        <td class="lblthree"><?=utf8_encode($row["modelo_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["serie_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Serie</td>
        <td class="lblthree"><?=utf8_encode($row["serie_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["observa2"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree"><?=utf8_encode($row["observa2"])?></td>
      </tr>
      <?php }?>
      
      <tr>
        <td class="lbltwo" height="21">Pruebas Iniciales</td>
        <td class="lblthree"><?=utf8_encode($row["pruebas_ini"])?></td>
      </tr>
      <?php
	  if($row["equipo_operativo"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">¿Equipo Operativo?</td>
        <td class="lblthree"><?=utf8_encode($row["equipo_operativo"])?></td>
      </tr>
      <?php }?>
      
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">MANTENIMIENTO (REFRIGERACI&Oacute;N)</td>
      </tr>
      <tr>
        <td width="29%" class="lbltwo" height="21">Evaporador</td>
        <td width="71%" class="lblthree">&nbsp;</td>
      </tr>
        
      <?php
	  if($row["limpia_filtro_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Filtros</td>
        <td class="lblthree"><?=utf8_encode($row["limpia_filtro_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_filtro_com_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Filtros - Observaciones</td>
        <td class="lblthree"><?=utf8_encode($row["limpia_filtro_com_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_carca_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Carcaza</td>
        <td class="lblthree"><?=utf8_encode($row["limpia_carca_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_carca_com_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Carcaza - Comentarios</td>
        <td class="lblthree"><?=utf8_encode($row["limpia_carca_com_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_motor_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza y Lubricaci&oacute;n Motores</td>
        <td class="lblthree"><?=utf8_encode($row["limpia_motor_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_motor_com_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza y Lubricaci&oacute;n Motores - Comen.</td>
        <td class="lblthree"><?=utf8_encode($row["limpia_motor_com_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_elec_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Parte El&eacute;ctrica</td>
        <td class="lblthree"><?=utf8_encode($row["limpia_elec_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_elec_com_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Parte El&eacute;ctrica - Comen.</td>
        <td class="lblthree"><?=utf8_encode($row["limpia_elec_com_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_drenaje"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Drenaje</td>
        <td class="lblthree"><?=utf8_encode($row["limpia_drenaje"])?></td>
      </tr>
      <?php }?>
      
      <tr>
        <td class="lbltwo" height="21">Condensador</td>
        <td class="lblthree">&nbsp;</td>
      </tr>
      
	  <?php
	  if($row["limpia_serpin_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Serpert&iacute;n</td>
        <td class="lblthree"><?=utf8_encode($row["limpia_serpin_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_carca_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Carcaza</td>
        <td class="lblthree"><?=utf8_encode($row["limpia_carca_conde"])?></td>
      </tr>
      <?php }?>
      
       <?php
	  if($row["limpia_carca_com_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Carcaza - Comentarios</td>
        <td class="lblthree"><?=utf8_encode($row["limpia_carca_com_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_motor_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza y Lubricaci&oacute;n Motores</td>
        <td class="lblthree"><?=utf8_encode($row["limpia_motor_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_motor_com_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza y Lubricaci&oacute;n Motores - Comen.</td>
        <td class="lblthree"><?=utf8_encode($row["limpia_motor_com_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_elec_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Parte El&eacute;ctrica</td>
        <td class="lblthree"><?=utf8_encode($row["limpia_elec_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["limpia_elec_com_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Limpieza de Parte El&eacute;ctrica - Comen.</td>
        <td class="lblthree"><?=utf8_encode($row["limpia_elec_com_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["condensador_mant"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree"><?=utf8_encode($row["condensador_mant"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["mante_fotos"] != ''){ ?>
      <tr>
        <td class="lbltwo" height="21">Captura de Fotos</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" class="lblone">
        <?php
        	if($row["mante_fotos"] != ''){
				$fotos = explode("|", substr($row["mante_fotos"],0,-1));
				for($i=0;$i<count($fotos);$i++){
					?><a href="reportes_vistos/mantenimiento/<?= $fotos[$i] ?>"><img src="reportes_vistos/mantenimiento/<?= $fotos[$i] ?>" width="143" height="147" /></a>&nbsp;<?
				}
			}
		?>
        </td>
        </tr>
        <?php }?>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">PRUEBAS FINALES EQUIPO (REFRIGERACI&Oacute;N)</td>
      </tr>
      <?php
	  if($row["pf_pregunta_1"] != ''){ ?>
      <tr>
        <td width="33%" class="lblone" height="21">¿El Equipo Enciende y Funciona?</td>
        <td width="67%" class="lblthree"><?=utf8_encode($row["pf_pregunta_1"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["pf_pregunta_2"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">¿El Equipo Enciende y Funciona? - Comen.</td>
        <td class="lblthree"><?=utf8_encode($row["pf_pregunta_2"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["pf_pregunta_3"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Prueba de Arranque y Funcionamiento</td>
        <td class="lblthree"><?=utf8_encode($row["pf_pregunta_3"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["pf_observa"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Observaciones Generales del Equipo</td>
        <td class="lblthree"><?=utf8_encode($row["pf_observa"])?></td>
      </tr>
      <?php }?>
    </table></td>
  </tr>
</table>
  <?php }?>  
  
  <?php }?>

<!-- END MANTENIMIENTO -->


<!-- --------------------------------------------------------------------------------------------------------------------------------- -->


<!-- REVISION MANTENIMIENTO -->

<?php 
$sql = "SELECT * FROM `reportes_revision_mantenimiento` WHERE id_orden IN($ids_orden)";


$res = $link->query($sql);

while($row = $res->fetch_assoc()){
	$conEqui++;
?>


<?php if($row["tipo_reporte"] == "REVISION CLIMATIZACION A" || $row["tipo_reporte"] == "REVISION CLIMATIZACION B"){?>
<table width="840" border="0" align="center" cellpadding="0" cellspacing="0" class="tblwithe">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="separa">&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">INFORMACION DEL EQUIPO #<?php echo $conEqui;?></td>
      </tr>
      <tr>
        <td width="29%" class="lblone" height="21">Tipo de Equipo</td>
        <td width="71%" class="lblthree"><?=utf8_encode($row["infequi_tipo"])?></td>
      </tr>
      <?php
	  if(trim($row["infequi_area_clima"]) != ''){ ?>
      <tr>
        <td class="lblone" height="21">Area Que Climatiza</td>
        <td class="lblthree"><?=utf8_encode($row["infequi_area_clima"])?></td>
      </tr>
      <?php }?>
      
       <?php
	  if($row["infequi_area_nom"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Nombre del Area</td>
        <td class="lblthree"><?=utf8_encode($row["infequi_area_nom"])?></td>
      </tr>
      <?php }?>
      
      <?php if($row["infequi_observa"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree"><?=utf8_encode($row["infequi_observa"])?></td>
      </tr>
      <?php }?>
      
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">CARACTERISTICAS EQUIPO #<?php echo $conEqui;?> (CLIMATIZACION)</td>
      </tr>
      
      <?php
	  if($row["carac_desc_equi"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n del Equipo</td>
        <td width="71%" class="lblthree"><?=utf8_encode($row["carac_desc_equi"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["carac_btu"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Capacidad (BTU)</td>
        <td class="lblthree"><?=utf8_encode($row["carac_btu"])?></td>
      </tr>
      <?php }?>
     
     <?php
	  if($row["carac_cantidad"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Cantidad</td>
        <td class="lblthree"><?=utf8_encode($row["carac_cantidad"])?></td>
      </tr>
      <?php }?>
      
       <?php
	  if($row["carac_observa"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree"><?=utf8_encode($row["carac_observa"])?></td>
      </tr>
      <?php }?>
      <?php
	  if($row["carac_otra"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Agregar otra Descripci&oacute;n de Equipo</td>
        <td class="lblthree"><?=utf8_encode($row["carac_otra"])?></td>
      </tr>
      <?php }?>
      
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <!--<tr>
    <td><table width="840" border="0" align="center" cellpadding="0" cellspacing="0" class="tblwithe">
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td colspan="2" class="lbltitle">ESPECIFICACIONES DEL TRABAJO</td>
          </tr>
          <?php
	  if($row["espec_dif_acceso"] != ''){ ?>
          <tr>
            <td width="29%" height="21" class="lblone">Dificultad de Acceso a Equipos</td>
            <td width="71%" class="lblthree"><?=utf8_encode($row["espec_dif_acceso"])?></td>
          </tr>
          <?php }?>
          <?php
	  if($row["espec_dif_accesocom"] != ''){ ?>
          <tr>
            <td class="lblone" height="21">Dificultad de Acceso a Equipos <br />
              Comentarios</td>
            <td class="lblthree"><?=utf8_encode($row["espec_dif_accesocom"])?></td>
          </tr>
          <?php }?>
          <?php
	  if($row["espec_acceso_agua"] != ''){ ?>
          <tr>
            <td class="lblone" height="21">Acceso a Puntos de Agua y Corriente</td>
            <td class="lblthree"><?=utf8_encode($row["espec_acceso_agua"])?></td>
          </tr>
          <?php }?>
          <?php
	  if($row["espec_acceso_aguacom"] != ''){ ?>
          <tr>
            <td class="lblone" height="21">Acceso a Puntos de Agua y Corriente <br />
              Comentarios</td>
            <td class="lblthree"><?=utf8_encode($row["espec_acceso_aguacom"])?></td>
          </tr>
          <?php }?>
          <?php
	  if($row["espec_fotos"] != ''){ ?>
          <tr>
            <td class="lbltwo" height="21">Captura de Fotos</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2" class="lblone"><?php
        	if($row["espec_fotos"] != ''){
				$fotos = explode("|", substr($row["espec_fotos"],0,-1));
				for($i=0;$i<count($fotos);$i++){
					?>
              <img src="reportes_vistos/revision_mantenimiento/<?php echo $fotos[$i];?>" width="143" height="147" />&nbsp;
              <?
				}
			}
		?></td>
          </tr>
          <?php }?>
          <?php
       if($row["espec_observa"] != ''): ?>
          <tr>
            <td class="lblone" height="21">Observaciones</td>
            <td class="lblthree"><?=utf8_encode($row["espec_observa"])?></td>
          </tr>
        <?php endif; ?>
        </table></td>
      </tr>
      
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td colspan="2" class="lbltitle">HERRAMIENTAS DE TRABAJO</td>
          </tr>
          <?php
	  if($row["herra_necesita"] != ''){ ?>
          <tr>
            <td width="33%" height="21" valign="top" class="lblone">Herramientas necesarias para el trabajo</td>
            <td width="67%" class="lblthree"><?=utf8_encode($row["herra_necesita"])?></td>
          </tr>
          <?php }?>
          <?php
	  if($row["herra_otras"] != ''){ ?>
          <tr>
            <td class="lblone" height="21">Si eligi&oacute; la opci&oacute;n OTROS por favor especificar</td>
            <td class="lblthree"><?=utf8_encode($row["herra_otras"])?></td>
          </tr>
          <?php }?>
        </table></td>
      </tr>
    </table></td>
  </tr>-->
</table>
  <?php }?>

<?php if($row["tipo_reporte"] == "REVISION VENTILACION"){?>
<table width="840" border="0" align="center" cellpadding="0" cellspacing="0" class="tblwithe">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="separa">&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">INFORMACION DEL EQUIPO #<?php echo $conEqui;?></td>
      </tr>
      <tr>
        <td width="29%" class="lblone" height="21">Tipo de Equipo</td>
        <td width="71%" class="lblthree"><?=utf8_encode($row["infequi_tipo"])?></td>
      </tr>
      <?php
	  if(trim($row["infequi_area_clima"]) != ''){ ?>
      <tr>
        <td class="lblone" height="21">Area Que Climatiza</td>
        <td class="lblthree"><?=utf8_encode($row["infequi_area_clima"])?></td>
      </tr>
      <?php }?>
      
       <?php
	  if($row["infequi_area_nom"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Nombre del Area</td>
        <td class="lblthree"><?=utf8_encode($row["infequi_area_nom"])?></td>
      </tr>
      <?php }?>
      
      <?php if($row["infequi_observa"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree"><?=utf8_encode($row["infequi_observa"])?></td>
      </tr>
      <?php }?>
      
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">CARACTERISTICAS EQUIPO #<?php echo $conEqui;?> (VENTILACION)</td>
      </tr>
      
      <?php
	  if($row["carac_desc_equi"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n del Equipo</td>
        <td width="71%" class="lblthree"><?=utf8_encode($row["carac_desc_equi"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["carac_hp"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Capacidad (HP)</td>
        <td class="lblthree"><?=utf8_encode($row["carac_hp"])?></td>
      </tr>
      <?php }?>
      
       <?php
	  if($row["carac_cfm"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Capacidad (CFM)</td>
        <td class="lblthree"><?=utf8_encode($row["carac_cfm"])?></td>
      </tr>
      <?php }?>
     
     <?php
	  if($row["carac_cantidad"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Cantidad</td>
        <td class="lblthree"><?=utf8_encode($row["carac_cantidad"])?></td>
      </tr>
      <?php }?>
      
       <?php
	  if($row["carac_observa"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor"  contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_revision_mantenimiento" data-column="carac_observa"><?=utf8_encode($row["carac_observa"])?></td>
      </tr>
      <?php }?>
      <?php
	  if($row["carac_otra"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Agregar otra Descripci&oacute;n de Equipo</td>
        <td class="lblthree"><?=utf8_encode($row["carac_otra"])?></td>
      </tr>
      <?php }?>
    </table></td>
  </tr>
  <!--<tr>
    <td><table width="840" border="0" align="center" cellpadding="0" cellspacing="0" class="tblwithe">
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td colspan="2" class="lbltitle">ESPECIFICACIONES DEL TRABAJO</td>
          </tr>
          <?php
	  if($row["espec_dif_acceso"] != ''){ ?>
          <tr>
            <td width="29%" height="21" class="lblone">Dificultad de Acceso a Equipos</td>
            <td width="71%" class="lblthree"><?=utf8_encode($row["espec_dif_acceso"])?></td>
          </tr>
          <?php }?>
          <?php
	  if($row["espec_dif_accesocom"] != ''){ ?>
          <tr>
            <td class="lblone" height="21">Dificultad de Acceso a Equipos <br />
              Comentarios</td>
            <td class="lblthree"><?=utf8_encode($row["espec_dif_accesocom"])?></td>
          </tr>
          <?php }?>
          <?php
	  if($row["espec_acceso_agua"] != ''){ ?>
          <tr>
            <td class="lblone" height="21">Acceso a Puntos de Agua y Corriente</td>
            <td class="lblthree"><?=utf8_encode($row["espec_acceso_agua"])?></td>
          </tr>
          <?php }?>
          <?php
	  if($row["espec_acceso_aguacom"] != ''){ ?>
          <tr>
            <td class="lblone" height="21">Acceso a Puntos de Agua y Corriente <br />
              Comentarios</td>
            <td class="lblthree"><?=utf8_encode($row["espec_acceso_aguacom"])?></td>
          </tr>
          <?php }?>
          <?php
	  if($row["espec_observa"] != ''){ ?>
          <tr>
            <td class="lblone" height="21">Observaciones</td>
            <td class="lblthree"><?=utf8_encode($row["espec_observa"])?></td>
          </tr>
          <?php }?>
          <?php
	  if($row["espec_fotos"] != ''){ ?>
          <tr>
            <td class="lbltwo" height="21">Captura de Fotos</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2" class="lblone"><?php
        	if($row["espec_fotos"] != ''){
				$fotos = explode("|", substr($row["espec_fotos"],0,-1));
				for($i=0;$i<count($fotos);$i++){
					?>
              <img src="reportes_vistos/revision_mantenimiento/<?php echo $fotos[$i];?>" width="143" height="147" />&nbsp;
              <?
				}
			}
		?></td>
          </tr>
          <?php }?>
        </table></td>
      </tr>
      
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td colspan="2" class="lbltitle">HERRAMIENTAS DE TRABAJO</td>
          </tr>
          <?php
	  if($row["herra_necesita"] != ''){ ?>
          <tr>
            <td width="33%" height="21" valign="top" class="lblone">Herramientas necesarias para el trabajo</td>
            <td width="67%" class="lblthree"><?=utf8_encode($row["herra_necesita"])?></td>
          </tr>
          <?php }?>
          <?php
	  if($row["herra_otras"] != ''){ ?>
          <tr>
            <td class="lblone" height="21">Si eligi&oacute; la opci&oacute;n OTROS por favor especificar</td>
            <td class="lblthree"><?=utf8_encode($row["herra_otras"])?></td>
          </tr>
          <?php }?>
        </table></td>
      </tr>
    </table></td>
  </tr>-->
</table>
  <?php }?>

<?php if($row["tipo_reporte"] == "REVISION REFRIGERACION"){?>
<table width="840" border="0" align="center" cellpadding="0" cellspacing="0" class="tblwithe">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="separa">&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">INFORMACION DEL EQUIPO #<?php echo $conEqui;?></td>
      </tr>
      <tr>
        <td width="29%" class="lblone" height="21">Tipo de Equipo</td>
        <td width="71%" class="lblthree editor"  contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_revision_mantenimiento" data-column="infequi_tipo"><?=utf8_encode($row["infequi_tipo"])?></td>
      </tr>
      <?php
	  if(trim($row["infequi_area_clima"]) != ''){ ?>
      <tr>
        <td class="lblone" height="21">Area Que Climatiza</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_revision_mantenimiento" data-column="infequi_area_clima"><?=utf8_encode($row["infequi_area_clima"])?></td>
      </tr>
      <?php }?>
      
       <?php
	  if($row["infequi_area_nom"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Nombre del Area</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_revision_mantenimiento" data-column="infequi_area_nom"><?=utf8_encode($row["infequi_area_nom"])?></td>
      </tr>
      <?php }?>
      
      <?php if($row["infequi_observa"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree infequi_observa" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_revision_mantenimiento" data-column="infequi_observa"><?=utf8_encode($row["infequi_observa"])?></td>
      </tr>
      <?php }?>
      
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">CARACTERISTICAS EQUIPO #<?php echo $conEqui;?> (REFRIGERACION)</td>
      </tr>
      
      <?php
	  if($row["carac_desc_equi"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n del Equipo</td>
        <td width="71%" class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_revision_mantenimiento" data-column="carac_desc_equi"><?=utf8_encode($row["carac_desc_equi"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["carac_hp"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Capacidad (HP)</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>" data-table="reportes_revision_mantenimiento" data-column="carac_hp"><?=utf8_encode($row["carac_hp"])?></td>
      </tr>
      <?php }?>
      
       <?php
	  if($row["carac_cfm"] != ''){ ?>
      <?php }?>
     
     <?php
	  if($row["carac_cantidad"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Cantidad</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>"  data-table="reportes_revision_mantenimiento"  data-column="carac_cantidad" ><?=utf8_encode($row["carac_cantidad"])?></td>
      </tr>
      <?php }?>
      
       <?php
	  if($row["caract_num_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Cantidad Evaporadores</td>
        <td class="lblthree editor" contenteditable="true" data-id = "<?= $row["id"] ?>"  data-table="reportes_revision_mantenimiento"   data-column="caract_num_eva"><?=utf8_encode($row["caract_num_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["caract_num_venti_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Cantidad Motor Ventiladores <br />(Evaporadores)</td>
        <td class="lblthree editor" contenteditable="true"  data-id = "<?= $row["id"] ?>"  data-table="reportes_revision_mantenimiento" data-column="caract_num_venti_eva"><?=utf8_encode($row["caract_num_venti_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["caract_num_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Cantidad Unidad Condensadoras</td>
        <td class="lblthree editor" contenteditable="true"  data-id = "<?= $row["id"] ?>"  data-table="reportes_revision_mantenimiento" data-column="caract_num_conde"><?=utf8_encode($row["caract_num_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["caract_num_venti_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Cantidad Motor Ventiladores <br />(Condensadoras)</td>
        <td class="lblthree editor" contenteditable="true"  data-id = "<?= $row["id"] ?>"  data-table="reportes_revision_mantenimiento"  data-column="caract_num_venti_conde"><?=utf8_encode($row["caract_num_venti_conde"])?></td>
      </tr>
      <?php }?>
      
       <?php
	  if($row["carac_observa"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree editor" contenteditable="true"  data-id = "<?= $row["id"] ?>"  data-table="reportes_revision_mantenimiento"  data-column="carac_observa"><?=utf8_encode($row["carac_observa"])?></td>
      </tr>
      <?php }?>
      <?php
	  if($row["carac_otra"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Agregar otra Descripci&oacute;n de Equipo</td>
        <td class="lblthree editor" contenteditable="true"  data-id = "<?= $row["id"] ?>"  data-table="reportes_revision_mantenimiento"  data-column="carac_otra"><?=utf8_encode($row["carac_otra"])?></td>
      </tr>
      <?php }?>
    </table></td>
  </tr>
  <!--<tr>
    <td><table width="840" border="0" align="center" cellpadding="0" cellspacing="0" class="tblwithe">
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td colspan="2" class="lbltitle">ESPECIFICACIONES DEL TRABAJO</td>
          </tr>
          <?php
	  if($row["espec_dif_acceso"] != ''){ ?>
          <tr>
            <td width="29%" height="21" class="lblone">Dificultad de Acceso a Equipos</td>
            <td width="71%" class="lblthree"><?=utf8_encode($row["espec_dif_acceso"])?></td>
          </tr>
          <?php }?>
          <?php
	  if($row["espec_dif_accesocom"] != ''){ ?>
          <tr>
            <td class="lblone" height="21">Dificultad de Acceso a Equipos <br />
              Comentarios</td>
            <td class="lblthree"><?=utf8_encode($row["espec_dif_accesocom"])?></td>
          </tr>
          <?php }?>
          <?php
	  if($row["espec_acceso_agua"] != ''){ ?>
          <tr>
            <td class="lblone" height="21">Acceso a Puntos de Agua y Corriente</td>
            <td class="lblthree"><?=utf8_encode($row["espec_acceso_agua"])?></td>
          </tr>
          <?php }?>
          <?php
	  if($row["espec_acceso_aguacom"] != ''){ ?>
          <tr>
            <td class="lblone" height="21">Acceso a Puntos de Agua y Corriente <br />
              Comentarios</td>
            <td class="lblthree"><?=utf8_encode($row["espec_acceso_aguacom"])?></td>
          </tr>
          <?php }?>
          <?php
	  if($row["espec_observa"] != ''){ ?>
          <tr>
            <td class="lblone" height="21">Observaciones</td>
            <td class="lblthree"><?=utf8_encode($row["espec_observa"])?></td>
          </tr>
          <?php }?>
          <?php
	  if($row["espec_fotos"] != ''){ ?>
          <tr>
            <td class="lbltwo" height="21">Captura de Fotos</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2" class="lblone"><?php
        	if($row["espec_fotos"] != ''){
				$fotos = explode("|", substr($row["espec_fotos"],0,-1));
				for($i=0;$i<count($fotos);$i++){
					?>
              <img src="reportes_vistos/revision_mantenimiento/<?php echo $fotos[$i];?>" width="143" height="147" />&nbsp;
              <?
				}
			}
		?></td>
          </tr>
          <?php }?>
        </table></td>
      </tr> 
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td colspan="2" class="lbltitle">HERRAMIENTAS DE TRABAJO</td>
          </tr>
          <?php
	  if($row["herra_necesita"] != ''){ ?>
          <tr>
            <td width="33%" height="21" valign="top" class="lblone">Herramientas necesarias para el trabajo</td>
            <td width="67%" class="lblthree"><?=utf8_encode($row["herra_necesita"])?></td>
          </tr>
          <?php }?>
          <?php
	  if($row["herra_otras"] != ''){ ?>
          <tr>
            <td class="lblone" height="21">Si eligi&oacute; la opci&oacute;n OTROS por favor especificar</td>
            <td class="lblthree"><?=utf8_encode($row["herra_otras"])?></td>
          </tr>
          <?php }?>
        </table></td>
      </tr>
    </table></td>
  </tr>-->
</table>
  <?php }?>


<?php }?>


<!-- END REVISION MANTENIMIENTO -->


<!-- REVISION INSTALACION -->

<?php 
$sql = "SELECT * FROM `reportes_revision_instalacion` WHERE id_orden IN($ids_orden) ORDER BY order_time";

$res = $link->query($sql);

while($row = $res->fetch_assoc()){
  $conEqui++;
?>


<?php if($row["tipo_reporte"] != ""){ ?>
    <table width="840" border="0" align="center" cellpadding="0" cellspacing="0" class="tblwithe">
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td class="separa">&nbsp;</td>
      </tr>
      <tr>
        <td class="multitable">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td colspan="2" class="lbltitle">INFORMACION DEL EQUIPO #<?= $conEqui;?></td>
            </tr>
            <tr>
              <td width="29%" class="lblone" height="21">Tipo de Equipo</td>
              <td width="71%" class="lblthree"><?=utf8_encode($row["tipo_equipo"])?></td>
            </tr>

            <?php if(trim($row["area_clima"]) != ''): ?>
            <tr>
              <td class="lblone" height="21">Area Que Climatiza</td>
              <td class="lblthree editor" contenteditable="true"  data-id="<?= $row["id"] ?>" data-table="reportes_revision_instalacion" data-column="area_clima"><?=utf8_encode($row["area_clima"])?></td>
            </tr>
            <?php endif; ?>
        
            <?php if($row["nombre_area"] != ''): ?>
            <tr>
              <td class="lblone" height="21">Nombre del Area</td>
              <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>"  data-column="nombre_area"><?=utf8_encode($row["nombre_area"])?></td>
            </tr>
            <?php endif; ?>
            
            <?php if($row["observa2"] != ''): ?>
            <tr>
              <td class="lblone" height="21">Observaciones</td>
              <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="observa2"><?=utf8_encode($row["observa2"])?></td>
            </tr>
            <?php endif; ?>
          </table>
        </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="multitable">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td colspan="2" class="lbltitle">CARACTERISTICAS EQUIPO #<?= $conEqui ?> <?= utf8_encode($row["tipo_equipo"]) ?></td>
        </tr>
      
        <?php if($row["cantidad_personas_area"] != ''): ?>
        <tr>
          <td class="lblone" height="21">Cantidad de Personas por Área: </td>
          <td width="71%" class="lblthree editor" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" contenteditable="true" data-column="cantidad_personas_area">
              <?=utf8_encode($row["cantidad_personas_area"])?>
          </td>
        </tr>
        <?php endif; ?>
      
        <?php if($row["largo"] != ''): ?>
        <tr>
          <td class="lblone" height="21">Largo (metros): </td>
          <td width="71%" class="lblthree editor" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" contenteditable="true" data-column="largo"><?=utf8_encode($row["largo"])?></td>
        </tr>
        <?php endif; ?>
     
        <?php if($row["ancho"] != ''): ?>
        <tr>
          <td class="lblone" height="21">Ancho (metros): </td>
          <td width="71%" class="lblthree editor" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" contenteditable="true" data-column="ancho"><?=utf8_encode($row["ancho"])?></td>
        </tr>
        <?php endif; ?>
      
        <?php if($row["area_total"] != ''): ?>
        <tr>
          <td class="lblone" height="21">Área Total (m2):</td>
          <td width="71%" class="lblthree editor" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" contenteditable="true" data-column="area_total"><?= $row["area_total"] ?></td>
        </tr>
        <?php endif; ?>

        <tr>
          <td class="lblone" height="21">Fotos del Área Total</td>
        </tr>
        <tr>
          <td colspan="3" class="lblone">
          <?php if($row["fotos_area_total"] != ''): 
            $fotos = explode("|", substr($row["fotos_area_total"],0,-1));
            for($i=0;$i<count($fotos);$i++): ?>
                <a href="reportes_vistos/revision_instalacion/<?= $fotos[$i] ?>"><img src="reportes_vistos/revision_instalacion/<?= $fotos[$i] ?>" width="143" height="147" /></a>&nbsp;
            <?php endfor; 
          endif; ?>
          </td>
        </tr>
        <tr>
          <td class="lblone" height="21">Fotos de Ubicación de Equipos Internos</td>
        </tr>
        <tr>
          <td colspan="3" class="lblone">
          <?php if($row["fotos_interior"] != ''): 
            $fotos = explode("|", substr($row["fotos_interior"],0,-1));
            for($i=0;$i<count($fotos);$i++): ?>
                <a href="reportes_vistos/revision_instalacion/<?= $fotos[$i] ?>"><img src="reportes_vistos/revision_instalacion/<?= $fotos[$i] ?>" width="143" height="147" /></a>&nbsp;
            <?php endfor; 
          endif; ?>
          </td>
        </tr>

        <tr>
          <td class="lblone" height="21">Fotos de Ubicación de Equipos Externos</td>
        </tr>
        <tr>
          <td colspan="3" class="lblone">
          <?php if($row["fotos_exterior"] != ''): 
            $fotos = explode("|", substr($row["fotos_exterior"],0,-1));
            for($i=0;$i<count($fotos);$i++): ?>
                <a href="reportes_vistos/revision_instalacion/<?= $fotos[$i] ?>"><img src="reportes_vistos/revision_instalacion/<?= $fotos[$i] ?>" width="143" height="147" /></a>&nbsp;
            <?php endfor; 
          endif; ?>
          </td>
        </tr>
        <?php if($row["observaciones"] != ''): ?>
        <tr>
          <td class="lblone" height="21">Observaciones: </td>
          <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="observaciones"><?=utf8_encode($row["observaciones"])?></td>
        </tr>
        <?php endif; ?>
      </table>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="840" border="0" align="center" cellpadding="0" cellspacing="0" class="tblwithe">
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td colspan="2" class="lbltitle">ESPECIFICACIONES DEL TRABAJO (<?= utf8_encode($row["tipo_equipo"]) ?>)</td>
            </tr>
            <tr>
              <td width="29%" height="21" class="lblone">Equipo Recomendado</td>
              <td width="71%" class="lblthree"><?=utf8_encode($row["equipo_remendado"])?></td>
            </tr>
            <tr>
              <td class="lblone" height="21">Descripción del Equipo:</td>
              <td class="lblthree"><?=utf8_encode($row["desc_equipo"])?></td>
            </tr>
            <tr>
              <td class="lblone" height="21">Capacidad (BTU):</td>
              <td class="lblthree"><?=utf8_encode($row["capacidad_btu"])?></td>
            </tr>
            <tr>
              <td class="lblone" height="21">Tiempo Aproximado para Instalación:</td>
              <td class="lblthree"><?=utf8_encode($row["tiempo_aprox_instalacion"])?></td>
            </tr>
            <tr>
              <td class="lblone" height="21">Observaciones</td>
              <td class="lblthree"><?=utf8_encode($row["orde_observas"])?></td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td class="multitable">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td colspan="2" class="lbltitle">Materiales Requeridos</td>
            </tr>

            <?php if($row["mat_item1_sel"] != ''): ?>
            <tr>
              <td width="29%" class="lbltwo" height="21">Item #1</td>
              <td width="71%">&nbsp;</td>
            </tr>
            <tr>
              <td class="lblone" height="21">Selecci&oacute;n de Item</td>
              <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item1_sel"><?=utf8_encode($row["mat_item1_sel"])?></td>
            </tr>
            <tr>
              <td class="lblone" height="21">Descripci&oacute;n de Item</td>
              <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item1_des"><?=utf8_encode($row["mat_item1_des"])?></td>
            </tr>
            <tr>
              <td class="lblone" height="21">Definir Unidad</td>
              <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item1_uni"><?=utf8_encode($row["mat_item1_uni"])?></td>
            </tr>
            <tr>
              <td class="lblone" height="21">Cantidad Requerida</td>
              <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item1_cant"><?=utf8_encode($row["mat_item1_cant"])?></td>
            </tr>
            <tr>
              <td class="lblone" height="21">Observaciones</td>
              <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item1_obser"><?=utf8_encode($row["mat_item1_obser"])?></td>
            </tr>
            <?php endif; ?>
          </table>
        </td>
      </tr>

        <?php if($row["mat_item2_sel"] != ''): ?>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td class="multitable">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="29%" class="lbltwo" height="21">Item #2</td>
                <td width="71%">&nbsp;</td>
              </tr>
              <tr>
                <td class="lblone" height="21">Selecci&oacute;n de Item</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item2_sel"><?=utf8_encode($row["mat_item2_sel"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Descripci&oacute;n de Item</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item2_des"><?=utf8_encode($row["mat_item2_des"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Definir Unidad</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item2_uni"><?=utf8_encode($row["mat_item2_uni"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Cantidad Requerida</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item2_cant"><?=utf8_encode($row["mat_item2_cant"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Observaciones</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item2_obser"><?=utf8_encode($row["mat_item2_obser"])?></td>
              </tr>
            </table>
          </td>
        </tr>
        <?php endif; ?>
        
        <?php if($row["mat_item3_sel"] != ''): ?>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td class="multitable">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="29%" class="lbltwo" height="21">Item #3</td>
                <td width="71%">&nbsp;</td>
              </tr>
              <tr>
                <td class="lblone" height="21">Selecci&oacute;n de Item</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item3_sel"><?=utf8_encode($row["mat_item3_sel"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Descripci&oacute;n de Item</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item3_des"><?=utf8_encode($row["mat_item3_des"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Definir Unidad</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item3_uni"><?=utf8_encode($row["mat_item3_uni"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Cantidad Requerida</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item3_cant"><?=utf8_encode($row["mat_item3_cant"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Observaciones</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item3_obser"><?=utf8_encode($row["mat_item3_obser"])?></td>
              </tr>
            </table>
          </td>
        </tr>
        <?php endif; ?>
        
        <?php if($row["mat_item4_sel"] != ''): ?>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td class="multitable">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="29%" class="lbltwo" height="21">Item #4</td>
                <td width="71%">&nbsp;</td>
              </tr>
              <tr>
                <td class="lblone" height="21">Selecci&oacute;n de Item</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item4_sel"><?=utf8_encode($row["mat_item4_sel"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Descripci&oacute;n de Item</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item4_des"><?=utf8_encode($row["mat_item4_des"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Definir Unidad</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item4_uni"><?=utf8_encode($row["mat_item4_uni"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Cantidad Requerida</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item4_cant"><?=utf8_encode($row["mat_item4_cant"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Observaciones</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item4_obser"><?=utf8_encode($row["mat_item4_obser"])?></td>
              </tr>
            </table>
          </td>
        </tr>
        <?php endif; ?>
        
        <?php if($row["mat_item5_sel"] != ''): ?>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td class="multitable">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="29%" class="lbltwo" height="21">Item #5</td>
                <td width="71%">&nbsp;</td>
              </tr>
              <tr>
                <td class="lblone" height="21">Selecci&oacute;n de Item</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item5_sel"><?=utf8_encode($row["mat_item5_sel"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Descripci&oacute;n de Item</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item5_des"><?=utf8_encode($row["mat_item5_des"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Definir Unidad</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item5_uni"><?=utf8_encode($row["mat_item5_uni"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Cantidad Requerida</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item5_cant"><?=utf8_encode($row["mat_item5_cant"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Observaciones</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item5_obser"><?=utf8_encode($row["mat_item5_obser"])?></td>
              </tr>
            </table>
          </td>
        </tr>
        <?php endif; ?>
        
        <?php if($row["mat_item6_sel"] != ''): ?>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td class="multitable">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="29%" class="lbltwo" height="21">Item #6</td>
                <td width="71%">&nbsp;</td>
              </tr>
              <tr>
                <td class="lblone" height="21">Selecci&oacute;n de Item</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item6_sel"><?=utf8_encode($row["mat_item6_sel"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Descripci&oacute;n de Item</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item6_des"><?=utf8_encode($row["mat_item6_des"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Definir Unidad</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item6_uni"><?=utf8_encode($row["mat_item6_uni"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Cantidad Requerida</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item6_cant"><?=utf8_encode($row["mat_item6_cant"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Observaciones</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item6_obser"><?=utf8_encode($row["mat_item6_obser"])?></td>
              </tr>
            </table>
          </td>
        </tr>
        <?php endif; ?>
        
        <?php if($row["mat_item7_sel"] != ''): ?>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td class="multitable">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="29%" class="lbltwo" height="21">Item #7</td>
                <td width="71%">&nbsp;</td>
              </tr>
              <tr>
                <td class="lblone" height="21">Selecci&oacute;n de Item</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item7_sel"><?=utf8_encode($row["mat_item7_sel"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Descripci&oacute;n de Item</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item7_des"><?=utf8_encode($row["mat_item7_des"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Definir Unidad</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item7_uni"><?=utf8_encode($row["mat_item7_uni"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Cantidad Requerida</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item7_cant"><?=utf8_encode($row["mat_item7_cant"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Observaciones</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item7_obser"><?=utf8_encode($row["mat_item7_obser"])?></td>
              </tr>
            </table>
          </td>
        </tr>
        <?php endif; ?>
        
        <?php if($row["mat_item8_sel"] != ''): ?>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td class="multitable">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="29%" class="lbltwo" height="21">Item #8</td>
                <td width="71%">&nbsp;</td>
              </tr>
              <tr>
                <td class="lblone" height="21">Selecci&oacute;n de Item</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item8_sel"><?=utf8_encode($row["mat_item8_sel"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Descripci&oacute;n de Item</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item8_des"><?=utf8_encode($row["mat_item8_des"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Definir Unidad</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item8_uni"><?=utf8_encode($row["mat_item8_uni"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Cantidad Requerida</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item8_cant"><?=utf8_encode($row["mat_item8_cant"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Observaciones</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item8_obser"><?=utf8_encode($row["mat_item8_obser"])?></td>
              </tr>
            </table>
          </td>
        </tr>
        <?php endif; ?>
        
        <?php if($row["mat_item9_sel"] != ''): ?>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td class="multitable">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="29%" class="lbltwo" height="21">Item #9</td>
                <td width="71%">&nbsp;</td>
              </tr>
              <tr>
                <td class="lblone" height="21">Selecci&oacute;n de Item</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item9_sel"><?=utf8_encode($row["mat_item9_sel"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Descripci&oacute;n de Item</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item9_des"><?=utf8_encode($row["mat_item9_des"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Definir Unidad</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item9_uni"><?=utf8_encode($row["mat_item9_uni"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Cantidad Requerida</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item9_cant"><?=utf8_encode($row["mat_item9_cant"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Observaciones</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item9_obser"><?=utf8_encode($row["mat_item9_obser"])?></td>
              </tr>
            </table>
          </td>
        </tr>
        <?php endif; ?>
        
        <?php if($row["mat_item10_sel"] != ''): ?>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td class="multitable">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="29%" class="lbltwo" height="21">Item #10</td>
                <td width="71%">&nbsp;</td>
              </tr>
              <tr>
                <td class="lblone" height="21">Selecci&oacute;n de Item</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item10_sel"><?=utf8_encode($row["mat_item10_sel"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Descripci&oacute;n de Item</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item10_des"><?=utf8_encode($row["mat_item10_des"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Definir Unidad</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item10_uni"><?=utf8_encode($row["mat_item10_uni"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Cantidad Requerida</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item10_cant"><?=utf8_encode($row["mat_item10_cant"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Observaciones</td>
                <td class="lblthree editor" contenteditable="true" data-table="reportes_revision_instalacion" data-id="<?= $row["id"] ?>" data-column="mat_item10_obser"><?=utf8_encode($row["mat_item10_obser"])?></td>
              </tr>
            </table>
          </td>
        </tr>
        <?php endif; ?>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <?php /*if($row["herramientas"] != ""): ?>
        <tr>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td colspan="2" class="lbltitle">Herramientas de Trabajo</td>
            </tr>

            <?php foreach(explode("|", substr($row["herramientas"],0,-1)) as $herr): ?>
            <tr>
              <td width="29%" class="lbltwo" height="21">&nbsp;</td>
              <td width="71%" class="lblthree"><?= utf8_encode($herr) ?></td>
            </tr>
            <?php endforeach; ?>
          </table>
        <?php endif;*/ ?>
      </table>
    </td>
    </tr>
    </table>
  </td>
  </tr>
</table>
  <?php }
  }?>


<!-- END REVISION MANTENIMIENTO -->


<!-- REVISION CORRECTIVO -->

<?php 
$sql = "SELECT * FROM `reportes_revision_correctivo` WHERE id_orden IN($ids_orden) ORDER BY order_time";
$res = $link->query($sql);

while($row = $res->fetch_assoc()){
  $conEqui++;
?>


  <?php if($row["tipo_reporte"] != "" && $row["tipo_reporte"] != "REVISION REFRIGERACION"){ ?>
    <table width="840" border="0" align="center" cellpadding="0" cellspacing="0" class="tblwithe">
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td class="separa">&nbsp;</td>
      </tr>
      <tr>
        <td>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td colspan="2" class="lbltitle">INFORMACION DEL EQUIPO #<?= $conEqui;?></td>
            </tr>
            <tr>
              <td width="29%" class="lblone" height="21">Tipo de Equipo</td>
              <td width="71%" class="lblthree"><?=utf8_encode($row["tipo_equipo"])?></td>
            </tr>

            <?php if(trim($row["infequi_area_clima"]) != ''): ?>
            <tr>
              <td class="lblone" height="21">Area Que Climatiza</td>
              <td class="lblthree"><?=utf8_encode($row["area_clima"])?></td>
            </tr>
            <?php endif; ?>
        
            <?php if($row["infequi_area_nom"] != ''): ?>
            <tr>
              <td class="lblone" height="21">Nombre del Area</td>
              <td class="lblthree"><?=utf8_encode($row["nombre_area"])?></td>
            </tr>
            <?php endif; ?>
            
            <?php if($row["infequi_observa"] != ''): ?>
            <tr>
              <td class="lblone" height="21">Observaciones</td>
              <td class="lblthree"><?=utf8_encode($row["obaserva2"])?></td>
            </tr>
            <?php endif; ?>
          </table>
        </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td colspan="2" class="lbltitle">CARACTERISTICAS EQUIPO #<?= $conEqui ?> <?= utf8_encode($row["tipo_equipo"]) ?></td>
        </tr>
      
        <?php if($row["codigo_equipo"] != ''): ?>
        <tr>
          <td width="29%" class="lblone" height="21">Código del Equipo:</td>
          <td width="71%" class="lblthree"><?=utf8_encode($row["codigo_equipo"])?></td>
        </tr>
        <?php endif; ?>
      
        <?php if($row["desc_equipo"] != ''): ?>
        <tr>
          <td class="lblone" height="21">Descripción del Equipo: </td>
          <td class="lblthree"><?=utf8_encode($row["desc_equipo"])?></td>
        </tr>
        <?php endif; ?>
     
        <?php if($row["area_clima"] != ''): ?>
        <tr>
          <td class="lblone" height="21">Area que Climatiza:</td>
          <td class="lblthree"><?=utf8_encode($row["area_clima"])?></td>
        </tr>
        <?php endif; ?>
      
        <?php if(trim($row["nombre_area"]) != ''): ?>
        <tr>
          <td class="lblone" height="21">Nombre del Area:</td>
          <td class="lblthree"><?=utf8_encode($row["nombre_area"])?></td>
        </tr>
        <?php endif; ?>

        <?php if($row["tipo_reporte"] == "REVISION INSTALACION A" || $row["tipo_reporte"] == "REVISION INSTALACION B"): ?>
        <tr>
          <td class="lblone" height="21"><?php if($row["tipo_reporte"] == "REVISION INSTALACION A") echo "Evaporador / Condensador"; 
                                               if($row["tipo_reporte"] == "REVISION INSTALACION B") echo "Evaporador"; ?></td>
        </tr>
        <?php endif; ?>

        <?php if($row["marca"] != ''): ?>
        <tr>
          <td class="lblone" height="21">Marca:</td>
          <td class="lblthree"><?=utf8_encode($row["marca"])?></td>
        </tr>
        <?php endif; ?>
        
        <?php if($row["capacidad_hp"] != ''): ?>
        <tr>
          <td class="lblone" height="21"><?php if($row["tipo_reporte"] == "REVISION INSTALACION A" || $row["tipo_reporte"] == "REVISION INSTALACION B") echo "Capacidad (BTU):"; else echo "Capacidad (HP):"; ?></td>
          <td class="lblthree"><?=utf8_encode($row["capacidad_hp"])?></td>
        </tr>
        <?php endif; ?>

        <?php if($row["capacidad_cfm"] != '' && $row["tipo_reporte"] != "REVISION INSTALACION B"): ?>
        <tr>
          <td class="lblone" height="21">Capacidad (BTU):</td>
          <td class="lblthree"><?=utf8_encode($row["capacidad_cfm"])?></td>
        </tr>
        <?php endif; ?>

        <?php if($row["modelo"] != ''): ?>
        <tr>
          <td class="lblone" height="21">Modelo:</td>
          <td class="lblthree"><?=utf8_encode($row["modelo"])?></td>
        </tr>
        <?php endif; ?>

        <?php if($row["serie"] != ''): ?>
        <tr>
          <td class="lblone" height="21">Serie:</td>
          <td class="lblthree"><?=utf8_encode($row["serie"])?></td>
        </tr>
        <?php endif; ?>

        <?php if($row["tipo_reporte"] == "REVISION INSTALACION B"): #condensador ?>
        <tr>
          <td class="lblone" height="21">Condensador</td>
        </tr>

        <?php if($row["marca"] != ''): ?>
        <tr>
          <td class="lblone" height="21">Marca:</td>
          <td class="lblthree"><?=utf8_encode($row["marca_conde"])?></td>
        </tr>
        <?php endif; ?>

        <?php if($row["capacidad_cfm"] != ''): ?>
        <tr>
          <td class="lblone" height="21">Capacidad (BTU):</td>
          <td class="lblthree"><?=utf8_encode($row["capacidad_cfm"])?></td>
        </tr>
        <?php endif; ?>

        <?php if($row["modelo"] != ''): ?>
        <tr>
          <td class="lblone" height="21">Modelo:</td>
          <td class="lblthree"><?=utf8_encode($row["modelo_conde"])?></td>
        </tr>
        <?php endif; ?>

        <?php if($row["serie"] != ''): ?>
        <tr>
          <td class="lblone" height="21">Serie:</td>
          <td class="lblthree"><?=utf8_encode($row["serie_conde"])?></td>
        </tr>
        <?php endif; ?>
        <?php endif; ?>

        <?php if($row["observa2"] != ''): ?>
        <tr>
          <td class="lblone" height="21">Observaciones:</td>
          <td class="lblthree"><?=utf8_encode($row["observa2"])?></td>
        </tr>
        <?php endif; ?>
      </table>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="840" border="0" align="center" cellpadding="0" cellspacing="0" class="tblwithe">
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td colspan="2" class="lbltitle">Diagnostico (<?= utf8_encode($row["tipo_equipo"]) ?>)</td>
            </tr>
            <?php if($row["tipo_reporte"] == "REVISION INSTALACION B"): ?>
            <tr>
              <td colspan="2" class="lblone">Evaporador</td>
            </tr>
            <?php endif; ?>
            <tr>
              <td width="29%" height="21" class="lblone" style="vertical-align: top;">Partes no Operativas (Reparación):</td>
              <td width="71%" class="lblthree">
              <?php 
                $partes = explode("|", $row["diag_piezas_no_operativas_reparacion"]); 
                foreach($partes as $part): ?>
                  <?= utf8_encode($part) ?><br>
              <?php endforeach; ?>
              </td>
            </tr>
            <tr>
              <td class="lblone" height="21" style="vertical-align: top;">Partes no Operativas (Cambio): </td>
              <td width="71%" class="lblthree">
              <?php 
                $partes = explode("|", $row["diag_piezas_no_operativas_cambio"]); 
                foreach($partes as $part): ?>
                  <?= utf8_encode($part) ?><br>
              <?php endforeach; ?>
              </td>
            </tr>
            <?php if($row["tipo_reporte"] == "REVISION INSTALACION B"): ?>
            <tr>
              <td colspan="2" class="lblone">Condensador</td>
            </tr>
            <tr>
              <td width="29%" height="21" class="lblone" style="vertical-align: top;">Partes no Operativas (Reparación):</td>
              <td width="71%" class="lblthree">
              <?php 
                $partes = explode("|", $row["diag_piezas_no_operativas_reparacion_conde"]); 
                foreach($partes as $part): ?>
                  <?= utf8_encode($part) ?><br>
              <?php endforeach; ?>
              </td>
            </tr>
            <tr>
              <td class="lblone" height="21" style="vertical-align: top;">Partes no Operativas (Cambio): </td>
              <td width="71%" class="lblthree">
              <?php 
                $partes = explode("|", $row["diag_piezas_no_operativas_cambio_conde"]); 
                foreach($partes as $part): ?>
                  <?= utf8_encode($part) ?><br>
              <?php endforeach; ?>
              </td>
            </tr>
            <?php endif; ?>
            <tr>
              <td class="lblone" height="21">Comentarios:</td>
              <td class="lblthree"><a href="reportes_vistos/<?= $rowEnc["tipoDato"] ?>/<?= $row["diag_comentario"] ?>">Audio de Comentarios</a></td>
            </tr>
            <tr>
              <td class="lblone" height="21" style="vertical-align: top;">Captura de Fotos</td>
              <td colspan="3" class="lblone">
                <?php if($row["diag_fotos"] != ''): 
                  $fotos = explode("|", substr($row["diag_fotos"],0,-1));
                  for($i=0;$i<count($fotos);$i++): ?>
                      <a href="reportes_vistos/revision_correctivo/<?= $fotos[$i] ?>"><img src="reportes_vistos/revision_correctivo/<?= $fotos[$i] ?>" width="143" height="147" /></a>&nbsp;
                  <?php endfor; 
                endif; ?>
              </td>
            </tr>
            <tr>
              <td class="lblone" height="21">Tiempo de Trabajo:</td>
              <td class="lblthree"><?=utf8_encode($row["diag_tiempo_trabajo"])?></td>
            </tr>
            <tr>
              <td class="lblone" height="21">Observaciones:</td>
              <td class="lblthree"><?=utf8_encode($row["diag_observaciones"])?></td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td colspan="2" class="lbltitle">Materiales Requeridos</td>
            </tr>

            <?php if($row["mat_item1_sel"] != ''): ?>
            <tr>
              <td width="29%" class="lbltwo" height="21">Item #1</td>
              <td width="71%">&nbsp;</td>
            </tr>
            <tr>
              <td class="lblone" height="21">Selecci&oacute;n de Item</td>
              <td class="lblthree"><?=utf8_encode($row["mat_item1_sel"])?></td>
            </tr>
            <tr>
              <td class="lblone" height="21">Descripci&oacute;n de Item</td>
              <td class="lblthree"><?=utf8_encode($row["mat_item1_des"])?></td>
            </tr>
            <tr>
              <td class="lblone" height="21">Definir Unidad</td>
              <td class="lblthree"><?=utf8_encode($row["mat_item1_uni"])?></td>
            </tr>
            <tr>
              <td class="lblone" height="21">Cantidad Requerida</td>
              <td class="lblthree"><?=utf8_encode($row["mat_item1_cant"])?></td>
            </tr>
            <tr>
              <td class="lblone" height="21">Observaciones</td>
              <td class="lblthree"><?=utf8_encode($row["mat_item1_obser"])?></td>

            </tr>
          </table>
        </td>
        </tr>
        <?php endif; ?>

        <?php if($row["mat_item2_sel"] != ''): ?>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="29%" class="lbltwo" height="21">Item #2</td>
                <td width="71%">&nbsp;</td>
              </tr>
              <tr>
                <td class="lblone" height="21">Selecci&oacute;n de Item</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item2_sel"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Descripci&oacute;n de Item</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item2_des"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Definir Unidad</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item2_uni"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Cantidad Requerida</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item2_cant"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Observaciones</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item2_obser"])?></td>
              </tr>
            </table>
          </td>
        </tr>
        <?php endif; ?>
        
        <?php if($row["mat_item3_sel"] != ''): ?>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="29%" class="lbltwo" height="21">Item #3</td>
                <td width="71%">&nbsp;</td>
              </tr>
              <tr>
                <td class="lblone" height="21">Selecci&oacute;n de Item</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item3_sel"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Descripci&oacute;n de Item</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item3_des"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Definir Unidad</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item3_uni"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Cantidad Requerida</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item3_cant"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Observaciones</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item3_obser"])?></td>
              </tr>
            </table>
          </td>
        </tr>
        <?php endif; ?>
        
        <?php if($row["mat_item4_sel"] != ''): ?>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="29%" class="lbltwo" height="21">Item #4</td>
                <td width="71%">&nbsp;</td>
              </tr>
              <tr>
                <td class="lblone" height="21">Selecci&oacute;n de Item</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item4_sel"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Descripci&oacute;n de Item</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item4_des"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Definir Unidad</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item4_uni"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Cantidad Requerida</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item4_cant"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Observaciones</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item1_obser4"])?></td>
              </tr>
            </table>
          </td>
        </tr>
        <?php endif; ?>
        
        <?php if($row["mat_item5_sel"] != ''): ?>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="29%" class="lbltwo" height="21">Item #5</td>
                <td width="71%">&nbsp;</td>
              </tr>
              <tr>
                <td class="lblone" height="21">Selecci&oacute;n de Item</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item5_sel"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Descripci&oacute;n de Item</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item5_des"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Definir Unidad</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item5_uni"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Cantidad Requerida</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item5_cant"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Observaciones</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item1_obser5"])?></td>
              </tr>
            </table>
          </td>
        </tr>
        <?php endif; ?>
        
        <?php if($row["mat_item6_sel"] != ''): ?>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="29%" class="lbltwo" height="21">Item #6</td>
                <td width="71%">&nbsp;</td>
              </tr>
              <tr>
                <td class="lblone" height="21">Selecci&oacute;n de Item</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item6_sel"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Descripci&oacute;n de Item</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item6_des"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Definir Unidad</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item6_uni"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Cantidad Requerida</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item6_cant"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Observaciones</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item1_obse6"])?></td>
              </tr>
            </table>
          </td>
        </tr>
        <?php endif; ?>
        
        <?php if($row["mat_item7_sel"] != ''): ?>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="29%" class="lbltwo" height="21">Item #7</td>
                <td width="71%">&nbsp;</td>
              </tr>
              <tr>
                <td class="lblone" height="21">Selecci&oacute;n de Item</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item7_sel"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Descripci&oacute;n de Item</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item7_des"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Definir Unidad</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item7_uni"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Cantidad Requerida</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item7_cant"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Observaciones</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item1_obser7"])?></td>
              </tr>
            </table>
          </td>
        </tr>
        <?php endif; ?>
        
        <?php if($row["mat_item8_sel"] != ''): ?>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="29%" class="lbltwo" height="21">Item #8</td>
                <td width="71%">&nbsp;</td>
              </tr>
              <tr>
                <td class="lblone" height="21">Selecci&oacute;n de Item</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item8_sel"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Descripci&oacute;n de Item</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item8_des"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Definir Unidad</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item8_uni"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Cantidad Requerida</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item8_cant"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Observaciones</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item1_obser8"])?></td>
              </tr>
            </table>
          </td>
        </tr>
        <?php endif; ?>
        
        <?php if($row["mat_item9_sel"] != ''): ?>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="29%" class="lbltwo" height="21">Item #9</td>
                <td width="71%">&nbsp;</td>
              </tr>
              <tr>
                <td class="lblone" height="21">Selecci&oacute;n de Item</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item9_sel"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Descripci&oacute;n de Item</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item9_des"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Definir Unidad</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item9_uni"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Cantidad Requerida</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item9_cant"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Observaciones</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item1_obser9"])?></td>
              </tr>
            </table>
          </td>
        </tr>
        <?php endif; ?>
        
        <?php if($row["mat_item10_sel"] != ''): ?>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="29%" class="lbltwo" height="21">Item #10</td>
                <td width="71%">&nbsp;</td>
              </tr>
              <tr>
                <td class="lblone" height="21">Selecci&oacute;n de Item</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item10_sel"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Descripci&oacute;n de Item</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item10_des"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Definir Unidad</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item10_uni"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Cantidad Requerida</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item10_cant"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Observaciones</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item1_obser10"])?></td>
              </tr>
            </table>
          </td>
        </tr>
        <?php endif; ?>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <!--<tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td colspan="2" class="lbltitle">Herramientas de Trabajo</td>
              </tr>

              <?php if($row["herramientas"] != ''): ?>
              <tr>
                <td width="29%" class="lblone" height="21" style="vertical-align: top;">Herramientas necesarias para el trabajo:</td>
                <td width="71%">
                  <?php foreach(explode("|", substr($row["herramientas"], 0, -1)) as $herr): ?>
                    <?= utf8_encode($herr) ?><br>
                  <?php endforeach; ?>
                </td>
              </tr>
              <?php endif; ?>
            </table>
          </td>
        </tr>-->
      </table>
    </td>
    </tr>
    </table>
  </td>
  </tr>
</table>
  <?php } ?>

  <?php if($row["tipo_reporte"] == "REVISION REFRIGERACION"){ ?>
    <table width="840" border="0" align="center" cellpadding="0" cellspacing="0" class="tblwithe">
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td class="separa">&nbsp;</td>
      </tr>
      <tr>
        <td>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td colspan="2" class="lbltitle">INFORMACION DEL EQUIPO #<?= $conEqui;?></td>
            </tr>
            <tr>
              <td width="29%" class="lblone" height="21">Tipo de Equipo</td>
              <td width="71%" class="lblthree"><?=utf8_encode($row["tipo_equipo"])?></td>
            </tr>

            <?php if(trim($row["infequi_area_clima"]) != ''): ?>
            <tr>
              <td class="lblone" height="21">Area Que Climatiza</td>
              <td class="lblthree"><?=utf8_encode($row["area_clima"])?></td>
            </tr>
            <?php endif; ?>
        
            <?php if($row["infequi_area_nom"] != ''): ?>
            <tr>
              <td class="lblone" height="21">Nombre del Area</td>
              <td class="lblthree"><?=utf8_encode($row["nombre_area"])?></td>
            </tr>
            <?php endif; ?>
            
            <?php if($row["infequi_observa"] != ''): ?>
            <tr>
              <td class="lblone" height="21">Observaciones</td>
              <td class="lblthree"><?=utf8_encode($row["obaserva2"])?></td>
            </tr>
            <?php endif; ?>
          </table>
        </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td colspan="2" class="lbltitle">CARACTERISTICAS EQUIPO #<?= $conEqui ?> <?= utf8_encode($row["tipo_equipo"]) ?></td>
        </tr>
      
        <?php if($row["codigo_equipo"] != ''): ?>
        <tr>
          <td width="29%" class="lblone" height="21">Código del Equipo:</td>
          <td width="71%" class="lblthree"><?=utf8_encode($row["codigo_equipo"])?></td>
        </tr>
        <?php endif; ?>
      
        <?php if($row["desc_equipo"] != ''): ?>
        <tr>
          <td class="lblone" height="21">Descripción del Equipo: </td>
          <td class="lblthree"><?=utf8_encode($row["desc_equipo"])?></td>
        </tr>
        <?php endif; ?>

        <tr>
          <td class="lblone" height="21">Condensador</td>
        </tr>

        <?php if($row["marca"] != ''): ?>
        <tr>
          <td class="lblone" height="21">Marca:</td>
          <td class="lblthree"><?=utf8_encode($row["marca"])?></td>
        </tr>
        <?php endif; ?>
        
        <?php if($row["capacidad_compresor"] != ''): ?>
        <tr>
          <td class="lblone" height="21">Capacidad Compresor (HP/BTU):</td>
          <td class="lblthree"><?=utf8_encode($row["capacidad_compresor"])?></td>
        </tr>
        <?php endif; ?>

        <?php if($row["cantidad_compresores"] != ''): ?>
        <tr>
          <td class="lblone" height="21">Cantidad Compresores:</td>
          <td class="lblthree"><?=utf8_encode($row["cantidad_compresores"])?></td>
        </tr>
        <?php endif; ?>

        <?php if($row["capacidad_motor_ventilador"] != ''): ?>
        <tr>
          <td class="lblone" height="21">Capacidad Motor Ventilador (HP):</td>
          <td class="lblthree"><?=utf8_encode($row["capacidad_motor_ventilador"])?></td>
        </tr>
        <?php endif; ?>

        <?php if($row["cantidad_motor_ventilador"] != ''): ?>
        <tr>
          <td class="lblone" height="21">Cantidad Motor Ventilador:</td>
          <td class="lblthree"><?=utf8_encode($row["cantidad_motor_ventilador"])?></td>
        </tr>
        <?php endif; ?>

        <?php if($row["observa2"] != ''): ?>
        <tr>
          <td class="lblone" height="21">Observaciones:</td>
          <td class="lblthree"><?=utf8_encode($row["observa2"])?></td>
        </tr>
        <?php endif; ?>
      </table>
    </td>
  </tr>
  <?php  
  $diagnostico = json_decode($row["diagnostico"], true);
  $evaporador1 = json_decode($row["evaporador1"], true);
  $evaporador2 = json_decode($row["evaporador2"], true);
  $evaporador3 = json_decode($row["evaporador3"], true);
  $evaporador4 = json_decode($row["evaporador4"], true);
  $evaporador5 = json_decode($row["evaporador5"], true);
  $evaporador6 = json_decode($row["evaporador6"], true);
  $evaporadores = array($evaporador1, $evaporador2, $evaporador3, $evaporador4, $evaporador5, $evaporador6);
  foreach($evaporadores as $evaporador):
    if($evaporador["marca"] != ""):
  ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td colspan="2" class="lbltitle"><?= $evaporador["nombre"] ?></td>
        </tr>
      
        <?php if($evaporador["marca"] != ""): ?>
        <tr>
          <td width="29%" class="lblone" height="21">Marca:</td>
          <td width="71%" class="lblthree"><?=utf8_encode($evaporador["marca"])?></td>
        </tr>
        <?php endif; ?>
      
        <?php if($evaporador["modelo"] != ''): ?>
        <tr>
          <td class="lblone" height="21">Modelo:</td>
          <td class="lblthree"><?=utf8_encode($evaporador["modelo"])?></td>
        </tr>
        <?php endif; ?>

        <?php if($evaporador["serie"] != ''): ?>
        <tr>
          <td class="lblone" height="21">Serie:</td>
          <td class="lblthree"><?=utf8_encode($evaporador["serie"])?></td>
        </tr>
        <?php endif; ?>
        
        <?php if($evaporador["capacidadMotorVentilador"] != ''): ?>
        <tr>
          <td class="lblone" height="21">Capacidad Motor Ventilador (HP):</td>
          <td class="lblthree"><?=utf8_encode($evaporador["capacidadMotorVentilador"])?></td>
        </tr>
        <?php endif; ?>

        <?php if($evaporador["cantidadMotorVentilador"] != ''): ?>
        <tr>
          <td class="lblone" height="21">Cantidad Motor Ventilador:</td>
          <td class="lblthree"><?=utf8_encode($evaporador["cantidadMotorVentilador"])?></td>
        </tr>
        <?php endif; ?>

        <?php if($evaporador["observaciones"] != ''): ?>
        <tr>
          <td class="lblone" height="21">Observaciones:</td>
          <td class="lblthree"><?=utf8_encode($evaporador["observaciones"])?></td>
        </tr>
        <?php endif; ?>

      </table>
    </td>
  </tr>
  <?php 
    endif;
  endforeach;
  ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="840" border="0" align="center" cellpadding="0" cellspacing="0" class="tblwithe">
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td colspan="2" class="lbltitle">Diagnostico (<?= utf8_encode($row["tipo_equipo"]) ?>)</td>
            </tr>
            <tr>
              <td colspan="2" class="lblone">Evaporador</td>
            </tr>
            <tr>
              <td width="29%" height="21" class="lblone" style="vertical-align: top;"><span>Partes no Operativas (Reparación):</span></td>
              <td width="71%" class="lblthree">
              <?php 
                $partes = explode("|", $diagnostico["partesNoOperativasReparacionEvaporador"]); 
                foreach($partes as $part): ?>
                  <?= limpiar($part) ?><br>
              <?php endforeach; ?>
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td class="lblone" height="21" style="vertical-align: top;">Partes no Operativas (Cambio): </td>
              <td width="71%" class="lblthree">
              <?php 
                $partes = explode("|", $diagnostico["partesNoOperativasCambioEvaporador"]); 
                foreach($partes as $part): ?>
                  <?= limpiar($part) ?><br>
              <?php endforeach; ?>
              </td>
            </tr>
            <tr>
              <td colspan="2" class="lblone">Condensador</td>
            </tr>
            <tr>
              <td width="29%" height="21" class="lblone" style="vertical-align: top;"><span>Partes no Operativas (Reparación):</span></td>
              <td width="71%" class="lblthree">
              <?php 
                $partes = explode("|", $diagnostico["partesNoOperativasReparacionCondensador"]); 
                foreach($partes as $part): 
                  #$part = str_replace("u00C1", "Á", $part);
                  #$part = str_replace("u00E1", "a", $part);
                  #$part = str_replace("u00c9", "É", $part);
                  #$part = str_replace("u00E9", "é", $part);
                  #$part = str_replace("u00CD", "Í", $part);
                  #$part = str_replace("u00cd", "Í", $part);
                  #$part = str_replace("u00ED", "í", $part);
                  #$part = str_replace("u00D3", "Ó", $part);
                  #$part = str_replace("u00F3", "ó", $part);
                  #$part = str_replace("u00DA", "Ú", $part);
                  #$part = str_replace("u00FA", "ú", $part);
                  #$part = str_replace("u00F1", "ñ", $part);
                  ?>

                  <?= limpiar($part) ?><br>
                <?php endforeach; ?>
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td class="lblone" height="21" style="vertical-align: top;">Partes no Operativas (Cambio): </td>
              <td width="71%" class="lblthree">
              <?php 
                $partes = explode("|", $diagnostico["partesNoOperativasCambioCondensador"]); 
                foreach($partes as $part): ?>
                  <?= limpiar($part) ?><br>
              <?php endforeach; ?>
              </td>
            </tr>
            <tr>
              <td colspan="2" class="lblone">Estructura Cámara de Frío</td>
            </tr>
            <tr>
              <td width="29%" height="21" class="lblone" style="vertical-align: top;"><span>Partes no Operativas (Reparación):</span></td>
              <td width="71%" class="lblthree">
              <?php 
                $partes = explode("|", $diagnostico["partesNoOperativasReparacionCamaraFrio"]); 
                foreach($partes as $part): ?>
                  <?= limpiar($part) ?><br>
              <?php endforeach; ?>
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td class="lblone" height="21" style="vertical-align: top;">Partes no Operativas (Cambio): </td>
              <td width="71%" class="lblthree">
              <?php 
                $partes = explode("|", $diagnostico["partesNoOperativasCambioCamaraFrio"]); 
                foreach($partes as $part): ?>
                  <?= limpiar($part) ?><br>
              <?php endforeach; ?>
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td class="lblone" height="21">Comentarios:</td>
              <td class="lblthree"><a href="reportes_vistos/<?= $rowEnc["tipoDato"] ?>/<?= $diagnostico["comentarios"] ?>">Audio de Comentarios</a></td>
            </tr>
            <tr>
              <td class="lblone" height="21">Captura de Fotos</td>
              <td colspan="2" class="lblone">
                <?php if($diagnostico["capturaFotos"] != ''): 
                  $fotos = explode("|", substr($diagnostico["capturaFotos"], 0, -1));
                  for($i=0; $i < count($fotos); $i++): ?>
                      <a href="reportes_vistos/revision_correctivo/<?= $fotos[$i] ?>"><img src="reportes_vistos/revision_correctivo/<?= $fotos[$i] ?>" width="143" height="147" /></a>
                  <?php endfor; 
                endif; ?>
              </td>
            </tr>
            <tr>
              <td class="lblone" height="21">Tiempo de Trabajo:</td>
              <td class="lblthree"><?=utf8_encode($diagnostico["tiempoTrabajo"])?></td>
            </tr>
            <tr>
              <td class="lblone" height="21">Observaciones:</td>
              <td class="lblthree"><?=utf8_encode($diagnostico["observaciones"])?></td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td colspan="2" class="lbltitle">Materiales Requeridos</td>
            </tr>

            <?php if($row["mat_item1_sel"] != ''): ?>
            <tr>
              <td width="29%" class="lbltwo" height="21">Item #1</td>
              <td width="71%">&nbsp;</td>
            </tr>
            <tr>
              <td class="lblone" height="21">Selecci&oacute;n de Item</td>
              <td class="lblthree"><?=utf8_encode($row["mat_item1_sel"])?></td>
            </tr>
            <tr>
              <td class="lblone" height="21">Descripci&oacute;n de Item</td>
              <td class="lblthree"><?=utf8_encode($row["mat_item1_des"])?></td>
            </tr>
            <tr>
              <td class="lblone" height="21">Definir Unidad</td>
              <td class="lblthree"><?=utf8_encode($row["mat_item1_uni"])?></td>
            </tr>
            <tr>
              <td class="lblone" height="21">Cantidad Requerida</td>
              <td class="lblthree"><?=utf8_encode($row["mat_item1_cant"])?></td>
            </tr>
            <tr>
              <td class="lblone" height="21">Observaciones</td>
              <td class="lblthree"><?=utf8_encode($row["mat_item1_obser"])?></td>

            </tr>
          </table>
        </td>
        </tr>
        <?php endif; ?>

        <?php if($row["mat_item2_sel"] != ''): ?>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="29%" class="lbltwo" height="21">Item #2</td>
                <td width="71%">&nbsp;</td>
              </tr>
              <tr>
                <td class="lblone" height="21">Selecci&oacute;n de Item</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item2_sel"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Descripci&oacute;n de Item</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item2_des"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Definir Unidad</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item2_uni"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Cantidad Requerida</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item2_cant"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Observaciones</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item2_obser"])?></td>
              </tr>
            </table>
          </td>
        </tr>
        <?php endif; ?>
        
        <?php if($row["mat_item3_sel"] != ''): ?>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="29%" class="lbltwo" height="21">Item #3</td>
                <td width="71%">&nbsp;</td>
              </tr>
              <tr>
                <td class="lblone" height="21">Selecci&oacute;n de Item</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item3_sel"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Descripci&oacute;n de Item</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item3_des"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Definir Unidad</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item3_uni"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Cantidad Requerida</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item3_cant"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Observaciones</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item3_obser"])?></td>
              </tr>
            </table>
          </td>
        </tr>
        <?php endif; ?>
        
        <?php if($row["mat_item4_sel"] != ''): ?>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="29%" class="lbltwo" height="21">Item #4</td>
                <td width="71%">&nbsp;</td>
              </tr>
              <tr>
                <td class="lblone" height="21">Selecci&oacute;n de Item</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item4_sel"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Descripci&oacute;n de Item</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item4_des"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Definir Unidad</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item4_uni"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Cantidad Requerida</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item4_cant"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Observaciones</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item4_obser"])?></td>
              </tr>
            </table>
          </td>
        </tr>
        <?php endif; ?>
        
        <?php if($row["mat_item5_sel"] != ''): ?>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="29%" class="lbltwo" height="21">Item #5</td>
                <td width="71%">&nbsp;</td>
              </tr>
              <tr>
                <td class="lblone" height="21">Selecci&oacute;n de Item</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item5_sel"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Descripci&oacute;n de Item</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item5_des"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Definir Unidad</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item5_uni"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Cantidad Requerida</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item5_cant"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Observaciones</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item5_obser"])?></td>
              </tr>
            </table>
          </td>
        </tr>
        <?php endif; ?>
        
        <?php if($row["mat_item6_sel"] != ''): ?>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="29%" class="lbltwo" height="21">Item #6</td>
                <td width="71%">&nbsp;</td>
              </tr>
              <tr>
                <td class="lblone" height="21">Selecci&oacute;n de Item</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item6_sel"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Descripci&oacute;n de Item</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item6_des"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Definir Unidad</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item6_uni"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Cantidad Requerida</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item6_cant"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Observaciones</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item6_obser"])?></td>
              </tr>
            </table>
          </td>
        </tr>
        <?php endif; ?>
        
        <?php if($row["mat_item7_sel"] != ''): ?>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="29%" class="lbltwo" height="21">Item #7</td>
                <td width="71%">&nbsp;</td>
              </tr>
              <tr>
                <td class="lblone" height="21">Selecci&oacute;n de Item</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item7_sel"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Descripci&oacute;n de Item</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item7_des"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Definir Unidad</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item7_uni"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Cantidad Requerida</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item7_cant"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Observaciones</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item7_obser"])?></td>
              </tr>
            </table>
          </td>
        </tr>
        <?php endif; ?>
        
        <?php if($row["mat_item8_sel"] != ''): ?>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="29%" class="lbltwo" height="21">Item #8</td>
                <td width="71%">&nbsp;</td>
              </tr>
              <tr>
                <td class="lblone" height="21">Selecci&oacute;n de Item</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item8_sel"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Descripci&oacute;n de Item</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item8_des"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Definir Unidad</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item8_uni"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Cantidad Requerida</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item8_cant"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Observaciones</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item8_obser"])?></td>
              </tr>
            </table>
          </td>
        </tr>
        <?php endif; ?>
        
        <?php if($row["mat_item9_sel"] != ''): ?>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="29%" class="lbltwo" height="21">Item #9</td>
                <td width="71%">&nbsp;</td>
              </tr>
              <tr>
                <td class="lblone" height="21">Selecci&oacute;n de Item</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item9_sel"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Descripci&oacute;n de Item</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item9_des"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Definir Unidad</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item9_uni"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Cantidad Requerida</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item9_cant"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Observaciones</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item9_obser"])?></td>
              </tr>
            </table>
          </td>
        </tr>
        <?php endif; ?>
        
        <?php if($row["mat_item10_sel"] != ''): ?>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="29%" class="lbltwo" height="21">Item #10</td>
                <td width="71%">&nbsp;</td>
              </tr>
              <tr>
                <td class="lblone" height="21">Selecci&oacute;n de Item</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item10_sel"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Descripci&oacute;n de Item</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item10_des"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Definir Unidad</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item10_uni"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Cantidad Requerida</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item10_cant"])?></td>
              </tr>
              <tr>
                <td class="lblone" height="21">Observaciones</td>
                <td class="lblthree"><?=utf8_encode($row["mat_item10_obser"])?></td>
              </tr>
            </table>
          </td>
        </tr>
        <?php endif; ?>
        <!--<tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td colspan="2" class="lbltitle">Herramientas de Trabajo</td>
              </tr>

              <?php if($row["herramientas"] != ''): ?>
              <tr>
                <td width="29%" class="lblone" style="vertical-align: top;" height="21">Herramientas necesarias para el trabajo:</td>
                <td width="71%">
                  <?php foreach(explode("|", substr($row["herramientas"], 0, -1)) as $herr): ?>
                    <?= utf8_encode($herr) ?><br>
                  <?php endforeach; ?>
                </td>
              </tr>
              <?php endif; ?>
            </table>
          </td>
        </tr>-->
      </table>
    </td>
    </tr>
    </table>
  </td>
  </tr>
</table>
  <?php } ?>

  <?php }?>


<!-- END REVISION MANTENIMIENTO -->

<?php
$new_table="";
switch($tipo){
	case "revision_mantenimiento":
		$sql = "SELECT herra_necesita as herramientas, herra_otras as otros,
				espec_dif_acceso, espec_dif_accesocom, espec_acceso_agua, espec_fotos, espec_observa
				FROM reportes_revision_mantenimiento WHERE id_orden IN($ids_orden)";
        $new_table="reportes_revision_mantenimiento";
		break;
	case "revision_instalacion":
		$sql = "SELECT herramientas, '' as otros,
				tipo_equipo, desc_equipo, capacidad_btu, tiempo_aprox_instalacion, orde_observas
				FROM reportes_revision_instalacion WHERE id_orden IN($ids_orden)";
        $new_table="reportes_revision_instalacion";
		break;
	case "revision_correctivo":
		$sql = "SELECT herramientas, '' as otros 
				FROM reportes_revision_correctivo WHERE id_orden IN($ids_orden)";
        $new_table="reportes_revision_correctivo";
		break;
	case "mantenimiento":
		$sql = "SELECT herr_detalle as herramientas, herr_otros as otros 
				FROM reportes_r_mantenimiento WHERE id_orden IN($ids_orden)";
        $new_table="reportes_r_mantenimiento";
		break;
}

$res = $link->query($sql);
while($fila = $res->fetch_assoc()){
	if(utf8_encode($fila["herramientas"]) != "" || utf8_encode($fila["otros"] != '')){
		$row2 = $fila;
		break;
	}
} ?>

<?
if($row2["equipo_remendado"] !=  ''): ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="2" class="lbltitle">ESPECIFICACIONES DEL TRABAJO (<?= utf8_encode($row2["tipo_equipo"]) ?>)</td>
	</tr>
	<tr>
		<td width="29%" height="21" class="lblone">Equipo Recomendado</td>
		<td width="71%" class="lblthree editor" contenteditable="true" data-table="<?= $new_table ?>" data-id="<?= $row["id"] ?>" data-column="equipo_remendado" ><?=utf8_encode($row["equipo_remendado"])?></td>
	</tr>
	<tr>
		<td class="lblone" height="21">Descripción del Equipo:</td>
		<td class="lblthree editor" contenteditable="true" data-column="desc_equipo" data-table="<?= $new_table ?>" data-id="<?= $row["id"] ?>"><?=utf8_encode($row["desc_equipo"])?></td>
	</tr>
	<tr>
		<td class="lblone" height="21">Capacidad (BTU):</td>
		<td class="lblthree editor" contenteditable="true" data-column="capacidad_btu" data-table="<?= $new_table ?>" data-id="<?= $row["id"] ?>"><?=utf8_encode($row["capacidad_btu"])?></td>
	</tr>
	<tr>
		<td class="lblone" height="21">Tiempo Aproximado para Instalación:</td>
		<td class="lblthree editor" contenteditable="true" data-column="tiempo_aprox_instalacion" data-table="<?= $new_table ?>" data-id="<?= $row["id"] ?>"><?=utf8_encode($row["tiempo_aprox_instalacion"])?></td>
	</tr>
	<tr>
		<td class="lblone" height="21">Observaciones</td>
		<td class="lblthree editor" contenteditable="true" data-column="orde_observas" data-table="<?= $new_table ?>" data-id="<?= $row["id"] ?>"><?=utf8_encode($row["orde_observas"])?></td>
	</tr>
</table>
<? endif; ?>

<? 
if($row2["espec_dif_acceso"] != ''): ?>

<table width="840" border="0" align="center" cellpadding="0" cellspacing="0" class="tblwithe">
	<tr>
		<td colspan="2" class="lbltitle">ESPECIFICACIONES DEL TRABAJO</td>
	</tr>
	<? if($row2["espec_dif_acceso"] != ''): ?>
	<tr>
		<td width="29%" height="21" class="lblone">Dificultad de Acceso a Equipos</td>
		<td width="71%" class="lblthree editor" contenteditable="true" data-column="espec_dif_acceso" data-table="<?= $new_table ?>" data-id="<?= $row["id"] ?>"><?=utf8_encode($row2["espec_dif_acceso"])?></td>
	</tr>
	<? endif; ?>
	<? if($row2["espec_dif_accesocom"] != ''): ?>
	<tr>
		<td class="lblone" height="21">Dificultad de Acceso a Equipos <br />
		Comentarios</td>
		<td class="lblthree editor" contenteditable="true" data-column="espec_dif_accesocom" data-table="<?= $new_table ?>" data-id="<?= $row["id"] ?>"><?=utf8_encode($row2["espec_dif_accesocom"])?></td>
	</tr>
	<? endif; ?>
	<? if($row2["espec_acceso_agua"] != ''): ?>
	<tr>
		<td class="lblone" height="21">Acceso a Puntos de Agua y Corriente</td>
		<td class="lblthree editor" contenteditable="true" data-column="espec_acceso_agua" data-table="<?= $new_table ?>" data-id="<?= $row["id"] ?>"><?=utf8_encode($row2["espec_acceso_agua"])?></td>
	</tr>
	<? endif; ?>
	<? if($row2["espec_acceso_aguacom"] != ''): ?>
	<tr>
		<td class="lblone" height="21">Acceso a Puntos de Agua y Corriente <br />
		Comentarios</td>
		<td class="lblthree editor" contenteditable="true" data-column="espec_acceso_aguacom" data-table="<?= $new_table ?>" data-id="<?= $row["id"] ?>"><?=utf8_encode($row2["espec_acceso_aguacom"])?></td>
	</tr>
	<? endif; ?>
	<? if($row2["espec_fotos"] != ''): ?>
	<tr>
		<td class="lbltwo" height="21">Captura de Fotos</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2" class="lblone">
		<?php if($row2["espec_fotos"] != ''):
			$fotos = explode("|", substr($row2["espec_fotos"],0,-1));
			for($i=0;$i<count($fotos);$i++){ ?>
				<a href="reportes_vistos/revision_mantenimiento/<?= $fotos[$i] ?>"><img src="reportes_vistos/revision_mantenimiento/<?= $fotos[$i] ?>" width="143" height="147" /></a>&nbsp;
			<? }
			endif;
		?>
		</td>
	</tr>
	<? endif; ?>
	<? if($row2["espec_observa"] != ''): ?>
	<tr>
		<td class="lblone" height="21">Observaciones</td>
		<td class="lblthree editor" contenteditable="true" data-column="espec_observa" data-table="<?= $new_table ?>" data-id="<?= $row["id"] ?>"><?=utf8_encode($row2["espec_observa"])?></td>
	</tr>
	<? endif; ?>
</table>
<? endif; ?>

<?php if(utf8_encode($row2["herramientas"]) != '' || utf8_encode($row2["otros"] != '')): ?>
<table width="840" border="0" align="center" cellpadding="0" cellspacing="0" class="tblwithe">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td colspan="2" class="lbltitle">HERRAMIENTAS DE TRABAJO</td>
        </tr>
        <?php if($row2["herramientas"] != ''): ?>
        <tr>
          <td width="29%" valign="top" class="lblone" height="21">Herramientas Necesarias</td>
          <td width="71%" class="lblthree editor" contenteditable="true" data-table="<?= $new_table ?>" data-id="<?= $row["id"] ?>" data-column="herramientas"><?= utf8_encode(str_replace("|", "<br>", $row2["herramientas"])) ?></td>
        </tr>
        <?php endif; ?>
        <?php if($row2["otros"] != ''): ?>
        <tr>
          <td class="lblone" height="21">Si eligi&oacute; la opci&oacute;n OTROS por favor <br />
          especificar</td>
          <td class="lblthree editor" contenteditable="true" data-table="<?= $new_table ?>" data-id="<?= $row["id"] ?>" data-column="otros"><?=utf8_encode($row2["otros"])?></td>
        </tr>
        <?php endif; ?>
      </table>
    </td>
  </tr>
</table>
<?php endif; ?>


<!-- FOOT OF PAGE -->

<?php 
  $res = $link->query($sql_firma);
  $rowEnc = $res->fetch_assoc();
?>

<?php if($rowEnc["img_firma"] != ""){ ?>

<table width="840" border="0" align="center" cellpadding="0" cellspacing="0" class="tblwithe">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <?php
	  if($rowEnc["responsable"] != '' || $rowEnc["cargo_responsable"] != '' || $rowEnc["img_firma"] != '' || $rowEnc["observa_fin"] != '' || $rowEnc["diag_audio"] != ''){ ?>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">OBSERVACIONES GENERALES Y FIRMAS</td>
      </tr>
      <?php
	  if($rowEnc["responsable"] != ''){ ?>
      <tr>
        <td width="37%" class="lblone" height="21">Nombre del Responsable que Supervisa el Trabajo</td>
        <td width="63%" class="lblthree"><?=utf8_encode($rowEnc["responsable"])?></td>
      </tr>
      <?php }?>
      <?php
	  if($rowEnc["cargo_responsable"] != ''){ ?>
      <tr>
        <td class="lblone" width="37%" height="21">Cargo</td>
        <td class="lblthree" width="63%"><?=utf8_encode($rowEnc["cargo_responsable"])?></td>
      </tr>
      <?php }?>
      <?php
	  if($rowEnc["img_firma"] != ''){ ?>
      <tr>
        <td class="lbltwo" height="21">Firma del Cliente o Responsable</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" class="lblone"><a href="reportes_vistos/<?= $rowEnc["tipoDato"] ?>/<?= $rowEnc["img_firma"] ?>"><img src="reportes_vistos/<?= $rowEnc["tipoDato"] ?>/<?= $rowEnc["img_firma"] ?>" width="299" height="85" /></a></td>
      </tr>
      <?php }?>
    <?php
    if($rowEnc["audio_comentario_gral"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Comentarios Generales</td>
        <td class="lblthree" width="63%"><a href="reportes_vistos/<?= $rowEnc["tipoDato"];?>/<?= $rowEnc["audio_comentario_gral"];?>">Audio de Comentarios</a></td>
      </tr>
      <?php }?>
      <?php
	  if($rowEnc["observa_fin"] != ''): ?>
      <tr>
        <td class="lblone" height="21">Observaciones Generales</td>
        <td class="lblthree" width="63%"><?=utf8_encode($rowEnc["observa_fin"])?></td>
      </tr>
      <?php endif;?>
    </table></td>
  </tr>
  <?php }?>
    <tr>
    <td>&nbsp;</td>
  </tr>
</table>

<?php }?>

<!-- FOOT OF PAGE END -->

</div>
</div>
</body>
</html>