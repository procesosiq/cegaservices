<link href="../assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
<style>
    @page {
      size: A4;
    }
    .textoFire{
    color: rgb(34, 34, 34); 
    font-family: arial, sans-serif; 
    font-size: 10px; 
    line-height: normal;
    }
    .borderBox {
        border: solid 1px;
        padding: 5px;
    }
    #ajax-modal {
        top:5%;
        right:50%;
        outline: none;
    }
    .button-estilo{
        background-color : #26C281;
		color:white;
    }
    .button-estilo-quitar {
        background-color : red;
		color:white;
    }
    .margin-total{
        margin-right : 10px;
        /*font-weight: bold;*/
    }
    .margin-total-gen{
        /*margin-top : 10px;*/
        margin-right : 40px;
        font-size : 16px;   
        font-weight: bold;  
    }
</style>

<div id="cont" class="page-content-wrapper" ng-app="app" ng-controller="cotizadorController" ng-cloak>
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1>COTIZADOR</h1>
            </div>
        </div>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="/cotizacionPruebas?id={{data.id_cotizacion}}">Volver a cotizacion</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Registro de cotizaciones</span>
            </li>
        </ul>
        <div>
            <?php include("./views/inicio_tags.php"); ?>
        </div>       
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet grey-cascade box">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>Lista de Productos y Servicios </div>
                            <div class="actions">
                                <div class="row">
                                    <div class="col-md-7">
                                        <input id="name_area" id="name_area" type="text" class="form-control" ng-model="data.nombre_area">
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn blue" ng-click="anadirArea()">Añadir area</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div ng-repeat="row in areas" class="panel-group accordion" id="accordion_{{row.id}}">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a ng-click="changeArea(row.id)" class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion{{ row.id }}" href="#collapse_{{ row.id }}_1" aria-expanded="false"> 
                                                <!-- {{ row.area }} <span class = "pull-right margin-total"> TOTAL : $ {{ t = (total_equipo_r_row.id + total_materiales_row.id + total_repuestos_row.id + total_mano_row.id) != '' ? t : '0.00'  | number : 2}} <span> -->
                                                {{ row.area }} <span class = "pull-right margin-total"> TOTAL : $ {{ row.total  | number : 2}} <span>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse_{{ row.id }}_1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">
                                            <div class="panel-group accordion" id="accordion_{ row.id }}">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a ng-click="request.carga_pestana_tres(row.id)" class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion{{ row.id }}" href="#collapse_{{ row.id }}_4" aria-expanded="false"> 
                                                                Equipos <span class = "pull-right margin-total"> $ {{ total_equipo_r == '' ? 0.00 : total_equipo_r | number : 2}} <span>
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapse_{{ row.id }}_4" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="table-responsive table-scrollable">
                                                                        <table class="table table-hover table-bordered table-striped">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th width = "25%"> Descripcion </th>
                                                                                    <th width = "15%"> Capacidad </th>
                                                                                    <th width = "15%"> Unidad </th>
                                                                                    <th width = "10%"> Cantidad </th>
                                                                                    <th width = "10%"> P. UNIT. </th>
                                                                                    <th width = "15%"> Total </th>
                                                                                    <th width = "10%"> Acciones </th>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <select width = "25%" ng-change="equiposR.cargaEquiposR()" name="tipo_equipo_r" id="tipo_equipo_r" ng-model="filtersEquipoR.tipo_equipo_r_id">
                                                                                            <option ng-repeat="(key, value) in tipo_equipo_r" ng-selected = "key == filtersEquipoR.tipo_equipo_r_id" value="{{key}}">{{value}}</option>
                                                                                        </select>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input width = "15%" type = "text" id = "txt_capacidad_equipo_r" name = "txt_capacidad_equipo_r" ng-model = "filtersEquipoR.capacidad_equipo_r">   
                                                                                    </td>
                                                                                    <td>
                                                                                        <input width = "15%" type = "text" id = "txt_unidad_equipo_r" name = "txt_unidad_equipo_r" ng-model = "filtersEquipoR.unidad_equipo_r" ng-readonly = "true">
                                                                                    </td>
                                                                                    <td>
                                                                                        <input width = "10%" type = "text" id = "txt_cantidad_equipo_r" name = "txt_cantidad_equipo_r" ng-model = "filtersEquipoR.cantidad_equipo_r">
                                                                                    </td>
                                                                                    <td>
                                                                                        <input width = "10%" type = "text" id = "txt_precio_equipo_r" name = "txt_precio_equipo_r" ng-model = "filtersEquipoR.precio_equipo_r">
                                                                                    </td>
                                                                                    <td>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input ng-click = "equiposR.insertEquiposR()" width = "10%" type = "button" id = "btn_agregar" value = "     +     " class = "button-estilo">
                                                                                    </td>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <tr ng-repeat = "row_equipos in detalle_equipo_r">
                                                                                    <td width = "25%">{{ row_equipos.item }}</td>
                                                                                    <td width = "15%">{{ row_equipos.capacidad }}</td>
                                                                                    <td width = "15%">{{ row_equipos.unidad }}</td>
                                                                                    <td width = "10%">{{ row_equipos.cantidad }}</td>
                                                                                    <td width = "10%">{{ row_equipos.precio_unit }}</td>
                                                                                    <td width = "15%">{{ row_equipos.cantidad * row_equipos.precio_unit | number : 2 }}</td>
                                                                                    <td width = "10%">
                                                                                        <input ng-click = "equiposR.deleteEquiposR(row_equipos.id,row_equipos.item)" width = "15%" type = "button" id = "btn_quitar" value = "      -     " class = "button-estilo-quitar">
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a ng-click="request.carga_pestana_dos(row.id)" class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion{{ row.id }}" href="#collapse_{{ row.id }}_3" aria-expanded="false"> 
                                                            Materiales <span class = "pull-right margin-total"> $ {{ total_materiales == '' ? 0.00 : total_materiales | number : 2}} </span></a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapse_{{ row.id }}_3" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="table-responsive table-scrollable">
                                                                        <table class="table table-hover table-bordered table-striped">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th width = "20%"> Ítem </th>
                                                                                    <th width = "20%"> Descripcion </th>
                                                                                    <th width = "20%"> Unidad </th>
                                                                                    <th width = "10%"> Cantidad </th>
                                                                                    <th width = "10%"> P. UNIT. </th>
                                                                                    <th width = "10%"> Total </th>
                                                                                    <th width = "10%"> Acciones </th>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <select width = "20%" ng-change="materiales.cargaTipoTrabajo()" name="tipo_trabajo" id="tipo_trabajo" ng-model="filtersMateriales.tipo_trabajo_id">
                                                                                            <option ng-repeat="(key, value) in tipo_trabajo" ng-selected = "key == filtersMateriales.tipo_trabajo_id" value="{{key}}">{{value}}</option>
                                                                                        </select>
                                                                                    </td>
                                                                                    <td>
                                                                                        <select width = "20%" ng-change="materiales.cargaCapacidad()" name="capacidad" id="capacidad" ng-model = "filtersMateriales.capacidad">
                                                                                            <option ng-repeat="(key, value) in capacidad" ng-selected = "key == filtersMateriales.capacidad" value="{{value}}">{{value}}</option>
                                                                                        </select>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input width = "10%" type = "text" id = "txt_unidad" name = "txt_unidad" ng-model = "filtersMateriales.unidad" ng-readonly = "true">
                                                                                    </td>
                                                                                    <td>
                                                                                        <input width = "10%" type = "text" id = "txt_cantidad" name = "txt_cantidad" ng-model = "filtersMateriales.cantidad">
                                                                                    </td>
                                                                                    <td>
                                                                                        <input width = "10%" type = "text" id = "txt_precio" name = "txt_precio" ng-model = "filtersMateriales.precio">
                                                                                    </td>
                                                                                    <td>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input ng-click="materiales.insertMateriales()" width = "10%" type = "button" id = "btn_agregar" value = "     +     " class = "button-estilo">
                                                                                    </td>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <tr ng-repeat = "row_mat in detalle_materiales">
                                                                                    <td width = "20%">{{ row_mat.item }}</td>
                                                                                    <td width = "20%">{{ row_mat.capacidad }}</td>
                                                                                    <td width = "20%">{{ row_mat.unidad }}</td>
                                                                                    <td width = "10%">{{ row_mat.cantidad }}</td>
                                                                                    <td width = "10%">{{ row_mat.precio_unit }}</td>
                                                                                    <td width = "10%">{{ row_mat.cantidad * row_mat.precio_unit | number : 2 }}</td>
                                                                                    <td width = "10%">
                                                                                        <input ng-click = "materiales.deleteMateriales(row_mat.id,row_mat.item)" width = "15%" type = "button" id = "btn_quitar" value = "      -     " class = "button-estilo-quitar">
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a ng-click="request.carga_pestana_uno(row.id)" class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion{{ row.id }}" href="#collapse_{{ row.id }}_2" aria-expanded="false"> 
                                                            Respuestos  <span class = "pull-right margin-total"> $ {{ total_repuestos == '' ? 0.00 : total_repuestos | number : 2 }} </span></a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapse_{{ row.id }}_2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="table-responsive table-scrollable">
                                                                        <table class="table table-hover table-bordered table-striped">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th width = "20%"> Tipo equipo </th>
                                                                                    <th width = "10%"> Descripcion </th>
                                                                                    <th width = "10%"> Parte </th>
                                                                                    <th width = "10%"> Pieza </th>
                                                                                    <th width = "10%"> Unidad </th>
                                                                                    <th width = "10%"> Cantidad </th>
                                                                                    <th width = "10%"> P. UNIT. </th>
                                                                                    <th width = "10%"> Total </th>
                                                                                    <th width = "10%"> Acciones </th>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <select width = "30%" ng-change="equipos.cargaTipoEquipos()" name="tipo_equipos" id="tipo_equipos" ng-model="filtersEquipos.tipo_equipo_id">
                                                                                            <option ng-repeat="(key, value) in tipo_equipos" ng-selected = "key == filtersEquipos.tipo_equipo_id" value="{{key}}">{{value}}</option>
                                                                                        </select>
                                                                                    </td>                                                                    
                                                                                    <td>
                                                                                        <select width = "10%" ng-change="equipos.cargaPorDescripcion()" name="descripcion_equipos" id="descripcion_equipos" ng-model = "filtersEquipos.descripcion_id">
                                                                                            <option ng-repeat="(key, value) in descripcion_equipos" ng-selected = "key == filtersEquipos.descripcion_id" value="{{key}}">{{value}}</option>
                                                                                        </select>
                                                                                    </td>
                                                                                    <td>
                                                                                        <select width = "10%" ng-change="equipos.cargaPorPartes()" name="parte" id="parte" ng-model="filtersEquipos.parte_id">
                                                                                            <option ng-repeat="(key, value) in parte_equipos" ng-selected = "key == filtersEquipos.parte_id" value="{{key}}">{{value}}</option>
                                                                                        </select>
                                                                                    </td>
                                                                                    <td>
                                                                                        <select width = "10%" ng-change="equipos.cargaPorPiezas()" name="pieza" id="pieza" ng-model="filtersEquipos.pieza_id">
                                                                                            <option ng-repeat="(key, value) in pieza_equipos" ng-selected = "key == filtersEquipos.pieza_id" value="{{key}}">{{value}}</option>
                                                                                        </select>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input width = "10%" type = "text" id = "txt_unidad_equipos" name = "txt_unidad_equipos" ng-model = "filtersEquipos.unidad" ng-readonly = "true">
                                                                                    </td>
                                                                                    <td>
                                                                                        <input width = "10%" type = "text" id = "txt_cantidad_equipos" name = "txt_cantidad_equipos" ng-model = "filtersEquipos.cantidad">
                                                                                    </td>
                                                                                    <td>
                                                                                        <input width = "10%" type = "text" id = "txt_precio_equipos" name = "txt_precio_equipos" ng-model = "filtersEquipos.precio">
                                                                                    </td>
                                                                                    <td>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input ng-click = "equipos.insertRepuestos()" width = "10%" type = "button" id = "btn_agregar_equipos" value = "     +     " class = "button-estilo">
                                                                                    </td>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <tr ng-repeat = "row_equi in detalle_repuestos">
                                                                                    <td width = "20%">{{ row_equi.item }}</td>
                                                                                    <td width = "20%">{{ row_equi.item_lista }}</td>
                                                                                    <td width = "20%">{{ row_equi.item_parte }}</td>
                                                                                    <td width = "20%">{{ row_equi.item_pieza }}</td>
                                                                                    <td width = "20%">{{ row_equi.unidad }}</td>
                                                                                    <td width = "10%">{{ row_equi.cantidad }}</td>
                                                                                    <td width = "10%">{{ row_equi.precio_unit }}</td>
                                                                                    <td width = "10%">{{ row_equi.cantidad * row_equi.precio_unit | number : 2 }}</td>
                                                                                    <td width = "10%">
                                                                                        <input ng-click = "equipos.deleteRepuestos(row_equi.id,row_equi.item)" width = "15%" type = "button" id = "btn_quitar" value = "      -     " class = "button-estilo-quitar">
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a ng-click="request.carga_pestana_cuatro(row.id)" class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion{{ row.id }}" href="#collapse_{{ row.id }}_5" aria-expanded="true">
                                                            Mano de obra <span class = "pull-right margin-total"> $ {{ total_mano == '' ? 0.00 : total_mano | number : 2 }}</span></a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapse_{{ row.id }}_5" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="table-responsive table-scrollable">
                                                                        <table class="table table-hover table-bordered table-striped">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th width = "20%"> Ítem </th>
                                                                                    <th width = "20%"> Descripcion </th>
                                                                                    <th width = "10%"> Unidad </th>
                                                                                    <th width = "10%"> Cantidad </th>
                                                                                    <th width = "10%"> P. UNIT. </th>
                                                                                    <th width = "10%"> Total </th>
                                                                                    <th width = "10%"> Acciones </th>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <select width = "20%" ng-change="mano.cargaAreaMano()" name="area_mano" id="area_mano" ng-model="filtersMano.area_mano_id">
                                                                                            <option ng-repeat="(key, value) in area_mano" ng-selected = "key == filtersMano.area_mano" value="{{key}}">{{value}}</option>
                                                                                        </select>
                                                                                    </td>
                                                                                    <td>
                                                                                        <select width = "20%" ng-change="mano.cargaDescripcionMano()" name="descripcion_mano" id="descripcion_mano" ng-model = "filtersMano.descripcion_id">
                                                                                            <option ng-repeat="(key, value) in descripcion_mano" ng-selected = "key == filtersMano.descripcion_id" value="{{key}}">{{value}}</option>
                                                                                        </select>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input width = "10%" type = "text" id = "txt_unidad_mano" name = "txt_unidad_mano" ng-model = "filtersMano.unidad">
                                                                                    </td>
                                                                                    <td>
                                                                                    <input width = "10%" type = "text" id = "txt_cantidad_mano" name = "txt_cantidad_mano" ng-model = "filtersMano.cantidad">
                                                                                    </td>
                                                                                    <td>
                                                                                    <input width = "10%" type = "text" id = "txt_precio_mano" name = "txt_precio_mano" ng-model = "filtersMano.precio">
                                                                                    </td>
                                                                                    <td>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input ng-click = "mano.insertMano()" width = "10%" type = "button" id = "btn_agregar_mano" value = "     +     " class = "button-estilo">
                                                                                    </td>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <tr ng-repeat = "row_mano in detalle_mano">
                                                                                    <td width = "20%">{{ row_mano.item }}</td>
                                                                                    <td width = "20%">{{ row_mano.descripcion }}</td>
                                                                                    <td width = "10%">{{ row_mano.unidad }}</td>
                                                                                    <td width = "10%">{{ row_mano.cantidad }}</td>
                                                                                    <td width = "10%">{{ row_mano.precio_unit }}</td>
                                                                                    <td width = "10%">{{ row_mano.cantidad * row_mano.precio_unit | number : 2 }}</td>
                                                                                    <td width = "10%">
                                                                                        <input ng-click = "mano.deleteMano(row_mano.id,row_mano.item)" width = "15%" type = "button" id = "btn_quitar" value = "      -     " class = "button-estilo-quitar">
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <div>
                                <span class = "pull-right margin-total-gen"> TOTAL : $ {{ 0.00 | number : 2}}<span>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</div>