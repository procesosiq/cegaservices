<?php
    $retval = $loader->GetCliente($_GET['id']);
    $areas = $loader->GetAreas();
?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Módulo de Clientes</h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="/clienteList">Listado de clientes</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Registro de clientes</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tab-pane" id="tab_1">
                                        <div class="portlet box blue">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>AGREGAR NUEVO CLIENTE</div>
                                            </div>
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                                <form action="#" class="horizontal-form" id="formcli" method="post">
                                                    <div class="form-body">
                                                        <h3 class="form-section">INFORMACIÓN DEL CLIENTE</h3><input type="hidden" id="idcli" value="<?php echo $_GET[id]; ?>">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Nombre y Apellidos</label>
                                                                    <input type="text" id="txtnom" class="form-control" value="<?php echo $retval->nombre; ?>">
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Correo Electrónico</label>
                                                                    <input type="text" id="txtemail" class="form-control" value="<?php echo $retval->email; ?>">
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Tipo de Cliente</label>
                                                                    <select class="form-control" id="s_tipocli">
                                                                        <option value="1" 
                                                                        <?php if($retval->id_tipcli==1){ ?>selected<?php }?>>Comercial</option>
                                                                        <option value="2" <?php if($retval->id_tipcli==2){ ?>selected<?php }?>>Industrial</option>
                                                                        <option value="3" <?php if($retval->id_tipcli==3){ ?>selected<?php }?>>Residencial</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Fecha de Registro</label>
                                                                    <input type="text" id="txtfec" class="form-control" value="<?php echo (isset($retval->fecha)?$retval->fecha:date("d/m/Y")); ?>" readonly> </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Cargo</label>
                                                                    <input type="text" id="s_equipos" class="form-control" value="<?php echo $retval->cargo ?>">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Area</label>
                                                                    <select class="form-control" data-placeholder="Elija una opción" tabindex="1" id="s_areas">
                                                                        <?php foreach($areas as $area){ ?>
                                                                            <option value="<?php echo $area->id; ?>"><?php echo $area->nombre; ?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/row-->
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Cantidad Equipos</label>
                                                                    <input type="text" id="s_equipos" class="form-control" value="<?php echo $retval->equipos ?>" readonly>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">¿Sucursales?</label>
                                                                    <select class="form-control" data-placeholder="Elija una opción" tabindex="1" id="s_sucursales">
                                                                        <option value="NO" <?php if($retval->sucursales=='NO'){ ?>selected<?php }?>>NO</option>
                                                                        <option value="SI" <?php if($retval->sucursales=='SI'){ ?>selected<?php }?>>SI</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                        <h3 class="form-section">INFORMACIÓN DE FACTURACIÓN</h3>
                                                        <div class="row">
                                                            <div class="col-md-12 ">
                                                                <div class="form-group">
                                                                    <label>Razón Social</label>
                                                                    <input type="text" class="form-control" id="txtrazon" value="<?php echo $retval->razon_social; ?>"> </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>RUC</label>
                                                                    <input type="text" class="form-control" id="txtruc" value="<?php echo $retval->ruc; ?>"> </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Dirección</label>
                                                                    <input type="text" class="form-control" id="txtdircli" value="<?php echo $retval->direccion; ?>"> </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Teléfono</label>
                                                                    <input type="text" class="form-control" id="txttel" value="<?php echo $retval->telefono; ?>"> </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Ciudad</label>
                                                                    <input type="text" class="form-control" id="txtciudad" value="<?php echo $retval->ciudad; ?>"> </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                    </div>
                                                    <div class="form-actions right">
                                                        <button id="btnCancelar" type="button" class="btn default">Cancelar</button>
                                                        <?php if($_GET[id]==''){?><button type="submit" class="btn blue" id="btnadd"><i class="fa fa-check"></i> Registrar</button><? }?>
                                                        <?php if($_GET[id]!=''){?><button type="submit" class="btn blue" id="btnupd"><i class="fa fa-check"></i> Modificar</button><? }?>
                                                    </div>
                                                </form>
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                    </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->