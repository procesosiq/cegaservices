        <link href="http://cdn.procesos-iq.com/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="http://cdn.procesos-iq.com/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper" ng-app="app" ng-controller="supervisores">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Materiales</h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="/">Inicio</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Inventario</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Begin: life time stats -->
                            <div class="portlet light portlet-fit portlet-datatable bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">LISTADO DE INVENTARIO</span>
                                    </div>
                                    <div class="tools">
                                        <div class="col-md-3 input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
                                            <input type="text" class="form-control form-filter input-sm" readonly name="order_date_from" placeholder="Inicio" ng-change="dateChange()" ng-model="date_start" id="date_from">
                                            <span class="input-group-btn">
                                                <button class="btn btn-sm default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <div class="col-md-3 input-group date date-picker" data-date-format="yyyy-mm-dd">
                                            <input type="text" class="form-control form-filter input-sm" readonly name="order_date_to" placeholder="Fin" ng-change="dateChange()" ng-model="date_end" id="date_to">
                                            <span class="input-group-btn">
                                                <button class="btn btn-sm default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <div class="btn-group">
                                            <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                <i class="fa fa-angle-down"></i>
                                            </button>
                                            <div class="dropdown-menu hold-on-click dropdown-checkboxes" role="menu">
                                                <label>
                                                    <input type="checkbox" checked index="0">Fecha</label>
                                                <label>
                                                    <input type="checkbox" checked index="1">OT</label>
                                                <label>
                                                    <input type="checkbox" checked index="2">Cliente</label>
                                                <label>
                                                    <input type="checkbox" checked index="3">Supervisor</label>
                                                <label>
                                                    <input type="checkbox" checked index="4">Item</label>
                                                <label>
                                                    <input type="checkbox" checked index="5">Descripcion</label>
                                                <label>
                                                    <input type="checkbox" checked index="6">Unidad</label>
                                                <label>
                                                    <input type="checkbox" checked index="7">Cant. Entregada</label>
                                                <label>
                                                    <input type="checkbox" checked index="8">Cant. Utilizada</label>
                                                <label>
                                                    <input type="checkbox" checked index="9">Por Entregar</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-container">
                                        <div class="table-actions-wrapper">

                                        </div>
                                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                                            <thead>
                                                <tr role="row" class="heading">
                                                    <th width="10%"> Fecha </th>
                                                    <th width="30%"> Orden<br>Trabajo </th>
                                                    <th width="30%"> Cliente </th>
                                                    <th width="30%"> Supervisor </th>
                                                    <th width="30%"> Item </th>
                                                    <th width="30%"> Descripción </th>
                                                    <th width="15%"> Unidad </th>
                                                    <th width="5%"> Cant. Entregada </th>
                                                    <th width="5%"> Cant. Utilizada </th>
                                                    <th width="5%"> Por Entregar </th>
                                                    <th width="5%"> Acciones </th>
                                                </tr>
                                                <tr role="row" class="filter">
                                                    <td>
                                                        <input type="hidden" class="form-control form-filter input-sm" name="from_date" value=""/>
                                                        <input type="hidden" class="form-control form-filter input-sm" name="to_date" value=""/>
                                                    </td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="order_ot"></td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="order_cliente"></td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="order_supervisor"> </td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="order_item"> </td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="order_descripcion"> </td>
                                                    <td> </td>
                                                    <td><input type="number" id="entregadas" class="form-control disable input-sm" value="0" readonly=""/></td>
                                                    <td><input type="number" id="utilizadas" class="form-control disable input-sm" value="0" readonly=""/></td>
                                                    <td><input type="number" id="por_entregar" class="form-control disable input-sm" value="0" readonly=""/></td>
                                                    <td>
                                                        <div class="margin-bottom-5">
                                                            <button class="btn btn-sm green btn-outline filter-submit margin-bottom">
                                                                <i class="fa fa-search"></i></button>
                                                            <button class="btn btn-sm red btn-outline filter-cancel">
                                                                <i class="fa fa-times"></i></button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody> </tbody>
                                            <tfoot>
                                                <th>TOTALES:</th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- End: life time stats -->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->