<!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>COTIZACIONES</h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">Inicio</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="#">Lista de cotizaciones</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Registro de cotizaciones</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-6 ">
                            <!-- BEGIN SAMPLE FORM PORTLET-->
                            <div class="portlet box purple">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i> Detalle de Cotización </div>
                                </div>
                                <div class="portlet-body form">
                                    <form role="form">
                                        <div class="form-body">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Nombre y Apellidos</label>
                                                                    <input type="text" id="txtnom" class="form-control" value="<?php echo $retval->nombre; ?>">
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Correo Electrónico</label>
                                                                    <input type="text" id="txtemail" class="form-control" value="<?php echo $retval->email; ?>">
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Tipo de Cliente</label>
                                                                    <select class="form-control" id="s_tipocli">
                                                                        <option value="1" 
                                                                        <?php if($retval->id_tipcli==1){ ?>selected<?php }?>>Comercial</option>
                                                                        <option value="2" <?php if($retval->id_tipcli==2){ ?>selected<?php }?>>Industrial</option>
                                                                        <option value="3" <?php if($retval->id_tipcli==3){ ?>selected<?php }?>>Residencial</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Fecha de Registro</label>
                                                                    <input type="text" id="txtfec" class="form-control date-picker" value="<?php echo $retval->fecha; ?>"> </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Cantidad Equipos</label>
                                                                    <input type="text" id="s_equipos" class="form-control" value="0" readonly>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">¿Sucursales?</label>
                                                                    <select class="form-control" data-placeholder="Choose a Category" tabindex="1" id="s_sucursales">
                                                                        <option value="NO" <?php if($retval->sucursales=='NO'){ ?>selected<?php }?>>NO</option>
                                                                        <option value="SI" <?php if($retval->sucursales=='SI'){ ?>selected<?php }?>>SI</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                        <h3 class="form-section">INFORMACIÓN DE FACTURACIÓN</h3>
                                                        <div class="row">
                                                            <div class="col-md-12 ">
                                                                <div class="form-group">
                                                                    <label>Razón Social</label>
                                                                    <input type="text" class="form-control" id="txtrazon" value="<?php echo $retval->razon_social; ?>"> </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>RUC</label>
                                                                    <input type="text" class="form-control" id="txtruc" value="<?php echo $retval->ruc; ?>"> </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Dirección</label>
                                                                    <input type="text" class="form-control" id="txtdircli" value="<?php echo $retval->direccion; ?>"> </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Teléfono</label>
                                                                    <input type="text" class="form-control" id="txttel" value="<?php echo $retval->telefono; ?>"> </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Ciudad</label>
                                                                    <input type="text" class="form-control" id="txtciudad" value="<?php echo $retval->ciudad; ?>"> </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                    </div>
                                        <div class="form-actions">
                                            <button type="button" class="btn default">Cancel</button>
                                            <button type="submit" class="btn red">Submit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- END SAMPLE FORM PORTLET-->
                            
                            <!-- BEGIN SAMPLE FORM PORTLET-->
                            <div class="portlet box blue ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i> Datos de Facturación </div>
                                </div>
                                <div class="portlet-body form">
                                    <form role="form">
                                        <div class="form-body">
                                            <div class="form-group has-success">
                                                <label class="control-label">Input with success</label>
                                                <input type="text" class="form-control" id="inputSuccess"> </div>
                                            <div class="form-group has-warning">
                                                <label class="control-label">Input with warning</label>
                                                <input type="text" class="form-control" id="inputWarning"> </div>
                                            <div class="form-group has-error">
                                                <label class="control-label">Input with error</label>
                                                <input type="text" class="form-control" id="inputError"> </div>
                                        </div>
                                        <div class="form-actions">
                                            <button type="button" class="btn default">Cancel</button>
                                            <button type="submit" class="btn red">Submit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- END SAMPLE FORM PORTLET-->
                            
                        </div>
                        <div class="col-md-6 ">
                            <!-- BEGIN SAMPLE FORM PORTLET-->
                            <div class="portlet box yellow">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i> Información del Cliente </div>
                                </div>
                                <div class="portlet-body form">
                                    <form role="form">
                                        <div class="form-body">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Tipo de Cliente</label>
                                                                    <select class="form-control" id="s_tipocli">
                                                                        <option value="1" 
                                                                        <?php if($retval->id_tipcli==1){ ?>selected<?php }?>>Comercial</option>
                                                                        <option value="2" <?php if($retval->id_tipcli==2){ ?>selected<?php }?>>Industrial</option>
                                                                        <option value="3" <?php if($retval->id_tipcli==3){ ?>selected<?php }?>>Residencial</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Nombre y Apellidos</label>
                                                                    <input type="text" id="txtnom" class="form-control" value="<?php echo $retval->nombre; ?>">
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Razón Social</label>
                                                                    <input type="text" class="form-control" id="txtrazon" value="<?php echo $retval->razon_social; ?>">
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>RUC</label>
                                                                    <input type="text" class="form-control" id="txtruc" value="<?php echo $retval->ruc; ?>"> </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Correo Electrónico</label>
                                                                    <input type="text" id="txtemail" class="form-control" value="<?php echo $retval->email; ?>">
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Teléfono</label>
                                                                    <input type="text" class="form-control" id="txttel" value="<?php echo $retval->telefono; ?>">
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                    </div>
                                        <div class="form-actions">
                                            <button type="button" class="btn default">Cancel</button>
                                            <button type="submit" class="btn red">Submit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- END SAMPLE FORM PORTLET-->
                            
                            <!-- BEGIN SAMPLE FORM PORTLET-->
                            <div class="portlet box green ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i> Dirección del Trabajo </div>
                                </div>
                                <div class="portlet-body form">
                                    <form role="form">
                                        <div class="form-body">
                                            <div class="form-group has-success">
                                                <label class="control-label">Input with success</label>
                                                <input type="text" class="form-control" id="inputSuccess"> </div>
                                            <div class="form-group has-warning">
                                                <label class="control-label">Input with warning</label>
                                                <input type="text" class="form-control" id="inputWarning"> </div>
                                            <div class="form-group has-error">
                                                <label class="control-label">Input with error</label>
                                                <input type="text" class="form-control" id="inputError"> </div>
                                        </div>
                                        <div class="form-actions">
                                            <button type="button" class="btn default">Cancel</button>
                                            <button type="submit" class="btn red">Submit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- END SAMPLE FORM PORTLET-->
                            
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->