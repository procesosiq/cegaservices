<?php

if(isset($_GET["ide"])){
    $retval = $loader->getEquipo($_GET["ide"]);
}

$datoscli = $loader->GetCliente($_GET['idc']);
$datossuc = $loader->GetSucursal($_GET['ids']);

$datosTipoEquipo = $loader->GetTipoEquipos();
$datosCapBUTU = $loader->GetCapacidadButu();
$datosCapHP = $loader->GetCapacidadHP();
$datosMarcas = $loader->GetMarcas();

$datosDescripciones = $loader->getDescripciones();
// $datosPartes = $loader->getPartes();
$datosRefrigerantes = $loader->getRefrigerantes();
$datosMotores = $loader->getMotores();

?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper" ng-app="app" ng-controller="equiposController">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content" ng-init="init(<?= (isset($retval->data_piezas)?true:false); ?>)">

        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1><?php echo strtoupper($datoscli->nombre); if(isset($_GET['ids'])  && $_GET['ids'] > 0){  echo " / "; }  echo strtoupper($datossuc->nombre); ?></h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->

        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="/clienteList">Clientes</a>
                <i class="fa fa-circle"></i>
            </li>
            <?php
            if(isset($_GET['ids']) && $_GET['ids'] > 0){
            ?>
            <li>
                <a href="/sucursalesList?idc=<?php echo $_GET['idc']; ?>">Sucursales</a>
                <i class="fa fa-circle"></i>
            </li>
            <?php
            }
            ?>
            <li>
                <a href="/equiposList?idc=<?php echo $_GET[idc]; ?>&ids=<?php echo $_GET[ids]; ?>">Equipos</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Registro de equipos</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->

        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="tab-pane" id="tab_1">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i>AGREGAR NUEVO EQUIPO</div>
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form action="#" class="horizontal-form" id="formcli" method="post">
                                        <div class="form-body">
                                            <!-- <label><b>Fecha de Registro: </b></label><label id="txtfec">  <?php echo " ".(isset($retval->fecha)?date("d/m/Y", strtotime($retval->fecha)):date("d/m/Y")); ?></label> -->
                                             <label class="control-label">Fecha de Registro : <?php echo (isset($retval->fecha)?$retval->fecha:date("d/m/Y")); ?></label>
                                            <h3 class="form-section">INFORMACIÓN GENERAL</h3><input type="hidden" id="idcli" value="<?php echo $_GET[idc]; ?>">
                                            <input type="hidden" id="idsuc" value="<?php echo $_GET[ids]; ?>">
                                            <input type="hidden" id="idequ" value="<?php echo $retval->id ?>">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input id="id_tipo_cliente" type="hidden" value="<?php echo $datoscli->id_tipcli ?>"/>
                                                        <label class="control-label">Tipo de Equipo</label>
                                                        <select class="form-control" id="s_tipoequi">
                                                            <option value="0"><< Seleccionar >></option>
                                                            <?php
                                                                foreach($datosTipoEquipo as $dato){
                                                                    echo "<option value='$dato->id' ".(($dato->id==$retval->id_tipo_equipo)?selected:'').">$dato->nombre</option>";
                                                                }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Descripción de Equipos</label>
                                                        <select class="form-control" id="s_descripcion">
                                                            <option value="0"><< Seleccionar >></option>
                                                            <?php
                                                                foreach($datosDescripciones as $dato){
                                                                    if($dato->id_tipoequipo == $retval->id_tipo_equipo):
                                                                    echo "<option value='$dato->id' ".(($dato->id==$retval->id_descripcion_equipo)?selected:'').">$dato->nombre</option>";
                                                                    endif;
                                                                }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Capacidad</label>
                                                        <input type="text" id="txtcapacidad" class="form-control" value="<?php echo $retval->capacidad; ?>"> 
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Área que Climatiza</label>
                                                        <select class="form-control" id="s_areaclima">
                                                            <option value='0'><< Seleccionar >></option>
                                                            <?php
                                                                if(isset($retval->id_tipo_equipo)){
                                                                    $data = $loader->getAreasClimatiza($retval->id_tipo_equipo, $datoscli->id_tipcli);
                                                                    foreach ($data as $key => $value) {
                                                                        $ids = explode(",", $value->ids);
                                                                        echo "<option value='".$value->id."' ".(in_array($retval->id_area_climatiza, $ids)?selected:'').">".$value->nombre."</option>";
                                                                    }
                                                                }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Nombre del área</label>
                                                        <input type="text" id="txtnombrearea" class="form-control" value="<?php echo $retval->nombre_area; ?>"> 
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Código</label>
                                                        <input type="text" id="txtcodigo" class="form-control" value="<?php echo $retval->codigo; ?>"> 
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->                                            
                                        </div>
                                        <div class="form-actions right">
                                            <button type="button" class="btn default">Cancelar</button>
                                            <?php if($_GET[ide]==''){?><button type="button" class="btn blue" id="btnadd"><i class="fa fa-check"></i> Registrar</button><? }?>
                                            <?php if($_GET[ide]!=''){?><button type="button" class="btn blue" id="btnupd"><i class="fa fa-check"></i> Modificar</button><? }?>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6 ">
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box yellow">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i> PARTES DEL EQUIPO </div>
                    </div>
                    <div class="portlet-body form">
                        <form role="form">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-12 ">
                                        <div class="form-group">
                                            <label class="control-label">Parte</label>
                                            <select class="form-control" id="s_parte">
                                                <option value="0"><< Seleccionar >></option>
                                                <?php
                                                foreach ($datosPartes as $key => $value) {
                                                    echo "<option value='".$value["id"]."'>".$value["nombre"]."</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 ">
                                        <div class="form-group">
                                            <label class="control-label">Piezas</label>
                                           <select class="form-control" id="cmbPiezas" multiple>
                                                
                                           </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Marca</label>
                                            <select class="form-control" name="marca" id="s_marcas">
                                                <option value="0"><< Seleccionar >></option>
                                            <?php
                                                foreach($datosMarcas as $dato){
                                                    echo "<option value='$dato->id'>$dato->nombre</option>";
                                                }
                                            ?>
                                            </select>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Capacidad</label>
                                            <input type="text" value class="form-control" id="s_capacidad2">
                                            <input type="hidden" value class="form-control" id="status" value="1">
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Modelo</label>
                                            <input type="text" class="form-control" id="txtmodelo" value="">
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Serie</label>
                                            <input type="text" class="form-control" id="txtserie" value=""> </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Refrigerante</label>
                                            <select class="form-control" name="refrigerantes" id="refrigerantes">
                                                <option value="0"><< Seleccionar >></option>
                                            <?php
                                                foreach($datosRefrigerantes as $dato){
                                                    echo "<option value='$dato->id'>$dato->descripcion</option>";
                                                }
                                            ?>
                                            </select>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Motor</label>
                                            <select class="form-control" id="motores" name="motores">
                                                <option value="0"><< Seleccionar >></option>
                                            <?php
                                                foreach($datosMotores as $dato){
                                                    echo "<option value='$dato->id'>$dato->descripcion</option>";
                                                }
                                            ?>
                                            </select>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="form-actions">
                                    <button ng-click="limpiar_partes()" type="button" class="btn default">Limpiar</button>
                                    <button id="agregar" ng-click="agregar_partes()" type="button" class="btn red">{{ (editar) ? "Editar" : "Agregar"  }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->   
            </div>
            <div class="col-md-6 ">
                <div class="portlet box green ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i>LISTA DE PARTES</div>
                    </div>
                    <div class="portlet-body portlet-datatable">
                    <div class="table-container table-scrollable">
                            <table class="table table-striped table-bordered table-hover" id="datatable_ajax">
                                <thead>
                                    <tr role="row" class="heading">
                                        <th class="all" width="30%"> Parte </th>
                                        <th class="all" width="20%"> Marca </th>
                                        <th class="all" width="10%">Capacidad</th>
                                        <th class="all" width="10%"> Modelo </th>
                                        <th class="all" width="10%"> Serie </th>
                                        <th class="all" width="10%"> Motor </th>
                                        <th class="all" width="10%"> Estado </th>
                                        <th class="all" width="10%"> </th> 
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="parte in partes">
                                        <td>{{ parte.parte_display }}</td>
                                        <td>{{ parte.marca }}</td>
                                        <td>{{ parte.capacidad }}</td>
                                        <td>{{ parte.modelo }}</td>
                                        <td>{{ parte.serie }}</td>
                                        <td>{{ parte.id_motor > 0 ? "MOTOR " + parte.id_motor : "S/N" }}</td>
                                        <td> 
                                            <button class="btn {{ parte.status > 0 ? 'green-jungle' : 'red' }}" ng-click="changeStatus(parte, parte.index)"> 
                                                <i class="fa {{ parte.status > 0 ? 'fa-check' : 'fa-close' }}"></i> 
                                            </button>
                                        </td>
                                        <td>
                                        <button ng-click="editarParte(parte, parte.index)" type="button" class="btn blue"> <i class="fa fa-pencil"></i></button>
                                        <button class="btn red" ng-click="elminarParte(parte, parte.index)"><i class="fa fa-eraser"></i> </button>
                                       </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->

    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->