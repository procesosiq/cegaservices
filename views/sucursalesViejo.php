<?php
    $retval = $loader->GetCliente($_GET['idc']);
    $retval2 = $loader->GetSucursal($_GET['id']);
?>
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1><?php echo strtoupper($retval->nombre); ?> 
                            <input type="hidden" id="idcli" value="<?php echo $_GET['idc']; ?>"></h1>
                            <input type="hidden" id="idsuc" value="<?php echo $_GET['id']; ?>"></h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="/clienteList">Clientes</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="/sucursalesList?idc=<?php echo $_GET['idc']; ?>"> Sucursales</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Registro de sucursales</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tab-pane" id="tab_1">
                                        <div class="portlet box blue">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>AGREGAR NUEVA SUCURSAL</div>
                                            </div>
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                                <form action="#" class="horizontal-form">
                                                    <div class="form-body">
                                                        <div class="row">
                                                             <div class="col-md-8">
                                                                <div class="form-group">
                                                                    <label class="control-label">Fecha de Registro : <?php echo (isset($retval2->fecha)?$retval2->fecha:date("d/m/Y")); ?></label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group pull-rigth">
                                                                    <label class="control-label">Datos Cliente Principal :</label>
                                                                    <input type="checkbox" id="datos_princial" class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <h3 class="form-section">INFORMACIÓN DE SUCURSAL</h3>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Nombre de la sucursal</label>
                                                                    <input type="text" id="txtnom" class="form-control" value="<?php echo $retval2->nombre; ?>">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 ">
                                                                <div class="form-group">
                                                                    <label>Razón Social</label>
                                                                    <input type="text" class="form-control" id="txtrazon" value="<?php echo $retval2->razon_social; ?>"> </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>RUC</label>
                                                                    <input type="text" class="form-control" id="txtruc" value="<?php echo $retval2->ruc; ?>"> </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Dirección</label>
                                                                    <input type="text" class="form-control" id="txtdircli" value="<?php echo $retval2->direccion; ?>"> </div>
                                                            </div>
                                                            
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Teléfono</label>
                                                                    <input type="text" class="form-control" id="txttel" value="<?php echo $retval2->telefono; ?>"> </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Tipo de Cliente</label>
                                                                    <select class="form-control" id="s_tipocli">
                                                                        <option value="1" 
                                                                        <?php if($retval2->id_tipcli==1){ ?>selected<?php }?>>Comercial</option>
                                                                        <option value="2" <?php if($retval2->id_tipcli==2){ ?>selected<?php }?>>Industrial</option>
                                                                        <option value="3" <?php if($retval2->id_tipcli==3){ ?>selected<?php }?>>Residencial</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Cantidad Equipos</label>
                                                                    <input type="text" id="s_equipos" class="form-control"  value="<?php echo $retval2->equipos; ?>" readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                       <?php if($_GET["id"]!=''){?>
                                                        <h3 class="form-section">INFORMACIÓN DE CONTACTO</h3>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Nombre del contacto</label>
                                                                    <input type="text" id="txtnom_contacto" class="form-control" value="<?php echo $retval->nombre_contacto; ?>">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Cargo</label>
                                                                    <input type="text" id="txtCargo" class="form-control" value="<?php echo $retval->cargo ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Teléfono</label>
                                                                    <input type="text" class="form-control" id="txttel_contacto" value="<?php echo $retval->telefono_contacto; ?>"> </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Correo Electrónico</label>
                                                                    <input type="text" id="txtemail" class="form-control" value="<?php echo $retval->email; ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Area</label>
                                                                    <input type="text" id="s_areas" class="form-control" value="<?php echo $retval->email; ?>">
                                                                </div>
                                                            </div>
                                                             <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="control-label">&nbsp</label>
                                                                    <button type="button" class="btn blue" id="btnAddcontact">Agregar Contacto</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <diw class="row">
                                                             <div class="col-md-12">
                                                               <table class="table table-striped table-hover">
                                                                   <thead>
                                                                       <tr>
                                                                            <td>ID</td>
                                                                            <td>Principal</td>
                                                                            <td>Nombre</td>
                                                                            <td>Cargo</td>
                                                                            <td>Teléfono</td>
                                                                            <td>Correo</td>
                                                                            <td>Área</td>
                                                                            <td>Acciones</td>
                                                                       </tr>
                                                                   </thead>
                                                                   <tbody id="rowsFilas">

                                                                   </tbody>
                                                               </table> 
                                                            </div>
                                                        </diw>
                                                        <?}?>
                                                    </div>
                                                    <div class="form-actions right">
                                                        <button type="button" id="btnCancelar" class="btn default">Cancelar</button>
                                                        <?php if($_GET[id]==''):?>
                                                            <button type="submit" class="btn blue" id="btnadd"><i class="fa fa-check"></i> Registrar</button>
                                                        <? endif;?>
                                                        <?php if($_GET[id]!=''):?>
                                                            <button type="submit" class="btn blue" id="btnupd"><i class="fa fa-check"></i> Guardar</button>
                                                        <? endif;?>
                                                    </div>
                                                </form>
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                    </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>