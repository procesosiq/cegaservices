<?php
    $retval = $loader->GetCliente($_GET['idc']);
    if($session->tipo_usuario != 1)
        die();
?>

        <link href="../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
                <style>
        #datatable_ajax.table td {
           text-align: center;   
        }
        </style>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1><?php echo strtoupper($retval->nombre); ?> 
                            <input type="hidden" id="idcli" value="<?php echo $_GET['idc']; ?>"></h1> 
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="/clienteList">Clientes</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Sucursales Registradas</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Begin: life time stats -->
                            <div class="portlet light portlet-fit portlet-datatable bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">LISTADO DE SUCURSALES</span>
                                    </div>
                                   <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-container">
                                        <div class="table-actions-wrapper">
                                            <a href="/sucursales?idc=<?php echo $_GET['idc']; ?>"><button class="btn btn-sm green">Agregar Sucursal</button></a>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                                            <thead>
                                                <tr role="row" class="heading">
                                                    <th width="2%"> ID</th>
                                                    <th width="2%"> ID</th>
                                                    <th width="17%"> Fecha </th>
                                                    <th width="20%"> Sucursal </th>
                                                    <th width="17%"> Contacto </th>
                                                    <th width="17%"> Teléfono </th>
                                                    <th width="2%"> Equipos </th>
                                                    <th width="17%"> Status </th>
                                                    <th width="3%"> Acciones </th>
                                                    <th width="5%"> Detalle </th>
                                                </tr>
                                                <tr role="row" class="filter">
                                                    <td> </td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="order_id"> </td>
                                                    <td>
                                                        <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
                                                            <input type="text" class="form-control form-filter input-sm" readonly name="order_date_from" placeholder="From">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-sm default" type="button">
                                                                    <i class="fa fa-calendar"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                        <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                                                            <input type="text" class="form-control form-filter input-sm" readonly name="order_date_to" placeholder="To">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-sm default" type="button">
                                                                    <i class="fa fa-calendar"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control form-filter input-sm" name="search_sucursal"> </td>
                                                    <td>
                                                        <input type="text" class="form-control form-filter input-sm" name="search_contacto"> </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <select name="order_status" class="form-control form-filter input-sm">
                                                            <option value="">Todos</option>
                                                            <option value="1">Activo</option>
                                                            <option value="0">Inactivo</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <div class="margin-bottom-5">
                                                            <button class="btn btn-sm green btn-outline filter-submit margin-bottom">
                                                                <i class="fa fa-search"></i></a></button>
                                                        </div>
                                                        <button class="btn btn-sm red btn-outline filter-cancel">
                                                        <i class="fa fa-times"></i></button>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </thead>
                                            <tbody> </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- End: life time stats -->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->