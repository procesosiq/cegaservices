        <link href="http://cdn.procesos-iq.com/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="http://cdn.procesos-iq.com/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="js/echarts.min.js"></script>
        <script src="js/macarons.js"></script>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper" ng-app="app" ng-controller="reportes">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content" ng-init="init()">
                    <!-- BEGIN PAGE BASE CONTENT -->

                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
                                <div class="visual">
                                    <i class="fa fa-comments"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span class="contadores" data-counter="counterup" data-value="0" id="cuadro_nuevas">0</span>
                                    </div>
                                    <div class="desc"> Nuevas Órdenes </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 red" href="#">
                                <div class="visual">
                                    <i class="fa fa-bar-chart-o"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span class="contadores" data-counter="counterup" data-value="0" id="cuadro_facturar">0</span>
                                    </div>
                                    <div class="desc"> Por facturar </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 grey-silver" href="#">
                                <div class="visual">
                                    <i class="fa fa-bar-chart-o"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span class="contadores" data-counter="counterup" data-value="0" id="cuadro_cantidad_rebotes">0</span>
                                    </div>
                                    <div class="desc"> Cantidad de Rebotes </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 purple-intense" href="#">
                                <div class="visual">
                                    <i class="fa fa-bar-chart-o"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span class="contadores" data-counter="counterup" data-value="0" id="cuadro_rendimiento">0 </span>%
                                    </div>
                                    <div class="desc"> Rendimiento </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 green" href="#">
                                <div class="visual">
                                    <i class="fa fa-shopping-cart"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span class="contadores" data-counter="counterup" data-value="0" id="cuadro_cumplimiento">0</span>%
                                    </div>
                                    <div class="desc"> Cumplimiento </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 purple" href="#">
                                <div class="visual">
                                    <i class="fa fa-globe"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span class="contadores" data-counter="counterup" data-value="0" id="cuadro_efectividad">0</span>% </div>
                                    <div class="desc"> Efectividad </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 yellow" href="#">
                                <div class="visual">
                                    <i class="fa fa-globe"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span class="contadores" data-counter="counterup" data-value="0" id="cuadro_rebotes">0</span>% </div>
                                    <div class="desc"> Rebotes </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 yellow-gold" href="#">
                                <div class="visual">
                                    <i class="fa fa-globe"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span class="contadores" data-counter="counterup" data-value="0" id="cuadro_procedimientos">0</span>% </div>
                                    <div class="desc"> Procedimientos </div>
                                </div>
                            </a>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12">
                            <!-- Begin: life time stats -->
                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <span class="caption-subject font-dark sbold uppercase">REPORTES RESUMEN</span>
                                    </div>
                                    <div class="actions">
                                        <div class="dt-buttons">
                                            <a class="dt-button buttons-print btn dark btn-outline" tabindex="0" aria-controls="datatable_ajax"><span>Imprimir</span></a>
                                            <a class="dt-button buttons-pdf buttons-html5 btn green btn-outline" tabindex="0" aria-controls="datatable_ajax"><span>PDF</span></a>
                                            <a class="dt-button buttons-excel buttons-html5 btn yellow btn-outline" tabindex="0" aria-controls="datatable_ajax"><span>Excel</span></a>
                                            <a class="dt-button buttons-csv buttons-html5 btn purple btn-outline" tabindex="0" aria-controls="datatable_ajax"><span>CSV</span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-body" style="height: 60px;">
                                    <!--PRIMER DATE PICKER DATE PICKER-->
                                    <div class="col-md-3 col-md-offset-2">
                                        <div class="input-group date" data-provide="datepicker"  data-date-format="yyyy-mm-dd">
                                            <input type="text" id="date_start" class="form-control" ng-model="date1" ng-change="refresh()">
                                            <div class="input-group-addon">
                                                <span class="glyphicon glyphicon-th"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <!--SEGUNDO DATE PICKER-->
                                    <div class="col-md-3 col-md-offset-2">
                                        <div class="input-group date" data-provide="datepicker" data-date-format="yyyy-mm-dd">
                                            <input type="text" id="date_end" class="form-control" ng-model="date2" ng-change="refresh()">
                                            <div class="input-group-addon">
                                                <span class="glyphicon glyphicon-th"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings"></i>Historico</div>
                                    <div class="tools">
                                        <select class="form-control" name="indicador" id="indicador" onchange="changeSupervisor()">
                                            <option value="cumplimiento">Cumplimiento</option>
                                            <option value="efectividad">Efectividad</option>
                                            <option value="rebotes">Cant. de Rebotes</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="portlet-body">

                                    <div id="chart_general" style="height:400px;"></div>
                                </div>
                            </div>
                            <!-- End: life time stats -->
                        </div>
                    </div>
                    <div class="row">
                        <!-- START: BOX TIPO DE CLIENTE -->
                        <div class="col-md-6">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings"></i>TIPO DE CLIENTE</div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse" data-original-title="Expandir/Contraer" title=""> </a>
                                        <!--
                                        <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
                                        <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
                                        <a href="javascript:;" class="remove" data-original-title="" title=""> </a>
                                        -->
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="portlet-body">

                                        <table class="table table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th width="35%"> T. Cliente </th>
                                                    <th width="20%"> % Cumpl </th>
                                                    <th width="20%"> % Efect </th>
                                                    <th width="20%"> Total </th>
                                                    <th width="5%">&nbsp;</th>
                                                </tr>
                                            </thead>
                                        </table>

                                        <div class="panel-group accordion" id="accordion3">
                                            <div class="panel panel-default" ng-repeat="tipo in tipoCliente">
                                                <div class="panel-heading">
                                                        <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse"  data-parent="#accordion3" href="#tipo{{tipo.id}}" aria-expanded="false"> 
                                                    <h4 class="panel-title">
                                                            <table class="table" class="nobmp-reporte-accordion" style="border: 0; padding: 0; margin: 0;">
                                                                <thead class="nobmp-reporte-accordion">
                                                                    <tr class="nobmp-reporte-accordion">
                                                                        <th width="40%" class="nobmp-reporte-accordion"> {{ tipo.nombre }} </th>
                                                                        <th width="20%" class="nobmp-reporte-accordion"> {{ (tipo.cumplimiento || 0) | number : 2 }}% </th>
                                                                        <th width="20%" class="nobmp-reporte-accordion"> {{ (tipo.efectividad || 0) | number : 2 }}% </th>
                                                                        <th width="20%" class="nobmp-reporte-accordion"> {{ tipo.total }} </th>
                                                                    </tr>
                                                            </thead>
                                                            </table>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="tipo{{tipo.id}}" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                                    <div class="panel-body">
														<table class="table" class="nobmp-reporte-accordion" style="border: 0; padding: 0; margin: 0;">
                                                            <thead class="nobmp-reporte-accordion">
                                                                <tr class="nobmp-reporte-accordion" ng-repeat="clientes in tipo.clientes">
                                                                    <th width="40%" class="nobmp-reporte-accordion"><a href="" ng-click="getTipoEquipo(clientes)"> {{clientes.nombre}} </a> </th>
                                                                    <td width="20%" class="nobmp-reporte-accordion">{{ (clientes.cumplimiento || 0) | number : 2 }}%</td>
                                                                    <td width="20%" class="nobmp-reporte-accordion">{{ (clientes.efectividad || 0) | number : 2 }}%</td>
                                                                    <td width="20%" class="nobmp-reporte-accordion">{{ clientes.total }}</td>
                                                                </tr>
                                                            </thead>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                            
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END: BOX TIPO DE CLIENTE -->
                        <!-- START: BOX TIPO DE EQUIPO -->
                        <div class="col-md-6">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings"></i>TIPO DE EQUIPO</div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse" data-original-title="Expandir/Contraer" title=""> </a>
                                        <!--
                                        <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
                                        <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
                                        <a href="javascript:;" class="remove" data-original-title="" title=""> </a>
                                        -->
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="portlet-body">

                                        <table class="table table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th width="45%"> T. Equipo </th>
                                                    <th width="25%"> Equipos </th>
                                                    <th width="25%"> Órdenes </th>
                                                    <th width="5%">&nbsp;</th>
                                                </tr>
                                            </thead>
                                        </table>


                                        <div class="panel-group accordion" id="accordion4">
                                            <div class="panel panel-default" ng-repeat="tipo in tipoEquipo">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion4" href="#equipo{{tipo.id}}" aria-expanded="false"> 
                                                            <table class="table" class="nobmp-reporte-accordion" style="border: 0; padding: 0; margin: 0;">
                                                                <thead class="nobmp-reporte-accordion">
                                                                    <tr class="nobmp-reporte-accordion">
                                                                        <th width="50%" class="nobmp-reporte-accordion"> {{tipo.nombre}}</th>
                                                                        <th width="30%" class="nobmp-reporte-accordion"> {{tipo.equipos}} </th>
                                                                        <th width="20%" class="nobmp-reporte-accordion"> {{tipo.ordenes}} </th>
                                                                    </tr>
                                                            </thead>
                                                            </table>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="equipo{{tipo.id}}" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                                    <div class="panel-body">
                                                        <table class="table" class="nobmp-reporte-accordion" style="border: 0; padding: 0; margin: 0;">
                                                                <thead class="nobmp-reporte-accordion">
                                                                    <tr class="nobmp-reporte-accordion" ng-repeat="descripcion in tipo.descripciones">
                                                                        <th width="50%" class="nobmp-reporte-accordion"> {{descripcion.nombre}}</th>
                                                                        <th width="30%" class="nobmp-reporte-accordion"> {{descripcion.equipos}} </th>
                                                                        <th width="20%" class="nobmp-reporte-accordion"> {{descripcion.ordenes}} </th>
                                                                    </tr>
                                                            </thead>
                                                            </table>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END: BOX TIPO DE EQUIPO -->
                    </div>
                    <div class="row" style="display: none">
                        <!-- START: GRAFICA TIPO DE CLIENTE -->
                        <div class="col-md-6">
                            <div class="portlet light">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings"></i>
                                        <span class="caption-subject"> TIPO DE CLIENTE </span>
                                    </div>
                                    <div class="tools">
                                        <div class="bootstrap-switch-container" style="width: 255px;">
                                            <span class="bootstrap-switch-label" style="width: 85px;">&nbsp;</span>
                                            <input type="checkbox" class="make-switch checked" data-on-text="&nbsp;Cumplimiento&nbsp;" data-off-text="&nbsp;Efectividad&nbsp;" checked>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="chart_tipo_cliente" style="height:350px;"></div>
                                </div>
                            </div>
                        </div>
                        <!-- END: GRAFICA TIPO DE CLIENTE -->

                        <!-- START: GRAFICA TIPO DE CLIENTE -->
                        <div class="col-md-6">
                            <div class="portlet light">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings"></i>
                                        <span class="caption-subject"> TIPO DE EQUIPO </span>
                                    </div>
                                    <div class="tools">
                                        <div class="bootstrap-switch-container" style="width: 255px;">
                                            <span class="bootstrap-switch-label" style="width: 85px;">&nbsp;</span>
                                            <input type="checkbox" class="make-switch checked" data-on-text="&nbsp;Cumplimiento&nbsp;" data-off-text="&nbsp;Efectividad&nbsp;" checked>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs ">
                                            <li class="active"><a href="#tab_15_1" data-toggle="tab">Total</a></li>
                                            <li><a href="#tab_15_2" data-toggle="tab">Clima A</a></li>
                                            <li><a href="#tab_15_3" data-toggle="tab">Clima B</a></li>
                                            <li><a href="#tab_15_4" data-toggle="tab">Ventilación</a></li>
                                            <li><a href="#tab_15_5" data-toggle="tab">Refrigeración</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_15_1">
                                                <div id="chart_tipo_equipo_1" style="height:350px;"></div>
                                                1
                                            </div>
                                            <div class="tab-pane" id="tab_15_2">
                                                <div id="chart_tipo_equipo_2" style="height:350px;"></div>
                                                2
                                            </div>
                                            <div class="tab-pane" id="tab_15_3">
                                                <div id="chart_tipo_equipo_3" style="height:350px;"></div>
                                                3
                                            </div>
                                            <div class="tab-pane" id="tab_15_4">
                                                <div id="chart_tipo_equipo_4" style="height:350px;"></div>
                                                4
                                            </div>
                                            <div class="tab-pane" id="tab_15_5">
                                                <div id="chart_tipo_equipo_5" style="height:350px;"></div>
                                                5
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END: GRAFICA TIPO DE CLIENTE -->
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>



            <!-- END CONTENT -->
