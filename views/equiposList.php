<?php
   
$datoscli = $loader->GetCliente($_GET['idc']);
$datossuc = $loader->GetSucursal($_GET['ids']);

?>
        <link href="//cdn.procesos-iq.com/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="//cdn.procesos-iq.com/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
                <style>
        #datatable_ajax.table td {
           text-align: center;   
        }
        </style>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1><?php echo strtoupper($datoscli->nombre); if(isset($_GET['ids'] )  && $_GET['ids'] > 0){  echo " / "; }  echo strtoupper($datossuc->nombre); ?></h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="/clienteList">Clientes</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <?php
                        if(isset($_GET['ids'])  && $_GET['ids'] > 0){
                        ?>
                        <li>
                            <a href="/sucursalesList?idc=<?php echo $_GET['idc']; ?>">Sucursales</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <?php
                        }
                        ?>
                        <li>
                            <span class="active">Equipos Registrados</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Begin: life time stats -->
                            <div class="portlet light portlet-fit portlet-datatable bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">LISTADO DE EQUIPOS
										
										<input type="hidden" id="idcli" value="<?php echo $_GET[idc]; ?>">
										<input type="hidden" id="idsuc" value="<?php echo $_GET[ids]; ?>">
                                            </span>
                                            <input type="hidden" id="idsuc" value="<?php echo $_GET['ids']; ?>">
                                            <input type="hidden" id="idcli" value="<?php echo $_GET['idc']; ?>">
                                    </div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-container">
                                        <div class="table-actions-wrapper">
                                            <a href="/equipos?idc=<?php echo $_GET['idc']; ?>&ids=<?php echo $_GET['ids']; ?>"><button class="btn btn-sm green">Agregar Equipo</button></a>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                                            <thead>
                                                <tr role="row" class="heading">
                                                    <th width="5%"> ID</th>
                                                    <th width="5%"> Código </th>
                                                    <th width="20%"> Nombre del Área </th>
                                                    <th width="15%"> Marca </th>
                                                    <!--<th width="10%"> Fecha </th>
                                                    <th width="200"> Tipo Equipo </th>-->
                                                    <th width="30%"> Descripción Equipo </th>
                                                    <th width="10%"> Capacidad </th>
                                                    <!--<th width="10%"> Área Climatiza </th>
                                                    <th width="10%"> Código </th>-->
                                                    <th width="10%"> Status </th>
                                                    <th width="10%"> Acciones </th>
                                                </tr>
                                                <tr role="row" class="filter">
                                                    <td> </td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="order_id"> </td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="order_name_area"> </td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="order_marca"> </td>
                                                    <!--<td>
                                                        <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
                                                            <input type="text" class="form-control form-filter input-sm" readonly name="order_date_from" placeholder="From">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-sm default" type="button">
                                                                    <i class="fa fa-calendar"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                        <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                                                            <input type="text" class="form-control form-filter input-sm" readonly name="order_date_to" placeholder="To">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-sm default" type="button">
                                                                    <i class="fa fa-calendar"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="search_t_equipo"> </td>-->
                                                    <td><input type="text" class="form-control form-filter input-sm" name="search_descripcion_equipo"> </td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="search_capacidad"></td>
                                                    <!--<td><input type="text" class="form-control form-filter input-sm" name="search_a_climatiza"></td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="search_codigo"></td>-->
                                                    <td>
                                                        <select name="order_status" class="form-control form-filter input-sm">
                                                            <option value="1">Activo</option>
                                                            <option value="0">Inactivo</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <div class="margin-bottom-5">
                                                            <button class="btn btn-sm green btn-outline filter-submit margin-bottom">
                                                                <i class="fa fa-search"></i> 
                                                            </button>
                                                        </div>
                                                        <button class="btn btn-sm red btn-outline filter-cancel">
                                                            <i class="fa fa-times"></i> 
                                                        </button>
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody> </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- End: life time stats -->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->