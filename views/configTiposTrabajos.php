<link href="../assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
<link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">

<style>
    @page {
      size: A4;
    }
    .textoFire{
    color: rgb(34, 34, 34); 
    font-family: arial, sans-serif; 
    font-size: 10px; 
    line-height: normal;
    }
    .borderBox {
        border: solid 1px;
        padding: 5px;
    }
    #ajax-modal {
        top:5%;
        right:50%;
        outline: none;
    }
    .button-estilo{
        background-color : #26C281;
		color:white;
    }
    .button-estilo-quitar {
        background-color : red;
		color:white;
    }
    .margin-total{
        margin-right : 10px;
        /*font-weight: bold;*/
    }
    .margin-total-gen{
        margin-top : 10px;
        margin-right : 40px;
        font-weight: bold;   
    }
    .div-margin{
        margin : 10px;
    }
    .div-margin-top{
        margin-top : 30px;
    }
    .div-margin-left{
        margin-left: 10px !important;
    }
    .margin-top{
        margin-top : 20px;
    }
    .button{
        background-color: white;
        border: white;
    }
    .center-text{
        text-align : center;
    }
    .estilos-form-control{
        height : 30px !important;
        font-size : 10px !important;
    }
    .altura{
        padding: 2px !important
    }
    .color-white{
        color : white !important;
    }
    .color-black{
        color : black !important;
    }
    .tamanio{
        width : 40% !important;
    }
</style>

<!-- background-color : #E35B5A !important; -->

<div id="cont" class="page-content-wrapper" ng-app="app" ng-controller="configTiposTrabajos" ng-cloak>
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1>CONFIGURACION DE TIPOS DE TRABAJOS</h1>
            </div>
        </div>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a>Tipos de Trabajos</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active"></span>
            </li>
        </ul>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet grey-cascade box">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>FORMULARIO</div>
                            <div class="actions">
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row col-md-12">
                                <div>
                                    <div class="col-md-6 div-margin">
                                        <div class="col-md-2">
                                            <label for="trabajo" class="control-label">Trabajo</label>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" name="trabajo" id="trabajo" class="form-control" required ng-model="data.trabajo">
                                        </div>
                                    </div>
                                    <div class="col-md-6 div-margin">
                                        <div class="col-md-2">
                                            <label for="status" class="control-label">Status</label>
                                        </div>
                                        <div class="col-md-6">
                                            <select name="status" id="status" class="form-control" ng-model="data.status">
                                                <option ng-selected="data.status == 'Activo'" value="Activo">Activo</option>
                                                <option ng-selected="data.status == 'Inactivo'" value="Inactivo">Inactivo</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 div-margin">
                                        <div class="col-md-2">
                                            <label for="color" class="control-label">Color</label>
                                        </div>
                                        <div class="col-md-6">
                                            <select name="color" id="color" class="form-control" required ng-selected = "key == data.color" ng-model="data.color">
                                                <option ng-repeat = "(key, value) in colores | orderBy:'label'" class="{{value.color}}" value="{{value.color}}">{{value.label}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 div-margin">
                                        <div class="col-md-2">
                                            <label for="" class="control-label"></label>
                                        </div>
                                        <div class="col-md-6">
                                            <button class="form-control btn blue" id="guardar" name="guardar" ng-click="insert()">Guardar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row div-margin-top">
                                <div class="div-margin-left">
                                    <h4 class="center-text">Listado de tipos de trabajo </h4>
                                </div>
                                <div class="col-md-12">
                                    <div class="table-responsive table-scrollable">
                                        <table class="table table-hover table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th class="center-text"> ID </th>
                                                    <th class="center-text"> Tipos trabajo </th>
                                                    <th class="center-text"> Color </th>
                                                    <th class="center-text"> Status </th>
                                                    <th class="center-text"> Acciones </th>
                                                </tr>                                           
                                                <tr role="row" class="filter">
                                                    <td><input type = "text" ng-model = "searchTable.id" class = "color-td-prac form-control form-filter"/></td>
                                                    <td><input type = "text" ng-model = "searchTable.descripcion" class = "color-td-prac form-control form-filter"/></td>
                                                    <td>
                                                        <select name="color" id="color" class="form-control {{color}}" required ng-model="searchTable.color">
                                                            <option value="">Todos</option>
                                                            <option ng-selected="color = {{value.color}}" ng-repeat = "(key, value) in colores | orderBy:'label'" class="{{value.color}}" value="{{value.color}}">{{value.label}}</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select name="prioridad" id="prioridad" ng-model = "searchTable.status" class = "color-td-pract form-control form-filter">
                                                            <option value="">Todos</option>
                                                            <option value="Activo">Activo</option>
                                                            <option value="Inactivo">Inactivo</option>
                                                        </select>
                                                    </td>
                                                    <td></td>
                                                </tr>                                               
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat = "row in trabajos | filter : searchTable | orderBy : 'id'">
                                                    <td class="center-text altura"> {{ row.id }} </td>
                                                    <td class="center-text altura"> {{ row.descripcion }} </td>
                                                    <td class="center-text altura {{ row.color }}"> {{ row.color }} </td>
                                                    <td class="center-text altura"> {{ row.status }}</td>
                                                    <td class="center-text altura">
                                                        <button class="form-control btn blue div-margin tamanio" ng-click="show(row.id)"> Editar </button>
                                                        <button class="form-control btn bg-red-intense div-margin tamanio" ng-click="eliminar(row.id)"> Eliminar </button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</div>