<link href="../assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
<link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">

<style>
    @page {
      size: A4;
    }
    .textoFire{
    color: rgb(34, 34, 34); 
    font-family: arial, sans-serif; 
    font-size: 10px; 
    line-height: normal;
    }
    .borderBox {
        border: solid 1px;
        padding: 5px;
    }
    #ajax-modal {
        top:5%;
        right:50%;
        outline: none;
    }
    .button-estilo{
        background-color : #26C281;
		color:white;
    }
    .button-estilo-quitar {
        background-color : red;
		color:white;
    }
    .margin-total{
        margin-right : 10px;
        /*font-weight: bold;*/
    }
    .margin-total-gen{
        margin-top : 10px;
        margin-right : 40px;
        font-weight: bold;   
    }
    .div-margin{
        margin : 10px;
    }
    .div-margin-top{
        margin-top : 20px;
    }
    .div-margin-left{
        margin-left: 10px !important;
    }
    .margin-top{
        margin-top : 20px;
    }
    .button{
        background-color: transparent;
        border: transparent;
    }
    .center-text{
        text-align : center;
    }
    .estilos-form-control{
        height : 30px !important;
        font-size : 10px !important;
    }
    .altura{
        padding: 0px !important
    }
    .color-white{
        color : white !important;
    }
    .color-black{
        color : black !important;
    }
    .select2-search__field{
        width : 100% !important;
        border : 1px solid #c2cad8 !important;
    }
</style>

<!-- background-color : #E35B5A !important; -->

<div id="cont" class="page-content-wrapper" ng-app="app" ng-controller="controlRequerimientos" ng-cloak>
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1>CONTROL DE REQUERIMIENTOS</h1>
            </div>
        </div>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a>Requerimientos</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active"></span>
            </li>
        </ul>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet grey-cascade box">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>CONTROL</div>
                            <div class="actions">
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="panel-group accordion" id="accordion3">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a ng-click="" class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_3" aria-expanded="false"> 
                                                FORMULARIO
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse_3_3" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div>
                                                        <div class="row div-margin">
                                                            <div class="col-md-4">
                                                                <label for="clientes" required class="control-label">Cliente<span class = "required">  * </span></label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <select ng-change="getSucursales()" name="clientes" id="clientes" class="form-control" required ng-selected = "key == data.cliente" ng-model="data.cliente">
                                                                    <option ng-repeat = "(key, value) in clientes | orderBy:'label'" value = "{{value.id}}">{{value.label}}</option>
                                                                    <option value="N/A">N/A</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="row div-margin">
                                                            <div class="col-md-4">
                                                                <label for="sucursal" required class="control-label">Sucursal</label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <select name="sucursal" id="sucursal" class="form-control" required ng-selected = "key == data.sucursal" ng-model="data.sucursal">
                                                                    <option ng-repeat = "(key, value) in sucursales | orderBy:'label'" value = "{{value.id}}">{{value.label}}</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="row div-margin">
                                                            <div class="col-md-4">
                                                                <label for="prioridad" class="control-label">Nivel de prioridad</label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <select name="prioridad" id="prioridad" class="form-control" ng-model="data.niv_prioridad">
                                                                    <option class="bg-green-jungle" value="BAJO">BAJO</option>
                                                                    <option class="bg-yellow-saffron" value="MEDIO">MEDIO</option>
                                                                    <option class="bg-red-intense" value="ALTO">ALTO</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="row div-margin">
                                                            <div class="col-md-4">
                                                                <label for="fecha_recepcion" class="control-label">Fecha recepcion</label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
                                                                    <input type="text" class="form-control form-filter input-sm" readonly name="fecha_recepcion" placeholder="YY/MM/DD" ng-model="data.fecha_recepcion" id="fecha_recepcion">
                                                                    <span class="input-group-btn">
                                                                        <button class="btn btn-sm default" type="button">
                                                                            <i class="fa fa-calendar"></i>
                                                                        </button>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row div-margin hide">
                                                            <div class="col-md-4">
                                                                <label for="receptor" class="control-label">Receptor</label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <input type="text" name="receptor" id="receptor" class="form-control" required ng-model="data.receptor">
                                                            </div>
                                                        </div>
                                                        <div class="row div-margin">
                                                            <div class="col-md-4">
                                                                <label for="fecha_confirmacion" class="control-label">Fecha confirmacion de cliente</label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
                                                                    <input type="text" class="form-control form-filter input-sm" readonly name="fecha_confirmacion" placeholder="YY/MM/DD" ng-model="data.fecha_confirmacion" id="fecha_confirmacion">
                                                                    <span class="input-group-btn">
                                                                        <button class="btn btn-sm default" type="button">
                                                                            <i class="fa fa-calendar"></i>
                                                                        </button>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row div-margin">
                                                            <div class="col-md-4">
                                                                <label for="tipo_trabajo" class="control-label">Tipo de trabajo</label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <select name="tipo_trabajo" id="tipo_trabajo" class="form-control" ng-model="data.tipo_trabajo">
                                                                    <option ng-repeat = "(key, value) in tipos_trabajos | orderBy:'label'" class="{{ value.color }}" value = "{{value.id}}">{{value.label}}</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="row div-margin">
                                                            <div class="col-md-4">
                                                                <label for="fecha_programada" class="control-label">Fecha programada de trabajo</label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
                                                                    <input type="text" class="form-control form-filter input-sm" readonly ng-change="llenaSemMes()" name="fecha_programada" placeholder="YY/MM/DD" ng-model="data.fecha_programada" id="fecha_programada">
                                                                    <span class="input-group-btn">
                                                                        <button class="btn btn-sm default" type="button">
                                                                            <i class="fa fa-calendar"></i>
                                                                        </button>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row div-margin">
                                                            <div class="col-md-4">
                                                                <label for="mes" class="control-label">Mes</label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <select name="mes" id="mes" class="form-control" ng-model="data.mes">
                                                                    <option ng-repeat="(key, value) in meses" ng-selected="data.mes == key" value = "{{value.id}}">{{value.label}}</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="row div-margin">
                                                            <div class="col-md-4">
                                                                <label for="semana" class="control-label">Semana del año</label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <select name="semana" id="semana" class="form-control" ng-model="data.semana">
                                                                    <option ng-repeat="(key, value) in semanas" ng-selected="data.semana == key" value = "{{value.id}}">{{value.label}}</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="row div-margin">
                                                            <div class="col-md-4">
                                                                <label for="hora" class="control-label">Hora</label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <div class="input-group">
                                                                    <div class='input-group date' id='hora'> 
                                                                        <input type='text' class="form-control"/><span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span> 
                                                                    </div>
                                                                </div>  
                                                            </div>
                                                        </div>
                                                        <div class="row div-margin">
                                                            <div class="col-md-4">
                                                                <label for="zona" class="control-label">Zona</label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <select name="zona" id="zona" class="form-control" ng-model="data.zona">
                                                                    <option ng-repeat = "(key, value) in zonas | orderBy:'label'" value = "{{value.id}}">{{value.label}}</option>
                                                                    <option value="OTROS">OTROS</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="row div-margin">
                                                            <div class="col-md-4">
                                                                <label for="personal_ejecutor" required class="control-label">Personal ejecutor <span class = "required">  * </span></label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <select name="personal_ejecutor" id="personal_ejecutor" class="form-control" required ng-selected = "key == data.personal" ng-model="data.personal">
                                                                    <option ng-repeat = "(key, value) in personal | orderBy:'label'" style="background-color:{{value.color}}" value = "{{value.id}}">{{value.label}}</option>
                                                                    <option value="0">N/A</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="row div-margin">
                                                            <div class="col-md-4">
                                                                <label for="auxiliar" required class="control-label">Auxiliar</label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <label for="auxiliar" required class="control-label"> {{data.auxiliares}} </label>
                                                                <select name="auxiliar" id="auxiliar" class="form-control" required multiple="true" ng-selected = "key == data.auxiliar" ng-model="data.auxiliar">
                                                                    <option ng-repeat = "(key, value) in auxiliar | orderBy:'label'" style="background-color:{{value.color}}" value = "{{value.id}}">{{value.label}}</option>
                                                                    <option value="0">N/A</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="row div-margin">
                                                            <div class="col-md-4">
                                                                <label for="estado" class="control-label">Estado</label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <select name="estado" id="estado" class="form-control" required ng-selected = "key == data.estado" ng-model="data.estado">
                                                                    <option ng-repeat = "(key, value) in estados | orderBy:'id'" class="{{value.color}}" value = "{{value.label}}">{{value.label}}</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="row div-margin">
                                                            <div class="col-md-12">
                                                                <div class="pull-right">
                                                                    <button class="btn blue" ng-click="insert()">Guardar</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row div-margin-top">
                                <div class="padding div-margin-left">
                                    <h4>Listado de requerimientos</h4>
                                </div>
                                <div class="col-md-12">
                                    <div class="table-responsive table-scrollable">
                                        <table class="table table-hover table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th style="min-width:250px" class="center-text"><a ng-click="orderByField = 'cliente'; reverseSort = !reverseSort">Cliente</a></th>
                                                    <th style="min-width:200px" class="center-text"><a ng-click="orderByField = 'sucursal'; reverseSort = !reverseSort">Sucursal</a></th>
                                                    <th style="min-width:150px" class="center-text"><a ng-click="orderByField = 'niv_prioridad'; reverseSort = !reverseSort">Niv. Prioridad</a></th>
                                                    <th style="min-width:100px" class="center-text"><a ng-click="orderByField = 'fecha_recepcion'; reverseSort = !reverseSort">Fecha Recep.</a></th>
                                                    <th style="min-width:250px" class="center-text"><a ng-click="orderByField = 'receptor'; reverseSort = !reverseSort">Receptor</a></th>
                                                    <th style="min-width:100px" class="center-text"><a ng-click="orderByField = 'fecha_confirmacion'; reverseSort = !reverseSort">Fecha Confirm.</a></th>
                                                    <th style="min-width:200px" class="center-text"><a ng-click="orderByField = 'tipo_trabajo'; reverseSort = !reverseSort">T. Trabajo</a></th>
                                                    <th style="min-width:30px" class="center-text"><a ng-click="orderByField = 'semana'; reverseSort = !reverseSort">Semana</a></th>
                                                    <th style="min-width:100px" class="center-text"><a ng-click="orderByField = 'fecha_programada'; reverseSort = !reverseSort">Fecha Prog.</a></th>
                                                    <th style="min-width:100px" class="center-text"><a ng-click="orderByField = 'hora'; reverseSort = !reverseSort">Hora</a></th>
                                                    <th style="min-width:200px" class="center-text"><a ng-click="orderByField = 'zona'; reverseSort = !reverseSort">Zona</a></th>
                                                    <th style="min-width:150px" class="center-text"><a ng-click="orderByField = 'mes'; reverseSort = !reverseSort">Mes</a></th>
                                                    <th style="min-width:200px" class="center-text"><a ng-click="orderByField = 'personal'; reverseSort = !reverseSort">Personal Eje.</a></th>
                                                    <th style="min-width:200px" class="center-text"><a ng-click="orderByField = 'auxiliar'; reverseSort = !reverseSort">Auxiliar</a></th>
                                                    <th style="min-width:200px" class="center-text"><a ng-click="orderByField = 'estado'; reverseSort = !reverseSort">Estado</a></th>
                                                    <th style="min-width:100px" class="center-text">Acciones</th>
                                                </tr>                                           
                                                <tr role="row" class="filter">
                                                    <td><input type = "text" ng-model = "searchTable.cliente" class = "color-td-prac form-control form-filter"/></td>
                                                    <td><input type = "text" ng-model = "searchTable.sucursal" class = "color-td-prac form-control form-filter"/></td>
                                                    <td>
                                                        <select name="prioridad" id="prioridad" ng-model = "searchTable.niv_prioridad" class = "color-td-pract form-control form-filter">
                                                            <option value="">TODOS</option>
                                                            <option class="bg-green-jungle" value="BAJO">BAJO</option>
                                                            <option class="bg-yellow-saffron" value="MEDIO">MEDIO</option>
                                                            <option class="bg-red-intense" value="ALTO">ALTO</option>
                                                        </select>
                                                    </td>
                                                    <td><input type = "text" ng-model = "searchTable.fecha_recepcion" class = "color-td-prac form-control form-filter"/></td>
                                                    <td><input type = "text" ng-model = "searchTable.receptor" class = "color-td-prac form-control form-filter"/></td>
                                                    <td><input type = "text" ng-model = "searchTable.fecha_confirmacion" class = "color-td-prac form-control form-filter"/></td>
                                                    <td><input type = "text" ng-model = "searchTable.tipo_trabajo" class = "color-td-prac form-control form-filter"/></td>    
                                                    <td><input type = "text" ng-model = "searchTable.semana" class = "color-td-prac form-control form-filter"/></td>
                                                    <td><input type = "text" ng-model = "searchTable.fecha_programada" class = "color-td-prac form-control form-filter"/></td>
                                                    <td><input type = "text" ng-model = "searchTable.hora" class = "color-td-prac form-control form-filter"/></td>
                                                    <td><input type = "text" ng-model = "searchTable.zona" class = "color-td-prac form-control form-filter"/></td>
                                                    <td><input type = "text" ng-model = "searchTable.mes" class = "color-td-prac form-control form-filter"/></td>
                                                    <td><input type = "text" ng-model = "searchTable.personal" class = "color-td-prac form-control form-filter"/></td>
                                                    <td><input type = "text" ng-model = "searchTable.auxiliar" class = "color-td-prac form-control form-filter"/></td>
                                                    <td>
                                                        <select name="estado" id="estado" class="form-control" required ng-model="searchTable.estado">
                                                            <option value="">TODOS</option>
                                                            <option ng-repeat = "(key, value) in estados | orderBy:'id'" class="{{value.color}}" value = "{{value.label}}">{{value.label}}</option>
                                                        </select>
                                                    </td>
                                                    <td></td>
                                                </tr>                                               
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat = "row in datatable | filter : searchTable | orderBy : orderByField : reverseSort">
                                                    <td class="center-text altura"> <button class="button" ng-click="modal(row.id, row.id_cliente, row.cliente)"><a href=""> {{ row.cliente }} </a></button></td>
                                                    <td class="center-text altura">{{ row.sucursal }}</td>
                                                    <td class="{{ 
                                                                    row.niv_prioridad == 'ALTO' ? 'bg-red-intense' 
                                                                    : row.niv_prioridad == 'MEDIO' 
                                                                    ? 'bg-yellow-saffron' 
                                                                    : 'bg-green-jungle' 
                                                                }} center-text altura" 
                                                       >{{ row.niv_prioridad }}
                                                    </td>
                                                    <td class="center-text altura">{{ row.fecha_recepcion }}</td>
                                                    <td class="center-text altura">{{ row.receptor }}</td>
                                                    <td class="center-text altura">{{ row.fecha_confirmacion }}</td>
                                                    <td class="{{ row.color_trabajo }} center-text altura">{{ row.tipo_trabajo }}</td>
                                                    <td class="center-text altura">{{ row.semana }}</td>
                                                    <td class="center-text altura">{{ row.fecha_programada }}</td>
                                                    <td class="center-text altura">{{ row.hora }}</td>
                                                    <td class="center-text altura">{{ row.zona }}</td>
                                                    <td class="center-text altura">{{ row.mes }}</td>
                                                    <td style="background-color : {{ row.color_personal }}" class="center-text altura {{ row.personal == 'N/A' ? 'color-black' : 'color-white'}}">{{ row.personal }}</td>
                                                    <td style="background-color : {{ row.color_auxiliar }}" class="center-text altura {{ row.auxiliar == 'N/A' ? 'color-black' : 'color-white'}}">{{ row.auxiliar }}</td>
                                                    <td class="center-text altura"> 
                                                        <select ng-change="update(row.id, row.estado)" name="estado" id="estado" class="form-control {{ row.color_estado }}" required ng-model="row.estado">
                                                            <option ng-repeat = "(key, value) in estados | orderBy:'id'" class="{{value.color}}" value="{{value.label}}">{{value.label}}</option>
                                                        </select>
                                                    </td>
                                                    <td class="center-text altura">
                                                        <button class="form-control btn blue" ng-click="show(row.id)"> Editar </button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
    <div id="modal_observaciones" class="modal fade" tabindex="-1" data-width="400">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header green">
                    <button type="buttton" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">OBSERV. DE SEGUIMIENTO AL CLIENTE : {{ nombre_cliente }}</h4>
                </div>
                <div id="modal_body_evaluacion" class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive table-scrollable">
                                <table class="table table-hover table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th style="min-width:100px"> Fecha </th>
                                            <th> Hora </th>
                                            <th> Observacion </th>
                                            <th> Usuario </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat = "row in observaciones | orderBy : 'timestamp' : false">
                                            <td style="min-width:100px">{{ row.fecha }}</td>
                                            <td>{{ row.hora }}</td>
                                            <td>{{ row.observaciones }}</td>
                                            <td>{{ row.usuario }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>                                      
                        <div class="col-md-12 margin-top in-line">
                            <!-- <div class="col-md-6">
                                <label for="observaciones"> Fecha : </label>
                                <div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
                                    <input type="text" class="form-control form-filter input-sm" readonly name="fecha_observacion" placeholder="YY/MM/DD" ng-model="data.fecha_observacion" id="fecha_observacion">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="hora" class="control-label">Hora</label>
                                <div class="input-group">
                                    <div class='input-group date' id='hora_observaciones'> 
                                        <input type='text' class="form-control"/><span class="input-group-addon"><span class="glyphicon glyphicon-time"></span> </span> 
                                    </div>
                                </div>  
                            </div>
                        </div> -->
                        <div class="col-md-12">
                            <label class="control-label" for="observacion"> Observacion : </label>
                            <input class="form-control" name="observacion" id="observacion" ng-model="data.observacion" type="text">
                            <button class="btn blue margin-top" name="agregar" id="btn_agregar" ng-click="insertModal()">Agregar observacion</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>