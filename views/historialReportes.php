<style>
    .textCenter{
        text-align: center !important;
    }
    .margin-bottom{
        margin-bottom : 10px;
    }
</style>
        <link href="http://cdn.procesos-iq.com/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="http://cdn.procesos-iq.com/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        
<!--         <script src="js/echarts.min.js"></script>
        <script src="js/macarons.js"></script> -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper" ng-app="app" ng-controller="reportes">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE BASE CONTENT -->

                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i>Filtros </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse" data-original-title="Expandir/Contraer" title=""> </a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                     <form action="#" class="horizontal-form">
                                        <div class="form-body">
                                            <div class="row">
                                                <div class="hide col-md-4 input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
                                                    <input type="text" class="form-control form-filter input-sm" readonly name="order_date_from" placeholder="Inicio" ng-change="dateChange()" ng-model="filters.fecha_inicio" id="date_from">
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-sm default" type="button">
                                                            <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                                <div class="hide margin-bottom col-md-4 input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
                                                    <input type="text" class="form-control form-filter input-sm" readonly name="order_date_to" placeholder="Fin" ng-change="dateChange()" ng-model="filters.fecha_fin" id="date_to">
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-sm default" type="button">
                                                            <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Tipo Cliente</label>
                                                        <select name="Tipocliente" id="Tipocliente" ng-model="filters.Tipocliente" ng-change="refresh()" class="form-control">
                                                            <option ng-repeat="data in select.Tipoclientes" value="{{data.id}}">{{data.nombre}}</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Tipo de Equipo</label>
                                                        <select name="tEquipo" id="tEquipo" ng-model="filters.tEquipo" ng-change="refresh()" class="form-control">
                                                            <option ng-repeat="data in select.tipoEquipos" value="{{data.id}}">{{data.nombre}}</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Marca</label>
                                                        <select name="marca" id="marca" ng-model="filters.marca" class="form-control">
                                                            <option ng-repeat="data in select.marcas" value="{{data.id}}">{{data.nombre}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Cliente</label>
                                                        <select name="cliente" id="cliente" ng-model="filters.cliente" ng-change="refresh()" class="form-control">
                                                            <option ng-repeat="data in select.clientes" value="{{data.id}}">{{data.nombre}}</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Descripcion de Equipo</label>
                                                        <select name="descripcion" id="descripcion" ng-model="filters.descripcion" ng-change="refresh()" class="form-control">
                                                            <option ng-repeat="data in select.descripcionEquipos" value="{{data.id}}">{{data.nombre}}</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">BTU</label>
                                                        <select name="btu" id="btu" ng-model="filters.btu" class="form-control">
                                                            <option ng-repeat="data in select.btu" value="{{data.id}}">{{data.nombre}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Sucursal</label>
                                                        <select name="sucursal" id="sucursal" ng-model="filters.sucursal" ng-change="refresh()" class="form-control">
                                                            <option ng-repeat="data in select.sucursales" value="{{data.id}}">{{data.nombre}}</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Tipo de Trabajo</label>
                                                        <select name="tTrabajo" id="tTrabajo" ng-model="filters.tTrabajo" ng-change="refresh()" class="form-control">
                                                            <option ng-repeat="data in select.tipoTrabajo" value="{{data.id}}">{{data.nombre}}</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Supervisor</label>
                                                        <select name="supervisor" id="supervisor" ng-model="filters.supervisor" class="form-control">
                                                            <option ng-repeat="data in select.supervisores" value="{{data.id}}">{{data.nombre}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!-- START: BOX TIPO DE CLIENTE -->
                        <div class="col-md-6">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings"></i>Clientes y Equipos</div>
                                    <div class="tools">
                                        <a href="javascript:;" class="expand" data-original-title="" title=""> </a>
                                        <!--
                                        <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
                                        <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
                                        <a href="javascript:;" class="remove" data-original-title="" title=""> </a>
                                        -->
                                    </div>
                                </div>
                                <div class="portlet-body" style="display: none;">
                                    <div class="portlet-body">
                                        <table class="table table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th width="45%"> Cliente </th>
                                                    <th width="10%"> Equipos </th>
                                                </tr>
                                            </thead>
                                        </table>

                                        <div class="panel-group" id="accordion1">
                                            <div class="panel panel-default" ng-repeat="Tclientes in tipoClientes">
                                                <div class="panel-heading">
                                                    <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse"  data-parent="#accordion1" href="#tipoCliente{{Tclientes.id}}" aria-expanded="false"> 
                                                        <h4 class="panel-title">
                                                            <table class="table" class="nobmp-reporte-accordion" style="border: 0; padding: 0; margin: 0;">
                                                                <thead class="nobmp-reporte-accordion">
                                                                    <tr class="nobmp-reporte-accordion">
                                                                        <th width="70%" class="nobmp-reporte-accordion">{{Tclientes.nombre}}</th>
                                                                        <th width="10%" class="nobmp-reporte-accordion rigth"> {{Tclientes.total}}</th>
                                                                    </tr>
                                                                </thead>
                                                            </table>
                                                        </h4>
                                                    </a>
                                                </div>
                                                <div id="tipoCliente{{Tclientes.id}}" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                                    <div class="panel-body">
                                                        <div class="panel panel-default" ng-repeat="cliente in Tclientes.clientes">
                                                            <div class="panel-heading">
                                                                <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse"  data-parent="#accordion3" href="#tipo{{cliente.id}}" aria-expanded="false"> 
                                                                    <h4 class="panel-title">
                                                                        <table class="table" class="nobmp-reporte-accordion" style="border: 0; padding: 0; margin: 0;">
                                                                            <thead class="nobmp-reporte-accordion">
                                                                                <tr class="nobmp-reporte-accordion">
                                                                                    <th width="70%" class="nobmp-reporte-accordion">{{cliente.nombre}}</th>
                                                                                    <th width="10%" class="nobmp-reporte-accordion rigth"> {{cliente.total}}</th>
                                                                                </tr>
                                                                            </thead>
                                                                        </table>
                                                                    </h4>
                                                                </a>
                                                            </div>
                                                            <div id="tipo{{cliente.id}}" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                                                <div class="panel-body">
                                                                    <table class="table" class="nobmp-reporte-accordion" style="border: 0; padding: 0; margin: 0;">
                                                                        <thead class="nobmp-reporte-accordion">
                                                                            <tr class="" ng-repeat="sucursal in cliente.sucursales">
                                                                                <th width="50%"><a ng-click="verSucursal(cliente, sucursal)"> {{sucursal.nombre_contacto}} </a> </th>
                                                                                <th width="10%">{{sucursal.total_equipos}}</th>
                                                                            </tr>
                                                                        </thead>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END: BOX TIPO DE CLIENTE -->
                        <!-- START: BOX TIPO DE EQUIPO -->
                        <div class="col-md-6">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings"></i>{{ selectCliente.nombre }}.{{ selectSucursal.nombre_contacto }}</div>
                                    <div class="tools">
                                        <a href="javascript:;" class="expand" data-original-title="" title=""> </a>
                                    </div>
                                </div>
                                <div class="portlet-body" style="display: none">
                                    <div class="portlet-body">

                                        <table class="table table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th width="45%"> T. Equipo </th>
                                                    <th width="25%"> Equipos </th>
                                                    <th width="5%">&nbsp;</th>
                                                </tr>
                                            </thead>
                                        </table>


                                        <div class="panel-group accordion" id="accordion4">
                                            <div class="panel panel-default" ng-repeat="tipo in tiposEquipos">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion4" href="#equipo{{tipo.id}}" aria-expanded="false"> 
                                                            <table class="table" class="nobmp-reporte-accordion" style="border: 0; padding: 0; margin: 0;">
                                                                <thead class="nobmp-reporte-accordion">
                                                                    <tr class="nobmp-reporte-accordion">
                                                                        <th width="70%" class="nobmp-reporte-accordion"> {{tipo.nombre}}</th>
                                                                        <th width="30%" class="nobmp-reporte-accordion"> {{tipo.equipos.length}} </th>
                                                                        <th width="0%" class="nobmp-reporte-accordion">  </th>
                                                                    </tr>
                                                            </thead>
                                                            </table>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="equipo{{tipo.id}}" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                                    <div class="panel-body">
                                                        <table class="table" class="nobmp-reporte-accordion" style="border: 0; padding: 0; margin: 0;">
                                                            <thead class="">
                                                                <tr class="">
                                                                    <th width="20%" class="nobmp-reporte-accordion" style="text-align: center">Código</th>
                                                                    <th width="50%" class="nobmp-reporte-accordion" style="text-align: center">Descripción</th>
                                                                    <th width="30%" class="nobmp-reporte-accordion" style="text-align: center">Marca</th>
                                                                    <th width="20%" class="nobmp-reporte-accordion" style="text-align: center">BTU</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody class="">
                                                            <tr class="nobmp-reporte-accordion" ng-repeat="equipo in tipo.equipos">
                                                                    <th width="20%" class="textCenter"><a ng-click="verHistorico(equipo)"> {{equipo.codigo}} </a></th>
                                                                    <th width="50%" class="textCenter"> {{equipo.nombre}} </th>
                                                                    <th width="30%" class="textCenter"> {{equipo.marca}} </th>
                                                                    <th width="20%" class="textCenter"> {{equipo.capacidad | number : 0}} </th>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END: BOX TIPO DE EQUIPO -->
                    </div>
                    <div class="row">
                        <!-- START: GRAFICA TIPO DE CLIENTE -->
                        <div class="col-md-12">
                            <div class="portlet light">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings"></i>
                                        <span class="caption-subject"> {{ selectCliente.nombre }}.{{ selectSucursal.nombre_contacto }}.{{selectCodigo.codigo}}</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-container">
                                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                                            <thead>
                                                <tr role="row" class="heading">
                                                    <th width="2%"></th>
                                                    <th width="10%"> Fecha </th>
                                                    <th width="15%"> Orden Trabajo </th>
                                                    <th width="10%"> Código </th>
                                                    <th width="33%"> Tipo Trabajo </th>
                                                    <th width="17%"> Supervisor </th>
                                                    <th width="10%"> Estado </th>
                                                    <th width="10%"> Acciones </th>
                                                </tr>
                                                <!-- <tr role="row" class="filter">
                                                    <td><input type="text" class="form-control form-filter input-sm" name="order_id"> </td>
                                                    <td>
                                                        <div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy/mm/dd">
                                                            <input type="text" class="form-control form-filter input-sm" readonly name="order_date_from" placeholder="From">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-sm default" type="button">
                                                                    <i class="fa fa-calendar"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                        <div class="input-group date date-picker" data-date-format="yyyy/mm/dd">
                                                            <input type="text" class="form-control form-filter input-sm" readonly name="order_date_to" placeholder="To">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-sm default" type="button">
                                                                    <i class="fa fa-calendar"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control form-filter input-sm" name="orden_trabajo"> 
                                                    </td>
                                                    <td> 
                                                        <input type="text" class="form-control form-filter input-sm" name="tipo_trabajo"> 
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control form-filter input-sm" name="supervisor"> 
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control form-filter input-sm" name="status"> 
                                                    </td>
                                                    <td>
                                                        <div class="margin-bottom-5">
                                                                <button class="btn btn-sm green btn-outline filter-submit margin-bottom">
                                                                    <i class="fa fa-search"></i></button>
                                                            <button class="btn btn-sm red btn-outline filter-cancel">
                                                                <i class="fa fa-times"></i></button>
                                                        </div>
                                                    </td>
                                                </tr> -->
                                            </thead>
                                            <tbody> </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END: GRAFICA TIPO DE CLIENTE -->
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
