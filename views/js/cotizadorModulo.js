app.controller('cotizadorController', ['$scope','$http','client','$controller', function($scope,$http,client, $controller){
//PETICION EQUIPOS
    $scope.detalleEquipoR = function(callback, params){
        if(id > 1){
            client.post("./controllers/index.php?accion=CotizadorModulo.detalleEquipoR" , function(r, b){
                b();
                location.href="http://cegaservices2.procesos-iq.com/cotizadorModulo?id="+r;
            } , params);
        }
    }

    $scope.peticionTipoEquipoR = function(callback, params){
        client.post("./controllers/index.php?accion=CotizadorModulo.tipoEquipoR" , function(d, r){			
            r();
            callback(d)
        } , params);
    }

    $scope.peticionCapacidadEquipoR = function(callback, params){
        client.post("./controllers/index.php?accion=CotizadorModulo.capacidadEquipoR" , function(d, r){			
            r();
            callback(d)
        } , params);
    }

    $scope.peticionPrecioEquipoR = function(callback, params){
        client.post("./controllers/index.php?accion=CotizadorModulo.precioUnitEquipoR" , function(d, r){			
            r();
            callback(d)
        } , params);
    }

    $scope.peticionUnidadEquipoR = function(callback, params){
        client.post("./controllers/index.php?accion=CotizadorModulo.unidadEquipoR" , function(d, r){			
            r();
            callback(d)
        } , params);
    }

    $scope.insertEquipoR = function(callback, params){
        client.post("./controllers/index.php?accion=CotizadorModulo.insertEquipoR" , function(d, r){			
            r();
            callback(d)
        } , params);
    }

    $scope.selectEquipoR = function(callback, params){
        client.post("./controllers/index.php?accion=CotizadorModulo.detalleEquipoR" , function(d, r){			
            r();
            callback(d)
        } , params);
    }

    $scope.deleteEquipoR = function(callback, params){
        client.post("./controllers/index.php?accion=CotizadorModulo.deleteEquipoR" , function(d, r){			
            r();
            callback(d)
        } , params);
    }
//END EQUIPOS

//PETICIONES MATERIALES
    $scope.detalle = function(callback, params){
        if(id > 1){
            client.post("./controllers/index.php?accion=CotizadorModulo.detalle" , function(r, b){
                b();
                location.href="http://cegaservices2.procesos-iq.com/cotizadorModulo?id="+r;
            } , params);
        }
    }

    $scope.peticionTipoTrabajo = function(callback, params){
        client.post("./controllers/index.php?accion=CotizadorModulo.tipoTrabajo" , function(d, r){			
            r();
            callback(d)
        } , params);
    }

    $scope.peticionCapacidad = function(callback, params){
        client.post("./controllers/index.php?accion=CotizadorModulo.capacidad" , function(d, r){			
            r();
            callback(d)
        } , params);
    }

    $scope.peticionPrecio = function(callback, params){
        client.post("./controllers/index.php?accion=CotizadorModulo.precioUnit" , function(d, r){			
            r();
            callback(d)
        } , params);
    }

    $scope.peticionUnidad = function(callback, params){
        client.post("./controllers/index.php?accion=CotizadorModulo.unidad" , function(d, r){			
            r();
            callback(d)
        } , params);
    }

    $scope.insertMateriales = function(callback, params){
        client.post("./controllers/index.php?accion=CotizadorModulo.insertMateriales" , function(d, r){			
            r();
            callback(d)
        } , params);
    }

    $scope.selectMateriales = function(callback, params){
        client.post("./controllers/index.php?accion=CotizadorModulo.detalle" , function(d, r){			
            r();
            callback(d)
        } , params);
    }

    $scope.deleteMateriales = function(callback, params){
        client.post("./controllers/index.php?accion=CotizadorModulo.deleteMateriales" , function(d, r){			
            r();
            callback(d)
        } , params);
    }
//END PETICIONES MATERIALES

//PETICIONES REPUESTOS 
    $scope.detalleEquipos = function(callback, params){
        if(id > 1){
            client.post("./controllers/index.php?accion=CotizadorModulo.detalleEquipos" , function(r, b){
                b();
                location.href="http://cegaservices2.procesos-iq.com/cotizadorModulo?id="+r;
            } , params);
        }
    }

    $scope.peticionTipoEquipos = function(callback, params){
        client.post("./controllers/index.php?accion=CotizadorModulo.tipoEquipos" , function(d, r){			
            r();
            callback(d)
        } , params);
    }

    $scope.peticionDescripcionEquipos = function(callback, params){
        client.post("./controllers/index.php?accion=CotizadorModulo.descripcionEquipos" , function(d, r){			
            r();
            callback(d)
        } , params);
    }

    $scope.peticionParteEquipos = function(callback, params){
        client.post("./controllers/index.php?accion=CotizadorModulo.parteEquipos" , function(d, r){			
            r();
            callback(d)
        } , params);
    }

    $scope.peticionPiezaEquipos = function(callback, params){
        client.post("./controllers/index.php?accion=CotizadorModulo.piezaEquipos" , function(d, r){			
            r();
            callback(d)
        } , params);
    }

    $scope.peticionPrecioEquipos = function(callback, params){
        client.post("./controllers/index.php?accion=CotizadorModulo.precioUnitEquipos" , function(d, r){			
            r();
            callback(d)
        } , params);
    }

    $scope.peticionUnidadEquipos = function(callback, params){
        client.post("./controllers/index.php?accion=CotizadorModulo.unidadEquipos" , function(d, r){			
            r();
            callback(d)
        } , params);
    }

    $scope.insertRepuestos = function(callback, params){
        client.post("./controllers/index.php?accion=CotizadorModulo.insertRepuestos" , function(d, r){			
            r();
            callback(d)
        } , params);
    }

    $scope.selectRepuestos = function(callback, params){
        client.post("./controllers/index.php?accion=CotizadorModulo.detalleRepuestos" , function(d, r){			
            r();
            callback(d)
        } , params);
    }

    $scope.deleteRepuestos = function(callback, params){
        client.post("./controllers/index.php?accion=CotizadorModulo.deleteRepuestos" , function(d, r){			
            r();
            callback(d)
        } , params);
    }
// END PETICIONES REPUESTOS

//PETICIONES MANO DE OBRA
    $scope.detalleMano = function(callback, params){
        if(id > 1){
            client.post("./controllers/index.php?accion=CotizadorModulo.detalleMano" , function(r, b){
                b();
                location.href="http://cegaservices2.procesos-iq.com/cotizadorModulo?id="+r;
            } , params);
        }
    }

    $scope.peticionAreaMano = function(callback, params){
        client.post("./controllers/index.php?accion=CotizadorModulo.areaMano" , function(d, r){			
            r();
            callback(d)
        } , params);
    }

    $scope.peticionDescripcionMano = function(callback, params){
        client.post("./controllers/index.php?accion=CotizadorModulo.descripcionMano" , function(d, r){			
            r();
            callback(d)
        } , params);
    }

    $scope.peticionPrecioMano = function(callback, params){
        client.post("./controllers/index.php?accion=CotizadorModulo.precioUnitMano" , function(d, r){			
            r();
            callback(d)
        } , params);
    }
    $scope.peticionUnidadMano = function(callback, params){
        client.post("./controllers/index.php?accion=CotizadorModulo.unidadMano" , function(d, r){			
            r();
            callback(d)
        } , params);
    }

    $scope.insertMano = function(callback, params){
        client.post("./controllers/index.php?accion=CotizadorModulo.insertMano" , function(d, r){			
            r();
            callback(d)
        } , params);
    }

    $scope.selectMano = function(callback, params){
        client.post("./controllers/index.php?accion=CotizadorModulo.detalleMano" , function(d, r){			
            r();
            callback(d)
        } , params);
    }

    $scope.deleteMano = function(callback, params){
        client.post("./controllers/index.php?accion=CotizadorModulo.deleteMano" , function(d, r){			
            r();
            callback(d)
        } , params);
    }
// END PETICIONES MANO DE OBRA

$scope.getAreas = function(callback, params){
    client.post("./controllers/index.php?accion=CotizadorModulo.getAreas" , function(d, r){			
        r();
        callback(d)
    } , params);
}

$scope.getTags = function(callback, params){
    client.post("./controllers/index.php?accion=CotizadorModulo.getTags" , function(d, r){			
        r();
        callback(d)
    } , params);
}

$scope.maxArea = function(callback, params){
    client.post("./controllers/index.php?accion=CotizadorModulo.maxArea" , function(d, r){			
        r();
        callback(d)
    } , params);
}

    $scope.data = {
        id_cotizacion : "<?= $_GET['c'] ?>",
        nombre_area : "AREA ",
    }
    //VAR EQUIPOS
        $scope.ban_repuestos = false;
        $scope.tipo_equipo_r = [];
        $scope.detalle_equipo_r = [];
        $scope.total_equipo_r = "";
        $scope.filtersEquipoR = {
            tipo_equipo_r : "",
            tipo_equipo_r_id : "",
            capacidad_equipo_r : "",
            precio_equipo_r : "",
            unidad_equipo_r : "",
            cantidad_equipo_r : "",
            id_cotizador : "",
            id_cotizacion : $scope.data.id_cotizacion,
            total_equipo_r : "",
            id_area : ""
        };
    //END VAR EQUIPOS

    //VAR MATERIALES
        $scope.ban_materiales = false;
        $scope.tipo_trabajo = [] ; 
        $scope.capacidad = [] ;
        $scope.precio_unit = [] ;
        $scope.detalle_materiales = [];
        $scope.total_materiales = "";
        $scope.filtersMateriales = {
            tipo_trabajo : "",
            tipo_trabajo_id : "",
            capacidad : "",
            precio : "",
            unidad : "",
            cantidad : "",
            id_cotizador : "",
            id_cotizacion : $scope.data.id_cotizacion,
            id_area : ""
        };
    //END VAR MATERIALES

    //VAR REPUESTOS
        $scope.ban_equipos = false;
        $scope.tipo_equipos = [];
        $scope.descripcion_equipos = [];
        $scope.parte_equipos = [];
        $scope.pieza_equipos = [];
        $scope.precio_equipos = [];
        $scope.unidad_equipos = [];
        $scope.detalle_repuestos = [];
        $scope.total_repuestos = [];
        $scope.filtersEquipos = {
            tipo_equipo_name : "",
            tipo_equipo_id : "",
            descripcion : "",
            descripcion_id : "",
            parte : "",
            parte_id : "",
            pieza : "",
            pieza_id : "",
            precio : "",
            unidad : "",
            cantidad : "",
            id_cotizador : "",
            id_cotizacion : $scope.data.id_cotizacion,
            id_area : ""
        }
    //END REPUESTOS

    //VAR MANO DE OBRA
        $scope.ban_mano_obra = false;
        $scope.area_mano = [] ; 
        $scope.descripcion_mano = [] ;
        $scope.precio_unit_mano = [] ;
        $scope.detalle_mano = [];
        $scope.total_mano = "";
        $scope.filtersMano = {
            area_mano : "",
            area_mano_id : "",
            descripcion : "",
            descripcion_id : "",
            precio : "",
            cantidad : "",
            unidad : "",
            id_cotizador : "",
            id_cotizacion : $scope.data.id_cotizacion,
            id_area : ""
        };
    //END VAR MANO DE OBRA
    $scope.max_area = {}

    $scope.anadirArea = function(){
        if(!$scope.data.nombre_area){
            alert("FAVOR DE AÑADIRLE UN NOMBRE AL AREA")
        } else {
            $scope.filtersEquipoR.nombre_area = $scope.data.nombre_area
            $scope.filtersMateriales.nombre_area = $scope.data.nombre_area
            $scope.filtersEquipos.nombre_area = $scope.data.nombre_area
            $scope.filtersMano.nombre_area = $scope.data.nombre_area
            if($scope.areas.length > 0) {
                m_area = parseFloat($scope.areas[$scope.areas.length -1].id) + 1
            } else {
                m_area = 1
            }
            $scope.areas.push({"id" : m_area, "area" : $scope.data.nombre_area, "total" : 0.00})
            $scope.data.nombre_area = "AREA "
        } 
    }

    $scope.changeArea = function(id_area){
        $scope.filtersEquipoR.id_area = id_area
        $scope.filtersMateriales.id_area = id_area
        $scope.filtersEquipos.id_area = id_area
        $scope.filtersMano.id_area = id_area
        $scope.selectEquipoR(r => {
            if(r){
                $scope.detalle_equipo_r = r.detalle_equipo_r;
                $scope.total_equipo_r = r.sum_total;
            }
        },$scope.filtersEquipoR)
        $scope.selectMateriales(r => {
            if(r){
                $scope.detalle_materiales = r.detalle_materiales;
                $scope.total_materiales = r.sum_total;
            }
        },$scope.filtersMateriales)
        $scope.selectRepuestos(r => {
            if(r){
                $scope.detalle_repuestos = r.detalle_repuestos;
                $scope.total_repuestos = r.sum_total;
            }
        },$scope.filtersEquipos)
        $scope.selectMano(r => {
            if(r){
                $scope.detalle_mano = r.detalle_mano;
                $scope.total_mano = r.sum_total;
            }
        },$scope.filtersMano)
    }
    
    $scope.init = {
        equipoR : function(){
            $scope.selectEquipoR(r => {
                if(r){
                    $scope.detalle_equipo_r = r.detalle_equipo_r;
                    $scope.total_equipo_r = r.sum_total;
                }
            },$scope.filtersEquipoR)
        },
        materiales : function(){
            $scope.selectMateriales(r => {
                if(r){
                    $scope.detalle_materiales = r.detalle_materiales;
                    $scope.total_materiales = r.sum_total;
                }
            },$scope.filtersMateriales)
        },
        repuestos : function(){
            $scope.selectRepuestos(r => {
                if(r){
                    $scope.detalle_repuestos = r.detalle_repuestos;
                    $scope.total_repuestos = r.sum_total;
                }
            },$scope.filtersEquipos)
        },
        mano : function(){
            $scope.selectMano(r => {
                if(r){
                    $scope.detalle_mano = r.detalle_mano;
                    $scope.total_mano = r.sum_total;
                }
            },$scope.filtersMano)
        },
        getAreas : function(){
            $scope.getAreas(r => {
                if(r){
                    $scope.areas = r.cat_areas;
                }
            },$scope.data)
        },
        tags : function(){
            $scope.getTags(r => {
                if(r){
                    $scope.tags = r.tags;
                    console.log($scope.tags)
                    setTimeout (function(){
                        $('#tags_indicators').delay(200).fadeIn();
                        $(".counter_tags").counterUp({
                            delay: 10,
                            time: 1000
                        });
                    }, 500);
                }
            },$scope.data)
        },
        all : function(){
            this.getAreas();
            this.tags();
        }
    }
    
    
    $scope.request = {
        carga_pestana_uno : function(id_area){
            if(!$scope.ban_equipos){
                $scope.ban_equipos = true;

                $scope.peticionTipoEquipos(r => {
                    if(r){
                        $scope.tipo_equipos= r.tipo_equipos;
                        //$scope.filtersEquipos.tipo_equipo_name = $scope.tipo_equipos[Object.keys($scope.tipo_equipos)[0]];
                        
                        $scope.peticionDescripcionEquipos(r => {
                            if(r){
                                $scope.descripcion_equipos = r.descripcion_equipos;
                                //$scope.filtersEquipos.descripcion = $scope.descripcion_equipos[Object.keys($scope.descripcion_equipos)[0]];
                
                                $scope.peticionParteEquipos(r => {
                                    if(r){
                                        $scope.parte_equipos = r.parte_equipos;
                                        //$scope.filtersEquipos.parte = $scope.parte_equipos[Object.keys($scope.parte_equipos)[0]];
            
                                        $scope.peticionPiezaEquipos(r => {
                                            if(r){
                                                $scope.pieza_equipos = r.pieza_equipos;
                                                //scope.filtersEquipos.pieza = $scope.pieza_equipos[Object.keys($scope.pieza_equipos)[0]];
                                            }
                                        },$scope.filtersEquipos)
                                    }
                                },$scope.filtersEquipos)
                            }
                        },$scope.filtersEquipos)
                    }
                },$scope.filtersEquipos)
            }
        },
        carga_pestana_dos : function(id_area){
            $scope.filtersMateriales.id_area = id_area
            if(!$scope.ban_materiales){
                $scope.ban_materiales = true;
                $scope.peticionTipoTrabajo(r => {
                    if(r){
                        $scope.tipo_trabajo = r.tipo_trabajo;
                        //$scope.filtersMateriales.tipo_trabajo = $scope.tipo_trabajo[Object.keys($scope.tipo_trabajo)[0]];
                        
                        $scope.peticionCapacidad(r => {
                            if(r){
                                $scope.capacidad = r.capacidad;
                            }
                        },$scope.filtersMateriales)
                    }
                },$scope.filtersMateriales)    
            } 
        },
        carga_pestana_tres : function(id_area){
            $scope.filtersEquipoR.id_area = id_area
            if(!$scope.ban_repuestos){
                $scope.ban_repuestos = true;

                $scope.peticionTipoEquipoR(r => {
                    if(r){
                        $scope.tipo_equipo_r = r.tipo_equipo_r;
                    }
                },$scope.filtersEquipoR)
            }  
        },
        carga_pestana_cuatro : function(id_area){
            $scope.filtersMano.id_area = id_area
            if(!$scope.ban_mano_obra){
                $scope.ban_mano_obra = true;
                
                $scope.peticionAreaMano(r => {
                    if(r){
                        $scope.area_mano = r.area_mano;
                        //$scope.filtersMano.area_mano = $scope.area_mano[Object.keys($scope.area_mano)[0]];
                        
                        $scope.peticionDescripcionMano(r => {
                            if(r){
                                $scope.descripcion_mano = r.descripcion_mano;
                                //$scope.filtersMano.descripcion = $scope.descripcion_mano[Object.keys($scope.descripcion_mano)[0]];
                            }
                        },$scope.filtersMano)
                    }
                },$scope.filtersMano)  
            }
        }        
    }
    
    $scope.equiposR = {
        limpiar : function(){
            $scope.filtersEquipoR.tipo_equipo_r_id = "";
            $scope.filtersEquipoR.capacidad_equipo_r = "";
            $scope.filtersEquipoR.unidad_equipo_r = "";
            $scope.filtersEquipoR.cantidad_equipo_r = "";
            $scope.filtersEquipoR.precio_equipo_r = "";
        },
        cargaEquiposR : function(){
            $scope.peticionCapacidadEquipoR(r => {
                if(r){
                    $scope.filtersEquipoR.capacidad_equipo_r = r.capacidad;
                }
            },$scope.filtersEquipoR)
            $scope.peticionPrecioEquipoR(r => {
                if(r){
                    $scope.filtersEquipoR.precio_equipo_r = r.precio;
                }
            },$scope.filtersEquipoR)
            $scope.peticionUnidadEquipoR(r => {
                if(r){
                    $scope.filtersEquipoR.unidad_equipo_r = r.unidad;
                }
            },$scope.filtersEquipoR)
        },
        insertEquiposR : function(){
            if($scope.filtersEquipoR.cantidad_equipo_r != '' && $scope.filtersEquipoR.cantidad_equipo_r != 0){
                if($scope.filtersEquipoR.precio_equipo_r != '' && $scope.filtersEquipoR.precio_equipo_r != 0){
                    $scope.insertEquipoR(r => {
                        if(r){
                            console.log("INFORMACION INSERTADA CORRECTAMENTE");
                            $scope.getAreas(r => {
                                if(r){
                                    $scope.areas = r.cat_areas
                                    this.selectEquiposR();
                                    this.limpiar();
                                    $scope.init.equiposR();
                                }
                            },$scope.data)
                        }
                    },$scope.filtersEquipoR)
                } else {
                    alert("Favor de agregar un Precio valido");
                }
            } else {
                alert("Favor de agregar una cantidad valida");
            }
        },
        selectEquiposR : function(){
            $scope.selectEquipoR(r => {
                if(r){
                    $scope.detalle_equipo_r = r.detalle_equipo_r;
                }
            },$scope.filtersEquipoR)
        },
        deleteEquiposR : function(id, nombre){
            if(confirm("¿Seguro que desea quitar el último registro agregado de " + nombre + " ?")){
                $scope.filtersEquipoR.id_cotizador = id;
                $scope.deleteEquipoR(r => {
                    if(r){
                        console.log("INFORMACION BORRADA CORRECTAMENTE");
                        $scope.getAreas(r => {
                            if(r){
                                $scope.areas = r.cat_areas
                                this.selectEquiposR();
                                $scope.init.equiposR();
                            }
                        },$scope.data)
                    }
                },$scope.filtersEquipoR)   
            }
        },
    }
    
    $scope.materiales = {
        limpiar : function(){
            $scope.filtersMateriales.tipo_trabajo_id = "";
            $scope.filtersMateriales.capacidad = "";
            $scope.filtersMateriales.unidad = "";
            $scope.filtersMateriales.cantidad = "";
            $scope.filtersMateriales.precio = "";
        },
        cargaTipoTrabajo : function(){
            $scope.peticionCapacidad(r => {
                if(r){
                    $scope.capacidad = r.capacidad;
                    $scope.filtersMateriales.capacidad = $scope.capacidad[Object.keys($scope.capacidad)[0]];
    
                    $scope.peticionPrecio(r => {
                        if(r){
                            $scope.filtersMateriales.precio = r.precio;
                        }
                    },$scope.filtersMateriales)
                    $scope.peticionUnidad(r => {
                        if(r){
                            $scope.filtersMateriales.unidad = r.unidad;
                        }
                    },$scope.filtersMateriales)
                }
            },$scope.filtersMateriales)
        },
        cargaCapacidad : function(){
            $scope.peticionPrecio(r => {
                if(r){
                    $scope.filtersMateriales.precio = r.precio;
                }
            },$scope.filtersMateriales)
            $scope.peticionUnidad(r => {
                if(r){
                    $scope.filtersMateriales.unidad = r.unidad;
                }
            },$scope.filtersMateriales)
        },
        insertMateriales : function(){
            if($scope.filtersMateriales.cantidad != '' && $scope.filtersMateriales.cantidad != 0){
                if($scope.filtersMateriales.precio != '' && $scope.filtersMateriales.precio != 0){
                    $scope.insertMateriales(r => {
                        if(r){
                            console.log("INFORMACION INSERTADA CORRECTAMENTE");
                            $scope.getAreas(r => {
                                if(r){
                                    $scope.areas = r.cat_areas
                                    this.selectMateriales();
                                    this.limpiar();
                                    $scope.init.materiales();
                                }
                            },$scope.data)
                        }
                    },$scope.filtersMateriales)
                } else {
                    alert("Favor de agregar un Precio valido");
                }
            } else {
                alert("Favor de agregar una cantidad valida");
            }
        },
        selectMateriales : function(){
            $scope.selectMateriales(r => {
                if(r){
                    $scope.detalle_materiales = r.detalle_materiales;
                }
            },$scope.filtersMateriales)
        },
        deleteMateriales : function(id, nombre){
            if(confirm("¿Seguro que desea quitar el último registro agregado de " + nombre + " ?")){
                $scope.filtersMateriales.id_cotizador = id;
                $scope.deleteMateriales(r => {
                    if(r){
                        console.log("INFORMACION BORRADA CORRECTAMENTE");
                        $scope.getAreas(r => {
                            if(r){
                                $scope.areas = r.cat_areas
                                this.selectMateriales();
                                $scope.init.materiales();
                            }
                        },$scope.data)
                    }
                },$scope.filtersMateriales)   
            }
        } 
    }
    //repuestos
    $scope.equipos = {
        limpiar : function(){
            $scope.filtersEquipos.tipo_equipo_id = "";
            $scope.filtersEquipos.descripcion_id = "";
            $scope.filtersEquipos.parte_id = "";
            $scope.filtersEquipos.pieza_id = "";
            $scope.filtersEquipos.capacidad = "";
            $scope.filtersEquipos.unidad = "";
            $scope.filtersEquipos.cantidad = "";
            $scope.filtersEquipos.precio = "";
        },
        cargaTipoEquipos : function(){
            $scope.peticionDescripcionEquipos(r => {
                if(r){
                    $scope.filtersEquipos.descripcion_id = Object.keys(r.descripcion_equipos)[0];
                    $scope.descripcion_equipos = r.descripcion_equipos;

                    $scope.peticionParteEquipos(r => {
                        if(r){
                            $scope.filtersEquipos.parte_id = Object.keys(r.parte_equipos)[0];
                            $scope.parte_equipos = r.parte_equipos;
            
                            $scope.peticionPiezaEquipos(r => {
                                if(r){
                                    $scope.filtersEquipos.pieza_id = Object.keys(r.pieza_equipos)[0];
                                    $scope.pieza_equipos = r.pieza_equipos;
                                }
                            },$scope.filtersEquipos)
                        }
                    },$scope.filtersEquipos)
                }
            },$scope.filtersEquipos)
        },
        cargaPorDescripcion : function() {
            $scope.peticionParteEquipos(r => {
                if(r){
                    $scope.filtersEquipos.parte_id = Object.keys(r.parte_equipos)[0];
                    $scope.parte_equipos = r.parte_equipos;

                    $scope.peticionPiezaEquipos(r => {
                        if(r){
                            $scope.filtersEquipos.pieza_id = Object.keys(r.pieza_equipos)[0];
                            $scope.pieza_equipos = r.pieza_equipos;
                        }
                    },$scope.filtersEquipos)
                }
            },$scope.filtersEquipos)
        },
        cargaPorPartes : function() {
            $scope.peticionPiezaEquipos(r => {
                if(r){
                    $scope.filtersEquipos.pieza_id = Object.keys(r.pieza_equipos)[0];
                    $scope.pieza_equipos = r.pieza_equipos;
                }
            },$scope.filtersEquipos)
        },
        cargaPorPiezas : function() {
        },
        insertRepuestos: function(){
            if($scope.filtersEquipos.cantidad != '' && $scope.filtersEquipos.cantidad != 0){
                if($scope.filtersEquipos.precio != '' && $scope.filtersEquipos.precio != 0){
                    $scope.insertRepuestos(r => {
                        if(r){
                            console.log("INFORMACION INSERTADA CORRECTAMENTE");
                            $scope.getAreas(r => {
                                if(r){
                                    $scope.areas = r.cat_areas
                                    this.selectRepuestos();
                                    this.limpiar();
                                    $scope.init.repuestos();
                                }
                            },$scope.data)
                        }
                    },$scope.filtersEquipos) 
                } else {
                    alert("Favor de agregar un Precio valido");
                }
            } else {
                alert("Favor de agregar una cantidad valida");
            }
        },
        selectRepuestos : function(){
            $scope.selectRepuestos(r => {
                if(r){
                    $scope.detalle_repuestos = r.detalle_repuestos;
                }
            },$scope.filtersEquipos)
        },
        deleteRepuestos : function(id, nombre){
            if(confirm("¿Seguro que desea quitar el último registro agregado de " + nombre + "?")){
                $scope.filtersEquipos.id_cotizador = id;
                $scope.deleteRepuestos(r => {
                    if(r){
                        console.log("INFORMACION BORRADA CORRECTAMENTE");
                        $scope.getAreas(r => {
                            if(r){
                                $scope.areas = r.cat_areas
                                this.selectRepuestos();
                                $scope.init.repuestos();
                            }
                        },$scope.data)
                    }
                },$scope.filtersEquipos)   
            }
        }
    }

    $scope.mano = {
        limpiar : function(){
            $scope.filtersMano.area_mano_id = "";
            $scope.filtersMano.descripcion_id = "";
            $scope.filtersMano.capacidad = "";
            $scope.filtersMano.unidad = "";
            $scope.filtersMano.cantidad = "";
            $scope.filtersMano.precio = "";
        },
        cargaAreaMano : function(){
            $scope.peticionDescripcionMano(r => {
                if(r){
                    $scope.filtersMano.descripcion_id = Object.keys(r.descripcion_mano)[0]
                    $scope.descripcion_mano = r.descripcion_mano;

                    $scope.peticionPrecioMano(r => {
                        if(r){
                            $scope.filtersMano.precio = r.precio;
                        }
                    },$scope.filtersMano)
                    $scope.peticionUnidadMano(r => {
                        if(r){
                            $scope.filtersMano.unidad = r.unidad;
                        }
                    },$scope.filtersMano)
                }
            },$scope.filtersMano)
        },
        cargaDescripcionMano : function(){
            $scope.peticionPrecioMano(r => {
                if(r){
                    $scope.filtersMano.precio = r.precio;
                }
            },$scope.filtersMano)
            $scope.peticionUnidadMano(r => {
                if(r){
                    $scope.filtersMano.unidad = r.unidad;
                }
            },$scope.filtersMano)
        },
        insertMano: function(){
            if($scope.filtersMano.cantidad != '' && $scope.filtersMano.cantidad != 0){
                if($scope.filtersMano.precio != '' && $scope.filtersMano.precio != 0){
                    $scope.insertMano(r => {
                        if(r){
                            console.log("INFORMACION INSERTADA CORRECTAMENTE");
                            $scope.getAreas(r => {
                                if(r){
                                    $scope.areas = r.cat_areas
                                    this.selectMano();
                                    this.limpiar();
                                    $scope.init.mano();
                                }
                            },$scope.data)
                        }
                    },$scope.filtersMano)
                } else {
                    alert("Favor de agregar un Precio valido");
                }
            } else {
                alert("Favor de agregar una cantidad valida");
            }
        },
        selectMano : function(){
            $scope.selectMano(r => {
                if(r){
                    $scope.detalle_mano = r.detalle_mano;
                }
            },$scope.filtersMano)
        },
        deleteMano: function(id, nombre){
            if(confirm("¿Seguro que desea quitar el último registro agregado de " + nombre + "?")){
                $scope.filtersMano.id_cotizador = id;
                $scope.deleteMano(r => {
                    if(r){
                        console.log("INFORMACION BORRADA CORRECTAMENTE");
                        $scope.getAreas(r => {
                            if(r){
                                $scope.areas = r.cat_areas
                                this.selectMano();
                                $scope.init.mano();
                            }
                        },$scope.data)
                    }
                },$scope.filtersMano)   
            }
        }  
    }

    $scope.init.all()

}]);