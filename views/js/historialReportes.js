var Services = angular.module('App.Services', [])
    .config(function ($httpProvider){
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
        $httpProvider.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
        $httpProvider.defaults.transformRequest = function(obj) {
            var str = [];
            for(var p in obj){
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            }
            return str.join("&");
        };
    });


app.controller('reportes', ['$scope','$http','$interval','client','$controller', function($scope,$http,$interval,client, $controller){

    $scope.filters = {
        Tipocliente : "",
        cliente : "",
        tEquipo : "",
        marca : "",
        sucursal : "",
        descripcion : "",
        btu : "",
        tTrabajo : "",
        supervisor : "",
        fecha_inicio : "",
        fecha_fin : ""
    }

    $scope.select = {
        Tipoclientes : [],
        clientes : [],
        sucursales : [],
        tipoEquipos : [],
        descripcionEquipos : [],
        tipoTrabajo : [],
        marcas : [],
        btu : [],
        supervisores : [],
    }

    var initPickers = function () {
        //init date pickers
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            autoclose: true
        });
    }

	$scope.init = function(){
        initPickers()
		client.post("./controllers/index.php?accion=Reportes.getClientesEquipos" , function(r, b){
            b();
            $scope.tipoClientes = r
            console.log(r)
        } , {});

        client.post("./controllers/index.php?accion=Reportes.getFilters" , function(r, b){
			b();
			$scope.select = r
		} , {});
    }
    
    $scope.dateChange = function(){
        if($scope.filters.fecha_incio != "" && $scope.filters.fecha_fin != ""){
            $("[name=from_date]").val($scope.filters.fecha_incio)
            $("[name=to_date]").val($scope.filters.fecha_fin)
        }
    }

    $scope.selectCliente = "";
    $scope.selectSucursal = "";
    $scope.selectCodigo = "";

    // var getParams = 

    $scope.verHistorico = function(equipo){
        $scope.selectCodigo = equipo;
        if($scope.selectCodigo != ""){
            table.ajax.reload();
        }
    }

    $scope.verSucursal = function(cliente, sucursal){
        $scope.selectCliente = cliente
        $scope.selectSucursal = sucursal

        $scope.filters.cliente = cliente.id
        if(sucursal.hasOwnProperty("not_sucursal")){
            $scope.filters.sucursal = -1;
        }else{
            $scope.filters.sucursal = sucursal.id
        }

        client.post("./controllers/index.php?accion=Reportes.getEquiposTipos", function(r, b){
            b()
            $scope.tiposEquipos = r;
        }, $scope.filters)

        $scope.selectCodigo = "";
        $scope.filters.codigo = "";
        table.ajax.reload();
    }

    $scope.refresh = function(){
        $scope.selectCliente = ""
        $scope.selectSucursal = ""
        $("#datatable_ajax tbody").html("")

		client.post("./controllers/index.php?accion=Reportes.getClientesEquipos" , function(r, b){
            b()
            $scope.tipoClientes = r 
        } , $scope.filters);
        client.post("./controllers/index.php?accion=Reportes.getFilters" , function(r, b){
			b();
			$scope.select = r
		} , $scope.filters);

		if($scope.selectSucursal != "" && $scope.selectCliente != ""){
			client.post("./controllers/index.php?accion=Reportes.getEquiposTipos", function(r, b){
	            b()
	            $scope.tiposEquipos = r;
	        }, $scope.filters)
		}
    }

    var table = $('#datatable_ajax').DataTable( {
        ajax : {
            url : './controllers/index.php?accion=Reportes.historicoEquipos',
            type : "POST",
            data : function(d){
                    var data = {
                        tTrabajo : $scope.filters.tTrabajo,
                        cliente : $scope.selectCliente.id,
                        sucursal : $scope.selectSucursal.id,
                        codigo : $scope.selectCodigo.codigo,
                    };
                    return $.extend( {}, d, data );
                }
        },
        columnDefs : [
            { className: "textCenter", "targets": [ 2 ] },
            { className: "textCenter", "targets": [ 3 ] },
            { className: "textCenter", "targets": [ 6 ] },
        ]
    } );

	$scope.init();

}]);