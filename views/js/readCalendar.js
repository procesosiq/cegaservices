    /*==================================
    =            CALENDARIO            =
    ==================================*/
    var calendar = {
        fullcalendarOrder : $('#calendar_object'),
        config : function(Loadevents){

            if (!jQuery().fullCalendar) {
                throw new Error("No existe el plugin de calendario")
                return;
            }

            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();

            if (App.isRTL()) {
                if (this.fullcalendarOrder.parents(".portlet").width() <= 720) {
                    this.fullcalendarOrder.addClass("mobile");
                    h = {
                        right: 'title, prev, next',
                        center: '',
                        left: 'agendaDay, agendaWeek, month, today'
                    };
                } else {
                    this.fullcalendarOrder.removeClass("mobile");
                    h = {
                        right: 'title',
                        center: '',
                        left: 'agendaDay, agendaWeek, month, today, prev,next'
                    };
                }
            } else {
                if (this.fullcalendarOrder.parents(".portlet").width() <= 720) {
                    this.fullcalendarOrder.addClass("mobile");
                    h = {
                        left: 'title, prev, next',
                        center: '',
                        right: 'today,month,agendaWeek,agendaDay'
                    };
                } else {
                    this.fullcalendarOrder.removeClass("mobile");
                    h = {
                        left: 'title',
                        center: '',
                        right: 'prev,next,today,month,agendaWeek,agendaDay'
                    };
                }
            }

            var events = Loadevents || [{
                    title: 'Click for Google',
                    start: new Date(y, m, 28),
                    end: new Date(y, m, 29),
                    backgroundColor: App.getBrandColor('yellow'),
                    description : "Prueba de Evento",
                    // url: 'http://google.com/',
                }];

            return { //re-initialize the calendar
                header: h,
                defaultView: 'agendaWeek', // change default view with available options from http://arshaw.com/fullcalendar/docs/views/Available_Views/
                slotMinutes: 15,
                lang: 'es',
                editable: true,
				eventDurationEditable:false,
                droppable: true, // this allows things to be dropped onto the calendar !!!
                events: events,
				drop : function(date, allDay) { // this function is called when something is dropped
					
				},
                eventClick: function(calEvent, jsEvent, view) {
                    var msj = 'Evento : ' + calEvent.title + ' <br> Fecha : ' + calEvent.start.format("YYYY-MM-DD HH:mm")
					+ ' <br> Cliente : ' + calEvent.cliente + ' <br> Tipo trabajo : ' + calEvent.tipo_trabajo 
                    + '<br> Vehiculo : ' + ((calEvent.vehiculo)? calEvent.vehiculo :'No Seleccionado')
                    + '<br> Supervisor : ' + calEvent.supervisor
                    + '<br> Auxiliares : ' + calEvent.auxiliares
                    + '<br> Detalle Trabajo : ' + calEvent.observaciones
                    + '<br> Listado Equipos : ' + calEvent.list_equipos
                    + '<br> Listado Materiales : ' + calEvent.list_materiales
                    + '<br> Listado Repuestos : ' + calEvent.list_repuestos
                    + '<br> Listado Herramientas : ' + calEvent.list_herramientas
                    alert(msj, 'Ordenes' , 'success');
                    $(this).css('border-color', 'red');

                },
                eventDrop: function(event, delta, revertFunc) {
					revertFunc();
				}
            }
        },
        init : function(){
            // console.log(this.config());
            var config = this.config();
            this.fullcalendarOrder.fullCalendar('destroy'); // destroy the calendar
            this.fullcalendarOrder.fullCalendar(config);
        },
        reload : function(events){
            this.fullcalendarOrder.fullCalendar('destroy'); // destroy the calendar
            if(events)
                this.fullcalendarOrder.fullCalendar(this.config(events));
            else
                this.fullcalendarOrder.fullCalendar(this.config());
        }
    }
    calendar.init();

    /*=====  End of CALENDARIO  ======*/

	$.fn.dataTable.pipeline = function ( opts ) {
    // Configuration options
    console.log(opts);
    var conf = $.extend( {
        pages: 5,     // number of pages to cache
        url: '',      // script url
        data: null,   // function or object with parameters to send to the server
                      // matching how `ajax.data` works in DataTables
        method: 'POST' // Ajax HTTP method
    }, opts );

    // Private variables for storing the cache
    var cacheLower = -1;
    var cacheUpper = null;
    var cacheLastRequest = null;
    var cacheLastJson = null;

    return function ( request, drawCallback, settings ) {
        var ajax          = false;
        var requestStart  = request.start;
        var drawStart     = request.start;
        var requestLength = request.length;
        var requestEnd    = requestStart + requestLength;

        if ( settings.clearCache ) {
            // API requested that the cache be cleared
            ajax = true;
            settings.clearCache = false;
        }
        else if ( cacheLower < 0 || requestStart < cacheLower || requestEnd > cacheUpper ) {
            // outside cached data - need to make a request
            ajax = true;
        }
        else if ( JSON.stringify( request.order )   !== JSON.stringify( cacheLastRequest.order ) ||
                  JSON.stringify( request.columns ) !== JSON.stringify( cacheLastRequest.columns ) ||
                  JSON.stringify( request.search )  !== JSON.stringify( cacheLastRequest.search )
        ) {
            // properties changed (ordering, columns, searching)
            ajax = true;
        }

        // Store the request for checking next time around
        cacheLastRequest = $.extend( true, {}, request );

        if ( ajax ) {
            // Need data from the server
            if ( requestStart < cacheLower ) {
                requestStart = requestStart - (requestLength*(conf.pages-1));

                if ( requestStart < 0 ) {
                    requestStart = 0;
                }
            }

            cacheLower = requestStart;
            cacheUpper = requestStart + (requestLength * conf.pages);

            request.start = requestStart;
            request.length = requestLength*conf.pages;

            // Provide the same `data` options as DataTables.
            console.log(conf);
            if ( $.isFunction ( conf.data ) ) {
                // As a function it is executed with the data object as an arg
                // for manipulation. If an object is returned, it is used as the
                // data object to submit
                var d = conf.data( request );
                if ( d ) {
                    $.extend( request, d );
                }
            }
            else if ( $.isPlainObject( conf.data ) ) {
                // As an object, the data given extends the default
                $.extend( request, conf.data );
            }

            settings.jqXHR = $.ajax( {
                "type":     conf.method,
                "url":      conf.url,
                "data":     request,
                "dataType": "json",
                "cache":    false,
                "success":  function ( json ) {
                    cacheLastJson = $.extend(true, {}, json);

                    if ( cacheLower != drawStart ) {
                        json.data.splice( 0, drawStart-cacheLower );
                    }
                    if($.isArray( json.data )){
                        json.data.splice( requestLength, json.data.length );
                    }
                    $('#datatable_ajax tbody').off( 'click', 'button');
                    $('#datatable_ajax').off('click', '.filter-submit');
                    $('#datatable_ajax').off('click', '.filter-cancel');
                    drawCallback( json );
                }
            } );
        }
        else {
            json = $.extend( true, {}, cacheLastJson );

            json.draw = request.draw; // Update the echo for each response
            //console.log($.isPlainObject( json.data ));
            //console.log($.isArray( json.data ));
            if($.isArray( json.data )){
                json.data.splice( 0, requestStart-cacheLower );
                json.data.splice( requestLength, json.data.length );
	            $('#datatable_ajax tbody').off( 'click', 'button');
	            $('#datatable_ajax').off('click', '.filter-submit');
	            $('#datatable_ajax').off('click', '.filter-cancel');
                drawCallback(json);
            }
        }
    }
};


// Register an API method that will empty the pipelined data, forcing an Ajax
// fetch on the next draw (i.e. `table.clearPipeline().draw()`)
$.fn.dataTable.Api.register( 'clearPipeline()', function () {
    return this.iterator( 'table', function ( settings ) {
        settings.clearCache = true;
    } );
} );

var printCal = function(responsable){
	var data = {
        grupo : responsable || $("#gruposcalendario :selected").val(),
        tipo : "Supervisor"
    }
    ahttp.post("./controllers/index.php?accion=OrdenesTrabajoNew.calendario" , function(r, b){
		b();
		printCalendar(r,b);
    }, data);
}

// var initDrag = function(el) {
//     // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
//     // it doesn't need to have a start or end
//     var eventObject = {
//         title: $.trim(el.text()) // use the element's text as the event title
//     };
//     // store the Event Object in the DOM element so we can get to it later
//     el.data('eventObject', eventObject);
//     // make the event draggable using jQuery UI
//     el.draggable({
//         zIndex: 999,
//         revert: true, // will cause the event to go back to its
//         revertDuration: 0 //  original position after the drag
//     });
// };


var printCalendar = function(r , b){
    b();
    if(r){
        console.log(r.calendario)
        var events = [] , calendario = {} , fecha ;
        for(var d in r.calendario){
            fecha = moment(r.calendario[d]['fecha_agendada']).format("YYYY-MM-DD HH:mm:ss");
            fecha2 = moment(r.calendario[d]['fecha_fin']).format("YYYY-MM-DD HH:mm:ss");
            
            if(r.calendario[d]['color'] == '#ffffff'){
                r.calendario[d]['color'] = '#000000'
            }
            console.log(r.calendario)
            events.push({
                    title :  r.calendario[d]['id'],
                    tipo_trabajo :  r.calendario[d]['tipo_trabajo'],
                    cliente :  r.calendario[d]['cliente'],
                    start : fecha,
                    end : fecha2,
                    backgroundColor : r.calendario[d]['color'],
                    description : "Orden tipo "+ r.calendario[d]['id']+ " para el Cliente "+r.calendario[d]['fecha_agendada'] ,
                    observaciones : r.calendario[d]['observaciones'],
                    supervisor : r.calendario[d]['supervisor'],
                    auxiliares : r.calendario[d]['auxiliares'],
                    vehiculo : r.calendario[d]['vehiculo'],
                    list_equipos : r.calendario[d]['list_equipos'],
                    list_materiales : r.calendario[d]['list_materiales'],
                    list_repuestos : r.calendario[d]['list_repuestos'],
                    list_herramientas : r.calendario[d]['list_herramientas'],
                });
        }
        calendar.reload(events);
    }
}

/*var eventCalendar = function () {
	$('#event_box').empty();
    ahttp.post("./controllers/index.php?accion=Ordenes.calendarioEvent" , function(r, b){
		b();
		console.log(r.calendario);
		for(var d in r.calendario){
            addEvent(''+r.calendario[d]['id'], r.calendario[d]['id_order']);
		}
    }, {});
}*/

jQuery(document).ready(function() {
});

app.controller('orderController', ['$scope','$http','$interval','$controller' , 'client', function($scope,$http,$interval,$controller ,client){
    
    $scope.formData = {
        areas : [],
        roles : [],
        trabajos_tipos : ['Correctivo' , 'Instalacion' , 'Mantenimiento'],
        id_orden : 0
    };

    $scope.init = function(){
        client.post("./controllers/index.php?accion=Configuracion.getSupervisores", function(r, b){
            b()
            if(r){
                $scope.supervisores = r
                printCal("<?= Session::getInstance()->id_supervisor?>")
            }
        })
    }
}]);
