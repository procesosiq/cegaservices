var valores = [0 , 0 ,0]

//
// Pipelining function for DataTables. To be used to the `ajax` option of DataTables
//
$.fn.dataTable.pipeline = function ( opts ) {
    // Configuration options
    console.log(opts);
    var conf = $.extend( {
        pages: 10,     // number of pages to cache
        url: '',      // script url
        data: null,   // function or object with parameters to send to the server
                      // matching how `ajax.data` works in DataTables
        method: 'POST' // Ajax HTTP method
    }, opts );
 
    // Private variables for storing the cache
    var cacheLower = -1;
    var cacheUpper = null;
    var cacheLastRequest = null;
    var cacheLastJson = null;
 
    return function ( request, drawCallback, settings ) {
        var ajax          = false;
        var requestStart  = request.start;
        var drawStart     = request.start;
        var requestLength = request.length;
        var requestEnd    = requestStart + requestLength;
         
        if ( settings.clearCache ) {
            // API requested that the cache be cleared
            ajax = true;
            settings.clearCache = false;
        }
        else if ( cacheLower < 0 || requestStart < cacheLower || requestEnd > cacheUpper ) {
            // outside cached data - need to make a request
            ajax = true;
        }
        else if ( JSON.stringify( request.order )   !== JSON.stringify( cacheLastRequest.order ) ||
                  JSON.stringify( request.columns ) !== JSON.stringify( cacheLastRequest.columns ) ||
                  JSON.stringify( request.search )  !== JSON.stringify( cacheLastRequest.search )
        ) {
            // properties changed (ordering, columns, searching)
            ajax = true;
        }
         
        // Store the request for checking next time around
        cacheLastRequest = $.extend( true, {}, request );
 
        if ( ajax ) {
            // Need data from the server
            if ( requestStart < cacheLower ) {
                requestStart = requestStart - (requestLength*(conf.pages-1));
 
                if ( requestStart < 0 ) {
                    requestStart = 0;
                }
            }
             
            cacheLower = requestStart;
            cacheUpper = requestStart + (requestLength * conf.pages);
 
            request.start = requestStart;
            request.length = requestLength*conf.pages;
 
            // Provide the same `data` options as DataTables.
            console.log(conf);
            if ( $.isFunction ( conf.data ) ) {
                // As a function it is executed with the data object as an arg
                // for manipulation. If an object is returned, it is used as the
                // data object to submit
                var d = conf.data( request );
                if ( d ) {
                    $.extend( request, d );
                }
            }
            else if ( $.isPlainObject( conf.data ) ) {
                // As an object, the data given extends the default
                $.extend( request, conf.data );
            }

            settings.jqXHR = $.ajax( {
                "type":     conf.method,
                "url":      conf.url,
                "data":     request,
                "dataType": "json",
                "cache":    false,
                "success":  function ( json ) {
                    cacheLastJson = $.extend(true, {}, json);
 
                    if ( cacheLower != drawStart ) {
                        json.data.splice( 0, drawStart-cacheLower );
                    }
                    if($.isArray( json.data )){
                        json.data.splice( requestLength, json.data.length );
                    }
                    $('#datatable_ajax tbody').off( 'click', 'button');
                    $('#datatable_ajax').off('click', '.filter-submit');
                    $('#datatable_ajax').off('click', '.filter-cancel');
                    drawCallback( json );
                }
            } );
        }
        else {
            json = $.extend( true, {}, cacheLastJson );
            
            json.draw = request.draw; // Update the echo for each response
            console.log($.isPlainObject( json.data ));
            console.log($.isArray( json.data ));
            if($.isArray( json.data )){
                json.data.splice( 0, requestStart-cacheLower );
                json.data.splice( requestLength, json.data.length );
                $('#datatable_ajax tbody').off( 'click', 'button');
                $('#datatable_ajax').off('click', '.filter-submit');
                $('#datatable_ajax').off('click', '.filter-cancel');
                drawCallback(json);
            }
        }
    }
};


// Register an API method that will empty the pipelined data, forcing an Ajax
// fetch on the next draw (i.e. `table.clearPipeline().draw()`)
$.fn.dataTable.Api.register( 'clearPipeline()', function () {
    return this.iterator( 'table', function ( settings ) {
        settings.clearCache = true;
    } );
} );
 

var TableDatatablesAjax = function () {

    var initPickers = function () {
        //init date pickers
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            autoclose: true
        });
    }

    var handleRecords = function () {
        var grid = new Datatable();
        grid.params = {};
        grid.init({
            src: $("#datatable_ajax"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },
            loadingMessage: 'Loading...',
            dataTable: {
                "lengthMenu": [
                            [10, 20, 50, 100, 150, -1],
                            [10, 20, 50, 100, 150, "Todos"] // change per page values here
                ],
                "language": {
                    "lengthMenu": "Vista de _MENU_ registros por página",
                    "zeroRecords": "No se encontro ningun registro",
                    "info": "Página _PAGE_ de _PAGES_",
                    "infoEmpty": "",
                    "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "aria": {
                        "sortAscending": ": orden acendente",
                        "sortDescending": ": orden decendente"
                    },
                    "emptyTable": "No hay registros en la tabla",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Ultimo",
                        "next":       "Siguiente",
                        "previous":   "Anterior",
                        "page": "Página",
                        "pageOf": "de"
                    },
                    "select": {
                        "rows": {
                            _: " %d filas selecionadas",
                            0: "",
                            1: "1 fila seleccionada"
                        }
                    }
                },
                paging : true,
                "pageLength": 10, // default record count per page
                "ajax":  $.fn.dataTable.pipeline( {
                            url: "./controllers/index.php?accion=Reportes.ListInventarios", // ajax source
                            pages: 10, // number of pages to cache,
                            data : function(){
                                var table = $('#datatable_ajax');
                                grid.params = {};
                                $('textarea.form-filter, select.form-filter, input.form-filter:not([type="radio"],[type="checkbox"])', table).each(function() {
                                    var _self = $(this);
                                    var name = _self.attr("name");
                                    var data = [];
                                    if (!data[name]) {
                                        data[name] = _self.val();
                                    }
                                    $.extend( grid.params, data);
                                });

                                // get all checkboxes
                                $('input.form-filter[type="checkbox"]:checked', table).each(function() {
                                    var _self = $(this);
                                    var name = _self.attr("name");
                                    var data = [];
                                    if (!data[name]) {
                                        data[name] = _self.val();
                                    }
                                    $.extend( grid.params, data);
                                });

                                // get all radio buttons
                                $('input.form-filter[type="radio"]:checked', table).each(function() {
                                    var _self = $(this);
                                    var name = _self.attr("name");
                                    var data = [];
                                    if (!data[name]) {
                                        data[name] = _self.val();
                                    }
                                    $.extend( grid.params, data);
                                });
                                var data = {
                                    data : grid.params
                                }

                                return grid.params;
                            }
                } ),
                "columnDefs": [
                            {
                                "targets" : [0, 1, 2, 3, 4],
                                "orderable": true
                            },
                            {
                                "targets" : [5, 6, 7, 8, 9],
                                "orderable": false
                            },
                            { className: "dt-center", "targets": "_all" },
                        ],
                "order": [
                    [0, "asc"]
                ],
                "buttons": [
                            { extend: 'print', className: 'btn dark btn-outline', "title" : "Listado de Inventario", "text" : "Imprimir",
                                customize: function ( win ) {
                                    $(win.document.body)
                                        .css( 'font-size', '10pt' )
                                        .css( 'float', 'rigth' )
                                        .prepend(
                                            '<img src="http://cegaservices.procesos-iq.com/assets/logo.png" />'
                                        );
                 
                                    $(win.document.body).find( 'table' )
                                        .addClass( 'compact' )
                                        .css( 'font-size', 'inherit' );
                                },
                                exportOptions: {
                                    columns: ':visible'
                                }
                            },
                            { extend: 'print', className: 'btn dark btn-outline', "title" : "Listado de Inventario", "text" : "Imprimir <i class='fa fa-check' aria-hidden='true'></i>",
                                customize: function ( win ) {
                                    $(win.document.body)
                                        .css( 'font-size', '10pt' )
                                        .css( 'float', 'rigth' )
                                        .prepend(
                                            '<img src="http://cegaservices.procesos-iq.com/assets/logo.png" />'
                                        );
                 
                                    $(win.document.body).find( 'table' )
                                        .addClass( 'compact' )
                                        .css( 'font-size', 'inherit' );
                                },
                                exportOptions: {
                                    modifier: {
                                        selected: true
                                    },
                                    columns: ':visible'
                                }
                            },
                             { extend: 'pdfHtml5',"text" : "PDF", "title" : "Listado de Inventario" ,className: 'btn green btn-outline',
                                customize: function ( doc ) {
                                    var cols = [];
                                       cols[0] = {text: 'Left part', alignment: 'left', margin:[20] };
                                       cols[1] = {text: 'Right part', alignment: 'right', margin:[0,0,20] };
                                       var objFooter = {};
                                       objFooter['columns'] = cols;
                                       doc['footer']=objFooter;
                                       doc.content.unshift(
                                        {
                                            margin: [0, 0, 0, 12],
                                            alignment: 'left',
                                            image: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wAARCAAoAIoDASIAAhEBAxEB/8QAHAAAAgIDAQEAAAAAAAAAAAAAAAcFBgEECAID/8QAORAAAAUDAgMGBAQEBwAAAAAAAQIDBAUABhESIQcTMRQiIzJBUUJhcYEVUrHwCBaR0WJyc4KhsuH/xAAZAQACAwEAAAAAAAAAAAAAAAAAAwECBAX/xAAtEQABBAECAwYGAwAAAAAAAAABAAIDERIEIRMxUSIyM0FxoQUjNFKBwWFikf/aAAwDAQACEQMRAD8A6BvK6W1sxwLLEOs4ObSmiX/sYfhIH5vt1pLznG5RBUQapC/VDrhTlIF+mMib99aiOO8msM2JuYfnOtSRcD5W5TYAv+4dx+9aNpwMdAx0W/lmSD+ZlCdpaNXAakWzfoCxyfGJ/QvQA361WWThW0f7+lgkle5+LNlINeP88kqGuMiDp/lLzSj/AF1j+lNTh7xdhbtdEYrpHjZQ/kRVNqKr/lNtkf8ACIAPtmtyGixlYbD5Ji5QAu6CzRLl/wBNIVXILhNaZ55WaMCxWzc+r8OMfwkVQ31a+ok8ogHpvnIbBlinLzzTGsmaQbsJz0UrD8WklzLOIS2p2Vh0DiRWQbo+Ht5jEAfMAfapiV4kRMcjCmKxlnR5dIyzZFu0EyukMZyURAc71ryCfxW9Ve6KpKvEOLawKkrJM5WNTBfsyaDtmZNZc4hkCpp7ibP96925f0dNy4RZ2UrFSRkxVSbSTUUTKkDqYm4gbFGQU5turVzoqixvEiIfXia2TNZFnJhr0g7RKmBsb7d4R3DcNtwq9UA2pDg7ks0ViipVlmiisUIWaKKKEIrGKzRQhJPifakc7kWbiRZgsPIMkUwmOXoIj8Ih71S+KiJ457bs61TH8JPHJR+oO8CCqWvwx/LkB298GroW44dKajjtz9xTzJqflN/7+lLJcj6C7QxkWaSzNx3VUHKetFcv6fsK4mqe/Tzue+yx3smGBk0eLaDlV4XiByGGgFQxp96u1pIrTNnTPa3AMxnSnbMhVHTq7hg1B9RH7gGQ9KriMdarRYHDW02JFw7wc1dVVLV/pmHFWSHbzFwPuecR7KbSU5lC+FoD4QL8vl065AaTFqY2vAitxKqzSSAXKaUVaXEBOz7YZ25PW9Mozcel2YrZu0MoVyIdDJmDYc+/vnGaj+JTZ5IXTaM85YXazSVbK9pRjtSizI2nu6dGQIJhHf3AAyACAhTlJCkKAAV7IFKHQO0GqDvCRj7ThFJSWfSvJIYqZSJKajKHHoUOn/OPWu0BI6m47rO6MBtOdsFSXh3Llra09FRVxSKNuuVUnDWTRN21cihA8YgD5zFz9+lfKPkbil71LJRbi6kbfQOd29SfsypE0gXIIIk06ziI/v32ojiRGu5KOaSLO5Y5OR09lcuDeGrq2KbJfQcl7wZDcM7b1u3ZfkVCTZoZkE/LyxPOiyPq07Z0j88ewDj1xTzp57xwSs4e9nsl2vbV6vWLm9CxCBJM0j+KJkUFUHxSJiJSoglo8mPhzkQx9K6Mh3gSUW1eclZDnplV5axBKdPIeUQHoIVQrUvOFuOFk3zVzLoqRyQqOWqygc0oAUR23wOdI+v1xUPBcVYKclGMfHpXF2p2pygIqYhdIAXOrumH99cUM084J7HLmpY+GPfPmnLRStgeIcHMW7NTBDTTdtFkKZci5iaz5zjTpMIZEQx1DetJxxWhGtvM5hZpcfZna6iKRB5Ws3LxqN58YyOOvXNX4ExNYJvHiq8k36KWN2cSIa2TMm6gSjySdJkUKzQEDKJlN01b4yP5QyP23rYsbiLDXSu7bJi9YPGxRUUQeCUvcDzGAc+nrnAhRwZQ3PHZAnjLsct0xqKTzrjRElVXNHxU2+jmxvHfIphyyh6G3HoPz05qyyfEeAY2c2uQFl12TkwJoppk8Uym+U8CIABgwbOR9PXbNjBKKtvNQNREbp3JXyildA8WWEjNMY59EykWd9oM2VcJhoUA3l9ehvQdw+lND7VSSN0ZpwV2SNkFtWa+ayRFkxIoQpyj1AwZCiiluFjdXWiSGjCH1Ej2hT+4Il/tUgAAAYAKKKhrGs7opSbXqlxxpW5Vq6F7cVnWKinjAisKZ24h5VA0lMP39PXYRoorRB4jfVI1HhlKPh+9lG18xbOx30y8gznS7Sg8J4aBBN4hR+HYPiAC5HYPnpyjJW1+I8yrcjiejW7lVYyL2KPyzKFMfWXvjsYMdS+ggGaKK7Z+oMXkR+Vxq+Tn/KnYOMRb2TeE9GsbiIo7ZmaAeSEhhc8w4ZMAEIAm6+bcNx+eLn/D7bbZraJZJ3Hpkkll1BKssl4hSB3AxqDIfF9cjRRWHUvIjeP7LXp2AyN9P2lA/gpz+aZi1Y1usRs/kylE/KNo0lOfQYRx0ADavsFW7jNBCnK2dbMU1XOzaoFSASpmMHiHAmoRLtkdGR+uaKK1OmcZGel/mlnawcN3rXui7ir2Zxs/mSYj3biJObWkqkTUGnk8sMemoo+g423qch3T+/mV1LsLUYxqbhmsk2fily13Jz7FKJsBnIdTbgA4oopT6OmbNXaFD3TGCpzF5FLCBO0jIh9Ezil3tpBRUxRjWAlTSXKIAHeIYB32N6DkMYpgTcUhb/Cpk0XtWYkmDxc7lQjhyVNywPpACGHQQcZD3DbODZziiin6jvsH3EEpUA7Dz0URwwfSyPEBixtB9MP7b1BzyvUjFTSSx3shuBRD0MGnI7bhmumcUUVg+Iipq6LfoN4r6r//2Q=='
                                        }
                                       );
                                },
                                exportOptions: {
                                    columns: ':visible'
                                }
                            },
                            { extend: 'pdfHtml5',"text" : "PDF <i class='fa fa-check' aria-hidden='true'></i>", "title" : "Listado de Inventario" ,className: 'btn green btn-outline',
                                customize: function ( doc ) {
                                      var cols = [];
                                       cols[0] = {text: 'Left part', alignment: 'left', margin:[20] };
                                       cols[1] = {text: 'Right part', alignment: 'right', margin:[0,0,20] };
                                       var objFooter = {};
                                       objFooter['columns'] = cols;
                                       doc['footer']=objFooter;
                                       doc.content.unshift(
                                        {
                                            margin: [0, 0, 0, 12],
                                            alignment: 'left',
                                            image: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wAARCAAoAIoDASIAAhEBAxEB/8QAHAAAAgIDAQEAAAAAAAAAAAAAAAcFBgEECAID/8QAORAAAAUDAgMGBAQEBwAAAAAAAQIDBAUABhESIQcTMRQiIzJBUUJhcYEVUrHwCBaR0WJyc4KhsuH/xAAZAQACAwEAAAAAAAAAAAAAAAAAAwECBAX/xAAtEQABBAECAwYGAwAAAAAAAAABAAIDERIEIRMxUSIyM0FxoQUjNFKBwWFikf/aAAwDAQACEQMRAD8A6BvK6W1sxwLLEOs4ObSmiX/sYfhIH5vt1pLznG5RBUQapC/VDrhTlIF+mMib99aiOO8msM2JuYfnOtSRcD5W5TYAv+4dx+9aNpwMdAx0W/lmSD+ZlCdpaNXAakWzfoCxyfGJ/QvQA361WWThW0f7+lgkle5+LNlINeP88kqGuMiDp/lLzSj/AF1j+lNTh7xdhbtdEYrpHjZQ/kRVNqKr/lNtkf8ACIAPtmtyGixlYbD5Ji5QAu6CzRLl/wBNIVXILhNaZ55WaMCxWzc+r8OMfwkVQ31a+ok8ogHpvnIbBlinLzzTGsmaQbsJz0UrD8WklzLOIS2p2Vh0DiRWQbo+Ht5jEAfMAfapiV4kRMcjCmKxlnR5dIyzZFu0EyukMZyURAc71ryCfxW9Ve6KpKvEOLawKkrJM5WNTBfsyaDtmZNZc4hkCpp7ibP96925f0dNy4RZ2UrFSRkxVSbSTUUTKkDqYm4gbFGQU5turVzoqixvEiIfXia2TNZFnJhr0g7RKmBsb7d4R3DcNtwq9UA2pDg7ks0ViipVlmiisUIWaKKKEIrGKzRQhJPifakc7kWbiRZgsPIMkUwmOXoIj8Ih71S+KiJ457bs61TH8JPHJR+oO8CCqWvwx/LkB298GroW44dKajjtz9xTzJqflN/7+lLJcj6C7QxkWaSzNx3VUHKetFcv6fsK4mqe/Tzue+yx3smGBk0eLaDlV4XiByGGgFQxp96u1pIrTNnTPa3AMxnSnbMhVHTq7hg1B9RH7gGQ9KriMdarRYHDW02JFw7wc1dVVLV/pmHFWSHbzFwPuecR7KbSU5lC+FoD4QL8vl065AaTFqY2vAitxKqzSSAXKaUVaXEBOz7YZ25PW9Mozcel2YrZu0MoVyIdDJmDYc+/vnGaj+JTZ5IXTaM85YXazSVbK9pRjtSizI2nu6dGQIJhHf3AAyACAhTlJCkKAAV7IFKHQO0GqDvCRj7ThFJSWfSvJIYqZSJKajKHHoUOn/OPWu0BI6m47rO6MBtOdsFSXh3Llra09FRVxSKNuuVUnDWTRN21cihA8YgD5zFz9+lfKPkbil71LJRbi6kbfQOd29SfsypE0gXIIIk06ziI/v32ojiRGu5KOaSLO5Y5OR09lcuDeGrq2KbJfQcl7wZDcM7b1u3ZfkVCTZoZkE/LyxPOiyPq07Z0j88ewDj1xTzp57xwSs4e9nsl2vbV6vWLm9CxCBJM0j+KJkUFUHxSJiJSoglo8mPhzkQx9K6Mh3gSUW1eclZDnplV5axBKdPIeUQHoIVQrUvOFuOFk3zVzLoqRyQqOWqygc0oAUR23wOdI+v1xUPBcVYKclGMfHpXF2p2pygIqYhdIAXOrumH99cUM084J7HLmpY+GPfPmnLRStgeIcHMW7NTBDTTdtFkKZci5iaz5zjTpMIZEQx1DetJxxWhGtvM5hZpcfZna6iKRB5Ws3LxqN58YyOOvXNX4ExNYJvHiq8k36KWN2cSIa2TMm6gSjySdJkUKzQEDKJlN01b4yP5QyP23rYsbiLDXSu7bJi9YPGxRUUQeCUvcDzGAc+nrnAhRwZQ3PHZAnjLsct0xqKTzrjRElVXNHxU2+jmxvHfIphyyh6G3HoPz05qyyfEeAY2c2uQFl12TkwJoppk8Uym+U8CIABgwbOR9PXbNjBKKtvNQNREbp3JXyildA8WWEjNMY59EykWd9oM2VcJhoUA3l9ehvQdw+lND7VSSN0ZpwV2SNkFtWa+ayRFkxIoQpyj1AwZCiiluFjdXWiSGjCH1Ej2hT+4Il/tUgAAAYAKKKhrGs7opSbXqlxxpW5Vq6F7cVnWKinjAisKZ24h5VA0lMP39PXYRoorRB4jfVI1HhlKPh+9lG18xbOx30y8gznS7Sg8J4aBBN4hR+HYPiAC5HYPnpyjJW1+I8yrcjiejW7lVYyL2KPyzKFMfWXvjsYMdS+ggGaKK7Z+oMXkR+Vxq+Tn/KnYOMRb2TeE9GsbiIo7ZmaAeSEhhc8w4ZMAEIAm6+bcNx+eLn/D7bbZraJZJ3Hpkkll1BKssl4hSB3AxqDIfF9cjRRWHUvIjeP7LXp2AyN9P2lA/gpz+aZi1Y1usRs/kylE/KNo0lOfQYRx0ADavsFW7jNBCnK2dbMU1XOzaoFSASpmMHiHAmoRLtkdGR+uaKK1OmcZGel/mlnawcN3rXui7ir2Zxs/mSYj3biJObWkqkTUGnk8sMemoo+g423qch3T+/mV1LsLUYxqbhmsk2fily13Jz7FKJsBnIdTbgA4oopT6OmbNXaFD3TGCpzF5FLCBO0jIh9Ezil3tpBRUxRjWAlTSXKIAHeIYB32N6DkMYpgTcUhb/Cpk0XtWYkmDxc7lQjhyVNywPpACGHQQcZD3DbODZziiin6jvsH3EEpUA7Dz0URwwfSyPEBixtB9MP7b1BzyvUjFTSSx3shuBRD0MGnI7bhmumcUUVg+Iipq6LfoN4r6r//2Q=='
                                        }
                                       );
                                },
                                exportOptions: {
                                    modifier: {
                                        selected: true
                                    },
                                    columns: ':visible'
                                }
                            },
                            { extend: 'excel', "text" :"Excel","title" : "Listado de Inventario" ,className: 'btn yellow btn-outline ',
                                exportOptions: {
                                    columns: ':visible'
                                }
                            },
                            { extend: 'csv',"text" :"CSV", "title" : "Listado de Inventario" , className: 'btn purple btn-outline ',
                                exportOptions: {
                                    columns: ':visible'
                                }
                            }
                        ],
                
                select: true,
                "bSort": true,
                "processing": true,
                "serverSide": true,
                "dom": "<'row' <'col-md-12'B>><'row'><'row'<'col-md-6 col-sm-12'l>r><'table-scrollable't><'row'<'col-md-6 col-sm-12'i><'col-md-6 col-sm-12'p>>", // horizobtal scrollable datatable
                "fnDrawCallback": function( oSettings ) {

                    $('#datatable_ajax_tools > li > a.tool-action').on('click', function(event) {
                        event.preventDefault();
                        var action = $(this).attr('data-action');
                        grid.getDataTable().button(action).trigger();
                    });
                    
                    $('#datatable_ajax').on('click', '.filter-submit', function(e) {
                        grid.getDataTable().clearPipeline().draw();
                    });

                    $("#datatable_ajax").delegate('.form-filter', 'keydown', function(event) {
                        if(event.which == 13) {
                            event.preventDefault();
                            grid.getDataTable().clearPipeline().draw();
                        }
                    });

                    $('#datatable_ajax').on('click', '.filter-cancel', function(e) {
                         $('textarea.form-filter, select.form-filter, input.form-filter', $('#datatable_ajax')).each(function() {
                            $(this).val("");
                        });
                        $('input.form-filter[type="checkbox"]', $('#datatable_ajax')).each(function() {
                            $(this).attr("checked", false);
                        });
                        $("#date_from").val("")
                        $("#date_to").val("")
                        grid.getDataTable().clearPipeline().draw();
                    });

                    $("#entregadas").val(valores[0])
                    $("#utilizadas").val(valores[1])
                    $("#por_entregar").val(valores[2])
                    valores[0] = 0
                    valores[1] = 0
                    valores[2] = 0
                },
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                    valores[0] += parseInt(aData[4])
                    valores[1] += parseInt(aData[5])
                    valores[2] += parseInt(aData[6])
                },
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;
        
                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };
        
                    // Total over all pages
                    /*total = api
                        .column( 4 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );*/
        
                    // Total over this page
                    var getTotal = function(column){
                        return api.column( column, { page: 'current'} )
                            .data()
                            .reduce( function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0 );
                    }

                    let entregada = 7
                    let utilizada = 8
                    let por_entregar = 9

                    var cant_entregada = getTotal(entregada)
                    var cant_utilizada = getTotal(utilizada)
                    var cant_por_entregar = getTotal(por_entregar)
        
                    // Update footer
                    $( api.column( entregada ).footer() ).html(cant_entregada);
                    $( api.column( utilizada ).footer() ).html(cant_utilizada);
                    $( api.column( por_entregar ).footer() ).html(cant_por_entregar);
                }
            }
        });
        grid.getTableWrapper().on('click', '.table-group-action-submit', function (e) {
            e.preventDefault();
            var action = $(".table-group-action-input", grid.getTableWrapper());
            if (action.val() != "" && grid.getSelectedRowsCount() > 0) {
                grid.setAjaxParam("customActionType", "group_action");
                grid.setAjaxParam("customActionName", action.val());
                grid.setAjaxParam("id", grid.getSelectedRows());
                grid.getDataTable().ajax.reload();
                //grid.clearAjaxParams();
            } else if (action.val() == "") {
                App.alert({
                    type: 'danger',
                    icon: 'warning',
                    message: 'Please select an action',
                    container: grid.getTableWrapper(),
                    place: 'prepend'
                });
            } else if (grid.getSelectedRowsCount() === 0) {
                App.alert({
                    type: 'danger',
                    icon: 'warning',
                    message: 'No record selected',
                    container: grid.getTableWrapper(),
                    place: 'prepend'
                });
            }
        });

        grid.setAjaxParam("customActionType", "group_action");
        grid.getDataTable().ajax.reload();
        grid.clearAjaxParams();

        $('#datatable_ajax_tools > li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            grid.getDataTable().button(action).trigger();
        });

        $("[role=menu] input").on('change', function(e){
            e.preventDefault();
            let index = $(this).attr("index")
            var column = grid.getDataTable().column(index)
            let visibility = !column.visible()
            
            column.visible(visibility)
            $($(this)[0]).prop("checked", visibility)
        })
    }

    return {
        init: function () {
            initPickers();
            handleRecords();
            return grid;
        }
    };

}();

jQuery(document).ready(function() {
    TableDatatablesAjax.init();
});

/*var Services = angular.module('App.Services', [])
    .config(function ($httpProvider){
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
        $httpProvider.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
        $httpProvider.defaults.transformRequest = function(obj) {
            var str = [];
            for(var p in obj){
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            }
            return str.join("&");
        };
    });*/

app.controller('supervisores', ['$scope','$http','$interval','$controller' , 'client', function($scope,$http,$interval,$controller ,client){

    $scope.init = function(){
        
    }

    $scope.dateChange = function(){
        if($scope.date_start != "" && $scope.date_start != undefined && $scope.date_end != "" && $scope.date_end != undefined){
            $("[name=from_date]").val($scope.date_start)
            $("[name=to_date]").val($scope.date_end)
        }
    }

    /*var table = $('#datatable_ajax').DataTable( {
        ajax : {
            url : './controllers/index.php?accion=Reportes.ListInventarios',
            type : "POST",
            data : function(d){
                var data = {
                    order_date_from : $scope.date_start,
                    order_date_to : $scope.date_end
                };
                return $.extend( {}, d, data);
            },
        },
        columnDefs : [
            { className: "textCenter", "targets": [ 3 ] },
            { className: "textCenter dt-center", "targets": [ 4 ] },
            { className: "textCenter dt-center", "targets": [ 5 ] },
            { className: "textCenter dt-center", "targets": [ 6 ] },
            { "targets" : [-1], "orderable": false }
        ]
    } );*/

    $scope.init();

}]);