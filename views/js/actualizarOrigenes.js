app.controller('actualizarOrigenes', ['$scope','$http','client','$controller', function($scope, $http, client, $controller){    
    $scope.ds_areas = function(callback, params){
        client.post("./controllers/index.php?accion=Data.ds_areas" , function(d, r){			
            r();
            callback(d)
        } , params);
    }
    $scope.ds_listadoRevisionInstalacion = function(callback, params){
        client.post("./controllers/index.php?accion=Data.ds_listadoRevisionInstalacion" , function(d, r){			
            r();
            callback(d)
        } , params);
    }
    $scope.ds_herramientas = function(callback, params){
        client.post("./controllers/index.php?accion=Data.ds_herramientas" , function(d, r){			
            r();
            callback(d)
        } , params);
    }
    $scope.ds_descriptionEquipo = function(callback, params){
        client.post("./controllers/index.php?accion=Data.ds_descriptionEquipo" , function(d, r){			
            r();
            callback(d)
        } , params);
    }
    $scope.ds_supervisores = function(callback, params){
        client.post("./controllers/index.php?accion=Data.ds_supervisores" , function(d, r){		
            r();
            callback(d)
        } , params);
    }
    $scope.ds_clientesInstalacion = function(callback, params){
        client.post("./controllers/index.php?accion=Data.ds_clientesInstalacion" , function(d, r){		
            r();
            callback(d)
        } , params);
    }
    $scope.ds_listadoSucursales = function(callback, params){
        client.post("./controllers/index.php?accion=Data.ds_listadoSucursales" , function(d, r){		
            r();
            callback(d)
        } , params);
    }
    $scope.ds_listadoEquiposClimatizacionADetalleInstalacion = function(callback, params){
        client.post("./controllers/index.php?accion=Data.ds_listadoEquiposClimatizacionADetalleInstalacion" , function(d, r){		
            r();
            callback(d)
        } , params);
    }
    $scope.ds_listadoEquiposClimatizacionBDetalleInstalacion = function(callback, params){
        client.post("./controllers/index.php?accion=Data.ds_listadoEquiposClimatizacionBDetalleInstalacion" , function(d, r){		
            r();
            callback(d)
        } , params);
    }
    $scope.ds_listadoEquiposVentilacionDetalleInstalacion = function(callback, params){
        client.post("./controllers/index.php?accion=Data.ds_listadoEquiposVentilacionDetalleInstalacion" , function(d, r){		
            r();
            callback(d)
        } , params);
    }
    $scope.ds_listadoEquiposRefrigeracionDetalleInstalacion = function(callback, params){
        client.post("./controllers/index.php?accion=Data.ds_listadoEquiposRefrigeracionDetalleInstalacion" , function(d, r){		
            r();
            callback(d)
        } , params);
    }
    $scope.ds_listadoEvaporadoresInstalacion = function(callback, params){
        client.post("./controllers/index.php?accion=Data.ds_listadoEvaporadoresInstalacion" , function(d, r){		
            r();
            callback(d)
        } , params);
    }
    $scope.ds_listadoRevisionMatenimiento = function(callback, params){
        client.post("./controllers/index.php?accion=Data.ds_listadoRevisionMatenimiento" , function(d, r){		
            r();
            callback(d)
        } , params);
    }
    $scope.ds_clientesMantenimiento = function(callback, params){
        client.post("./controllers/index.php?accion=Data.ds_clientesMantenimiento" , function(d, r){		
            r();
            callback(d)
        } , params);
    }
    $scope.ds_listadoEquiposClimatizacionADetalleMantenimiento = function(callback, params){
        client.post("./controllers/index.php?accion=Data.ds_listadoEquiposClimatizacionADetalleMantenimiento" , function(d, r){		
            r();
            callback(d)
        } , params);
    }
    $scope.ds_listadoEquiposClimatizacionBDetalleMantenimiento = function(callback, params){
        client.post("./controllers/index.php?accion=Data.ds_listadoEquiposClimatizacionBDetalleMantenimiento" , function(d, r){		
            r();
            callback(d)
        } , params);
    }
    $scope.ds_listadoEquiposVentilacionDetalleMantenimiento = function(callback, params){
        client.post("./controllers/index.php?accion=Data.ds_listadoEquiposVentilacionDetalleMantenimiento" , function(d, r){		
            r();
            callback(d)
        } , params);
    }
    $scope.ds_listadoEquiposRefrigeracionDetalleMantenimiento = function(callback, params){
        client.post("./controllers/index.php?accion=Data.ds_listadoEquiposRefrigeracionDetalleMantenimiento" , function(d, r){		
            r();
            callback(d)
        } , params);
    }
    $scope.ds_listadoHerramientas = function(callback, params){
        client.post("./controllers/index.php?accion=Data.ds_listadoHerramientas" , function(d, r){		
            r();
            callback(d)
        } , params);
    }
    $scope.ds_listadoEquiposClimatizacionADetalleRevisionCorrectivo = function(callback, params){
        client.post("./controllers/index.php?accion=Data.ds_listadoEquiposClimatizacionADetalleRevisionCorrectivo" , function(d, r){		
            r();
            callback(d)
        } , params);
    }
    $scope.ds_listadoEquiposClimatizacionBDetalleRevisionCorrectivo = function(callback, params){
        client.post("./controllers/index.php?accion=Data.ds_listadoEquiposClimatizacionBDetalleRevisionCorrectivo" , function(d, r){		
            r();
            callback(d)
        } , params);
    }
    $scope.ds_listadoRevisionCorrectivo = function(callback, params){
        client.post("./controllers/index.php?accion=Data.ds_listadoRevisionCorrectivo" , function(d, r){		
            r();
            callback(d)
        } , params);
    }
    $scope.ds_listadoEquiposVentilacionDetalleRevisionCorrectivo = function(callback, params){
        client.post("./controllers/index.php?accion=Data.ds_listadoEquiposVentilacionDetalleRevisionCorrectivo" , function(d, r){		
            r();
            callback(d)
        } , params);
    }
    $scope.ds_listadoEquiposRefrigeracionDetalleRevisionCorrectivo = function(callback, params){
        client.post("./controllers/index.php?accion=Data.ds_listadoEquiposRefrigeracionDetalleRevisionCorrectivo" , function(d, r){		
            r();
            callback(d)
        } , params);
    }
    $scope.ds_listadoEvaporadoresRevisionCorrectivo = function(callback, params){
        client.post("./controllers/index.php?accion=Data.ds_listadoEvaporadoresRevisionCorrectivo" , function(d, r){		
            r();
            callback(d)
        } , params);
    }
    $scope.ds_listadoEquiposClimatizacionADetalleCorrectivo = function(callback, params){
        client.post("./controllers/index.php?accion=Data.ds_listadoEquiposClimatizacionADetalleCorrectivo" , function(d, r){		
            r();
            callback(d)
        } , params);
    }
    $scope.ds_listadoEquiposClimatizacionBDetalleCorrectivo = function(callback, params){
        client.post("./controllers/index.php?accion=Data.ds_listadoEquiposClimatizacionBDetalleCorrectivo" , function(d, r){		
            r();
            callback(d)
        } , params);
    }
    $scope.ds_listadoEquiposRefrigeracionDetalleCorrectivo = function(callback, params){
        client.post("./controllers/index.php?accion=Data.ds_listadoEquiposRefrigeracionDetalleCorrectivo" , function(d, r){		
            r();
            callback(d)
        } , params);
    }
    $scope.ds_listadoEquiposVentilacionDetalleCorrectivo = function(callback, params){
        client.post("./controllers/index.php?accion=Data.ds_listadoEquiposVentilacionDetalleCorrectivo" , function(d, r){		
            r();
            callback(d)
        } , params);
    }
    $scope.ds_listadoEvaporadoresCorrectivo = function(callback, params){
        client.post("./controllers/index.php?accion=Data.ds_listadoEvaporadoresCorrectivo" , function(d, r){		
            r();
            callback(d)
        } , params);
    }
    $scope.ds_clientesCorrectivo = function(callback, params){
        client.post("./controllers/index.php?accion=Data.ds_clientesCorrectivo" , function(d, r){		
            r();
            callback(d)
        } , params);
    }
    $scope.ds_horasDia = function(callback, params){
        client.post("./controllers/index.php?accion=Data.ds_horasDia" , function(d, r){		
            r();
            callback(d)
        } , params);
    }
    $scope.ds_clientes = function(callback, params){
        client.post("./controllers/index.php?accion=Data.ds_clientes" , function(d, r){		
            r();
            callback(d)
        } , params);
    }
    $scope.ds_listadoEquipos = function(callback, params){
        client.post("./controllers/index.php?accion=Data.ds_listadoEquipos" , function(d, r){		
            r();
            callback(d)
        } , params);
    }
    $scope.ds_listadoRepuestos = function(callback, params){
        client.post("./controllers/index.php?accion=Data.ds_listadoRepuestos" , function(d, r){		
            r();
            callback(d)
        } , params);
    }
    $scope.ds_listadoMateriales = function(callback, params){
        client.post("./controllers/index.php?accion=Data.ds_listadoMateriales" , function(d, r){		
            r();
            callback(d)
        } , params);
    }
    
    $scope.actualizarTodos = function(){
        $scope.ds_listadoEquipos(r => {alert("ORIGEN ACTUALIZADO CORRECTAMENTE","ALERTA","success")})
        $scope.ds_listadoRepuestos(r => {alert("ORIGEN ACTUALIZADO CORRECTAMENTE","ALERTA","success")})
        $scope.ds_clientes(r => {alert("ORIGEN ACTUALIZADO CORRECTAMENTE","ALERTA","success")})
        $scope.ds_areas(r => {alert("ORIGEN ACTUALIZADO CORRECTAMENTE","ALERTA","success")})
        $scope.ds_listadoRevisionInstalacion(r => {alert("ORIGEN ACTUALIZADO CORRECTAMENTE","ALERTA","success")})
        $scope.ds_herramientas(r => {alert("ORIGEN ACTUALIZADO CORRECTAMENTE","ALERTA","success")})
        $scope.ds_descriptionEquipo(r => {alert("ORIGEN ACTUALIZADO CORRECTAMENTE","ALERTA","success")})
        $scope.ds_supervisores(r => {alert("ORIGEN ACTUALIZADO CORRECTAMENTE","ALERTA","success")})
        $scope.ds_clientesInstalacion(r => {alert("ORIGEN ACTUALIZADO CORRECTAMENTE","ALERTA","success")})
        $scope.ds_listadoSucursales(r => {alert("ORIGEN ACTUALIZADO CORRECTAMENTE","ALERTA","success")})
        $scope.ds_listadoEquiposClimatizacionADetalleInstalacion(r => {alert("ORIGEN ACTUALIZADO CORRECTAMENTE","ALERTA","success")})
        $scope.ds_listadoEquiposClimatizacionBDetalleInstalacion(r => {alert("ORIGEN ACTUALIZADO CORRECTAMENTE","ALERTA","success")})
        $scope.ds_listadoEquiposVentilacionDetalleInstalacion(r => {alert("ORIGEN ACTUALIZADO CORRECTAMENTE","ALERTA","success")})
        $scope.ds_listadoEquiposRefrigeracionDetalleInstalacion(r => {alert("ORIGEN ACTUALIZADO CORRECTAMENTE","ALERTA","success")})
        $scope.ds_listadoEvaporadoresInstalacion(r => {alert("ORIGEN ACTUALIZADO CORRECTAMENTE","ALERTA","success")})
        $scope.ds_clientesMantenimiento(r => {alert("ORIGEN ACTUALIZADO CORRECTAMENTE","ALERTA","success")})
        $scope.ds_listadoRevisionMatenimiento(r => {alert("ORIGEN ACTUALIZADO CORRECTAMENTE","ALERTA","success")})
        $scope.ds_listadoEquiposClimatizacionADetalleMantenimiento(r => {alert("ORIGEN ACTUALIZADO CORRECTAMENTE","ALERTA","success")})
        $scope.ds_listadoEquiposClimatizacionBDetalleMantenimiento(r => {alert("ORIGEN ACTUALIZADO CORRECTAMENTE","ALERTA","success")})
        $scope.ds_listadoEquiposVentilacionDetalleMantenimiento(r => {alert("ORIGEN ACTUALIZADO CORRECTAMENTE","ALERTA","success")})
        $scope.ds_listadoEquiposRefrigeracionDetalleMantenimiento(r => {alert("ORIGEN ACTUALIZADO CORRECTAMENTE","ALERTA","success")})
        $scope.ds_listadoHerramientas(r => {alert("ORIGEN ACTUALIZADO CORRECTAMENTE","ALERTA","success")})
        $scope.ds_listadoRevisionCorrectivo(r => {alert("ORIGEN ACTUALIZADO CORRECTAMENTE","ALERTA","success")})
        $scope.ds_listadoEquiposClimatizacionADetalleRevisionCorrectivo(r => {alert("ORIGEN ACTUALIZADO CORRECTAMENTE","ALERTA","success")})
        $scope.ds_listadoEquiposClimatizacionBDetalleRevisionCorrectivo(r => {alert("ORIGEN ACTUALIZADO CORRECTAMENTE","ALERTA","success")})
        $scope.ds_listadoEquiposVentilacionDetalleRevisionCorrectivo(r => {alert("ORIGEN ACTUALIZADO CORRECTAMENTE","ALERTA","success")})
        $scope.ds_listadoEquiposRefrigeracionDetalleRevisionCorrectivo(r => {alert("ORIGEN ACTUALIZADO CORRECTAMENTE","ALERTA","success")})
        $scope.ds_listadoEvaporadoresRevisionCorrectivo(r => {alert("ORIGEN ACTUALIZADO CORRECTAMENTE","ALERTA","success")})
        $scope.ds_clientesCorrectivo(r => {alert("ORIGEN ACTUALIZADO CORRECTAMENTE","ALERTA","success")})
        $scope.ds_listadoEquiposClimatizacionADetalleCorrectivo(r => {alert("ORIGEN ACTUALIZADO CORRECTAMENTE","ALERTA","success")})
        $scope.ds_listadoEquiposClimatizacionBDetalleCorrectivo(r => {alert("ORIGEN ACTUALIZADO CORRECTAMENTE","ALERTA","success")})
        $scope.ds_listadoEquiposRefrigeracionDetalleCorrectivo(r => {alert("ORIGEN ACTUALIZADO CORRECTAMENTE","ALERTA","success")})
        $scope.ds_listadoEquiposVentilacionDetalleCorrectivo(r => {alert("ORIGEN ACTUALIZADO CORRECTAMENTE","ALERTA","success")})
        $scope.ds_horasDia(r => {alert("ORIGEN ACTUALIZADO CORRECTAMENTE","ALERTA","success")})
        $scope.ds_listadoEvaporadoresCorrectivo(r => {alert("ORIGEN ACTUALIZADO CORRECTAMENTE","ALERTA","success")})
        $scope.ds_listadoMateriales(r => {alert("ORIGEN ACTUALIZADO CORRECTAMENTE","ALERTA","success")})
    }

    
    $scope.cronograma = function(){
        $scope.ds_horasDia(r => {alert("HORAS ACTUALIZADO CORRECTAMENTE","CRÓNOGRAMA DE ORDENES","success")})
        $scope.ds_supervisores(r => {alert("SUPERVISORES ACTUALIZADO CORRECTAMENTE","CRÓNOGRAMA DE ORDENES","success")})
        $scope.ds_clientes(r => {alert("CLIENTES ACTUALIZADO CORRECTAMENTE","CRÓNOGRAMA DE ORDENES","success")})
        $scope.ds_listadoSucursales(r => {alert("SUCURSALES ACTUALIZADO CORRECTAMENTE","CRÓNOGRAMA DE ORDENES","success")})
        $scope.ds_listadoMateriales(r => {alert("MATERIALES ACTUALIZADO CORRECTAMENTE","CRÓNOGRAMA DE ORDENES","success")})
        $scope.ds_listadoHerramientas(r => {alert("HERRAMIENTAS ACTUALIZADO CORRECTAMENTE","CRÓNOGRAMA DE ORDENES","success")})
        $scope.ds_listadoEquipos(r => {alert("EQUIPOS ACTUALIZADO CORRECTAMENTE","CRÓNOGRAMA DE ORDENES","success")})
        $scope.ds_listadoRepuestos(r => {alert("REPUESTOS ACTUALIZADO CORRECTAMENTE","CRÓNOGRAMA DE ORDENES","success")})
    }

    $scope.actualizarRevisionInstalacion = function(){
        $scope.ds_areas(r => {alert("AREAS ACTUALIZADO CORRECTAMENTE","REVISIÓN INSTALACIÓN","success")})
        $scope.ds_descriptionEquipo(r => {alert("EQUIPOS ACTUALIZADO CORRECTAMENTE","REVISIÓN INSTALACIÓN","success")})
        $scope.ds_supervisores(r => {alert("SUPERVISORES ACTUALIZADO CORRECTAMENTE","REVISIÓN INSTALACIÓN","success")})
        $scope.ds_listadoRevisionInstalacion(r => {alert("CLIENTES ACTUALIZADO CORRECTAMENTE","REVISIÓN INSTALACIÓN","success")})
        $scope.ds_herramientas(r => {alert("HERRAMIENTAS ACTUALIZADO CORRECTAMENTE","REVISIÓN INSTALACIÓN","success")})
    }

    $scope.actualizarInstalacion = function(){
        $scope.ds_supervisores(r => {alert("SUPERVISORES ACTUALIZADO CORRECTAMENTE","INSTALACIÓN","success")})
        $scope.ds_clientesInstalacion(r => {alert("CLIENTES ACTUALIZADO CORRECTAMENTE","INSTALACIÓN","success")})
        $scope.ds_listadoSucursales(r => {alert("SUCURSALES ACTUALIZADO CORRECTAMENTE","INSTALACIÓN","success")})
        $scope.ds_areas(r => {alert("AREAS ACTUALIZADO CORRECTAMENTE","INSTALACIÓN","success")})
        $scope.ds_listadoEquiposClimatizacionADetalleInstalacion(r => {alert("EQUIPOS CLIMAT. A ACTUALIZADO CORRECTAMENTE","INSTALACIÓN","success")})
        $scope.ds_listadoEquiposClimatizacionBDetalleInstalacion(r => {alert("EQUIPOS CLIMAT. B ACTUALIZADO CORRECTAMENTE","INSTALACIÓN","success")})
        $scope.ds_listadoEquiposVentilacionDetalleInstalacion(r => {alert("EQUIPOS VENTILACION ACTUALIZADO CORRECTAMENTE","INSTALACIÓN","success")})
        $scope.ds_descriptionEquipo(r => {alert("EQUIPOS ACTUALIZADO CORRECTAMENTE","INSTALACIÓN","success")})
        $scope.ds_listadoEquiposRefrigeracionDetalleInstalacion(r => {alert("EQUIPOS REFRIG. ACTUALIZADO CORRECTAMENTE","INSTALACIÓN","success")})
        $scope.ds_listadoEvaporadoresInstalacion(r => {alert("EVAPORADORES ACTUALIZADO CORRECTAMENTE","INSTALACIÓN","success")})
    }

    $scope.actualizarRevisionMantenimiento = function(){
        $scope.ds_areas(r => {alert("AREAS ACTUALIZADO CORRECTAMENTE","REVISIÓN MANTENIMIENTO","success")})
        $scope.ds_descriptionEquipo(r => {alert("EQUIPOS ACTUALIZADO CORRECTAMENTE","REVISIÓN MANTENIMIENTO","success")})
        $scope.ds_supervisores(r => {alert("SUPERVISORES ACTUALIZADO CORRECTAMENTE","REVISIÓN MANTENIMIENTO","success")})
        $scope.ds_listadoRevisionMatenimiento(r => {alert("CLIENTES ACTUALIZADO CORRECTAMENTE","REVISIÓN MANTENIMIENTO","success")})
        $scope.ds_herramientas(r => {alert("HERRAMIENTAS ACTUALIZADO CORRECTAMENTE","REVISIÓN MANTENIMIENTO","success")})
    }

    $scope.actualizarMantenimiento = function(){
        $scope.ds_supervisores(r => {alert("SUPERVISORES ACTUALIZADO CORRECTAMENTE","MANTENIMIENTO","success")})
        $scope.ds_listadoSucursales(r => {alert("SUCURSALES ACTUALIZADO CORRECTAMENTE","MANTENIMIENTO","success")})
        $scope.ds_areas(r => {alert("AREAS ACTUALIZADO CORRECTAMENTE","MANTENIMIENTO","success")})
        $scope.ds_descriptionEquipo(r => {alert("EQUIPOS ACTUALIZADO CORRECTAMENTE","MANTENIMIENTO","success")})
        $scope.ds_clientesMantenimiento(r => {alert("CLIENTES ACTUALIZADO CORRECTAMENTE","MANTENIMIENTO","success")})
        $scope.ds_listadoEquiposClimatizacionADetalleMantenimiento(r => {alert("EQUIPOS CLIMAT. A ACTUALIZADO CORRECTAMENTE","MANTENIMIENTO","success")})
        $scope.ds_listadoEquiposClimatizacionBDetalleMantenimiento(r => {alert("EQUIPOS CLIMAT. B ACTUALIZADO CORRECTAMENTE","MANTENIMIENTO","success")})
        $scope.ds_listadoEquiposVentilacionDetalleMantenimiento(r => {alert("EQUIPOS VENTILACION ACTUALIZADO CORRECTAMENTE","MANTENIMIENTO","success")})
        $scope.ds_listadoEquiposRefrigeracionDetalleMantenimiento(r => {alert("EQUIPOS REFRIG. ACTUALIZADO CORRECTAMENTE","MANTENIMIENTO","success")})
        $scope.ds_listadoHerramientas(r => {alert("HERRAMIENTAS ACTUALIZADO CORRECTAMENTE","MANTENIMIENTO","success")})
    }

    $scope.actualizarRevisionCorrectivo = function(){
        $scope.ds_supervisores(r => {alert("SUPERVISORES ACTUALIZADO CORRECTAMENTE","REVISIÓN CORRECTIVO","success")})
        $scope.ds_descriptionEquipo(r => {alert("EQUIPOS ACTUALIZADO CORRECTAMENTE","REVISIÓN CORRECTIVO","success")})
        $scope.ds_herramientas(r => {alert("HERRAMIENTAS ACTUALIZADO CORRECTAMENTE","REVISIÓN CORRECTIVO","success")})
        $scope.ds_listadoRevisionCorrectivo(r => {alert("CLIENTES ACTUALIZADO CORRECTAMENTE","REVISIÓN CORRECTIVO","success")})
        $scope.ds_listadoEquiposClimatizacionADetalleRevisionCorrectivo(r => {alert("EQUIPOS CLIMAT. A ACTUALIZADO CORRECTAMENTE","REVISIÓN CORRECTIVO","success")})
        $scope.ds_listadoEquiposClimatizacionBDetalleRevisionCorrectivo(r => {alert("EQUIPOS CLIMAT. B ACTUALIZADO CORRECTAMENTE","REVISIÓN CORRECTIVO","success")})
        $scope.ds_listadoEquiposVentilacionDetalleRevisionCorrectivo(r => {alert("EQUIPOS VENTILACION ACTUALIZADO CORRECTAMENTE","REVISIÓN CORRECTIVO","success")})
        $scope.ds_listadoEquiposRefrigeracionDetalleRevisionCorrectivo(r => {alert("EQUIPOS REFRIG. ACTUALIZADO CORRECTAMENTE","REVISIÓN CORRECTIVO","success")})
        $scope.ds_listadoEvaporadoresRevisionCorrectivo(r => {alert("EVAPORADORES ACTUALIZADO CORRECTAMENTE","REVISIÓN CORRECTIVO","success")})
    }

    $scope.actualizarCorrectivo = function(){
        $scope.ds_supervisores(r => {alert("SUPERVISORES ACTUALIZADO CORRECTAMENTE","CORRECTIVO","success")})
        $scope.ds_listadoSucursales(r => {alert("SUCURSALES ACTUALIZADO CORRECTAMENTE","CORRECTIVO","success")})
        $scope.ds_descriptionEquipo(r => {alert("EQUIPOS ACTUALIZADO CORRECTAMENTE","CORRECTIVO","success")})
        $scope.ds_clientesCorrectivo(r => {alert("CLIENTES ACTUALIZADO CORRECTAMENTE","CORRECTIVO","success")})
        $scope.ds_listadoEquiposClimatizacionADetalleCorrectivo(r => {alert("EQUIPOS CLIMAT. A ACTUALIZADO CORRECTAMENTE","CORRECTIVO","success")})
        $scope.ds_listadoEquiposClimatizacionBDetalleCorrectivo(r => {alert("EQUIPOS CLIMAT. B ACTUALIZADO CORRECTAMENTE","CORRECTIVO","success")})
        $scope.ds_listadoEquiposRefrigeracionDetalleCorrectivo(r => {alert("EQUIPOS REFRIG. ACTUALIZADO CORRECTAMENTE","CORRECTIVO","success")})
        $scope.ds_listadoEquiposVentilacionDetalleCorrectivo(r => {alert("EQUIPOS VENTILACION ACTUALIZADO CORRECTAMENTE","CORRECTIVO","success")})
        $scope.ds_listadoEvaporadoresCorrectivo(r => {alert("EVAPORADORES ACTUALIZADO CORRECTAMENTE","CORRECTIVO","success")})
    }

}]);