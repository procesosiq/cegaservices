    /*==================================
    =            CALENDARIO            =
    ==================================*/
    var calendar = {
        fullcalendarOrder : $('#calendar_object'),
        config : function(Loadevents){

            if (!jQuery().fullCalendar) {
                throw new Error("No existe el plugin de calendario")
                return;
            }

            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();

            if (App.isRTL()) {
                if (this.fullcalendarOrder.parents(".portlet").width() <= 720) {
                    this.fullcalendarOrder.addClass("mobile");
                    h = {
                        right: 'title, prev, next',
                        center: '',
                        left: 'agendaDay, agendaWeek, month, today'
                    };
                } else {
                    this.fullcalendarOrder.removeClass("mobile");
                    h = {
                        right: 'title',
                        center: '',
                        left: 'agendaDay, agendaWeek, month, today, prev,next'
                    };
                }
            } else {
                if (this.fullcalendarOrder.parents(".portlet").width() <= 720) {
                    this.fullcalendarOrder.addClass("mobile");
                    h = {
                        left: 'title, prev, next',
                        center: '',
                        right: 'today,month,agendaWeek,agendaDay'
                    };
                } else {
                    this.fullcalendarOrder.removeClass("mobile");
                    h = {
                        left: 'title',
                        center: '',
                        right: 'prev,next,today,month,agendaWeek,agendaDay'
                    };
                }
            }

            var events = Loadevents || [{
                    title: 'Click for Google',
                    start: new Date(y, m, 28),
                    end: new Date(y, m, 29),
                    backgroundColor: App.getBrandColor('yellow'),
                    description : "Prueba de Evento",
                    // url: 'http://google.com/',
                }];

                // console.log(events)

            return { //re-initialize the calendar
                header: h,
                defaultView: 'agendaWeek', // change default view with available options from http://arshaw.com/fullcalendar/docs/views/Available_Views/ 
                slotMinutes: 15,
                lang: 'es',
                editable: true,
				eventDurationEditable:false,
                droppable: true, // this allows things to be dropped onto the calendar !!!
                events: events,
				drop : function(date, allDay) { // this function is called when something is dropped
					if($("#gruposcalendario").val()==0){
						console.log("todos");
					alert("Para poder agendar necesita seleccionar un grupo", "Ordenes", "error");}
				else{
                    // retrieve the dropped element's stored Event Object
                    var originalEventObject = $(this).data('eventObject');
                    // we need to copy it, so that multiple events don't have a reference to the same object
                    var copiedEventObject = $.extend({}, originalEventObject);

                    // assign it the date that was reported
                    copiedEventObject.start = date;
                    copiedEventObject.className = $(this).attr("data-class");
					var id = copiedEventObject.title.split(" ");
					if(parseInt(id[2])>0&&id[1]=="-"&& id.length>3){
					 id=id[0] + " - " + parseInt(id[2]);
					}
					else{
						id = id[0];
					}
					var data = {
						id: id,
						grupo: $("#gruposcalendario").val(),
						fecha: date.format("YYYY-MM-DD HH:mm")
					}
        	ahttp.post("./controllers/index.php?accion=Ordenes.editCalendar" , function(r, b){
				b();
				//console.log(r);
				eventCalendar();
				printCal();
				
				 //alert(' El evento ' + event.title + " ha sido movido " + event.start.format("YYYY-MM-DD HH:mm") , 'Ordenes' , 'info');
            	//$scope.formData.materiales = r.materiales;
				}, data);
				
                    // render the event on the calendar
                    // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                    $('#calendar_object').fullCalendar('renderEvent', copiedEventObject, true);

                    // is the "remove after drop" checkbox checked?
                    if ($('#drop-remove').is(':checked')) {
                        // if so, remove the element from the "Draggable Events" list
                        $(this).remove();
                    }
                }
				},
                eventClick: function(calEvent, jsEvent, view) {
                    var msj = 'Evento : ' + calEvent.title + ' <br> Fecha : ' + calEvent.start.format("YYYY-MM-DD HH:mm")
					+ ' <br> Cliente : ' + calEvent.cliente + ' <br> Tipo trabajo : ' + calEvent.tipo_trabajo 
					+ ' <br> <button class="btn btn-sm bg-red-thunderbird bg-font-red-thunderbird" onclick="removeEvent('+"'"+calEvent.title+"'"+')">Remover evento</button>';
                    alert(msj, 'Ordenes' , 'success');
                    // change the border color just for fun
                    $(this).css('border-color', 'red');

                },
                eventDrop: function(event, delta, revertFunc) {

					if (confirm("Esta seguro de cambiar el evento?")) {
                       
					var id =  event.title.split(" ");
					if(parseInt(id[2])>0&&id[1]=="-"&& id.length>3){
					 id=id[0] + " - " + parseInt(id[2]);
					}
					else{
						id = id[0];
					}

					var data = {
						id: id,
						grupo: 0,
						fecha: event.start.format("YYYY-MM-DD HH:mm")
					}
        	ahttp.post("./controllers/index.php?accion=Ordenes.editCalendar" , function(r, b){
				b();
				//console.log(r);
				eventCalendar();
				printCal();
				
				 alert(' El evento ' + event.title + " ha sido movido " + event.start.format("YYYY-MM-DD HH:mm") , 'Ordenes' , 'info');
            	//$scope.formData.materiales = r.materiales;
				}, data);
                   

                    
					}
					else {
						revertFunc();
					}
                
				}
            }
        },
        init : function(){
            // console.log(this.config());
            var config = this.config();
            this.fullcalendarOrder.fullCalendar('destroy'); // destroy the calendar
            this.fullcalendarOrder.fullCalendar(config);
        },
        reload : function(events){
			//console.log("entre");
            this.fullcalendarOrder.fullCalendar('destroy'); // destroy the calendar
            if(events)
                this.fullcalendarOrder.fullCalendar(this.config(events));
            else
                this.fullcalendarOrder.fullCalendar(this.config());
        }
    }
    calendar.init();
	var removeEvent = function(title){
	if (confirm("Esta seguro de remover el evento "+title+"?")) {
                       
					var id =  title.split(" ");
					if(parseInt(id[2])>0&&id[1]=="-"&& id.length>3){
					 id=id[0] + " - " + parseInt(id[2]);
					}
					else{
						id = id[0];
					}
					var data = {
						id: id,
					}
        	ahttp.post("./controllers/index.php?accion=Ordenes.removeCalendar" , function(r, b){
				b();
				//console.log(r);
				eventCalendar();
				printCal();
				
				 alert(' El evento ' + title + " ha sido removido" , 'Ordenes' , 'info');
            	//$scope.formData.materiales = r.materiales;
				}, data);
                   

                    
					}
}
    /*=====  End of CALENDARIO  ======*/

	$.fn.dataTable.pipeline = function ( opts ) {
    // Configuration options
    console.log(opts);
    var conf = $.extend( {
        pages: 5,     // number of pages to cache
        url: '',      // script url
        data: null,   // function or object with parameters to send to the server
                      // matching how `ajax.data` works in DataTables
        method: 'POST' // Ajax HTTP method
    }, opts );
 
    // Private variables for storing the cache
    var cacheLower = -1;
    var cacheUpper = null;
    var cacheLastRequest = null;
    var cacheLastJson = null;
 
    return function ( request, drawCallback, settings ) {
        var ajax          = false;
        var requestStart  = request.start;
        var drawStart     = request.start;
        var requestLength = request.length;
        var requestEnd    = requestStart + requestLength;
         
        if ( settings.clearCache ) {
            // API requested that the cache be cleared
            ajax = true;
            settings.clearCache = false;
        }
        else if ( cacheLower < 0 || requestStart < cacheLower || requestEnd > cacheUpper ) {
            // outside cached data - need to make a request
            ajax = true;
        }
        else if ( JSON.stringify( request.order )   !== JSON.stringify( cacheLastRequest.order ) ||
                  JSON.stringify( request.columns ) !== JSON.stringify( cacheLastRequest.columns ) ||
                  JSON.stringify( request.search )  !== JSON.stringify( cacheLastRequest.search )
        ) {
            // properties changed (ordering, columns, searching)
            ajax = true;
        }
         
        // Store the request for checking next time around
        cacheLastRequest = $.extend( true, {}, request );
 
        if ( ajax ) {
            // Need data from the server
            if ( requestStart < cacheLower ) {
                requestStart = requestStart - (requestLength*(conf.pages-1));
 
                if ( requestStart < 0 ) {
                    requestStart = 0;
                }
            }
             
            cacheLower = requestStart;
            cacheUpper = requestStart + (requestLength * conf.pages);
 
            request.start = requestStart;
            request.length = requestLength*conf.pages;
 
            // Provide the same `data` options as DataTables.
            console.log(conf);
            if ( $.isFunction ( conf.data ) ) {
                // As a function it is executed with the data object as an arg
                // for manipulation. If an object is returned, it is used as the
                // data object to submit
                var d = conf.data( request );
                if ( d ) {
                    $.extend( request, d );
                }
            }
            else if ( $.isPlainObject( conf.data ) ) {
                // As an object, the data given extends the default
                $.extend( request, conf.data );
            }

            settings.jqXHR = $.ajax( {
                "type":     conf.method,
                "url":      conf.url,
                "data":     request,
                "dataType": "json",
                "cache":    false,
                "success":  function ( json ) {
                    cacheLastJson = $.extend(true, {}, json);
 
                    if ( cacheLower != drawStart ) {
                        json.data.splice( 0, drawStart-cacheLower );
                    }
                    if($.isArray( json.data )){
                        json.data.splice( requestLength, json.data.length );
                    }
                    $('#datatable_ajax tbody').off( 'click', 'button');
                    $('#datatable_ajax').off('click', '.filter-submit');
                    $('#datatable_ajax').off('click', '.filter-cancel');
                    drawCallback( json );
                }
            } );
        }
        else {
            json = $.extend( true, {}, cacheLastJson );
            
            json.draw = request.draw; // Update the echo for each response
            console.log($.isPlainObject( json.data ));
            console.log($.isArray( json.data ));
            if($.isArray( json.data )){
                json.data.splice( 0, requestStart-cacheLower );
                json.data.splice( requestLength, json.data.length );
	            $('#datatable_ajax tbody').off( 'click', 'button');
	            $('#datatable_ajax').off('click', '.filter-submit');
	            $('#datatable_ajax').off('click', '.filter-cancel');
                drawCallback(json);
            }
        }
    }
};
 



// Register an API method that will empty the pipelined data, forcing an Ajax
// fetch on the next draw (i.e. `table.clearPipeline().draw()`)
$.fn.dataTable.Api.register( 'clearPipeline()', function () {
    return this.iterator( 'table', function ( settings ) {
        settings.clearCache = true;
    } );
} );
 

var grid = new Datatable(); 
    var TableDatatablesAjax = function () {

    var initPickers = function () {
        //init date pickers
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            autoclose: true
        });
    }

    var handleRecords = function () {

        

        grid.init({
            src: $("#datatable_ajax"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 

                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js). 
                // So when dropdowns used the scrollable div should be removed. 
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                
                // save datatable state(pagination, sort, etc) in cookie.
                // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                            [10, 20, 50, 100, 150, -1],
                            [10, 20, 50, 100, 150, "Todos"] // change per page values here
                ],
                "language": {
                    "lengthMenu": "Vista de _MENU_ registros por página",
                    "zeroRecords": "No se encontro ningun registro",
                    "info": "Página _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros disponibles",
                    "infoFiltered": "",
                    "aria": {
                        "sortAscending": ": orden acendente",
                        "sortDescending": ": orden decendente"
                    },
                    "emptyTable": "No hay registros en la tabla",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Ultimo",
                        "next":       "Siguiente",
                        "previous":   "Anterior",
                        "page": "Página",
                        "pageOf": "de"
                    },
                    "select": {
                        "rows": {
                            _: " %d filas selecionadas",
                            0: "",
                            1: "1 fila seleccionada"
                        }
                    }
                },
                "pageLength": 10, // default record count per page
                "ajax": $.fn.dataTable.pipeline( {
                            url: "./controllers/index.php?accion=Ordenes.ListOrden", // ajax source
                            pages: 10, // number of pages to cache,
                            data : function(){
                                var table = $('#datatable_ajax');
                                grid.params = {};
                                $('textarea.form-filter, select.form-filter, input.form-filter:not([type="radio"],[type="checkbox"])', table).each(function() {
                                    var _self = $(this);
                                    var name = _self.attr("name");
                                    var data = [];
                                    if (!data[name]) {
                                        data[name] = _self.val();
                                    }
                                    $.extend( grid.params, data);
                                });

                                // get all checkboxes
                                $('input.form-filter[type="checkbox"]:checked', table).each(function() {
                                    var _self = $(this);
                                    var name = _self.attr("name");
                                    var data = [];
                                    if (!data[name]) {
                                        data[name] = _self.val();
                                    }
                                    $.extend( grid.params, data);
                                });

                                // get all radio buttons
                                $('input.form-filter[type="radio"]:checked', table).each(function() {
                                    var _self = $(this);
                                    var name = _self.attr("name");
                                    var data = [];
                                    if (!data[name]) {
                                        data[name] = _self.val();
                                    }
                                    $.extend( grid.params, data);
                                });
                                // console.log(grid.params);
                                var data = {
                                    data : grid.params
                                }

                                return grid.params;
                            }
                } ),/*$.fn.dataTable.pipeline( {
                            url: "./controllers/index.php?accion=Ordenes.ListOrden", // ajax source
                            pages: 10, // number of pages to cache,
                            data : function(){
                                var table = $('#datatable_ajax');
                                grid.params = {};
                                $('textarea.form-filter, select.form-filter, input.form-filter:not([type="radio"],[type="checkbox"])', table).each(function() {
                                    var _self = $(this);
                                    var name = _self.attr("name");
                                    var data = [];
                                    if (!data[name]) {
                                        data[name] = _self.val();
                                    }
                                    $.extend( grid.params, data);
                                });

                                // get all checkboxes
                                $('input.form-filter[type="checkbox"]:checked', table).each(function() {
                                    var _self = $(this);
                                    var name = _self.attr("name");
                                    var data = [];
                                    if (!data[name]) {
                                        data[name] = _self.val();
                                    }
                                    $.extend( grid.params, data);
                                });

                                // get all radio buttons
                                $('input.form-filter[type="radio"]:checked', table).each(function() {
                                    var _self = $(this);
                                    var name = _self.attr("name");
                                    var data = [];
                                    if (!data[name]) {
                                        data[name] = _self.val();
                                    }
                                    $.extend( grid.params, data);
                                });
                                // console.log(grid.params);
                                var data = {
                                    data : grid.params
                                }

                                return grid.params;
                            }
                } ),*/
				"columnDefs": [
                            {
                                "targets": [ 0 ],
                                "visible": false,
                                "searchable": false
                            },
                            {
                                "targets": [ -1 ],  
                                "orderable": false
                            },
                            {
                                "targets": [ -3 ],  
                                "orderable": false
                            },
                            {
                                "targets": [ -4 ],  
                                "orderable": false
                            }
                        ],
                "order": [
                    [0, "desc"]
                ],// set first column as a default sort by asc
                "buttons": [
                            { extend: 'print', className: 'btn dark btn-outline' },
                            // { extend: 'copy', className: 'btn red btn-outline' },
                            { extend: 'pdf', className: 'btn green btn-outline' },
                            { extend: 'excel', className: 'btn yellow btn-outline ' },
                            { extend: 'csv', className: 'btn purple btn-outline ' }
                            // { extend: 'colvis', className: 'btn dark btn-outline', text: 'Columns'}
                            /*{
                                text: 'Nueva Orden',
                                className: 'btn blue btn-outline',
                                action: function ( e, dt, node, config ) {
                                    //dt.ajax.reload();
                                    // document.location.href = 'clientes';
                                    $('a[href="#registro"]').tab('show') 
                                }
                            }*/
                        ],
                select: false,
                "bSort": true,
                "processing": true,
                "serverSide": true,
                "dom": "<'row' <'col-md-12'B>><'row'><'row'<'col-md-6 col-sm-12'l>r><'table-scrollable't><'row'<'col-md-6 col-sm-12'i><'col-md-6 col-sm-12'p>>", // horizobtal scrollable datatable
                "fnDrawCallback": function( oSettings ) {
                        //alert("Cargo los datos");
                        var count = 0;
                        function details (event) {
                            console.log('ENTRA2');
                            //event.preventDefault();
                            //event.stopPropagation();
                            // console.log(grid.table.getSelectedRows());
                            // console.log($(this).parents('tr'));
                            // console.log(grid.table.getDataTable());
                            var btnid = $(this).prop("id");            
                            var data = grid.getDataTable().row( $(this).parents('tr') ).data();
							

                             if(data.length > 0){
                                if(btnid == "edit"){
                                    $("#registro").addClass("active");
                    $("#registro_2").addClass("active");
                    $("#listado").removeClass("active");
                    $("#listado_2").removeClass("active");
                    /* $.ajax({
                        type: 'POST',
                        url: 'controllers/index.php',
                        data: "accion=Ordenes.edit&idOrder="+data[1],
                        dataType: 'json',

                   $scope.formData.fecha = ordn.fecha;
                    $scope.formData.cliente = ordn.cliente;
                    //$scope.formData.tipo_cliente = ordn.tipo_cliente;
                    $scope.formData.direccion = ordn.direccion;
                    $scope.formData.tipos_trabajo = ordn.tipo_trabajo;
                    $scope.formData.tiempo = ordn.tiempo_estimado;

                    });*/

                    $("#oculto").val(data[0]);
                    angular.element($('#orderController')).scope().get(data[0], data[1]);
                    return false;
                    $('a[href="#registro"]').tab('show');
                                }
                                else if(btnid == "status"){
                                       if(confirm("¿Esta seguro de cambiar el registro?")){
                         $.ajax({
                            type: "POST",
                            url: "controllers/index.php",
                            data: "accion=Ordenes.ChangeStatus&idorden="+data[0],
                            success: function(msg){
								console.log(msg);
                                alert("Registro cambiado" , "Ordenes" , 'success' , function(){
                                    grid.getDataTable().clearPipeline().draw();
                                });
                            }
                        });
                    }
                                    
                                }
                                else if(btnid == "garantia"){
                                       if(confirm("¿Esta seguro de crear una garantia de la orden " +data[1]+"?")){
                         $.ajax({
                            type: "POST",
                            url: "controllers/index.php",
                            data: "accion=Ordenes.CrearGarantia&codigo="+data[1]+"&idorden="+data[0],
                            success: function(msg){
								console.log(msg);
                                alert("Registro cambiado" , "Ordenes" , 'success' , function(){
                                    grid.getDataTable().clearPipeline().draw();
                                });
                            }
                        });
                    }
                                    
                                }
                             }
                        }
                        $('#datatable_ajax tbody > button').unbind('click', details)
                        $('#datatable_ajax_tools > li > a.tool-action').on('click', function(event) {
                            event.preventDefault();
                            var action = $(this).attr('data-action');
                            grid.getDataTable().button(action).trigger();
                        });

                        $('#datatable_ajax tbody').on( 'click', 'button',  details);
                        
                        $('#datatable_ajax').on('click', '.filter-submit', function(e) {
                            grid.getDataTable().clearPipeline().draw();
                        });

                        $('#datatable_ajax').on('click', '.filter-cancel', function(e) {
                             $('textarea.form-filter, select.form-filter, input.form-filter', $('#datatable_ajax')).each(function() {
                                $(this).val("");
                            });
                            $('input.form-filter[type="checkbox"]', $('#datatable_ajax')).each(function() {
                                $(this).attr("checked", false);
                            });
                            grid.getDataTable().clearPipeline().draw();
                        });

                 }  
				}
        });

        // handle group actionsubmit button click
        grid.getTableWrapper().on('click', '.table-group-action-submit', function (e) {
            e.preventDefault();
            var action = $(".table-group-action-input", grid.getTableWrapper());
            if (action.val() != "" && grid.getSelectedRowsCount() > 0) {
                grid.setAjaxParam("customActionType", "group_action");
                grid.setAjaxParam("customActionName", action.val());
                grid.setAjaxParam("id", grid.getSelectedRows());
                grid.getDataTable().ajax.reload();
                grid.clearAjaxParams();
            } else if (action.val() == "") {
                App.alert({
                    type: 'danger',
                    icon: 'warning',
                    message: 'Please select an action',
                    container: grid.getTableWrapper(),
                    place: 'prepend'
                });
            } else if (grid.getSelectedRowsCount() === 0) {
                App.alert({
                    type: 'danger',
                    icon: 'warning',
                    message: 'No record selected',
                    container: grid.getTableWrapper(),
                    place: 'prepend'
                });
            }
        });

        grid.setAjaxParam("customActionType", "group_action");
        grid.getDataTable().ajax.reload();
        grid.clearAjaxParams();

        $('#datatable_ajax_tools > li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            grid.getDataTable().button(action).trigger();
        });

        $('#datatable_ajax tbody').on( 'click', 'button', function () {
            // console.log(grid.table.getSelectedRows());
            // console.log($(this).parents('tr'));
            // console.log(grid.table.getDataTable());
            var btnid = $(this).prop("id");            
            var data = grid.getDataTable().row( $(this).parents('tr') ).data();
             if(data.length > 0){
                if(btnid == "edit"){
                    $("#registro").addClass("active");
                    $("#registro_2").addClass("active");
                    $("#listado").removeClass("active");
                    $("#listado_2").removeClass("active");
                    /* $.ajax({
                        type: 'POST',
                        url: 'controllers/index.php',
                        data: "accion=Ordenes.edit&idOrder="+data[1],
                        dataType: 'json',

                   $scope.formData.fecha = ordn.fecha;
                    $scope.formData.cliente = ordn.cliente;
                    //$scope.formData.tipo_cliente = ordn.tipo_cliente;
                    $scope.formData.direccion = ordn.direccion;
                    $scope.formData.tipos_trabajo = ordn.tipo_trabajo;
                    $scope.formData.tiempo = ordn.tiempo_estimado;

                    });*/

                    $("#oculto").val(data[0]);
                    angular.element($('#orderController')).scope().get(data[0],data[1]);
                    return false;
                    $('a[href="#registro"]').tab('show');
                    
                }else if(btnid == "status"){
                    
                }
             }
        } );
    }

    return {

        //main function to initiate the module
        init: function () {

            initPickers();
            handleRecords();
        }

    };

}();

var printCal = function (){
	
	var data = {
			grupo: $("#gruposcalendario").val(),
        	}
        	ahttp.post("./controllers/index.php?accion=Ordenes.calendario" , function(r, b){
				b();
				printCalendar(r,b);
            	//$scope.formData.materiales = r.materiales;
            }, data);
}
 $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
          // e.target // newly activated tab
          var tab = $(e.target).attr("href");
          if(tab == "#calendar_tab"){
			  eventCalendar();
				printCal();
          }
		  else if(tab == "#listado"){
				
			grid.getDataTable().clearPipeline().draw();
		  }
          // e.relatedTarget // previous active tab
        });
		
		var initDrag = function(el) {
                // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
                // it doesn't need to have a start or end
                var eventObject = {
                    title: $.trim(el.text()) // use the element's text as the event title
                };
                // store the Event Object in the DOM element so we can get to it later
                el.data('eventObject', eventObject);
                // make the event draggable using jQuery UI
                el.draggable({
                    zIndex: 999,
                    revert: true, // will cause the event to go back to its
                    revertDuration: 0 //  original position after the drag
                });
            };

            addEvent = function(title) {
                title = title.length === 0 ? "Untitled Event" : title;
                var html = $('<div class="external-event label label-default">' + title + '</div>');
                jQuery('#event_box').append(html);
                initDrag(html);
            };

				
var printCalendar = function(r , b){
   b();
   console.log(r);
   if(r){
    var events = [] , calendario = {} , fecha ;
    for(var d in r.calendario){
     
     fecha = moment(r.calendario[d]['fecha_agendada']).format("YYYY-MM-DD HH:mm");
	 
	 fecha2 = moment(r.calendario[d]['fecha_fin']).format("YYYY-MM-DD HH:mm");
     // fecha = moment(r.data[d][2]).format("id");
	 if(r.calendario[d]['grupo_trabajo']==1){
     events.push({
      title :  r.calendario[d]['id'],
      tipo_trabajo :  r.calendario[d]['tipo_trabajo'],
      cliente :  r.calendario[d]['cliente'],
      start : new Date(fecha),
      end : new Date(fecha2),
      backgroundColor :	'#BF55EC',
      description : "Orden tipo "+ r.calendario[d]['id']+ " para el Cliente "+r.calendario[d]['fecha_agendada'] ,
     });}
	 else if(r.calendario[d]['grupo_trabajo']==2){
     events.push({
      title :  r.calendario[d]['id'],
      tipo_trabajo :  r.calendario[d]['tipo_trabajo'],
      cliente :  r.calendario[d]['cliente'],
      start : new Date(fecha),
      end : new Date(fecha2),
      backgroundColor : App.getBrandColor('green'),
      description : "Orden tipo "+ r.calendario[d]['id']+ " para el Cliente "+r.calendario[d]['fecha_agendada'] ,
     });}
	 else if(r.calendario[d]['grupo_trabajo']==3){
     events.push({
      title :  r.calendario[d]['id'],
      tipo_trabajo :  r.calendario[d]['tipo_trabajo'],
      cliente :  r.calendario[d]['cliente'],
      start : new Date(fecha),
      end : new Date(fecha2),
      backgroundColor : App.getBrandColor('yellow'),
      description : "Orden tipo "+ r.calendario[d]['id']+ " para el Cliente "+r.calendario[d]['fecha_agendada'] ,
     });}
	 else if(r.calendario[d]['grupo_trabajo']==4){
     events.push({
      title :  r.calendario[d]['id'],
      tipo_trabajo :  r.calendario[d]['tipo_trabajo'],
      cliente :  r.calendario[d]['cliente'],
      start : new Date(fecha),
      end : new Date(fecha2),
      backgroundColor : '#b5824a',
      description : "Orden tipo "+ r.calendario[d]['id']+ " para el Cliente "+r.calendario[d]['fecha_agendada'] ,
     });}
	 
     //console.log(r.calendario[d]['fecha_agendada']);
    }
    calendar.reload(events);
   }
  }
  
  var eventCalendar = function () {
	  $('#event_box').empty();
  var data = {
        	}
        	ahttp.post("./controllers/index.php?accion=Ordenes.calendarioEvent" , function(r, b){
				b();
				console.log(r.calendario);
				for(var d in r.calendario){
				addEvent(''+r.calendario[d]['id']);
				}
            	//$scope.formData.materiales = r.materiales;
            }, data);
  }
jQuery(document).ready(function() {
	console.log(ahttp);
    TableDatatablesAjax.init();
	$('#summernote_1').summernote({height: 300});
	var tiempo = $('#clockface_2').clockface('getTime');
});

    // var app = angular.module('app', []);
    app.controller('orderController', ['$scope','$http','$interval','$controller' , 'client', function($scope,$http,$interval,$controller ,client){
        $scope.formData = {
        	areas : [],
        	roles : [],
        	trabajos_tipos : ['Correctivo' , 'Instalacion' , 'Mantenimiento'],
            id_orden : 0
        };

        $scope.get = function(id, codigo){
            $scope.limpiar();
			console.log(id);
            if(id > 0){
                $scope.getOrden(id, codigo);
            }
        }
		$scope.editRowMaterial = function(data , index){
		if(data){
			$scope.formData.materiales[index].edit = 1;
		}
		}
		$scope.editRowRepuesto = function(data , index){
		if(data){
			$scope.formData.tableRepuesto[index].edit = 1;
		}
		}
		$scope.editRowHerramienta = function(data , index){
		if(data){
			$scope.formData.herramientas[index].edit = 1;
		}
		}
		$scope.saveRowMaterial = function(material , index){
			var data = {
				id_cotizacion : $scope.formData.id_order,
				id_material : $scope.formData.materiales[index]['id'],
        		cantidad : $("#cantidad"+index).val(),
				codigoOrden : $scope.codigoOrden,
        	}
			 client.post("./controllers/index.php?accion=Ordenes.editMaterial" , function(r, b){
				b();
            	$scope.formData.materiales = r.materiales;
            }, data);
		}
		$scope.saveRowRepuesto = function(material , index){
			var data = {
				id_cotizacion : $scope.formData.id_order,
				id : $scope.formData.tableRepuesto[index]['id'],
        		cantidad : $("#repuesto"+index).val(),
				codigoOrden : $scope.codigoOrden,
        	}
			 client.post("./controllers/index.php?accion=Ordenes.editRepuesto" , function(r, b){
				b();
            	$scope.formData.tableRepuesto = r.repuestos;
            }, data);
		}
		$scope.saveRowHerramienta = function(material , index){
			var data = {
				id_cotizacion : $scope.formData.id_order,
				id_herramienta : $scope.formData.herramientas[index]['id'],
        		herramienta : $("#herramienta"+index).val(),
        		requerimientos : $("#requerimientos"+index).val(),
				codigoOrden : $scope.codigoOrden,				
        	}
			 client.post("./controllers/index.php?accion=Ordenes.editHerramienta" , function(r, b){
				b();
            	$scope.formData.herramientas = r.herramientas;
            }, data);
		}
		
		
  
        $scope.getOrden = function (ordenID, codigo){
            $scope.formData.id_order = ordenID;
			$scope.codigoOrden = codigo;
            var data = {
            id_orden: ordenID,
            codigo: $scope.codigoOrden,
             }
            client.post("./controllers/index.php?accion=Ordenes.edit" , function(r, b){
            b();
            $("a[href='#registro']").tab("show");
                    //$("#registro").css("active");
                   // $("#registro_2").css("active");
                    //$("#listado").css("active");
                    //$("#listado_2").css("active");
                 var reg = angular.element( document.querySelector( '#registro' ) );
                 reg.addClass('active'); 
                 var reg2 = angular.element( document.querySelector( '#registro_2' ) );
                 reg2.addClass('active');  
                var lis = angular.element( document.querySelector( '#listado' ) );
                lis.removeClass('active');  
                var lis2 = angular.element( document.querySelector( '#listado_2' ) );
                lis2.removeClass('active'); 
				$scope.formData.insTab = 0;
				$scope.formData.manTab = 0;
				$scope.formData.corTab = 0;
				$scope.formData.revTab = 0;
				var classactive = 0;
				var tip = r.data.tipo_trabajo.split(", ");
				console.log(r);
                for(var x = 0; x<tip.length; x++){
					if(tip[x]=="INSTALACION"){
					$scope.formData.insTab = 1;
					classactive = 1;
					}
					else if(tip[x]=="MANTENIMIENTO"){
					$scope.formData.manTab = 1;
					if(classactive!=1){
					classactive = 2;
					}
					}
					else if(tip[x]=="CORRECTIVO"){
					$scope.formData.corTab = 1;
					if(classactive!=2 || classactive!=1){
					classactive = 3;
					}
					}
					else if(tip[x]=="REVISION"){
						$scope.formData.revTab = 1;
					if(classactive!=2 || classactive!=1|| classactive!=3){
					classactive = 4;
					}
					}
				}
				if(classactive==1){
					$("#instalacion_tab").addClass("active");
					$("#revision_tab").removeClass("active");
					$("#mantenimiento_tab").removeClass("active");
					$("#correctivo_tab").removeClass("active");
				}
				else if(classactive==2){
					$("#mantenimiento_tab").addClass("active");
					$("#revision_tab").removeClass("active");
					$("#instalacion_tab").removeClass("active");
					$("#correctivo_tab").removeClass("active");
				}
				else if(classactive==3){
					$("#correctivo_tab").addClass("active");
					$("#revision_tab").removeClass("active");
					$("#instalacion_tab").removeClass("active");
					$("#mantenimiento_tab").removeClass("active");
				}
				else if(classactive==4){
					$("#revision_tab").addClass("active");
					$("#correctivo_tab").removeClass("active");
					$("#instalacion_tab").removeClass("active");
					$("#mantenimiento_tab").removeClass("active");
				}
				//alert(classactive);
				//printCalendar(r,b);
                $scope.formData.fecha = r.data.fecha_aprobacion;
                $scope.formData.cliente = r.data.nombre;
                $scope.formData.id_orden = r.data.id_orden;
                $scope.formData.sucursal = r.data.sucursaaaal;
                $scope.formData.tipo_cliente = r.data.tipo_cliente;
                $scope.formData.direccion = r.data.direccion;
                $scope.formData.tipos_trabajo = r.data.tipo_trabajo;
                $scope.formData.tiempo = r.data.tiempo_estimado;
				$scope.formData.materiales = r.materiales;
				$scope.formData.equiposVal = r.equiposVal;
				$("#summernote_1").code(r.data.obser);
                $scope.formData.observaciones = r.data.obser;
				$scope.formData.insclimA = r.insclimA;
				$scope.formData.manclimA = r.manclimA;
				$scope.formData.corclimA = r.corclimA;
				$scope.formData.revclimA = r.revclimA;
				$scope.formData.insclimB = r.insclimB;
				$scope.formData.manclimB = r.manclimB;
				$scope.formData.tableRepuesto = r.repuestos;
				$scope.formData.corclimB = r.corclimB;
				$scope.formData.revclimB = r.revclimB;
				$scope.validItems = 0;
				$scope.validParte = 0;
				$scope.formData.herramientas2 = r.herramientas2;
				$scope.formData.materiales2 = r.materiales2;
				$scope.formData.insVentilacion = r.insVentilacion;
				$scope.formData.manVentilacion = r.manVentilacion;
				$scope.formData.corVentilacion = r.corVentilacion;
				$scope.formData.revVentilacion = r.revVentilacion;
				$scope.formData.insRefri = r.insRefri;
				$scope.formData.manRefri = r.manRefri;
				$scope.formData.corRefri = r.corRefri;
				$scope.formData.revRefri = r.revRefri;
				$scope.formData.herramientas = r.herramientas;
				$scope.formData.equiposclimA = r.equiposclimA;
				$scope.formData.equiposclimB = r.equiposclimB;
				$scope.formData.equiposRefri = r.equiposRefri;
				$scope.formData.equiposVentilacion = r.equiposVentilacion;
                $('#clockface_2').clockface('setTime',  $scope.formData.tiempo);

            }, data);
        }


		$scope.addRowMateriales = function(){
			var id_mat = 0;
			for(x=0;x<$scope.formData.materiales2.length; x++){
				if($scope.formData.materiales2[x].item == $("#materiales2").val()){
					id_mat = $scope.formData.materiales2[x].id;
					x=$scope.formData.materiales2.length +1;
				}
			}
			var codigo = 0;
			for(x=0;x<$scope.formData.equiposVal.length; x++){
				if($scope.formData.equiposVal[x].dos == $("#equiposVal").val()){
					codigo = $scope.formData.equiposVal[x].codigo;
					x=$scope.formData.equiposVal.length +1;
				}
			}
			if(parseInt($("#cantidad").val())<0 || $("#cantidad").val()==""){
				alert("Ingresar una cantidad positiva", "Ordenes de trabajo", "error");
				return false;
			}
			
			if(id_mat > 0 && codigo != 0){
        	var data = {
				id_cotizacion : $scope.formData.id_order,
        		item : id_mat,
        		codigo : codigo,
        		codigoOrden : $scope.codigoOrden,
        		cantidad : $("#cantidad").val()
        	}
			 client.post("./controllers/index.php?accion=Ordenes.addMaterial" , function(r, b){
				b();
            	$scope.formData.materiales = r.materiales;
				$("#materiales2").val("");
				$("#cantidad").val("");
            }, data);}
			else{
				
                alert("Favor de elegir un material existente", "Ordenes de trabajo", "error");
			}
        }
		
		$scope.addRowRepuestos = function(){
			var item = 0;
			for(x=0;x<$scope.formData.repuestos.length; x++){
				if($scope.formData.repuestos[x].descripcion == $("#repuestos").val()){
					item = $scope.formData.repuestos[x].descripcion;
					x=$scope.formData.repuestos.length +1;
				}
			}
			var codigo = 0;
			var descripcion = 0;
			for(x=0;x<$scope.formData.equiposVal.length; x++){
				if($scope.formData.equiposVal[x].dos == $("#equiposValR").val()){
					codigo = $scope.formData.equiposVal[x].codigo;
					descripcion = $scope.formData.equiposVal[x].nombre;
					x=$scope.formData.equiposVal.length +1;
				}
			}
			var id_parte = 0;
			for(x=0;x<$scope.formData.parte.length; x++){
				if($scope.formData.parte[x].nombre == $scope.equiposValParte){
					id_parte = $scope.formData.parte[x].id;
					x=$scope.formData.parte.length +1;
				}
			}
			if(parseInt($("#cantidadRep").val())<0 || $("#cantidadRep").val()==""){
				alert("Ingresar una cantidad positiva", "Ordenes de trabajo", "error");
				return false;
			}
			
			if(item != 0 && codigo != 0 && descripcion !=0 && id_parte>0){
        	var data = {
				id_cotizacion : $scope.formData.id_order,
        		codigo : codigo,
        		descripcion : descripcion,
        		parte : $scope.equiposValParte,
        		item : item,
        		cantidad : $("#cantidadRep").val(),
				codigoOrden : $scope.codigoOrden,
        	}
			console.log(data);
			 client.post("./controllers/index.php?accion=Ordenes.addRepuesto" , function(r, b){
				b();
				console.log(r);
            	$scope.formData.tableRepuesto = r.repuestos;
            	$scope.validParte = 0;
            	$scope.validItems = 0;
				$("#equiposValR").val("");
				$("#parte").val("");
				$("#repuestos").val("");
				$("#cantidadRep").val("");
            }, data);}
			else{
				
                alert("Favor de lenar con informacion correcta los campos", "Ordenes de trabajo", "error");
			}
        }
		
		$scope.addRowinsclimA = function(){
        	var data = {
				id_cotizacion : $scope.formData.id_order,
        		equipo : $("#equipo").val(),
				table : 'ins_caracteristicas_climatizacion_a'
        		
        	}
			 client.post("./controllers/index.php?accion=Ordenes.addinsclimA" , function(r, b){
				b();
            	$scope.formData.insclimA = r.insclimA;
				$scope.formData.equiposVal = r.equiposVal;
            }, data);
        }
		
		$scope.addRowmanclimA = function(){
        	var data = {
				id_cotizacion : $scope.formData.id_order,
        		equipo : $("#equipoMA").val(),
				table : 'man_caracteristicas_climatizacion_a'
        		
        	}
			 client.post("./controllers/index.php?accion=Ordenes.addinsclimA" , function(r, b){
				b();
            	$scope.formData.manclimA = r.insclimA;
				$scope.formData.equiposVal = r.equiposVal;
            }, data);
        }
		
		$scope.addRowcorclimA = function(){
        	var data = {
				id_cotizacion : $scope.formData.id_order,
        		equipo : $("#equipoCA").val(),
				table : 'cor_caracteristicas_climatizacion_a'
        		
        	}
			 client.post("./controllers/index.php?accion=Ordenes.addinsclimA" , function(r, b){
				b();
            	$scope.formData.corclimA = r.insclimA;
				$scope.formData.equiposVal = r.equiposVal;
            }, data);
        }
		$scope.addRowrevclimA = function(){
        	var data = {
				id_cotizacion : $scope.formData.id_order,
        		equipo : $("#equipoRA").val(),
				table : 'rev_caracteristicas_climatizacion_a'
        		
        	}
			 client.post("./controllers/index.php?accion=Ordenes.addinsclimA" , function(r, b){
				b();
            	$scope.formData.revclimA = r.insclimA;
				$scope.formData.equiposVal = r.equiposVal;
            }, data);
        }
		
		$scope.addRowinsclimB = function(){
        	var data = {
				id_cotizacion : $scope.formData.id_order,
        		equipo : $("#equipoB").val(),
				table : 'ins_caracteristicas_climatizacion_b'
        		
        	}
			 client.post("./controllers/index.php?accion=Ordenes.addinsclimB" , function(r, b){
				b();
				//console.log(r.data);
            	$scope.formData.insclimB = r.insclimB;
				$scope.formData.equiposVal = r.equiposVal;
            }, data);
        }
		
		$scope.addRowmanclimB = function(){
        	var data = {
				id_cotizacion : $scope.formData.id_order,
        		equipo : $("#equipoMB").val(),
        		table : 'man_caracteristicas_climatizacion_b'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.addinsclimB" , function(r, b){
				b();
				//console.log(r.data);
            	$scope.formData.manclimB = r.insclimB;
				$scope.formData.equiposVal = r.equiposVal;
            }, data);
        }
		
		$scope.addRowcorclimB = function(){
        	var data = {
				id_cotizacion : $scope.formData.id_order,
        		equipo : $("#equipoCB").val(),
        		table : 'cor_caracteristicas_climatizacion_b'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.addinsclimB" , function(r, b){
				b();
				//console.log(r.data);
            	$scope.formData.corclimB = r.insclimB;
				$scope.formData.equiposVal = r.equiposVal;
            }, data);
        }
		
		$scope.addRowrevclimB = function(){
        	var data = {
				id_cotizacion : $scope.formData.id_order,
        		equipo : $("#equipoRB").val(),
        		table : 'rev_caracteristicas_climatizacion_b'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.addinsclimB" , function(r, b){
				b();
				//console.log(r.data);
            	$scope.formData.revclimB = r.insclimB;
				$scope.formData.equiposVal = r.equiposVal;
            }, data);
        }
		
		$scope.addRowinsVenti = function(){
        	var data = {
				id_cotizacion : $scope.formData.id_order,
        		equipo : $("#equipoV").val(),
        		table : 'ins_caracteristicas_ventilacion'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.addinsVenti" , function(r, b){
				b();
            	$scope.formData.insVentilacion = r.insVentilacion;
				$scope.formData.equiposVal = r.equiposVal;
				
            }, data);
        }
		
		$scope.addRowmanVenti = function(){
        	var data = {
				id_cotizacion : $scope.formData.id_order,
        		equipo : $("#equipoMV").val(),
        		table : 'man_caracteristicas_ventilacion'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.addinsVenti" , function(r, b){
				b();
            	$scope.formData.manVentilacion = r.insVentilacion;
				$scope.formData.equiposVal = r.equiposVal;
            }, data);
        }
		
		$scope.addRowcorVenti = function(){
        	var data = {
				id_cotizacion : $scope.formData.id_order,
        		equipo : $("#equipoCV").val(),
        		table : 'cor_caracteristicas_ventilacion'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.addinsVenti" , function(r, b){
				b();
            	$scope.formData.corVentilacion = r.insVentilacion;
				$scope.formData.equiposVal = r.equiposVal;
            }, data);
        }
		
		$scope.addRowrevVenti = function(){
        	var data = {
				id_cotizacion : $scope.formData.id_order,
        		equipo : $("#equipoRV").val(),
        		table : 'rev_caracteristicas_ventilacion'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.addinsVenti" , function(r, b){
				b();
            	$scope.formData.revVentilacion = r.insVentilacion;
				$scope.formData.equiposVal = r.equiposVal;
            }, data);
        }
		
		$scope.addRowinsRefri = function(){
        	var data = {
				id_cotizacion : $scope.formData.id_order,
        		equipo : $("#equipoR").val(),
        		table : 'ins_caracteristicas_refrigeracion'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.addinsRefri" , function(r, b){
				b();
            	$scope.formData.insRefri = r.insRefri;
				$scope.formData.equiposVal = r.equiposVal;
            }, data);
        }
		
		$scope.addRowmanRefri = function(){
        	var data = {
				id_cotizacion : $scope.formData.id_order,
        		equipo : $("#equipoMR").val(),
        		table : 'man_caracteristicas_refrigeracion'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.addinsRefri" , function(r, b){
				b();
            	$scope.formData.manRefri = r.insRefri;
				$scope.formData.equiposVal = r.equiposVal;
            }, data);
        }
		
		$scope.addRowcorRefri = function(){
        	var data = {
				id_cotizacion : $scope.formData.id_order,
        		equipo : $("#equipoCR").val(),
        		table : 'cor_caracteristicas_refrigeracion'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.addinsRefri" , function(r, b){
				b();
            	$scope.formData.corRefri = r.insRefri;
				$scope.formData.equiposVal = r.equiposVal;
            }, data);
        }
		
		$scope.addRowrevRefri = function(){
        	var data = {
				id_cotizacion : $scope.formData.id_order,
        		equipo : $("#equipoRR").val(),
        		table : 'rev_caracteristicas_refrigeracion'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.addinsRefri" , function(r, b){
				b();
            	$scope.formData.revRefri = r.insRefri;
				$scope.formData.equiposVal = r.equiposVal;
            }, data);
        }
		
		$scope.addRowHerramientas = function(){
			var id_her = 0;
			for(x=0;x<$scope.formData.herramientas2.length; x++){
				if($scope.formData.herramientas2[x].herramienta == $("#herramientas").val()){
					id_her = $scope.formData.herramientas2[x].id;
					x=$scope.formData.herramientas2.length +1;
				}
			}

			//console.log(l);
			if(id_her > 0){
				var data = {
				id_cotizacion : $scope.formData.id_order,
        		herramienta : id_her,
				codigoOrden : $scope.codigoOrden,
        	}
			 client.post("./controllers/index.php?accion=Ordenes.addHerramienta" , function(r, b){
				b();
				console.log(r);
            	$scope.formData.herramientas = r.herramientas;
				$("#herramientas").val("");
            }, data);
        	}
			else{
				
                alert("Favor de elegir una herramienta existente", "Ordenes de trabajo", "error");
			}
        	
        }

		$scope.deleteRepuesto = function(item , index){
			
        	var data = {
				id : $scope.formData.tableRepuesto[index]['id'],
				id_cotizacion : $scope.formData.id_order,
				codigoOrden : $scope.codigoOrden,
        	}
			console.log(data);
			 client.post("./controllers/index.php?accion=Ordenes.deleteRepuesto" , function(r, b){
				b();
				console.log(r);
            	$scope.formData.tableRepuesto = r.repuestos;
            }, data);
        	
        }
		$scope.deleteMaterial = function(item , index){
			
        	var data = {
				id_material : $scope.formData.materiales[index]['id'],
				id_cotizacion : $scope.formData.id_order,
				codigoOrden : $scope.codigoOrden,
        	}
			console.log(data);
			 client.post("./controllers/index.php?accion=Ordenes.deleteMaterial" , function(r, b){
				b();
				console.log(r);
            	$scope.formData.materiales = r.materiales;
            }, data);
        	
        }
		

		$scope.deleteinsclimA = function(item , index){
        	var data = {
				id : $scope.formData.insclimA[index]['id'],
				id_cotizacion : $scope.formData.id_order,
				table : 'ins_caracteristicas_climatizacion_a'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.deleteinsclimA" , function(r, b){
				b();
            	$scope.formData.insclimA = r.insclimA;
				$scope.formData.equiposVal = r.equiposVal;
            }, data);
        	
        }
		
		$scope.deletemanclimA = function(item , index){
        	var data = {
				id : $scope.formData.manclimA[index]['id'],
				id_cotizacion : $scope.formData.id_order,
				table : 'man_caracteristicas_climatizacion_a'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.deleteinsclimA" , function(r, b){
				b();
            	$scope.formData.manclimA = r.insclimA;
				$scope.formData.equiposVal = r.equiposVal;
            }, data);
        	
        }
		
		$scope.deletecorclimA = function(item , index){
        	var data = {
				id : $scope.formData.corclimA[index]['id'],
				id_cotizacion : $scope.formData.id_order,
				table : 'cor_caracteristicas_climatizacion_a'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.deleteinsclimA" , function(r, b){
				b();
            	$scope.formData.corclimA = r.insclimA;
				$scope.formData.equiposVal = r.equiposVal;
            }, data);
        	
        }
		
		$scope.deleterevclimA = function(item , index){
        	var data = {
				id : $scope.formData.revclimA[index]['id'],
				id_cotizacion : $scope.formData.id_order,
				table : 'rev_caracteristicas_climatizacion_a'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.deleteinsclimA" , function(r, b){
				b();
            	$scope.formData.revclimA = r.insclimA;
				$scope.formData.equiposVal = r.equiposVal;
            }, data);
        	
        }
		
		$scope.deleteinsVenti = function(item , index){
        	var data = {
				id : $scope.formData.insVentilacion[index]['id'],
				id_cotizacion : $scope.formData.id_order,
				table : 'ins_caracteristicas_ventilacion'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.deleteinsVentilacion" , function(r, b){
				b();
            	$scope.formData.insVentilacion = r.insVentilacion;
				$scope.formData.equiposVal = r.equiposVal;
            }, data);
        	
        }
		
		$scope.deletemanVenti = function(item , index){
        	var data = {
				id : $scope.formData.manVentilacion[index]['id'],
				id_cotizacion : $scope.formData.id_order,
				table : 'man_caracteristicas_ventilacion'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.deleteinsVentilacion" , function(r, b){
				b();
            	$scope.formData.manVentilacion = r.insVentilacion;
				$scope.formData.equiposVal = r.equiposVal;
            }, data);
        	
        }
		
		$scope.deletecorVenti = function(item , index){
        	var data = {
				id : $scope.formData.corVentilacion[index]['id'],
				id_cotizacion : $scope.formData.id_order,
				table : 'cor_caracteristicas_ventilacion'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.deleteinsVentilacion" , function(r, b){
				b();
            	$scope.formData.corVentilacion = r.insVentilacion;
				$scope.formData.equiposVal = r.equiposVal;
            }, data);
        	
        }
		
		$scope.deleterevVenti = function(item , index){
        	var data = {
				id : $scope.formData.revVentilacion[index]['id'],
				id_cotizacion : $scope.formData.id_order,
				table : 'rev_caracteristicas_ventilacion'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.deleteinsVentilacion" , function(r, b){
				b();
            	$scope.formData.revVentilacion = r.insVentilacion;
				$scope.formData.equiposVal = r.equiposVal;
            }, data);
        	
        }
		
		$scope.deleteinsRefri = function(item , index){
        	var data = {
				id : $scope.formData.insRefri[index]['id'],
				id_cotizacion : $scope.formData.id_order,
				table : 'ins_caracteristicas_refrigeracion'				
        	}
			 client.post("./controllers/index.php?accion=Ordenes.deleteinsRefri" , function(r, b){
				b();
            	$scope.formData.insRefri = r.insRefri;
				$scope.formData.equiposVal = r.equiposVal;
            }, data);
        	
        }
		
		$scope.deletemanRefri = function(item , index){
        	var data = {
				id : $scope.formData.manRefri[index]['id'],
				id_cotizacion : $scope.formData.id_order,
				table : 'man_caracteristicas_refrigeracion'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.deleteinsRefri" , function(r, b){
				b();
            	$scope.formData.manRefri = r.insRefri;
				$scope.formData.equiposVal = r.equiposVal;
            }, data);
        	
        }
		$scope.deletecorRefri = function(item , index){
        	var data = {
				id : $scope.formData.corRefri[index]['id'],
				id_cotizacion : $scope.formData.id_order,
				table : 'cor_caracteristicas_refrigeracion'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.deleteinsRefri" , function(r, b){
				b();
            	$scope.formData.corRefri = r.insRefri;
				$scope.formData.equiposVal = r.equiposVal;
            }, data);
        	
        }
		$scope.deleterevRefri = function(item , index){
        	var data = {
				id : $scope.formData.revRefri[index]['id'],
				id_cotizacion : $scope.formData.id_order,
				table : 'rev_caracteristicas_refrigeracion'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.deleteinsRefri" , function(r, b){
				b();
            	$scope.formData.revRefri = r.insRefri;
				$scope.formData.equiposVal = r.equiposVal;
            }, data);
        	
        }
		$scope.deleteinsclimB = function(item , index){
        	var data = {
				id : $scope.formData.insclimB[index]['id'],
				id_cotizacion : $scope.formData.id_order,
				table : 'ins_caracteristicas_climatizacion_b'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.deleteinsclimB" , function(r, b){
				b();
            	$scope.formData.insclimB = r.insclimB;
				$scope.formData.equiposVal = r.equiposVal;
            }, data);
        	
        }
		
		$scope.deletemanclimB = function(item , index){
        	var data = {
				id : $scope.formData.manclimB[index]['id'],
				id_cotizacion : $scope.formData.id_order,
				table : 'man_caracteristicas_climatizacion_b'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.deleteinsclimB" , function(r, b){
				b();
            	$scope.formData.manclimB = r.insclimB;
				$scope.formData.equiposVal = r.equiposVal;
            }, data);
        	
        }
		$scope.deletecorclimB = function(item , index){
        	var data = {
				id : $scope.formData.corclimB[index]['id'],
				id_cotizacion : $scope.formData.id_order,
				table : 'cor_caracteristicas_climatizacion_b'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.deleteinsclimB" , function(r, b){
				b();
            	$scope.formData.corclimB = r.insclimB;
				$scope.formData.equiposVal = r.equiposVal;
            }, data);
        	
        }
		$scope.deleterevclimB = function(item , index){
        	var data = {
				id : $scope.formData.revclimB[index]['id'],
				id_cotizacion : $scope.formData.id_order,
				table : 'rev_caracteristicas_climatizacion_b'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.deleteinsclimB" , function(r, b){
				b();
            	$scope.formData.revclimB = r.insclimB;
				$scope.formData.equiposVal = r.equiposVal;
            }, data);
        	
        }
		$scope.changeEquipo = function(){
        	var codigo = 0;
			for(x=0;x<$scope.formData.equiposVal.length; x++){
				if($scope.formData.equiposVal[x].dos == $scope.equiposValR){
					codigo = $scope.formData.equiposVal[x].codigo;
					x=$scope.formData.equiposVal.length +1;
				}
			}
			var id_piezas = [];
			
			if(codigo==0){
				$scope.validItems = 0;
				$scope.validParte = 0;
				$('#repuestos').val("");
				$('#parte').val("");
			}
			else{
				var data = {
				codigo : codigo,
				}
				client.post("./controllers/index.php?accion=Ordenes.changeEquipo" , function(r, b){
				b();
				
            	console.log(r);
				$scope.formData.parte = r;
            }, data);
				$scope.validParte=1;
			}
			
        	
        }
		
		$scope.changeParte = function(){
        	var codigo = 0;
			for(x=0;x<$scope.formData.equiposVal.length; x++){
				if($scope.formData.equiposVal[x].dos == $scope.equiposValR){
					codigo = $scope.formData.equiposVal[x].codigo;
					x=$scope.formData.equiposVal.length +1;
				}
			}
			var id_parte = 0;
			for(x=0;x<$scope.formData.parte.length; x++){
				if($scope.formData.parte[x].nombre == $scope.equiposValParte){
					id_parte = $scope.formData.parte[x].id;
					x=$scope.formData.parte.length +1;
				}
			}
			
			if(id_parte==0){
				$scope.validItems = 0;
				$('#repuestos').val("");
			}
			else{
				var data = {
				codigo : codigo,
				id_parte : id_parte,
				}
				//console.log(data);
				client.post("./controllers/index.php?accion=Ordenes.getPiezas" , function(r, b){
				b();
				
            	//console.log(r);
				$scope.formData.repuestos = r;
            }, data);
				$scope.validItems=1;
			}
			
        	
        }
		$scope.deleteHerramienta = function(item , index){
        	var data = {
				id_herramienta : $scope.formData.herramientas[index]['id'],
				id_cotizacion : $scope.formData.id_order,
				codigoOrden : $scope.codigoOrden,
        	}
			 client.post("./controllers/index.php?accion=Ordenes.deleteHerramienta" , function(r, b){
				b();
            	$scope.formData.herramientas = r.herramientas;
            }, data);
        	
        }
		
		$scope.editDatos = function(){
        	var data = {
				id_cotizacion : $scope.formData.id_order,
				tiempo_estimado : $('#clockface_2').clockface('getTime'),
				observaciones :$("#summernote_1").code(),
				codigo : $scope.codigoOrden,
        	}
			console.log(data);
			 client.post("./controllers/index.php?accion=Ordenes.editDatos" , function(r, b){
				b();
				//console.log(r);
				location.href="http://cegaservices2.procesos-iq.com/ordenesTrabajo";
            	//$scope.formData.herramientas = r.herramientas;
            }, data);
        	
        }

       

        $scope.limpiar = function(){
            $scope.formData.fecha = "";
            $scope.formData.cliente = "";
            $scope.formData.tipo_cliente = 0;
            $scope.formData.direccion = "";
            $scope.formData.tipo_trabajo = 0;
			$scope.formData.observaciones = "";
			$("#summernote_1").code("");
			$scope.formData.tiempo = "";
            $scope.formData.insclimA = [];
            $scope.formData.manclimA = [];
            $scope.formData.corclimA = [];
            $scope.formData.revclimA = [];
            $scope.formData.insclimB = [];
            $scope.formData.manclimB = [];
            $scope.formData.corclimB = [];
            $scope.formData.revclimB = [];
            $scope.formData.insRefri = [];
            $scope.formData.manRefri = [];
            $scope.formData.revRefri = [];
            $scope.formData.herramientas2 = [];
            $scope.formData.materiales2 = [];
            $scope.formData.manRefri = [];
            $scope.formData.corRefri = [];
			$scope.formData.insVentilacion = [];
			$scope.formData.manVentilacion = [];
			$scope.formData.corVentilacion = [];
			$scope.formData.revVentilacion = [];
            $scope.formData.materiales = [];
            $scope.formData.herramientas = [];
            $scope.formData.roles = [];
            $scope.formData.equiposclimA = [];
            $scope.formData.equiposRefri = [];
            $scope.formData.id_orden = 0;

        }


        $(".save").on("click" , function(){
            if($scope.formData.fecha != "" && $scope.formData.cliente != "" && $scope.formData.tipo_cliente != "" && $scope.formData.direccion != "" && $scope.formData.tipo_cliente != "" && $scope.formData.areas.length > 0){
                console.log($scope.formData);
            }else{
                alert("Favor de completar el formulario", "Ordenes de trabajo", "error");
            }
        })

    }]);