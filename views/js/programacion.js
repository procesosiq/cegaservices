/*==================================
=            CALENDARIO            =
==================================*/
var calendar = {
    fullcalendarOrder : $('#calendar_object'),
    config : function(Loadevents){
        if (!jQuery().fullCalendar) {
            throw new Error("No existe el plugin de calendario")
            return;
        }
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();

        if (App.isRTL()) {
            if (this.fullcalendarOrder.parents(".portlet").width() <= 720) {
                this.fullcalendarOrder.addClass("mobile");
                h = {
                    right: 'title, prev, next',
                    center: '',
                    left: 'agendaDay, agendaWeek, month, today'
                };
            } else {
                this.fullcalendarOrder.removeClass("mobile");
                h = {
                    right: 'title',
                    center: '',
                    left: 'agendaDay, agendaWeek, month, today, prev,next'
                };
            }
        } else {
            if (this.fullcalendarOrder.parents(".portlet").width() <= 720) {
                this.fullcalendarOrder.addClass("mobile");
                h = {
                    left: 'title, prev, next',
                    center: '',
                    right: 'today,month,agendaWeek,agendaDay'
                };
            } else {
                this.fullcalendarOrder.removeClass("mobile");
                h = {
                    left: 'title',
                    center: '',
                    right: 'prev,next,today,month,agendaWeek,agendaDay'
                };
            }
        }

        var events = Loadevents || [{
                title: 'Click for Google',
                start: new Date(y, m, 28),
                end: new Date(y, m, 29),
                backgroundColor: App.getBrandColor('yellow'),
                description : "Prueba de Evento",
                // url: 'http://google.com/',
            }];

        return { //re-initialize the calendar
            header: h,
            defaultView: 'agendaWeek', // change default view with available options from http://arshaw.com/fullcalendar/docs/views/Available_Views/ 
            slotMinutes: 15,
            lang: 'es',
            editable: true,
            eventDurationEditable:false,
            droppable: true, // this allows things to be dropped onto the calendar !!!
            events: events,
		        drop : function(date, allDay) { // this function is called when something is dropped
                if($("#gruposcalendario").val()==0){
                    console.log("todos");
                    alert("Para poder agendar necesita seleccionar un grupo", "Programación", "error");}
                else{
                    // retrieve the dropped element's stored Event Object
                    var originalEventObject = $(this).data('eventObject');
                    // we need to copy it, so that multiple events don't have a reference to the same object
                    var copiedEventObject = $.extend({}, originalEventObject);

                    // assign it the date that was reported
                    copiedEventObject.start = date;
                    copiedEventObject.className = $(this).attr("data-class");
                    var id = copiedEventObject.title.split(" - ");
					
                    id = id[0];
                    var data = {
						            id: id,
						            grupo: $("#gruposcalendario").val(),
						            fecha: date.format("YYYY-MM-DD HH:mm")
                    }
                    ahttp.post("./controllers/index.php?accion=Programacion.editCalendar" , function(r, b){
                        b();
                        eventCalendar();
                        printCal();
                        //alert(' El evento ' + event.title + " ha sido movido " + event.start.format("YYYY-MM-DD HH:mm") , 'Ordenes' , 'info');
                        //$scope.formData.materiales = r.materiales;
				            }, data);
				
                    // render the event on the calendar
                    // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                    $('#calendar_object').fullCalendar('renderEvent', copiedEventObject, true);

                    // is the "remove after drop" checkbox checked?
                    if ($('#drop-remove').is(':checked')) {
                        // if so, remove the element from the "Draggable Events" list
                        $(this).remove();
                    }
                }
            },
            eventClick: function(calEvent, jsEvent, view) {
                var equipos = 'Equipos : <br>';
                for(x=0; x<calEvent.equipos.length;x++){
                    console.log(calEvent.equipos[x]);
                    equipos = equipos+calEvent.equipos[x]+'<br>';
                }
                var msj = 'Evento : ' + calEvent.title + ' <br> Fecha : ' + calEvent.start.format("YYYY-MM-DD HH:mm")
                + ' <br> Cliente : ' + calEvent.cliente + ' <br> Sucursal : ' + calEvent.sucursal + ' <br> Tipo trabajo : ' + calEvent.tipo_trabajo + '<br>'+equipos
                + ' <br> <button class="btn btn-sm bg-red-thunderbird bg-font-red-thunderbird" onclick="removeEvent('+"'"+calEvent.title+"'"+')">Remover evento</button>';
                alert(msj, 'Programación' , 'success');
                // change the border color just for fun
                $(this).css('border-color', 'red');
            },
            eventDrop: function(event, delta, revertFunc) {
                if (confirm("Esta seguro de cambiar el evento?")) {
                  var id = event.title.split(" - ");
                  id = id[0];
                  var data = {
                      id: id,
                      grupo: 0,
                      fecha: event.start.format("YYYY-MM-DD HH:mm")
                  }
                  ahttp.post("./controllers/index.php?accion=Programacion.editCalendar" , function(r, b){ 
                      b();
  				            eventCalendar();
                      printCal();				
                      alert(' El evento ' + event.title + " ha sido movido " + event.start.format("YYYY-MM-DD HH:mm") , 'Programación' , 'info');
                  }, data);
                }
                else {
                    revertFunc();
                }
				      }
            }
        },
        init : function(){
            // console.log(this.config());
            var config = this.config();
            this.fullcalendarOrder.fullCalendar('destroy'); // destroy the calendar
            this.fullcalendarOrder.fullCalendar(config);
        },
        reload : function(events){
			//console.log("entre");
            this.fullcalendarOrder.fullCalendar('destroy'); // destroy the calendar
            if(events)
                this.fullcalendarOrder.fullCalendar(this.config(events));
            else
                this.fullcalendarOrder.fullCalendar(this.config());
        }
    }
    calendar.init();
	var removeEvent = function(title){
	if (confirm("Esta seguro de remover el evento "+title+"?")) {
                       
					var id = title.split(" - ");
					
						id = id[0];
					
					var data = {
						id: id,
					}
        	ahttp.post("./controllers/index.php?accion=Programacion.removeCalendar" , function(r, b){
				b();
				//console.log(r);
				eventCalendar();
				printCal();
				
				 alert(' El evento ' + title + " ha sido removido" , 'Programación' , 'info');
            	//$scope.formData.materiales = r.materiales;
				}, data);
                   

                    
					}
}
    /*=====  End of CALENDARIO  ======*/
 



// Register an API method that will empty the pipelined data, forcing an Ajax
// fetch on the next draw (i.e. `table.clearPipeline().draw()`)

var printCal = function (){
    var data = {
        grupo: $("#gruposcalendario").val(),
    }
    ahttp.post("./controllers/index.php?accion=Programacion.calendario" , function(r, b){
				b();
				printCalendar(r,b);
    }, data);
}
		
var initDrag = function(el) {
    var eventObject = {
        title: $.trim(el.text()) // use the element's text as the event title
    };
    el.data('eventObject', eventObject);
    el.draggable({
        zIndex: 999,
        revert: true, // will cause the event to go back to its
        revertDuration: 0 //  original position after the drag
    });
}

addEvent = function(title) {
    title = title.length === 0 ? "Untitled Event" : title;
    var id = title.split("-")[0].trim()
    var html = $('<div class="external-event label label-default"> <button id="event-'+id+'" class="bg-red-thunderbird btn input-sm">X</button>  ' + title + '</div>');
    jQuery('#event_box').append(html);
    $("#event-"+id).click(function(){
        ahttp.post("./controllers/index.php?accion=Programacion.deleteEvent" , function(r, b){
            b();
            if(r){
              eventCalendar()
            }
        }, {id_event : id});
    })
    initDrag(html);
};

				
var printCalendar = function(r , b){
    b();
    console.log(r);
    if(r){
        var events = [] , calendario = {} , fecha ;
        for(var d in r.calendario){
            fecha = moment(r.calendario[d]['fecha_agendada']).format("YYYY-MM-DD HH:mm");
            fecha2 = moment(r.calendario[d]['fecha_fin']).format("YYYY-MM-DD HH:mm");

            events.push({
                    title :  r.calendario[d]['id'],
                    tipo_trabajo :  r.calendario[d]['tipo_trabajo'],
                    cliente :  r.calendario[d]['cliente'],
                    start : fecha,
                    end : fecha2,
                    backgroundColor : r.calendario[d]['color'],
                    description : "Orden tipo "+ r.calendario[d]['id']+ " para el Cliente "+r.calendario[d]['fecha_agendada'] ,
                    observaciones : r.calendario[d]['observaciones'],
                    auxiliares : r.calendario[d]['auxiliares'],
                    vehiculo : r.calendario[d]['vehiculo'],
                });

     // fecha = moment(r.data[d][2]).format("id");
	 /*if(r.calendario[d]['grupo_trabajo']==1){
     events.push({
      title :  r.calendario[d]['id'],
      tipo_trabajo :  r.calendario[d]['tipo_trabajo'],
      cliente :  r.calendario[d]['cliente'],
      sucursal :  r.calendario[d]['sucursal'],
      equipos :  r.calendario[d]['equipos'],
      start : new Date(fecha),
      end : new Date(fecha2),
      backgroundColor :	'#BF55EC',
      description : "Orden tipo "+ r.calendario[d]['id']+ " para el Cliente "+r.calendario[d]['fecha_agendada'] ,
     });}
	 else if(r.calendario[d]['grupo_trabajo']==2){
     events.push({
      title :  r.calendario[d]['id'],
      tipo_trabajo :  r.calendario[d]['tipo_trabajo'],
      cliente :  r.calendario[d]['cliente'],
	  sucursal :  r.calendario[d]['sucursal'],
	  equipos :  r.calendario[d]['equipos'],
      start : new Date(fecha),
      end : new Date(fecha2),
      backgroundColor : App.getBrandColor('green'),
      description : "Orden tipo "+ r.calendario[d]['id']+ " para el Cliente "+r.calendario[d]['fecha_agendada'] ,
     });}
	 else if(r.calendario[d]['grupo_trabajo']==3){
     events.push({
      title :  r.calendario[d]['id'],
      tipo_trabajo :  r.calendario[d]['tipo_trabajo'],
      cliente :  r.calendario[d]['cliente'],
      start : new Date(fecha),
	  sucursal :  r.calendario[d]['sucursal'],
	  equipos :  r.calendario[d]['equipos'],
      end : new Date(fecha2),
      backgroundColor : App.getBrandColor('yellow'),
      description : "Orden tipo "+ r.calendario[d]['id']+ " para el Cliente "+r.calendario[d]['fecha_agendada'] ,
     });}
	 else if(r.calendario[d]['grupo_trabajo']==4){
     events.push({
      title :  r.calendario[d]['id'],
      tipo_trabajo :  r.calendario[d]['tipo_trabajo'],
	  equipos :  r.calendario[d]['equipos'],
      cliente :  r.calendario[d]['cliente'],
	  sucursal :  r.calendario[d]['sucursal'],
      start : new Date(fecha),
      end : new Date(fecha2),
      backgroundColor : '#b5824a',
      description : "Orden tipo "+ r.calendario[d]['id']+ " para el Cliente "+r.calendario[d]['fecha_agendada'] ,
     });}*/
	 
     //console.log(r.calendario[d]['fecha_agendada']);
    }
    calendar.reload(events);
   }
  }
  
var eventCalendar = function () {
    $('#event_box').empty();
    var data = {}
    ahttp.post("./controllers/index.php?accion=Programacion.calendarioEvent" , function(r, b){
        b();
				for(var d in r.calendario){
				    addEvent(''+r.calendario[d]['id']);
				}
    }, data);
}

jQuery(document).ready(function() {
	eventCalendar();
	printCal();
    
});

app.controller('programacion', ['$scope','$http','$interval','$controller' , 'client', function($scope,$http,$interval,$controller ,client){
    $scope.options = [];
		$scope.clientes = 0;
		$scope.sucursales = "";
		$scope.tipoTrabajo = 0;
		$scope.equipos = 0;
    $scope.options.clientes = [];
    $scope.options.tipoTrabajo = [];
    $scope.options.equipos = [];
    $scope.options.sucursales = [{id:"", nombre:'Elegir un cliente..'}];

    $scope.get = function(id, codigo){
        client.post("./controllers/index.php?accion=Programacion.getClientes" , function(r, b){
    				b();
            $scope.options.clientes = r.clientes;
            $scope.options.tipoTrabajo = r.tipoTrabajo;
    				$scope.tipoTrabajo = $scope.options.tipoTrabajo[0].id;
    				$scope.clientes = "";
    				$scope.sucursales = "";
            console.log($scope.options.clientes);
        }, {});

        client.post("./controllers/index.php?accion=Programacion.getSupervisores" , function(r, b){
            b();
            $scope.options.supervisores = r.supervisores;
        }, {});
    }
		
		$scope.addEvent = function(){
        if($scope.clientes==''){
				    alert("Seleccione un cliente por favor", "Programación", "error");
				    return false;
        }
        var sucursal = "NO";
        var contador = 0;
        var equipos = [];
        for(x=0; x<$scope.options.clientes.length; x++){
            if($scope.options.clientes[x].id == $scope.clientes){
                sucursal = $scope.options.clientes[x].sucursales;
            }
        }
        if(sucursal == "SI"){
            if($scope.sucursales==''){
                alert("Seleccione una sucursal por favor", "Programación", "error");
                return false;
            }
        }
        for(x=0; x < $scope.options.equipos.length; x++){
            if($('#equipo-'+$scope.options.equipos[x].id).is(':checked')){
                contador = contador+1;
                equipos.push($scope.options.equipos[x].id);
				    }
        }
        if(contador==0){
            alert("Debe elegir al menos un equipo", "Programación", "error");
            return false;
        }
        var data = {
            id_cliente : $scope.clientes,
            id_sucursal : $scope.sucursales,
				    id_tipoTrabajo : $scope.tipoTrabajo,
				    equiposs : equipos,
				}
				
        client.post("./controllers/index.php?accion=Programacion.addEvent" , function(r, b){
            b();
            $scope.options.equipos = [];
            $scope.options.sucursales = [{id:"", nombre:'Elegir un cliente..'}];
            $scope.clientes = "";
            $scope.sucursales = "";
            eventCalendar();
        }, data);
    }

		$scope.getSucursalOrEquipo = function(){
  			var sucursal = "NO";
  			$scope.options.equipos = [];
  			var id_cliente = 0;
  			for(x=0; x<$scope.options.clientes.length; x++){
            if($scope.options.clientes[x].id == $scope.clientes){
                sucursal = $scope.options.clientes[x].sucursales;
                id_cliente = $scope.options.clientes[x].id;
            }
        }
        $scope.sucursales = "";
        $scope.equipos = 0;
  			if(sucursal == "NO"){
            $('#sucursal').prop( "disabled", true );
            $scope.options.sucursales = [{id:"", nombre:'Sin sucursales..'}];
            $scope.sucursales = "";
            $scope.getEquipo();
  			}else{
            $('#sucursal').prop( "disabled", false );
            var data = {
                id_cliente : id_cliente,
            }
            client.post("./controllers/index.php?accion=Programacion.getSucursal" , function(r, b){
                b();
              	$scope.options.sucursales = r.sucursales;
              	$scope.sucursales = $scope.options.sucursales[0].id;
            }, data);
  			}
		}
		
		$scope.getEquipo = function(){
        var sucursal = "NO";
        var id_cliente = 0;
        var id_sucursal = 0;
        var id_tipoTrabajo = 0;
        for(x=0; x<$scope.options.clientes.length; x++){
            if($scope.options.clientes[x].id == $scope.clientes){
                sucursal = $scope.options.clientes[x].sucursales;
                id_cliente = $scope.options.clientes[x].id;
            }
        }
        if(sucursal == "NO"){
            id_sucursal=0;
        }else{
            for(x=0; x<$scope.options.sucursales.length; x++){
                if($scope.options.sucursales[x].id == $scope.sucursales){
                    id_sucursal = $scope.options.sucursales[x].id;
                }
            }
        }
				for(x=0; x<$scope.options.tipoTrabajo.length; x++){
            if($scope.options.tipoTrabajo[x].id == $scope.tipoTrabajo){
                id_tipoTrabajo = $scope.options.tipoTrabajo[x].id;
            }
        }
				var data = {
				    id_cliente : id_cliente,
				    id_sucursal : id_sucursal,
				    id_tipoTrabajo : id_tipoTrabajo,
        }
        client.post("./controllers/index.php?accion=Programacion.getEquipos" , function(r, b){
				    b();

            $("#equiposInput").html("")
            $scope.options.equipos = r.equipos;
            $.each($scope.options.equipos, function(index, value){
                $("#equiposInput").append("<label><input type='checkbox' id='equipo-"+value.id+"'/>"+value.nombre+"</label>")
            })
        }, data);
		}

    $scope.get();
}]);
   