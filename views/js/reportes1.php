        <link href="../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />

        <script src="js/echarts.min.js"></script>
        <script src="js/macarons.js"></script>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper" ng-app="app" ng-controller="reportes">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE BASE CONTENT -->

                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
                                <div class="visual">
                                    <i class="fa fa-comments"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span class="contadores" data-counter="counterup" data-value="0" id="cuadro_nuevas">0</span>
                                    </div>
                                    <div class="desc"> Nuevas Órdenes </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 red" href="#">
                                <div class="visual">
                                    <i class="fa fa-bar-chart-o"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span class="contadores" data-counter="counterup" data-value="0" id="cuadro_facturar">0</span>
                                    </div>
                                    <div class="desc"> Por facturar </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 green" href="#">
                                <div class="visual">
                                    <i class="fa fa-shopping-cart"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span class="contadores" data-counter="counterup" data-value="0" id="cuadro_cumplimiento">0</span>%
                                    </div>
                                    <div class="desc"> Cumplimiento </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 purple" href="#">
                                <div class="visual">
                                    <i class="fa fa-globe"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span class="contadores" data-counter="counterup" data-value="0" id="cuadro_efectividad">0</span>% </div>
                                    <div class="desc"> Efectividad </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <!-- Begin: life time stats -->
                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <span class="caption-subject font-dark sbold uppercase">REPORTES RESUMEN</span>
                                    </div>
                                    <div class="actions">
                                        <div class="dt-buttons">
                                            <a class="dt-button buttons-print btn dark btn-outline" tabindex="0" aria-controls="datatable_ajax"><span>Imprimir</span></a>
                                            <a class="dt-button buttons-pdf buttons-html5 btn green btn-outline" tabindex="0" aria-controls="datatable_ajax"><span>PDF</span></a>
                                            <a class="dt-button buttons-excel buttons-html5 btn yellow btn-outline" tabindex="0" aria-controls="datatable_ajax"><span>Excel</span></a>
                                            <a class="dt-button buttons-csv buttons-html5 btn purple btn-outline" tabindex="0" aria-controls="datatable_ajax"><span>CSV</span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-body">
                                    <div id="chart_general" style="height:400px;"></div>
                                </div>
                            </div>
                            <!-- End: life time stats -->
                        </div>
                    </div>
                    <div class="row">
                        <!-- START: BOX TIPO DE CLIENTE -->
                        <div class="col-md-6">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings"></i>TIPO DE CLIENTE</div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse" data-original-title="Expandir/Contraer" title=""> </a>
                                        <!--
                                        <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
                                        <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
                                        <a href="javascript:;" class="remove" data-original-title="" title=""> </a>
                                        -->
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="portlet-body">

                                        <table class="table table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th width="35%"> T. Cliente </th>
                                                    <th width="20%"> % Cumpl </th>
                                                    <th width="20%"> % Efect </th>
                                                    <th width="20%"> Total </th>
                                                    <th width="5%">&nbsp;</th>
                                                </tr>
                                            </thead>
                                        </table>


                                        <div class="panel-group accordion" id="accordion3">
                                            <div class="panel panel-default" ng-repeat="tipo in tipoCliente">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1" aria-expanded="false"> 
                                                            <table class="table" class="nobmp-reporte-accordion" style="border: 0; padding: 0; margin: 0;">
                                                                <thead class="nobmp-reporte-accordion">
                                                                    <tr class="nobmp-reporte-accordion">
                                                                        <th width="40%" class="nobmp-reporte-accordion"> {{tipo.nombre}} </th>
                                                                        <th width="20%" class="nobmp-reporte-accordion">  lala</th>
                                                                        <th width="20%" class="nobmp-reporte-accordion">  lelo</th>
                                                                        <th width="20%" class="nobmp-reporte-accordion"> {{tipo.total}}</th>
                                                                    </tr>
                                                            </thead>
                                                            </table>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapse_3_1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                                    <div class="panel-body">
                                                        <p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut. </p>
                                                        <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                                                            </p>
                                                    </div>
                                                </div>
                                            </div>

                                            
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END: BOX TIPO DE CLIENTE -->
                        <!-- START: BOX TIPO DE EQUIPO -->
                        <div class="col-md-6">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings"></i>TIPO DE EQUIPO</div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse" data-original-title="Expandir/Contraer" title=""> </a>
                                        <!--
                                        <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
                                        <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
                                        <a href="javascript:;" class="remove" data-original-title="" title=""> </a>
                                        -->
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="portlet-body">

                                        <table class="table table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th width="45%"> T. Equipo </th>
                                                    <th width="25%"> Equipos </th>
                                                    <th width="25%"> Órdenes </th>
                                                    <th width="5%">&nbsp;</th>
                                                </tr>
                                            </thead>
                                        </table>


                                        <div class="panel-group accordion" id="accordion4">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion4" href="#collapse_4_1" aria-expanded="false"> 
                                                            <table class="table" class="nobmp-reporte-accordion" style="border: 0; padding: 0; margin: 0;">
                                                                <thead class="nobmp-reporte-accordion">
                                                                    <tr class="nobmp-reporte-accordion">
                                                                        <th width="50%" class="nobmp-reporte-accordion"> Climatización A </th>
                                                                        <th width="30%" class="nobmp-reporte-accordion"> 30 </th>
                                                                        <th width="20%" class="nobmp-reporte-accordion"> 150 </th>
                                                                    </tr>
                                                            </thead>
                                                            </table>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapse_4_1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                                    <div class="panel-body">
                                                        <p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut. </p>
                                                        <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                                                            </p>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END: BOX TIPO DE EQUIPO -->
                    </div>
                    <div class="row">
                        <!-- START: GRAFICA TIPO DE CLIENTE -->
                        <div class="col-md-6">
                            <div class="portlet light">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings"></i>
                                        <span class="caption-subject"> TIPO DE CLIENTE </span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="chart_tipo_cliente" style="height:350px;"></div>
                                </div>
                            </div>
                        </div>
                        <!-- END: GRAFICA TIPO DE CLIENTE -->

                        <!-- START: GRAFICA TIPO DE CLIENTE -->
                        <div class="col-md-6">
                            <div class="portlet light">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings"></i>
                                        <span class="caption-subject"> TIPO DE EQUIPO </span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs ">
                                            <li class="active"><a href="#tab_15_1" data-toggle="tab">Total</a></li>
                                            <li><a href="#tab_15_2" data-toggle="tab">Clima A</a></li>
                                            <li><a href="#tab_15_3" data-toggle="tab">Clima B</a></li>
                                            <li><a href="#tab_15_4" data-toggle="tab">Ventilación</a></li>
                                            <li><a href="#tab_15_5" data-toggle="tab">Refrigeración</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_15_1">
                                                <div id="chart_tipo_equipo_1" style="height:350px;"></div>
                                                1
                                            </div>
                                            <div class="tab-pane" id="tab_15_2">
                                                <div id="chart_tipo_equipo_2" style="height:350px;"></div>
                                                2
                                            </div>
                                            <div class="tab-pane" id="tab_15_3">
                                                <div id="chart_tipo_equipo_3" style="height:350px;"></div>
                                                3
                                            </div>
                                            <div class="tab-pane" id="tab_15_4">
                                                <div id="chart_tipo_equipo_4" style="height:350px;"></div>
                                                4
                                            </div>
                                            <div class="tab-pane" id="tab_15_5">
                                                <div id="chart_tipo_equipo_5" style="height:350px;"></div>
                                                5
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END: GRAFICA TIPO DE CLIENTE -->
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
