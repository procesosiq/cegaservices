
function reporte_general(){
    angular.injector(['ng', 'App.Services']).invoke(function (Reporte1) {
        
        Reporte1.getGeneral(100).then(function(r){
            var data = r.data;

            /** CUADROS SUPERIORES **/
            if(data.cuadros){
                $("#cuadro_nuevas").attr("data-value", data.cuadros.nuevas_ordenes);
                $("#cuadro_nuevas").html(data.cuadros.nuevas_ordenes);
                $("#cuadro_facturar").attr("data-value", data.cuadros.por_facturar);
                $("#cuadro_facturar").html(data.cuadros.por_facturar);
                $("#cuadro_cumplimiento").attr("data-value", precise_round(data.cuadros.cumplimiento,2));
                $("#cuadro_cumplimiento").html(precise_round(data.cuadros.cumplimiento,2));
                $("#cuadro_efectividad").attr("data-value", precise_round(data.cuadros.efectividad,2));
                $("#cuadro_efectividad").html(precise_round(data.cuadros.efectividad,2));
            }
            $('.contadores').counterUp();

            /**
             * GRAFICA
             */

            var gdata_cumplimiento = [];
            var gdata_efectividad  = [];
            var gdata_x = [];
            for(var f in data.grafica_general){
                var obj = data.grafica_general[f];
                gdata_cumplimiento.push(obj.cumplimiento);
                gdata_efectividad.push(obj.efectividad);
                gdata_x.push(f);
            }

            var cg_series = {
                legend: ['Cumplimiento', 'Efectividad'],
                data: [
                        {
                            name:'Cumplimiento',
                            type:'line',
                            data:gdata_cumplimiento,
                        },
                        {
                            name:'Efectividad',
                            type:'line',
                            data:gdata_efectividad,
                        }
                    ],
                data_x:gdata_x
            }

            var chartGeneral = {
                config: {
                    div: 'chart_general',
                    theme: 'shine'
                },

                chart: {
                    title : {
                        show: false,
                        text: 'Text',
                        subtext: 'Subtext',
                        borderColor: '#FF00FF',
                        backgroundColor: 'rgba(0,0,0,0)'
                    },
                    tooltip : {
                        trigger: 'axis'
                    },
                    legend: {
                        data: cg_series.legend
                    },
                    toolbox: {
                        show : true,
                        feature : {
                            mark : {show: false},
                            dataView : {show: false, readOnly: true},
                            magicType : {show: true, type: ['line', 'bar']},
                            restore : {show: false},
                            saveAsImage : {show: true}
                        }
                    },
                    dataZoom : {
                        show : true,
                        realtime: true/*,
                        start : 50,
                        end : 100*/
                    },
                    calculable : true,
                    xAxis : [
                        {
                            type : 'category',
                            boundaryGap : false,
                            splitLine: {
                                show:false
                            },
                            data : cg_series.data_x
                        }
                    ],
                    yAxis : [
                        {
                            type : 'value',
                            splitLine: {
                                show:true,
                                lineStyle:{
                                    type: 'dashed'
                                }
                            },
                            axisLabel : {
                                formatter: '{value}'
                            }
                        }
                    ],
                    series: cg_series.data
                }
            };

            var chart1 = echarts.init(document.getElementById(chartGeneral.config.div), chartGeneral.config.theme);
            chart1.setOption(chartGeneral.chart);

        });
    });
}