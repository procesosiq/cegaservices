app.controller('sucursales', ['$scope','$http','client','$controller', function($scope,$http,client, $controller){
    $scope.peticiones = {
        getSucursal : function(callback, params){
            client.post("./controllers/index.php?accion=Sucursales.getSucursal" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        getContactos : function(callback, params){
            client.post("./controllers/index.php?accion=Sucursales.getContactos" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        getFacturacion : function(callback, params){
            client.post("./controllers/index.php?accion=Sucursales.getFacturacion" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        insertAll : function(callback, params){
            client.post("./controllers/index.php?accion=Sucursales.insertAll" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        insertContacto : function(callback, params){
            client.post("./controllers/index.php?accion=Sucursales.insertContacto" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        insertFacturacion : function(callback, params){
            client.post("./controllers/index.php?accion=Sucursales.insertFacturacion" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        edit : function(callback, params){
            client.post("./controllers/index.php?accion=Sucursales.editSucursal" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        editContacto : function(callback, params){
            client.post("./controllers/index.php?accion=Sucursales.editContacto" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        editFacturacion : function(callback, params){
            client.post("./controllers/index.php?accion=Sucursales.editFacturacion" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        eliminarContacto : function(callback, params){
            client.post("./controllers/index.php?accion=Sucursales.eliminarContacto" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        eliminarFacturacion: function(callback, params){
            client.post("./controllers/index.php?accion=Sucursales.eliminarFacturacion" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        principalContacto : function(callback, params){
            client.post("./controllers/index.php?accion=Sucursales.principalContacto" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        principalFacturacion: function(callback, params){
            client.post("./controllers/index.php?accion=Sucursales.principalFacturacion" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        getDataPrincipal: function(callback, params){
            client.post("./controllers/index.php?accion=Sucursales.getDataPrincipal" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
    }

    $scope.searchTable = {}
    
    $scope.sucursal = {}
    $scope.contacto = {}
    $scope.contactos = {}
    $scope.facturacion = {}
    $scope.data = {
        id_sucursal : parseInt('<?= isset($_GET["id"]) ? $_GET["id"] : 0 ?>'),
        id_cliente : parseInt('<?= isset($_GET["idc"]) ? $_GET["idc"] : 0 ?>'),
        principal : false
    }

    $scope.init = function(){
        if($scope.data.id_sucursal > 0){
            $scope.sucursal.id = $scope.data.id_sucursal
            $scope.peticiones.getSucursal(r => {
                if(r){
                    $scope.sucursal = r.sucursal
                }
            },$scope.sucursal)
            $scope.peticiones.getContactos(r => {
                if(r){
                    $scope.table_contactos = r.table_contactos
                }
            },$scope.sucursal)
            $scope.peticiones.getFacturacion(r => {
                if(r){
                    $scope.table_facturacion = r.table_facturacion
                }
            },$scope.sucursal)
        }
    }

    $scope.newContacto = function(){
        $scope.peticiones.getContactos(r => {
            if(r){
                $scope.table_contactos = r.table_contactos
            }
        },$scope.sucursal)
    }

    $scope.newFacturacion = function(){
        $scope.peticiones.getFacturacion(r => {
            if(r){
                $scope.table_facturacion = r.table_facturacion
            }
        },$scope.sucursal)
    }

    $scope.guardar = function(){
        if($scope.data.id_sucursal > 0){
            $scope.sucursal.id = $scope.data.id_sucursal
            $scope.peticiones.edit(r => {
                if(r.status == 200){
                    alert("INFORMACIÓN GUARDADA CORRECTAMENTE","CONFIRMAR","success")
                    window.location.href = "/sucursalesList?idc="+$scope.data.id_cliente
                }
            },$scope.sucursal)
        } else {
            $scope.peticiones.insertAll(r => {
                if(r.status == 200){
                    alert("INFORMACIÓN GUARDADA CORRECTAMENTE","CONFIRMAR","success")
                    window.location.href = "/sucursalesList?idc="+$scope.data.id_cliente
                }
            },{
                data : $scope.data,
                sucursal : $scope.sucursal,
                contacto : $scope.contacto,
                facturacion : $scope.facturacion
            })
        }
    }

    $scope.guardarContacto = function(){
        $scope.contacto.id_sucursal = $scope.data.id_sucursal
        $scope.peticiones.insertContacto(r => {
            if(r.status == 200){
                alert("INFORMACIÓN GUARDADA CORRECTAMENTE","CONFIRMAR","success")
                $scope.contacto = {}
                $scope.newContacto()
            }
        },$scope.contacto)
    }

    $scope.guardarFacturacion = function(){
        $scope.facturacion.id_sucursal = $scope.data.id_sucursal
        $scope.peticiones.insertFacturacion(r => {
            if(r.status == 200){
                alert("INFORMACIÓN GUARDADA CORRECTAMENTE","CONFIRMAR","success")
                $scope.facturacion = {}
                $scope.data.principal = false
                $scope.newFacturacion()
            }
        },$scope.facturacion)
    }

    $scope.editContacto = function(contacto){
        $scope.peticiones.editContacto(r => {
            if(r.status == 200){
                alert("INFORMACIÓN GUARDADA CORRECTAMENTE","CONFIRMAR","success")
                
                $scope.newContacto()
            }
        },contacto)
    }

    $scope.editFacturacion = function(facturacion){
        $scope.peticiones.editFacturacion(r => {
            if(r.status == 200){
                alert("INFORMACIÓN GUARDADA CORRECTAMENTE","CONFIRMAR","success")
                $scope.newFacturacion()
            }
        },facturacion)
    }

    $scope.eliminarContacto = function(id){
        $scope.contacto.id = id
        $scope.peticiones.eliminarContacto(r => {
            if(r.status == 200){
                alert("REGISTRO ELIMINADO CORRECTAMENTE","CONFIRMAR","success")
                $scope.newContacto()
            }
        },$scope.contacto) 
    }

    $scope.eliminarFacturacion = function(id){
        $scope.facturacion.id = id
        $scope.peticiones.eliminarFacturacion(r => {
            if(r.status == 200){
                alert("REGISTRO ELIMINADO CORRECTAMENTE","CONFIRMAR","success")
                $scope.newFacturacion()
            }
        },$scope.facturacion) 
    }

    $scope.principalContacto = function(row){
        $scope.peticiones.principalContacto(r => {
            if(r.status == 200){
                alert("CONTACTO PRINCIPAL ASIGNADO CORRECTAMENTE","CONFIRMAR","success")
                setTimeout(() => {
                    $scope.newContacto()
                }, 300)
            }
        },row) 
    }

    $scope.principalFacturacion = function(row){
        $scope.peticiones.principalFacturacion(r => {
            if(r.status == 200){
                alert("CONTACTO PRINCIPAL ASIGNADO CORRECTAMENTE","CONFIRMAR","success")
                setTimeout(() => {
                    $scope.newFacturacion()
                }, 300)
            }
        },row) 
    }

    $scope.getDataPrincipal = function(bool){
        $scope.data.principal = bool
        if(bool){
            $scope.peticiones.getDataPrincipal(r => {
                if(r){
                    $scope.facturacion = r.facturacion
                }
            },$scope.data)
        } else {
            $scope.data.principal = false 
            $scope.facturacion = {}
        }
    }


    $scope.cancelar = function(){
        window.location.href = "/sucursalesList?idc="+$scope.data.id_cliente
    }

    limpiar = function(){
        $scope.sucursal = {}
        $scope.contacto = {}
        $scope.facturacion = {}
        $scope.data.principal = false
    }

    $scope.init()

}]);