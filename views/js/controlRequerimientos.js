app.controller('controlRequerimientos', ['$scope','$http','client','$controller', function($scope,$http,client, $controller){
    $scope.peticiones = {
        insert : function(callback, params){
            client.post("./controllers/index.php?accion=ControlRequerimientos.insert" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        update : function(callback, params){
            client.post("./controllers/index.php?accion=ControlRequerimientos.update" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        select : function(callback, params){
            client.post("./controllers/index.php?accion=ControlRequerimientos.select" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        clientes : function(callback, params){
            client.post("./controllers/index.php?accion=ControlRequerimientos.clientes" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        tiposTrabajos : function(callback, params){
            client.post("./controllers/index.php?accion=ControlRequerimientos.tiposTrabajos" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        personal : function(callback, params){
            client.post("./controllers/index.php?accion=ControlRequerimientos.personal" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        auxiliar : function(callback, params){
            client.post("./controllers/index.php?accion=ControlRequerimientos.auxiliar" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        selectObservaciones : function(callback, params){
            client.post("./controllers/index.php?accion=ControlRequerimientos.selectObservaciones" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        insertObservaciones : function(callback, params){
            client.post("./controllers/index.php?accion=ControlRequerimientos.insertObservaciones" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        show : function(callback, params){
            client.post("./controllers/index.php?accion=ControlRequerimientos.show" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        zonas : function(callback, params){
            client.post("./controllers/index.php?accion=ControlRequerimientos.zonas" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        estados : function(callback, params){
            client.post("./controllers/index.php?accion=ControlRequerimientos.estados" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        sucursales : function(callback, params){
            client.post("./controllers/index.php?accion=ControlRequerimientos.sucursales" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
    }

    var time = new Date
    $scope.orderByField = "fecha_recepcion"
    $scope.reverseSort = true
    $scope.searchTable = {
        estado : "POR EJECUTAR"
    }
    $scope.data = {
        hora : "",
        hora_observaciones : time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds(),
        fecha_observacion : time.getFullYear() + "-" + (time.getMonth() + 1) + "-" + time.getDate(),
        cliente : "",
        personal : "",
        auxiliar : "",
        observacion : "",
        estado : "",
        id_requerimiento : "",
        fecha_programada : "",
        
    }
    $scope.semanas = {
        0 : { id : 1 , label : 1 },
        1 : { id : 2 , label : 2 },
        2 : { id : 3 , label : 3 },
        3 : { id : 4 , label : 4 },
        4 : { id : 5 , label : 5 },
        5 : { id : 6 , label : 6 },
        6 : { id : 7 , label : 7 },
        7 : { id : 8 , label : 8 },
        8 : { id : 9 , label : 9 },
        9 : { id : 10 , label : 10 },
        10 : { id : 11 , label : 11 },
        11 : { id : 12 , label : 12 },
        12 : { id : 13 , label : 13 },
        13 : { id : 14 , label : 14 },
        14 : { id : 15 , label : 15 },
        15 : { id : 16 , label : 16 },
        16 : { id : 17 , label : 17 },
        17 : { id : 18 , label : 18 },
        18 : { id : 19 , label : 19 },
        19 : { id : 20 , label : 20 },
        20 : { id : 21 , label : 21 },
        21 : { id : 22 , label : 22 },
        22 : { id : 23 , label : 23 },
        23 : { id : 24 , label : 24 },
        24 : { id : 25 , label : 25 },
        25 : { id : 26 , label : 26 },
        26 : { id : 27 , label : 27 },
        27 : { id : 28 , label : 28 },
        28 : { id : 29 , label : 29 },
        29 : { id : 30 , label : 30 },
        30 : { id : 31 , label : 31 },
        31 : { id : 32 , label : 32 },
        32 : { id : 33 , label : 33 },
        33 : { id : 34 , label : 34 },
        34 : { id : 35 , label : 35 },
        35 : { id : 36 , label : 36 },
        36 : { id : 37 , label : 37 },
        37 : { id : 38 , label : 38 },
        38 : { id : 39 , label : 39 },
        39 : { id : 40 , label : 40 },
        40 : { id : 41 , label : 41 },
        41 : { id : 42 , label : 42 },
        42 : { id : 43 , label : 43 },
        43 : { id : 44 , label : 44 },
        44 : { id : 45 , label : 45 },
        45 : { id : 46 , label : 46 },
        46 : { id : 47 , label : 47 },
        47 : { id : 48 , label : 48 },
        48 : { id : 49 , label : 49 },
        49 : { id : 50 , label : 50 },
        50 : { id : 51 , label : 51 },
        51 : { id : 52 , label : 52 },        
    }
    $scope.meses = {
        0 : { id : 1 , label : 'ENERO' },
        1 : { id : 2 , label : 'FEBRERO' },
        2 : { id : 3 , label : 'MARZO' },
        3 : { id : 4 , label : 'ABRIL' },
        4 : { id : 5 , label : 'MAYO' },
        5 : { id : 6 , label : 'JUNIO' },
        6 : { id : 7 , label : 'JULIO' },
        7 : { id : 8 , label : 'AGOSTO' },
        8 : { id : 9 , label : 'SEPTIEMBRE' },
        9 : { id : 10 , label : 'OCTUBRE' },
        10 : { id : 11 , label : 'NOVIMIEMBRE' },
        11 : { id : 12 , label : 'DICIEMBRE' },
     
    }

    limpiar = function(){
        $scope.data = {
            semana : moment().week(),
            hora : "",
            cliente : "",
            personal_ejecutado : "",
            hora_observaciones : time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds(),
            fecha_observacion : time.getFullYear() + "-" + (time.getMonth() + 1) + "-" + time.getDate(),
            cliente : "",
            personal : "",
            auxiliar : "",
            observacion : "",
            estado : "",
            id_requerimiento : "",
            fecha_programada : "",
        }
    }

    $scope.modal = function(id, id_cliente, nombre_cliente){
        $scope.data.id = id
        $scope.data.id_cliente = id_cliente
        $scope.nombre_cliente = nombre_cliente
        $("#modal_observaciones").modal("show")
        $scope.peticiones.selectObservaciones(r => {
            if(r){
                $scope.observaciones = r.observaciones
            }
        },$scope.data)
    }

    $scope.getRegistroObservacion = function(){
        $scope.peticiones.selectObservaciones(r => {
            if(r){
                $scope.observaciones = r.observaciones
            }
        },$scope.data)
    }

    $scope.insertModal = function(){
        if($scope.data.observacion != ""){
            $scope.peticiones.insertObservaciones(r => {
                if(r.status == 200){
                    alert("OBSERVACION GUARDADA CORRECTAMENTE","CONFIRMAR","success")
                    $scope.getRegistroObservacion()
                    $scope.data.hora_observaciones = time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds(),
                    $scope.data.fecha_observacion = time.getFullYear() + "-" + (time.getMonth() + 1) + "-" + time.getDate()
                    $scope.data.observacion = ""
                    $scope.data.id_requerimiento = ""
                }
            },$scope.data)
        } else {
            alert("POR FAVOR COMPLETE LOS CAMPOS REQUERIDOS", "ALERTA")
        }
    }

    var initPickers = function () {
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            autoclose: true
        });
    }
    
    $scope.init = function(){
        initPickers()
        $scope.peticiones.select(r => {
            if(r){
                $scope.datatable = r.data
            }
        },$scope.data)
        $scope.peticiones.clientes(r => {
            if(r){
                $scope.clientes = r.clientes
            }
        },$scope.data)
        $scope.peticiones.tiposTrabajos(r => {
            if(r){
                $scope.tipos_trabajos = r.tipos_trabajos
            }
        },$scope.data)
        $scope.peticiones.personal(r => {
            if(r){
                $scope.personal = r.personal
            }
        },$scope.data)
        $scope.peticiones.auxiliar(r => {
            if(r){
                $scope.auxiliar = r.auxiliar
            }
        },$scope.data)
        $scope.peticiones.zonas(r => {
            if(r){
                $scope.zonas = r.zonas
            }
        },$scope.data)
        $scope.peticiones.estados(r => {
            if(r){
                $scope.estados = r.estados
            }
        },$scope.data)
    }

    
    $scope.getSucursales = function(){
        $scope.peticiones.sucursales(r => {
            if(r){
                $scope.sucursales = r.sucursales
            }
        },$scope.data)
    }

    $scope.getRegistro = function(){
        $scope.peticiones.select(r => {
            if(r){
                $scope.datatable = r.data
            }
        },$scope.data)
    }

    $scope.recarga = function(){
        $scope.peticiones.select(r => {
            if(r){
                $scope.datatable = r.data
            }
        },$scope.data)
    }
    
    $scope.insert = function(){
        if($scope.data.cliente != "" || $scope.data.auxiliar != ""){
            $scope.peticiones.insert(r => {
                if(r.status == 200){
                    alert("INFORMACIÓN GUARDADA CORRECTAMENTE","CONFIRMAR","success")
                    $scope.getRegistro()
                    limpiar()
                    setTimeout(() => {
                        $("a[data-parent=#accordion3]").click()
                    }, 100)
                }
            },$scope.data)
        } else {
            alert("POR FAVOR COMPLETE LOS CAMPOS REQUERIDOS", "ALERTA")
        }
        
    }

    $scope.update = function(id, estado){
        $scope.data.id = id
        $scope.data.estado = estado
        if($scope.data.estado != ""){
            $scope.peticiones.update(r => {
                if(r.status == 200){
                    alert("ESTADO ACTUALIZADO CORRECTAMENTE","CONFIRMAR","success")
                    $scope.recarga()
                }
            },$scope.data)
        } else {
            alert("POR FAVOR SELECCIONAR UN ESTADO VALIDO", "ALERTA")
        }
        
    }

    $scope.show = function(id){
        $scope.data.id_requerimiento = id
        $scope.peticiones.show(r => {
            if(r){
                $scope.data = r.data
                $('#hora').datetimepicker({
                    date : $scope.data.hora
                })
                setTimeout(() => {
                    $("a[data-parent=#accordion3]").click()
                }, 100)
            }
        },$scope.data)
    }

    $scope.llenaSemMes = function(){
        if(moment($scope.data.fecha_programada).week() !=  $scope.data.semana){
            $scope.data.semana = moment($scope.data.fecha_programada).week()
        }
        if(moment($scope.data.fecha_programada).month() !=  $scope.data.mes){
            $scope.data.mes = moment($scope.data.fecha_programada).month()
        }
    }

    $('#hora').on('dp.change', function(e){ 
        $scope.data.hora = e.date.format('LT')
    })

    $scope.init();

    $("#hora").datetimepicker({
        format : 'LT',
    });

    $("#auxiliar").select2({ 
        tags : true,
        placeholder : ""
    })

}]);