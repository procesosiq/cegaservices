app.controller('configZonas', ['$scope','$http','client','$controller', function($scope,$http,client, $controller){
    $scope.peticiones = {
        insert : function(callback, params){
            client.post("./controllers/index.php?accion=ConfigZonas.insert" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        eliminar : function(callback, params){
            client.post("./controllers/index.php?accion=ConfigZonas.eliminar" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        show : function(callback, params){
            client.post("./controllers/index.php?accion=ConfigZonas.show" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        zonas : function(callback, params){
            client.post("./controllers/index.php?accion=ConfigZonas.zonas" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
    }

    $scope.searchTable = {}
    $scope.data = {
        zona : "",
        status : "Activo"
    }

    limpiar = function(){
        $scope.data = {
            zona : "",
            status : "Activo"
        }
    }

    $scope.init = function(){
        $scope.peticiones.zonas(r => {
            if(r){
                $scope.zonas = r.zonas
            }
        },$scope.data)
    }

    $scope.insert = function(){
        if($scope.data.zona != ""){
            $scope.peticiones.insert(r => {
                if(r.status == 200){
                    alert("INFORMACIÓN GUARDADA CORRECTAMENTE","CONFIRMAR","success")
                    limpiar()
                    $scope.init()
                }
            },$scope.data)
        } else {
            alert("POR FAVOR COMPLETE LOS CAMPOS REQUERIDOS", "ALERTA")
        }
        
    }

    $scope.eliminar = function(id, estado){
        $scope.data.id_zona = id
        $scope.peticiones.eliminar(r => {
            if(r.status == 200){
                alert("REGISTRO ELIMINADO CORRECTAMENTE","CONFIRMAR","success")
                $scope.init()
            }
        },$scope.data) 
    }

    $scope.show = function(id){
        $scope.data.id_zona = id
        $scope.peticiones.show(r => {
            if(r){
                $scope.data = r.data
            }
        },$scope.data)
    }

    $scope.init();

}]);