app.controller('trabajosEjec', ['$scope','$http','client','$controller', function($scope,$http,client, $controller){
    $scope.peticiones = {
        listado : function(callback, params){
            client.post("./controllers/index.php?accion=TrabajosEjecutados.listado" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
    }

    $scope.searchTable = {}
    $scope.data = {}

    limpiar = function(){
        $scope.data = {
            estado : "",
            status : "Activo"
        }
    }

    $scope.init = function(){
        $scope.peticiones.listado(r => {
            if(r){
                $scope.data = r.list
            }
        },$scope.data)
    }

    $scope.color = function(value){
        if(value == 'CORRECTIVO'){
            return 'bg-red-thunderbird bg-font-red-thunderbird'
        } else if(value == 'MANTENIMIENTO'){
            return 'bg-blue bg-font-blue'
        } else if(value == 'INSTALACIÓN'){
            return 'bg-green-jungle bg-font-green-jungle'
        } else if(value == 'REVISIÓN CORRECTIVO'){
            return 'bg-purple bg-font-purple'
        } else if(value == 'REVISIÓN MANTENIMIENTO'){
            return 'bg-yellow-gold bg-font-yellow-gold'
        } else if(value == 'REVISIÓN INSTALACIÓN'){
            return 'bg-grey-mint bg-font-grey-mint'
        }
    }

    $scope.init();

}]);