app.controller('configTiposTrabajos', ['$scope','$http','client','$controller', function($scope,$http,client, $controller){
    $scope.peticiones = {
        insert : function(callback, params){
            client.post("./controllers/index.php?accion=ConfigTiposTrabajos.insert" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        eliminar : function(callback, params){
            client.post("./controllers/index.php?accion=ConfigTiposTrabajos.eliminar" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        show : function(callback, params){
            client.post("./controllers/index.php?accion=ConfigTiposTrabajos.show" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        trabajo : function(callback, params){
            client.post("./controllers/index.php?accion=ConfigTiposTrabajos.trabajos" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        colores : function(callback, params){
            client.post("./controllers/index.php?accion=ConfigTiposTrabajos.colores" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
    }

    $scope.searchTable = {}
    
    $scope.data = {
        trabajo : "",
        status : "Activo"
    }

    limpiar = function(){
        $scope.data = {
            trabajo : "",
            status : "Activo"
        }
    }

    $scope.init = function(){
        $scope.peticiones.trabajo(r => {
            if(r){
                $scope.trabajos = r.trabajos
            }
        },$scope.data)
        $scope.peticiones.colores(r => {
            if(r){
                $scope.colores = r.colores
            }
        },$scope.data)
    }

    $scope.insert = function(){
        if($scope.data.trabajo != ""){
            $scope.peticiones.insert(r => {
                if(r.status == 200){
                    alert("INFORMACIÓN GUARDADA CORRECTAMENTE","CONFIRMAR","success")
                    limpiar()
                    $scope.init()
                }
            },$scope.data)
        } else {
            alert("POR FAVOR COMPLETE LOS CAMPOS REQUERIDOS", "ALERTA")
        }
        
    }

    $scope.eliminar = function(id, trabajo){
        $scope.data.id_trabajo = id
        $scope.peticiones.eliminar(r => {
            if(r.status == 200){
                alert("REGISTRO ELIMINADO CORRECTAMENTE","CONFIRMAR","success")
                $scope.init()
            }
        },$scope.data) 
    }

    $scope.show = function(id){
        $scope.data.id_trabajo = id
        $scope.peticiones.show(r => {
            if(r){
                $scope.data = r.data
            }
        },$scope.data)
    }

    $scope.init();

}]);