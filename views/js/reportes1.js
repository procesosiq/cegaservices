var Services = angular.module('App.Services', [])
    .config(function ($httpProvider){
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
        $httpProvider.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
        $httpProvider.defaults.transformRequest = function(obj) {
            var str = [];
            for(var p in obj){
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            }
            return str.join("&");
        };
    });

Services.factory('Reporte1', function ($http, $q) {
    return new function () {
        /**
         * GENERAL
         */
        this.getGeneral = function (valor) {
            var deferred;
            deferred = $q.defer();
            $http({
                method: 'POST',
                skipAuthorization: true,
                url: "./controllers/index.php?accion=reporte.general",
                data: {
                    hola: valor,
                    indicador : $("#indicador :selected").val(),
                    start : $("#date_start").val(),
                    end : $("#date_end").val()
                }
            })
            .then(function(res)
            {
                deferred.resolve(res);
            })
            .then(function(error)
            {
                deferred.reject(error);
            });
            return deferred.promise;
        };

    }
});

var chartTipoCliente = {
        config: {
            div: 'chart_tipo_cliente',
            theme: 'macarons'
        },

        chart: {
            tooltip: {
                trigger: 'item',
                formatter: "{a} <br/>{b}: {c} ({d}%)"
            },
            legend: {
                orient: 'vertical',
                x: 'left',
                data:['COMERCIAL', 'INDUSTRIAL', 'RESIDENCIAL']
            },
            toolbox: {
                show : true,
                feature : {
                    mark : {show: true},
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            legend: {
                orient: 'vertical',
                x: 'left',
                data:['COMERCIAL', 'INDUSTRIAL', 'RESIDENCIAL']
            },
            calculable : true,
            series: [
                {
                    name:'Prueba',
                    type:'pie',
                    avoidLabelOverlap: false,
                    radius: ['50%', '70%'],
                    center: ['50%', '60%'],
                    label: {
                        normal: {
                            show: false,
                            position: 'center'
                        },
                        emphasis: {
                            show: true,
                            textStyle: {
                                fontSize: '20',
                                fontWeight: 'bold'
                            }
                        }
                    },
                    labelLine: {
                        normal: {
                            show: false
                        }
                    },
                    data:[
                        {value:335, name:'COMERCIAL'},
                        {value:310, name:'INDUSTRIAL'},
                        {value:234, name:'RESIDENCIAL'}
                    ]
                }
            ]
        }
    };

    var chart2 = echarts.init(document.getElementById(chartTipoCliente.config.div), chartTipoCliente.config.theme);
    chart2.setOption(chartTipoCliente.chart);

    /** TIPO DE EQUIPO **/
    function dibujarGrafica(id){
        var chartTipoEquipo = {
            config: {
                div: 'chart_tipo_equipo_' + id,
                theme: 'macarons'
            },

            chart: {
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b}: {c} ({d}%)"
                },
                legend: {
                    orient: 'vertical',
                    x: 'left',
                    data:['COMERCIAL','INDUSTRIAL','RESIDENCIAL']
                },
                toolbox: {
                    show : true,
                    feature : {
                        mark : {show: true},
                        restore : {show: true},
                        saveAsImage : {show: true}
                    }
                },
                legend: {
                    orient: 'vertical',
                    x: 'left',
                    data:['COMERCIAL','INDUSTRIAL','RESIDENCIAL']
                },
                calculable : true,
                series: [
                    {
                        name:'Prueba',
                        type:'pie',
                        avoidLabelOverlap: false,
                        radius: ['50%', '70%'],
                        center: ['50%', '60%'],
                        label: {
                            normal: {
                                show: false,
                                position: 'center'
                            },
                            emphasis: {
                                show: true,
                                textStyle: {
                                    fontSize: '20',
                                    fontWeight: 'bold'
                                }
                            }
                        },
                        labelLine: {
                            normal: {
                                show: false
                            }
                        },
                        data:[
                            {value:335, name:'COMERCIAL'},
                            {value:310, name:'INDUSTRIAL'},
                            {value:234, name:'RESIDENCIAL'}
                        ]
                    }
                ]
            }
        };

        var chart = echarts.init(document.getElementById(chartTipoEquipo.config.div), chartTipoEquipo.config.theme);
        chart.setOption(chartTipoEquipo.chart);
    }
	
function data(supervisor){
    supervisor = (supervisor - 1);
    var cumplimiento = {};
    var cumplimiento = {};
    var efectividad = [90 , 80 , 90 , 70 , 90 , 80];
    var efectividad = [90 , 80 , 90 , 70 , 90 , 80];

    var data = [];

    data.push({
        cumplimiento : cumplimiento[supervisor],
        efectividad : cumplimiento[efectividad],
    })
}

function reporte_general(supervisor){
    angular.injector(['ng', 'App.Services']).invoke(function (Reporte1) {
        
        Reporte1.getGeneral(100).then(function(r){
            var data = r.data;

            /** CUADROS SUPERIORES **/
            console.log(data.cuadros)
            if(data.cuadros){
                //cant nuevas
                $("#cuadro_nuevas").attr("data-value", data.cuadros.nuevas_ordenes);
                $("#cuadro_nuevas").html(data.cuadros.nuevas_ordenes);
                //cant facturar
                $("#cuadro_facturar").attr("data-value", data.cuadros.por_facturar);
                $("#cuadro_facturar").html(data.cuadros.por_facturar);
                //% cumplimiento
                $("#cuadro_cumplimiento").attr("data-value", precise_round(data.cuadros.cumplimiento,2));
                $("#cuadro_cumplimiento").html(precise_round(data.cuadros.cumplimiento,2));
                //% efectividad
                $("#cuadro_efectividad").attr("data-value", precise_round(data.cuadros.efectividad,2));
                $("#cuadro_efectividad").html(precise_round(data.cuadros.efectividad,2));
                //% rebotes
                $("#cuadro_rebotes").attr("data-value", data.cuadros.rebotes);
                $("#cuadro_rebotes").html(data.cuadros.rebotes);
                //cant rebotes
                $("#cuadro_cantidad_rebotes").attr("data-value", data.cuadros.cantidad_rebotes);
                $("#cuadro_cantidad_rebotes").html(data.cuadros.cantidad_rebotes);
                //% rendimiento
                $("#cuadro_rendimiento").attr("data-value", precise_round(data.cuadros.rendimiento, 2));
                $("#cuadro_rendimiento").html(precise_round(data.cuadros.rendimiento, 2));
            }
            $('.contadores').counterUp();

            /**
             * GRAFICA
             */

            var gdata_cumplimiento = [];
            var gdata_efectividad  = [];
            var gdata_x = [];
            data.grafica_general = {};

            var data_graphic = [];
            for(var l in data.legends){
                var mData = [];
                for(var p in data.grafica[data.legends[l]]){
                    mData.push(data.grafica[data.legends[l]][p] || 0);
                }

                data_graphic.push({
                    name : data.legends[l],
                    type : 'line',
                    data : mData
                });
            }
            
            /*for(var f in data.grafica){
                var obj = data.grafica[f];
                gdata_cumplimiento.push(obj.cumplimiento || 0);
                gdata_efectividad.push(obj.efectividad || 0);
                gdata_x.push(f);
            }*/

            /*var cg_series = {
                legend: ['Cumplimiento', 'Efectividad'],
                data: [
                        {
                            name:'Cumplimiento',
                            type:'line',
                            data:gdata_cumplimiento,
                        },
                        {
                            name:'Efectividad',
                            type:'line',
                            data:gdata_efectividad,
                        },
                    ],
                data_x: gdata_x
            }*/

            var cg_series = {
                legend: data.legends,
                data : data_graphic,
                data_x: data.fechas
            }

            var chartGeneral = {
                config: {
                    div: 'chart_general',
                    theme: 'shine'
                },

                chart: {
                    title : {
                        show: false,
                        text: 'Text',
                        subtext: 'Subtext',
                        borderColor: '#FF00FF',
                        backgroundColor: 'rgba(0,0,0,0)'
                    },
                    tooltip : {
                        trigger: 'axis'
                    },
                    legend: {
                        data: cg_series.legend,
                        y : 'top'
                    },
                    toolbox: {
                        show : true,
                        y : 'bottom',
                        feature : {
                            mark : {show: false},
                            dataView : {show: false, readOnly: true},
                            magicType : {show: true, type: ['line', 'bar']},
                            restore : {show: false},
                            saveAsImage : {show: true}
                        }
                    },
                    dataZoom : {
                        show : true,
                        realtime: true
                    },
                    calculable : true,
                    xAxis : [
                        {
                            type : 'category',
                            boundaryGap : false,
                            splitLine: {
                                show:false
                            },
                            data : cg_series.data_x
                        }
                    ],
                    yAxis : [
                        {
                            type : 'value',
                            splitLine: {
                                show:true,
                                lineStyle:{
                                    type: 'dashed'
                                }
                            },
                            axisLabel : {
                                formatter: '{value}'
                            }
                        }
                    ],
                    series: cg_series.data
                }
            };

            var chart1 = echarts.init(document.getElementById(chartGeneral.config.div), chartGeneral.config.theme);
            chart1.setOption(chartGeneral.chart);
            window.onresize = function(){
                chart1.resize();
            }

        });
    });
}

    /* click en los tabs */
    document.addEventListener('DOMContentLoaded',function(){

        // reporte general
        reporte_general($("#supervisor :selected").val());

        /*
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
          var target = $(e.target).attr("href").toString();
          var lastChar = target.substr(target.length - 1);
          dibujarGrafica(lastChar);
        });

        dibujarGrafica("1");
        */
    });

    function precise_round(num,decimals){
        return Math.round(num*Math.pow(10,decimals))/Math.pow(10,decimals);
    }

var changeSupervisor = function(){
    var supervisor = $("#supervisor").val();
    reporte_general(supervisor);
}
app.controller('reportes', ['$scope','$http','$interval','client','$controller', function($scope, $http, $interval, client, $controller){

    $scope.init = function(){

        var date = new Date();
        const offset = date.getTimezoneOffset();
        date = new Date(date.getTime() - (offset*60*1000));
        date = date.toISOString().split('T')[0];

        $scope.date1 = date;
        $scope.date2 = date;

		client.post("./controllers/index.php?accion=reporte" , function(r, b){
			b();
			$scope.tipoCliente =  r.tipoCliente;
			$scope.tipoEquipo =  r.tipoEquipo;
            $scope.supervisores = r.supervisores;
            indicadores = $scope.indicadores = r.indicadores;
            console.log(r)

		} , {});
	}

	$scope.getTipoEquipo = function(datos){
		var data = {
				id : datos.id
			}
		client.post("./controllers/index.php?accion=reporte.getEquiposByClient" , function(r, b){
			b();
			$scope.tipoEquipo =  r.tipoEquipo;
		} , data);
	}

    $scope.refresh = function(){
        reporte_general($("#supervisor :selected").val());
    }

}]);

/*
app.controller('.datepicker', function ($scope) {
    $scope.datepicker = {
        dt: false,
        dtSecond: false
    }
    $scope.today = function() {
        $scope.formData.dt = new Date();

        $scope.formData.dtSecond = new Date();
    };
    $scope.today();

    $scope.showWeeks = true;
    $scope.toggleWeeks = function () {
        $scope.showWeeks = ! $scope.showWeeks;
    };

    $scope.clear = function () {
        $scope.dt = null;
    };

    // Disable weekend selection
    $scope.disabled = function(date, mode) {
        return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
    };

    $scope.toggleMin = function() {
        $scope.minDate = ( $scope.minDate ) ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open = function($event, which) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.datepickers[which]= true;
    };

    $scope.dateOptions = {
        'year-format': "'yyyy'",
        'starting-day': 1
    };

    $scope.formats = ['yyyy/MM/dd'];
    $scope.format = $scope.formats[0];
    var data{

        date: data.format;
    }
    client.post("./controllers/index.php?accion=reporte.getCumplimiento" , function(r, b){
        b();
        $scope.datepicker =  r.datepicker;
        //location.href="http://cegaservices2.procesos-iq.com/cotizacion?id="+r;
    } , data);

});
*/

