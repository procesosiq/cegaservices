var TableDatatablesAjax = function () {

    var initPickers = function () {
        //init date pickers
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            autoclose: true
        });
    }

    var handleRecords = function () {

        var grid = new Datatable();

        grid.init({
            src: $("#datatable_ajax"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 

                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js). 
                // So when dropdowns used the scrollable div should be removed. 
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                
                "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 150, -1],
                    [10, 20, 50, 100, 150, "All"] // change per page values here
                ],
                "pageLength": 10, // default record count per page
                "ajax": {
                    "url": "./controllers/index.php?accion=sucursales&idc=<?php echo $_GET['idc']; ?>", // ajax source
                },
                "order": [
                    [1, "asc"]
                ],// set first column as a default sort by asc
                "language": {
                    "lengthMenu": "Vista de _MENU_ registros por pagina",
                    "zeroRecords": "No se encontro ningun registro",
                    "info": "Pagina _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros disponibles",
                    "infoFiltered": "(filtrado de un total de _MAX_ registros)"
                },
                "buttons": [
                            { extend: 'print', className: 'btn dark btn-outline' },
                            // { extend: 'copy', className: 'btn red btn-outline' },
                            { extend: 'pdf', className: 'btn green btn-outline' },
                            { extend: 'excel', className: 'btn yellow btn-outline ' },
                            { extend: 'csv', className: 'btn purple btn-outline ' },
                            // { extend: 'colvis', className: 'btn dark btn-outline', text: 'Columns'}
                            {
                                text: 'Nueva Sucursal',
                                className: 'btn blue btn-outline',
                                action: function ( e, dt, node, config ) {
                                    //dt.ajax.reload();
                                    document.location.href = '/sucursales?idc=<?php echo $_GET['idc']; ?>';
                                }
                            }
                        ],
                "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l>><'table-scrollable't><'row'<'col-md-5 col-sm-12'i>>" // horizobtal scrollable datatable
            }
        });

        // handle group actionsubmit button click
        grid.getTableWrapper().on('click', '.table-group-action-submit', function (e) {
            e.preventDefault();
            var action = $(".table-group-action-input", grid.getTableWrapper());
            if (action.val() != "" && grid.getSelectedRowsCount() > 0) {
                grid.setAjaxParam("customActionType", "group_action");
                grid.setAjaxParam("customActionName", action.val());
                grid.setAjaxParam("id", grid.getSelectedRows());
                grid.getDataTable().ajax.reload();
                grid.clearAjaxParams();
            } else if (action.val() == "") {
                App.alert({
                    type: 'danger',
                    icon: 'warning',
                    message: 'Please select an action',
                    container: grid.getTableWrapper(),
                    place: 'prepend'
                });
            } else if (grid.getSelectedRowsCount() === 0) {
                App.alert({
                    type: 'danger',
                    icon: 'warning',
                    message: 'No record selected',
                    container: grid.getTableWrapper(),
                    place: 'prepend'
                });
            }
        });

        grid.setAjaxParam("customActionType", "group_action");
        grid.getDataTable().ajax.reload();
        grid.clearAjaxParams();

        $('#datatable_ajax_tools > li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            grid.getDataTable().button(action).trigger();
        });

        $('#datatable_ajax tbody').on( 'click', 'button', function () {
            // console.log(grid.table.getSelectedRows());
            // console.log($(this).parents('tr'));
            // console.log(grid.table.getDataTable());
            var btnid = $(this).prop("id");            
            var data = grid.getDataTable().row( $(this).parents('tr') ).data();

             if(data.length > 0){
                if(btnid == "edit"){
                    document.location.href = "/sucursales?id=" + data[1] + "&idc="+$("#idcli").val();
                }
                else if(btnid == "equi"){
                    document.location.href = "/equiposList?ids=" + data[1] + "&idc="+$("#idcli").val();
                }else if(btnid == "status"){
                    if(confirm("¿Esta seguro de cambiar el registro?")){
                         $.ajax({
                            type: "POST",
                            url: "controllers/index.php",
                            data: "accion=sucursales.ChangeStatus&idsuc="+data[1],
                            success: function(msg){
                                alert("Registro cambiado" , "SUCURSALES" , 'success' , function(){
                                    grid.getDataTable().ajax.reload();
                                });
                            }
                        });
                    }
                }
             }
        } );
    }

    return {

        //main function to initiate the module
        init: function () {

            initPickers();
            handleRecords();
        }

    };

}();

jQuery(document).ready(function() {
    TableDatatablesAjax.init();
});