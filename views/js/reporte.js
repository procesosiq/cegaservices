var Services = angular.module('App.Services', [])
    .config(function ($httpProvider){

        delete $httpProvider.defaults.headers.common['X-Requested-With'];
        $httpProvider.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
        $httpProvider.defaults.transformRequest = function(obj) {
            var str = [];
            for(var p in obj){
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            }
            return str.join("&");
        };
    });

Services.factory('Reporte1', function ($http, $q) {

    return new function () {

        /**
         * GENERAL
         */
        this.getGeneral = function () {
            var deferred;
            deferred = $q.defer();
            $http({
                method: 'POST',
                skipAuthorization: true,
                url: "./controllers/index.php?accion=reporte.general",
                data: {
                    hola: "hola"
                }
            })
            .then(function(res)
            {
                deferred.resolve(res);
            })
            .then(function(error)
            {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        this.GetAll = function () {
            return "GetAll";
        };

    };

});

function HOLA(){
    angular.injector(['ng', 'App.Services']).invoke(function (Reporte1) {
        Reporte1.getGeneral();
    });
}