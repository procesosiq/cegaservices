app.controller('equiposController', ['$scope','$http','$interval','$controller', function($scope,$http,$interval,$controller){

    $scope.init = function(start){
        if(start){
            $.ajax({
                type: "POST",
                url: "controllers/index.php",
                data: "accion=equipos.getPartesEquipo&id="+$("#idequ").val(),
                success: function(msg){
                    var obj =eval(msg);
                    for (var i = 0; i < obj.length; i++) {
                        var nuevaParte = {
                            index : i,
                            id_parte : obj[i].id_parte,
                            parte : obj[i].parte,
                            id_pieza : obj[i].id_pieza,
                            pieza : obj[i].pieza,
                            id_marca : obj[i].id_marca,
                            marca : obj[i].marca,
                            id_capacidad : obj[i].id_capacidad,
                            capacidad : obj[i].capacidad,
                            modelo : obj[i].modelo,
                            serie : obj[i].serie,
                            id_refrigerante : obj[i].id_refrigerante,
                            id_motor : obj[i].id_motor
                        }
                        $scope.partes.push(nuevaParte);
                    }
                    $scope.$apply(function () {
                        $scope.index = obj.length;
                    });
                }
            });
        }
    }

    $scope.partes = [];
    $scope.editar = false;
    $scope.index = 0;

    $scope.limpiar_main = function(){
        $("#s_tipoequi").val(0);
        $("#s_descripcion").val(0);
        //$("#s_descripcion").html("");
        $("#txtcapacidad").val("");
        $("#s_areaclima").html("");
        $("#txtcodigo").val("");
        $("#txtnombrearea").val("");
        $scope.limpiar_partes();
        $scope.partes = [];
    }

    $scope.editarParte = function(parte, index){
        console.log(parte);
        $("#s_parte").val(parte.id_parte);
        $("#cmbPiezas").val(parte.id_pieza);
        $("#s_marcas").val(parte.id_marca);
        $("#s_capacidad2").val(parte.id_capacidad);
        $("#txtmodelo").val(parte.modelo);
        $("#txtserie").val(parte.serie);
        $("#refrigerantes").val(parte.id_refrigerante);
        $("#motores").val(parte.id_motor);
        $scope.buscarPiezas(parte);
        $scope.editar = true;
        $scope.indexEdit = index;
    }

    $scope.elminarParte = function(parte, index){
        for (var i = 0; i < $scope.partes.length; i++) {
            if($scope.partes[i].index == index){
                delete $scope.partes[i];
                $scope.partes.splice(i , 1);
            }
        }
    }

    $scope.limpiar_partes = function(){
        $("#s_parte").val(0);
        $("#s_marcas").val(0);
        $("#s_capacidad2").val(0);
        $("#txtmodelo").val("");
        $("#txtserie").val("");
        $("#cmbPiezas").html("");
        $("#motores").val(0);
        $("#refrigerantes").val(0);
        $scope.editar = false;
        $scope.indexEdit = 0;
    }

    $scope.agregar_partes = function(){
        console.log($("#cmbPiezas").val());
        if($("#s_parte").val()>0 && $("#s_marcas").val()>0 && $("#s_capacidad2").val()>0 && $("#txtmodel").val()!="" && $("#txtserie").val()!=""){
            var nuevaParte = {
                index : $scope.index,
                id_parte : $("#s_parte").val(),
                parte : $("#s_parte :selected").text(),
                id_pieza : $("#cmbPiezas").val(),
                pieza : $("#cmbPiezas :selected").text(),
                id_marca : $("#s_marcas").val(),
                marca : $("#s_marcas :selected").text(),
                id_capacidad : $("#s_capacidad2").val(),
                capacidad : $("#s_capacidad2 :selected").text(),
                modelo : $("#txtmodelo").val(),
                serie : $("#txtserie").val(),
                id_refrigerante : $("#refrigerantes").val(),
                refrigerante : (($("#refrigerantes").val()>0)?$("#refrigerantes :selected").text():''),
                id_motor : $("#motores").val(),
                motor : (($("#motores").val()>0)?$("#motores :selected").text():'')
            };
            if($scope.editar){
                for (var i = 0; i < $scope.partes.length; i++) {
                    if($scope.partes[i].index == $scope.indexEdit){
                        delete $scope.partes[i];
                        $scope.partes.splice(i , 1);
                    }
                }
            }
            $scope.partes.push(nuevaParte);
            $scope.index++;
            $scope.limpiar_partes();
            //console.log($scope.partes);
        }else{
            alert("Favor de completar el formulario");
        }
    }

    $("#btnadd").on("click" , function(){
        $scope.registrar();
    });

    $("#btnupd").on("click", function(){
        $scope.modificar();
    });

    $scope.modificar = function(){
        if($("#s_tipoequi").val()==0){
            alert("Favor de seleccionar un tipo de equipo");
            return false;
        }
        else if($("#s_descripcion").val()==0){
            alert("Favor de seleccionar una Descripción de equipo");
            return false;
        }
        else if($("#s_capacidad").val()==0){
            alert("Favor de seleccionar la capacidad");
            return false;
        }
        else if($("#s_areaclima").val()==0){
            alert("Favor de seleccionar el área que climatiza");
            return false;
        }
        else if($("#txtnombrearea").val()==""){
            alert("Favor de asignar un nombre de área");
            return false;
        }
        else if($("#txtcodigo").val()==''){
            alert("Favor de ingresar un código");
            return false;
        }
        else if($scope.partes.length==0){
            alert("Favor de agregar almenos una parte al equipo");
            return false;
        }
        else{
            $.ajax({
                type: "POST",
                url: "controllers/index.php",
                data: "accion=equipos.UpdateEquipo&fecha="+$("#txtfec").html()+"&tipo_equipo="+$("#s_tipoequi").val()+"&id_descripcion="+$("#s_descripcion").val()+
                "&capacidad="+$("#txtcapacidad").val()+"&area_climatiza="+$("#s_areaclima").val()+"&codigo="+$("#txtcodigo").val()+"&id="+$("#idequ").val()+
                "&id_cliente="+$("#idcli").val()+"&id_sucursal="+$("#idsuc").val()+"&nombre_area="+$("#txtnombrearea").val()+"&partes="+JSON.stringify($scope.partes),
                success: function(msg){
                    console.log(msg);
                    if(msg>0){
                        alert("Equipo modificado con exito","Equipo Modificado","success");
                        document.location.href = "/equiposList?idc="+$("#idcli").val()+"&ids="+$("#idsuc").val();
                    }
                    else{
                        alert("Hubo un error al guardar el equipo");
                    }

                    /*$('input[type=text]').each(function() {
                        $(this).val('');
                    });*/
                },
                error: function(msg){
                    alert("error "+msg);
                }
            });
            var data = {
                partes : $scope.partes , 
                s_tipoequi : $("#s_tipoequi").val() ,
                s_listaequi : $("#s_listaequi").val() ,
                s_capacidad : $("#s_capacidad").val() ,
                s_areaclima : $("#s_areaclima").val() ,
                txtcodigo : $("#txtcodigo").val() ,
                idcli : $("#idcli").val() ,
                idsuc : $("#idsuc").val() 
            }
            /*ahttp.post("./controllers/index.php?accion=equipos.AddEquipo",function(r,b){
                b();
                console.log(r);
            },data);*/
        }
        return false;
    }

    $scope.registrar = function(){
        // if($("#txtfec").val()==''){
        //     alert("Favor de ingresar fecha de registro");
        //     return false;
        // }
        if($("#s_tipoequi").val()==0){
            alert("Favor de seleccionar un tipo de equipo");
            return false;
        }
        else if($("#s_descripcion").val()==0){
            alert("Favor de seleccionar una Descripción de equipo");
            return false;
        }
        else if($("#s_capacidad").val()==0){
            alert("Favor de seleccionar la capacidad");
            return false;
        }
        else if($("#s_areaclima").val()==0){
            alert("Favor de seleccionar el área que climatiza");
            return false;
        }
        else if($("#txtnombrearea").val()==""){
            alert("Favor de asignar un nombre de área");
            return false;
        }
        else if($("#txtcodigo").val()==''){
            alert("Favor de ingresar un código");
            return false;
        }
        else if($scope.partes.length==0){
            alert("Favor de agregar almenos una parte al equipo");
            return false;
        }
        else{
            $.ajax({
                type: "POST",
                url: "controllers/index.php",
                data: "accion=equipos.AddEquipo&fecha="+$("#txtfec").html()+"&tipo_equipo="+$("#s_tipoequi").val()+"&id_descripcion="+$("#s_descripcion").val()+
                "&capacidad="+$("#txtcapacidad").val()+"&area_climatiza="+$("#s_areaclima").val()+"&codigo="+$("#txtcodigo").val()+
                "&id_cliente="+$("#idcli").val()+"&id_sucursal="+$("#idsuc").val()+"&nombre_area="+$("#txtnombrearea").val()+"&partes="+JSON.stringify($scope.partes),
                success: function(msg){
                    console.log(msg);
                    if(msg>0){
                        alert("Equipo registrado con el ID "+msg ,"Equipo Registrado","success");
                        $scope.limpiar_main();
                    }
                    else{
                        alert("Hubo un error al guardar el equipo");
                    }

                    /*$('input[type=text]').each(function() {
                        $(this).val('');
                    });*/
                },
                error: function(msg){
                    alert("error "+msg);
                }
            });
            var data = {
                partes : $scope.partes , 
                s_tipoequi : $("#s_tipoequi").val() ,
                s_listaequi : $("#s_listaequi").val() ,
                s_capacidad : $("#s_capacidad").val() ,
                s_areaclima : $("#s_areaclima").val() ,
                txtcodigo : $("#txtcodigo").val() ,
                idcli : $("#idcli").val() ,
                idsuc : $("#idsuc").val() 
            }
            /*ahttp.post("./controllers/index.php?accion=equipos.AddEquipo",function(r,b){
                b();
                console.log(r);
            },data);*/
        }
        return false;
    }

    $("#s_parte").change(function(){
        $scope.buscarPiezas(null);
    });

    $scope.buscarPiezas = function(parte){
        if($("#s_parte").val()>0){
            $.ajax({
                type: "POST",
                url: "controllers/index.php",
                data: "accion=equipos.getPiezas&id_parte_equipo="+$("#s_parte").val(),
                success: function(msg){
                    var obj =eval(msg);
                    $("#cmbPiezas").html("");
                    for(var i=0; i<obj.length;i++){
                        if($scope.editar)
                            $("#cmbPiezas").append("<option value='"+obj[i].id+"' "+(($.inArray(obj[i].id, (parte!=null)?parte.id_pieza:0)>=0)?'selected':'')+">"+obj[i].descripcion+"</option>");
                        else
                            $("#cmbPiezas").append("<option value='"+obj[i].id+"'>"+obj[i].descripcion+"</option>");
                    }
                }
            });
        }else{
            $("#cmbPiezas").html("");
        }
        return false; 
    }

}]);

$(function(){
    
    $('.date-picker').datepicker({
        rtl: App.isRTL(),
        autoclose: true
    });
    
    $("#s_tipoequi").change(function(){ 
        $.ajax({
            type: "POST",
            url: "controllers/index.php",
            data: "accion=equipos.GetListaEqu&idTipE="+$("#s_tipoequi").val(),
            success: function(msg){
                var obj =eval(msg);
                // $("#s_listaequi").html("");
                // $("#s_listaequi").append("<option value='0'>&lt;&lt; Seleccionar &gt;&gt;</option>");
                // for(var i=0; i<obj.length;i++){
                //     $("#s_listaequi").append("<option value='"+obj[i].id+"'>"+obj[i].nombre+"</option>");
                // }                

                $("#s_descripcion").html("");
                $("#s_descripcion").append("<option value='0'>&lt;&lt; Seleccionar &gt;&gt;</option>");
                for(var i=0; i<obj.length;i++){
                    $("#s_descripcion").append("<option value='"+obj[i].id+"'>"+obj[i].nombre+"</option>");
                }
            }
        });
        
        $.ajax({
            type: "POST",
            url: "controllers/index.php",
            data: "accion=equipos.GetAreaClima&idTipE="+$("#s_tipoequi").val()+"&tipo_cliente="+$("#id_tipo_cliente").val(),
            success: function(msg){
                var obj =eval(msg);
                $("#s_areaclima").html("");
                $("#s_areaclima").append("<option value='0'>&lt;&lt; Seleccionar &gt;&gt;</option>");
                for(var i=0; i<obj.length;i++){
                    $("#s_areaclima").append("<option value='"+obj[i].id+"' "+((obj[i].id==$("#s_areaclima").attr("data-id_selected"))?selected:'')+">"+obj[i].nombre+"</option>");
                }
            }
        });
        
        return false;
	});
    
    $("#s_descripcion").change(function(){ 
        if(this.value>0){
            $.ajax({
                type: "POST",
                url: "controllers/index.php",
                data: "accion=equipos.GetPartesEqu&idLisE="+this.value,
                success: function(msg){
                    var obj =eval(msg);
                    $("#s_parte").html("");
                    $("#s_parte").append("<option value='0'>&lt;&lt; Seleccionar &gt;&gt;</option>");
                    for(var i=0; i<obj.length;i++){
                        $("#s_parte").append("<option value='"+obj[i].id+"'>"+obj[i].nombre+"</option>");
                    }
                }
            });   
        }        
        return false;
	});
    
    $("#btnupdaa").click(function(){
        
        if($("#txtnom").val()==''){
            alert("Favor de ingresar un nombre de cliente");
            return false;
        }
        else if($("#txtemail").val()==''){
            alert("Favor de ingresar un email del cliente");
            return false;
        }
        else if($("#txtfec").val()==''){
            alert("Favor de ingresar fecha de registro");
            return false;
        }
        else if($("#txtrazon").val()==''){
            alert("Favor de ingresar la razón social");
            return false;
        }
        else if($("#txtruc").val()==''){
            alert("Favor de ingresar el RUC");
            return false;
        }
        else if($("#txtdircli").val()==''){
            alert("Favor de ingresar una dirección");
            return false;
        }
        else if($("#txttel").val()==''){
            alert("Favor de ingresar un teléfono");
            return false;
        }
        else if($("#txtciudad").val()==''){
            alert("Favor de ingresar una ciudad");
            return false;
        }
        else{ 
            $.ajax({
				type: "POST",
				url: "controllers/index.php",
				data: "accion=clientes.UpdateCliente&txtnom="+$("#txtnom").val()+"&txtemail="+$("#txtemail").val()+"&s_tipocli="+$("#s_tipocli").val()+
                "&txtfec="+$("#txtfec").val()+"&s_sucursales="+$("#s_sucursales").val()+"&txtrazon="+$("#txtrazon").val()+"&id_marca="+$("#s_marcas").val()+
                "&txtruc="+$("#txtruc").val()+"&txtdircli="+$("#txtdircli").val()+"&txttel="+$("#txttel").val()+"&txtciudad="+$("#txtciudad").val()+"&idcli="+$("#idcli").val(),
				success: function(msg){ 
                    if(msg){
                        alert("Cliente modificado con éxito");
                        document.location.href='/clienteList';
                    }
                    else{
                        alert("Error encontrado, favor de intentar más tarde");
                    }
				}
			});
        }
        return false;
	});
    
    $("#limpiar_partes").click(function(){
        $("#s_parte").val(0);
        $("#s_marcas").val(0);
        $("#s_capacidad2").val(0);
        $("#txtmodelo").val("");
        $("#txtserie").val("");
        $("#cmbPiezas").html("");
        $("#motores").val(0);
        $("#refrigerantes").val(0);
    });
});