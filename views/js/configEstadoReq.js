app.controller('configEstadoReq', ['$scope','$http','client','$controller', function($scope,$http,client, $controller){
    $scope.peticiones = {
        insert : function(callback, params){
            client.post("./controllers/index.php?accion=ConfigEstadoReq.insert" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        eliminar : function(callback, params){
            client.post("./controllers/index.php?accion=ConfigEstadoReq.eliminar" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        show : function(callback, params){
            client.post("./controllers/index.php?accion=ConfigEstadoReq.show" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        estados : function(callback, params){
            client.post("./controllers/index.php?accion=ConfigEstadoReq.estados" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        colores : function(callback, params){
            client.post("./controllers/index.php?accion=ConfigEstadoReq.colores" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
    }

    $scope.searchTable = {}
    
    $scope.data = {
        estado : "",
        status : "Activo"
    }

    limpiar = function(){
        $scope.data = {
            estado : "",
            status : "Activo"
        }
    }

    $scope.init = function(){
        $scope.peticiones.estados(r => {
            if(r){
                $scope.estados = r.estados
            }
        },$scope.data)
        $scope.peticiones.colores(r => {
            if(r){
                $scope.colores = r.colores
            }
        },$scope.data)
    }

    $scope.insert = function(){
        if($scope.data.estado != ""){
            $scope.peticiones.insert(r => {
                if(r.status == 200){
                    alert("INFORMACIÓN GUARDADA CORRECTAMENTE","CONFIRMAR","success")
                    limpiar()
                    $scope.init()
                }
            },$scope.data)
        } else {
            alert("POR FAVOR COMPLETE LOS CAMPOS REQUERIDOS", "ALERTA")
        }
        
    }

    $scope.eliminar = function(id, estado){
        $scope.data.id_estado = id
        $scope.peticiones.eliminar(r => {
            if(r.status == 200){
                alert("REGISTRO ELIMINADO CORRECTAMENTE","CONFIRMAR","success")
                $scope.init()
            }
        },$scope.data) 
    }

    $scope.show = function(id){
        $scope.data.id_estado = id
        $scope.peticiones.show(r => {
            if(r){
                $scope.data = r.data
            }
        },$scope.data)
    }

    $scope.init();

}]);