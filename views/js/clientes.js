app.controller('clientes', ['$scope','$http','client','$controller', function($scope,$http,client, $controller){
    $scope.peticiones = {
        getCliente : function(callback, params){
            client.post("./controllers/index.php?accion=Clientes.getCliente" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        getContactos : function(callback, params){
            client.post("./controllers/index.php?accion=Clientes.getContactos" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        getFacturacion : function(callback, params){
            client.post("./controllers/index.php?accion=Clientes.getFacturacion" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        insertAll : function(callback, params){
            client.post("./controllers/index.php?accion=Clientes.insertAll" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        insertContacto : function(callback, params){
            client.post("./controllers/index.php?accion=Clientes.insertContacto" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        insertFacturacion : function(callback, params){
            client.post("./controllers/index.php?accion=Clientes.insertFacturacion" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        edit : function(callback, params){
            client.post("./controllers/index.php?accion=Clientes.editCliente" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        editContacto : function(callback, params){
            client.post("./controllers/index.php?accion=Clientes.editContacto" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        editFacturacion : function(callback, params){
            client.post("./controllers/index.php?accion=Clientes.editFacturacion" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        eliminarContacto : function(callback, params){
            client.post("./controllers/index.php?accion=Clientes.eliminarContacto" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        eliminarFacturacion: function(callback, params){
            client.post("./controllers/index.php?accion=Clientes.eliminarFacturacion" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        principalContacto : function(callback, params){
            client.post("./controllers/index.php?accion=Clientes.principalContacto" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        principalFacturacion: function(callback, params){
            client.post("./controllers/index.php?accion=Clientes.principalFacturacion" , function(r, b){			
                b();
                callback(r)
            } , params)
        },
        
    }

    $scope.searchTable = {}
    
    $scope.cliente = {}
    $scope.contacto = {}
    $scope.contactos = {}
    $scope.facturacion = {}
    $scope.data = {
        id_cliente : parseInt('<?= isset($_GET["id"]) ? $_GET["id"] : 0 ?>'),
        principal : false
    }

    $scope.init = function(){
        if($scope.data.id_cliente > 0){
            $scope.cliente.id = $scope.data.id_cliente
            $scope.peticiones.getCliente(r => {
                if(r){
                    $scope.cliente = r.cliente
                }
            },$scope.cliente)
            $scope.peticiones.getContactos(r => {
                if(r){
                    $scope.table_contactos = r.table_contactos
                }
            },$scope.cliente)
            $scope.peticiones.getFacturacion(r => {
                if(r){
                    $scope.table_facturacion = r.table_facturacion
                }
            },$scope.cliente)
        }
    }

    $scope.newContacto = function(){
        $scope.peticiones.getContactos(r => {
            if(r){
                $scope.table_contactos = r.table_contactos
            }
        },$scope.cliente)
    }

    $scope.newFacturacion = function(){
        $scope.peticiones.getFacturacion(r => {
            if(r){
                $scope.table_facturacion = r.table_facturacion
            }
        },$scope.cliente)
    }

    $scope.guardar = function(){
        if($scope.data.id_cliente > 0){
            $scope.cliente.id = $scope.data.id_cliente
            $scope.peticiones.edit(r => {
                if(r.status == 200){
                    alert("INFORMACIÓN GUARDADA CORRECTAMENTE","CONFIRMAR","success")
                    window.location.href = "/clienteList"
                }
            },$scope.cliente)
        } else {
            $scope.peticiones.insertAll(r => {
                if(r.status == 200){
                    alert("INFORMACIÓN GUARDADA CORRECTAMENTE","CONFIRMAR","success")
                    window.location.href = "/clienteList"
                }
            },{
                data : $scope.data,
                cliente : $scope.cliente,
                contacto : $scope.contacto,
                facturacion : $scope.facturacion
            })
        }
    }

    $scope.guardarContacto = function(){
        $scope.contacto.id_cliente = $scope.data.id_cliente
        $scope.peticiones.insertContacto(r => {
            if(r.status == 200){
                alert("INFORMACIÓN GUARDADA CORRECTAMENTE","CONFIRMAR","success")
                $scope.contacto = {}
                $scope.newContacto()
            }
        },$scope.contacto)
    }

    $scope.guardarFacturacion = function(){
        $scope.facturacion.id_cliente = $scope.data.id_cliente
        $scope.peticiones.insertFacturacion(r => {
            if(r.status == 200){
                alert("INFORMACIÓN GUARDADA CORRECTAMENTE","CONFIRMAR","success")
                $scope.facturacion = {}
                $scope.newFacturacion()
            }
        },$scope.facturacion)
    }

    $scope.editContacto = function(contacto){
        $scope.peticiones.editContacto(r => {
            if(r.status == 200){
                alert("INFORMACIÓN GUARDADA CORRECTAMENTE","CONFIRMAR","success")
                
                $scope.newContacto()
            }
        },contacto)
    }

    $scope.editFacturacion = function(facturacion){
        $scope.peticiones.editFacturacion(r => {
            if(r.status == 200){
                alert("INFORMACIÓN GUARDADA CORRECTAMENTE","CONFIRMAR","success")
                $scope.newFacturacion()
            }
        },facturacion)
    }

    $scope.eliminarContacto = function(id){
        $scope.contacto.id = id
        $scope.peticiones.eliminarContacto(r => {
            if(r.status == 200){
                alert("REGISTRO ELIMINADO CORRECTAMENTE","CONFIRMAR","success")
                $scope.newContacto()
            }
        },$scope.contacto) 
    }

    $scope.eliminarFacturacion = function(id){
        $scope.facturacion.id = id
        $scope.peticiones.eliminarFacturacion(r => {
            if(r.status == 200){
                alert("REGISTRO ELIMINADO CORRECTAMENTE","CONFIRMAR","success")
                $scope.newFacturacion()
            }
        },$scope.facturacion) 
    }

    $scope.cancelar = function(){
        window.location.href = "/clienteList"
    }

    $scope.principalContacto = function(row){
        $scope.peticiones.principalContacto(r => {
            if(r.status == 200){
                alert("CONTACTO PRINCIPAL ASIGNADO CORRECTAMENTE","CONFIRMAR","success")
                setTimeout(() => {
                    $scope.newContacto()
                }, 300)
            }
        },row) 
    }

    $scope.principalFacturacion = function(row){
        $scope.peticiones.principalFacturacion(r => {
            if(r.status == 200){
                alert("CONTACTO PRINCIPAL ASIGNADO CORRECTAMENTE","CONFIRMAR","success")
                setTimeout(() => {
                    $scope.newFacturacion()
                }, 300)
            }
        },row) 
    }

    limpiar = function(){
        $scope.cliente = {}
        $scope.contacto = {}
        $scope.facturacion = {}
    }

    $scope.init()

}]);