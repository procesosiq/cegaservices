<style type="text/css" media="screen">
    .textoFire{
        color: rgb(34, 34, 34); 
        font-family: arial, sans-serif; 
        font-size: 10px; 
        line-height: normal;
    }
    .borderBox {
        border: solid 1px;
        padding: 5px;
    }
</style>
            <div class="page-content-wrapper" ng-app="app" ng-controller="cotizacionController" ng-cloak>
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content" ng-init="init()">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>COTIZACIONES</h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="/cotizacionList">Lista de cotizaciones</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Registro de cotizaciones</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Begin: life time stats -->
                            <div class="portlet light portlet-fit portlet-datatable bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase"> HACER COTIZACIÓN</span>
                                    </div>
                                    <div class="actions">
                                        <!-- <button type="buttton" ng-click="saveCotizacion()" class="btn green-jungle">
                                            Guardar Cotizacion
                                        </button> -->
                                       <div class="dt-buttons">
                                            <a class="dt-button buttons-print btn dark btn-outline" tabindex="0" aria-controls="datatable_ajax">
                                                <span>Print</span>
                                            </a>
                                            <a class="dt-button buttons-pdf buttons-html5 btn green btn-outline" tabindex="0" aria-controls="datatable_ajax">
                                                <span>PDF</span>
                                            </a>
                                            <a class="dt-button buttons-excel buttons-html5 btn yellow btn-outline" tabindex="0" aria-controls="datatable_ajax">
                                                <span>Excel</span>
                                            </a>
                                            <a class="dt-button buttons-csv buttons-html5 btn purple btn-outline" tabindex="0" aria-controls="datatable_ajax">
                                                <span>CSV</span>
                                            </a>
                                            <a class="dt-button btn blue btn-outline btnSave" ng-click="saveCotizacion()" tabindex="0" aria-controls="datatable_ajax">
                                                <span> Guardar Cotizacion</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs nav-tabs-lg">
                                            <li class="active">
                                                <a href="#tab_1" data-toggle="tab" style="display: none;"> Detalles </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_1">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12">
                                                        <div class="portlet yellow-crusta box">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-cogs"></i>Detalle de la Cotización </div>
                                                                <div class="actions">
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body">
                                                                <div class="row static-info">
                                                                    <div class="col-md-5 name"> Cotización #: </div>
                                                                    <div class="col-md-7 value"> <span id="folio">{{cotizacion.folio}}</span>
                                                                        <button ng-click="sendEmail()" class="btn btn-small label label-info label-sm"> Enviar Email de Confirmacion </button>
                                                                    </div>
                                                                </div>
                                                                <div class="row static-info">
                                                                    <div class="col-md-5 name"> Fecha y hora: </div>
                                                                    <div class="col-md-7 value"> {{cotizacion.fecha}} </div>
                                                                </div>
                                                                <div class="row static-info">
                                                                    <div class="col-md-5 name"> Estado de la Cotización: </div>
                                                                    <div class="col-md-7 value">
                                                                        <span class="font-red-mint"> {{cotizacion.status}} </span>
                                                                    </div>
                                                                </div>
                                                                <div class="row static-info">
                                                                    <div class="col-md-5 name"> Gran Total: </div>
                                                                    <div class="col-md-7 value"> {{cotizacion.total | currency}} </div>
                                                                </div>
                                                                <div class="row static-info">
                                                                    <div class="col-md-5 name"> Forma de Pago: </div>
                                                                    <div class="col-md-7 value"> 
                                                                     <select name="forma_pago" class="form-control" ng-model="cotizacion.forma_pago" id="forma_pago" ng-disabled="cotizacion.id_cliente == 0">
                                                                            <option ng-repeat="(key , value) in options.forma_pago" 
                                                                                    ng-selected="value == cotizacion.forma_pago" 
                                                                                    value="{{value}}">
                                                                                    {{value}}
                                                                            </option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12">
                                                        <div class="portlet blue-hoki box">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-cogs"></i>Información del Cliente </div>
                                                                <div class="actions">
                                                                    <button class="btn c-btn-border-1x c-btn-blue-hoki" 
                                                                     type="button" id="nuevoCliente" ng-click="clearClient()" >Nuevo Cliente</button>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body">
                                                                <div class="row static-info">
                                                                    <div class="col-md-5 name"> RUC o C.I: </div>
                                                                    <div class="col-md-7"> <input type="text" id="ruc" name="ruc" ng-keyup="getClient()" ng-model="cotizacion.ruc" value=""><small id="msj"></small> </div>
                                                                </div>
                                                                <div class="row static-info">
                                                                    <div class="col-md-5 name"> Tipo de cliente: </div>
                                                                    <div class="col-md-7"> 
                                                                        <select name="tipo_cliente" ng-model="cotizacion.tcliente" class="form-control formClass" id="tipo_cliente" disabled>
                                                                            <option ng-repeat="(key , value) in options.tipo_cliente" 
                                                                                    ng-selected="value.id == cotizacion.tcliente" 
                                                                                    value="{{value.id}}">
                                                                                    {{value.nombre}}
                                                                            </option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="row static-info">
                                                                    <div class="col-md-5 name"> Nombre del cliente: </div>
                                                                    <div class="col-md-7"> <input type="text" class="form-control formClass" id="ruc" name="ruc" value="" ng-model="cotizacion.nombre_cliente" readonly placeholder="Nombre del cliente"> </div>
                                                                </div>
                                                                <div class="row static-info">
                                                                    <div class="col-md-5 name"> Razón social: </div>
                                                                    <div class="col-md-7"> <input type="text" class="form-control formClass" id="ruc" name="ruc" value="" readonly ng-model="cotizacion.razon_social" placeholder="Razón social"> </div>
                                                                </div>
                                                                <div class="row static-info">
                                                                    <div class="col-md-5 name"> Email: </div>
                                                                    <div class="col-md-7"> <input type="text" class="form-control formClass" id="ruc" name="ruc" value="" readonly ng-model="cotizacion.email" placeholder="Email"> </div>
                                                                </div>
                                                                <div class="row static-info">
                                                                    <div class="col-md-5 name"> Telefono: </div>
                                                                    <div class="col-md-7"> <input type="text" class="form-control formClass" id="ruc" name="ruc" value="" readonly ng-model="cotizacion.telefono" placeholder="Telefono"> </div>
                                                                </div>
                                                                <div class="row static-info">
                                                                    <div class="col-md-6 name"> </div>
                                                                    <div class="col-md-6"> 
                                                                        <div class="col-md-6"> 
                                                                            <button class="btn green-jungle" id="saveCliente" type="button" ng-click="save()" >
                                                                                Guardar
                                                                            </button> 
                                                                        </div>
                                                                        <div class="col-md-6"> 
                                                                            <button class="btn red-mint" id="cancelCliente" type="button" ng-click="cancel()" >
                                                                                Cancelar
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12">
                                                        <div class="portlet grey-cascade box">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-cogs"></i>Lista de Productos y Servicios </div>
                                                                <div class="actions">
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body">
                                                                <div class="table-responsive">
                                                                    <table class="table table-hover table-bordered table-striped">
                                                                        <thead>
                                                                            <tr>
                                                                                <th width="20%"> Tipo de Trabajo </th>
                                                                                <th width="38%"> Descripción </th>
                                                                                <th width="10%"> Cantidad </th>
                                                                                <th width="12%"> Precio </th>
                                                                                <th width="10%"> Total </th>
                                                                                <th width="10%" class="detailsDelete"> Acciones </th>
                                                                            </tr>
                                                                            <tr id="headTable">
                                                                                <td width="20%"> 
                                                                                    <select name="tipo_trabajo" class="form-control tipo_trabajo" id="tipo_trabajo">
                                                                                        <option ng-repeat="tWork in options.tipo_trabajo" 
                                                                                                value="{{tWork.id}}" class="{{tWork.class}}">
                                                                                                {{tWork.name}}
                                                                                        </option>
                                                                                    </select>
                                                                                </td>
                                                                                <td width="45%"> 
                                                                                    <input type="text" class="form-control" id="des" name="des" value="" placeholder="Descripcion" ng-disabled="cotizacion.id_cliente == 0"> 
                                                                                </td>
                                                                                <td width="5%"> 
                                                                                    <input type="number" min="1" class="form-control countRow" id="count" name="count" value="" placeholder="Cantidad" ng-disabled="cotizacion.id_cliente == 0"> 
                                                                                </td>
                                                                                <td width="10%"> 
                                                                                    <input type="number" min="1" class="form-control countRow" id="price" name="price" value="" placeholder="Precio" ng-disabled="cotizacion.id_cliente == 0"> 
                                                                                </td>
                                                                                <td width="10%"> 
                                                                                    <input type="number" class="form-control" id="total" name="total" value="" placeholder="Total" ng-disabled="0 == 0"> 
                                                                                </td>
                                                                                <td align="center" colspan="2" width="20%"> <button type="button" class="btn green-jungle" ng-click="addRow()"><i class="fa fa-plus-square-o"  aria-hidden="true" ng-disabled="cotizacion.id_cliente == 0"></i></button> </td>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                           <tr ng-repeat="details in cotizacion.details">
                                                                                <td style="vertical-align: middle !important;">
                                                                                    <!-- <span class="form-control label {{details.class_trabajo}}">{{details.des_trabajo}}</span> -->
                                                                                     <select name="tipo_trabajo{{}}" class="form-control tipo_trabajo_details" id="tipo_trabajo">
                                                                                        <option ng-repeat="tWork in options.tipo_trabajo" 
                                                                                                value="{{tWork.id}}" ng-selected="tWork.id == details.tipo_trabajo" class="{{tWork.class}}">
                                                                                                {{tWork.name}}
                                                                                        </option>
                                                                                    </select>
                                                                                </td>
                                                                               <td style="vertical-align: middle !important;">{{details.des}}</td>
                                                                               <td style="vertical-align: middle !important;">{{details.count}}</td>
                                                                               <td style="vertical-align: middle !important;">{{details.price | currency}}</td>
                                                                               <td style="vertical-align: middle !important;">{{(details.count * details.price) | currency}}</td>
                                                                               <td style="vertical-align: middle !important;" class="detailsDelete"><button type="button" ng-click="dropRow(details , $index)" class="btn red-mint">Eliminar</button></td>
                                                                           </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6"> </div>
                                                    <div class="col-md-6">
                                                        <div class="well">
                                                            <div class="row static-info align-reverse">
                                                                <div class="col-md-8 name"> Sub Total: </div>
                                                                <div class="col-md-3 value"> {{cotizacion.subtotal | currency}} </div>
                                                            </div>
                                                            <div class="row static-info align-reverse">
                                                                <div class="col-md-8 name"> Descuento: </div>
                                                                <div class="col-md-3 value input-group"> <input type="number" class="form-control" id="discount" name="discount" value="" ng-model="cotizacion.discount" placeholder="Descuento" 
                                                                 ng-blur="cotizacion.getTotals()" style="text-align: right;" ng-disabled="cotizacion.id_cliente == 0">  
                                                                 <span class="input-group-addon">
                                                                    %
                                                                </span>
                                                                 </div>
                                                            </div>
                                                             <div class="row static-info align-reverse">
                                                                <div class="col-md-8 name"> Sub Total: </div>
                                                                <div class="col-md-3 value"> {{cotizacion.sumsubtotal | currency}} </div>
                                                            </div>
                                                            <div class="row static-info align-reverse">
                                                                <div class="col-md-8 name"> IVA: </div>
                                                                <div class="col-md-3 value"> {{cotizacion.iva | currency}} </div>
                                                            </div>
                                                            <div class="row static-info align-reverse">
                                                                <div class="col-md-8 name"> Total a pagar: </div>
                                                                <div class="col-md-3 value"> {{cotizacion.total | currency}} </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="row static-info">
                                                            <div class="col-md-2 name"> Observaciones: </div>
                                                            <div class="col-md-10 obser"> <textarea id="summernote_1" name="summernote_1"></textarea> </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="row static-info">
                                                            <!-- <div class="col-md-2 name"> Observaciones: </div> -->
                                                            <div class="col-md-10 col-sm-offset-2 textfire"> 
                                                                <textarea id="summernote_2" name="summernote_2">
                                                                    <div class="borderBox">
                                                                        <p><span style="font-weight: bold;">La oferta no incluye:</span></p>
                                                                        <ul>
                                                                        <li>Transporte fuera de la ciudad de Guayaquil (al menos que se indique cobro de viaticos)</li>
                                                                        <li>Trabajos de albañilería, pintura, u otra adecuación civil</li>
                                                                        <li>Apertura, sellado, Impermeabilización, de boquetes por losa, tumbado o paredes para paso de ductos, tubería y demás requerimientos para instalación de material y equipos.</li>
                                                                        <li>Acometidas eléctricas, breakers y puntos para termostatos y drenajes.</li>
                                                                        <li><span style="line-height: 1.42857;">Cualquier otro trabajo, equipo y/o material no estipulado en la oferta.</span></li>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="borderBox">
                                                                        <p><span style="font-weight: bold;">Validez de la oferta:</span></p>
                                                                            <ul>
                                                                            <li>15 días o hasta agotar stock de equipos y/o materiales</li>
                                                                            </ul>
                                                                    </div>
                                                                    <div class="borderBox">
                                                                        <p><span style="font-weight: bold;">Forma de pago:</span></p>
                                                                        <ol type="A">
                                                                        <li>Precio establecido con descuento para pago directo por medio de cheque o transferencia bancaria a favor de CEGASERVICES S.A.</li>
                                                                        <li>Disponible pago con tarjeta de crédito DINERS, VISA, MASTERCARD para el cual no aplica el descuento otorgado y cuyo costo final será confirmado una ve indicado medio de pago con tarjeta de crédito.</li>
                                                                        </ol>
                                                                    </div>
                                                                    <div class="borderBox">
                                                                        <p><span style="font-weight: bold;">Garantía:</span></p>
                                                                        <ul>
                                                                        <li><span style="line-height: 1.42857; font-weight: bold;">Mano de obra:</span></li>
                                                                        </ul>
                                                                        <p style="margin-left: 75px;"><span style="text-decoration: underline; font-style: italic;">Mantenimientos&nbsp;</span>: Para equipos de requerimiento de mantenimiento mensual o bi-mensual garantía de 15 días, para trimestrales o más se otorga garantía de 1 mes en los trabajos realizados. No incluye fallas por desgaste o uso de las partes / equipo que pueden presentarse después de un mantenimiento.</p>
                                                                        <p style="margin-left: 75px;"><span style="text-decoration: underline; font-style: italic;">Instalación y correctivos :</span> 3 semanas de garantía ya que las fallas en este rubro se presentan siempre en las primeras 3 semanas de realizado un trabajo.</p>
                                                                        <ul>
                                                                        <li><span style="line-height: 1.42857;"><span style="font-weight: bold;">Repuestos:</span> Garantía por fallas de fabricación de 3 meses para compresores, turbinas y motores eléctricos y de 1 mes para partes y componentes eléctricos menores.</span><br></li>
                                                                        <li><span style="font-weight: bold;">Equipos ECOX:</span> partes y piezas 1 año, compresor 3 años por fallas de fabricación</li>
                                                                        </ul>
                                                                        <p><span style="line-height: 1.42857; font-style: italic; font-size: 11px;">Para todas las áreas, la garantía no cubre fallas por problemas eléctricos, variaciones de voltaje, mala instalación y manipulación (si no realizado por personal calificado por CEGASERVICES S.A.), falta de mantenimiento, mal manejo o uso que incluye el no seguir las recomendaciones indicadas por le empresa</span><br></p>
                                                                    </div>
                                                                    <div class="borderBox">
                                                                        <p><span style="font-weight: bold;">Tiempo aproximado de trabajo:</span></p>
                                                                        <ul>
                                                                        <li>Ingreso y ejecución según coordinación con cliente</li>
                                                                        </ul>
                                                                    </div>
                                                                </textarea> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End: life time stats -->
                        </div>
                    </div>
                    <!-- BEGIN PAGE BASE CONTENT -->

                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT