$(function(){

    $('.date-picker').datepicker({
        rtl: App.isRTL(),
        autoclose: true
    });

    function isEmail(email) {
      var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      return regex.test(email);
    }

    $("#btnadd").click(function(){
        if($("#txtnom").val()==''){
            alert("Favor de ingresar un nombre de cliente");
            return false;
        }
        else if($("#txtruc").val() != '' && !($("#txtruc").val().length == 10 || $("#txtruc").val().length == 13)){
            alert("Favor de ingresar el RUC o Cédula de Identidad");
            return false;
        }
        else{ 
            var email = $("#txtemail").val() || ""
            var cargo = $("#txtCargo").val() || ""
            var nombre_contacto = $("#txtnom_contacto").val() || ""
            var tel_contacto = $("#txttel_contacto").val() || ""

            $.ajax({
				type: "POST",
				url: "controllers/index.php",
				data: "accion=clientes.AddCliente&txtnom="+$("#txtnom").val()+"&txtemail="+email+"&s_tipocli="+$("#s_tipocli").val()+
                "&txtfec="+$("#txtfec").val()+"&s_sucursales="+$("#s_sucursales").val()+"&txtrazon="+$("#txtrazon").val()+
                "&txtruc="+$("#txtruc").val()+"&txtdircli="+$("#txtdircli").val()+"&txttel="+$("#txttel").val()+"&txtCargo="+cargo+
                "&txttel_contacto="+tel_contacto+"&txtnom_contacto="+nombre_contacto+"&txtreferencia="+$("#txtrefcli").val(),
				success: function(msg){
                    alert("Cliente registrado con el ID "+ msg , "CLIENTES" , 'success' , function(){
                        if(msg > 0){
                            document.location.href='/clientes?id='+msg;
                            //document.location.href='/clienteList';
                        }
                    });
                    $('input[type=text]').each(function() {
                        $(this).val('');
                    });
				}
			});
        }
        return false;
	});
    
    $("#btnupd").click(function(){
        
        if($("#txtnom").val()==''){
            alert("Favor de ingresar un nombre de cliente");
            return false;
        }
        else if($("#txtruc").val().trim() != '' && !($("#txtruc").val().trim().length == 10 || $("#txtruc").val().trim().length == 13)){
            alert("Favor de ingresar el RUC o Cédula de Identidad");
            return false;
        } else {
            var email = $("#txtemail").val() || ""
            var cargo = $("#txtCargo").val() || ""
            var nombre_contacto = $("#txtnom_contacto").val() || ""
            var tel_contacto = $("#txttel_contacto").val() || ""

            $.ajax({
				type: "POST",
				url: "controllers/index.php",
				data: "accion=clientes.UpdateCliente&txtnom="+$("#txtnom").val()+"&txtemail="+email+"&s_tipocli="+$("#s_tipocli").val()+
                "&txtfec="+$("#txtfec").val()+"&s_sucursales="+$("#s_sucursales").val()+"&txtrazon="+$("#txtrazon").val()+
                "&txtruc="+$("#txtruc").val()+"&txtdircli="+$("#txtdircli").val()+"&txttel="+$("#txttel").val()+"&txtCargo="+cargo+"&idcli="+$("#idcli").val()+
                "&txttel_contacto="+tel_contacto+"&txtnom_contacto="+nombre_contacto+"&txtreferencia="+$("#txtrefcli").val(),
				success: function(msg){ 
                    if(msg){
                        alert("Cliente modificado con éxito" , "CLIENTES" , 'success' , function(){
                            document.location.href='/clienteList';
                        });
                    }
                    else{
                        alert("Error encontrado, favor de intentar más tarde");
                    }
				}
			});
        }
        return false;
	});

    // $("#s_tipocli").change(function(){
    //     alert("cambio");
    // });
    
    $("#btnCancelar").click(function(){
        window.location.assign("http://cegaservices2.procesos-iq.com/clienteList");
    });

    $("#btnAddcontact").on("click" , function(){
        addRow()
    });
	
	$("#btnAddFac").on("click" , function(){
        addRowF()
    });

    function addRow(){
        var data = {
            id : "<? echo $_GET['id']?>",
            nombre : $("#txtnom_contacto").val(),
            cargo : $("#txtCargo").val(),
            telefono : $("#txttel_contacto").val(),
            correo : $("#txtemail").val(),
            area : $("#s_areas").val()
        }

        /*if(!isEmail($("#txtemail").val())){
            alert("Favor de ingresar un correo valido");
            return false;
        }*/

        if(data.lote != ""){
            console.log(data);
            ahttp.post("./controllers/index.php?accion=clientes.addContact",printData , data);
        }
    }
	
	function addRowF(){
        var data = {
            id : "<? echo $_GET['id']?>",
            rs : $("#txtRsF").val(),
            ruc : $("#txtRucF").val(),
            dir : $("#txtDirF").val(),
            tel : $("#txtTelF").val(),
            correo : $("#txtemailfac").val(),
        }

        if(data.lote != ""){
            console.log(data);
            ahttp.post("./controllers/index.php?accion=clientes.addFact",printDataF , data);
        }
    }

    $("#rowsFilas").on("click" , ".editRow" , function(){
        var id = this.id;
        editRow(id);
    });
	
	$("#rowsFilasF").on("click" , ".editRowF" , function(){
        var id = this.id;
        editRowF(id);
    });

    $("#facturaciones").on("click", "input[type=checkbox]", function(){

        var is_principal = $(this).hasClass("principal")
        var data = {
            id_fact : $(this)[0].id.split("_")[0],
            id : "<? echo $_GET['id']?>",
            principal : is_principal
        }

        if(confirm( is_principal
            ? "Desea desmarcar esta opción como principal?"
            : "Desea marcar esta opción como principal?"
            )){
            ahttp.post("./controllers/index.php?accion=clientes.mainFact", printDataF , data);
        }
    })

    function editRow(id){
         var data = {
            id : "<? echo $_GET['id']?>",
            id_contacto : id,
            nombre : $("#nombre_" + id).val(),
            cargo : $("#cargo_" + id).val(),
            telefono : $("#telefono_" + id).val(),
            correo : $("#correo_" + id).val(),
            area : $("#area_" + id).val()
        }

        /*if(!isEmail(data.correo)){
            alert("Favor de ingresar un correo valido");
            return false;
        }*/

        if(data.lote != ""){
            console.log(data);
            ahttp.post("./controllers/index.php?accion=clientes.editContact",printData , data);
        }
    }
	
	function editRowF(id){
		var data = {
            id : "<? echo $_GET['id']?>",
            id_fact : id,
            rs : $("#rsF_"+id).val(),
            ruc : $("#rucF_"+id).val(),
            dir : $("#dirF_"+id).val(),
            tel : $("#telF"+id).val(),
            correo : $("#correoF"+id).val(),
        }

        if(data.lote != ""){
            console.log(data);
            ahttp.post("./controllers/index.php?accion=clientes.editFact",printDataF , data);
        }
    }

    $("#rowsFilas").on("click" ,".removeRow", function(){
        var id = this.id;
        removeRow(id)
    });
	$("#rowsFilasF").on("click" ,".removeRowF", function(){
        var id = this.id;
        removeRowF(id)
    });

    function removeRow(id){
        console.log(id);
        if(confirm("Esta seguro de eliminar el contacto?")){
            if(id && id > 0){
                var data = {
                    id : "<? echo $_GET['id']?>",
                    id_contacto : id
                }
              ahttp.post("./controllers/index.php?accion=clientes.removeContact",printData , data);  
            }
        }
    }
	
	function removeRowF(id){
        console.log(id);
        if(confirm("Esta seguro de eliminar el contacto?")){
            if(id && id > 0){
                var data = {
                    id : "<? echo $_GET['id']?>",
                    id_fact : id
                }
              ahttp.post("./controllers/index.php?accion=clientes.removeFact",printDataF , data);  
            }
        }
    }

    function getData(){
        var data = {
            id : "<? echo $_GET['id']?>"
        }

        ahttp.post("./controllers/index.php?accion=clientes.Contactos",printData , data);
    }
	function getDataF(){
        var data = {
            id : "<? echo $_GET['id']?>"
        }

        ahttp.post("./controllers/index.php?accion=clientes.Facturacion",printDataF , data);
    }

    function printData(r , b){
        b();
        if(r){
            var inHTML = [];
            $.each(r, function(index, value){
                inHTML.push("<tr>");
                    inHTML.push("<td>");
                    inHTML.push(value.id);
                    inHTML.push("</td>");
                    inHTML.push("<td>");
                    // inHTML.push(value.nombre);
                    inHTML.push('<input type="text" id="nombre_'+value.id+'" class="form-control" value="'+value.nombre+'"');
                    inHTML.push("</td>");
                    inHTML.push("<td>");
                    // inHTML.push(value.cargo);
                    inHTML.push('<input type="text" id="cargo_'+value.id+'" class="form-control" value="'+value.cargo+'"');
                    inHTML.push("</td>");
                    inHTML.push("<td>");
                    // inHTML.push(value.telefono);
                    inHTML.push('<input type="text" id="telefono_'+value.id+'" class="form-control" value="'+value.telefono+'"');
                    inHTML.push("</td>");
                    inHTML.push("<td>");
                    inHTML.push('<input type="text" id="correo_'+value.id+'" class="form-control" value="'+value.correo+'"');
                    // inHTML.push(value.correo);
                    inHTML.push("</td>");
                    inHTML.push("<td>");
                    inHTML.push('<input type="text" id="area_'+value.id+'" class="form-control" value="'+value.area+'"');
                    // inHTML.push(value.area);
                    inHTML.push("</td>");
                    inHTML.push("<td>");
                    inHTML.push('<button type="button" class="btn btn-sm blue editRow" id="'+value.id+'">Editar</button>');
                    inHTML.push('<button type="button" class="btn btn-sm red-thunderbird removeRow" id="'+value.id+'">Eliminar</button>');
                    inHTML.push("</td>");
                inHTML.push("</tr>"); 
            });

            $("#rowsFilas").html(inHTML.join("")); //add generated tr html to corresponding table
            $("#txtnom_contacto").val('');
            $("#txtCargo").val('');
            $("#txttel_contacto").val('');
            $("#txtemail").val('');
            $("#s_areas").val('');
        }
    }
	function printDataF(r , b){
        b();
        if(r){
            var inHTML = [];
            $.each(r, function(index, value){
                inHTML.push("<tr>");
                    inHTML.push("<td>");
                    inHTML.push(value.id);
                    inHTML.push("</td>");

                    inHTML.push("<td>");
                    if(value.principal == 0){
                        inHTML.push('<input type="checkbox" class="btn btn-sm blue main" data-id="'+value.id+'" id="'+value.id+'_'+index+'"/>');
                    }else{
                        inHTML.push('<input type="hidden" class="btn btn-sm blue main" id="id_fact_main" value="'+value.id+'"/>');
                        inHTML.push("Principal");
                        inHTML.push('<input type="checkbox" class="btn btn-sm blue main principal" data-id="'+value.id+'" id="'+value.id+'_'+index+'" checked=""/>');
                    }
                    inHTML.push("</td>");

                    inHTML.push("<td>");
                    inHTML.push('<input type="text" id="rsF_'+value.id+'" class="form-control" value="'+value.rs+'"');
                    inHTML.push("</td>");

                    inHTML.push("<td>");
                    inHTML.push('<input type="text" id="rucF_'+value.id+'" class="form-control" value="'+value.ruc+'"');
                    inHTML.push("</td>");

                    inHTML.push("<td>");
                    inHTML.push('<input type="text" id="dirF_'+value.id+'" class="form-control" value="'+value.direccion+'"');
                    inHTML.push("</td>");

                    inHTML.push("<td>");
                    inHTML.push('<input type="text" id="telF'+value.id+'" class="form-control" value="'+value.telefono+'"');
                    inHTML.push("</td>");

                    inHTML.push("<td>");
                    inHTML.push('<input type="text" id="correoF'+value.id+'" class="form-control" value="'+value.correo+'"');
                    inHTML.push("</td>");

                    inHTML.push("<td>");
                    inHTML.push('<button type="button" class="btn btn-sm blue editRowF" id="'+value.id+'">Editar</button>');
                    inHTML.push('<button type="button" class="btn btn-sm red-thunderbird removeRowF" id="'+value.id+'">Eliminar</button>');
                    inHTML.push("</td>");
                inHTML.push("</tr>"); 
            });

            $("#rowsFilasF").html(inHTML.join("")); //add generated tr html to corresponding table
			$("#txtRsF").val('');
            $("#txtRucF").val('');
            $("#txtDirF").val('');
            $("#txtTelF").val('');
            $("#txtemailfac").val('');
        }
    }
    
    getData();
    getDataF();
    
});