
var chartTipoCliente = {
        config: {
            div: 'chart_tipo_cliente',
            theme: 'macarons'
        },

        chart: {
            tooltip: {
                trigger: 'item',
                formatter: "{a} <br/>{b}: {c} ({d}%)"
            },
            legend: {
                orient: 'vertical',
                x: 'left',
                data:['Testamento de alguien 1','Test 2','Test 3','Test 4','Test 5']
            },
            toolbox: {
                show : true,
                feature : {
                    mark : {show: true},
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            legend: {
                orient: 'vertical',
                x: 'left',
                data:['Testamento de alguien 1','Test 2','Test 3','Test 4','Test 5']
            },
            calculable : true,
            series: [
                {
                    name:'Prueba',
                    type:'pie',
                    avoidLabelOverlap: false,
                    radius: ['50%', '70%'],
                    center: ['50%', '60%'],
                    label: {
                        normal: {
                            show: false,
                            position: 'center'
                        },
                        emphasis: {
                            show: true,
                            textStyle: {
                                fontSize: '20',
                                fontWeight: 'bold'
                            }
                        }
                    },
                    labelLine: {
                        normal: {
                            show: false
                        }
                    },
                    data:[
                        {value:335, name:'Testamento de alguien 1'},
                        {value:310, name:'Test 2'},
                        {value:234, name:'Test 3'},
                        {value:135, name:'Test 4'},
                        {value:1548, name:'Test 5'}
                    ]
                }
            ]
        }
    };

    var chart2 = echarts.init(document.getElementById(chartTipoCliente.config.div), chartTipoCliente.config.theme);
    chart2.setOption(chartTipoCliente.chart);

    /** TIPO DE EQUIPO **/
    function dibujarGrafica(id){
        var chartTipoEquipo = {
            config: {
                div: 'chart_tipo_equipo_' + id,
                theme: 'macarons'
            },

            chart: {
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b}: {c} ({d}%)"
                },
                legend: {
                    orient: 'vertical',
                    x: 'left',
                    data:['Testamento de alguien ' + id,'Test 2','Test 3','Test 4','Test 5']
                },
                toolbox: {
                    show : true,
                    feature : {
                        mark : {show: true},
                        restore : {show: true},
                        saveAsImage : {show: true}
                    }
                },
                legend: {
                    orient: 'vertical',
                    x: 'left',
                    data:['Testamento de alguien ' + id,'Test 2','Test 3','Test 4','Test 5']
                },
                calculable : true,
                series: [
                    {
                        name:'Prueba',
                        type:'pie',
                        avoidLabelOverlap: false,
                        radius: ['50%', '70%'],
                        center: ['50%', '60%'],
                        label: {
                            normal: {
                                show: false,
                                position: 'center'
                            },
                            emphasis: {
                                show: true,
                                textStyle: {
                                    fontSize: '20',
                                    fontWeight: 'bold'
                                }
                            }
                        },
                        labelLine: {
                            normal: {
                                show: false
                            }
                        },
                        data:[
                            {value:335, name:'Testamento de alguien ' + id},
                            {value:310, name:'Test 2'},
                            {value:234, name:'Test 3'},
                            {value:135, name:'Test 4'},
                            {value:1548, name:'Test 5'}
                        ]
                    }
                ]
            }
        };

        var chart = echarts.init(document.getElementById(chartTipoEquipo.config.div), chartTipoEquipo.config.theme);
        chart.setOption(chartTipoEquipo.chart);
    }
	
function reporte_general(){
    angular.injector(['ng', 'App.Services']).invoke(function (Reporte1) {
        
        Reporte1.getGeneral(100).then(function(r){
            var data = r.data;

            /** CUADROS SUPERIORES **/
            if(data.cuadros){
                $("#cuadro_nuevas").attr("data-value", data.cuadros.nuevas_ordenes);
                $("#cuadro_nuevas").html(data.cuadros.nuevas_ordenes);
                $("#cuadro_facturar").attr("data-value", data.cuadros.por_facturar);
                $("#cuadro_facturar").html(data.cuadros.por_facturar);
                $("#cuadro_cumplimiento").attr("data-value", precise_round(data.cuadros.cumplimiento,2));
                $("#cuadro_cumplimiento").html(precise_round(data.cuadros.cumplimiento,2));
                $("#cuadro_efectividad").attr("data-value", precise_round(data.cuadros.efectividad,2));
                $("#cuadro_efectividad").html(precise_round(data.cuadros.efectividad,2));
            }
            $('.contadores').counterUp();

            /**
             * GRAFICA
             */

            var gdata_cumplimiento = [];
            var gdata_efectividad  = [];
            var gdata_x = [];
            for(var f in data.grafica_general){
                var obj = data.grafica_general[f];
                gdata_cumplimiento.push(obj.cumplimiento);
                gdata_efectividad.push(obj.efectividad);
                gdata_x.push(f);
            }

            var cg_series = {
                legend: ['Cumplimiento', 'Efectividad'],
                data: [
                        {
                            name:'Cumplimiento',
                            type:'line',
                            data:gdata_cumplimiento,
                        },
                        {
                            name:'Efectividad',
                            type:'line',
                            data:gdata_efectividad,
                        }
                    ],
                data_x:gdata_x
            }

            var chartGeneral = {
                config: {
                    div: 'chart_general',
                    theme: 'shine'
                },

                chart: {
                    title : {
                        show: false,
                        text: 'Text',
                        subtext: 'Subtext',
                        borderColor: '#FF00FF',
                        backgroundColor: 'rgba(0,0,0,0)'
                    },
                    tooltip : {
                        trigger: 'axis'
                    },
                    legend: {
                        data: cg_series.legend
                    },
                    toolbox: {
                        show : true,
                        feature : {
                            mark : {show: false},
                            dataView : {show: false, readOnly: true},
                            magicType : {show: true, type: ['line', 'bar']},
                            restore : {show: false},
                            saveAsImage : {show: true}
                        }
                    },
                    dataZoom : {
                        show : true,
                        realtime: true/*,
                        start : 50,
                        end : 100*/
                    },
                    calculable : true,
                    xAxis : [
                        {
                            type : 'category',
                            boundaryGap : false,
                            splitLine: {
                                show:false
                            },
                            data : cg_series.data_x
                        }
                    ],
                    yAxis : [
                        {
                            type : 'value',
                            splitLine: {
                                show:true,
                                lineStyle:{
                                    type: 'dashed'
                                }
                            },
                            axisLabel : {
                                formatter: '{value}'
                            }
                        }
                    ],
                    series: cg_series.data
                }
            };

            var chart1 = echarts.init(document.getElementById(chartGeneral.config.div), chartGeneral.config.theme);
            chart1.setOption(chartGeneral.chart);

        });
    });
}

    /* click en los tabs */
    document.addEventListener('DOMContentLoaded',function(){

        // reporte general
        reporte_general();

        /*
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
          var target = $(e.target).attr("href").toString();
          var lastChar = target.substr(target.length - 1);
          dibujarGrafica(lastChar);
        });

        dibujarGrafica("1");
        */
    });

    function precise_round(num,decimals){
        return Math.round(num*Math.pow(10,decimals))/Math.pow(10,decimals);
    }
	
app.controller('reportes', ['$scope','$http','$interval','client','$controller', function($scope,$http,$interval,client, $controller){

	// get id (de la cotizacion) de la URL
	var cotizacionID = location.search.split('id=')[1];

	$scope.sucursales = [];
	$scope.clientesNombre = [];
	$scope.cotizacion = {
		id : 0,
		id_cliente : 0,
		id_sucursal : 0,
		ruc : "",
		tcliente : "",
		subtotal : "",
		sumsubtotal : "",
		folio : "000000",
		status : "Pendiente",
		forma_pago : "",
		_iva : 0.14,
		iva : "",
		total : 0,
		discount : 0,
		_discount : 0,
		active : 0,
		newClient : 0,
		details : [],
		getTotals : function(){
			$scope.cotizacion.subtotal = 0;
			for (var i = $scope.cotizacion.details.length - 1; i >= 0; i--) {
				if($scope.cotizacion.details[i].count > 0 && $scope.cotizacion.details[i].price > 0){
					$scope.cotizacion.subtotal += ($scope.cotizacion.details[i].count * $scope.cotizacion.details[i].price);
				}
			}
			// console.log($scope.cotizacion.discount)
			var subtotal = $scope.cotizacion.subtotal;
			if($scope.cotizacion.discount && $scope.cotizacion.discount != ""){
				if($scope.cotizacion.subtotal > 0 && $scope.cotizacion.discount > 0 && $scope.cotizacion.discount <= 100){
					$scope.cotizacion._discount = (($scope.cotizacion.discount / 100) * $scope.cotizacion.subtotal);
					subtotal = ($scope.cotizacion.subtotal - $scope.cotizacion._discount);
				}
			}
			$scope.cotizacion.sumsubtotal = subtotal;
			$scope.cotizacion.iva = ($scope.cotizacion._iva * subtotal);
			$scope.cotizacion.total = (subtotal + $scope.cotizacion.iva);

		}
	};
	$scope.options = {
		tipo_cliente : [],
		tipo_trabajo : [],
		forma_pago : ["Contado" , "Credito" , "Tarjeta" ],
		abonos : ["1" , "2" , "3" , "4", "5"],
		tiempo : ["30 días" , "60 días" , "90 días" , "120 días"],
	}

	$scope.clearClient = function(){
		alert($scope.cotizacion.ruc);
		$scope.cotizacion.active = 1;
		
		$(".formClass").removeAttr('disabled');
		$(".formClass").removeAttr('readonly');
		$("#ruc").removeAttr('disabled');
		$("#ruc").removeAttr('readonly');
		$("#nombre_cliente").removeAttr('disabled');
		$("#nombre_cliente").removeAttr('readonly');
		$("#filtro").attr('disabled', 'disabled');
		$("#filtro").attr('readonly', 'readonly');
		$("#saveCliente").css('display', '');
		$("#cancelCliente").css('display', '');
		$("#nuevoCliente").css('display', 'none');
	}

	$scope.cancel = function(){
		$scope.filtro();
		$("#filtro").removeAttr('disabled');
		$("#filtro").removeAttr('readonly');
		$(".formClass").attr('disabled' , 'disabled');
		$(".formClass").attr('readonly' , 'readonly');
		$("#saveCliente").css('display', 'none');
		$("#cancelCliente").css('display', 'none');
	}

	$scope.totalText = function(){
		if(parseInt($("#count").val()) > 0 && parseInt($("#price").val()) > 0){
			var total = $("#count").val() * $("#price").val();
			$("#total").val(total);
		}
	}

	$(".countRow").on("keyup" , function(){
		$scope.totalText();
	});

	$scope.copiaCotizacion = function(id){
		var data = {
				id : id
			}
		if(id > 1){
			if(confirm('¿Seguro que desea generar una copia de esta cotización?')){
				
				client.post("./controllers/index.php?accion=cotizacion.copiaCotizacion" , function(r, b){
			b();
			console.log(r);
			location.href="http://cegaservices2.procesos-iq.com/cotizacion?id="+r;
		} , data);
			}
			}
	}
	$scope.saveCotizacion = function(){
		if($scope.cotizacion.id_cliente <= 0){
			alert("Favor de asignar cliente" , "COTIZACION");
			return false;
		}
		else if($scope.cotizacion.details.length <= 0){
			alert("Favor de ingresar al menos una accion" , "COTIZACION");
			return false;
		}else if($scope.cotizacion.forma_pago == ""){
			alert("Favor de ingresar algun metodo de pago" , "COTIZACION");
			return false;
		}else if($scope.cotizacion.abonos == ""){
			alert("Favor de ingresar algun tipo de abono" , "COTIZACION");
			return false;
		}else{
			console.log($scope.cotizacion.abonos);
			var data = {
				details : $scope.cotizacion.details,
				id_client : $scope.cotizacion.id_cliente,
				observaciones : $("#summernote_1").code(),
				observaciones_base : $("#summernote_2").code(),
				params : $scope.cotizacion
			}
			console.log($scope.cotizacion)
			if($scope.cotizacion.id > 1){
				data.id_cotizacion = $scope.cotizacion.id;
				client.post("./controllers/index.php?accion=cotizacion.edit" ,$scope.finally , data);
				location.href="http://cegaservices2.procesos-iq.com/cotizacionList";
			}
			else{
				client.post("./controllers/index.php?accion=cotizacion.save" ,$scope.finally , data);
				location.href="http://cegaservices2.procesos-iq.com/cotizacionList";
			}
		}
	}

	$scope.finally = function( r , b){
		b();
		// console.log(r);
		if(r && r.id){
			$scope.cotizacion.id = r.id;
			$scope.cotizacion.id_cliente = 0;
			$scope.cotizacion.folio = r.folio;
			$scope.cotizacion.fecha = r.fecha;
			$("input").attr("disabled" , "disabled");
			$("select").attr("disabled" , "disabled");
			$("textarea").attr("disabled" , "disabled");
			$(".detailsDelete").css("display" , "none");
			$("#headTable").css("display" , "none");
			$(".btnSave").css("display" , "none");
			var htmlObservaciones = $("#summernote_1").code();
			$(".obser").empty().html(htmlObservaciones);
			var texto = $("#summernote_2").code();
			$(".textfire").empty().html(texto);
			// $("#summernote_1").summernote('destroy');
			// $("#summernote_2").summernote('destroy');
		}
	}

	$scope.save = function(){
		if($scope.cotizacion.id_cliente > 0){
			alert("Ya existe un cliente seleccionado" , "COTIZACION");
			return false;
		}
		else if($scope.cotizacion.ruc==''){
            alert("Favor de ingresar el RUC o Cédula de Identidad");
            return false;
        }
        else if($scope.cotizacion.ruc.length <= 9){
            alert("Favor de ingresar el RUC o Cédula de Identidad");
            return false;
        }
        else if($scope.cotizacion.ruc.length > 13){
            alert("Favor de ingresar el RUC o Cédula de Identidad");
            return false;
        }
		else if($scope.cotizacion.nombre_cliente == ""){
			alert("Favor de asignar nombre del cliente" , "COTIZACION");
			return false;
		}
		else if($scope.cotizacion.razon_social == ""){
			alert("Favor de ingresar razon social del cliente" , "COTIZACION");
			return false;
		}
		else if($scope.cotizacion.correo_contacto == ""){
			alert("Favor de ingresar email del cliente" , "COTIZACION");
			return false;
		}
		else if($scope.cotizacion.telefono == ""){
			alert("Favor de ingresar telefono del cliente" , "COTIZACION");
			return false;
		}else{
			var data = {
				nombre_cliente : $scope.cotizacion.nombre_cliente,
				razon_social : $scope.cotizacion.razon_social,
				email : $scope.cotizacion.correo_contacto,
				telefono : $scope.cotizacion.telefono,
				tipo_cliente : $scope.cotizacion.tcliente,
				ruc : $scope.cotizacion.ruc,
			}
			
			client.post("./controllers/index.php?accion=cotizacion.saveCliente" ,$scope.registredClient , data);
		}
	}

	$scope.registredClient = function(r , b){
		b();
		if(r && r.id_tipcli){
			$scope.cotizacion.ruc = r.ruc;
			$scope.cancel();
			$scope.getClient2();
		}
	}

	$scope.editRow = function(data , index){
		if(data){
			$scope.cotizacion.details[index].edit = 1;
		}
	}

	$scope.saveRow = function(data , index){
		if(data){
			$scope.cotizacion.details[index].edit = 0;
			$scope.cotizacion.getTotals();
		}
    	console.log( angular.toJson( $scope.cotizacion.details ) );
	}

 	$scope.addRow = function(){
    	var data = {
    		tipo_trabajo : $("#tipo_trabajo").val(),
    		des_trabajo : $("#tipo_trabajo option:selected").text(),
    		class_trabajo : $("#tipo_trabajo option:selected").prop("class"),
    		des : $("#des").val(),
    		count : $("#count").val(),
    		price : $("#price").val(),
    		edit : 0
    	}

    	if(data){
    		//console.log(data);
	    	if(data.count > 0 && data.price > 0 || data.tipo_trabajo == 3){
	    		// console.log($scope.formData.areas);
	    		$scope.cotizacion.details.push(data);
	  			$scope.cotizacion.getTotals();
	  			$scope.clearRow();
			}else if ($scope.cotizacion.id_cliente == 0){
				alert("Debe seleccionar primero un cliente.");
	    		if(data.price < 0) $("#price").val(1);
	    		if(data.count < 0) $("#count").val(1);
			}else{
	    		alert("No puede ingresar numeros negativos");
	    		if(data.price < 0) $("#price").val(1);
	    		if(data.count < 0) $("#count").val(1);
	    	}
    	}
    	console.log( angular.toJson( $scope.cotizacion.details ) );
    }
	
	$scope.validRevision = function(){
		
		if($("#tipo_trabajo").val()==3 && $scope.cotizacion.id_cliente > 0){
			$("#des").removeAttr('disabled');
			$("#des").removeAttr('readonly');
			$("#count").attr('disabled', 'disabled');
			$("#count").attr('readonly', 'readonly');
			$("#count").val(0);
			$("#price").attr('disabled', 'disabled');
			$("#price").attr('readonly', 'readonly');
		}
		else if ($("#tipo_trabajo").val()!=3 && $scope.cotizacion.id_cliente > 0){
			$("#des").removeAttr('disabled');
			$("#count").removeAttr('disabled');
			$("#price").removeAttr('disabled');
			$("#des").removeAttr('readonly');
			$("#count").removeAttr('readonly');
			$("#price").removeAttr('readonly');
		}
    	
    }

    $scope.clearRow = function(){
		$("#tipo_trabajo").val('');
		$("#des").val('');
		$("#count").val('');
		$("#price").val('');
		$("#total").val('');
		$("#tipo_trabajo").removeClass();
		$("#tipo_trabajo").addClass("form-control");
    }

    $scope.dropRow = function(data , index){
    	if(confirm("Esta seguro de Eliminar el registro?")){
	    	var data = data || {};
	    	if(data.tipo_trabajo){
	    		try{
	    			delete $scope.cotizacion.details[index];
	    			$scope.cotizacion.details.splice(index , 1);
	    		}catch(e){
	    			throw Error("Error al Elminar");
	    		}
	    		$scope.cotizacion.getTotals();
	    	}
    	}
    }

	$scope.opciones = {
    availableOptions: [
      {id: '1', name: 'RUC o C.I'},
      {id: '2', name: 'NOMBRE DEL CLIENTE'}
    ],
    selectedOption: {id: '1', name: 'RUC o C.I'} //This sets the default value of the select in the ui
    };
	
	$scope.getClient = function(keyEvent){
		if (keyEvent.which === 13){
		if($scope.cotizacion.ruc.length >= 9){
			var data = {
				ruc : $scope.cotizacion.ruc
			}
			client.post("./controllers/index.php?accion=cotizacion.GetCliente" ,$scope.printClient , data);
		}
		}
	};
	
	$scope.getClient2 = function(){
		if($scope.cotizacion.ruc.length > 9){
			var data = {
				ruc : $scope.cotizacion.ruc
			}
			client.post("./controllers/index.php?accion=cotizacion.GetCliente" ,$scope.printClient , data);
		}
		
	};
	
	$scope.getClientNombre = function(keyEvent){
		if (keyEvent.which === 13){
		var data = {
				nombre_cliente : $scope.cotizacion.nombre_cliente
			}
			client.post("./controllers/index.php?accion=cotizacion.GetClienteNombre" ,$scope.printClient , data);
		
		}
	};

	// $(".tipo_trabajo").on("change" , function(){
	// 	var className = $(this).find("option:selected").attr("class");
	// 	$(this).removeClass();
	// 	$(this).addClass(className + " form-control");
	// })

	$(document).on("change" , '.tipo_trabajo_details' , function(){
		var className = $(this).find("option:selected").attr("class");
		$(this).removeClass();
		$(this).addClass(className + " form-control tipo_trabajo_details");
	});

	$scope.getDes = function(index){
		var select = $("#tipo_trabajo"+index);
		console.log(index);
		console.log(select);
    	console.log($scope.cotizacion.details[index])
		$scope.cotizacion.details[index].des_trabajo = $("#tipo_trabajo"+index+" option:selected").text();
    	$scope.cotizacion.details[index].class_trabajo = $("#tipo_trabajo"+index+" option:selected").prop("class");
    	console.log($scope.cotizacion.details[index])
	}

	$scope.init = function(){
		$("#nuevoCliente").css('display', 'none');
		$("#saveCliente").css('display', 'none');
		$("#cancelCliente").css('display', 'none');
		$('#summernote_1').summernote({height: 300});
		$('#summernote_2').summernote({height: 300});
		$scope.cotizacion.ruc = '';
		client.post("./controllers/index.php?accion=cotizacion" ,$scope.print , {});
	}
	
	$scope.filtro = function() {
		var idfiltro =  parseInt($scope.opciones.selectedOption.id);
        if(idfiltro==1){
			$("#ruc").removeAttr('disabled');
			$("#ruc").removeAttr('readonly');
			
			$scope.cotizacion.tcliente = "";
			$scope.cotizacion.nombre_cliente = "";
			$scope.cotizacion.razon_social = "";
			$scope.cotizacion.correo_contacto = "";
			$scope.cotizacion.telefono = "";
			$("#nombre_cliente").attr('disabled', 'disabled');
			$("#nombre_cliente").attr('readonly', 'readonly');
		}
		else if(idfiltro==2){
			$("#nombre_cliente").removeAttr('disabled');
			$("#nombre_cliente").removeAttr('readonly');
			$scope.cotizacion.ruc = "";
			$scope.cotizacion.tcliente = "";
			$scope.cotizacion.razon_social = "";
			$scope.cotizacion.correo_contacto = "";
			$scope.cotizacion.telefono = "";
			$("#ruc").attr('disabled', 'disabled');
			$("#ruc").attr('readonly', 'readonly');
		}
      };

	$scope.printClient = function(r , b){
		b();
		//console.log(r.data);
		if(r && r.data){
			if(r.ti == 2){
				if(r.data.length > 0){
				console.log(r.data);
				$scope.clientesNombre = r.data;
				$modal2.modal();
				}
				else{
			alert("Cliente no registrado" , "Cotizaciones" , "warning" , function(){
				$("#nuevoCliente").css('display', '');
				$scope.cotizacion.id_cliente = 0;
				$scope.cotizacion.id_cliente = "";
				$scope.cotizacion.id_sucursal = 0;
				$scope.cotizacion.id_sucursal = "";
				$scope.cotizacion.tcliente = "";
				$scope.cotizacion.razon_social = "";
				$scope.cotizacion.correo_contacto = "";
				$scope.cotizacion.telefono = "";
				$(".formClass").val('');
				$scope.cancel();
			});
		}
			}
			else{
			console.log(r.success)
			
			if(r.data.length == 1){
				var data = r.data[0];
				var nombre_cliente = (r.success == 300) ? "Sucursal "+data.nombre_contacto : data.nombre;
				var id_cliente = (r.success == 300) ? data.id_cliente : data.id;
				var id_sucursal = (r.success == 300) ? data.id_cliente : 0;
				$scope.cotizacion.id_cliente = id_cliente;
				$scope.cotizacion.id_sucursal = id_sucursal;
				$scope.cotizacion.ruc = data.ruc;
				$scope.cotizacion.tcliente = data.id_tipcli;
				$scope.cotizacion.nombre_cliente = nombre_cliente;
				$scope.cotizacion.razon_social = data.razon_social;
				//$scope.cotizacion.email = data.email;
				$scope.cotizacion.correo_contacto = data.correo_contacto;
				console.log(data.correo_contacto);
				$scope.cotizacion.telefono = data.telefono;
				$("#nuevoCliente").css('display', 'none');
				$("#saveCliente").css('display', 'none');
			}else{
				if(r.success==250){
					$scope.facturacion = r.data;
					$modal3.modal();
				}
			else{
				$scope.sucursales = r.data;
			$modal.modal();}
			}
			}
			
				
			
		}else{
			alert("Cliente no registrado" , "Cotizaciones" , "warning" , function(){
				$("#nuevoCliente").css('display', '');
				$scope.cotizacion.id_cliente = 0;
				$scope.cotizacion.id_cliente = "";
				$scope.cotizacion.id_sucursal = 0;
				$scope.cotizacion.id_sucursal = "";
				$scope.cotizacion.tcliente = "";
				$scope.cotizacion.razon_social = "";
				$scope.cotizacion.correo_contacto = "";
				$scope.cotizacion.telefono = "";
				$(".formClass").val('');
				$scope.cancel();
			});
		}
	}

	$scope.setClient = function(data , nombre_cliente, toggleModal){
		var tm = toggleModal || false;
		if(data){
			//console.log(data);
			$scope.cotizacion.id_sucursal = data.id;
			$scope.cotizacion.id_cliente = data.id_cliente;
			$scope.cotizacion.tcliente = data.id_tipcli;
			$scope.cotizacion.nombre_cliente = nombre_cliente + data.nombre_contacto;
			$scope.cotizacion.razon_social = data.razon_social;
			//$scope.cotizacion.email = data.email;
			$scope.cotizacion.correo_contacto = data.correo_contacto;
			$scope.cotizacion.telefono = data.telefono;
			if(!toggleModal){
				$("#nuevoCliente").css('display', 'none');
				$("#saveCliente").css('display', 'none');
				$modal.modal('toggle');
			}
		}
	}
	
	$scope.setClientFact = function(data , nombre_cliente, toggleModal){
		var tm = toggleModal || false;
		if(data){
			console.log(data);
			$scope.cotizacion.id_cliente = data.id;
			$scope.cotizacion.ruc = data.ruc;
			$scope.cotizacion.id_raz = data.id_raz;
			$scope.cotizacion.tcliente = data.id_tipcli;
			$scope.cotizacion.nombre_cliente = data.nombre;
			$scope.cotizacion.razon_social = data.razon_social;
			//$scope.cotizacion.email = data.email;
			$scope.cotizacion.correo_contacto = data.correo_contacto;
			$scope.cotizacion.telefono = data.telefono;
			if(!toggleModal){
				$("#nuevoCliente").css('display', 'none');
				$("#saveCliente").css('display', 'none');
				$modal3.modal('toggle');
			}
		}
	}
	
	$scope.setClientNombre = function(data , nombre_cliente, toggleModal){
		var tm = toggleModal || false;
		var data = {
				ruc : data.ruc
			}
			client.post("./controllers/index.php?accion=cotizacion.GetCliente" ,$scope.printClient , data);
			if(!toggleModal){
				$("#nuevoCliente").css('display', 'none');
				$("#saveCliente").css('display', 'none');
				$modal2.modal('toggle');
			}
		
	}

	$scope.print = function(r , b){
		b();
		if(r){
			$scope.options.tipo_cliente = r.tipo_cliente
			$scope.options.tipo_trabajo = r.tipo_trabajo
		}
	}

	$scope.sendMailConfirmation = function(){
		var data = {
			id_cotizacion: $scope.cotizacion.id,
			correo: $scope.cotizacion.correo_contacto
		}
		client.post("./controllers/index.php?accion=cotizacion.sendMailConfirmation" , function(d, r){
			console.log(d);
			console.log(r);
			r();
		} , data);
	}

	$scope.printCotizacion = function(){
		angular.element(".page-header.navbar.navbar-fixed-top").css({display:"none"});
		angular.element(".page-sidebar-wrapper").css({display:"none"});
		angular.element(".page-content").css({"margin-left":"0px"});
		$("input").attr("disabled" , "disabled");
		$("select").attr("disabled" , "disabled");
		$("textarea").attr("disabled" , "disabled");
		$(".detailsDelete").css("display" , "none");
		$("#headTable").css("display" , "none");
		$(".btnSave").css("display" , "none");
		var htmlObservaciones = $("#summernote_1").code();
		$(".obser").empty().html(htmlObservaciones);
		var texto = $("#summernote_2").code();
		$(".textfire").empty().html(texto);
		window.print();
	}
	
  


	$scope.getCotizacionData = function(){
		$scope.cotizacion.id = cotizacionID;
		var data = {
			id_cotizacion: cotizacionID
		}

		client.post("./controllers/index.php?accion=cotizacion.GetCotizacion" , function(r, b){
			b();
			console.log(r);
			$("#summernote_1").code(r.datos.observaciones);
			if(r.status1==2)
				location.href="http://cegaservices2.procesos-iq.com/404";
			$scope.cotizacion = r.datos;
			$scope.cotizacion._iva = 0.14;
			$scope.cotizacion.discount =  parseFloat(r.datos.descuento_porcentaje);
			$scope.cotizacion._discount = parseFloat(r.datos.descuento);
			$scope.cotizacion.sumsubtotal = parseFloat(r.datos.subtotal)-parseFloat(r.datos.descuento);
			$scope.cotizacion.getTotals = function(){
				$scope.cotizacion.subtotal = 0;
				for (var i = $scope.cotizacion.details.length - 1; i >= 0; i--) {
					if($scope.cotizacion.details[i].count > 0 && $scope.cotizacion.details[i].price > 0){
						$scope.cotizacion.subtotal += ($scope.cotizacion.details[i].count * $scope.cotizacion.details[i].price);
					}
				}
				// console.log($scope.cotizacion.discount)
				var subtotal = $scope.cotizacion.subtotal;
				if($scope.cotizacion.discount && $scope.cotizacion.discount != ""){
					if($scope.cotizacion.subtotal > 0 && $scope.cotizacion.discount > 0 && $scope.cotizacion.discount <= 100){
						$scope.cotizacion._discount = (($scope.cotizacion.discount / 100) * $scope.cotizacion.subtotal);
						subtotal = ($scope.cotizacion.subtotal - $scope.cotizacion._discount);
					}
				}
				console.log(subtotal);
				$scope.cotizacion.sumsubtotal = subtotal;
				$scope.cotizacion.iva = ($scope.cotizacion._iva * subtotal);
				console.log( $scope.cotizacion._iva );
				$scope.cotizacion.total = (subtotal + $scope.cotizacion.iva);
				console.log('jsjsjajsa');
			}

			$scope.cotizacion.details = r.datos.details;

			if(r.datos.status == 0)
				$scope.cotizacion.status = 'Pendiente';
			else if (r.datos.status == 1)
				$scope.cotizacion.status = 'Por Aprobar';
			else if (r.datos.status == 2)
				$scope.cotizacion.status = 'Aprobado';

			if(angular.equals({}, r.sucursal)&&angular.equals({}, r.facturacion))
				{$scope.setClient(r.cliente, r.cliente.nombre, true);
			$scope.cotizacion.ruc= r.cliente.ruc;
			console.log('vacion');
			}
			else if(!angular.equals({}, r.sucursal)){
			$scope.setClient(r.sucursal, r.sucursal.nombre_cliente, true);
			$scope.cotizacion.ruc= r.cliente.ruc;	
			console.log('sucursal');
			}
			else
				{
					console.log('factu');
					$scope.cotizacion.ruc = r.facturacion.ruc;
					$scope.cotizacion.tcliente = r.cliente.id_tipcli;
					$scope.cotizacion.nombre_cliente = r.cliente.nombre;
					$scope.cotizacion.razon_social = r.facturacion.rs	;
					$scope.cotizacion.telefono = r.facturacion.telefono	;
				}

			
			$scope.cotizacion.correo_contacto = r.contacto_email;
			$scope.cotizacion.fecha = r.fecha;

		}, data);
	}
	if(cotizacionID){
		$scope.getCotizacionData();
	}

}]);