<style>
    @page {
      size: A4;
    }
    .textoFire{
    color: rgb(34, 34, 34); 
    font-family: arial, sans-serif; 
    font-size: 10px; 
    line-height: normal;
    }
    .borderBox {
        border: solid 1px;
        padding: 5px;
    }
    #ajax-modal {
        top:5%;
        right:50%;
        outline: none;
    }
    .button-estilo{
        background-color : #26C281;
		color:white;
    }
    .button-estilo-quitar {
        background-color : red;
		color:white;
    }
    .margin-total{
        margin-right : 10px;
        /*font-weight: bold;*/
    }
    .margin-total-gen{
        margin-top : 10px;
        margin-right : 40px;
        font-weight: bold;   
    }
    .div-margin{
        margin : 10px;
    }
    .div-margin-top{
        margin-top : 30px;
    }
    .div-margin-left{
        margin-left: 10px !important;
    }
    .margin-top{
        margin-top : 20px;
    }
    .button{
        background-color: white;
        border: white;
    }
    .center-text{
        text-align : center;
    }
    .estilos-form-control{
        height : 30px !important;
        font-size : 10px !important;
    }
    .altura{
        padding: 2px !important
    }
    .color-white{
        color : white !important;
    }
    .color-black{
        color : black !important;
    }
    .tamanio{
        width : 40% !important;
    }
    .label-class {
        margin-left : 20px;
        color : red;
        font-size : 15px;
    }
</style>

<!-- background-color : #E35B5A !important; -->

<div id="cont" class="page-content-wrapper" ng-app="app" ng-controller="trabajosEjec" ng-cloak>
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1>REPORTE PDF DE FORMULARIOS ENVIADOS </h1>
            </div>
        </div>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a>REPORTES PDF'S</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active"></span>
            </li>
        </ul>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet grey-cascade box">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i></div>
                            <div class="actions">
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row div-margin-top">
                                <label class="label-class">*Click al cliente para ver detalles</label>
                                <div class="col-md-12">
                                    <div class="table-responsive table-scrollable">
                                        <table class="table table-hover table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th style="min-width:70px" class="center-text"> ORDEN </th>
                                                    <th style="min-width:150px" class="center-text"> TIPO DE TRABAJO </th>
                                                    <th style="min-width:200px" class="center-text"> CLIENTE </th>
                                                    <th style="min-width:90px" class="center-text"> FECHA </th>
                                                    <th style="min-width:90px" class="center-text"> H. LLEGADA </th>
                                                    <th style="min-width:90px" class="center-text"> H. SALIDA </th>
                                                    <th style="min-width:200px" class="center-text"> REFERENCIA </th>
                                                </tr>                                           
                                                <tr role="row" class="filter">
                                                    <td><input type = "text" ng-model = "searchTable.orden" class = "color-td-prac form-control form-filter"/></td>
                                                    <td><input type = "text" ng-model = "searchTable.tipo_trabajo" class = "color-td-prac form-control form-filter"/>
                                                    <td><input type = "text" ng-model = "searchTable.cliente" class = "color-td-prac form-control form-filter"/>
                                                    <td><input type = "text" ng-model = "searchTable.fecha" class = "color-td-prac form-control form-filter"/>
                                                    <td><input type = "text" ng-model = "searchTable.hora_llegada" class = "color-td-prac form-control form-filter"/>
                                                    <td><input type = "text" ng-model = "searchTable.hora_salida" class = "color-td-prac form-control form-filter"/>
                                                    <td><input type = "text" ng-model = "searchTable.numero_referencia" class = "color-td-prac form-control form-filter"/>
                                                </tr>                                               
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat-start = "row in data | filter : searchTable | orderBy : 'id'" ng-click="row.expanded = !row.expanded">
                                                    <td class="center-text"> {{ row.orden }} </td>
                                                    <td class="center-text"></td>
                                                    <td class="center-text"> {{ row.cliente }} </td>
                                                    <td class="center-text"> {{ row.fecha }} </td>
                                                    <td class="center-text"></td>
                                                    <td class="center-text"></td>
                                                    <td class="center-text"><a ng-if="row.status == 3" href="http://cegaservices2.procesos-iq.com/print_reporte.php?id={{row.id_cotizacion}}" target="_blank">VER REPORTE WEB</a></td>
                                                </tr>
                                                <tr ng-repeat-end="row" ng-show="row.expanded" ng-repeat = "detalle in row.detalle">
                                                    <td class="center-text"></td>
                                                    <td class="center-text  {{ color(detalle.tipo_trabajo) }}"> {{ detalle.tipo_trabajo }} </td>
                                                    <td class="center-text"></td>
                                                    <td class="center-text"> {{ detalle.fecha }}</td>
                                                    <td class="center-text"> {{ detalle.hora_llegada }} </td>
                                                    <td class="center-text"> {{ detalle.hora_salida }} </td>
                                                    <td class="center-text"><a href="https://s3.amazonaws.com/json-publicos/climat/cegaservices/{{detalle.tabla}}/{{detalle.numero_referencia}}.pdf" target="_blank">{{ detalle.numero_referencia }}</a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</div>