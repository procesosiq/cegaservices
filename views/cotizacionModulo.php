<link href="../assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
<style>
    @page {
      size: A4;
    }
    .textoFire{
    color: rgb(34, 34, 34); 
    font-family: arial, sans-serif; 
    font-size: 10px; 
    line-height: normal;
    }
    .borderBox {
        border: solid 1px;
        padding: 5px;
    }
    #ajax-modal {
        top:5%;
        right:50%;
        outline: none;
    }
    .reporte .clearfix:after {
    content: "";
    display: table;
    clear: both;
    }

    .reporte a {
    color: #0087C3;
    text-decoration: none;
    }

    .reporte body {
    position: relative;
    width: 25cm;  
    height: 29.7cm; 
    margin: 0 auto; 
    color: black;
    background: #FFFFFF; 
    font-family: Arial, sans-serif; 
    font-size: 14px; 
    font-family: SourceSansPro;
    }

    .reporte header {
    padding: 10px 0;
    margin-bottom: 20px;
    }

    .reporte #logo {
    float: left;
    margin-top: 8px;
    }

    .reporte #empresa {
    margin-top: 30px;
    text-align:center;
    font-weight: bold;
    }

    .reporte #numero {
        margin-top:20px;
    text-align:center;
    font-size:20px;
    font-weight: bold;
    }

    .reporte #logo img {
    height: 40px;
    }

    .reporte #corp {
    float: right;
    text-align: right;
    }

    .reporte #corp img {
    height: 50px;  
    margin-top: 8px;
    }


    .reporte #client {
    float: left;
    }


    .reporte h2.name {
    font-size: 1.4em;
    font-weight: normal;
    margin: 0;
    }

    .reporte .invoice {
    float: right;
    text-align: right;
    }

    .reporte table {
    width: 100%;
    }

    .reporte table.header th,
    table.header td {
    background: #FFFFFF;
    text-align: center;
    border: 1px solid #000000;
    font-size:9px;
    }

    .reporte table.header th {
    font-weight: bold;
    background: #76923c;
    color:white;
    font-size:9px;
    }

    .reporte #thanks{
    font-size: 1em;
    }

    .reporte #notices{
    padding-left: 6px;
    border-left: 6px solid #0087C3;  
    }

    .reporte #notices .notice {
    font-size: 1.2em;
    }
    .reporte li span { font-weight: normal; }
    footer {
    color: #777777;
    width: 100%;
    height: 30px;
    position: absolute;
    bottom: 0;
    border-top: 1px solid #AAAAAA;
    padding: 8px 0;
    text-align: center;
    }
    .reporte .border{
    border: 1px solid black;
    border-collapse: collapse;
    }
    .reporte .no-border{
        border :0px;
    }
    .reporte .font{
        font-family: Tahoma,Geneva,sans-serif;
        font-size: 12px;
    }
    .reporte .font-bold{
        font-weight: bold;
    }
    .reporte .margin{
        margin-top : 10px;
        margin-left : 10px;
        margin-right : 5px;
        margin-bottom : 5px;
    }
    .reporte .margin-table{
        margin-top : 10px;
    }
    .reporte .table-align{
        text-align: right;
    }
    .reporte .text-informativo{
        font-size: 10px;
        color: #1876e0;
    }
    .reporte .margin-div{
        margin-top: 15px;
        margin-bottom: 12px;
    }
    .reporte .table-td{
        margin: 5px;
    }
    .reporte .table {
        border: 1px solid black;
        border-collapse: collapse;
    }
    .reporte .thead {
        text-align: center;
        font-weight: bold;
        background: #76923c;
        color:white;
        font-size:12px;
    }
    .reporte .tbody {
        text-align: center;
        font-size:12px;
    }
    .reporte .tfoot{
        text-align : right;
        font-size:12px;
        font-weight: bold;
    }
    .reporte .tfoot-td{
        padding : 2px;
        text-align : left;
        font-size:12px;
        font-weight: bold;   
    }
    .reporte .tfoot-td-p{
        padding : 2px;
        text-align : right;
        font-size:12px;
        font-weight: bold;
    }
</style>

<div id="cont" class="page-content-wrapper" ng-app="app" ng-controller="cotizacionController" ng-cloak>
    <div class="page-content" ng-init="init()">
        <div class="page-head">
            <div class="page-title">
                <h1>COTIZACIONES</h1>
            </div>
        </div>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="/cotizacionList">Lista de cotizaciones</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Registro de cotizaciones</span>
            </li>
        </ul>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class = "reporte">
                        <div>
                            <table class="border header">
                                <tr class="border header">
                                        <td width="25%" height="65px"  class="border"><img src="cegaservice.png" style="width: 135px; height: 45px" /></td>
                                        <td width="50%" height="65px" class="border" style="font-family: Tahoma,Geneva,sans-serif;"><strong>CEGASERVICES S.A.</strong><br>Una Empresa del Grupo</td> 
                                        <td width="25%" height="65px" class="border"><img src="cegacorp.png" style="width: 135px; height: 45px"></td>
                                </tr>
                                <tr class="border header" >
                                    <td width="25%" height="30px"  style="font-size: 10px; font-family: Tahoma,Geneva,sans-serif;" class="border header"><b>RUC</b> 0992273399001</td>
                                    <td width="50%" height="30px" class="border header" style="font-family: Tahoma,Geneva,sans-serif;"><strong>C O T I Z A C I &Oacute; N</strong></td> 
                                    <td width="25%" height="30px" style="font-size: 10px; font-family: Tahoma,Geneva,sans-serif;" class="border header">Lotizaci&oacute;n Santa Adriana, solar 4 al 8 Km. 7 V&iacute;a Daule</td>
                                </tr>
                            </table> 
                        </div>
                        <div class = "border margin-div">
                            <table class = "no-border margin-table">
                                <tbody>
                                    <tr>
                                        <td width="20%" class ="no-border table-align">
                                            <label for="" class = "font font-bold margin">Divisi&oacute;n :</label>
                                        </td>
                                        <td width = "40%" class = "no-border">
                                            <label for="" class = "font margin">CLIMATIZACI&Oacute;N & REFRIGERACI&Oacute;N</label>
                                        </td>
                                        <td width = "35%" class = "no-border">
                                            <label for = "" class = "text-informativo font-bold">Asesoría y soluciones integrales</label>
                                            <label for = "" class = "text-informativo font-bold">en proyectos de Climatización y Refrigeración</label>
                                        </td>
                                        <td width = "5%" class = "no-border"></td>
                                    </tr>
                                    <tr>
                                        <td width = "20%" class = "no-border table-align">
                                            <label for="" class = "font font-bold margin">Cotizacion N.&deg; :</label>
                                        </td>
                                        <td width = "40%" class = "no-border">
                                            <input id = "txt_nocotizacion" class = "font margin" type="text" value = "1236545"/>
                                        </td>
                                        <td width = "35%" class = "no-border">
                                            
                                        </td>
                                        <td width = "5%" class = "no-border"></td>
                                    </tr>
                                    <tr>
                                        <td width = "20%" class = "no-border table-align">
                                            <label for="" class = "font font-bold margin">Cotizado por : </label>
                                        </td>
                                        <td width = "40%" class = "no-border"> 
                                            <select id = "sel_cotizadopor" class = "font margin">
                                                <option value = "">
                                                    Todos                                
                                                </option>
                                            </select>
                                        </td>
                                        <td width = "35%" class = "no-border"></td>
                                        <td width = "5%" class = "no-border"></td>
                                    </tr>
                                    <tr>
                                        <td width = "20%" class = "no-border table-align">
                                            <label for="" class = "font font-bold margin">Cliente : </label>
                                        </td>
                                        <td width = "40%" class = "no-border"> 
                                            <input id = "txt_cliente" class = "font margin" type="text" value = "Mariam Rodriguez G."/>
                                        </td>
                                        <td width = "35%" class = "no-border"></td>
                                        <td width = "5%" class = "no-border"></td>
                                    </tr>
                                    <tr>
                                        <td width = "20%" class = "no-border table-align">
                                            <label for="" class = "font font-bold margin">Teléfonos : </label>
                                        </td>
                                        <td width = "40%" class = "no-border"> 
                                            <input id = "txt_telefono_uno" class = "font margin" type="text" value = "6632457819"/>
                                        </td>
                                        <td width = "35%" class = "no-border table-align"></td>
                                        <td width = "5%" class = "no-border"></td>
                                    </tr>
                                    <tr>
                                        <td width = "20%" class = "no-border table-align">
                                            <label for="" class = "font font-bold margin">Email : </label>
                                        </td>
                                        <td width = "40%" class = "no-border"> 
                                            <input id = "txt_email" class = "font margin" type="text" value = "akalixx@akalixx.com">
                                        </td>
                                        <td width = "35%" class = "no-border"></td>
                                        <td width = "5%" class = "no-border"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- TABLA -->
                        <div class = "margin-div">
                            <table class = "table">
                                <thead class = "thead">
                                    <tr>
                                        <th> TIPO DE TRABAJO </th>
                                        <th> UM </th>
                                        <th> DESCRIPCION </th>
                                        <th> CANT </th>
                                        <th> P. UNITARIO </th>
                                        <th> % DCTO </th>
                                        <th> SUBTOTAL </th>
                                    </tr>
                                </thead>
                                <tbody class = "tbody">
                                    <tr>
                                        <td>R1</td>
                                        <td>R1</td>
                                        <td>R1</td>
                                        <td>R1</td>
                                        <td>R1</td>
                                        <td>R1</td>
                                        <td>R1</td>
                                    </tr>
                                </tbody>
                            </table>
                            <table>
                                <tfoot class = "tfoot border">
                                        <tr>
                                            <td class = "tfoot-td-p"> SUBTOTAL </td>
                                            <td colspan = "6" class = "tfoot-td border"> $ </td>
                                        </tr>
                                        <tr>
                                            <td class = "tfoot-td-p"> DESCUENTO TOTAL </td>
                                            <td colspan = "6" class = "tfoot-td border">$ </td>
                                        </tr>
                                        <tr>
                                            <td class = "tfoot-td-p"> IVA 14% </td>
                                            <td colspan = "6" class = "tfoot-td border">$ </td>
                                        </tr>
                                        <tr>
                                            <td class = "tfoot-td-p"> TOTAL </td>
                                            <td colspan = "6" class = "tfoot-td border">$ </td>
                                        </tr>
                                    </tfoot>
                            </table>
                        </div>
                        <!-- TABLA -->
                        <div class = "margin-div">
                            <table class = "border">
                                <thead class = "thead" style = "text-align : left !important">
                                    <tr>
                                        <td> Nota :</td>
                                    </tr>
                                </thead>
                                <tbody class = "tbody" style = "text-align : left !important">
                                    <tr>
                                        <td>
                                            <p>
                                                Instalación básica considera acceso adecuado para la instalación de evaporador y condensador, 
                                                y una distancia no mayor a 3 metros entre las unidades. Costos de instalación básica no consideran 
                                                trabajos que no corresponden a la instalación del equipo como: desmontajes de equipos usados, trabajos en puntos eléctricos, 
                                                puntos de drenaje, trabajos de albañilería u otros requerimientos adicionales a la conexión básica del equipo, 
                                                equipos especiales para trabajos en altura o sobre estructuras fragiles como cubiertas, 
                                                tejas u otras estructuras no firmes para el paso de personal o apoyo de equipos. 
                                                En caso de requerir más material o tiempo técnico el presupuesto varía según requerimiento, 
                                                no se considera ningún trabajo no estipulado en la proforma. 
                                                Todo adicional deberá ser aprobado por el cliente previo a la ejecución de los trabajos, 
                                                se recomienda visita técnica previo a la ejecución de los trabajos en caso de que el cliente no este seguro 
                                                de la capacidad del aire requerido o si su instalación entra bajo los criterios de instalación básica.
                                            </p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class = "margin-div">
                            <table class = "border">
                                <thead class = "thead" style = "text-align : left !important">
                                    <tr>
                                        <td> La Oferta No Incluye :</td>
                                    </tr>
                                </thead>
                                <tbody class = "tbody" style = "text-align : left !important">
                                    <tr>
                                        <td>
                                            <p>
                                                • Transporte fuera de la ciudad de Guayaquil.<br>
                                                • Instalaciones eléctricas para energizar equipos o sus partes, breakers, puntos para termostatos, movimiento de puntos, canaletas o cualquier otro requerimiento eléctrico necesario para el correcto funcionamiento de los equipos.<br>
                                                • Puntos para defogue de agua, equipos para traslado de agua, y cualquier material correspondiente para flujo de agua proveniente de los equipos.<br>
                                                •  Apertura, sellado e impermeabilización de boquetes en losa, tumbado o paredes para paso de ductos, tubería de cobre u otros requerimiento para los trabajos de instalación.<br>
                                                •  Trabajos de albañilería, pintura u otra adecuación civil.<br>
                                                •  Cualquier otro rubro (equipos, materiales o trabajos) no estipulado en la oferta.<br>
                                            </p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class = "margin-div">
                            <table class = "border">
                                <thead class = "thead" style = "text-align : left !important">
                                    <tr>
                                        <td> Forma de Pago :</td>
                                    </tr>
                                </thead>
                                <tbody class = "tbody" style = "text-align : left !important">
                                    <tr>
                                        <td>
                                            <p>
                                                CONTADO / CRÉDITO / TARJETA DE CRÉDITO<br>
                                                <br>
                                                TIEMPO DE PAGO:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;60 Días<br>
                                                # DE ABONOS:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2 
                                                <br>
                                                • Efectivo, Cheque o Transferencia Bancaria.<br>
                                                Con cheque previa presentación de Certificado Bancario. Los cheques deben ser girados a favor de CEGASERVICES S.A.<br>
                                                Datos para Depósito o Transferencia Bancaria: Banco Pichincha | Cta. Cte. N.° 3427635104 |CEGASERVICES S.A. | RUC 0992273399001<br>
                                                <br>
                                                • Tarjeta de crédito DINERS, MASTERCARD, VISA<br>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;Pago corriente o hasta 6 meses sin intereses<br>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pagos hasta 12 meses con intereses disponible (interés cargado por la tarjeta no incluido en precio)<br>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;Consultar asesor de ventas para detalles de pago con tarjeta
                                            </p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class = "margin-div">
                            <table class = "border">
                                <thead class = "thead" style = "text-align : left !important">
                                    <tr>
                                        <td> Validez de la Oferta :</td>
                                    </tr>
                                </thead>
                                <tbody class = "tbody" style = "text-align : left !important">
                                    <tr>
                                        <td>
                                            <p>
                                                • 15 días o hasta agotar stock de equipos o materiales..<br>
                                                • Reserva de equipos y precios disponibles con entrega de anticipos (consultar con asesor de ventas).
                                            </p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class = "margin-div">
                            <table class = "border">
                                <thead class = "thead" style = "text-align : left !important">
                                    <tr>
                                        <td> Duracion de Trabajos (Tiempo Referencial) :</td>
                                    </tr>
                                </thead>
                                <tbody class = "tbody" style = "text-align : left !important">
                                    <tr>
                                        <td>
                                            <p>
                                                • Instalaciones Básicas: de 3 a 4 horas por equipo.<br>
                                                • Mantenimientos: de 45 min  a 2 horas por equipo.<br>
                                                • Trabajos Correctivos: Menores: 1 hora por equipo; Mayores: 4 a 6 horas por equipo.<br>
                                                • Tiempos de ejecución reales pueden variar según las condiciones específicas de cada trabajo.<br>
                                            </p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class = "margin-div">
                            <table class = "border">
                                <thead class = "thead" style = "text-align : left !important">
                                    <tr>
                                        <td> Garantía :</td>
                                    </tr>
                                </thead>
                                <tbody class = "tbody" style = "text-align : left !important">
                                    <tr>
                                        <td>
                                            <p>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Equipos ECOX:</b><br>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;partes y piezas 1 año, compresor 6 años por fallas de fabricación<br>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Mano de obra: </b><br>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>Mantenimientos: Para equipos de requerimiento de mantenimiento mensual o bi-mensual garantía de 15 días,</u><br>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;para trimestrales o más se otorga garantía de 1 mes en los trabajos realizados. No incluye fallas por desgaste o<br>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;uso de las partes / equipo que pueden presentarse después de un mantenimiento.<br>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>Instalación y correctivos:</u> 3 meses de garantía para fallas correspondientes a la ejecución técnica del trabajo<br>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;realizado.<br>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Repuestos: Garantía por fallas de fabricación de 3 meses para compresores, turbinas y motores eléctricos y de</b><br>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1 mes para partes y componentes eléctricos menores.<br>
                                                Para todas las áreas, la garantía no cubre fallas por problemas eléctricos, variaciones de voltaje, mala instalación y manipulación<br>
                                                (si no realizado por personal calificado por CEGASERVICES S.A.), falta de mantenimiento, mal manejo o uso que <u>incluye el no<br>
                                                seguir las recomendaciones indicadas por le empresa</u>
                                            </p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class = "margin-div">
                            <table>
                                <thead style = "text-align : left !important">
                                    <tr>
                                        <td><b><i>Elaborado por :</i></b></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>  
        </div>  
    </div>
</div>