<!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>COTIZACIONES</h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">Inicio</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="#">Lista de cotizaciones</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Registro de cotizaciones</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Begin: life time stats -->
                            <div class="portlet light portlet-fit portlet-datatable bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase"> HACER COTIZACIÓN</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs nav-tabs-lg">
                                            <li class="active">
                                                <a href="#tab_1" data-toggle="tab"> Detalles </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_1">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12">
                                                        <div class="portlet yellow-crusta box">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-cogs"></i>Detalle de la Cotización </div>
                                                                <div class="actions">
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body">
                                                                <div class="row static-info">
                                                                    <div class="col-md-5 name"> Cotización #: </div>
                                                                    <div class="col-md-7 value"> 12313232
                                                                        <span class="label label-info label-sm"> Email confirmation was sent </span>
                                                                    </div>
                                                                </div>
                                                                <div class="row static-info">
                                                                    <div class="col-md-5 name"> Fecha y hora: </div>
                                                                    <div class="col-md-7 value"> Dec 27, 2013 7:16:25 PM </div>
                                                                </div>
                                                                <div class="row static-info">
                                                                    <div class="col-md-5 name"> Estado de la Cotización: </div>
                                                                    <div class="col-md-7 value">
                                                                        <span class="label label-success"> Cerrado </span>
                                                                    </div>
                                                                </div>
                                                                <div class="row static-info">
                                                                    <div class="col-md-5 name"> Gran Total: </div>
                                                                    <div class="col-md-7 value"> $175.25 </div>
                                                                </div>
                                                                <div class="row static-info">
                                                                    <div class="col-md-5 name"> Forma de Pago: </div>
                                                                    <div class="col-md-7 value"> Credit Card </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12">
                                                        <div class="portlet blue-hoki box">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-cogs"></i>Información del Cliente </div>
                                                                <div class="actions">
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body">
                                                                <div class="row static-info">
                                                                    <div class="col-md-5 name"> Tipo de cliente: </div>
                                                                    <div class="col-md-7 value"> Jhon Doe </div>
                                                                </div>
                                                                <div class="row static-info">
                                                                    <div class="col-md-5 name"> Nombre del cliente: </div>
                                                                    <div class="col-md-7 value"> jhon@doe.com </div>
                                                                </div>
                                                                <div class="row static-info">
                                                                    <div class="col-md-5 name"> Razón social: </div>
                                                                    <div class="col-md-7 value"> New York </div>
                                                                </div>
                                                                <div class="row static-info">
                                                                    <div class="col-md-5 name"> RUC: </div>
                                                                    <div class="col-md-7 value"> 12234389 </div>
                                                                </div>
                                                                <div class="row static-info">
                                                                    <div class="col-md-5 name"> Email: </div>
                                                                    <div class="col-md-7 value"> 12234389 </div>
                                                                </div>
                                                                <div class="row static-info">
                                                                    <div class="col-md-5 name"> Telefono: </div>
                                                                    <div class="col-md-7 value"> 12234389 </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12">
                                                        <div class="portlet green-meadow box">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-cogs"></i>Dirección de Facturación </div>
                                                                <div class="actions">
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body">
                                                                <div class="row static-info">
                                                                    <div class="col-md-12 value"> Jhon Done
                                                                        <br> #24 Park Avenue Str
                                                                        <br> New York
                                                                        <br> Connecticut, 23456 New York
                                                                        <br> United States
                                                                        <br> T: 123123232
                                                                        <br> F: 231231232
                                                                        <br> </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12">
                                                        <div class="portlet red-sunglo box">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-cogs"></i>Dirección del Trabajo </div>
                                                                <div class="actions">
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body">
                                                                <div class="row static-info">
                                                                    <div class="col-md-12 value"> Jhon Done
                                                                        <br> #24 Park Avenue Str
                                                                        <br> New York
                                                                        <br> Connecticut, 23456 New York
                                                                        <br> United States
                                                                        <br> T: 123123232
                                                                        <br> F: 231231232
                                                                        <br> </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12">
                                                        <div class="portlet grey-cascade box">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-cogs"></i>Lista de Productos y Servicios </div>
                                                                <div class="actions">
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body">
                                                                <div class="table-responsive">
                                                                    <table class="table table-hover table-bordered table-striped">
                                                                        <thead>
                                                                            <tr>
                                                                                <th width="20%"> Tipo de Trabajo </th>
                                                                                <th width="45%"> Descripción </th>
                                                                                <th width="5%"> Cantidad </th>
                                                                                <th width="10%"> Precio </th>
                                                                                <th width="10%"> Descuento </th>
                                                                                <th width="10%"> Total </th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                    <a href="javascript:;"> Product 1 </a>
                                                                                </td>
                                                                                <td>
                                                                                    <span class="label label-sm label-success"> Available </td>
                                                                                <td> 345.50$ </td>
                                                                                <td> 345.50$ </td>
                                                                                <td> 2 </td>
                                                                                <td> 2.00$ </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <a href="javascript:;"> Product 1 </a>
                                                                                </td>
                                                                                <td>
                                                                                    <span class="label label-sm label-success"> Available </td>
                                                                                <td> 345.50$ </td>
                                                                                <td> 345.50$ </td>
                                                                                <td> 2 </td>
                                                                                <td> 2.00$ </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <a href="javascript:;"> Product 1 </a>
                                                                                </td>
                                                                                <td>
                                                                                    <span class="label label-sm label-success"> Available </td>
                                                                                <td> 345.50$ </td>
                                                                                <td> 345.50$ </td>
                                                                                <td> 2 </td>
                                                                                <td> 2.00$ </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <a href="javascript:;"> Product 1 </a>
                                                                                </td>
                                                                                <td>
                                                                                    <span class="label label-sm label-success"> Available </td>
                                                                                <td> 345.50$ </td>
                                                                                <td> 345.50$ </td>
                                                                                <td> 2 </td>
                                                                                <td> 2.00$ </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6"> </div>
                                                    <div class="col-md-6">
                                                        <div class="well">
                                                            <div class="row static-info align-reverse">
                                                                <div class="col-md-8 name"> Sub Total: </div>
                                                                <div class="col-md-3 value"> $1,124.50 </div>
                                                            </div>
                                                            <div class="row static-info align-reverse">
                                                                <div class="col-md-8 name"> IVA: </div>
                                                                <div class="col-md-3 value"> $40.50 </div>
                                                            </div>
                                                            <div class="row static-info align-reverse">
                                                                <div class="col-md-8 name"> Total a pagar: </div>
                                                                <div class="col-md-3 value"> $1,260.00 </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End: life time stats -->
                        </div>
                    </div>
                    <!-- BEGIN PAGE BASE CONTENT -->

                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->