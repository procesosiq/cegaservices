        <link href="http://cdn.procesos-iq.com/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="http://cdn.procesos-iq.com/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper" ng-app="app" ng-controller="supervisores">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content" ng-init="init()">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Usuarios</h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="/">Inicio</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Configuración</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Usuarios</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <div class="modal fade" id="modal-supervisor">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">{{ (usuario.id > 0 && usuario.id != undefined) ? "Editar" : "Agregar"  }} Usuario</h4>
                                </div>
                                <div class="modal-body">
                                    <form>
                                        <div class="tabbable tabbable-custom">
                                            <div class="tab-content">
                                                <div class="tab-pane padding-t-sm active" id="tab_driver">
                                                    <div class="row margin-b-sm">
                                                        <label class="col-sm-5">Nombre</label>
                                                        <div class="col-sm-7">
                                                            <input type="hidden" id="id" name="id" ng-model="usuario.id"/>
                                                            <input type="text" id="nombre" name="nombre" ng-model="usuario.nombre" class="form-control validate[required]"/>
                                                        </div>
                                                    </div>
                                                    <div class="row margin-b-sm">
                                                        <label class="col-sm-5">Email</label>
                                                        <div class="col-sm-7">
                                                            <input type="text" id="email" name="email" ng-model="usuario.email" class="form-control validate[required]"/>
                                                        </div>
                                                    </div>
                                                    <div class="row margin-b-sm">
                                                        <label class="col-sm-5">Móvil</label>
                                                        <div class="col-sm-7">
                                                            <input type="text" id="movil" name="movil" ng-model="usuario.movil" class="form-control validate[required]"/>
                                                        </div>
                                                    </div>
                                                    <div class="row margin-b-sm">
                                                        <label class="col-sm-5">Usuario</label>
                                                        <div class="col-sm-7">
                                                            <input type="text" id="usuario" name="usuario" ng-model="usuario.usuario" class="form-control validate[required]"/>
                                                        </div>
                                                    </div>
                                                    <div class="row margin-b-sm">
                                                        <label class="col-sm-5">Contraseña</label>
                                                        <div class="col-sm-7">
                                                            <input type="text" id="pass" name="pass" ng-model="usuario.pass" class="form-control validate[required]"/>
                                                        </div>
                                                    </div>
                                                    <div class="row margin-b-sm">
                                                        <label class="col-sm-5">Tipo de Usuario</label>
                                                        <div class="col-sm-7">
                                                            <select id="tipo_usuario" class="form-control" ng-change="changeTipo()" ng-model="usuario.tipo_usuario">
                                                                <option value=""><< Seleccionar >></option>
                                                                <option value="1">Administrador</option>
                                                                <option value="4">Operativo</option>
                                                                <option value="5">Supervisor</option>
                                                                <option value="3">Facturador</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <br><br>
                                                    <div class="row margin-b-sm">
                                                        <label class="col-sm-5">Vincular a : </label>
                                                        <div class="col-sm-7">
                                                            <select id="supervisor" ng-model="usuario.supervisor" class="form-control">
                                                                <option value=""></option>
                                                                <option value="{{ supervisor.id }}" ng-repeat="supervisor in supervisores">{{ supervisor.nombre }}</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" ng-click="cancel()" data-dismiss="modal">Cancelar</button>
                                    <button type="button" class="btn btn-primary" ng-click="editarUsuario()">Agregar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Begin: life time stats -->
                            <div class="portlet light portlet-fit portlet-datatable bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">LISTADO DE USUARIOS</span>
                                    </div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-container">
                                        <div class="table-actions-wrapper">
                                            <a href="/supervisor"><button class="btn btn-sm green">Agregar Nuevo Usuario</button></a>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                                            <thead>
                                                <tr role="row" class="heading">
                                                    <th width="5%"> # </th>
                                                    <th width="15%"> Nombre </th>
                                                    <th width="15%"> Email </th>
                                                    <th width="10%"> Móvil </th>
                                                    <th width="15%"> Usuario </th>
                                                    <th width="15%"> Contraseña </th>
                                                    <th width="15%"> Tipo de Usuario </th>
                                                    <th width="10%"> Status </th>
                                                    <th width="15%"> Acciones </th>
                                                </tr>
                                                <tr role="row" class="filter">
                                                    <td><input type="text" class="form-control form-filter input-sm" name="order_id"> </td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="order_name"> </td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="order_email"> </td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="order_movil"> </td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="order_usuario"> </td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="order_pass"> </td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="order_tipo"> </td>
                                                    <td>
                                                        <select name="order_status" class="form-control form-filter input-sm">
                                                            <option value="">TODOS</option>
                                                            <option value="1">ACTIVO</option>
                                                            <option value="2">INACTIVO</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <div class="margin-bottom-5">
                                                            <button class="btn btn-sm green btn-outline filter-submit margin-bottom">
                                                                <i class="fa fa-search"></i></button>
                                                            <button class="btn btn-sm red btn-outline filter-cancel">
                                                                <i class="fa fa-times"></i></button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody> </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- End: life time stats -->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->