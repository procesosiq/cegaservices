<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="http://cdn.procesos-iq.com/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
<link href="http://cdn.procesos-iq.com/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<link href="http://cdn.procesos-iq.com/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
<link href="http://cdn.procesos-iq.com/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<link href="http://cdn.procesos-iq.com/global/plugins/clockface/css/clockface.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="http://cdn.procesos-iq.com/global/plugins/moment.min.js" type="text/javascript"></script>
<script src="http://cdn.procesos-iq.com/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
<script src="http://cdn.procesos-iq.com/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="http://cdn.procesos-iq.com/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
<script src="http://cdn.procesos-iq.com/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="http://cdn.procesos-iq.com/global/plugins/clockface/js/clockface.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<script src="../assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>