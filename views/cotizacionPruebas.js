
// Prevent the backspace key from navigating back.
$(document).unbind('keydown').bind('keydown', function (event) {
    var doPrevent = false;
    if (event.keyCode === 8) {
        var d = event.srcElement || event.target;
        if ((d.tagName.toUpperCase() === 'INPUT' &&
	             (
	                 d.type.toUpperCase() === 'TEXT' ||
	                 d.type.toUpperCase() === 'PASSWORD' ||
	                 d.type.toUpperCase() === 'FILE' ||
	                 d.type.toUpperCase() === 'SEARCH' ||
	                 d.type.toUpperCase() === 'EMAIL' ||
	                 d.type.toUpperCase() === 'NUMBER' ||
	                 d.type.toUpperCase() === 'DATE'
	             )
             )
        	|| d.tagName.toUpperCase() === 'TEXTAREA' || d.contentEditable == "true") {
            doPrevent = d.readOnly || d.disabled;
        }
        else {
            doPrevent = true;
        }
    }

    if (doPrevent) {
        event.preventDefault();
    }
});

var validar = function(){

		if($("#tipo_trabajo").val()==3 || $("#tipo_trabajo").val()==5 || $("#tipo_trabajo").val()==6){
			$("#des").removeAttr('disabled');
			$("#des").removeAttr('readonly');
			$("#count").attr('disabled', 'disabled');
			$("#count").attr('readonly', 'readonly');
			$("#count").val(0);
			$("#price").val(0.00);
			$("#total").val(0.00);
			$("#price").attr('disabled', 'disabled');
			$("#price").attr('readonly', 'readonly');
		}
		else if ($("#tipo_trabajo").val()!=3 && $("#tipo_trabajo").val()!=5 && $("#tipo_trabajo").val()!=6){
			$("#des").removeAttr('disabled');
			$("#count").removeAttr('disabled');
			$("#price").removeAttr('disabled');
			$("#des").removeAttr('readonly');
			$("#count").removeAttr('readonly');
			$("#price").removeAttr('readonly');
		}

    }
// general settings
// $.fn.modalmanager.defaults.spinner =
// 	'<div class="loading-spinner" style="width: 200px; margin-left: -100px;">' +
// 		'<div class="progress progress-striped active">' +
// 			'<div class="progress-bar" style="width: 100%;"></div>' +
// 		'</div>' +
// 	'</div>';
// $.fn.modal.defaults.spinner = $.fn.modalmanager.defaults.spinner;


// $.fn.modalmanager.defaults.resize = true;

//ajax demo:
var $modal = $('#ajax-modal');
var $modal2 = $('#ajax-modal2');
var $modal3 = $('#ajax-modal3');

// $('#ajax-demo').on('click', function(){
// 	// create the backdrop and wait for next modal to be triggered
// 	$('body').modalmanager('loading');
// 	var el = $(this);
// 	$modal.load(el.attr('data-url'), '', function(){
// 		$modal.modal();
// 	});

// 	$modal.on('click', '.update', function(){
// 		$modal.modal('loading');
// 		setTimeout(function(){
// 			$modal
// 				.modal('loading')
// 				.find('.modal-body')
// 				.prepend('<div class="alert alert-info fade in">' +
// 					'Updated!<button type="button" class="close" data-dismiss="alert">&times;</button>' +
// 				'</div>');
// 		}, 1000);
// 	});
// });

app.directive('stringToNumber', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(value) {
        return '' + value;
      });
      ngModel.$formatters.push(function(value) {
        return parseFloat(value, 10);
      });
    }
  };
});


app.controller('cotizacionPruebas', ['$scope','$http','$interval','client','$controller', function($scope,$http,$interval,client, $controller){

	// get id (de la cotizacion) de la URL
	var cotizacionID = location.search.split('id=')[1];

	$scope.sucursales = [];
	$scope.clientesNombre = [];
	$scope.cotizacion = {
		id : 0,
		id_cliente : 0,
		id_sucursal : 0,
		ruc : "",
		tcliente : "",
		subtotal : "",
		sumsubtotal : "",
		folio : "000000",
		status : "Pendiente",
		forma_pago : "",
		_iva : 0.14,
		iva : "",
		total : 0,
		active : 0,
		newClient : 0,
		details : [],
		getTotals : function(){
			$scope.cotizacion.subtotal = 0;
            $scope.cotizacion.descuentoo = 0;
			for (var i = $scope.cotizacion.details.length - 1; i >= 0; i--) {
				if($scope.cotizacion.details[i].count > 0 && $scope.cotizacion.details[i].price > 0){
					//$scope.cotizacion.subtotal += ($scope.cotizacion.details[i].count * $scope.cotizacion.details[i].price);
						var total = $scope.cotizacion.details[i].count * $scope.cotizacion.details[i].price;
						var desc = $scope.cotizacion.details[i].desc / 100;
						desc = total*desc;
						//total = total - desc;
						console.log(total);
						$scope.cotizacion.subtotal += total;
                        $scope.cotizacion.descuentoo += desc;
				}
			}
			// console.log($scope.cotizacion.discount)
			var subtotal = $scope.cotizacion.subtotal;
            var desctot = $scope.cotizacion.descuentoo;


			$scope.cotizacion.sumsubtotal = subtotal;
            $scope.cotizacion.sumDescu = desctot;
            console.log("es tuyo noe");
			$scope.cotizacion.iva = ($scope.cotizacion._iva * (subtotal-desctot));
			$scope.cotizacion.total = ((subtotal-desctot) + $scope.cotizacion.iva);

		}
	};
	$scope.options = {
		tipo_cliente : [],
		tipo_trabajo : [],
		forma_pago : ["Contado" , "Credito" , "Tarjeta" ],
		abonos : ["1" , "2" , "3" , "4", "5"],
		tiempo : ["30 días" , "60 días" , "90 días" , "120 días"],
	}

	$scope.clearClient = function(){
		alert($scope.cotizacion.ruc);
		$scope.cotizacion.active = 1;

		$(".formClass").removeAttr('disabled');
		$(".formClass").removeAttr('readonly');
		$("#ruc").removeAttr('disabled');
		$("#ruc").removeAttr('readonly');
		$("#nombre_cliente").removeAttr('disabled');
		$("#nombre_cliente").removeAttr('readonly');
		$("#filtro").attr('disabled', 'disabled');
		$("#filtro").attr('readonly', 'readonly');
		$("#saveCliente").css('display', '');
		$("#cancelCliente").css('display', '');
		$("#nuevoCliente").css('display', 'none');
	}

	$scope.cancel = function(){
		$scope.filtro();
		$("#filtro").removeAttr('disabled');
		$("#filtro").removeAttr('readonly');
		$(".formClass").attr('disabled' , 'disabled');
		$(".formClass").attr('readonly' , 'readonly');
		$("#saveCliente").css('display', 'none');
		$("#cancelCliente").css('display', 'none');
	}

	$scope.totalText = function(){
		if(parseInt($("#count").val()) > 0 && parseInt($("#price").val()) > 0 && parseInt($("#desc").val()) >= 0){
			var total = $("#count").val() * $("#price").val();
			var desc = $("#desc").val() / 100;
			desc = total*desc;
			total = total - desc;
			$("#total").val(total);
		}
	}

	$scope.getTotal = function(count, price, desc){
			var total = count * price;
			var desc = desc / 100;
			desc = total*desc;
			total = total - desc;
			return total;

	}

	$(".countRow").on("change" , function(){
		$scope.totalText();
	});

	$scope.copiaCotizacion = function(id){
		var data = {
				id : id
			}
		if(id > 1){
			if(confirm('¿Seguro que desea generar una copia de esta cotización?')){

				client.post("./controllers/index.php?accion=cotizacionPruebas.copiaCotizacion" , function(r, b){
			b();
			console.log(r);
			location.href="http://cegaservices2.procesos-iq.com/cotizacionPruebas?id="+r;
		} , data);
			}
			}
	}

    $scope.saveCotizacion = function(){
		if($scope.cotizacion.id_cliente <= 0){
			alert("Favor de asignar cliente" , "COTIZACION");
			return false;
		}
		else if($scope.cotizacion.details.length <= 0){
			alert("Favor de ingresar al menos una accion" , "COTIZACION");
			return false;
		}else if($scope.cotizacion.forma_pago == ""){
			alert("Favor de ingresar algun metodo de pago" , "COTIZACION");
			return false;
		}else if($scope.cotizacion.abonos == ""){
			alert("Favor de ingresar algun tipo de abono" , "COTIZACION");
			return false;
		}else{
			console.log($scope.cotizacion.abonos);
			var data = {
				details : $scope.cotizacion.details,
				id_client : $scope.cotizacion.id_cliente,
				observaciones : $("#summernote_1").code(),
				observaciones_base : $("#summernote_2").code(),
				params : $scope.cotizacion
			}
			console.log($scope.cotizacion)
			if($scope.cotizacion.id > 1){
				data.id_cotizacion = $scope.cotizacion.id;
				client.post("./controllers/index.php?accion=cotizacionPruebas.edit" ,$scope.finally , data);
				location.href="http://cegaservices2.procesos-iq.com/cotizacionList";
			}
			else{
				client.post("./controllers/index.php?accion=cotizacionPruebas.save" ,$scope.finally , data);
				location.href="http://cegaservices2.procesos-iq.com/cotizacionList";
			}
		}
	}

	$scope.finally = function( r , b){
		b();
		// console.log(r);
		if(r && r.id){
			$scope.cotizacion.id = r.id;
			$scope.cotizacion.id_cliente = 0;
			$scope.cotizacion.folio = r.folio;
			$scope.cotizacion.fecha = r.fecha;
			$("input").attr("disabled" , "disabled");
			$("select").attr("disabled" , "disabled");
			$("textarea").attr("disabled" , "disabled");
			$(".detailsDelete").css("display" , "none");
			$("#headTable").css("display" , "none");
			$(".btnSave").css("display" , "none");
			var htmlObservaciones = $("#summernote_1").code();
			$(".obser").empty().html(htmlObservaciones);
			var texto = $("#summernote_2").code();
			$(".textfire").empty().html(texto);
			// $("#summernote_1").summernote('destroy');
			// $("#summernote_2").summernote('destroy');
		}
	}

	$scope.save = function(){
		if($scope.cotizacion.id_cliente > 0){
			alert("Ya existe un cliente seleccionado" , "COTIZACION");
			return false;
		}
		else if($scope.cotizacion.ruc==''){
            alert("Favor de ingresar el RUC o Cédula de Identidad");
            return false;
        }
        else if($scope.cotizacion.ruc.length <= 9){
            alert("Favor de ingresar el RUC o Cédula de Identidad");
            return false;
        }
        else if($scope.cotizacion.ruc.length > 13){
            alert("Favor de ingresar el RUC o Cédula de Identidad");
            return false;
        }
		else if($scope.cotizacion.nombre_cliente == ""){
			alert("Favor de asignar nombre del cliente" , "COTIZACION");
			return false;
		}
		else if($scope.cotizacion.razon_social == ""){
			alert("Favor de ingresar razon social del cliente" , "COTIZACION");
			return false;
		}
		else if($scope.cotizacion.correo_contacto == ""){
			alert("Favor de ingresar email del cliente" , "COTIZACION");
			return false;
		}
		else if($scope.cotizacion.telefono == ""){
			alert("Favor de ingresar telefono del cliente" , "COTIZACION");
			return false;
		}else{
			var data = {
				nombre_cliente : $scope.cotizacion.nombre_cliente,
				razon_social : $scope.cotizacion.razon_social,
				email : $scope.cotizacion.correo_contacto,
				telefono : $scope.cotizacion.telefono,
				tipo_cliente : $scope.cotizacion.tcliente,
				ruc : $scope.cotizacion.ruc,
			}

			client.post("./controllers/index.php?accion=cotizacion.saveCliente" ,$scope.registredClient , data);
		}
	}

	$scope.registredClient = function(r , b){
		b();
		if(r && r.id_tipcli){
			$scope.cotizacion.ruc = r.ruc;
			$scope.cancel();
			$scope.getClient2();
		}
	}

	$scope.editRow = function(data , index){
		if(data){
			$scope.cotizacion.details[index].edit = 1;
		}
	}

	$scope.saveRow = function(data , index){
		if(data){
			$scope.cotizacion.details[index].edit = 0;
			$scope.cotizacion.getTotals();
		}
    	console.log( angular.toJson( $scope.cotizacion.details ) );
	}

 	$scope.addRow = function(){
    	var data = {
    		tipo_trabajo : $("#tipo_trabajo").val(),
    		des_trabajo : $("#tipo_trabajo option:selected").text(),
    		class_trabajo : $("#tipo_trabajo option:selected").prop("class"),
    		des : $("#des").val(),
    		count : $("#count").val(),
    		desc : $("#desc").val(),
    		uni : $("#uni").val(),
    		price : $("#price").val(),
    		edit : 0
    	}

    	if(data){
    		//console.log(data);
	    	if(data.count > 0 && data.price > 0 || (data.tipo_trabajo == 3 || data.tipo_trabajo == 5 || data.tipo_trabajo == 6)){
	    		// console.log($scope.formData.areas);
	    		$scope.cotizacion.details.push(data);
	  			$scope.cotizacion.getTotals();
	  			$scope.clearRow();
			}else if ($scope.cotizacion.id_cliente == 0){
				alert("Debe seleccionar primero un cliente.");
	    		if(data.price < 0) $("#price").val(1);
	    		if(data.count < 0) $("#count").val(1);
			}else{
	    		alert("No puede ingresar numeros negativos");
	    		if(data.price < 0) $("#price").val(1);
	    		if(data.count < 0) $("#count").val(1);
	    	}
    	}
    	console.log( angular.toJson( $scope.cotizacion.details ) );
    }

	$scope.validRevision = function(){

		if(($("#tipo_trabajo").val()==3 || $("#tipo_trabajo").val()==5 || $("#tipo_trabajo").val()==6) && $scope.cotizacion.id_cliente > 0){
			$("#des").removeAttr('disabled');
			$("#des").removeAttr('readonly');
			$("#count").attr('disabled', 'disabled');
			$("#count").attr('readonly', 'readonly');
			$("#count").val(0);
			$("#price").attr('disabled', 'disabled');
			$("#price").attr('readonly', 'readonly');
		}
		else if (($("#tipo_trabajo").val()!=3 && $("#tipo_trabajo").val()!=5 && $("#tipo_trabajo").val()!=6) && $scope.cotizacion.id_cliente > 0){
			$("#des").removeAttr('disabled');
			$("#count").removeAttr('disabled');
			$("#price").removeAttr('disabled');
			$("#des").removeAttr('readonly');
			$("#count").removeAttr('readonly');
			$("#price").removeAttr('readonly');
		}

    }

    $scope.clearRow = function(){
		$("#des").val('');
		$("#count").val('');
		$("#desc").val('');
		$("#uni").val('');
		$("#price").val('');
		$("#total").val('');
    }

    $scope.dropRow = function(data , index){
    	if(confirm("Esta seguro de Eliminar el registro?")){
	    	var data = data || {};
	    	if(data.tipo_trabajo){
	    		try{
	    			delete $scope.cotizacion.details[index];
	    			$scope.cotizacion.details.splice(index , 1);
	    		}catch(e){
	    			throw Error("Error al Elminar");
	    		}
	    		$scope.cotizacion.getTotals();
	    	}
    	}
    }

	$scope.opciones = {
    availableOptions: [
      {id: '1', name: 'RUC o C.I'},
      {id: '2', name: 'NOMBRE DEL CLIENTE'}
    ],
    selectedOption: {id: '1', name: 'RUC o C.I'} //This sets the default value of the select in the ui
    };

	$scope.getClient = function(keyEvent){
		if (keyEvent.which === 13){
		if($scope.cotizacion.ruc.length >= 9){
			var data = {
				ruc : $scope.cotizacion.ruc
			}
			client.post("./controllers/index.php?accion=cotizacion.GetCliente" ,$scope.printClient , data);
		}
		}
	};

	$scope.getClient2 = function(){
		if($scope.cotizacion.ruc.length > 9){
			var data = {
				ruc : $scope.cotizacion.ruc
			}
			client.post("./controllers/index.php?accion=cotizacion.GetCliente" ,$scope.printClient , data);
		}

	};

	$scope.getClientNombre = function(keyEvent){
		if (keyEvent.which === 13){
		var data = {
				nombre_cliente : $scope.cotizacion.nombre_cliente
			}
			client.post("./controllers/index.php?accion=cotizacion.GetClienteNombre" ,$scope.printClient , data);

		}
	};

	// $(".tipo_trabajo").on("change" , function(){
	// 	var className = $(this).find("option:selected").attr("class");
	// 	$(this).removeClass();
	// 	$(this).addClass(className + " form-control");
	// })

	$(document).on("change" , '.tipo_trabajo_details' , function(){
		var className = $(this).find("option:selected").attr("class");
		$(this).removeClass();
		$(this).addClass(className + " form-control tipo_trabajo_details");
	});

	$scope.getDes = function(index){
		var select = $("#tipo_trabajo"+index);
		console.log(index);
		console.log(select);
    	console.log($scope.cotizacion.details[index])
		$scope.cotizacion.details[index].des_trabajo = $("#tipo_trabajo"+index+" option:selected").text();
    	$scope.cotizacion.details[index].class_trabajo = $("#tipo_trabajo"+index+" option:selected").prop("class");
    	console.log($scope.cotizacion.details[index])
	}

	$scope.init = function(){
		$("#nuevoCliente").css('display', 'none');
		$("#saveCliente").css('display', 'none');
		$("#cancelCliente").css('display', 'none');
		$('#summernote_1').summernote({height: 300});
		$('#summernote_2').summernote({height: 300});
		$scope.cotizacion.ruc = '';
		client.post("./controllers/index.php?accion=cotizacion" ,$scope.print , {});
	}

	$scope.filtro = function() {
		var idfiltro =  parseInt($scope.opciones.selectedOption.id);
        if(idfiltro==1){
			$("#ruc").removeAttr('disabled');
			$("#ruc").removeAttr('readonly');

			$scope.cotizacion.tcliente = "";
			$scope.cotizacion.nombre_cliente = "";
			$scope.cotizacion.razon_social = "";
			$scope.cotizacion.correo_contacto = "";
			$scope.cotizacion.telefono = "";
			$("#nombre_cliente").attr('disabled', 'disabled');
			$("#nombre_cliente").attr('readonly', 'readonly');
		}
		else if(idfiltro==2){
			$("#nombre_cliente").removeAttr('disabled');
			$("#nombre_cliente").removeAttr('readonly');
			$scope.cotizacion.ruc = "";
			$scope.cotizacion.tcliente = "";
			$scope.cotizacion.razon_social = "";
			$scope.cotizacion.correo_contacto = "";
			$scope.cotizacion.telefono = "";
			$("#ruc").attr('disabled', 'disabled');
			$("#ruc").attr('readonly', 'readonly');
		}
      };

	$scope.printClient = function(r , b){
		b();
		//console.log(r.data);
		if(r && r.data){
			if(r.ti == 2){
				if(r.data.length > 0){
				console.log(r.data);
				$scope.clientesNombre = r.data;
				$modal2.modal();
				}
				else{
			alert("Cliente no registrado" , "Cotizaciones" , "warning" , function(){
				$("#nuevoCliente").css('display', '');
				$scope.cotizacion.id_cliente = 0;
				$scope.cotizacion.id_cliente = "";
				$scope.cotizacion.id_sucursal = 0;
				$scope.cotizacion.id_sucursal = "";
				$scope.cotizacion.tcliente = "";
				$scope.cotizacion.razon_social = "";
				$scope.cotizacion.correo_contacto = "";
				$scope.cotizacion.telefono = "";
				$(".formClass").val('');
				$scope.cancel();
			});
		}
			}
			else{
			console.log(r.success)

			if(r.data.length == 1){
				var data = r.data[0];
				var nombre_cliente = (r.success == 300) ? "Sucursal "+data.nombre_contacto : data.nombre;
				var id_cliente = (r.success == 300) ? data.id_cliente : data.id;
				var id_sucursal = (r.success == 300) ? data.id_cliente : 0;
				$scope.cotizacion.id_cliente = id_cliente;
				$scope.cotizacion.id_sucursal = id_sucursal;
				$scope.cotizacion.ruc = data.ruc;
				$scope.cotizacion.tcliente = data.id_tipcli;
				$scope.cotizacion.nombre_cliente = nombre_cliente;
				$scope.cotizacion.razon_social = data.razon_social;
				//$scope.cotizacion.email = data.email;
				$scope.cotizacion.correo_contacto = data.correo_contacto;
				console.log(data.correo_contacto);
				$scope.cotizacion.telefono = data.telefono;
				$("#nuevoCliente").css('display', 'none');
				$("#saveCliente").css('display', 'none');
			}else{
				if(r.success==250){
					$scope.facturacion = r.data;
					$modal3.modal();
				}
			else{
				$scope.sucursales = r.data;
			$modal.modal();}
			}
			}



		}else{
			alert("Cliente no registrado" , "Cotizaciones" , "warning" , function(){
				$("#nuevoCliente").css('display', '');
				$scope.cotizacion.id_cliente = 0;
				$scope.cotizacion.id_cliente = "";
				$scope.cotizacion.id_sucursal = 0;
				$scope.cotizacion.id_sucursal = "";
				$scope.cotizacion.tcliente = "";
				$scope.cotizacion.razon_social = "";
				$scope.cotizacion.correo_contacto = "";
				$scope.cotizacion.telefono = "";
				$(".formClass").val('');
				$scope.cancel();
			});
		}
	}

	$scope.setClient = function(data , nombre_cliente, toggleModal){
		var tm = toggleModal || false;
		if(data){
			//console.log(data);
			$scope.cotizacion.id_sucursal = data.id_sucursal || data.id;
			$scope.cotizacion.id_cliente = data.id_cliente;
			$scope.cotizacion.tcliente = data.id_tipcli;
			$scope.cotizacion.nombre_cliente = nombre_cliente + data.nombre_contacto;
			$scope.cotizacion.razon_social = data.razon_social;
			//$scope.cotizacion.email = data.email;
			$scope.cotizacion.correo_contacto = data.correo_contacto;
			$scope.cotizacion.telefono = data.telefono;
			if(!toggleModal){
				$("#nuevoCliente").css('display', 'none');
				$("#saveCliente").css('display', 'none');
				$modal.modal('toggle');
			}
		}
	}

	$scope.setClientFact = function(data , nombre_cliente, toggleModal){
		var tm = toggleModal || false;
		if(data){
			console.log(data);
			$scope.cotizacion.id_cliente = data.id;
			$scope.cotizacion.ruc = data.ruc;
			$scope.cotizacion.id_raz = data.id_raz;
			$scope.cotizacion.tcliente = data.id_tipcli;
			$scope.cotizacion.nombre_cliente = data.nombre;
			$scope.cotizacion.razon_social = data.razon_social;
			//$scope.cotizacion.email = data.email;
			$scope.cotizacion.correo_contacto = data.correo_contacto;
			$scope.cotizacion.telefono = data.telefono;
			if(!toggleModal){
				$("#nuevoCliente").css('display', 'none');
				$("#saveCliente").css('display', 'none');
				$modal3.modal('toggle');
			}
		}
	}

	$scope.setClientNombre = function(data , nombre_cliente, toggleModal){
		var tm = toggleModal || false;
		var data = {
				ruc : data.ruc
			}
			client.post("./controllers/index.php?accion=cotizacion.GetCliente" ,$scope.printClient , data);
			if(!toggleModal){
				$("#nuevoCliente").css('display', 'none');
				$("#saveCliente").css('display', 'none');
				$modal2.modal('toggle');
			}

	}

	$scope.print = function(r , b){
		b();
		if(r){
			$scope.options.tipo_cliente = r.tipo_cliente
			$scope.options.tipo_trabajo = r.tipo_trabajo
		}
	}

	$scope.sendMailConfirmation = function(){
		var data = {
			id_cotizacion: $scope.cotizacion.id,
			correo: $scope.cotizacion.correo_contacto
		}
		client.post("./controllers/index.php?accion=cotizacion.sendMailConfirmation" , function(d, r){
			console.log(d);
			console.log(r);
			r();
		} , data);
	}

	$scope.printCotizacion = function(){
		angular.element(".page-header.navbar.navbar-fixed-top").css({display:"none"});
		angular.element(".page-sidebar-wrapper").css({display:"none"});
		angular.element(".page-content").css({"margin-left":"0px"});
		$("input").attr("disabled" , "disabled");
		$("select").attr("disabled" , "disabled");
		$("textarea").attr("disabled" , "disabled");
		$(".detailsDelete").css("display" , "none");
		$("#headTable").css("display" , "none");
		$(".btnSave").css("display" , "none");
		var htmlObservaciones = $("#summernote_1").code();
		$(".obser").empty().html(htmlObservaciones);
		var texto = $("#summernote_2").code();
		$(".textfire").empty().html(texto);
		window.print();
	}


	$scope.getCotizacionData = function(){
		$scope.cotizacion.id = cotizacionID;
		var data = {
			id_cotizacion: cotizacionID
		}

		client.post("./controllers/index.php?accion=cotizacionPruebas.GetCotizacion" , function(r, b){
			b();
			console.log(r);

			$("#summernote_1").code(r.datos.observaciones);
			//if(r.status1==2)
				//location.href="http://cegaservices2.procesos-iq.com/404";
			$scope.cotizacion = r.datos;
			$scope.cotizacion._iva = 0.14;

			$scope.cotizacion.getTotals = function(){
				$scope.cotizacion.subtotal = 0;
                $scope.cotizacion.descuentoo = 0;
				for (var i = $scope.cotizacion.details.length - 1; i >= 0; i--) {
					if($scope.cotizacion.details[i].count > 0 && $scope.cotizacion.details[i].price > 0){

						var total = $scope.cotizacion.details[i].count * $scope.cotizacion.details[i].price;
						var desc = $scope.cotizacion.details[i].desc / 100;
						desc = total*desc;
						//total = total - desc;
						console.log(total);
						$scope.cotizacion.subtotal += total;
                        $scope.cotizacion.descuentoo += desc;
					}
				}

				var subtotal = $scope.cotizacion.subtotal;
                var desctot = $scope.cotizacion.descuentoo;

				console.log(subtotal);
				$scope.cotizacion.sumsubtotal = $scope.cotizacion.subtotal;
                $scope.cotizacion.sumDescu = desctot;

				$scope.cotizacion.iva = ($scope.cotizacion._iva * (subtotal-desctot));
				console.log( $scope.cotizacion._iva );
				$scope.cotizacion.total = ((subtotal-desctot) + $scope.cotizacion.iva);
				console.log('jsjsjajsa');
			}

			$scope.cotizacion.sumsubtotal = $scope.cotizacion.subtotal;
			$scope.cotizacion.details = r.datos.details;
            $scope.cotizacion.getTotals();

			if(r.datos.status == 0)
				$scope.cotizacion.status = 'Pendiente';
			else if (r.datos.status == 1)
				$scope.cotizacion.status = 'Por Aprobar';
			else if (r.datos.status == 2)
				$scope.cotizacion.status = 'Aprobado';

			if(angular.equals({}, r.sucursal)&&angular.equals({}, r.facturacion))
				{$scope.setClient(r.cliente, r.cliente.nombre, true);
			$scope.cotizacion.ruc= r.cliente.ruc;
			console.log('vacion');
			}
			else if(!angular.equals({}, r.sucursal)){
			$scope.setClient(r.sucursal, r.sucursal.nombre_cliente, true);
			$scope.cotizacion.ruc= r.cliente.ruc;
			console.log('sucursal');
			}
			else
				{
					console.log('factu');
					$scope.cotizacion.ruc = r.facturacion.ruc;
					$scope.cotizacion.tcliente = r.cliente.id_tipcli;
					$scope.cotizacion.nombre_cliente = r.cliente.nombre;
					$scope.cotizacion.razon_social = r.facturacion.rs	;
					$scope.cotizacion.telefono = r.facturacion.telefono	;
				}


			$scope.cotizacion.correo_contacto = r.contacto_email;
			$scope.cotizacion.fecha = r.fecha;

		}, data);
	}

    if(cotizacionID){
		$scope.getCotizacionData();
	}

}]);
