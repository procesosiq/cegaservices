    <style>
        .radio{
            padding: 0;
        }
    </style>
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content" ng-app="app" ng-controller="orderController" id="orderController">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head" ng-init="init()">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Listado de Órdenes de Trabajo
                                <small>Ultimas Órdenes registradas</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.php">Inicio</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="#">Órdenes</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Listado de Órdenes</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
<!--                     <div class="note note-info">
                        <p> A black page template with a minimal dependency assets to use as a base for any custom page you create </p>
                    </div> -->
                    <!-- END PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
<!--                             <div class="note note-danger">
                                <p> NOTE: The below datatable is not connected to a real database so the filter and sorting is just simulated for demo purposes only. </p>
                            </div> -->
                            <!-- Begin: life time stats -->
                            <!-- End: life time stats -->
                        </div>
                        <div class="col-md-12">
                            <div class="row" class="col-md-12">
                                <div class="portlet">
                                    <div class="portlet-body">
                                        <div class="tabbable-bordered">
                                            <ul class="nav nav-tabs">
                                                <li id="listado_2"  class="active">
                                                    <a href="#listado" data-toggle="tab"> Listado de Órdenes </a>
                                                </li>
                                                <li  id="registro_2">
                                                    <a href="#registro" data-toggle="tab"> Órdenes </a>
                                                </li>
                                                <li>
                                                    <a href="#calendar_tab" data-toggle="tab"> Calendario de Órdenes </a>
                                                </li>
                                            </ul>
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="listado">
                                                    <div class="portlet light portlet-fit portlet-datatable bordered">
                                                        <div class="portlet-title">
                                                            <div class="caption">
                                                                <i class="icon-settings font-dark"></i>
                                                                <span class="caption-subject font-dark sbold uppercase">Listado de Órdenes de Trabajo</span>
                                                            </div>
                                                            <!--<div class="actions">
                                                                <div class="btn-group btn-group-devided" data-toggle="buttons">
                                                                    <label class="btn btn-transparent grey-salsa btn-outline btn-circle btn-sm active">
                                                                        <input type="radio" name="options" class="toggle" id="option1">Actions</label>
                                                                    <label class="btn btn-transparent grey-salsa btn-outline btn-circle btn-sm">
                                                                        <input type="radio" name="options" class="toggle" id="option2">Settings</label>
                                                                </div>
                                                                <div class="btn-group">
                                                                    <a class="btn red btn-outline btn-circle" href="javascript:;" data-toggle="dropdown">
                                                                        <i class="fa fa-share"></i>
                                                                        <span class="hidden-xs"> Tools </span>
                                                                        <i class="fa fa-angle-down"></i>
                                                                    </a>
                                                                    <ul class="dropdown-menu pull-right">
                                                                        <li>
                                                                            <a href="javascript:;"> Export to Excel </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="javascript:;"> Export to CSV </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="javascript:;"> Export to XML </a>
                                                                        </li>
                                                                        <li class="divider"> </li>
                                                                        <li>
                                                                            <a href="javascript:;"> Print Invoices </a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>-->
                                                        </div>
                                                        <div class="portlet-body">
                                                            <div class="table-container">
                                                                <div class="table-actions-wrapper">
                                                                    <span> </span>
                                                                    <select class="table-group-action-input form-control input-inline input-small input-sm">
                                                                        <option value="">Select...</option>
                                                                        <option value="Cancel">Cancel</option>
                                                                        <option value="Cancel">Hold</option>
                                                                        <option value="Cancel">On Hold</option>
                                                                        <option value="Close">Close</option>
                                                                    </select>
                                                                    <button class="btn btn-sm green table-group-action-submit">
                                                                        <i class="fa fa-check"></i> Submit</button>
                                                                </div>
                                                                <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                                                                    <thead>
                                                                        <tr role="row" class="heading">
                                                                            <th width="2%">
                                                                                <input type="checkbox" class="group-checkable"> </th>
                                                                            <th width="5%"> N # </th>
                                                                            <th width="15%"> Fecha </th>
                                                                            <th width="200"> Cliente </th>
                                                                            <th width="10%"> Tipo de Cliente </th>
                                                                            <th width="10%"> Tipo de Trabajo </th>
                                                                            <th width="10%"> # de Equipo </th>
                                                                            <th width="10%"> Estado </th>
                                                                            <th width="10%"> Acciones </th>
                                                                        </tr>
                                                                        <tr role="row" class="filter">
                                                                            <td> </td>
                                                                            <td>
                                                                                <input type="text" class="form-control form-filter input-sm" name="order_id"> </td>
                                                                            <td>
                                                                                <div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
                                                                                    <input type="text" class="form-control form-filter input-sm" readonly name="order_date_from" placeholder="From">
                                                                                    <span class="input-group-btn">
                                                                                        <button class="btn btn-sm default" type="button">
                                                                                            <i class="fa fa-calendar"></i>
                                                                                        </button>
                                                                                    </span>
                                                                                </div>
                                                                                <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                                                                    <input type="text" class="form-control form-filter input-sm" readonly name="order_date_to" placeholder="To">
                                                                                    <span class="input-group-btn">
                                                                                        <button class="btn btn-sm default" type="button">
                                                                                            <i class="fa fa-calendar"></i>
                                                                                        </button>
                                                                                    </span>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" class="form-control form-filter input-sm" name="search_cliente"> </td>
                                                                            <td>
                                                                                <input type="text" class="form-control form-filter input-sm" name="search_tipo_cliente"> </td>
                                                                            <td>
                                                                                </td>
                                                                            <td>
                                                                                </td>
                                                                            <td>
                                                                                <select name="order_status" class="form-control form-filter input-sm">
                                                                                    <option value="">TODOS</option>
                                                                                    <option value="1">POR AGENDAR</option>
                                                                                    <option value="2">AGENDADA</option>
                                                                                    <option value="3">FINALIZADA</option>
                                                                                </select>
                                                                            </td>
                                                                            <td>
                                                                                <div class="margin-bottom-5">
                                                                <button class="btn btn-sm green btn-outline filter-submit margin-bottom">
                                                                    <i class="fa fa-search"></i></button>
                                                            <button class="btn btn-sm red btn-outline filter-cancel">
                                                                <i class="fa fa-times"></i></button>
                                                        </div>
                                                                            </td>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody> </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="calendar_tab">
                                                    <div class="portlet light portlet-fit bordered  bordered calendar">
                                                        <div class="portlet-title">
                                                            <div class="caption">
                                                                <i class=" icon-layers font-green"></i>
                                                                <span class="caption-subject font-green sbold uppercase">Calendar</span>
                                                            </div>
                                                        </div>
                                                        <div class="portlet-body">
                                                            <div class="row">
                                                                <div class="col-md-3 col-sm-3">
                                                                    <div id="external-events">
                                                                        <!-- <form class="inline-form">
                                                                            <input type="text" value="" class="form-control" placeholder="Event Title..." id="event_title" />
                                                                            <br/>
                                                                            <a href="javascript:;" id="event_add" class="btn green"> Add Event </a>
                                                                        </form> -->
																		<select onchange="printCal()" class="table-group-action-input form-control input-medium" id="gruposcalendario">
																			<option value="0">Ver todo</option>
																			<option value="1">MIGUEL SORIA</option>
																			<option value="2">CARLOS PINCAY</option>
																			<option value="3">WILSON BENAVIDES</option>
																			<option value="4">Contratistas</option>
																		</select>
                                                                        <h3 class="event-form-title margin-bottom-20">Ordenes sin agendar</h3>
                                                                        <hr/>
                                                                        <div id="event_box" class="margin-bottom-10">
                                                                            
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-9 col-sm-9">
                                                                    <div id="calendar_object" class="has-toolbar"> </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>  
                                                </div>  
                                                <div class="tab-pane" id="registro">
                                                    <form id="formularioOrden"  role="form" method="post" class="form-horizontal form-row-seperated">
                                                        <div class="portlet">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-shopping-cart"></i>Registro / Edicion Orden de Trabajo </div>
                                                                <div class="actions btn-set">
                                                                    <!-- <button type="button" name="back" class="btn btn-secondary-outline">
                                                                        <i class="fa fa-angle-left"></i> Regresar</button> -->
                                                                    <button type="button" class="btn btn-secondary-outline" ng-click="limpiar()">
                                                                        <i class="fa fa-reply"></i> Limpiar</button>
                                                                    <button type="button" class="btn btn-success" ng-show="formData.id_order>0" id="btnaddord" ng-click="editDatos()">
                                                                        <i class="fa fa-check"></i> Guardar</button>
                                                                    <!-- <button type="button" class="btn btn-success">
                                                                        <i class="fa fa-check-circle save"></i> Guardar & Listar</button> -->
                                                                </div>
                                                                <input type="hidden" value="0" ng-model="formData.idOrder" id="idOrder">
                                                            </div>
                                                            <div class="form-body portlet-body">
                                                                
                                                                <div class="form-group">
                                                                    <label class="col-md-2 control-label">Cliente:
                                                                        <span class="required"> * </span>
                                                                    </label>
                                                                    <div class="col-md-10">
                                                                        <input disabled type="text" ng-model="formData.cliente" class="form-control" name="cliente" id="cliente" placeholder="" > </div>
                                                                    </div>
                                                                </div>
																<div class="form-group">
                                                                    <label class="col-md-2 control-label">Sucursal:
                                                                        <span class="required"> * </span>
                                                                    </label>
                                                                    <div class="col-md-10">
                                                                        <input disabled type="text" ng-model="formData.sucursal" class="form-control" name="sucursal" id="sucursal" placeholder="" > </div>
                                                                    </div>
                                                                <div class="form-group">
                                                                    <label class="col-md-2 control-label">Tipo de Cliente :
                                                                        <span class="required"> * </span>
                                                                    </label>
                                                                    <div class="col-md-10">
                                                                         <select  disabled class="table-group-action-input form-control input-medium" name="tipo_cliente" ng-model="formData.tipo_cliente" id="tipo_cliente">
                                                                            <option value="1">Comercial</option>
                                                                            <option value="2">Industrial</option>
                                                                            <option value="3">Residencial</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-md-2 control-label">Direccion :
                                                                        <span class="required"> * </span>
                                                                    </label>
                                                                    <div class="col-md-10">
                                                                        <span class="help-block"> Direccion del lugar </span>
                                                                        <textarea disabled class="form-control" ng-model="formData.direccion" name="direccion" id="direccion"></textarea>
                                                                    </div>
                                                                </div>
                                                                <!-- <div class="form-group">
                                                                    <label class="col-md-2 control-label">Tipo de Trabajo:
                                                                        <span class="required"> * </span>
                                                                    </label>
                                                                    <div class="col-md-10">
                                                                        <div class="form-control height-auto">
                                                                            <div class="scroller" style="height:75px;" data-always-visible="1">
                                                                                <ul class="list-unstyled">
                                                                                    <li>
                                                                                        <ul class="list-unstyled">
                                                                                            <li ng-repeat="tipo in formData.tipos_trabajo">
                                                                                                <input type="checkbox" checked="checked"/> {{ tipo }}
                                                                                            </li>
                                                                                        </ul>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div> -->
                                                                <div class="form-group">
                                                                <label class="control-label col-md-2">Tiempo</label>
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <input type="text" ng-model="formData.tiempo" id="clockface_2" value="1:30" class="form-control" readonly="" />
                                                                        <span class="input-group-btn">
                                                                            <button class="btn default" type="button" id="clockface_2_toggle">
                                                                                <i class="fa fa-clock-o"></i>
                                                                            </button>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                                <br><br>
                                                                <fieldset class="scheduler-border">
                                                                    <legend class="scheduler-border">Equipos</legend>
                                                                    <ul class="nav nav-tabs">
                                                                        <li ng-show="formData.insTab > 0" >
                                                                            <a href="#instalacion_tab" data-toggle="tab"> Instalación </a>
                                                                        </li>
                                                                        <li ng-show="formData.manTab > 0">
                                                                            <a href="#mantenimiento_tab" data-toggle="tab"> Mantenimiento </a>
                                                                        </li>
                                                                        <li ng-show="formData.corTab > 0">
                                                                            <a href="#correctivo_tab" data-toggle="tab"> Correctivo </a>
                                                                        </li>
																		<li ng-show="formData.revTab > 0">
                                                                            <a href="#revision_tab" data-toggle="tab"> Revision </a>
                                                                        </li>
                                                                    </ul>
                                                                    <!--Instalacioón-->
                                                                    <div class="tab-content" >
                                                                    <div  ng-show="formData.insTab > 0" class="tab-pane active" id="instalacion_tab">
                                                                    <ul class="nav nav-tabs">
                                                                        <li class="active">
                                                                            <a href="#ins_clima" data-toggle="tab"> Climatización A </a>
                                                                        </li>
                                                                        <li >
                                                                            <a href="#ins_climb" data-toggle="tab"> Climatización B </a>
                                                                        </li>
                                                                        <li >
                                                                            <a href="#ins_ventilacion" data-toggle="tab"> Ventilación </a>
                                                                        </li>
                                                                        <li >
                                                                            <a href="#ins_refrigeracion" data-toggle="tab"> Refrigeración </a>
                                                                        </li>
                                                                        </ul>
                                                                        <div class="tab-content" style="overflow-x:scroll; ">
                                                                        <!--Clima A mantenimiento-->
                                                                        <div class="tab-pane active" id="ins_clima">
                                                                    <div class="form-group">
                                                                        <label class="col-md-2 control-label">Equipo:
                                                                            <span class="required"> * </span>
                                                                        </label>
                                                                        <div class="col-md-4">
                                                                            <div class="input-group">
                                                                                <select class="table-group-action-input form-control" name="equipo" id="equipo">
                                                                                    <option value="">Seleccione...</option>
                                                                                    <option ng-repeat="equipo in formData.equiposclimA" 

                                                                                                value="{{equipo.id}}">

                                                                                                {{equipo.nombre}}

                                                                                </option>
                                                                                </select>
                                                                                <div class="input-group-btn">
                                                                                    <button class="btn btn-success" ng-click="addRowinsclimA()" id="btnaddarea" type="button">
                                                                                        <i class="fa fa-plus"></i> Agregar</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="col-md-8 col-md-offset-2">
                                                                            <table class="table table-striped table-bordered table-hover table-checkable">
                                                                                <tr>
                                                                                    <th>#</th>
                                                                                    <th>Área</th>
                                                                                    <th>Descripcion del Equipo</th>
                                                                                    <th>Código del Equipo</th>
                                                                                    <th>Marca del Equipo</th>
                                                                                    <th>Capacidad (BTU) Evaporador</th>
                                                                                    <th>Modelo Evaporador</th>
                                                                                    <th>Serie Evaporador</th>
                                                                                    <th>Opciones</th>
                                                                                </tr>
                                                                                <tr ng-repeat="ins in formData.insclimA">
                                                                                    <td>{{($index+1)}}</td>
                                                                                    <td>{{ins.area}}</td>
                                                                                    <td>{{ins.desequipo}}</td>
                                                                                    <td>{{ins.codigo}}</td>
                                                                                    <td>{{ins.marca}}</td>
                                                                                    <td>{{ins.capacidadBTU}}</td>
                                                                                    <td>{{ins.modelo}}</td>
                                                                                    <td>{{ins.serie}}</td>
                                                                                    <td><button class="btn btn-danger" type="button" 
                                                                                    ng-click="deleteinsclimA(ins , $index)">Eliminar</button></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    </div>
                                                                        <!--Fin clima A mantenimiento-->
                                                                        <!--Clima b mantenimiento-->
                                                                        <div class="tab-pane" id="ins_climb">
                                                                        <div class="form-group">
                                                                        <label class="col-md-2 control-label">Equipo:
                                                                            <span class="required"> * </span>
                                                                        </label>
                                                                        <div class="col-md-4">
                                                                            <div class="input-group">
                                                                                <select class="table-group-action-input form-control" name="equipoB" id="equipoB">
                                                                                    <option value="">Seleccione...</option>
                                                                                    <option ng-repeat="equipo in formData.equiposclimB" 

                                                                                                value="{{equipo.id}}">

                                                                                                {{equipo.nombre}}

                                                                                </option>
                                                                                </select>
                                                                                <div class="input-group-btn">
                                                                                    <button class="btn btn-success" ng-click="addRowinsclimB()" id="btnaddarea" type="button">
                                                                                        <i class="fa fa-plus"></i> Agregar</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="col-md-8 col-md-offset-2">
                                                                            <table class="table table-striped table-bordered table-hover table-checkable">
                                                                                <tr>
                                                                                    <th>#</th>
                                                                                    <th>Área</th>
                                                                                    <th>Descripcion del Equipo</th>
                                                                                    <th>Código del Equipo</th>
                                                                                    <th>Marca Evaporador</th>
                                                                                    <th>Capacidad (BTU) Evaporador</th>
                                                                                    <th>Modelo Evaporador</th>
                                                                                    <th>Serie Evaporador</th>
																					<th>Marca Condensador</th>
                                                                                    <th>Capacidad (BTU) Condensador</th>
                                                                                    <th>Modelo Condensador</th>
                                                                                    <th>Serie Condensador</th>
                                                                                    <th>Opciones</th>
                                                                                </tr>
                                                                                <tr ng-repeat="ins in formData.insclimB">
                                                                                    <td>{{($index+1)}}</td>
                                                                                    <td>{{ins.area}}</td>
                                                                                    <td>{{ins.desequipo}}</td>
                                                                                    <td>{{ins.codigo}}</td>
                                                                                    <td>{{ins.marcaE}}</td>
                                                                                    <td>{{ins.capacidadBTUE}}</td>
                                                                                    <td>{{ins.modeloE}}</td>
                                                                                    <td>{{ins.serieE}}</td>
																					<td>{{ins.marcaC}}</td>
                                                                                    <td>{{ins.capacidadBTUC}}</td>
                                                                                    <td>{{ins.modeloC}}</td>
                                                                                    <td>{{ins.serieC}}</td>
                                                                                    <td><button class="btn btn-danger" type="button" 
                                                                                    ng-click="deleteinsclimB(ins , $index)">Eliminar</button></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>       
																		
                                                                    </div>                                                                            
                                                                        <!--Fin clima B mantenimiento-->
                                                                         <!--Ventilacion mantenimiento-->
                                                                        <div class="tab-pane" id="ins_ventilacion">
                                                                                <div class="form-group">
                                                                        <label class="col-md-2 control-label">Equipo:
                                                                            <span class="required"> * </span>
                                                                        </label>
                                                                        <div class="col-md-4">
                                                                            <div class="input-group">
                                                                                <select class="table-group-action-input form-control" name="equipoV" id="equipoV">
                                                                                    <option value="">Seleccione...</option>
                                                                                    <option ng-repeat="equipo in formData.equiposVentilacion" 

                                                                                                value="{{equipo.id}}">

                                                                                                {{equipo.nombre}}

                                                                                </option>
                                                                                </select>
                                                                                <div class="input-group-btn">
                                                                                    <button class="btn btn-success" ng-click="addRowinsVenti()" id="btnaddarea" type="button">
                                                                                        <i class="fa fa-plus"></i> Agregar</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="col-md-8 col-md-offset-2">
                                                                            <table class="table table-striped table-bordered table-hover table-checkable">
                                                                                <tr>
                                                                                    <th>#</th>
                                                                                    <th>Área</th>
                                                                                    <th>Descripcion del Equipo</th>
                                                                                    <th>Código del Equipo</th>
                                                                                    <th>Marca del Equipo</th>
                                                                                    <th>Capacidad HP</th>
                                                                                    <th>Capacidad CNF</th>
                                                                                    <th>Modelo</th>
                                                                                    <th>Serie</th>
                                                                                    <th>Opciones</th>
                                                                                </tr>
                                                                                <tr ng-repeat="ins in formData.insVentilacion">
                                                                                    <td>{{($index+1)}}</td>
                                                                                    <td>{{ins.area}}</td>
                                                                                    <td>{{ins.desequipo}}</td>
                                                                                    <td>{{ins.codigo}}</td>
                                                                                    <td>{{ins.marca}}</td>
                                                                                    <td>{{ins.capacidadHP}}</td>
                                                                                    <td>{{ins.capacidadCNF}}</td>
                                                                                    <td>{{ins.modelo}}</td>
                                                                                    <td>{{ins.serie}}</td>
                                                                                    <td><button class="btn btn-danger" type="button" 
                                                                                    ng-click="deleteinsVenti(ins , $index)">Eliminar</button></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                        </div>
                                                                   
                                                                    </div>                                                                            
                                                                        <!--Fin ventilacion instalacion-->
                                                                        <!--Refrigeración instalacion-->
                                                                        <div class="tab-pane" id="ins_refrigeracion">
                                                                            <div class="form-group">
                                                                        <label class="col-md-2 control-label">Equipo:
                                                                            <span class="required"> * </span>
                                                                        </label>
                                                                        <div class="col-md-4">
                                                                            <div class="input-group">
                                                                                <select class="table-group-action-input form-control" name="equipoR" id="equipoR">
                                                                                    <option value="">Seleccione...</option>
                                                                                    <option ng-repeat="equipo in formData.equiposRefri" 

                                                                                                value="{{equipo.id}}">

                                                                                                {{equipo.nombre}}

                                                                                </option>
                                                                                </select>
                                                                                <div class="input-group-btn">
                                                                                    <button class="btn btn-success" ng-click="addRowinsRefri()" id="btnaddarea" type="button">
                                                                                        <i class="fa fa-plus"></i> Agregar</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="col-md-8 col-md-offset-2">
                                                                            <table class="table table-striped table-bordered table-hover table-checkable">
                                                                                <tr>
                                                                                    <th>#</th>
                                                                                    <th>Área</th>
                                                                                    <th>Descripcion del Equipo</th>
                                                                                    <th>Código del Equipo</th>
                                                                                    <th>Marca del Equipo</th>
																					<th>Modelo</th>
                                                                                    <th>Serie</th>
                                                                                    <th>Capacidad (HP/BTU)</th>
                                                                                    <th>Cantidad compresores</th>
                                                                                     <th>Capacidad (HP)</th>
                                                                                     <th>Cantidad Motor</th>
                                                                                    <th>Opciones</th>
                                                                                </tr>
                                                                                <tr ng-repeat="ins in formData.insRefri">
                                                                                    <td>{{($index+1)}}</td>
                                                                                    <td>{{ins.area}}</td>
                                                                                    <td>{{ins.desequipo}}</td>
                                                                                    <td>{{ins.codigo}}</td>
                                                                                    <td>{{ins.marca}}</td>
																					<td>{{ins.modelo}}</td>
                                                                                    <td>{{ins.serie}}</td>
                                                                                    <td>{{ins.capacidadHPBTU}}</td>
                                                                                    <td>{{ins.cantidadCompre}}</td>
                                                                                    <td>{{ins.capacidadHP}}</td>
                                                                                    <td>{{ins.cantidadMotor}}</td>
                                                                                    
                                                                                    <td><button class="btn btn-danger" type="button" 
                                                                                    ng-click="deleteinsRefri(ins , $index)">Eliminar</button></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                        </div>   
                                                                    
                                                                    </div>                                                                            
                                                                        <!--Fin refigeracion instalacion-->
                                                                        </div><!--Tab content-->
                                                                    </div> <!--Fin Instalacion-->
                                                                    <!--Mantenimiento-->
                                                                    <div ng-show="formData.manTab > 0" class="tab-pane" id="mantenimiento_tab">
                                                                        <ul class="nav nav-tabs">
                                                                        <li class="active">
                                                                            <a href="#man_clima" data-toggle="tab"> Climatización A </a>
                                                                        </li>
                                                                        <li >
                                                                            <a href="#man_climb" data-toggle="tab"> Climatización B </a>
                                                                        </li>
                                                                        <li >
                                                                            <a href="#man_ventilacion" data-toggle="tab"> Ventilación </a>
                                                                        </li>
                                                                        <li >
                                                                            <a href="#man_refrigeracion" data-toggle="tab"> Refrigeración </a>
                                                                        </li>
                                                                        </ul>
                                                                        <div class="tab-content" style="overflow-x:scroll; ">
                                                                        <!--Clima A mantenimiento-->
                                                                        <div class="tab-pane active" id="man_clima">
                                                                    <div class="form-group">
                                                                        <label class="col-md-2 control-label">Equipo:
                                                                            <span class="required"> * </span>
                                                                        </label>
                                                                        <div class="col-md-4">
                                                                            <div class="input-group">
                                                                                <select class="table-group-action-input form-control" name="equipoMA" id="equipoMA">
                                                                                    <option value="">Seleccione...</option>
                                                                                    <option ng-repeat="equipo in formData.equiposclimA" 

                                                                                                value="{{equipo.id}}">

                                                                                                {{equipo.nombre}}

                                                                                </option>
                                                                                </select>
                                                                                <div class="input-group-btn">
                                                                                    <button class="btn btn-success" ng-click="addRowmanclimA()" id="btnaddarea" type="button">
                                                                                        <i class="fa fa-plus"></i> Agregar</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="col-md-8 col-md-offset-2">
                                                                            <table class="table table-striped table-bordered table-hover table-checkable">
                                                                                <tr>
                                                                                    <th>#</th>
                                                                                    <th>Área</th>
                                                                                    <th>Descripcion del Equipo</th>
                                                                                    <th>Código del Equipo</th>
                                                                                    <th>Marca del Equipo</th>
                                                                                    <th>Capacidad (BTU) Evaporador</th>
                                                                                    <th>Modelo Evaporador</th>
                                                                                    <th>Serie Evaporador</th>
                                                                                    <th>Opciones</th>
                                                                                </tr>
                                                                                <tr ng-repeat="ins in formData.manclimA">
                                                                                    <td>{{($index+1)}}</td>
                                                                                    <td>{{ins.area}}</td>
                                                                                    <td>{{ins.desequipo}}</td>
                                                                                    <td>{{ins.codigo}}</td>
                                                                                    <td>{{ins.marca}}</td>
                                                                                    <td>{{ins.capacidadBTU}}</td>
                                                                                    <td>{{ins.modelo}}</td>
                                                                                    <td>{{ins.serie}}</td>
                                                                                    <td><button class="btn btn-danger" type="button" 
                                                                                    ng-click="deletemanclimA(ins , $index)">Eliminar</button></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    </div>
                                                                        <!--Fin clima A mantenimiento-->
                                                                        <!--Clima b mantenimiento-->
                                                                        <div class="tab-pane" id="man_climb">
                                                                    <div class="form-group">
                                                                        <label class="col-md-2 control-label">Equipo:
                                                                            <span class="required"> * </span>
                                                                        </label>
                                                                        <div class="col-md-4">
                                                                            <div class="input-group">
                                                                                <select class="table-group-action-input form-control" name="equipoMB" id="equipoMB">
                                                                                    <option value="">Seleccione...</option>
                                                                                    <option ng-repeat="equipo in formData.equiposclimB" 

                                                                                                value="{{equipo.id}}">

                                                                                                {{equipo.nombre}}

                                                                                </option>
                                                                                </select>
                                                                                <div class="input-group-btn">
                                                                                    <button class="btn btn-success" ng-click="addRowmanclimB()" id="btnaddarea" type="button">
                                                                                        <i class="fa fa-plus"></i> Agregar</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="col-md-8 col-md-offset-2">
                                                                            <table class="table table-striped table-bordered table-hover table-checkable">
                                                                                <tr>
                                                                                    <th>#</th>
                                                                                    <th>Área</th>
                                                                                    <th>Descripcion del Equipo</th>
                                                                                    <th>Código del Equipo</th>
                                                                                    <th>Marca Evaporador</th>
                                                                                    <th>Capacidad (BTU) Evaporador</th>
                                                                                    <th>Modelo Evaporador</th>
                                                                                    <th>Serie Evaporador</th>
																					<th>Marca Condensador</th>
                                                                                    <th>Capacidad (BTU) Condensador</th>
                                                                                    <th>Modelo Condensador</th>
                                                                                    <th>Serie Condensador</th>
                                                                                    <th>Opciones</th>
                                                                                </tr>
                                                                                <tr ng-repeat="ins in formData.manclimB">
                                                                                    <td>{{($index+1)}}</td>
                                                                                    <td>{{ins.area}}</td>
                                                                                    <td>{{ins.desequipo}}</td>
                                                                                    <td>{{ins.codigo}}</td>
                                                                                    <td>{{ins.marcaE}}</td>
                                                                                    <td>{{ins.capacidadBTUE}}</td>
                                                                                    <td>{{ins.modeloE}}</td>
                                                                                    <td>{{ins.serieE}}</td>
																					<td>{{ins.marcaC}}</td>
                                                                                    <td>{{ins.capacidadBTUC}}</td>
                                                                                    <td>{{ins.modeloC}}</td>
                                                                                    <td>{{ins.serieC}}</td>
                                                                                    <td><button class="btn btn-danger" type="button" 
                                                                                    ng-click="deletemanclimB(ins , $index)">Eliminar</button></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>             
                                                                    
                                                                    </div>                                                                            
                                                                        <!--Fin clima B mantenimiento-->
                                                                         <!--Ventilacion mantenimiento-->
                                                                        <div class="tab-pane" id="man_ventilacion">
                                                                                <div class="form-group">
                                                                        <label class="col-md-2 control-label">Equipo:
                                                                            <span class="required"> * </span>
                                                                        </label>
                                                                        <div class="col-md-4">
                                                                            <div class="input-group">
                                                                                <select class="table-group-action-input form-control" name="equipoMV" id="equipoMV">
                                                                                    <option value="">Seleccione...</option>
                                                                                    <option ng-repeat="equipo in formData.equiposVentilacion" 

                                                                                                value="{{equipo.id}}">

                                                                                                {{equipo.nombre}}

                                                                                </option>
                                                                                </select>
                                                                                <div class="input-group-btn">
                                                                                    <button class="btn btn-success" ng-click="addRowmanVenti()" id="btnaddarea" type="button">
                                                                                        <i class="fa fa-plus"></i> Agregar</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="col-md-8 col-md-offset-2">
                                                                            <table class="table table-striped table-bordered table-hover table-checkable">
                                                                                <tr>
                                                                                    <th>#</th>
                                                                                    <th>Área</th>
                                                                                    <th>Descripcion del Equipo</th>
                                                                                    <th>Código del Equipo</th>
                                                                                    <th>Marca del Equipo</th>
                                                                                    <th>Capacidad HP</th>
                                                                                    <th>Capacidad CNF</th>
                                                                                    <th>Modelo</th>
                                                                                    <th>Serie</th>
                                                                                    <th>Opciones</th>
                                                                                </tr>
                                                                                <tr ng-repeat="ins in formData.manVentilacion">
                                                                                    <td>{{($index+1)}}</td>
                                                                                    <td>{{ins.area}}</td>
                                                                                    <td>{{ins.desequipo}}</td>
                                                                                    <td>{{ins.codigo}}</td>
                                                                                    <td>{{ins.marca}}</td>
                                                                                    <td>{{ins.capacidadHP}}</td>
                                                                                    <td>{{ins.capacidadCNF}}</td>
                                                                                    <td>{{ins.modelo}}</td>
                                                                                    <td>{{ins.serie}}</td>
                                                                                    <td><button class="btn btn-danger" type="button" 
                                                                                    ng-click="deletemanVenti(ins , $index)">Eliminar</button></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                        </div>
                                                                    </div>                                                                            
                                                                        <!--Fin ventilacion mantenimiento-->
                                                                        <!--Refrigeración mantenimiento-->
                                                                        <div class="tab-pane" id="man_refrigeracion">
                                                                             <div class="form-group">
                                                                        <label class="col-md-2 control-label">Equipo:
                                                                            <span class="required"> * </span>
                                                                        </label>
                                                                        <div class="col-md-4">
                                                                            <div class="input-group">
                                                                                <select class="table-group-action-input form-control" name="equipoMR" id="equipoMR">
                                                                                    <option value="">Seleccione...</option>
                                                                                    <option ng-repeat="equipo in formData.equiposRefri" 

                                                                                                value="{{equipo.id}}">

                                                                                                {{equipo.nombre}}

                                                                                </option>
                                                                                </select>
                                                                                <div class="input-group-btn">
                                                                                    <button class="btn btn-success" ng-click="addRowmanRefri()" id="btnaddarea" type="button">
                                                                                        <i class="fa fa-plus"></i> Agregar</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="col-md-8 col-md-offset-2">
                                                                            <table class="table table-striped table-bordered table-hover table-checkable">
                                                                                <tr>
                                                                                    <th>#</th>
                                                                                    <th>Área</th>
                                                                                    <th>Descripcion del Equipo</th>
                                                                                    <th>Código del Equipo</th>
                                                                                    <th>Marca del Equipo</th>
																					<th>Modelo</th>
                                                                                    <th>Serie</th>
                                                                                    <th>Capacidad (HP/BTU)</th>
                                                                                    <th>Cantidad compresores</th>
                                                                                     <th>Capacidad (HP)</th>
                                                                                     <th>Cantidad Motor</th>
                                                                                    <th>Opciones</th>
                                                                                </tr>
                                                                                <tr ng-repeat="ins in formData.manRefri">
                                                                                    <td>{{($index+1)}}</td>
                                                                                    <td>{{ins.area}}</td>
                                                                                    <td>{{ins.desequipo}}</td>
                                                                                    <td>{{ins.codigo}}</td>
                                                                                    <td>{{ins.marca}}</td>
																					<td>{{ins.modelo}}</td>
                                                                                    <td>{{ins.serie}}</td>
                                                                                    <td>{{ins.capacidadHPBTU}}</td>
                                                                                    <td>{{ins.cantidadCompre}}</td>
                                                                                    <td>{{ins.capacidadHP}}</td>
                                                                                    <td>{{ins.cantidadMotor}}</td>
                                                                                    
                                                                                    <td><button class="btn btn-danger" type="button" 
                                                                                    ng-click="deletemanRefri(ins , $index)">Eliminar</button></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                        </div>    
                                                                   
                                                                    </div>                                                                            
                                                                        <!--Fin refigeracion mantenimiento-->
                                                                        </div><!--Tab content-->
                                                                    </div>
                                                                    <!--Correctivo-->
                                                                    <div ng-show="formData.corTab > 0" class="tab-pane" id="correctivo_tab">
                                                                        <ul class="nav nav-tabs">
                                                                        <li class="active">
                                                                            <a href="#cor_clima" data-toggle="tab"> Climatización A </a>
                                                                        </li>
                                                                        <li >
                                                                            <a href="#cor_climb" data-toggle="tab"> Climatización B </a>
                                                                        </li>
                                                                        <li >
                                                                            <a href="#cor_ventilacion" data-toggle="tab"> Ventilación </a>
                                                                        </li>
                                                                        <li >
                                                                            <a href="#cor_refrigeracion" data-toggle="tab"> Refrigeración </a>
                                                                        </li>
                                                                        </ul>
                                                                        <div class="tab-content" style="overflow-x:scroll; ">
                                                                        <!--Clima A correctivo-->
                                                                        <div class="tab-pane active" id="cor_clima">
                                                                    <div class="form-group">
                                                                        <label class="col-md-2 control-label">Equipo:
                                                                            <span class="required"> * </span>
                                                                        </label>
                                                                        <div class="col-md-4">
                                                                            <div class="input-group">
                                                                                <select class="table-group-action-input form-control" name="equipoCA" id="equipoCA">
                                                                                    <option value="">Seleccione...</option>
                                                                                    <option ng-repeat="equipo in formData.equiposclimA" 

                                                                                                value="{{equipo.id}}">

                                                                                                {{equipo.nombre}}

                                                                                </option>
                                                                                </select>
                                                                                <div class="input-group-btn">
                                                                                    <button class="btn btn-success" ng-click="addRowcorclimA()" id="btnaddarea" type="button">
                                                                                        <i class="fa fa-plus"></i> Agregar</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="col-md-8 col-md-offset-2">
                                                                            <table class="table table-striped table-bordered table-hover table-checkable">
                                                                                <tr>
                                                                                    <th>#</th>
                                                                                    <th>Área</th>
                                                                                    <th>Descripcion del Equipo</th>
                                                                                    <th>Código del Equipo</th>
                                                                                    <th>Marca del Equipo</th>
                                                                                    <th>Capacidad (BTU) Evaporador</th>
                                                                                    <th>Modelo Evaporador</th>
                                                                                    <th>Serie Evaporador</th>
                                                                                    <th>Opciones</th>
                                                                                </tr>
                                                                                <tr ng-repeat="ins in formData.corclimA">
                                                                                    <td>{{($index+1)}}</td>
                                                                                    <td>{{ins.area}}</td>
                                                                                    <td>{{ins.desequipo}}</td>
                                                                                    <td>{{ins.codigo}}</td>
                                                                                    <td>{{ins.marca}}</td>
                                                                                    <td>{{ins.capacidadBTU}}</td>
                                                                                    <td>{{ins.modelo}}</td>
                                                                                    <td>{{ins.serie}}</td>
                                                                                    <td><button class="btn btn-danger" type="button" 
                                                                                    ng-click="deletecorclimA(ins , $index)">Eliminar</button></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    </div>
                                                                        <!--Fin clima A correctivo-->
                                                                        <!--Clima b correctivo-->
                                                                        <div class="tab-pane" id="cor_climb">
                                                                            <div class="form-group">
                                                                        <label class="col-md-2 control-label">Equipo:
                                                                            <span class="required"> * </span>
                                                                        </label>
                                                                        <div class="col-md-4">
                                                                            <div class="input-group">
                                                                                <select class="table-group-action-input form-control" name="equipoCB" id="equipoCB">
                                                                                    <option value="">Seleccione...</option>
                                                                                    <option ng-repeat="equipo in formData.equiposclimB" 

                                                                                                value="{{equipo.id}}">

                                                                                                {{equipo.nombre}}

                                                                                </option>
                                                                                </select>
                                                                                <div class="input-group-btn">
                                                                                    <button class="btn btn-success" ng-click="addRowcorclimB()" id="btnaddarea" type="button">
                                                                                        <i class="fa fa-plus"></i> Agregar</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="col-md-8 col-md-offset-2">
                                                                            <table class="table table-striped table-bordered table-hover table-checkable">
                                                                                <tr>
                                                                                    <th>#</th>
                                                                                    <th>Área</th>
                                                                                    <th>Descripcion del Equipo</th>
                                                                                    <th>Código del Equipo</th>
                                                                                    <th>Marca Evaporador</th>
                                                                                    <th>Capacidad (BTU) Evaporador</th>
                                                                                    <th>Modelo Evaporador</th>
                                                                                    <th>Serie Evaporador</th>
																					<th>Marca Condensador</th>
                                                                                    <th>Capacidad (BTU) Condensador</th>
                                                                                    <th>Modelo Condensador</th>
                                                                                    <th>Serie Condensador</th>
                                                                                    <th>Opciones</th>
                                                                                </tr>
                                                                                <tr ng-repeat="ins in formData.corclimB">
                                                                                    <td>{{($index+1)}}</td>
                                                                                    <td>{{ins.area}}</td>
                                                                                    <td>{{ins.desequipo}}</td>
                                                                                    <td>{{ins.codigo}}</td>
                                                                                    <td>{{ins.marcaE}}</td>
                                                                                    <td>{{ins.capacidadBTUE}}</td>
                                                                                    <td>{{ins.modeloE}}</td>
                                                                                    <td>{{ins.serieE}}</td>
																					<td>{{ins.marcaC}}</td>
                                                                                    <td>{{ins.capacidadBTUC}}</td>
                                                                                    <td>{{ins.modeloC}}</td>
                                                                                    <td>{{ins.serieC}}</td>
                                                                                    <td><button class="btn btn-danger" type="button" 
                                                                                    ng-click="deletecorclimB(ins , $index)">Eliminar</button></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>      
                                                                    
                                                                    </div>                                                                            
                                                                        <!--Fin clima B correctivo-->
                                                                         <!--Ventilacion correctivo-->
                                                                        <div class="tab-pane" id="cor_ventilacion">
                                                                      <div class="form-group">
                                                                        <label class="col-md-2 control-label">Equipo:
                                                                            <span class="required"> * </span>
                                                                        </label>
                                                                        <div class="col-md-4">
                                                                            <div class="input-group">
                                                                                <select class="table-group-action-input form-control" name="equipoCV" id="equipoCV">
                                                                                    <option value="">Seleccione...</option>
                                                                                    <option ng-repeat="equipo in formData.equiposVentilacion" 

                                                                                                value="{{equipo.id}}">

                                                                                                {{equipo.nombre}}

                                                                                </option>
                                                                                </select>
                                                                                <div class="input-group-btn">
                                                                                    <button class="btn btn-success" ng-click="addRowcorVenti()" id="btnaddarea" type="button">
                                                                                        <i class="fa fa-plus"></i> Agregar</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="col-md-8 col-md-offset-2">
                                                                            <table class="table table-striped table-bordered table-hover table-checkable">
                                                                                <tr>
                                                                                    <th>#</th>
                                                                                    <th>Área</th>
                                                                                    <th>Descripcion del Equipo</th>
                                                                                    <th>Código del Equipo</th>
                                                                                    <th>Marca del Equipo</th>
                                                                                    <th>Capacidad HP</th>
                                                                                    <th>Capacidad CNF</th>
                                                                                    <th>Modelo</th>
                                                                                    <th>Serie</th>
                                                                                    <th>Opciones</th>
                                                                                </tr>
                                                                                <tr ng-repeat="ins in formData.corVentilacion">
                                                                                    <td>{{($index+1)}}</td>
                                                                                    <td>{{ins.area}}</td>
                                                                                    <td>{{ins.desequipo}}</td>
                                                                                    <td>{{ins.codigo}}</td>
                                                                                    <td>{{ins.marca}}</td>
                                                                                    <td>{{ins.capacidadHP}}</td>
                                                                                    <td>{{ins.capacidadCNF}}</td>
                                                                                    <td>{{ins.modelo}}</td>
                                                                                    <td>{{ins.serie}}</td>
                                                                                    <td><button class="btn btn-danger" type="button" 
                                                                                    ng-click="deletecorVenti(ins , $index)">Eliminar</button></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                        </div>
                                                                    </div>                                                                            
                                                                        <!--Fin ventilacion correctivo-->
                                                                        <!--Refrigeración correctivo-->
                                                                        <div class="tab-pane" id="cor_refrigeracion">
                                                                     <div class="form-group">
                                                                        <label class="col-md-2 control-label">Equipo:
                                                                            <span class="required"> * </span>
                                                                        </label>
                                                                        <div class="col-md-4">
                                                                            <div class="input-group">
                                                                                <select class="table-group-action-input form-control" name="equipoCR" id="equipoCR">
                                                                                    <option value="">Seleccione...</option>
                                                                                    <option ng-repeat="equipo in formData.equiposRefri" 

                                                                                                value="{{equipo.id}}">

                                                                                                {{equipo.nombre}}

                                                                                </option>
                                                                                </select>
                                                                                <div class="input-group-btn">
                                                                                    <button class="btn btn-success" ng-click="addRowcorRefri()" id="btnaddarea" type="button">
                                                                                        <i class="fa fa-plus"></i> Agregar</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="col-md-8 col-md-offset-2">
                                                                            <table class="table table-striped table-bordered table-hover table-checkable">
                                                                                <tr>
                                                                                    <th>#</th>
                                                                                    <th>Área</th>
                                                                                    <th>Descripcion del Equipo</th>
                                                                                    <th>Código del Equipo</th>
                                                                                    <th>Marca del Equipo</th>
																					<th>Modelo</th>
                                                                                    <th>Serie</th>
                                                                                    <th>Capacidad (HP/BTU)</th>
                                                                                    <th>Cantidad compresores</th>
                                                                                     <th>Capacidad (HP)</th>
                                                                                     <th>Cantidad Motor</th>
                                                                                    <th>Opciones</th>
                                                                                </tr>
                                                                                <tr ng-repeat="ins in formData.corRefri">
                                                                                    <td>{{($index+1)}}</td>
                                                                                    <td>{{ins.area}}</td>
                                                                                    <td>{{ins.desequipo}}</td>
                                                                                    <td>{{ins.codigo}}</td>
                                                                                    <td>{{ins.marca}}</td>
																					<td>{{ins.modelo}}</td>
                                                                                    <td>{{ins.serie}}</td>
                                                                                    <td>{{ins.capacidadHPBTU}}</td>
                                                                                    <td>{{ins.cantidadCompre}}</td>
                                                                                    <td>{{ins.capacidadHP}}</td>
                                                                                    <td>{{ins.cantidadMotor}}</td>
                                                                                    
                                                                                    <td><button class="btn btn-danger" type="button" 
                                                                                    ng-click="deletecorRefri(ins , $index)">Eliminar</button></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                        </div>  
                                                                    </div>                                                                            
                                                                        <!--Fin refigeracion correctivo-->
                                                                        </div><!--Tab content-->
                                                                    </div>
																	<!--revision-->
                                                                    <div ng-show="formData.revTab > 0" class="tab-pane" id="revision_tab">
                                                                        <ul class="nav nav-tabs">
                                                                        <li class="active">
                                                                            <a href="#rev_clima" data-toggle="tab"> Climatización A </a>
                                                                        </li>
                                                                        <li >
                                                                            <a href="#rev_climb" data-toggle="tab"> Climatización B </a>
                                                                        </li>
                                                                        <li >
                                                                            <a href="#rev_ventilacion" data-toggle="tab"> Ventilación </a>
                                                                        </li>
                                                                        <li >
                                                                            <a href="#rev_refrigeracion" data-toggle="tab"> Refrigeración </a>
                                                                        </li>
                                                                        </ul>
                                                                        <div class="tab-content" style="overflow-x:scroll; ">
                                                                        <!--Clima A revision-->
                                                                        <div class="tab-pane active" id="rev_clima">
                                                                    <div class="form-group">
                                                                        <label class="col-md-2 control-label">Equipo:
                                                                            <span class="required"> * </span>
                                                                        </label>
                                                                        <div class="col-md-4">
                                                                            <div class="input-group">
                                                                                <select class="table-group-action-input form-control" name="equipoRA" id="equipoRA">
                                                                                    <option value="">Seleccione...</option>
                                                                                    <option ng-repeat="equipo in formData.equiposclimA" 

                                                                                                value="{{equipo.id}}">

                                                                                                {{equipo.nombre}}

                                                                                </option>
                                                                                </select>
                                                                                <div class="input-group-btn">
                                                                                    <button class="btn btn-success" ng-click="addRowrevclimA()" id="btnaddarea" type="button">
                                                                                        <i class="fa fa-plus"></i> Agregar</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="col-md-8 col-md-offset-2">
                                                                            <table class="table table-striped table-bordered table-hover table-checkable">
                                                                                <tr>
                                                                                    <th>#</th>
                                                                                    <th>Área</th>
                                                                                    <th>Descripcion del Equipo</th>
                                                                                    <th>Código del Equipo</th>
                                                                                    <th>Marca del Equipo</th>
                                                                                    <th>Capacidad (BTU) Evaporador</th>
                                                                                    <th>Modelo Evaporador</th>
                                                                                    <th>Serie Evaporador</th>
                                                                                    <th>Opciones</th>
                                                                                </tr>
                                                                                <tr ng-repeat="ins in formData.revclimA">
                                                                                    <td>{{($index+1)}}</td>
                                                                                    <td>{{ins.area}}</td>
                                                                                    <td>{{ins.desequipo}}</td>
                                                                                    <td>{{ins.codigo}}</td>
                                                                                    <td>{{ins.marca}}</td>
                                                                                    <td>{{ins.capacidadBTU}}</td>
                                                                                    <td>{{ins.modelo}}</td>
                                                                                    <td>{{ins.serie}}</td>
                                                                                    <td><button class="btn btn-danger" type="button" 
                                                                                    ng-click="deleterevclimA(ins , $index)">Eliminar</button></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    </div>
                                                                        <!--Fin clima A revision-->
                                                                        <!--Clima b revision-->
                                                                        <div class="tab-pane" id="rev_climb">
                                                                            <div class="form-group">
                                                                        <label class="col-md-2 control-label">Equipo:
                                                                            <span class="required"> * </span>
                                                                        </label>
                                                                        <div class="col-md-4">
                                                                            <div class="input-group">
                                                                                <select class="table-group-action-input form-control" name="equipoRB" id="equipoRB">
                                                                                    <option value="">Seleccione...</option>
                                                                                    <option ng-repeat="equipo in formData.equiposclimB" 

                                                                                                value="{{equipo.id}}">

                                                                                                {{equipo.nombre}}

                                                                                </option>
                                                                                </select>
                                                                                <div class="input-group-btn">
                                                                                    <button class="btn btn-success" ng-click="addRowrevclimB()" id="btnaddarearev" type="button">
                                                                                        <i class="fa fa-plus"></i> Agregar</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="col-md-8 col-md-offset-2">
                                                                            <table class="table table-striped table-bordered table-hover table-checkable">
                                                                                <tr>
                                                                                    <th>#</th>
                                                                                    <th>Área</th>
                                                                                    <th>Descripcion del Equipo</th>
                                                                                    <th>Código del Equipo</th>
                                                                                    <th>Marca Evaporador</th>
                                                                                    <th>Capacidad (BTU) Evaporador</th>
                                                                                    <th>Modelo Evaporador</th>
                                                                                    <th>Serie Evaporador</th>
																					<th>Marca Condensador</th>
                                                                                    <th>Capacidad (BTU) Condensador</th>
                                                                                    <th>Modelo Condensador</th>
                                                                                    <th>Serie Condensador</th>
                                                                                    <th>Opciones</th>
                                                                                </tr>
                                                                                <tr ng-repeat="ins in formData.revclimB">
                                                                                    <td>{{($index+1)}}</td>
                                                                                    <td>{{ins.area}}</td>
                                                                                    <td>{{ins.desequipo}}</td>
                                                                                    <td>{{ins.codigo}}</td>
                                                                                    <td>{{ins.marcaE}}</td>
                                                                                    <td>{{ins.capacidadBTUE}}</td>
                                                                                    <td>{{ins.modeloE}}</td>
                                                                                    <td>{{ins.serieE}}</td>
																					<td>{{ins.marcaC}}</td>
                                                                                    <td>{{ins.capacidadBTUC}}</td>
                                                                                    <td>{{ins.modeloC}}</td>
                                                                                    <td>{{ins.serieC}}</td>
                                                                                    <td><button class="btn btn-danger" type="button" 
                                                                                    ng-click="deleterevclimB(ins , $index)">Eliminar</button></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>      
                                                                    
                                                                    </div>                                                                            
                                                                        <!--Fin clima B revision-->
                                                                         <!--Ventilacion revision-->
                                                                        <div class="tab-pane" id="rev_ventilacion">
                                                                      <div class="form-group">
                                                                        <label class="col-md-2 control-label">Equipo:
                                                                            <span class="required"> * </span>
                                                                        </label>
                                                                        <div class="col-md-4">
                                                                            <div class="input-group">
                                                                                <select class="table-group-action-input form-control" name="equipoRV" id="equipoRV">
                                                                                    <option value="">Seleccione...</option>
                                                                                    <option ng-repeat="equipo in formData.equiposVentilacion" 

                                                                                                value="{{equipo.id}}">

                                                                                                {{equipo.nombre}}

                                                                                </option>
                                                                                </select>
                                                                                <div class="input-group-btn">
                                                                                    <button class="btn btn-success" ng-click="addRowrevVenti()" id="btnaddarea" type="button">
                                                                                        <i class="fa fa-plus"></i> Agregar</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="col-md-8 col-md-offset-2">
                                                                            <table class="table table-striped table-bordered table-hover table-checkable">
                                                                                <tr>
                                                                                    <th>#</th>
                                                                                    <th>Área</th>
                                                                                    <th>Descripcion del Equipo</th>
                                                                                    <th>Código del Equipo</th>
                                                                                    <th>Marca del Equipo</th>
                                                                                    <th>Capacidad HP</th>
                                                                                    <th>Capacidad CNF</th>
                                                                                    <th>Modelo</th>
                                                                                    <th>Serie</th>
                                                                                    <th>Opciones</th>
                                                                                </tr>
                                                                                <tr ng-repeat="ins in formData.revVentilacion">
                                                                                    <td>{{($index+1)}}</td>
                                                                                    <td>{{ins.area}}</td>
                                                                                    <td>{{ins.desequipo}}</td>
                                                                                    <td>{{ins.codigo}}</td>
                                                                                    <td>{{ins.marca}}</td>
                                                                                    <td>{{ins.capacidadHP}}</td>
                                                                                    <td>{{ins.capacidadCNF}}</td>
                                                                                    <td>{{ins.modelo}}</td>
                                                                                    <td>{{ins.serie}}</td>
                                                                                    <td><button class="btn btn-danger" type="button" 
                                                                                    ng-click="deleterevVenti(ins , $index)">Eliminar</button></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                        </div>
                                                                    </div>                                                                            
                                                                        <!--Fin ventilacion revision-->
                                                                        <!--Refrigeración revision-->
                                                                        <div class="tab-pane" id="rev_refrigeracion">
                                                                     <div class="form-group">
                                                                        <label class="col-md-2 control-label">Equipo:
                                                                            <span class="required"> * </span>
                                                                        </label>
                                                                        <div class="col-md-4">
                                                                            <div class="input-group">
                                                                                <select class="table-group-action-input form-control" name="equipoRR" id="equipoRR">
                                                                                    <option value="">Seleccione...</option>
                                                                                    <option ng-repeat="equipo in formData.equiposRefri" 

                                                                                                value="{{equipo.id}}">

                                                                                                {{equipo.nombre}}

                                                                                </option>
                                                                                </select>
                                                                                <div class="input-group-btn">
                                                                                    <button class="btn btn-success" ng-click="addRowrevRefri()" id="btnaddarea" type="button">
                                                                                        <i class="fa fa-plus"></i> Agregar</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="col-md-8 col-md-offset-2">
                                                                            <table class="table table-striped table-bordered table-hover table-checkable">
                                                                                <tr>
                                                                                    <th>#</th>
                                                                                    <th>Área</th>
                                                                                    <th>Descripcion del Equipo</th>
                                                                                    <th>Código del Equipo</th>
                                                                                    <th>Marca del Equipo</th>
																					<th>Modelo</th>
                                                                                    <th>Serie</th>
                                                                                    <th>Capacidad (HP/BTU)</th>
                                                                                    <th>Cantidad compresores</th>
                                                                                     <th>Capacidad (HP)</th>
                                                                                     <th>Cantidad Motor</th>
                                                                                    <th>Opciones</th>
                                                                                </tr>
                                                                                <tr ng-repeat="ins in formData.revRefri">
                                                                                    <td>{{($index+1)}}</td>
                                                                                    <td>{{ins.area}}</td>
                                                                                    <td>{{ins.desequipo}}</td>
                                                                                    <td>{{ins.codigo}}</td>
                                                                                    <td>{{ins.marca}}</td>
																					<td>{{ins.modelo}}</td>
                                                                                    <td>{{ins.serie}}</td>
                                                                                    <td>{{ins.capacidadHPBTU}}</td>
                                                                                    <td>{{ins.cantidadCompre}}</td>
                                                                                    <td>{{ins.capacidadHP}}</td>
                                                                                    <td>{{ins.cantidadMotor}}</td>
                                                                                    
                                                                                    <td><button class="btn btn-danger" type="button" 
                                                                                    ng-click="deleterevRefri(ins , $index)">Eliminar</button></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                        </div>  
                                                                    </div>                                                                            
                                                                        <!--Fin refigeracion revision-->
                                                                        </div><!--Tab content-->
                                                                    </div>
                                                                    </div>
                                                                </fieldset>
                                                                <br><br>
                                                                <fieldset class="scheduler-border">
                                                                    <legend class="scheduler-border">Materiales</legend>
                                                                    <div class="form-group">
                                                                        <label class="col-md-2 control-label">Equipo :
                                                                            <span class="required"> * </span>
                                                                        </label>
                                                                        <div class="col-md-10">
																		<input id="equiposVal" class="table-group-action-input form-control" list="listEquipoM" type="" />
																		<datalist id="listEquipoM">
																			<option ng-repeat="equipo in formData.equiposVal" value="{{equipo.dos}}"></option>
																		</datalist>
																		<!--
                                                                            <select class="table-group-action-input form-control" name="materiales2" id="materiales2">
                                                                                    <option value="">Seleccione...</option>
                                                                                    <option ng-repeat="equipo in formData.materiales2" 

                                                                                                value="{{equipo.id}}">

                                                                                                {{equipo.item}}

                                                                                </option>
                                                                                </select>-->
                                                                        </div>
                                                                    </div>
																	<div class="form-group">
                                                                        <label class="col-md-2 control-label">Item :
                                                                            <span class="required"> * </span>
                                                                        </label>
                                                                        <div class="col-md-10">
																		<input  id="materiales2" class="table-group-action-input form-control" list="listMateriales" type="" />
																		<datalist id="listMateriales">
																			<option ng-repeat="equipo in formData.materiales2" value="{{equipo.item}}"></option>
																		</datalist>
																		<!--
                                                                            <select class="table-group-action-input form-control" name="materiales2" id="materiales2">
                                                                                    <option value="">Seleccione...</option>
                                                                                    <option ng-repeat="equipo in formData.materiales2" 

                                                                                                value="{{equipo.id}}">

                                                                                                {{equipo.item}}

                                                                                </option>
                                                                                </select>-->
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    <div class="form-group">
                                                                        <label class="col-md-2 control-label">Cantidad requerida :
                                                                            <span class="required"> * </span>
                                                                        </label>
                                                                        <div class="col-md-4">
                                                                            <div class="input-group">
                                                                                <input type="text" id="cantidad"/>
                                                                                <div class="input-group-btn">
                                                                                    <button class="btn btn-success" ng-click="addRowMateriales()" type="button">
                                                                                        <i class="fa fa-plus"></i> Agregar</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="col-md-8 col-md-offset-2">
                                                                            <table class="table table-striped table-bordered table-hover table-checkable">
                                                                                <tr>
                                                                                    <th>#</th>
																					<th>Codigo de Equipo</th>
                                                                                    <th>Item</th>
                                                                                    <th>Descripción Item</th>
                                                                                    <th>Unidad</th>
                                                                                    <th>Cantidad requerida</th>
                                                                                    <th>Opciones</th>
                                                                                </tr>
                                                                                <tr ng-repeat="material in formData.materiales">
                                                                                    <td>{{($index+1)}}</td></td>
                                                                                    <td><span >{{material.codigo}}</span></td>                                                                                    
                                                                                    <td><span >{{material.item}}</span></td>                                                                                    
                                                                                    <td><span >{{material.descripcion_item}}</span></td>
                                                                                    <td><span >{{material.unidad}}</span></td>
																					
                                                                                    <td>
																					<span ng-show="material.edit == 0">{{material.cantidad}}</span>

                                                                                    <input type="text" min="1" class="form-control" id="cantidad{{$index}}" name="cantidad{{$index}}" 

                                                                                            value="{{material.cantidad}}" 

                                                                                            ng-show="material.edit > 0">
																					</td>
                                                                                    <td>
																					 <button type="button" ng-click="editRowMaterial(material , $index)" ng-show="material.edit == 0" class="btn red-mint">

                                                                                        <i class="fa fa-pencil"></i>

                                                                                    </button>

                                                                                    <button type="button" ng-click="saveRowMaterial(material , $index)" ng-show="material.edit > 0" class="btn blue">

                                                                                        <i class="fa fa-save"></i>

                                                                                    </button>
																					<button type="button" ng-click="deleteMaterial(material , $index)" class="btn red-mint">

                                                                                        <i class="fa fa-close"></i>

                                                                                    </button></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    </div>
                                                                </fieldset>
                                                                <br><br>
																
                                                                <fieldset class="scheduler-border">
                                                                    <legend class="scheduler-border">Repuestos</legend>
                                                                    
                                                                    <div class="form-group">
                                                                        <label class="col-md-2 control-label">Equipo :
                                                                            <span class="required"> * </span>
                                                                        </label>
                                                                        <div class="col-md-10">
																		<input id="equiposValR" ng-model="equiposValR" ng-change="changeEquipo()" class="table-group-action-input form-control" list="listEquipoR" type="" />
																		<datalist id="listEquipoR">
																			<option ng-repeat="equipo in formData.equiposVal" value="{{equipo.dos}}"></option>
																		</datalist>
																		<!--
                                                                            <select class="table-group-action-input form-control" name="materiales2" id="materiales2">
                                                                                    <option value="">Seleccione...</option>
                                                                                    <option ng-repeat="equipo in formData.materiales2" 

                                                                                                value="{{equipo.id}}">

                                                                                                {{equipo.item}}

                                                                                </option>
                                                                                </select>-->
                                                                        </div>
                                                                    </div>
																	<div class="form-group">
                                                                        <label class="col-md-2 control-label">Parte :
                                                                            <span class="required"> * </span>
                                                                        </label>
                                                                        <div class="col-md-10">
																		<input  ng-disabled="validParte == 0" ng-model="equiposValParte" ng-change="changeParte()" id="parte" class="table-group-action-input form-control" list="listparte" type="" />
																		<datalist id="listparte">
																			<option ng-repeat="equipo in formData.parte" value="{{equipo.nombre}}"></option>
																		</datalist>
																		<!--
                                                                            <select class="table-group-action-input form-control" name="materiales2" id="materiales2">
                                                                                    <option value="">Seleccione...</option>
                                                                                    <option ng-repeat="equipo in formData.materiales2" 

                                                                                                value="{{equipo.id}}">

                                                                                                {{equipo.item}}

                                                                                </option>
                                                                                </select>-->
                                                                        </div>
                                                                    </div>
																	<div class="form-group">
                                                                        <label class="col-md-2 control-label">Item :
                                                                            <span class="required"> * </span>
                                                                        </label>
                                                                        <div class="col-md-10">
																		<input  ng-disabled="validItems == 0" id="repuestos" class="table-group-action-input form-control" list="listrepuestos" type="" />
																		<datalist id="listrepuestos">
																			<option ng-repeat="equipo in formData.repuestos" value="{{equipo.descripcion}}"></option>
																		</datalist>
																		<!--
                                                                            <select class="table-group-action-input form-control" name="materiales2" id="materiales2">
                                                                                    <option value="">Seleccione...</option>
                                                                                    <option ng-repeat="equipo in formData.materiales2" 

                                                                                                value="{{equipo.id}}">

                                                                                                {{equipo.item}}

                                                                                </option>
                                                                                </select>-->
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    <div class="form-group">
                                                                        <label class="col-md-2 control-label">Cantidad requerida :
                                                                            <span class="required"> * </span>
                                                                        </label>
                                                                        <div class="col-md-4">
                                                                            <div class="input-group">
                                                                                <input type="text" id="cantidadRep"/>
                                                                                <div class="input-group-btn">
                                                                                    <button class="btn btn-success" ng-click="addRowRepuestos()" type="button">
                                                                                        <i class="fa fa-plus"></i> Agregar</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="col-md-8 col-md-offset-2">
                                                                            <table class="table table-striped table-bordered table-hover table-checkable">
                                                                                <tr>
                                                                                    <th>#</th>
																					<th>Codigo de Equipo</th>
                                                                                    <th>Parte</th>
                                                                                    <th>Descripción</th>
                                                                                    <th>Item</th>
                                                                                    <th>Cantidad</th>
                                                                                    <th>Opciones</th>
                                                                                </tr>
                                                                                <tr ng-repeat="repuesto in formData.tableRepuesto">
                                                                                    <td>{{($index+1)}}</td></td>
                                                                                    <td><span >{{repuesto.codigo}}</span></td>                                                                                    
                                                                                    <td><span >{{repuesto.parte}}</span></td>                                                                                  
                                                                                    <td><span >{{repuesto.descripcion}}</span></td>                                                                                  
                                                                                    <td><span >{{repuesto.item}}</span></td>																					
                                                                                    <td>
																					<span ng-show="repuesto.edit == 0">{{repuesto.cantidad}}</span>

                                                                                    <input type="text" min="1" class="form-control" id="repuesto{{$index}}" name="repuesto{{$index}}" 

                                                                                            value="{{repuesto.cantidad}}" 

                                                                                            ng-show="repuesto.edit > 0">
																					</td>
                                                                                    <td>
																					 <button type="button" ng-click="editRowRepuesto(repuesto , $index)" ng-show="repuesto.edit == 0" class="btn red-mint">

                                                                                        <i class="fa fa-pencil"></i>

                                                                                    </button>

                                                                                    <button type="button" ng-click="saveRowRepuesto(repuesto , $index)" ng-show="repuesto.edit > 0" class="btn blue">

                                                                                        <i class="fa fa-save"></i>

                                                                                    </button>
																					<button type="button" ng-click="deleteRepuesto(repuesto , $index)" class="btn red-mint">

                                                                                        <i class="fa fa-close"></i>

                                                                                    </button></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </fieldset>
                                                                <br><br>
																<fieldset class="scheduler-border">
                                                                    <legend class="scheduler-border">Herramientas</legend>
                                                                    
                                                                    <div class="form-group">
                                                                        <label class="col-md-2 control-label">Herramienta de trabajo :
                                                                            <span class="required"> * </span>
                                                                        </label>
                                                                        <div class="col-md-4">
                                                                            <div class="input-group">
																				<input id="herramientas" class="table-group-action-input form-control" list="listherramientas" type="" />
																				<datalist id="listherramientas">
																					<option ng-repeat="equipo in formData.herramientas2" value="{{equipo.herramienta}}"></option>
																				</datalist>
                                                                                                                                                                <div class="input-group-btn">
                                                                                    <button class="btn btn-success" ng-click="addRowHerramientas()" type="button">
                                                                                        <i class="fa fa-plus"></i> Agregar</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="col-md-8 col-md-offset-2">
                                                                            <table class="table table-striped table-bordered table-hover table-checkable">
                                                                                <tr>
                                                                                    <th>#</th>
                                                                                    <th>Herramienta de Trabajo</th>
                                                                                    <th>Especificación</th>
                                                                                    <th>Opciones</th>
                                                                                </tr>
                                                                                <tr ng-repeat="herramienta in formData.herramientas">
                                                                                    <td>{{($index+1)}}</td>
                                                                                    <td>
																					<span ng-show="herramienta.edit == 0">{{herramienta.herramienta}}</span>
																		
                                                                                    
																					</td>
                                                                                    <td>
																					<span ng-show="herramienta.edit == 0">{{herramienta.requerimientos}}</span>

                                                                                    
																					</td>
                                                                                    <td>
																					<button type="button" ng-click="deleteHerramienta(herramienta , $index)" class="btn red-mint">

                                                                                        <i class="fa fa-close"></i>

                                                                                    </button></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </fieldset>
                                                                <br><br>
																<fieldset class="scheduler-border">
                                                                    <legend class="scheduler-border">Detalle del trabajo</legend>
                                                                <div class="form-group">
                                                                    
                                                                    <div class="col-md-12">
                                                                        <textarea ng-model="formData.observaciones" id="summernote_1" name="summernote_1"></textarea>
                                                                    </div>
                                                                </div>
																 </fieldset>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->
            <a href="javascript:;" class="page-quick-sidebar-toggler">
                <i class="icon-login"></i>
            </a>
            <div class="page-quick-sidebar-wrapper" data-close-on-body-click="false">
                <div class="page-quick-sidebar">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="javascript:;" data-target="#quick_sidebar_tab_1" data-toggle="tab"> Users
                                <span class="badge badge-danger">2</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" data-target="#quick_sidebar_tab_2" data-toggle="tab"> Alerts
                                <span class="badge badge-success">7</span>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> More
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="javascript:;" data-target="#quick_sidebar_tab_3" data-toggle="tab">
                                        <i class="icon-bell"></i> Alerts </a>
                                </li>
                                <li>
                                    <a href="javascript:;" data-target="#quick_sidebar_tab_3" data-toggle="tab">
                                        <i class="icon-info"></i> Notifications </a>
                                </li>
                                <li>
                                    <a href="javascript:;" data-target="#quick_sidebar_tab_3" data-toggle="tab">
                                        <i class="icon-speech"></i> Activities </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="javascript:;" data-target="#quick_sidebar_tab_3" data-toggle="tab">
                                        <i class="icon-settings"></i> Settings </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active page-quick-sidebar-chat" id="quick_sidebar_tab_1">
                            <div class="page-quick-sidebar-chat-users" data-rail-color="#ddd" data-wrapper-class="page-quick-sidebar-list">
                                <h3 class="list-heading">Staff</h3>
                                <ul class="media-list list-items">
                                    <li class="media">
                                        <div class="media-status">
                                            <span class="badge badge-success">8</span>
                                        </div>
                                        <img class="media-object" src="<?=$ruta?>/assets/layouts/layout/img/avatar3.jpg" alt="...">
                                        <div class="media-body">
                                            <h4 class="media-heading">Bob Nilson</h4>
                                            <div class="media-heading-sub"> Project Manager </div>
                                        </div>
                                    </li>
                                    <li class="media">
                                        <img class="media-object" src="<?=$ruta?>/assets/layouts/layout/img/avatar1.jpg" alt="...">
                                        <div class="media-body">
                                            <h4 class="media-heading">Nick Larson</h4>
                                            <div class="media-heading-sub"> Art Director </div>
                                        </div>
                                    </li>
                                    <li class="media">
                                        <div class="media-status">
                                            <span class="badge badge-danger">3</span>
                                        </div>
                                        <img class="media-object" src="<?=$ruta?>/assets/layouts/layout/img/avatar4.jpg" alt="...">
                                        <div class="media-body">
                                            <h4 class="media-heading">Deon Hubert</h4>
                                            <div class="media-heading-sub"> CTO </div>
                                        </div>
                                    </li>
                                    <li class="media">
                                        <img class="media-object" src="<?=$ruta?>/assets/layouts/layout/img/avatar2.jpg" alt="...">
                                        <div class="media-body">
                                            <h4 class="media-heading">Ella Wong</h4>
                                            <div class="media-heading-sub"> CEO </div>
                                        </div>
                                    </li>
                                </ul>
                                <h3 class="list-heading">Customers</h3>
                                <ul class="media-list list-items">
                                    <li class="media">
                                        <div class="media-status">
                                            <span class="badge badge-warning">2</span>
                                        </div>
                                        <img class="media-object" src="<?=$ruta?>/assets/layouts/layout/img/avatar6.jpg" alt="...">
                                        <div class="media-body">
                                            <h4 class="media-heading">Lara Kunis</h4>
                                            <div class="media-heading-sub"> CEO, Loop Inc </div>
                                            <div class="media-heading-small"> Last seen 03:10 AM </div>
                                        </div>
                                    </li>
                                    <li class="media">
                                        <div class="media-status">
                                            <span class="label label-sm label-success">new</span>
                                        </div>
                                        <img class="media-object" src="<?=$ruta?>/assets/layouts/layout/img/avatar7.jpg" alt="...">
                                        <div class="media-body">
                                            <h4 class="media-heading">Ernie Kyllonen</h4>
                                            <div class="media-heading-sub"> Project Manager,
                                                <br> SmartBizz PTL </div>
                                        </div>
                                    </li>
                                    <li class="media">
                                        <img class="media-object" src="<?=$ruta?>/assets/layouts/layout/img/avatar8.jpg" alt="...">
                                        <div class="media-body">
                                            <h4 class="media-heading">Lisa Stone</h4>
                                            <div class="media-heading-sub"> CTO, Keort Inc </div>
                                            <div class="media-heading-small"> Last seen 13:10 PM </div>
                                        </div>
                                    </li>
                                    <li class="media">
                                        <div class="media-status">
                                            <span class="badge badge-success">7</span>
                                        </div>
                                        <img class="media-object" src="<?=$ruta?>/assets/layouts/layout/img/avatar9.jpg" alt="...">
                                        <div class="media-body">
                                            <h4 class="media-heading">Deon Portalatin</h4>
                                            <div class="media-heading-sub"> CFO, H&D LTD </div>
                                        </div>
                                    </li>
                                    <li class="media">
                                        <img class="media-object" src="<?=$ruta?>/assets/layouts/layout/img/avatar10.jpg" alt="...">
                                        <div class="media-body">
                                            <h4 class="media-heading">Irina Savikova</h4>
                                            <div class="media-heading-sub"> CEO, Tizda Motors Inc </div>
                                        </div>
                                    </li>
                                    <li class="media">
                                        <div class="media-status">
                                            <span class="badge badge-danger">4</span>
                                        </div>
                                        <img class="media-object" src="<?=$ruta?>/assets/layouts/layout/img/avatar11.jpg" alt="...">
                                        <div class="media-body">
                                            <h4 class="media-heading">Maria Gomez</h4>
                                            <div class="media-heading-sub"> Manager, Infomatic Inc </div>
                                            <div class="media-heading-small"> Last seen 03:10 AM </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="page-quick-sidebar-item">
                                <div class="page-quick-sidebar-chat-user">
                                    <div class="page-quick-sidebar-nav">
                                        <a href="javascript:;" class="page-quick-sidebar-back-to-list">
                                            <i class="icon-arrow-left"></i>Back</a>
                                    </div>
                                    <div class="page-quick-sidebar-chat-user-messages">
                                        <div class="post out">
                                            <img class="avatar" alt="" src="<?=$ruta?>/assets/layouts/layout/img/avatar3.jpg" />
                                            <div class="message">
                                                <span class="arrow"></span>
                                                <a href="javascript:;" class="name">Bob Nilson</a>
                                                <span class="datetime">20:15</span>
                                                <span class="body"> When could you send me the report ? </span>
                                            </div>
                                        </div>
                                        <div class="post in">
                                            <img class="avatar" alt="" src="<?=$ruta?>/assets/layouts/layout/img/avatar2.jpg" />
                                            <div class="message">
                                                <span class="arrow"></span>
                                                <a href="javascript:;" class="name">Ella Wong</a>
                                                <span class="datetime">20:15</span>
                                                <span class="body"> Its almost done. I will be sending it shortly </span>
                                            </div>
                                        </div>
                                        <div class="post out">
                                            <img class="avatar" alt="" src="<?=$ruta?>/assets/layouts/layout/img/avatar3.jpg" />
                                            <div class="message">
                                                <span class="arrow"></span>
                                                <a href="javascript:;" class="name">Bob Nilson</a>
                                                <span class="datetime">20:15</span>
                                                <span class="body"> Alright. Thanks! :) </span>
                                            </div>
                                        </div>
                                        <div class="post in">
                                            <img class="avatar" alt="" src="<?=$ruta?>/assets/layouts/layout/img/avatar2.jpg" />
                                            <div class="message">
                                                <span class="arrow"></span>
                                                <a href="javascript:;" class="name">Ella Wong</a>
                                                <span class="datetime">20:16</span>
                                                <span class="body"> You are most welcome. Sorry for the delay. </span>
                                            </div>
                                        </div>
                                        <div class="post out">
                                            <img class="avatar" alt="" src="<?=$ruta?>/assets/layouts/layout/img/avatar3.jpg" />
                                            <div class="message">
                                                <span class="arrow"></span>
                                                <a href="javascript:;" class="name">Bob Nilson</a>
                                                <span class="datetime">20:17</span>
                                                <span class="body"> No probs. Just take your time :) </span>
                                            </div>
                                        </div>
                                        <div class="post in">
                                            <img class="avatar" alt="" src="<?=$ruta?>/assets/layouts/layout/img/avatar2.jpg" />
                                            <div class="message">
                                                <span class="arrow"></span>
                                                <a href="javascript:;" class="name">Ella Wong</a>
                                                <span class="datetime">20:40</span>
                                                <span class="body"> Alright. I just emailed it to you. </span>
                                            </div>
                                        </div>
                                        <div class="post out">
                                            <img class="avatar" alt="" src="<?=$ruta?>/assets/layouts/layout/img/avatar3.jpg" />
                                            <div class="message">
                                                <span class="arrow"></span>
                                                <a href="javascript:;" class="name">Bob Nilson</a>
                                                <span class="datetime">20:17</span>
                                                <span class="body"> Great! Thanks. Will check it right away. </span>
                                            </div>
                                        </div>
                                        <div class="post in">
                                            <img class="avatar" alt="" src="<?=$ruta?>/assets/layouts/layout/img/avatar2.jpg" />
                                            <div class="message">
                                                <span class="arrow"></span>
                                                <a href="javascript:;" class="name">Ella Wong</a>
                                                <span class="datetime">20:40</span>
                                                <span class="body"> Please let me know if you have any comment. </span>
                                            </div>
                                        </div>
                                        <div class="post out">
                                            <img class="avatar" alt="" src="<?=$ruta?>/assets/layouts/layout/img/avatar3.jpg" />
                                            <div class="message">
                                                <span class="arrow"></span>
                                                <a href="javascript:;" class="name">Bob Nilson</a>
                                                <span class="datetime">20:17</span>
                                                <span class="body"> Sure. I will check and buzz you if anything needs to be corrected. </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="page-quick-sidebar-chat-user-form">
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Type a message here...">
                                            <div class="input-group-btn">
                                                <button type="button" class="btn green">
                                                    <i class="icon-paper-clip"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane page-quick-sidebar-alerts" id="quick_sidebar_tab_2">
                            <div class="page-quick-sidebar-alerts-list">
                                <h3 class="list-heading">General</h3>
                                <ul class="feeds list-items">
                                    <li>
                                        <div class="col1">
                                            <div class="cont">
                                                <div class="cont-col1">
                                                    <div class="label label-sm label-info">
                                                        <i class="fa fa-check"></i>
                                                    </div>
                                                </div>
                                                <div class="cont-col2">
                                                    <div class="desc"> You have 4 pending tasks.
                                                        <span class="label label-sm label-warning "> Take action
                                                            <i class="fa fa-share"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col2">
                                            <div class="date"> Just now </div>
                                        </div>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <div class="col1">
                                                <div class="cont">
                                                    <div class="cont-col1">
                                                        <div class="label label-sm label-success">
                                                            <i class="fa fa-bar-chart-o"></i>
                                                        </div>
                                                    </div>
                                                    <div class="cont-col2">
                                                        <div class="desc"> Finance Report for year 2013 has been released. </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col2">
                                                <div class="date"> 20 mins </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <div class="col1">
                                            <div class="cont">
                                                <div class="cont-col1">
                                                    <div class="label label-sm label-danger">
                                                        <i class="fa fa-user"></i>
                                                    </div>
                                                </div>
                                                <div class="cont-col2">
                                                    <div class="desc"> You have 5 pending membership that requires a quick review. </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col2">
                                            <div class="date"> 24 mins </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="col1">
                                            <div class="cont">
                                                <div class="cont-col1">
                                                    <div class="label label-sm label-info">
                                                        <i class="fa fa-shopping-cart"></i>
                                                    </div>
                                                </div>
                                                <div class="cont-col2">
                                                    <div class="desc"> New order received with
                                                        <span class="label label-sm label-success"> Reference Number: DR23923 </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col2">
                                            <div class="date"> 30 mins </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="col1">
                                            <div class="cont">
                                                <div class="cont-col1">
                                                    <div class="label label-sm label-success">
                                                        <i class="fa fa-user"></i>
                                                    </div>
                                                </div>
                                                <div class="cont-col2">
                                                    <div class="desc"> You have 5 pending membership that requires a quick review. </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col2">
                                            <div class="date"> 24 mins </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="col1">
                                            <div class="cont">
                                                <div class="cont-col1">
                                                    <div class="label label-sm label-info">
                                                        <i class="fa fa-bell-o"></i>
                                                    </div>
                                                </div>
                                                <div class="cont-col2">
                                                    <div class="desc"> Web server hardware needs to be upgraded.
                                                        <span class="label label-sm label-warning"> Overdue </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col2">
                                            <div class="date"> 2 hours </div>
                                        </div>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <div class="col1">
                                                <div class="cont">
                                                    <div class="cont-col1">
                                                        <div class="label label-sm label-default">
                                                            <i class="fa fa-briefcase"></i>
                                                        </div>
                                                    </div>
                                                    <div class="cont-col2">
                                                        <div class="desc"> IPO Report for year 2013 has been released. </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col2">
                                                <div class="date"> 20 mins </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                                <h3 class="list-heading">System</h3>
                                <ul class="feeds list-items">
                                    <li>
                                        <div class="col1">
                                            <div class="cont">
                                                <div class="cont-col1">
                                                    <div class="label label-sm label-info">
                                                        <i class="fa fa-check"></i>
                                                    </div>
                                                </div>
                                                <div class="cont-col2">
                                                    <div class="desc"> You have 4 pending tasks.
                                                        <span class="label label-sm label-warning "> Take action
                                                            <i class="fa fa-share"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col2">
                                            <div class="date"> Just now </div>
                                        </div>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <div class="col1">
                                                <div class="cont">
                                                    <div class="cont-col1">
                                                        <div class="label label-sm label-danger">
                                                            <i class="fa fa-bar-chart-o"></i>
                                                        </div>
                                                    </div>
                                                    <div class="cont-col2">
                                                        <div class="desc"> Finance Report for year 2013 has been released. </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col2">
                                                <div class="date"> 20 mins </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <div class="col1">
                                            <div class="cont">
                                                <div class="cont-col1">
                                                    <div class="label label-sm label-default">
                                                        <i class="fa fa-user"></i>
                                                    </div>
                                                </div>
                                                <div class="cont-col2">
                                                    <div class="desc"> You have 5 pending membership that requires a quick review. </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col2">
                                            <div class="date"> 24 mins </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="col1">
                                            <div class="cont">
                                                <div class="cont-col1">
                                                    <div class="label label-sm label-info">
                                                        <i class="fa fa-shopping-cart"></i>
                                                    </div>
                                                </div>
                                                <div class="cont-col2">
                                                    <div class="desc"> New order received with
                                                        <span class="label label-sm label-success"> Reference Number: DR23923 </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col2">
                                            <div class="date"> 30 mins </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="col1">
                                            <div class="cont">
                                                <div class="cont-col1">
                                                    <div class="label label-sm label-success">
                                                        <i class="fa fa-user"></i>
                                                    </div>
                                                </div>
                                                <div class="cont-col2">
                                                    <div class="desc"> You have 5 pending membership that requires a quick review. </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col2">
                                            <div class="date"> 24 mins </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="col1">
                                            <div class="cont">
                                                <div class="cont-col1">
                                                    <div class="label label-sm label-warning">
                                                        <i class="fa fa-bell-o"></i>
                                                    </div>
                                                </div>
                                                <div class="cont-col2">
                                                    <div class="desc"> Web server hardware needs to be upgraded.
                                                        <span class="label label-sm label-default "> Overdue </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col2">
                                            <div class="date"> 2 hours </div>
                                        </div>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <div class="col1">
                                                <div class="cont">
                                                    <div class="cont-col1">
                                                        <div class="label label-sm label-info">
                                                            <i class="fa fa-briefcase"></i>
                                                        </div>
                                                    </div>
                                                    <div class="cont-col2">
                                                        <div class="desc"> IPO Report for year 2013 has been released. </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col2">
                                                <div class="date"> 20 mins </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane page-quick-sidebar-settings" id="quick_sidebar_tab_3">
                            <div class="page-quick-sidebar-settings-list">
                                <h3 class="list-heading">General Settings</h3>
                                <ul class="list-items borderless">
                                    <li> Enable Notifications
                                        <input type="checkbox" class="make-switch" checked data-size="small" data-on-color="success" data-on-text="ON" data-off-color="default" data-off-text="OFF"> </li>
                                    <li> Allow Tracking
                                        <input type="checkbox" class="make-switch" data-size="small" data-on-color="info" data-on-text="ON" data-off-color="default" data-off-text="OFF"> </li>
                                    <li> Log Errors
                                        <input type="checkbox" class="make-switch" checked data-size="small" data-on-color="danger" data-on-text="ON" data-off-color="default" data-off-text="OFF"> </li>
                                    <li> Auto Sumbit Issues
                                        <input type="checkbox" class="make-switch" data-size="small" data-on-color="warning" data-on-text="ON" data-off-color="default" data-off-text="OFF"> </li>
                                    <li> Enable SMS Alerts
                                        <input type="checkbox" class="make-switch" checked data-size="small" data-on-color="success" data-on-text="ON" data-off-color="default" data-off-text="OFF"> </li>
                                </ul>
                                <h3 class="list-heading">System Settings</h3>
                                <ul class="list-items borderless">
                                    <li> Security Level
                                        <select class="form-control input-inline input-sm input-small">
                                            <option value="1">Normal</option>
                                            <option value="2" selected>Medium</option>
                                            <option value="e">High</option>
                                        </select>
                                    </li>
                                    <li> Failed Email Attempts
                                        <input class="form-control input-inline input-sm input-small" value="5" /> </li>
                                    <li> Secondary SMTP Port
                                        <input class="form-control input-inline input-sm input-small" value="3560" /> </li>
                                    <li> Notify On System Error
                                        <input type="checkbox" class="make-switch" checked data-size="small" data-on-color="danger" data-on-text="ON" data-off-color="default" data-off-text="OFF"> </li>
                                    <li> Notify On SMTP Error
                                        <input type="checkbox" class="make-switch" checked data-size="small" data-on-color="warning" data-on-text="ON" data-off-color="default" data-off-text="OFF"> </li>
                                </ul>
                                <div class="inner-content">
                                    <button class="btn btn-success">
                                        <i class="icon-settings"></i> Save Changes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END QUICK SIDEBAR -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        