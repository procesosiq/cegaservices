<link href="http://cdn.procesos-iq.com/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />

<link href="http://cdn.procesos-iq.com/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
<script>
<?php if(isset($_GET['pdf']) || $_GET['pdf'] > 0) { ?>
    window.onload = genPDFV;
<?php } ?>
function genPDF(){
	<?php if(isset($_GET['id']) || $_GET['id'] > 0) { ?>
	location.href="http://cegaservices2.procesos-iq.com/pdfCotizacion.php?id=<?php echo $_GET['id']?>";
	<? } else { ?>
	alert("Cotizacion no guardada.");
	<? } ?>
	
}
function genPDFV(){
	<?php if(isset($_GET['id']) || $_GET['id'] > 0) { ?>
	$(".page-header.navbar.navbar-fixed-top").css({display:"none"});
		$(".page-sidebar-wrapper").css({display:"none"});
		$(".page-content").css({"margin-left":"0px"});
		$("input").attr("disabled" , "disabled");
		$("select").attr("disabled" , "disabled");
		$(".dt-buttons").css("display" , "none");
		$("textarea").attr("disabled" , "disabled");
		$(".detailsDelete").css("display" , "none");
		$("#headTable").css("display" , "none");
        $(".btnSave").css("display" , "none");
        $(".btnCotizador").css("display" , "none");
		$(".btn").css("display" , "none");
		var htmlObservaciones = $("#summernote_1").code();
		$(".obser").empty().html(htmlObservaciones);
		var texto = $("#summernote_2").code();
		$(".textfire").empty().html(texto);
		html2canvas(document.getElementById("cont"), {
            onrendered: function(canvas) {     
				var imgData = canvas.toDataURL('image/png');
                
                var doc = new jsPDF(); //210mm wide and 297mm high
                
                doc.addImage(imgData, 'JPEG', 0, 0,205,297);
				doc.save('Cotización-Vista-<?php echo$_GET['id'] ?>.pdf');
				<?php if(isset($_GET['pdf']) || $_GET['pdf'] > 0) { ?>
				location.href="http://cegaservices2.procesos-iq.com/cotizacionList";
				<?php } else {?>
				location.reload();
				<?php } ?>
            }
        });
	<? } else { ?>
	alert("Cotizacion no guardada.");
	<? } ?>
	
}
function genExcel(){
    window.open('data:application/vnd.ms-excel,' + $('#cont').html());
    e.preventDefault();
}
</script>
<style type="text/css" media="screen">

    @page {
      size: A4;
    }
    .textoFire{

        color: rgb(34, 34, 34); 

        font-family: arial, sans-serif; 

        font-size: 10px; 

        line-height: normal;

    }

    .borderBox {

        border: solid 1px;

        padding: 5px;

    }



    #ajax-modal {

        top:5%;

        right:50%;

        outline: none;

    }
	#ajax-modal2 {

        top:5%;

        right:50%;

        outline: none;

    }
	#ajax-modal3 {

        top:5%;

        right:50%;

        outline: none;

    }
	
	.elec{
		background-color: white;
		color:white;
	}
	
	.elec:hover{
		background-color: #7777d6;;
		color:white;
	}

</style>

            <div id="cont" class="page-content-wrapper" ng-app="app" ng-controller="cotizacionPruebas" ng-cloak>

                <!-- BEGIN CONTENT BODY -->

                <div class="page-content" ng-init="init()">

                    <!-- BEGIN PAGE HEAD-->

                    <div class="page-head">

                        <!-- BEGIN PAGE TITLE -->

                        <div class="page-title">

                            <h1>COTIZACIONES</h1>

                        </div>

                        <!-- END PAGE TITLE -->

                    </div>

                    <!-- END PAGE HEAD-->

                    <!-- BEGIN PAGE BREADCRUMB -->

                    <ul class="page-breadcrumb breadcrumb">

                        <li>

                            <a href="/cotizacionList">Lista de cotizaciones</a>

                            <i class="fa fa-circle"></i>

                        </li>

                        <li>

                            <span class="active">Registro de cotizaciones</span>

                        </li>

                    </ul>

                    <!-- END PAGE BREADCRUMB -->

                    <div class="row">

                        <div class="col-md-12">

                            <!-- Begin: life time stats -->

                            <div class="portlet light portlet-fit portlet-datatable bordered">

                                <div class="portlet-title">

                                    <div class="caption">

                                        <i class="icon-settings font-dark"></i>

                                        <span class="caption-subject font-dark sbold uppercase"> HACER COTIZACIÓN</span>

                                    </div>

                                    <div class="actions">

                                        <!-- <button type="buttton" ng-click="saveCotizacion()" class="btn green-jungle">

                                            Guardar Cotizacion

                                        </button> -->

                                       <div class="dt-buttons">

                                            <a class="dt-button buttons-print btn dark btn-outline" ng-click="printCotizacion()" tabindex="0" aria-controls="datatable_ajax">

                                                <span>Print</span>

                                            </a>

                                            <a class="dt-button buttons-pdf buttons-html5 btn green btn-outline" href="javascript:genPDF()" tabindex="0" aria-controls="datatable_ajax">

                                                <span>PDF</span>

                                            </a>
											<a class="dt-button buttons-pdf buttons-html5 btn green btn-outline" href="javascript:genPDFV()" tabindex="0" aria-controls="datatable_ajax">

                                                <span>PDF<i class="fa fa-check" aria-hidden="true"></i></span>

                                            </a>

                                            <a class="dt-button buttons-excel buttons-html5 btn yellow btn-outline" href="" tabindex="0" aria-controls="datatable_ajax">

                                                <span>Excel</span>

                                            </a>

                                            <a class="dt-button buttons-csv buttons-html5 btn purple btn-outline" tabindex="0" aria-controls="datatable_ajax">

                                                <span>CSV</span>

                                            </a>

											<?php if(isset($_GET['id']) || $_GET['id'] > 0) { ?>
											<a class="dt-button btn blue btn-outline btnSave" ng-click="copiaCotizacion(<?php echo $_GET['id']?>)"  tabindex="0" aria-controls="datatable_ajax">

												<span> Generar copia</span>

											</a>
											<? } ?>
                                            
											<a class="dt-button btn blue btn-outline btnSave" ng-click="saveCotizacion()"  tabindex="0" aria-controls="datatable_ajax">

                                                <span> Guardar Cotización</span>

                                            </a>

                                        </div>

                                    </div>

                                </div>
                                <div id="contenedor" class="portlet-body">

                                    <div class="tabbable-line">

                                        <ul class="nav nav-tabs nav-tabs-lg">

                                            <li class="active">

                                                <a href="#tab_1" data-toggle="tab" style="display: none;"> Detalles </a>

                                            </li>

                                        </ul>

                                        <div class="tab-content">

                                            <div class="tab-pane active" id="tab_1">

                                                <div class="row">

                                                    <div class="col-md-6 col-sm-12">

                                                        <div class="portlet yellow-crusta box">

                                                            <div class="portlet-title">

                                                                <div class="caption">

                                                                    <i class="fa fa-cogs"></i>Detalle de la Cotización </div>

                                                                <div class="actions">

                                                                    

                                                                </div>

                                                            </div>

                                                            <div class="portlet-body">

                                                                <div class="row static-info">

                                                                    <div class="col-md-5 name"> Cotización #: </div>

                                                                    <div class="col-md-7 value"> <span id="folio">{{cotizacion.folio}}</span>

                                                                        <button ng-click="sendMailConfirmation()" class="btn btn-small label label-info label-sm" ng-disabled="cotizacion.id == 0"> Enviar Correo de Confirmacion </button>

                                                                    </div>

                                                                </div>

                                                                <div class="row static-info">

                                                                    <div class="col-md-5 name"> Fecha y hora: </div>

                                                                    <div class="col-md-7 value"> {{cotizacion.fecha}} </div>

                                                                </div>

                                                                <div class="row static-info">

                                                                    <div class="col-md-5 name"> Estado de la Cotización: </div>

                                                                    <div class="col-md-7 value">

                                                                        <span class="font-red-mint"> {{cotizacion.status}} </span>

                                                                    </div>

                                                                </div>

                                                                <div class="row static-info">

                                                                    <div class="col-md-5 name"> Gran Total: </div>

                                                                    <div class="col-md-7 value"> {{cotizacion.total | currency}} </div>

                                                                </div>

                                                                <div class="row static-info">

                                                                    <div class="col-md-5 name"> Forma de Pago: </div>

                                                                    <div class="col-md-7 value"> 

                                                                     <select name="forma_pago" class="form-control" ng-model="cotizacion.forma_pago" id="forma_pago" ng-disabled="cotizacion.id_cliente == 0">

                                                                            <option ng-repeat="(key , value) in options.forma_pago" 

                                                                                    ng-selected="value == cotizacion.forma_pago" 

                                                                                    value="{{value}}">

                                                                                    {{value}}

                                                                            </option>

                                                                        </select>

                                                                    </div>

                                                                </div>
																<div class="row static-info" ng-show="cotizacion.forma_pago == 'Credito'">

                                                                    <div class="col-md-5 name"> Abonos: </div>

                                                                    <div class="col-md-7 value"> 

                                                                     <select name="abonos" class="form-control" ng-model="cotizacion.abonos" id="abonos" ng-disabled="cotizacion.id_cliente == 0">

                                                                            <option ng-repeat="(key , value) in options.abonos" 

                                                                                    ng-selected="value == cotizacion.abonos" 

                                                                                    value="{{value}}">

                                                                                    {{value}}

                                                                            </option>

                                                                        </select>

                                                                    </div>

                                                                </div>
																<div class="row static-info" ng-show="cotizacion.forma_pago == 'Credito'">

                                                                    <div class="col-md-5 name"> Tiempo: </div>

                                                                    <div class="col-md-7 value"> 

                                                                     <select name="tiempo" class="form-control" ng-model="cotizacion.tiempo" id="tiempo" ng-disabled="cotizacion.id_cliente == 0">

                                                                            <option ng-repeat="(key , value) in options.tiempo" 

                                                                                    ng-selected="value == cotizacion.tiempo" 

                                                                                    value="{{value}}">

                                                                                    {{value}}

                                                                            </option>

                                                                        </select>

                                                                    </div>

                                                                </div>

                                                                <div class="row static-info">

                                                                    <div class="col-md-5 name"> Referencia: </div>

                                                                    <div class="col-md-7 value">
                                                                        <input type="text" class="form-control" id="referencia" name="referencia" value="" ng-model="cotizacion.referencia" ng-disabled="cotizacion.id_cliente == 0" placeholder="Referencia">
                                                                    </div>

                                                                </div>

                                                            </div>

                                                        </div>

                                                    </div>
                                                    <div class="col-md-6 col-sm-12">

                                                        <div class="portlet blue-hoki box">

                                                            <div class="portlet-title">

                                                                <div class="caption">

                                                                    <i class="fa fa-cogs"></i>Información del Cliente </div>

                                                                <div class="actions">

                                                                    <button class="btn c-btn-border-1x c-btn-blue-hoki" 

                                                                     type="button" id="nuevoCliente" ng-click="clearClient()" >Nuevo Cliente</button>

                                                                </div>

                                                            </div>

                                                            <div class="portlet-body">
																<?php if(!isset($_GET['id']) || $_GET['id'] < 1) { ?>
                                                                <div class="row static-info">

                                                                    <div class="col-md-5 name"> Filtrar por: </div>

                                                                    <div class="col-md-7"> 
																		<select class="form-control" name="filtro" id="filtro"
																		ng-change="filtro()"
																		ng-options="option.name for option in opciones.availableOptions track by option.id"
																		ng-model="opciones.selectedOption"></select>
																	</div>

                                                                </div>
																<?php } ?>
																<div class="row static-info">

                                                                    <div class="col-md-5 name"> RUC o C.I: </div>

                                                                    <div class="col-md-7"> <input type="text" placeholder="RUC o C.I" class="form-control" id="ruc" name="ruc" ng-keypress="getClient($event)" ng-model="cotizacion.ruc" value="" ng-disabled="cotizacion.id > 0"><small id="msj"></small> </div>

                                                                </div>

                                                                <div class="row static-info">

                                                                    <div class="col-md-5 name"> Tipo de cliente: </div>

                                                                    <div class="col-md-7"> 

                                                                        <select name="tipo_cliente" ng-model="cotizacion.tcliente" class="form-control formClass" id="tipo_cliente" disabled>

                                                                            <option ng-repeat="(key , value) in options.tipo_cliente" 

                                                                                    ng-selected="value.id == cotizacion.tcliente" 

                                                                                    value="{{value.id}}">

                                                                                    {{value.nombre}}

                                                                            </option>

                                                                        </select>

                                                                    </div>

                                                                </div>

                                                                <div class="row static-info">

                                                                    <div class="col-md-5 name"> Nombre del cliente: </div>

                                                                    <div class="col-md-7"> <input type="text" class="form-control"  ng-keypress="getClientNombre($event)" id="nombre_cliente" name="nombre_cliente" value="" ng-model="cotizacion.nombre_cliente" disabled readonly placeholder="Nombre del cliente"> </div>

                                                                </div>

                                                                <div class="row static-info">

                                                                    <div class="col-md-5 name"> Razón social: </div>

                                                                    <div class="col-md-7"> <input type="text" class="form-control formClass" id="razon_social" name="razon_social" value="" readonly ng-model="cotizacion.razon_social" placeholder="Razón social"> </div>

                                                                </div>

                                                                <div class="row static-info">

                                                                    <div class="col-md-5 name"> Correo: </div>

                                                                    <div class="col-md-7"> <input type="text" class="form-control formClass" id="email" name="email" value="" readonly ng-model="cotizacion.correo_contacto" placeholder="Correo"> </div>

                                                                </div>

                                                                <div class="row static-info">

                                                                    <div class="col-md-5 name"> Telefono: </div>

                                                                    <div class="col-md-7"> <input type="text" class="form-control formClass" id="tel" name="tel" value="" readonly ng-model="cotizacion.telefono" placeholder="Telefono"> </div>

                                                                </div>

                                                                <div class="row static-info">

                                                                    <div class="col-md-6 name"> </div>

                                                                    <div class="col-md-6"> 

                                                                        <div class="col-md-6"> 

                                                                            <button class="btn green-jungle" id="saveCliente" type="button" ng-click="save()" >

                                                                                Guardar

                                                                            </button> 

                                                                        </div>

                                                                        <div class="col-md-6"> 

                                                                            <button class="btn red-mint" id="cancelCliente" type="button" ng-click="cancel()" >

                                                                                Cancelar

                                                                            </button>

                                                                        </div>

                                                                    </div>

                                                                </div>

                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>
                                                <div class="row">

                                                    <div class="col-md-12 col-sm-12">

                                                        <div class="portlet grey-cascade box">

                                                            <div class="portlet-title">

                                                                <div class="caption">

                                                                    <i class="fa fa-cogs"></i>Lista de Productos y Servicios 
                                                                
                                                                </div>

                                                                <div class="actions">
                                                                    <a ng-if="cotizacion.id > 0" class="dt-button btn white btn-outline btnCotizador" ng-click="redirec()"  tabindex="0" aria-controls="datatable_ajax">
                                                                        <span> AÑADIR AL COTIZADOR </span>
                                                                    </a>
                                                                </div>
                                                            
                                                            </div>

                                                            <div class="portlet-body">

                                                                <div class="table-responsive">

                                                                    <table class="table table-hover table-bordered table-striped">

                                                                        <thead>

                                                                            <tr>

                                                                                <th width="15%"> Tipo de Trabajo </th>

                                                                                <th width="30%"> Descripción </th>
																				
                                                                                <th width="10%"> Unidad </th>

                                                                                <th width="6%"> Cantidad </th>
																				
                                                                                <th width="8%"> Descuento % </th>

                                                                                <th width="10%"> Precio </th>

                                                                                <th width="14%"> Total </th>

                                                                                <th width="7%" class="detailsDelete"> Acciones </th>

                                                                            </tr>

                                                                            <tr id="headTable">

                                                                                <td width="15%"> 

                                                                                    <select  onchange="validar()" ng-disabled="cotizacion.id_cliente == 0" name="tipo_trabajo" class="bg-grey-cascade bg-font-grey-cascade form-control" id="tipo_trabajo">

                                                                                        <option ng-repeat="tWork in options.tipo_trabajo" 

                                                                                                value="{{tWork.id}}" class="{{tWork.class}}">

                                                                                                {{tWork.name}}

                                                                                        </option>

                                                                                    </select>
																					
                                                                                </td>

                                                                                <td width="30%"> 

                                                                                    <input type="text" class="form-control" id="des" name="des" value="" placeholder="Descripción" ng-disabled="cotizacion.id_cliente == 0"> 

                                                                                </td>
																				
																				<td width="10%"> 

                                                                                    <input type="text" class="form-control" id="uni" name="uni" value="" placeholder="Unidad" ng-disabled="cotizacion.id_cliente == 0"> 

                                                                                </td>

                                                                                <td width="6%"> 

                                                                                    <input type="number" min="1" class="form-control countRow" id="count" name="count" value="" placeholder="Cantidad" ng-disabled="cotizacion.id_cliente == 0"> 

                                                                                </td>
                                                                                <td width="8%"> 

                                                                                    <input type="number" min="0" class="form-control countRow" id="desc" name="desc" value="0" placeholder="Cantidad" ng-disabled="cotizacion.id_cliente == 0"> 

                                                                                </td>

                                                                                <td width="10%"> 

                                                                                    <input type="number" min="1" class="form-control countRow" id="price" name="price" value="" placeholder="Precio" ng-disabled="cotizacion.id_cliente == 0"> 

                                                                                </td>

                                                                                <td width="14%"> 

                                                                                    <input type="number" class="form-control" id="total" name="total" value="" placeholder="Total" ng-disabled="0 == 0"> 

                                                                                </td>

                                                                                <td align="center" colspan="2" width="7%"> <button type="button" class="btn green-jungle" ng-click="addRow()"><i class="fa fa-plus-square-o"  aria-hidden="true" ng-disabled="cotizacion.id_cliente == 0"></i></button> </td>

                                                                            </tr>

                                                                        </thead>

                                                                        <tbody>

                                                                           <tr ng-repeat="details in cotizacion.details">

                                                                                <td style="vertical-align: middle !important;">

                                                                                    <span class="form-control label {{details.class_trabajo}}" ng-show="details.edit == 0">{{details.des_trabajo}}</span>

                                                                                     <select name="tipo_trabajo{{$index}}" class="{{details.class_trabajo}} form-control tipo_trabajo_details" ng-show="details.edit > 0" id="tipo_trabajo{{$index}}" ng-change="getDes($index)" ng-model="cotizacion.details[$index].tipo_trabajo">

                                                                                        <option ng-repeat="tWork in options.tipo_trabajo" 

                                                                                                value="{{tWork.id}}" ng-selected="tWork.id == details.tipo_trabajo" class="{{tWork.class}}">

                                                                                                {{tWork.name}}

                                                                                        </option>

                                                                                    </select>

                                                                                </td>

                                                                               <td style="vertical-align: middle !important;">

                                                                                    <span ng-show="details.edit == 0">{{details.des}}</span>

                                                                                    <input type="text" class="form-control" id="des{{$index}}" ng-model="cotizacion.details[$index].des" name="des{{$index}}" value="" ng-show="details.edit > 0"> 

                                                                                </td>
																				
																				<td style="vertical-align: middle !important;">

                                                                                    <span ng-show="details.edit == 0">{{details.uni}}</span>

                                                                                    <input type="text" class="form-control" id="uni{{$index}}" ng-model="cotizacion.details[$index].uni" name="uni{{$index}}" value="" ng-show="details.edit > 0"> 

                                                                                </td>

                                                                               <td style="vertical-align: middle !important;">

                                                                                    <span ng-show="details.edit == 0">{{details.count}}</span>

                                                                                    <input type="number" min="1" class="form-control" id="count{{$index}}" string-to-number ng-model="cotizacion.details[$index].count" name="count{{$index}}" value="" ng-show="details.edit > 0"> 

                                                                                </td>
																				
																				<td style="vertical-align: middle !important;">

                                                                                    <span ng-show="details.edit == 0">{{details.desc}} %</span>

                                                                                    <input type="number" min="0" class="form-control" id="desc{{$index}}" string-to-number ng-model="cotizacion.details[$index].desc" name="desc{{$index}}" value="" ng-show="details.edit > 0"> 

                                                                                </td>

                                                                               <td style="vertical-align: middle !important;">

                                                                                    <span ng-show="details.edit == 0">{{details.price | currency}}</span>

                                                                                    <input type="number" min="1" class="form-control" id="price{{$index}}" string-to-number ng-model="cotizacion.details[$index].price" name="price{{$index}}" value="" ng-show="details.edit > 0">

                                                                                </td>

                                                                               <td style="vertical-align: middle !important;">

                                                                                    <span ng-show="details.edit == 0">{{ getTotal(details.count, details.price, details.desc) | currency}}</span>

                                                                                    <input type="text" min="1" class="form-control" id="total_details{{$index}}" name="total_details{{$index}}" 

                                                                                            value="{{getTotal(details.count, details.price, details.desc) | currency}}" 

                                                                                            ng-disabled="0 == 0" ng-show="details.edit > 0">

                                                                                </td>

                                                                               <td style="vertical-align: middle !important;" class="detailsDelete">

                                                                                    <button type="button" ng-click="editRow(details , $index)" ng-show="details.edit == 0" class="btn red-mint">

                                                                                        <i class="fa fa-pencil"></i>

                                                                                    </button>

                                                                                    <button type="button" ng-click="saveRow(details , $index)" ng-show="details.edit > 0" class="btn blue">

                                                                                        <i class="fa fa-save"></i>

                                                                                    </button>

                                                                                    <button type="button" ng-click="dropRow(details , $index)" class="btn red-mint">

                                                                                        <i class="fa fa-close"></i>

                                                                                    </button>

                                                                                </td>

                                                                           </tr>

                                                                        </tbody>

                                                                    </table>

                                                                </div>

                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>
                                                <div class="row">

                                                    <div class="col-md-6"> </div>

                                                    <div class="col-md-6">

                                                        <div class="well">

                                                            

                                                            <div class="row static-info align-reverse">

                                                                <div class="col-md-8 name"> Sub Total: </div>

                                                                <div class="col-md-3 value"> {{cotizacion.sumsubtotal | currency}} </div>

                                                            </div>
                                                            
                                                            <div class="row static-info align-reverse">

                                                                <div class="col-md-8 name"> Descu: </div>

                                                                <div class="col-md-3 value"> {{cotizacion.sumDescu | currency}} </div>

                                                            </div>

                                                            <div class="row static-info align-reverse">

                                                                <div class="col-md-8 name"> IVA: </div>

                                                                <div class="col-md-3 value"> {{cotizacion.iva | currency}} </div>

                                                            </div>

                                                            <div class="row static-info align-reverse">

                                                                <div class="col-md-8 name"> Total a pagar: </div>

                                                                <div class="col-md-3 value"> {{cotizacion.total | currency}} </div>

                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>
                                                <div class="row">

                                                    <div class="col-md-12">

                                                        <div class="row static-info">

                                                            <div class="col-md-2 name"> Observaciones: </div>

                                                            <div class="col-md-10 obser"> <textarea ng-model="cotizacion.observaciones" id="summernote_1" name="summernote_1"></textarea> </div>

                                                        </div>

                                                    </div>

                                                </div>

                                                <div class="row">

                                                    <div class="col-md-12">

                                                        <div class="row static-info">

                                                            <!-- <div class="col-md-2 name"> Observaciones: </div> -->

                                                            <div class="col-md-10 col-sm-offset-2 textfire"> 

                                                                <textarea id="summernote_2" name="summernote_2">

                                                                    <div class="borderBox">

                                                                        <p><span style="font-weight: bold;">La oferta no incluye:</span></p>

                                                                        <ul>

                                                                        <li>Transporte fuera de la ciudad de Guayaquil (al menos que se indique cobro de viaticos)</li>

                                                                        <li>Trabajos de albañilería, pintura, u otra adecuación civil</li>

                                                                        <li>Apertura, sellado, Impermeabilización, de boquetes por losa, tumbado o paredes para paso de ductos, tubería y demás requerimientos para instalación de material y equipos.</li>

                                                                        <li>Acometidas eléctricas, breakers y puntos para termostatos y drenajes.</li>

                                                                        <li><span style="line-height: 1.42857;">Cualquier otro trabajo, equipo y/o material no estipulado en la oferta.</span></li>

                                                                        </ul>

                                                                    </div>

                                                                    <div class="borderBox">

                                                                        <p><span style="font-weight: bold;">Forma de pago:</span></p>

                                                                        <ol type="A">

                                                                        <li>Precio establecido con descuento para pago directo por medio de cheque o transferencia bancaria a favor de CEGASERVICES S.A.</li>

                                                                        <li>Disponible pago con tarjeta de crédito DINERS, VISA, MASTERCARD para el cual no aplica el descuento otorgado y cuyo costo final será confirmado una ve indicado medio de pago con tarjeta de crédito.</li>

                                                                        </ol>

                                                                    </div>

                                                                    <div class="borderBox">

                                                                        <p><span style="font-weight: bold;">Validez de la oferta:</span></p>

                                                                            <ul>

                                                                            <li>15 días o hasta agotar stock de equipos y/o materiales</li>

                                                                            </ul>

                                                                    </div>

                                                                    <div class="borderBox">

                                                                        <p><span style="font-weight: bold;">Duración de Trabajos (Tiempo Referencial):</span></p>

                                                                        <ul>

                                                                            <li>Instalaciones Básicas: de 3 a 4 horas por equipo.</li>

                                                                            <li>Mantenimientos: de 45 min a 2 horas por equipo.</li>

                                                                            <li>Trabajos Correctivos: Menores: 1 hora por equipo; Mayores: 4 a 6 horas por equipo.</li>

                                                                            <li>Tiempos especificos de ejecución pueden variar según las condiciones específicas de cada trabajo</li>

                                                                        </ul>

                                                                    </div>

                                                                    <div class="borderBox">

                                                                        <p><span style="font-weight: bold;">Garantía:</span></p>

                                                                        <ul>

                                                                        <li><span style="line-height: 1.42857; font-weight: bold;">Mano de obra:</span></li>

                                                                        </ul>

                                                                        <p style="margin-left: 75px;"><span style="text-decoration: underline; font-style: italic;">Mantenimientos&nbsp;</span>: Para equipos de requerimiento de mantenimiento mensual o bi-mensual garantía de 15 días, para trimestrales o más se otorga garantía de 1 mes en los trabajos realizados. No incluye fallas por desgaste o uso de las partes / equipo que pueden presentarse después de un mantenimiento.</p>

                                                                        <p style="margin-left: 75px;"><span style="text-decoration: underline; font-style: italic;">Instalación y correctivos :</span> 3 semanas de garantía ya que las fallas en este rubro se presentan siempre en las primeras 3 semanas de realizado un trabajo.</p>

                                                                        <ul>

                                                                        <li><span style="line-height: 1.42857;"><span style="font-weight: bold;">Repuestos:</span> Garantía por fallas de fabricación de 3 meses para compresores, turbinas y motores eléctricos y de 1 mes para partes y componentes eléctricos menores.</span><br></li>

                                                                        <li><span style="font-weight: bold;">Equipos ECOX:</span> partes y piezas 1 año, compresor 5 años por fallas de fabricación</li>

                                                                        </ul>

                                                                        <p><span style="line-height: 1.42857; font-style: italic; font-size: 11px;">Para todas las áreas, la garantía no cubre fallas por problemas eléctricos, variaciones de voltaje, mala instalación y manipulación (si no realizado por personal calificado por CEGASERVICES S.A.), falta de mantenimiento, mal manejo o uso que incluye el no seguir las recomendaciones indicadas por le empresa</span><br></p>

                                                                    </div>

                                                                </textarea> 

                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>
												<div class="row">
													 <div class="col-md-12" style="text-align:center">
													 <br>
													<strong>Elaborado por:&nbsp;</strong>{{cotizacion.usuario}}
													</div>
												</div>
                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                            <!-- End: life time stats -->

                        </div>

                    </div>

                    <!-- BEGIN PAGE BASE CONTENT -->

                    <div id="ajax-modal" class="modal fade" tabindex="-1">

                        <div class="modal-header">

                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>

                            <h4 class="modal-title">Sucursales</h4>

                        </div>

                        <div class="modal-body">

                         <div class="table-responsive">

                            <table class="table table-hover table-bordered table-striped">

                                <thead>

                                    <tr>

                                        <th>RUC / CI</th>

                                        <th>Nombre</th>

                                        <th>Accion</th>

                                    </tr>

                                </thead>

                                <tbody>

                                    <tr ng-repeat="sucu in sucursales">

                                        <td>{{sucu.ruc}}</td>

                                        <td>{{sucu.nombre_contacto}}</td>

                                        <td>

                                            <button type="button" class="btn elec" ng-click="setClient(sucu , 'Sucursal ')">

                                                <i class='fa fa-check' aria-hidden='true'></i>

                                            </button>

                                        </td>

                                    </tr>

                                </tbody>

                            </table>

                        </div>

                        <div class="modal-footer">

                            <button type="button" data-dismiss="modal" class="btn btn-outline dark">Cerrar</button>

                        </div>

                    </div>

                    <!-- END PAGE BASE CONTENT -->

                </div>
				
				<div id="ajax-modal2" class="modal fade" tabindex="-1">

                        <div class="modal-header">

                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>

                            <h4 class="modal-title">Clientes</h4>

                        </div>

                        <div class="modal-body">

                         <div class="table-responsive">

                            <table class="table table-hover table-bordered table-striped">

                                <thead>

                                    <tr>

                                        <th>RUC / CI</th>

                                        <th>Nombre</th>

                                        <th>Accion</th>

                                    </tr>

                                </thead>

                                <tbody>

                                    <tr ng-repeat="cli in clientesNombre">

                                        <td>{{cli.ruc}}</td>

                                        <td>{{cli.nombre}}</td>

                                        <td>

                                            <button type="button" class="btn elec" ng-click="setClientNombre(cli , 'Cliente ')">

                                                <i class='fa fa-check' aria-hidden='true'></i>

                                            </button>

                                        </td>

                                    </tr>

                                </tbody>

                            </table>

                        </div>

                        <div class="modal-footer">

                            <button type="button" data-dismiss="modal" class="btn btn-outline dark">Cerrar</button>

                        </div>

                    </div>

                    <!-- END PAGE BASE CONTENT -->

                </div>
				
				<div id="ajax-modal3" class="modal fade" tabindex="-1">

                        <div class="modal-header">

                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>

                            <h4 class="modal-title">Información de Facturación</h4>

                        </div>

                        <div class="modal-body">

                         <div class="table-responsive">

                            <table class="table table-hover table-bordered table-striped">

                                <thead>

                                    <tr>

                                        <th>RUC / CI</th>

                                        <th>Razón Social</th>

                                        <th>Accion</th>

                                    </tr>

                                </thead>

                                <tbody>

                                    <tr ng-repeat="fact in facturacion">

                                        <td>{{fact.ruc}}</td>

                                        <td>{{fact.razon_social}}</td>

                                        <td>

                                            <button type="button" class="btn elec" ng-click="setClientFact(fact , 'Facturacion ')">

                                                <i class='fa fa-check' aria-hidden='true'></i>

                                            </button>

                                        </td>

                                    </tr>

                                </tbody>

                            </table>

                        </div>

                        <div class="modal-footer">

                            <button type="button" data-dismiss="modal" class="btn btn-outline dark">Cerrar</button>

                        </div>

                    </div>

                    <!-- END PAGE BASE CONTENT -->

                </div>

                <!-- END CONTENT BODY -->

            </div>

            <!-- END CONTENT