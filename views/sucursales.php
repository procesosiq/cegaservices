<style>
    .margin-bottom{
        margin-bottom : 2px; 
    }
    .button-style{
        background: white;
        border: none;
    }
</style>
<div class="page-content-wrapper" ng-controller="sucursales" ng-app="app">
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
            </div>
        </div>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="/clienteList">Clientes</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="/sucursalesList?idc=<?php echo $_GET['idc']; ?>"> Sucursales</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Registro de sucursales</span>
            </li>
        </ul>
        <div class="row">
            <div class="col-md-12">
                <div class="tab-pane" id="tab_1">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i>AGREGAR NUEVA SUCURSAL
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="control-label">Fecha de Registro : <?php echo (isset($retval2->fecha)?$retval2->fecha : date("d/m/Y")); ?> </label>
                                        </div>
                                    </div>
                                </div>
                                <h3 class="form-section">INFORMACIÓN DE SUCURSAL</h3>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Nombre de la sucursal</label>
                                            <input type="text" id="txtnom" class="form-control" value="" ng-model="sucursal.nombre">
                                        </div>
                                    </div>
                                    <div class="col-md-6 ">
                                        <div class="form-group">
                                            <label>Razón Social</label>
                                            <input type="text" class="form-control" id="txtrazon" value="" ng-model="sucursal.razon_social"> 
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>RUC</label>
                                            <input type="text" class="form-control" id="txtruc" value="" ng-model="sucursal.cedula"> 
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Dirección</label>
                                            <input type="text" class="form-control" id="txtdircli" value="" ng-model="sucursal.direccion"> 
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Teléfono</label>
                                            <input type="text" class="form-control" id="txttel" value="" ng-model="sucursal.telefono"> 
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Tipo de Cliente</label>
                                            <select class="form-control" ng-model="sucursal.tipo_cliente">
                                                <option value="1">COMERCIAL</option>
                                                <option value="2">INDUSTRIAL</option>
                                                <option value="3">RESIDENCIAL</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Cantidad Equipos</label>
                                            <input type="text" id="s_equipos" class="form-control" value="" ng-model="sucursal.equipos" readonly>
                                        </div>
                                    </div>
                                </div>
                                <h3 class="form-section">INFORMACIÓN DE CONTACTO</h3>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Nombre del contacto</label>
                                            <input type="text" id="txtnom_contacto" class="form-control" value="" ng-model="contacto.nombre">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Cargo</label>
                                            <input type="text" id="txtCargo" class="form-control" value="" ng-model="contacto.cargo">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Teléfono</label>
                                            <input type="text" class="form-control" id="txttel_contacto" value="" ng-model="contacto.telefono"> 
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Correo Electrónico</label>
                                            <input type="text" id="txtemail" class="form-control" value="" ng-model="contacto.correo">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Area</label>
                                            <input type="text" id="s_areas" class="form-control" value="" ng-model="contacto.area">
                                        </div>
                                    </div>
                                </div>
                                <?php if($_GET["id"]!=''){?>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <button class="btn blue" id="addContact" ng-click="guardarContacto()">Agregar Contacto</button>
                                            </div>
                                        </div>
                                    </div>
                                <?}?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive table-scrollable">
                                            <table class="table table-hover table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <td>ID</td>
                                                        <td>Nombre</td>
                                                        <td>Cargo</td>
                                                        <td>Teléfono</td>
                                                        <td>Correo</td>
                                                        <td>Área</td>
                                                        <td style="min-width : 100px">Principal</td>
                                                        <td style="min-width : 150px">Acciones</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr ng-repeat="row in table_contactos">
                                                        <td>{{ row.id }}</td>
                                                        <td><input 
                                                                type="text" 
                                                                name="nombre" 
                                                                id="nombre" 
                                                                class="form-control center-text" 
                                                                ng-model="row.nombre"
                                                                ng-change="contacto = row" 
                                                        ></td>
                                                        <td><input 
                                                                type="text" 
                                                                name="cargo" 
                                                                id="cargo" 
                                                                class="form-control center-text" 
                                                                ng-model="row.cargo"
                                                                ng-change="contacto = row" 
                                                        ></td>
                                                        <td><input 
                                                                type="text" 
                                                                name="telefono" 
                                                                id="telefono" 
                                                                class="form-control center-text" 
                                                                ng-model="row.telefono"
                                                                ng-change="contacto = row" 
                                                        ></td>
                                                        <td><input 
                                                                type="text" 
                                                                name="correo" 
                                                                id="correo" 
                                                                class="form-control center-text" 
                                                                ng-model="row.correo"
                                                                ng-change="contacto = row" 
                                                        ></td>
                                                        <td><input 
                                                                type="text" 
                                                                name="area" 
                                                                id="area" 
                                                                class="form-control center-text" 
                                                                ng-model="row.area"
                                                                ng-change="contacto = row" 
                                                        ></td>
                                                        <td>
                                                            <select class="form-control" name="principal" id="principal" ng-model="row.principal" ng-change="principalContacto(row)">
                                                                <option ng-selected="value == row.principal" value="1">SI</option>
                                                                <option value="0">NO</option>
                                                            </select>
                                                        </td>
                                                        <td class="center-text">
                                                            <button class="btn blue margin-bottom" id="editar" name="editar" ng-click="editContacto(row)"><i class="fa fa-pencil"></i></button>
                                                            <button class="btn bg-red-intense" id="eliminar" name="eliminar" ng-click="eliminarContacto(row.id)"><i class="fa fa-times"></i></button>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table> 
                                        </div>
                                    </div>
                                </div>
                                <h3 class="form-section">INFORMACIÓN DE FACTURACIÓN</h3>
                                <label class="control-label">Datos Cliente Principal :</label>
                                <button ng-if="data.principal" class="button-style" ng-click="getDataPrincipal(false)"><i class="fa fa-check-square-o"></i></button>
                                <button ng-if="!data.principal" class="button-style" ng-click="getDataPrincipal(true)"><i class="fa fa-square-o"></i></button>
                                <br>
                                <br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Razón Social</label>
                                            <input type="text" id="txtRsF" class="form-control" ng-model="facturacion.razon_social">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Ruc</label>
                                            <input type="text" id="txtRucF" class="form-control" ng-model="facturacion.cedula">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Dirección</label>
                                            <input type="text" class="form-control" id="txtDirF" ng-model="facturacion.direccion"> 
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Teléfono</label>
                                            <input type="text" id="txtTelF" class="form-control" ng-model="facturacion.telefono">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Correo Electrónico</label>
                                            <input type="text" id="txtemailfac" class="form-control" value="" ng-model="facturacion.correo">
                                        </div>
                                    </div>
                                </div>
                                <?php if($_GET["id"]!=''){?>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <button class="btn blue" id="btnAddFac" ng-click="guardarFacturacion()">Agregar información de facturación</button>
                                            </div>
                                        </div>
                                    </div>
                                <?}?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive table-scrollable">
                                            <table class="table table-hover table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <td>ID</td>
                                                        <td>Razón Social</td>
                                                        <td>Ruc</td>
                                                        <td>Dirección</td>
                                                        <td>Teléfono</td>
                                                        <td>Correo</td>
                                                        <td style="min-width : 100px">Principal</td>
                                                        <td style="min-width : 150px">Acciones</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <tr ng-repeat="row in table_facturacion">
                                                        <td>{{ row.id }}</td>
                                                        <td><input 
                                                                type="text" 
                                                                name="nombre" 
                                                                id="nombre" 
                                                                class="form-control center-text" 
                                                                ng-model="row.razon_social"
                                                                ng-change="facturacion = row" 
                                                        ></td>
                                                        <td><input 
                                                                type="text" 
                                                                name="cedula" 
                                                                id="cedula" 
                                                                class="form-control center-text" 
                                                                ng-model="row.cedula"
                                                                ng-change="facturacion = row" 
                                                        ></td>
                                                        <td><input 
                                                                type="text" 
                                                                name="direccion" 
                                                                id="direccion" 
                                                                class="form-control center-text" 
                                                                ng-model="row.direccion"
                                                                ng-change="facturacion = row" 
                                                        ></td>
                                                        <td><input 
                                                                type="text" 
                                                                name="telefono" 
                                                                id="telefono" 
                                                                class="form-control center-text" 
                                                                ng-model="row.telefono"
                                                                ng-change="facturacion = row" 
                                                        ></td>
                                                        <td><input 
                                                                type="text" 
                                                                name="correo" 
                                                                id="correo" 
                                                                class="form-control center-text" 
                                                                ng-model="row.correo"
                                                                ng-change="facturacion = row" 
                                                        ></td>
                                                        <td>
                                                            <select class="form-control" name="principal" id="principal" ng-model="row.principal" ng-change="principalFacturacion(row)">
                                                                <option ng-selected="value == row.principal" value="1">SI</option>
                                                                <option value="0">NO</option>
                                                            </select>
                                                        </td>
                                                        <td style="min-width : 150px">
                                                            <button class="btn blue margin-bottom" id="editar" name="editar" ng-click="editFacturacion(row)"><i class="fa fa-pencil"></i></button>
                                                            <button class="btn bg-red-intense" id="eliminar" name="eliminar" ng-click="eliminarFacturacion(row.id)"><i class="fa fa-times"></i></button>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table> 
                                        </div>
                                    </div>
                                    <div class="pull-right">
                                        <button style="margin:5px" class="btn bg-red-intense bg-font-red-intense" name="cancelar" id="cancelar" ng-click="cancelar()"><i class="fa fa-times"></i>  Cancelar</button>
                                        <button style="margin:5px" class="btn blue" name="guardar" id="guardar" ng-click="guardar()"><i class="fa fa-check"></i>  Guardar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>