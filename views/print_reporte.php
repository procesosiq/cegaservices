<?php

require_once("dompdf/dompdf_config.inc.php");
$host="procesos-iq.com";
$db_user="auditoriasbonita";
$db_pass="u[V(fTIUbcVb";
$db_name="cegaservices2";

$link = new mysqli($host, $db_user, $db_pass, $db_name);
if($link->connect_errno){ 
	echo "Fallo al conectar a MySQL: ". $link->connect_error; 
	return;
}

$ids_orden = "";

$sql = "SELECT id FROM orden_trabajo WHERE id_cotizacion='$_GET[id]'";
$res = $link->query($sql);

while($rowid = $res->fetch_assoc()){
	$ids_orden .= $rowid["id"].",";
}

if($ids_orden != ""){
	$ids_orden = substr($ids_orden,0,-1);
}
else
	$ids_orden = 0;

$sql = "SELECT * FROM `reportes_r_correctivo` WHERE (img_firma!='' OR audio_comentario_gral != '') AND id_orden = '$ids_orden'";
echo $sql;
die($sql);

$res = $link->query($sql);
$rowEnc = $res->fetch_assoc();
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>REPORTES</title>
<style type="text/css">
body{
	background-color:#999;
}
.lblone{
	font-family:Arial, Helvetica, sans-serif;
	font-weight:bold;
	font-size:12px;
}
.lbltwo{
	font-family:Arial, Helvetica, sans-serif;
	font-weight:bold;
	font-size:12px;
	color:#002E5B;
}
.lblthree{
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
	border-bottom:1px dotted #999999;
}
.lbltitle{
	font-family:Arial, Helvetica, sans-serif;
	font-weight:bold;
	font-size:14px;
	border-bottom: 3px solid #035719;
	color:#035719;
}
.tblwithe{
	background-color:#FFF;
}
.separa{
	border-top: 10px solid #D09B35;
}
</style>
</head>

<body>
<div style="width:100%; height:100%;" align="center">
<div style="width:910px; background-color:#FFF; padding:5px;">

<table width="840" border="0" align="center" cellpadding="0" cellspacing="0" class="tblwithe">
  <tr>
    <td><img src="logocega.jpg" width="275" height="80" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">INFORMACION GENERAL</td>
      </tr>
      <tr>
        <td width="29%" height="21" class="lblone">Geolocalizaci&oacute;n</td>
        <td width="71%" class="lblthree"><a href="https://www.google.com/maps?q=<?php echo $rowEnc["geolocalizacion"];?>" target="_blank">Ver Mapa</a></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Fecha de Correctivo</td>
        <td class="lblthree"><?=$rowEnc["fecha_correctivo"]?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Hora de Llegada</td>
        <td class="lblthree"><?=$rowEnc["hora_llegada"]?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cliente</td>
        <td class="lblthree"><?=utf8_encode($rowEnc["cliente"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Hora Programada</td>
        <td class="lblthree"><?=$rowEnc["hora_programada"]?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Sucursal</td>
        <td class="lblthree"><?=utf8_encode($rowEnc["sucursal"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Direcci&oacute;n Sucursal</td>
        <td class="lblthree"><?=utf8_encode($rowEnc["direccion"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Referencia</td>
        <td class="lblthree"><?=utf8_encode($rowEnc["referencia"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Detalle del Trabajo</td>
        <td class="lblthree"><?=utf8_encode($rowEnc["detalle_trabajo"])?></td>
      </tr>
      <?php
	  if($rowEnc["observacion1"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree"><?=utf8_encode($rowEnc["observacion1"])?></td>
      </tr>
      <?php }?>
    </table></td>
  </tr>
</table>




<?php 
$sql = "SELECT * FROM `reportes_r_correctivo` WHERE id_orden='$_GET[id]'";
print_r($sql);
$res = $link->query($sql);
$conEqui = 0;
while($row = $res->fetch_assoc()){
	$conEqui++;
?>

<?php if($row["tipo_reporte"] == "CORRECTIVO B"){?>
<table width="840" border="0" align="center" cellpadding="0" cellspacing="0" class="tblwithe">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="separa">&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">INFORMACION DEL EQUIPO #<?php echo $conEqui;?></td>
      </tr>
      <tr>
        <td width="29%" class="lblone" height="21">Tipo de Equipo</td>
        <td width="71%" class="lblthree"><?=utf8_encode($row["tipo_equipo"])?></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">CARACTERISTICAS EQUIPO (CLIMATIZACION B)</td>
      </tr>
      <?php
	  if($row["codigo_equipo"] != ''){ ?>
      <tr>
        <td width="29%" class="lblone" height="21">C&oacute;digo del Equipo</td>
        <td width="71%" class="lblthree"><?=utf8_encode($row["codigo_equipo"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["desc_equipo"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n del Equipo</td>
        <td class="lblthree"><?=utf8_encode($row["desc_equipo"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if(trim($row["area_clima"]) != ''){ ?>
      <tr>
        <td class="lblone" height="21">Area Que Climatiza</td>
        <td class="lblthree"><?=utf8_encode($row["area_clima"])?></td>
      </tr>
      <?php }?>
      
       <?php
	  if($row["nombre_area"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Nombre del Area</td>
        <td class="lblthree"><?=utf8_encode($row["nombre_area"])?></td>
      </tr>
      <?php }?>
      <tr>
        <td class="lbltwo" height="21">Evaporador</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <?php
	  if($row["marca_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Marca</td>
        <td class="lblthree"><?=utf8_encode($row["marca_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["btu_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Capacidad (BTU)</td>
        <td class="lblthree"><?=utf8_encode($row["btu_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["modelo_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Modelo</td>
        <td class="lblthree"><?=utf8_encode($row["modelo_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["serie_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Serie</td>
        <td class="lblthree"><?=utf8_encode($row["serie_eva"])?></td>
      </tr>
      <?php }?>
      <tr>
        <td class="lbltwo" height="21">Condensador</td>
        <td>&nbsp;</td>
      </tr>
      
      <?php
	  if($row["marca_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Marca</td>
        <td class="lblthree"><?=utf8_encode($row["marca_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["btu_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Capacidad (BTU)</td>
        <td class="lblthree"><?=utf8_encode($row["btu_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["modelo_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Modelo</td>
        <td class="lblthree"><?=utf8_encode($row["modelo_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["serie_conde"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Serie</td>
        <td class="lblthree"><?=utf8_encode($row["serie_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["observa2"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree"><?=utf8_encode($row["observa2"])?></td>
      </tr>
      <?php }?>
      
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">CORRECTIVO EQUIPO (CLIMATIZACION B)</td>
      </tr>
      <tr>
        <td height="21" class="lbltwo">Evaporador</td>
        <td>&nbsp;</td>
      </tr>
      
      <?php
	  if($row["correc_newrep_eva"] != ''){ ?>
      <tr>
        <td width="29%" height="21" valign="top" class="lblone">Instalaci&oacute;n Repuestos (Nuevo)</td>
        <td width="71%" class="lblthree"><?=utf8_encode($row["correc_newrep_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["correc_reparep_eva"] != ''){ ?>
      <tr>
        <td height="21" valign="top" class="lblone">Instalaci&oacute;n Repuestos (Reparados)</td>
        <td class="lblthree"><?=utf8_encode($row["correc_reparep_eva"])?></td>
      </tr>
      <?php }?>
      
      <tr>
        <td height="21" class="lbltwo">Condensador</td>
        <td>&nbsp;</td>
      </tr>
      
      <?php
	  if($row["correc_newrep_conde"] != ''){ ?>
      <tr>
        <td height="21" valign="top" class="lblone">Instalaci&oacute;n Repuestos (Nuevo)</td> 
        <td class="lblthree"><?=utf8_encode($row["correc_newrep_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["correc_reparep_conde"] != ''){ ?>
      <tr>
        <td height="21" valign="top" class="lblone">Instalaci&oacute;n Repuestos (Reparados)</td>
        <td class="lblthree"><?=utf8_encode($row["correc_reparep_conde"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["correc_audio"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Comentarios</td> 
        <td class="lblthree"><a href="reportes_vistos/correctivo/<?php echo $rowEnc["correc_audio"];?>">Audio de Comentarios</a></td>
      </tr>
      <?php }?>
      
       <?php
	  if($row["correc_descrip"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n del Trabajo Realizado</td>
        <td class="lblthree"><?=utf8_encode($row["correc_descrip"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["correc_fotos"] != ''){ ?>
      <tr>
        <td class="lbltwo" height="21">Captura de Fotos</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" class="lblone">
        <?php
        	if($row["correc_fotos"] != ''){
				$fotos = explode("|", substr($row["correc_fotos"],0,-1));
				for($i=0;$i<count($fotos);$i++){
					?><img src="reportes_vistos/correctivo/<?php echo $fotos[$i];?>" width="143" height="147" />&nbsp;<?
				}
			}
		?>
        </td>
        </tr>
     <?php }?>
     
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <?php
	  if($row["mat_item1_sel"] != ''){ ?>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">MATERIALES REQUERIDOS</td>
      </tr>
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #1</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item1_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item1_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item1_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item1_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item1_obser"])?></td>

      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item2_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #2</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item2_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item2_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item2_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item2_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item2_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item3_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #3</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item3_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item3_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item3_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item3_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item3_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item4_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #4</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item4_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item4_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item4_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item4_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item4_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item5_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #5</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item5_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item5_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item5_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item5_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item5_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item6_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #6</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item6_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item6_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item6_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item6_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item6_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item7_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #7</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item7_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item7_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item7_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item7_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item7_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item8_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #8</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item8_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item8_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item8_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item8_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item8_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item9_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #9</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item9_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item9_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item9_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item9_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item9_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item10_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #10</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item10_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item10_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item10_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item10_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item10_obser"])?></td>
      </tr>
    </table></td>
  </tr>
   <?php }?>
</table>

  <?php }?>
 
<?php if($row["tipo_reporte"] == "CORRECTIVO A"){?>
<table width="840" border="0" align="center" cellpadding="0" cellspacing="0" class="tblwithe">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="separa">&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">INFORMACION DEL EQUIPO #<?php echo $conEqui;?></td>
      </tr>
      <tr>
        <td width="29%" class="lblone" height="21">Tipo de Equipo</td>
        <td width="71%" class="lblthree"><?=utf8_encode($row["tipo_equipo"])?></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">CARACTERISTICAS EQUIPO (CLIMATIZACION A)</td>
      </tr>
      <?php
	  if($row["codigo_equipo"] != ''){ ?>
      <tr>
        <td width="29%" class="lblone" height="21">C&oacute;digo del Equipo</td>
        <td width="71%" class="lblthree"><?=utf8_encode($row["codigo_equipo"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["desc_equipo"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n del Equipo</td>
        <td class="lblthree"><?=utf8_encode($row["desc_equipo"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if(trim($row["area_clima"]) != ''){ ?>
      <tr>
        <td class="lblone" height="21">Area Que Climatiza</td>
        <td class="lblthree"><?=utf8_encode($row["area_clima"])?></td>
      </tr>
      <?php }?>
      
       <?php
	  if($row["nombre_area"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Nombre del Area</td>
        <td class="lblthree"><?=utf8_encode($row["nombre_area"])?></td>
      </tr>
      <?php }?>
      <tr>
        <td class="lbltwo" height="21">Evaporador / Condensador</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <?php
	  if($row["marca_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Marca</td>
        <td class="lblthree"><?=utf8_encode($row["marca_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["btu_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Capacidad (BTU)</td>
        <td class="lblthree"><?=utf8_encode($row["btu_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["modelo_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Modelo</td>
        <td class="lblthree"><?=utf8_encode($row["modelo_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["serie_eva"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Serie</td>
        <td class="lblthree"><?=utf8_encode($row["serie_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["observa2"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree"><?=utf8_encode($row["observa2"])?></td>
      </tr>
      <?php }?>
      
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">CORRECTIVO EQUIPO (CLIMATIZACION A)</td>
      </tr>
      <tr>
        <td height="21" class="lbltwo">Evaporador / Condensador</td>
        <td>&nbsp;</td>
      </tr>
      
      <?php
	  if($row["correc_newrep_eva"] != ''){ ?>
      <tr>
        <td width="29%" height="21" valign="top" class="lblone">Instalaci&oacute;n Repuestos (Nuevo)</td>
        <td width="71%" class="lblthree"><?=utf8_encode($row["correc_newrep_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["correc_reparep_eva"] != ''){ ?>
      <tr>
        <td height="21" valign="top" class="lblone">Instalaci&oacute;n Repuestos (Reparados)</td>
        <td class="lblthree"><?=utf8_encode($row["correc_reparep_eva"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["correc_audio"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Comentarios</td> 
        <td class="lblthree"><a href="reportes_vistos/correctivo/<?php echo $rowEnc["correc_audio"];?>">Audio de Comentarios</a></td>
      </tr>
      <?php }?>
      
       <?php
	  if($row["correc_descrip"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n del Trabajo Realizado</td>
        <td class="lblthree"><?=utf8_encode($row["correc_descrip"])?></td>
      </tr>
      <?php }?>
      
      <?php
	  if($row["correc_fotos"] != ''){ ?>
      <tr>
        <td class="lbltwo" height="21">Captura de Fotos</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" class="lblone">
        <?php
        	if($row["correc_fotos"] != ''){
				$fotos = explode("|", substr($row["correc_fotos"],0,-1));
				for($i=0;$i<count($fotos);$i++){
					?><img src="reportes_vistos/correctivo/<?php echo $fotos[$i];?>" width="143" height="147" />&nbsp;<?
				}
			}
		?>
        </td>
        </tr>
     <?php }?>
     
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <?php
	  if($row["mat_item1_sel"] != ''){ ?>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">MATERIALES REQUERIDOS</td>
      </tr>
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #1</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item1_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item1_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item1_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item1_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item1_obser"])?></td>

      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item2_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #2</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item2_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item2_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item2_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item2_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item2_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item3_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #3</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item3_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item3_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item3_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item3_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item3_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item4_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #4</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item4_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item4_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item4_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item4_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item4_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item5_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #5</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item5_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item5_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item5_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item5_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item5_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item6_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #6</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item6_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item6_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item6_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item6_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item6_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item7_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #7</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item7_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item7_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item7_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item7_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item7_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item8_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #8</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item8_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item8_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item8_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item8_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item8_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item9_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #9</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item9_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item9_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item9_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item9_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item9_obser"])?></td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  
  <?php
	  if($row["mat_item10_sel"] != ''){ ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%" class="lbltwo" height="21">Item #10</td>
        <td width="71%">&nbsp;</td>
      </tr>
      <tr>
        <td class="lblone" height="21">Selecci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item10_sel"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Descripci&oacute;n de Item</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item10_des"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Definir Unidad</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item10_uni"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Cantidad Requerida</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item10_cant"])?></td>
      </tr>
      <tr>
        <td class="lblone" height="21">Observaciones</td>
        <td class="lblthree"><?=utf8_encode($row["mat_item10_obser"])?></td>
      </tr>
    </table></td>
  </tr>
   <?php }?>
</table>

  <?php }?>
  
<?php }?>
  
  
  
  
  
<table width="840" border="0" align="center" cellpadding="0" cellspacing="0" class="tblwithe">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <?php
	  if($rowEnc["responsable"] != '' || $rowEnc["cargo_responsable"] != '' || $rowEnc["img_firma"] != '' || $rowEnc["observa_fin"] != '' || $rowEnc["diag_audio"] != ''){ ?>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" class="lbltitle">OBSERVACIONES GENERALES Y FIRMAS</td>
      </tr>
      <?php
	  if($rowEnc["responsable"] != ''){ ?>
      <tr>
        <td width="37%" class="lblone" height="21">Nombre del Responsable que Supervisa el Trabajo</td>
        <td width="63%" class="lblthree"><?=utf8_encode($rowEnc["responsable"])?></td>
      </tr>
      <?php }?>
      <?php
	  if($rowEnc["cargo_responsable"] != ''){ ?>
      <tr>
        <td class="lblone" width="37%" height="21">Cargo</td>
        <td class="lblthree" width="63%"><?=utf8_encode($rowEnc["cargo_responsable"])?></td>
      </tr>
      <?php }?>
      <?php
	  if($rowEnc["img_firma"] != ''){ ?>
      <tr>
        <td class="lbltwo" height="21">Firma del Cliente o Responsable</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" class="lblone"><img src="reportes_vistos/correctivo/<?php echo $rowEnc["img_firma"];?>" width="299" height="85" /></td>
      </tr>
      <?php }?>
      <?php
	  if($rowEnc["observa_fin"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Observaciones Generales</td>
        <td class="lblthree" width="63%"><?=utf8_encode($rowEnc["observa_fin"])?></td>
      </tr>
      <?php }?>
      <?php
	  if($rowEnc["audio_comentario_gral"] != ''){ ?>
      <tr>
        <td class="lblone" height="21">Comentarios Generales</td>
        <td class="lblthree" width="63%"><a href="reportes_vistos/correctivo/<?php echo $rowEnc["audio_comentario_gral"];?>">Audio de Comentarios</a></td>
      </tr>
      <?php }?>
    </table></td>
  </tr>
  <?php }?>
    <tr>
    <td>&nbsp;</td>
  </tr>
</table>

</div>
</div>
</body>
</html>