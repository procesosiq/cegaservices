<style >
    #iframeGeneral{border:0px;position:relative;width:100%;height:4219px;}
    #divIframe{padding:3px;}
    @media (min-width: 768px) {
        #iframeGeneral{height: 4170px;}
         #divIframe{padding:0px;}
    }
    @media (min-width: 992px) {
        #divIframe{height: 2900px;}
        #iframeGeneral{height: 2900px;}
    }
    @media (min-width: 1250px) {
        #divIframe{height: 2890px;}
        #iframeGeneral{height: 2890px;}
    }
    @media print{.divPorcentajeRecurr{position: relative;margin-top:-140px;} #iframeGeneral{height: 2590px;}#tags_indicators .col-xs-12{width: 33%;}
    .yellow-casablanca{    background-color: #f2784b!important;}
    .counter_tags,.desc,.more,.number{color:white!important;}
    .dashboard-stat.red{background-color: #e7505a!important;}
    .dashboard-stat.yellow {background-color: #c49f47!important;}
    .dashboard-stat.yellow-gold {background-color: #E87E04!important;}

}
</style>                  
                    <div class="row" id="tags_indicators" style="display:none">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat yellow-casablanca">
                                <div class="visual">
                                    <i class="fa fa-shopping-cart"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span class="counter_tags" data-counter="counterup" data-value="{{ (tags.total_equipos == null) ? '0.00' : tags.total_equipos | number : 2 }}">{{ (tags.total_equipos == null) ? '0.00' : tags.total_equipos | number : 2 }}</span>
                                    </div>
                                    <div class="desc"> EQUIPOS $ </div>
                                </div>
                                <!-- <a class="more" href="javascript:;" ng-click="getCategories('R')"> VER REGISTRADOS
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a> -->
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat red">
                                <div class="visual">
                                    <i class="fa fa-shopping-cart"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span class="counter_tags" data-counter="counterup" data-value="{{ (tags.total_materiales == null) ? '0.00' : tags.total_materiales | number : 2 }}">{{ (tags.total_materiales == null) ? '0.00' : tags.total_materiales | number : 2 }}</span></div>
                                    <div class="desc"> MATERIALES $ </div>
                                </div>
                                <!-- <a class="more" href="javascript:;" ng-click="getCategories('P')"> VER PUBLICADOS
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a> -->
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat yellow">
                                <div class="visual">
                                    <i class="fa fa-shopping-cart"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span class="counter_tags" data-counter="counterup" data-value="{{ (tags.total_repuestos == null) ? '0.00' : tags.total_repuestos | number : 2 }}">{{ (tags.total_repuestos == null) ? '0.00' : tags.total_repuestos | number : 2 }}</span>
                                    </div>
                                    <div class="desc"> REPUESTOS $ </div>
                                </div>
                                <!-- <a class="more" href="javascript:;" ng-click="getCategories('B')"> VER BORRADORES
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a>-->
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat yellow-gold">
                                <div class="visual">
                                    <i class="fa fa-shopping-cart"></i>
                                </div>
                                <div class="details">
                                    <div class="number"> 
                                        <span class="counter_tags" data-counter="counterup" data-value="{{ (tags.total_mano == null) ? '0.00' : tags.total_mano | number : 2 }}">{{ (tags.total_mano == null) ? '0.00' : tags.total_mano | number : 2 }}</span></div>
                                    <div class="desc"> MANO DE OBRA $  </div>
                                </div>
                                <!-- <a class="more" href="javascript:;"> VER MÁS
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a> -->
                            </div>
                        </div>
                        <!--Nuevo-->
                         <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat yellow">
                                <div class="visual">
                                    <i class="fa fa-shopping-cart"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span class="counter_tags" data-counter="counterup" data-value="{{ (tags.total == null) ? '0.00' : tags.total | number : 2 }}">{{ (tags.total == null) ? '0.00' : tags.total | number : 2 }}</span>
                                    </div>
                                    <div class="desc"> TOTAL $ </div>
                                </div>
                                <!-- <a class="more" href="javascript:;"> 
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a> -->
                            </div>
                        </div>
                    </div>
                   <!-- <div class="saltopagina"></div>-->
                    <!--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="divIframe">
                            <iframe src="/especifico"  id="iframeGeneral" scrolling="no" ></iframe>
                    </div>-->
                    <div class="clearfix"></div>
                    