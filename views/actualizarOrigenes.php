<link href="http://cdn.procesos-iq.com/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="http://cdn.procesos-iq.com/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />

<style>
    #datatable_ajax.table td {
    text-align: center;   
}.margin{
    margin : 5px;
}
</style>
<div id="cont" class="page-content-wrapper" ng-app="app" ng-controller="actualizarOrigenes" ng-cloak>
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1>Actualizador</h1>
            </div>
        </div>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="/">Inicio</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Actualización de origenes de datos</span>
            </li>
        </ul>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject font-dark sbold uppercase">Actualizar</span>
                        </div>
                        <div class="tools"> 
                        </div>
                    </div> 
                    <div class="portlet-body">
                        <!-- <button ng-disabled="false" class="btn blue" ng-click="actualizarTodos()">Todos los origenes</button> -->
                        <button ng-disabled="false" class="btn blue margin" ng-click="cronograma()">Crónograma de ordenes</button>
                        <button ng-disabled="false" class="btn blue margin" ng-click="actualizarRevisionInstalacion()">Resvision Instalación</button>
                        <button ng-disabled="false" class="btn blue margin" ng-click="actualizarInstalacion()">Instalación</button>
                        <button ng-disabled="false" class="btn blue margin" ng-click="actualizarRevisionMantenimiento()">Revision Mantenimiento</button>
                        <button ng-disabled="false" class="btn blue margin" ng-click="actualizarMantenimiento()">Mantenimiento</button>
                        <button ng-disabled="false" class="btn blue margin" ng-click="actualizarRevisionCorrectivo()">Revision Correctivo</button>
                        <button ng-disabled="false" class="btn blue margin" ng-click="actualizarCorrectivo()">Correctivo</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>