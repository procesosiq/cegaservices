    <style>
        .radio{
            padding: 0;
        }
    </style>
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content" ng-app="app" ng-controller="programacion" id="programacion">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Programación</h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        
                        <li>
                            <span class="active">Programación</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
<!--                     <div class="note note-info">
                        <p> A black page template with a minimal dependency assets to use as a base for any custom page you create </p>
                    </div> -->
                    <!-- END PAGE BASE CONTENT -->
                    <div class="row">
                       
                        <div class="col-md-12">
                            <div class="row" class="col-md-12">
                                <div class="portlet">
                                    <div class="portlet-body">
                                        <div class="tabbable-bordered">
                                            <div class="tab-pane" id="calendar_tab">
                                                    <div class="portlet light portlet-fit bordered  bordered calendar">
                                                        <div class="portlet-title">
                                                            <div class="caption">
                                                                <i class=" icon-layers font-green"></i>
                                                                <span class="caption-subject font-green sbold uppercase">Calendario</span>
                                                            </div>
                                                        </div>
                                                        <div class="portlet-body">
                                                            <div class="row">
                                                                <div class="col-md-3 col-sm-3">
                                                                    <div id="external-events">
                                                                        <!-- <form class="inline-form">
                                                                            <input type="text" value="" class="form-control" placeholder="Event Title..." id="event_title" />
                                                                            <br/>
                                                                            <a href="javascript:;" id="event_add" class="btn green"> Add Event </a>
                                                                        </form> -->
																		<span>Cliente:</span>
																		<select ng-change="getSucursalOrEquipo()" class="table-group-action-input form-control input-medium" ng-model="clientes">
																			<option value="">Clientes...</option>
																			<option ng-repeat="cliente in options.clientes" value="{{cliente.id}}">{{cliente.nombre}}</option>
																		</select>
																		<span>Sucursal:</span>
																		<select ng-change="getEquipo();" class="table-group-action-input form-control input-medium" id="sucursal" ng-model="sucursales" ng-disabled="clientes == 0">
																			<option ng-repeat="sucursal in options.sucursales" value="{{sucursal.id}}">{{sucursal.nombre}}</option>
																		</select>
																		<span>Tipo de Trabajo:</span>
																		<select id="tipoTrabajo" class="table-group-action-input form-control input-medium" ng-model="tipoTrabajo">
																			<option ng-repeat="cliente in options.tipoTrabajo" value="{{cliente.id}}">{{cliente.nombre}}</option>
																		</select>
																		<span>Equipos:</span>
																		<div style="max-height:80px; overflow:auto">
    																		<div id="equiposInput">
    																		</div>
																		</div>
																		<br>
																		<button type="button" class="btn btn-success" id="btnaddevent" ng-click="addEvent()">
                                                                        Agregar Evento</button>
																		<!--<div  ng-repeat="equipo in options.equipos" class="checkbox">
																		  <label><input type="checkbox" value="equipo-{{equipo.id}}">{{equipo.nombre}}</label>
																		</div>-->
																		
																	
																		<br>
																		<br>
																		
                                                                        <h3 class="event-form-title margin-bottom-20">Eventos sin agendar</h3>
																		<select onchange="printCal()" class="table-group-action-input form-control input-medium" id="gruposcalendario">
																			<option value="0">Ver todo</option>
                                                                            <option ng-repeat="superv in options.supervisores" value="{{superv.id}}">
                                                                                {{superv.nombre}}
                                                                            </option>
																		</select>
                                                                        <hr/>
                                                                        <div id="event_box" class="margin-bottom-10">
                                                                            
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-9 col-sm-9">
                                                                    <div id="calendar_object" class="has-toolbar"> </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>  
                                                    
                                            </div>
            
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>

            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->


            <!-- END QUICK SIDEBAR -->
 
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        